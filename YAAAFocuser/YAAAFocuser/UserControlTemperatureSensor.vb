﻿Imports System.ComponentModel

Public Class UserControlTemperatureSensor

    Private sensor_ As YAAASharedClassLibrary.YAAASensor = Nothing

    Private control_ As YAAASensorUserControlLibrary.IUserControlSensor = Nothing

    Private sensor_device_type_ As System.Type = Nothing

    Private sensor_unit_type_ As System.Type = Nothing

    Public Sub New(sensor As YAAASharedClassLibrary.YAAASensor, index As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        sensor_ = sensor.Clone

        GroupBox1.Text = "Temperature Sensor " & index

        ComboBoxSensorDevice.Items.Clear()
        ComboBoxSensorUnit.Items.Clear()

        sensor_device_type_ = GetType(YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t)
        sensor_unit_type_ = GetType(YAAASharedClassLibrary.YAAASensor.unit_temperature_t)

        ComboBoxSensorDevice.Items.AddRange([Enum].GetNames(sensor_device_type_))

        If [Enum].IsDefined(sensor_device_type_, CInt(sensor_.sensor_settings_.sensor_device_)) Then
            ComboBoxSensorDevice.SelectedItem = [Enum].Parse(sensor_device_type_, sensor_.sensor_settings_.sensor_device_).ToString
        End If

        ComboBoxSensorUnit.Items.AddRange([Enum].GetNames(sensor_unit_type_))

        If [Enum].IsDefined(sensor_unit_type_, CInt(sensor_.sensor_settings_.sensor_unit_)) Then
            ComboBoxSensorUnit.SelectedItem = [Enum].Parse(sensor_unit_type_, sensor_.sensor_settings_.sensor_unit_).ToString
        End If

        'if no valid sensor was selected or the sensor device stored in sensor_.sensor_settings_.sensor_device_ does not correspond to
        'any sensor devices known for this sensor type, select the first item of the drop down list.
        If ComboBoxSensorDevice.SelectedIndex < 0 Then
            ComboBoxSensorDevice.SelectedItem = "DEVICE_UNKNOWN"
        End If

        If ComboBoxSensorUnit.SelectedIndex < 0 Then
            ComboBoxSensorUnit.SelectedItem = "UNIT_UNKNOWN"
        End If
    End Sub

    Friend Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean
        Dim valid As Boolean = True

        If Not control_ Is Nothing Then
            valid = valid And control_.getSensor(sensor_)
        End If

        With sensor_.sensor_settings_
            .sensor_device_ = [Enum].Parse(sensor_device_type_, ComboBoxSensorDevice.SelectedItem.ToString)
            .sensor_unit_ = [Enum].Parse(sensor_unit_type_, ComboBoxSensorUnit.SelectedItem.ToString)
        End With

        sensor = sensor_.Clone()

        Return valid
    End Function

    Private Sub updateUserControl()
        FlowLayoutPanel1.Controls.Clear()

        control_ = Nothing

        If sensor_.sensor_settings_.sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_ANALOG_IC Then
            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureAnalogIC()
        ElseIf sensor_.sensor_settings_.sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_THERMISTOR Then
            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureThermistor()
        ElseIf sensor_.sensor_settings_.sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_ONEWIRE_DS18 Then
            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureOneWireDS18()
        ElseIf sensor_.sensor_settings_.sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t.DEVICE_MLX90614 Then
            control_ = New YAAASensorUserControlLibrary.UserControlSensorTemperatureMLX90614()
        End If

        If Not control_ Is Nothing Then
            control_.setSensor(sensor_)
            FlowLayoutPanel1.Controls.Add(control_)
        End If
    End Sub

    Private Sub ComboBoxSensorDevice_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxSensorDevice.SelectedIndexChanged
        sensorDeviceChanged()
    End Sub

    Private Sub sensorDeviceChanged()
        sensor_.sensor_settings_.sensor_device_ = 255

        If Not [Enum].TryParse(Of YAAASharedClassLibrary.YAAASensor.sensor_temperature_device_t)(ComboBoxSensorDevice.SelectedItem.ToString, sensor_.sensor_settings_.sensor_device_) Then
            sensor_.sensor_settings_.sensor_device_ = YAAASharedClassLibrary.YAAASensor.sensor_brightness_device_t.DEVICE_UNKNOWN
        End If

        updateUserControl()
    End Sub

    Private Sub ComboBoxSensorUnit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxSensorUnit.SelectedIndexChanged

    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs) Handles Label5.Click

    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub FlowLayoutPanel1_Paint(sender As Object, e As PaintEventArgs) Handles FlowLayoutPanel1.Paint

    End Sub
End Class
