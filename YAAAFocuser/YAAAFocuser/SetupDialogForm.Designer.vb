<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SetupDialogForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.chkTrace = New System.Windows.Forms.CheckBox()
        Me.ComboBoxComPort = New System.Windows.Forms.ComboBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxTempComp = New System.Windows.Forms.CheckBox()
        Me.TextBoxMicronsPerRev = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CheckBoxAbsolute = New System.Windows.Forms.CheckBox()
        Me.TextBoxMaxStep = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBoxMaxIncrement = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox23 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadioButtonNetwork = New System.Windows.Forms.RadioButton()
        Me.RadioButtonSerial = New System.Windows.Forms.RadioButton()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.GroupBox37 = New System.Windows.Forms.GroupBox()
        Me.TextBoxPort = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.TextBoxIPAddress = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.GroupBox36 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBoxBaudRate = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.ComboBoxTerminal = New System.Windows.Forms.ComboBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.ComboBoxI2CAddress = New System.Windows.Forms.ComboBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.ComboBoxStepType = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.CheckBoxPin5Inverted = New System.Windows.Forms.CheckBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.CheckBoxPin4Inverted = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CheckBoxPin6Inverted = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CheckBoxASR = New System.Windows.Forms.CheckBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.CheckBoxPin3Inverted = New System.Windows.Forms.CheckBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CheckBoxPin2Inverted = New System.Windows.Forms.CheckBox()
        Me.TextBoxMaxSpeed = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.CheckBoxPin1Inverted = New System.Windows.Forms.CheckBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBoxGearRatio = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBoxPin5 = New System.Windows.Forms.TextBox()
        Me.TextBoxStepsRev = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBoxPin6 = New System.Windows.Forms.TextBox()
        Me.ComboBoxMotorDriver = New System.Windows.Forms.ComboBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.TextBoxPin3 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBoxPin4 = New System.Windows.Forms.TextBox()
        Me.TextBoxPin2 = New System.Windows.Forms.TextBox()
        Me.TextBoxPin1 = New System.Windows.Forms.TextBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLimitUpper = New System.Windows.Forms.TextBox()
        Me.CheckBoxLimitUpperEnabled = New System.Windows.Forms.CheckBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLimitLower = New System.Windows.Forms.TextBox()
        Me.CheckBoxLimitLowerEnabled = New System.Windows.Forms.CheckBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.GroupBox28 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopUpperInverted = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopUpperPin = New System.Windows.Forms.TextBox()
        Me.CheckBoxEndstopUpperEnabled = New System.Windows.Forms.CheckBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupBox29 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopLowerInverted = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopLowerPin = New System.Windows.Forms.TextBox()
        Me.CheckBoxEndStopLowerEnabled = New System.Windows.Forms.CheckBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ButtonReadSwitchState = New System.Windows.Forms.Button()
        Me.CheckBoxHSTriggered = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESLTriggered = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESUTriggered = New System.Windows.Forms.CheckBox()
        Me.GroupBox22 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxMCLSI = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMCOutI = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMCInI = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMCA = New System.Windows.Forms.CheckBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.CheckBoxMCLSA = New System.Windows.Forms.CheckBox()
        Me.TextBoxMCIn = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.TextBoxMCOut = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.TextBoxMCL = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxHPS = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSI = New System.Windows.Forms.CheckBox()
        Me.CheckBoxSAH = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSA = New System.Windows.Forms.CheckBox()
        Me.TextBoxHomePosition = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextBoxHSN = New System.Windows.Forms.TextBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanelTemperatureSensors = New System.Windows.Forms.FlowLayoutPanel()
        Me.NumericUpDownTCSensor = New System.Windows.Forms.NumericUpDown()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBoxTCFactor = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.ButtonReadEEPROM = New System.Windows.Forms.Button()
        Me.ButtonWriteEEPROM = New System.Windows.Forms.Button()
        Me.CheckBoxEEPROMForceWrite = New System.Windows.Forms.CheckBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ButtonExportSettings = New System.Windows.Forms.Button()
        Me.ButtonImportSettings = New System.Windows.Forms.Button()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox37.SuspendLayout()
        Me.GroupBox36.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox28.SuspendLayout()
        Me.GroupBox29.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox22.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.NumericUpDownTCSensor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(452, 584)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'chkTrace
        '
        Me.chkTrace.AutoSize = True
        Me.chkTrace.Location = New System.Drawing.Point(458, 18)
        Me.chkTrace.Name = "chkTrace"
        Me.chkTrace.Size = New System.Drawing.Size(69, 17)
        Me.chkTrace.TabIndex = 8
        Me.chkTrace.Text = "Trace on"
        Me.chkTrace.UseVisualStyleBackColor = True
        '
        'ComboBoxComPort
        '
        Me.ComboBoxComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxComPort.FormattingEnabled = True
        Me.ComboBoxComPort.Location = New System.Drawing.Point(99, 13)
        Me.ComboBoxComPort.Name = "ComboBoxComPort"
        Me.ComboBoxComPort.Size = New System.Drawing.Size(89, 21)
        Me.ComboBoxComPort.TabIndex = 9
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(597, 529)
        Me.TabControl1.TabIndex = 10
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.GroupBox23)
        Me.TabPage1.Controls.Add(Me.PictureBox1)
        Me.TabPage1.Controls.Add(Me.chkTrace)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(589, 503)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBoxTempComp)
        Me.GroupBox1.Controls.Add(Me.TextBoxMicronsPerRev)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.CheckBoxAbsolute)
        Me.GroupBox1.Controls.Add(Me.TextBoxMaxStep)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.TextBoxMaxIncrement)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 164)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(382, 99)
        Me.GroupBox1.TabIndex = 91
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Focuser Settings"
        '
        'CheckBoxTempComp
        '
        Me.CheckBoxTempComp.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxTempComp.Location = New System.Drawing.Point(205, 42)
        Me.CheckBoxTempComp.Name = "CheckBoxTempComp"
        Me.CheckBoxTempComp.Size = New System.Drawing.Size(171, 39)
        Me.CheckBoxTempComp.TabIndex = 100
        Me.CheckBoxTempComp.Text = "Temperature Compensation Available"
        Me.CheckBoxTempComp.UseVisualStyleBackColor = True
        '
        'TextBoxMicronsPerRev
        '
        Me.TextBoxMicronsPerRev.Location = New System.Drawing.Point(105, 69)
        Me.TextBoxMicronsPerRev.Name = "TextBoxMicronsPerRev"
        Me.TextBoxMicronsPerRev.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxMicronsPerRev.TabIndex = 99
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(20, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 17)
        Me.Label1.TabIndex = 98
        Me.Label1.Text = "Microns / Rev"
        '
        'CheckBoxAbsolute
        '
        Me.CheckBoxAbsolute.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxAbsolute.Location = New System.Drawing.Point(205, 19)
        Me.CheckBoxAbsolute.Name = "CheckBoxAbsolute"
        Me.CheckBoxAbsolute.Size = New System.Drawing.Size(171, 17)
        Me.CheckBoxAbsolute.TabIndex = 97
        Me.CheckBoxAbsolute.Text = "Absolute Positioning"
        Me.CheckBoxAbsolute.UseVisualStyleBackColor = True
        '
        'TextBoxMaxStep
        '
        Me.TextBoxMaxStep.Location = New System.Drawing.Point(105, 43)
        Me.TextBoxMaxStep.Name = "TextBoxMaxStep"
        Me.TextBoxMaxStep.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxMaxStep.TabIndex = 96
        Me.ToolTip1.SetToolTip(Me.TextBoxMaxStep, "Maximum step position permitted. ")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(20, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 95
        Me.Label5.Text = "Max Step"
        Me.ToolTip1.SetToolTip(Me.Label5, "Maximum step position permitted. ")
        '
        'TextBoxMaxIncrement
        '
        Me.TextBoxMaxIncrement.Location = New System.Drawing.Point(105, 17)
        Me.TextBoxMaxIncrement.Name = "TextBoxMaxIncrement"
        Me.TextBoxMaxIncrement.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxMaxIncrement.TabIndex = 94
        Me.ToolTip1.SetToolTip(Me.TextBoxMaxIncrement, "Maximum number of steps allowed in one move operation. ")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 13)
        Me.Label2.TabIndex = 93
        Me.Label2.Text = "Max Increment"
        Me.ToolTip1.SetToolTip(Me.Label2, "Maximum number of steps allowed in one move operation. ")
        '
        'GroupBox23
        '
        Me.GroupBox23.Controls.Add(Me.Panel1)
        Me.GroupBox23.Controls.Add(Me.Label44)
        Me.GroupBox23.Controls.Add(Me.GroupBox37)
        Me.GroupBox23.Controls.Add(Me.GroupBox36)
        Me.GroupBox23.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Size = New System.Drawing.Size(382, 152)
        Me.GroupBox23.TabIndex = 90
        Me.GroupBox23.TabStop = False
        Me.GroupBox23.Text = "Communication"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButtonNetwork)
        Me.Panel1.Controls.Add(Me.RadioButtonSerial)
        Me.Panel1.Location = New System.Drawing.Point(96, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(280, 35)
        Me.Panel1.TabIndex = 90
        '
        'RadioButtonNetwork
        '
        Me.RadioButtonNetwork.AutoSize = True
        Me.RadioButtonNetwork.Location = New System.Drawing.Point(103, 9)
        Me.RadioButtonNetwork.Name = "RadioButtonNetwork"
        Me.RadioButtonNetwork.Size = New System.Drawing.Size(65, 17)
        Me.RadioButtonNetwork.TabIndex = 90
        Me.RadioButtonNetwork.TabStop = True
        Me.RadioButtonNetwork.Text = "Network"
        Me.RadioButtonNetwork.UseVisualStyleBackColor = True
        '
        'RadioButtonSerial
        '
        Me.RadioButtonSerial.AutoSize = True
        Me.RadioButtonSerial.Location = New System.Drawing.Point(14, 9)
        Me.RadioButtonSerial.Name = "RadioButtonSerial"
        Me.RadioButtonSerial.Size = New System.Drawing.Size(51, 17)
        Me.RadioButtonSerial.TabIndex = 90
        Me.RadioButtonSerial.TabStop = True
        Me.RadioButtonSerial.Text = "Serial"
        Me.RadioButtonSerial.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(12, 23)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(78, 13)
        Me.Label44.TabIndex = 90
        Me.Label44.Text = "Connect using:"
        '
        'GroupBox37
        '
        Me.GroupBox37.Controls.Add(Me.TextBoxPort)
        Me.GroupBox37.Controls.Add(Me.Label60)
        Me.GroupBox37.Controls.Add(Me.TextBoxIPAddress)
        Me.GroupBox37.Controls.Add(Me.Label58)
        Me.GroupBox37.Location = New System.Drawing.Point(6, 101)
        Me.GroupBox37.Name = "GroupBox37"
        Me.GroupBox37.Size = New System.Drawing.Size(370, 46)
        Me.GroupBox37.TabIndex = 91
        Me.GroupBox37.TabStop = False
        Me.GroupBox37.Text = "Network"
        '
        'TextBoxPort
        '
        Me.TextBoxPort.Location = New System.Drawing.Point(275, 19)
        Me.TextBoxPort.Name = "TextBoxPort"
        Me.TextBoxPort.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxPort.TabIndex = 91
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(194, 22)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(26, 13)
        Me.Label60.TabIndex = 81
        Me.Label60.Text = "Port"
        '
        'TextBoxIPAddress
        '
        Me.TextBoxIPAddress.Location = New System.Drawing.Point(99, 19)
        Me.TextBoxIPAddress.Name = "TextBoxIPAddress"
        Me.TextBoxIPAddress.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxIPAddress.TabIndex = 90
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(14, 22)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(17, 13)
        Me.Label58.TabIndex = 80
        Me.Label58.Text = "IP"
        '
        'GroupBox36
        '
        Me.GroupBox36.Controls.Add(Me.Label4)
        Me.GroupBox36.Controls.Add(Me.ComboBoxComPort)
        Me.GroupBox36.Controls.Add(Me.Label3)
        Me.GroupBox36.Controls.Add(Me.ComboBoxBaudRate)
        Me.GroupBox36.Location = New System.Drawing.Point(6, 53)
        Me.GroupBox36.Name = "GroupBox36"
        Me.GroupBox36.Size = New System.Drawing.Size(370, 42)
        Me.GroupBox36.TabIndex = 90
        Me.GroupBox36.TabStop = False
        Me.GroupBox36.Text = "Serial"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 78
        Me.Label4.Text = "Com Port"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label3.Location = New System.Drawing.Point(194, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 79
        Me.Label3.Text = "Baud Rate"
        '
        'ComboBoxBaudRate
        '
        Me.ComboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBaudRate.FormattingEnabled = True
        Me.ComboBoxBaudRate.Items.AddRange(New Object() {"300", "600", "1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "115200"})
        Me.ComboBoxBaudRate.Location = New System.Drawing.Point(275, 13)
        Me.ComboBoxBaudRate.Name = "ComboBoxBaudRate"
        Me.ComboBoxBaudRate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComboBoxBaudRate.Size = New System.Drawing.Size(89, 21)
        Me.ComboBoxBaudRate.TabIndex = 2
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = Global.ASCOM.YAAADevice.My.Resources.Resources.ASCOM
        Me.PictureBox1.Location = New System.Drawing.Point(533, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 56)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.ComboBoxTerminal)
        Me.TabPage2.Controls.Add(Me.Label49)
        Me.TabPage2.Controls.Add(Me.ComboBoxI2CAddress)
        Me.TabPage2.Controls.Add(Me.Label47)
        Me.TabPage2.Controls.Add(Me.Label48)
        Me.TabPage2.Controls.Add(Me.ComboBoxStepType)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin5Inverted)
        Me.TabPage2.Controls.Add(Me.Label46)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin4Inverted)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin6Inverted)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.CheckBoxASR)
        Me.TabPage2.Controls.Add(Me.Label45)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin3Inverted)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin2Inverted)
        Me.TabPage2.Controls.Add(Me.TextBoxMaxSpeed)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin1Inverted)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.TextBoxGearRatio)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.TextBoxPin5)
        Me.TabPage2.Controls.Add(Me.TextBoxStepsRev)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.TextBoxPin6)
        Me.TabPage2.Controls.Add(Me.ComboBoxMotorDriver)
        Me.TabPage2.Controls.Add(Me.Label50)
        Me.TabPage2.Controls.Add(Me.TextBoxPin3)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.TextBoxPin4)
        Me.TabPage2.Controls.Add(Me.TextBoxPin2)
        Me.TabPage2.Controls.Add(Me.TextBoxPin1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(589, 503)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Drive Control"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'ComboBoxTerminal
        '
        Me.ComboBoxTerminal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTerminal.DropDownWidth = 49
        Me.ComboBoxTerminal.FormattingEnabled = True
        Me.ComboBoxTerminal.Items.AddRange(New Object() {"1", "2"})
        Me.ComboBoxTerminal.Location = New System.Drawing.Point(198, 194)
        Me.ComboBoxTerminal.Name = "ComboBoxTerminal"
        Me.ComboBoxTerminal.Size = New System.Drawing.Size(49, 21)
        Me.ComboBoxTerminal.TabIndex = 29
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(9, 171)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(92, 13)
        Me.Label49.TabIndex = 129
        Me.Label49.Text = "Motor Driver Type"
        '
        'ComboBoxI2CAddress
        '
        Me.ComboBoxI2CAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxI2CAddress.DropDownWidth = 67
        Me.ComboBoxI2CAddress.FormattingEnabled = True
        Me.ComboBoxI2CAddress.Items.AddRange(New Object() {"0x60", "0x61", "0x62", "0x63", "0x64", "0x65", "0x66", "0x67", "0x68", "0x69", "0x6A", "0x6B", "0x6C", "0x6D", "0x6E", "0x6F"})
        Me.ComboBoxI2CAddress.Location = New System.Drawing.Point(125, 194)
        Me.ComboBoxI2CAddress.Name = "ComboBoxI2CAddress"
        Me.ComboBoxI2CAddress.Size = New System.Drawing.Size(67, 21)
        Me.ComboBoxI2CAddress.TabIndex = 28
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label47.Location = New System.Drawing.Point(9, 378)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(64, 13)
        Me.Label47.TabIndex = 127
        Me.Label47.Text = "Pin 6 / MS3"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(9, 352)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(64, 13)
        Me.Label48.TabIndex = 128
        Me.Label48.Text = "Pin 5 / MS2"
        '
        'ComboBoxStepType
        '
        Me.ComboBoxStepType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepType.DropDownWidth = 160
        Me.ComboBoxStepType.FormattingEnabled = True
        Me.ComboBoxStepType.Location = New System.Drawing.Point(125, 100)
        Me.ComboBoxStepType.Name = "ComboBoxStepType"
        Me.ComboBoxStepType.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxStepType.TabIndex = 24
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label12.Location = New System.Drawing.Point(9, 326)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 13)
        Me.Label12.TabIndex = 125
        Me.Label12.Text = "Pin 4 / MS1"
        '
        'CheckBoxPin5Inverted
        '
        Me.CheckBoxPin5Inverted.AutoSize = True
        Me.CheckBoxPin5Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5Inverted.Location = New System.Drawing.Point(226, 351)
        Me.CheckBoxPin5Inverted.Name = "CheckBoxPin5Inverted"
        Me.CheckBoxPin5Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin5Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin5Inverted.TabIndex = 40
        Me.CheckBoxPin5Inverted.UseVisualStyleBackColor = True
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(9, 300)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(75, 13)
        Me.Label46.TabIndex = 126
        Me.Label46.Text = "Pin 3 / Enable"
        '
        'CheckBoxPin4Inverted
        '
        Me.CheckBoxPin4Inverted.AutoSize = True
        Me.CheckBoxPin4Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4Inverted.Location = New System.Drawing.Point(226, 325)
        Me.CheckBoxPin4Inverted.Name = "CheckBoxPin4Inverted"
        Me.CheckBoxPin4Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin4Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin4Inverted.TabIndex = 39
        Me.CheckBoxPin4Inverted.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label6.Location = New System.Drawing.Point(9, 274)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 123
        Me.Label6.Text = "Pin 2 / Direction"
        '
        'CheckBoxPin6Inverted
        '
        Me.CheckBoxPin6Inverted.AutoSize = True
        Me.CheckBoxPin6Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6Inverted.Location = New System.Drawing.Point(226, 377)
        Me.CheckBoxPin6Inverted.Name = "CheckBoxPin6Inverted"
        Me.CheckBoxPin6Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin6Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin6Inverted.TabIndex = 41
        Me.CheckBoxPin6Inverted.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 248)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 124
        Me.Label7.Text = "Pin 1 / Step"
        '
        'CheckBoxASR
        '
        Me.CheckBoxASR.AutoSize = True
        Me.CheckBoxASR.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASR.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASR.Location = New System.Drawing.Point(182, 127)
        Me.CheckBoxASR.Name = "CheckBoxASR"
        Me.CheckBoxASR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxASR.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxASR.TabIndex = 26
        Me.CheckBoxASR.Text = "Enabled"
        Me.CheckBoxASR.UseVisualStyleBackColor = True
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(9, 197)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(112, 13)
        Me.Label45.TabIndex = 122
        Me.Label45.Text = "I�C Address / Terminal"
        '
        'CheckBoxPin3Inverted
        '
        Me.CheckBoxPin3Inverted.AutoSize = True
        Me.CheckBoxPin3Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3Inverted.Location = New System.Drawing.Point(226, 299)
        Me.CheckBoxPin3Inverted.Name = "CheckBoxPin3Inverted"
        Me.CheckBoxPin3Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin3Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin3Inverted.TabIndex = 38
        Me.CheckBoxPin3Inverted.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(9, 128)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(102, 26)
        Me.Label14.TabIndex = 121
        Me.Label14.Text = "Automatic Stepper Release"
        '
        'CheckBoxPin2Inverted
        '
        Me.CheckBoxPin2Inverted.AutoSize = True
        Me.CheckBoxPin2Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2Inverted.Location = New System.Drawing.Point(226, 273)
        Me.CheckBoxPin2Inverted.Name = "CheckBoxPin2Inverted"
        Me.CheckBoxPin2Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin2Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin2Inverted.TabIndex = 37
        Me.CheckBoxPin2Inverted.UseVisualStyleBackColor = True
        '
        'TextBoxMaxSpeed
        '
        Me.TextBoxMaxSpeed.Location = New System.Drawing.Point(125, 24)
        Me.TextBoxMaxSpeed.Name = "TextBoxMaxSpeed"
        Me.TextBoxMaxSpeed.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxMaxSpeed.TabIndex = 20
        Me.ToolTip1.SetToolTip(Me.TextBoxMaxSpeed, "Maximum Stepper Motor speed in steps / second.")
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(9, 53)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 13)
        Me.Label9.TabIndex = 116
        Me.Label9.Text = "Steps / Rev"
        '
        'CheckBoxPin1Inverted
        '
        Me.CheckBoxPin1Inverted.AutoSize = True
        Me.CheckBoxPin1Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1Inverted.Location = New System.Drawing.Point(226, 247)
        Me.CheckBoxPin1Inverted.Name = "CheckBoxPin1Inverted"
        Me.CheckBoxPin1Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin1Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin1Inverted.TabIndex = 36
        Me.CheckBoxPin1Inverted.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(9, 103)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 119
        Me.Label13.Text = "Step Type"
        '
        'TextBoxGearRatio
        '
        Me.TextBoxGearRatio.Location = New System.Drawing.Point(125, 74)
        Me.TextBoxGearRatio.Name = "TextBoxGearRatio"
        Me.TextBoxGearRatio.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxGearRatio.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label8.Location = New System.Drawing.Point(9, 77)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 115
        Me.Label8.Text = "Gear Ratio"
        '
        'TextBoxPin5
        '
        Me.TextBoxPin5.Location = New System.Drawing.Point(125, 348)
        Me.TextBoxPin5.Name = "TextBoxPin5"
        Me.TextBoxPin5.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin5.TabIndex = 34
        '
        'TextBoxStepsRev
        '
        Me.TextBoxStepsRev.Location = New System.Drawing.Point(125, 50)
        Me.TextBoxStepsRev.Name = "TextBoxStepsRev"
        Me.TextBoxStepsRev.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxStepsRev.TabIndex = 22
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 13)
        Me.Label11.TabIndex = 118
        Me.Label11.Text = "Max Speed"
        Me.ToolTip1.SetToolTip(Me.Label11, "Maximum Stepper Motor speed in steps / second.")
        '
        'TextBoxPin6
        '
        Me.TextBoxPin6.Location = New System.Drawing.Point(125, 374)
        Me.TextBoxPin6.Name = "TextBoxPin6"
        Me.TextBoxPin6.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin6.TabIndex = 35
        '
        'ComboBoxMotorDriver
        '
        Me.ComboBoxMotorDriver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMotorDriver.DropDownWidth = 200
        Me.ComboBoxMotorDriver.FormattingEnabled = True
        Me.ComboBoxMotorDriver.Location = New System.Drawing.Point(125, 167)
        Me.ComboBoxMotorDriver.Name = "ComboBoxMotorDriver"
        Me.ComboBoxMotorDriver.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxMotorDriver.TabIndex = 27
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(201, 228)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(46, 13)
        Me.Label50.TabIndex = 102
        Me.Label50.Text = "Inverted"
        '
        'TextBoxPin3
        '
        Me.TextBoxPin3.Location = New System.Drawing.Point(125, 296)
        Me.TextBoxPin3.Name = "TextBoxPin3"
        Me.TextBoxPin3.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin3.TabIndex = 32
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(125, 228)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(62, 13)
        Me.Label18.TabIndex = 102
        Me.Label18.Text = "Pin Number"
        '
        'TextBoxPin4
        '
        Me.TextBoxPin4.Location = New System.Drawing.Point(125, 322)
        Me.TextBoxPin4.Name = "TextBoxPin4"
        Me.TextBoxPin4.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin4.TabIndex = 33
        '
        'TextBoxPin2
        '
        Me.TextBoxPin2.Location = New System.Drawing.Point(125, 270)
        Me.TextBoxPin2.Name = "TextBoxPin2"
        Me.TextBoxPin2.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin2.TabIndex = 31
        '
        'TextBoxPin1
        '
        Me.TextBoxPin1.Location = New System.Drawing.Point(125, 244)
        Me.TextBoxPin1.Name = "TextBoxPin1"
        Me.TextBoxPin1.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin1.TabIndex = 30
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.GroupBox10)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(589, 503)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Endstops and Limits"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Controls.Add(Me.GroupBox6)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 160)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(164, 151)
        Me.GroupBox4.TabIndex = 89
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Limits"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBoxLimitUpper)
        Me.GroupBox5.Controls.Add(Me.CheckBoxLimitUpperEnabled)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(149, 60)
        Me.GroupBox5.TabIndex = 25
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Upper"
        '
        'TextBoxLimitUpper
        '
        Me.TextBoxLimitUpper.Location = New System.Drawing.Point(78, 34)
        Me.TextBoxLimitUpper.Name = "TextBoxLimitUpper"
        Me.TextBoxLimitUpper.Size = New System.Drawing.Size(65, 20)
        Me.TextBoxLimitUpper.TabIndex = 13
        '
        'CheckBoxLimitUpperEnabled
        '
        Me.CheckBoxLimitUpperEnabled.AutoSize = True
        Me.CheckBoxLimitUpperEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLimitUpperEnabled.Location = New System.Drawing.Point(6, 17)
        Me.CheckBoxLimitUpperEnabled.Name = "CheckBoxLimitUpperEnabled"
        Me.CheckBoxLimitUpperEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxLimitUpperEnabled.TabIndex = 14
        Me.CheckBoxLimitUpperEnabled.Text = "Enabled"
        Me.CheckBoxLimitUpperEnabled.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 37)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(74, 13)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "Limit (Microns)"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TextBoxLimitLower)
        Me.GroupBox6.Controls.Add(Me.CheckBoxLimitLowerEnabled)
        Me.GroupBox6.Controls.Add(Me.Label20)
        Me.GroupBox6.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(149, 58)
        Me.GroupBox6.TabIndex = 24
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Lower"
        '
        'TextBoxLimitLower
        '
        Me.TextBoxLimitLower.Location = New System.Drawing.Point(78, 32)
        Me.TextBoxLimitLower.Name = "TextBoxLimitLower"
        Me.TextBoxLimitLower.Size = New System.Drawing.Size(65, 20)
        Me.TextBoxLimitLower.TabIndex = 10
        '
        'CheckBoxLimitLowerEnabled
        '
        Me.CheckBoxLimitLowerEnabled.AutoSize = True
        Me.CheckBoxLimitLowerEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLimitLowerEnabled.Location = New System.Drawing.Point(6, 15)
        Me.CheckBoxLimitLowerEnabled.Name = "CheckBoxLimitLowerEnabled"
        Me.CheckBoxLimitLowerEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxLimitLowerEnabled.TabIndex = 11
        Me.CheckBoxLimitLowerEnabled.Text = "Enabled"
        Me.CheckBoxLimitLowerEnabled.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 35)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(74, 13)
        Me.Label20.TabIndex = 15
        Me.Label20.Text = "Limit (Microns)"
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.GroupBox28)
        Me.GroupBox10.Controls.Add(Me.GroupBox29)
        Me.GroupBox10.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(164, 151)
        Me.GroupBox10.TabIndex = 88
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Endstops"
        '
        'GroupBox28
        '
        Me.GroupBox28.Controls.Add(Me.CheckBoxEndstopUpperInverted)
        Me.GroupBox28.Controls.Add(Me.TextBoxEndstopUpperPin)
        Me.GroupBox28.Controls.Add(Me.CheckBoxEndstopUpperEnabled)
        Me.GroupBox28.Controls.Add(Me.Label22)
        Me.GroupBox28.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox28.Name = "GroupBox28"
        Me.GroupBox28.Size = New System.Drawing.Size(149, 60)
        Me.GroupBox28.TabIndex = 25
        Me.GroupBox28.TabStop = False
        Me.GroupBox28.Text = "Upper"
        '
        'CheckBoxEndstopUpperInverted
        '
        Me.CheckBoxEndstopUpperInverted.AutoSize = True
        Me.CheckBoxEndstopUpperInverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperInverted.Location = New System.Drawing.Point(78, 17)
        Me.CheckBoxEndstopUpperInverted.Name = "CheckBoxEndstopUpperInverted"
        Me.CheckBoxEndstopUpperInverted.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperInverted.TabIndex = 15
        Me.CheckBoxEndstopUpperInverted.Text = "Inverted"
        Me.CheckBoxEndstopUpperInverted.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopUpperPin
        '
        Me.TextBoxEndstopUpperPin.Location = New System.Drawing.Point(78, 34)
        Me.TextBoxEndstopUpperPin.Name = "TextBoxEndstopUpperPin"
        Me.TextBoxEndstopUpperPin.Size = New System.Drawing.Size(65, 20)
        Me.TextBoxEndstopUpperPin.TabIndex = 13
        '
        'CheckBoxEndstopUpperEnabled
        '
        Me.CheckBoxEndstopUpperEnabled.AutoSize = True
        Me.CheckBoxEndstopUpperEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperEnabled.Location = New System.Drawing.Point(6, 17)
        Me.CheckBoxEndstopUpperEnabled.Name = "CheckBoxEndstopUpperEnabled"
        Me.CheckBoxEndstopUpperEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperEnabled.TabIndex = 14
        Me.CheckBoxEndstopUpperEnabled.Text = "Enabled"
        Me.CheckBoxEndstopUpperEnabled.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 37)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(62, 13)
        Me.Label22.TabIndex = 20
        Me.Label22.Text = "Pin Number"
        '
        'GroupBox29
        '
        Me.GroupBox29.Controls.Add(Me.CheckBoxEndstopLowerInverted)
        Me.GroupBox29.Controls.Add(Me.TextBoxEndstopLowerPin)
        Me.GroupBox29.Controls.Add(Me.CheckBoxEndStopLowerEnabled)
        Me.GroupBox29.Controls.Add(Me.Label40)
        Me.GroupBox29.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox29.Name = "GroupBox29"
        Me.GroupBox29.Size = New System.Drawing.Size(149, 58)
        Me.GroupBox29.TabIndex = 24
        Me.GroupBox29.TabStop = False
        Me.GroupBox29.Text = "Lower"
        '
        'CheckBoxEndstopLowerInverted
        '
        Me.CheckBoxEndstopLowerInverted.AutoSize = True
        Me.CheckBoxEndstopLowerInverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopLowerInverted.Location = New System.Drawing.Point(78, 13)
        Me.CheckBoxEndstopLowerInverted.Name = "CheckBoxEndstopLowerInverted"
        Me.CheckBoxEndstopLowerInverted.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopLowerInverted.TabIndex = 12
        Me.CheckBoxEndstopLowerInverted.Text = "Inverted"
        Me.CheckBoxEndstopLowerInverted.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopLowerPin
        '
        Me.TextBoxEndstopLowerPin.Location = New System.Drawing.Point(78, 32)
        Me.TextBoxEndstopLowerPin.Name = "TextBoxEndstopLowerPin"
        Me.TextBoxEndstopLowerPin.Size = New System.Drawing.Size(65, 20)
        Me.TextBoxEndstopLowerPin.TabIndex = 10
        '
        'CheckBoxEndStopLowerEnabled
        '
        Me.CheckBoxEndStopLowerEnabled.AutoSize = True
        Me.CheckBoxEndStopLowerEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndStopLowerEnabled.Location = New System.Drawing.Point(6, 13)
        Me.CheckBoxEndStopLowerEnabled.Name = "CheckBoxEndStopLowerEnabled"
        Me.CheckBoxEndStopLowerEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndStopLowerEnabled.TabIndex = 11
        Me.CheckBoxEndStopLowerEnabled.Text = "Enabled"
        Me.CheckBoxEndStopLowerEnabled.UseVisualStyleBackColor = True
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(6, 35)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(62, 13)
        Me.Label40.TabIndex = 15
        Me.Label40.Text = "Pin Number"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Controls.Add(Me.GroupBox22)
        Me.TabPage4.Controls.Add(Me.GroupBox3)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(589, 503)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Homing and Manual Control"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ButtonReadSwitchState)
        Me.GroupBox2.Controls.Add(Me.CheckBoxHSTriggered)
        Me.GroupBox2.Controls.Add(Me.CheckBoxESLTriggered)
        Me.GroupBox2.Controls.Add(Me.CheckBoxESUTriggered)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 394)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(171, 83)
        Me.GroupBox2.TabIndex = 113
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Switch State"
        '
        'ButtonReadSwitchState
        '
        Me.ButtonReadSwitchState.Location = New System.Drawing.Point(6, 19)
        Me.ButtonReadSwitchState.Name = "ButtonReadSwitchState"
        Me.ButtonReadSwitchState.Size = New System.Drawing.Size(57, 48)
        Me.ButtonReadSwitchState.TabIndex = 115
        Me.ButtonReadSwitchState.Text = "Read Switch State"
        Me.ButtonReadSwitchState.UseVisualStyleBackColor = True
        '
        'CheckBoxHSTriggered
        '
        Me.CheckBoxHSTriggered.AutoSize = True
        Me.CheckBoxHSTriggered.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSTriggered.Enabled = False
        Me.CheckBoxHSTriggered.Location = New System.Drawing.Point(76, 59)
        Me.CheckBoxHSTriggered.Name = "CheckBoxHSTriggered"
        Me.CheckBoxHSTriggered.Size = New System.Drawing.Size(89, 17)
        Me.CheckBoxHSTriggered.TabIndex = 118
        Me.CheckBoxHSTriggered.TabStop = False
        Me.CheckBoxHSTriggered.Text = "Home Switch"
        Me.CheckBoxHSTriggered.UseVisualStyleBackColor = True
        '
        'CheckBoxESLTriggered
        '
        Me.CheckBoxESLTriggered.AutoSize = True
        Me.CheckBoxESLTriggered.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESLTriggered.Enabled = False
        Me.CheckBoxESLTriggered.Location = New System.Drawing.Point(68, 13)
        Me.CheckBoxESLTriggered.Name = "CheckBoxESLTriggered"
        Me.CheckBoxESLTriggered.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESLTriggered.TabIndex = 116
        Me.CheckBoxESLTriggered.TabStop = False
        Me.CheckBoxESLTriggered.Text = "Lower Endstop"
        Me.CheckBoxESLTriggered.UseVisualStyleBackColor = True
        '
        'CheckBoxESUTriggered
        '
        Me.CheckBoxESUTriggered.AutoSize = True
        Me.CheckBoxESUTriggered.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESUTriggered.Enabled = False
        Me.CheckBoxESUTriggered.Location = New System.Drawing.Point(68, 36)
        Me.CheckBoxESUTriggered.Name = "CheckBoxESUTriggered"
        Me.CheckBoxESUTriggered.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESUTriggered.TabIndex = 117
        Me.CheckBoxESUTriggered.TabStop = False
        Me.CheckBoxESUTriggered.Text = "Upper Endstop"
        Me.CheckBoxESUTriggered.UseVisualStyleBackColor = True
        '
        'GroupBox22
        '
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCLSI)
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCOutI)
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCInI)
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCA)
        Me.GroupBox22.Controls.Add(Me.Label16)
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCLSA)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCIn)
        Me.GroupBox22.Controls.Add(Me.Label56)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCOut)
        Me.GroupBox22.Controls.Add(Me.Label57)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCL)
        Me.GroupBox22.Location = New System.Drawing.Point(3, 174)
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.Size = New System.Drawing.Size(171, 214)
        Me.GroupBox22.TabIndex = 112
        Me.GroupBox22.TabStop = False
        Me.GroupBox22.Text = "Manual Control"
        '
        'CheckBoxMCLSI
        '
        Me.CheckBoxMCLSI.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCLSI.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCLSI.Location = New System.Drawing.Point(6, 143)
        Me.CheckBoxMCLSI.Name = "CheckBoxMCLSI"
        Me.CheckBoxMCLSI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxMCLSI.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxMCLSI.TabIndex = 115
        Me.CheckBoxMCLSI.Text = "Lock Switch inverted"
        Me.CheckBoxMCLSI.UseVisualStyleBackColor = True
        '
        'CheckBoxMCOutI
        '
        Me.CheckBoxMCOutI.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCOutI.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCOutI.Location = New System.Drawing.Point(6, 189)
        Me.CheckBoxMCOutI.Name = "CheckBoxMCOutI"
        Me.CheckBoxMCOutI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxMCOutI.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxMCOutI.TabIndex = 114
        Me.CheckBoxMCOutI.Text = "Outward Switch inverted"
        Me.CheckBoxMCOutI.UseVisualStyleBackColor = True
        '
        'CheckBoxMCInI
        '
        Me.CheckBoxMCInI.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCInI.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCInI.Location = New System.Drawing.Point(6, 166)
        Me.CheckBoxMCInI.Name = "CheckBoxMCInI"
        Me.CheckBoxMCInI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxMCInI.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxMCInI.TabIndex = 113
        Me.CheckBoxMCInI.Text = "Inward Switch inverted"
        Me.CheckBoxMCInI.UseVisualStyleBackColor = True
        '
        'CheckBoxMCA
        '
        Me.CheckBoxMCA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCA.Location = New System.Drawing.Point(6, 19)
        Me.CheckBoxMCA.Name = "CheckBoxMCA"
        Me.CheckBoxMCA.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxMCA.TabIndex = 112
        Me.CheckBoxMCA.TabStop = False
        Me.CheckBoxMCA.Text = "Available"
        Me.CheckBoxMCA.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 68)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(49, 13)
        Me.Label16.TabIndex = 111
        Me.Label16.Text = "Lock Pin"
        '
        'CheckBoxMCLSA
        '
        Me.CheckBoxMCLSA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCLSA.Location = New System.Drawing.Point(6, 42)
        Me.CheckBoxMCLSA.Name = "CheckBoxMCLSA"
        Me.CheckBoxMCLSA.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxMCLSA.TabIndex = 110
        Me.CheckBoxMCLSA.TabStop = False
        Me.CheckBoxMCLSA.Text = "Lock Switch Available"
        Me.CheckBoxMCLSA.UseVisualStyleBackColor = True
        '
        'TextBoxMCIn
        '
        Me.TextBoxMCIn.Location = New System.Drawing.Point(105, 91)
        Me.TextBoxMCIn.Name = "TextBoxMCIn"
        Me.TextBoxMCIn.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCIn.TabIndex = 15
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(6, 94)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(57, 13)
        Me.Label56.TabIndex = 97
        Me.Label56.Text = "Inward Pin"
        '
        'TextBoxMCOut
        '
        Me.TextBoxMCOut.Location = New System.Drawing.Point(105, 117)
        Me.TextBoxMCOut.Name = "TextBoxMCOut"
        Me.TextBoxMCOut.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCOut.TabIndex = 16
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(6, 120)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(65, 13)
        Me.Label57.TabIndex = 99
        Me.Label57.Text = "Outward Pin"
        '
        'TextBoxMCL
        '
        Me.TextBoxMCL.Location = New System.Drawing.Point(105, 65)
        Me.TextBoxMCL.Name = "TextBoxMCL"
        Me.TextBoxMCL.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCL.TabIndex = 36
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CheckBoxHPS)
        Me.GroupBox3.Controls.Add(Me.CheckBoxHSI)
        Me.GroupBox3.Controls.Add(Me.CheckBoxSAH)
        Me.GroupBox3.Controls.Add(Me.CheckBoxHSA)
        Me.GroupBox3.Controls.Add(Me.TextBoxHomePosition)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.TextBoxHSN)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(171, 165)
        Me.GroupBox3.TabIndex = 83
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Homing Control"
        '
        'CheckBoxHPS
        '
        Me.CheckBoxHPS.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHPS.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHPS.Location = New System.Drawing.Point(6, 117)
        Me.CheckBoxHPS.Name = "CheckBoxHPS"
        Me.CheckBoxHPS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHPS.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxHPS.TabIndex = 95
        Me.CheckBoxHPS.Text = "Home Position Sync"
        Me.CheckBoxHPS.UseVisualStyleBackColor = True
        '
        'CheckBoxHSI
        '
        Me.CheckBoxHSI.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSI.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSI.Location = New System.Drawing.Point(6, 42)
        Me.CheckBoxHSI.Name = "CheckBoxHSI"
        Me.CheckBoxHSI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSI.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxHSI.TabIndex = 11
        Me.CheckBoxHSI.Text = "Home Switch inverted"
        Me.CheckBoxHSI.UseVisualStyleBackColor = True
        '
        'CheckBoxSAH
        '
        Me.CheckBoxSAH.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSAH.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CheckBoxSAH.Location = New System.Drawing.Point(6, 140)
        Me.CheckBoxSAH.Name = "CheckBoxSAH"
        Me.CheckBoxSAH.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxSAH.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxSAH.TabIndex = 5
        Me.CheckBoxSAH.Text = "Startup Auto home"
        Me.CheckBoxSAH.UseVisualStyleBackColor = True
        '
        'CheckBoxHSA
        '
        Me.CheckBoxHSA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSA.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSA.Location = New System.Drawing.Point(6, 19)
        Me.CheckBoxHSA.Name = "CheckBoxHSA"
        Me.CheckBoxHSA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSA.Size = New System.Drawing.Size(159, 17)
        Me.CheckBoxHSA.TabIndex = 10
        Me.CheckBoxHSA.Text = "Home Switch enabled"
        Me.CheckBoxHSA.UseVisualStyleBackColor = True
        '
        'TextBoxHomePosition
        '
        Me.TextBoxHomePosition.Location = New System.Drawing.Point(104, 91)
        Me.TextBoxHomePosition.Name = "TextBoxHomePosition"
        Me.TextBoxHomePosition.Size = New System.Drawing.Size(61, 20)
        Me.TextBoxHomePosition.TabIndex = 13
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 68)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(62, 13)
        Me.Label17.TabIndex = 51
        Me.Label17.Text = "Pin Number"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(6, 94)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(75, 13)
        Me.Label21.TabIndex = 94
        Me.Label21.Text = "Home Position"
        '
        'TextBoxHSN
        '
        Me.TextBoxHSN.Location = New System.Drawing.Point(104, 65)
        Me.TextBoxHSN.Name = "TextBoxHSN"
        Me.TextBoxHSN.Size = New System.Drawing.Size(61, 20)
        Me.TextBoxHSN.TabIndex = 12
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.FlowLayoutPanelTemperatureSensors)
        Me.TabPage5.Controls.Add(Me.NumericUpDownTCSensor)
        Me.TabPage5.Controls.Add(Me.Label10)
        Me.TabPage5.Controls.Add(Me.Label15)
        Me.TabPage5.Controls.Add(Me.TextBoxTCFactor)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(589, 503)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Temperature Sensors"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanelTemperatureSensors
        '
        Me.FlowLayoutPanelTemperatureSensors.AutoScroll = True
        Me.FlowLayoutPanelTemperatureSensors.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanelTemperatureSensors.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanelTemperatureSensors.Name = "FlowLayoutPanelTemperatureSensors"
        Me.FlowLayoutPanelTemperatureSensors.Size = New System.Drawing.Size(511, 426)
        Me.FlowLayoutPanelTemperatureSensors.TabIndex = 96
        Me.FlowLayoutPanelTemperatureSensors.WrapContents = False
        '
        'NumericUpDownTCSensor
        '
        Me.NumericUpDownTCSensor.Location = New System.Drawing.Point(188, 435)
        Me.NumericUpDownTCSensor.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumericUpDownTCSensor.Name = "NumericUpDownTCSensor"
        Me.NumericUpDownTCSensor.Size = New System.Drawing.Size(89, 20)
        Me.NumericUpDownTCSensor.TabIndex = 95
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(6, 437)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(176, 18)
        Me.Label10.TabIndex = 80
        Me.Label10.Text = "Temperature Compensation Sensor"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 464)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(152, 13)
        Me.Label15.TabIndex = 91
        Me.Label15.Text = "Compensation Factor (�m / �K)"
        '
        'TextBoxTCFactor
        '
        Me.TextBoxTCFactor.Location = New System.Drawing.Point(188, 461)
        Me.TextBoxTCFactor.Name = "TextBoxTCFactor"
        Me.TextBoxTCFactor.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxTCFactor.TabIndex = 92
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBoxEEPROMForceWrite, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(12, 543)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.9863!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.0137!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(141, 73)
        Me.TableLayoutPanel3.TabIndex = 91
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonReadEEPROM, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonWriteEEPROM, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 30)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(135, 40)
        Me.TableLayoutPanel2.TabIndex = 89
        '
        'ButtonReadEEPROM
        '
        Me.ButtonReadEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonReadEEPROM.Location = New System.Drawing.Point(3, 3)
        Me.ButtonReadEEPROM.Name = "ButtonReadEEPROM"
        Me.ButtonReadEEPROM.Size = New System.Drawing.Size(61, 34)
        Me.ButtonReadEEPROM.TabIndex = 83
        Me.ButtonReadEEPROM.Text = "Read EEPROM"
        '
        'ButtonWriteEEPROM
        '
        Me.ButtonWriteEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonWriteEEPROM.Cursor = System.Windows.Forms.Cursors.Default
        Me.ButtonWriteEEPROM.Location = New System.Drawing.Point(70, 3)
        Me.ButtonWriteEEPROM.Name = "ButtonWriteEEPROM"
        Me.ButtonWriteEEPROM.Size = New System.Drawing.Size(62, 34)
        Me.ButtonWriteEEPROM.TabIndex = 84
        Me.ButtonWriteEEPROM.Text = "Write EEPROM"
        '
        'CheckBoxEEPROMForceWrite
        '
        Me.CheckBoxEEPROMForceWrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxEEPROMForceWrite.AutoSize = True
        Me.CheckBoxEEPROMForceWrite.Location = New System.Drawing.Point(3, 7)
        Me.CheckBoxEEPROMForceWrite.Name = "CheckBoxEEPROMForceWrite"
        Me.CheckBoxEEPROMForceWrite.Size = New System.Drawing.Size(127, 17)
        Me.CheckBoxEEPROMForceWrite.TabIndex = 86
        Me.CheckBoxEEPROMForceWrite.Text = "Force EEPROM write"
        Me.CheckBoxEEPROMForceWrite.UseVisualStyleBackColor = True
        '
        'ButtonExportSettings
        '
        Me.ButtonExportSettings.Location = New System.Drawing.Point(3, 3)
        Me.ButtonExportSettings.Name = "ButtonExportSettings"
        Me.ButtonExportSettings.Size = New System.Drawing.Size(67, 21)
        Me.ButtonExportSettings.TabIndex = 93
        Me.ButtonExportSettings.Text = "Export"
        Me.ToolTip1.SetToolTip(Me.ButtonExportSettings, "Export current settings to a .xml file.")
        Me.ButtonExportSettings.UseVisualStyleBackColor = True
        '
        'ButtonImportSettings
        '
        Me.ButtonImportSettings.Location = New System.Drawing.Point(76, 3)
        Me.ButtonImportSettings.Name = "ButtonImportSettings"
        Me.ButtonImportSettings.Size = New System.Drawing.Size(67, 21)
        Me.ButtonImportSettings.TabIndex = 94
        Me.ButtonImportSettings.Text = "Import"
        Me.ToolTip1.SetToolTip(Me.ButtonImportSettings, "Import settings from a .xml file.")
        Me.ButtonImportSettings.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonExportSettings, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonImportSettings, 1, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(452, 554)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(146, 27)
        Me.TableLayoutPanel4.TabIndex = 95
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSplitButton1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 625)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(610, 22)
        Me.StatusStrip1.TabIndex = 96
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(132, 20)
        Me.ToolStripSplitButton1.Text = "ToolStripSplitButton1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(120, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'SetupDialogForm
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(610, 647)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel4)
        Me.Controls.Add(Me.TableLayoutPanel3)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SetupDialogForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DeviceName Setup"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        Me.GroupBox23.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox37.ResumeLayout(False)
        Me.GroupBox37.PerformLayout()
        Me.GroupBox36.ResumeLayout(False)
        Me.GroupBox36.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox28.ResumeLayout(False)
        Me.GroupBox28.PerformLayout()
        Me.GroupBox29.ResumeLayout(False)
        Me.GroupBox29.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox22.ResumeLayout(False)
        Me.GroupBox22.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.NumericUpDownTCSensor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents chkTrace As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxComPort As System.Windows.Forms.ComboBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GroupBox23 As GroupBox
    Friend WithEvents RadioButtonNetwork As RadioButton
    Friend WithEvents RadioButtonSerial As RadioButton
    Friend WithEvents Label44 As Label
    Friend WithEvents GroupBox37 As GroupBox
    Friend WithEvents TextBoxPort As TextBox
    Private WithEvents Label60 As Label
    Friend WithEvents TextBoxIPAddress As TextBox
    Private WithEvents Label58 As Label
    Friend WithEvents GroupBox36 As GroupBox
    Private WithEvents Label4 As Label
    Private WithEvents Label3 As Label
    Friend WithEvents ComboBoxBaudRate As ComboBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents CheckBoxTempComp As CheckBox
    Friend WithEvents TextBoxMicronsPerRev As TextBox
    Private WithEvents Label1 As Label
    Friend WithEvents CheckBoxAbsolute As CheckBox
    Friend WithEvents TextBoxMaxStep As TextBox
    Private WithEvents Label5 As Label
    Friend WithEvents TextBoxMaxIncrement As TextBox
    Private WithEvents Label2 As Label
    Friend WithEvents ComboBoxTerminal As ComboBox
    Private WithEvents Label49 As Label
    Friend WithEvents ComboBoxI2CAddress As ComboBox
    Private WithEvents Label47 As Label
    Private WithEvents Label48 As Label
    Friend WithEvents ComboBoxStepType As ComboBox
    Private WithEvents Label12 As Label
    Friend WithEvents CheckBoxPin5Inverted As CheckBox
    Private WithEvents Label46 As Label
    Friend WithEvents CheckBoxPin4Inverted As CheckBox
    Private WithEvents Label6 As Label
    Friend WithEvents CheckBoxPin6Inverted As CheckBox
    Private WithEvents Label7 As Label
    Friend WithEvents CheckBoxASR As CheckBox
    Private WithEvents Label45 As Label
    Friend WithEvents CheckBoxPin3Inverted As CheckBox
    Private WithEvents Label14 As Label
    Friend WithEvents CheckBoxPin2Inverted As CheckBox
    Private WithEvents TextBoxMaxSpeed As TextBox
    Private WithEvents Label9 As Label
    Friend WithEvents CheckBoxPin1Inverted As CheckBox
    Private WithEvents Label13 As Label
    Private WithEvents TextBoxGearRatio As TextBox
    Private WithEvents Label8 As Label
    Private WithEvents TextBoxPin5 As TextBox
    Private WithEvents TextBoxStepsRev As TextBox
    Private WithEvents Label11 As Label
    Private WithEvents TextBoxPin6 As TextBox
    Friend WithEvents ComboBoxMotorDriver As ComboBox
    Private WithEvents Label50 As Label
    Private WithEvents TextBoxPin3 As TextBox
    Private WithEvents Label18 As Label
    Private WithEvents TextBoxPin4 As TextBox
    Private WithEvents TextBoxPin2 As TextBox
    Private WithEvents TextBoxPin1 As TextBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents ButtonReadEEPROM As Button
    Friend WithEvents ButtonWriteEEPROM As Button
    Friend WithEvents CheckBoxEEPROMForceWrite As CheckBox
    Friend WithEvents TextBoxTCFactor As TextBox
    Private WithEvents Label15 As Label
    Private WithEvents Label10 As Label
    Friend WithEvents NumericUpDownTCSensor As NumericUpDown
    Friend WithEvents GroupBox10 As GroupBox
    Friend WithEvents GroupBox28 As GroupBox
    Friend WithEvents CheckBoxEndstopUpperInverted As CheckBox
    Friend WithEvents TextBoxEndstopUpperPin As TextBox
    Friend WithEvents CheckBoxEndstopUpperEnabled As CheckBox
    Friend WithEvents Label22 As Label
    Friend WithEvents GroupBox29 As GroupBox
    Friend WithEvents CheckBoxEndstopLowerInverted As CheckBox
    Friend WithEvents TextBoxEndstopLowerPin As TextBox
    Friend WithEvents CheckBoxEndStopLowerEnabled As CheckBox
    Friend WithEvents Label40 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents CheckBoxHPS As CheckBox
    Friend WithEvents CheckBoxHSI As CheckBox
    Friend WithEvents CheckBoxSAH As CheckBox
    Friend WithEvents CheckBoxHSA As CheckBox
    Private WithEvents TextBoxHomePosition As TextBox
    Private WithEvents Label17 As Label
    Private WithEvents Label21 As Label
    Private WithEvents TextBoxHSN As TextBox
    Friend WithEvents GroupBox22 As GroupBox
    Private WithEvents Label16 As Label
    Friend WithEvents CheckBoxMCLSA As CheckBox
    Private WithEvents TextBoxMCIn As TextBox
    Private WithEvents Label56 As Label
    Private WithEvents TextBoxMCOut As TextBox
    Private WithEvents Label57 As Label
    Private WithEvents TextBoxMCL As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents ButtonReadSwitchState As Button
    Friend WithEvents CheckBoxHSTriggered As CheckBox
    Friend WithEvents CheckBoxESLTriggered As CheckBox
    Friend WithEvents CheckBoxESUTriggered As CheckBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents TextBoxLimitUpper As TextBox
    Friend WithEvents CheckBoxLimitUpperEnabled As CheckBox
    Friend WithEvents Label19 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents TextBoxLimitLower As TextBox
    Friend WithEvents CheckBoxLimitLowerEnabled As CheckBox
    Friend WithEvents Label20 As Label
    Friend WithEvents CheckBoxMCA As CheckBox
    Friend WithEvents CheckBoxMCLSI As CheckBox
    Friend WithEvents CheckBoxMCOutI As CheckBox
    Friend WithEvents CheckBoxMCInI As CheckBox
    Friend WithEvents FlowLayoutPanelTemperatureSensors As FlowLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ButtonExportSettings As Button
    Friend WithEvents ButtonImportSettings As Button
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripSplitButton1 As ToolStripDropDownButton
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
End Class
