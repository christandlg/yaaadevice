Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports ASCOM.Utilities
Imports ASCOM.YAAADevice

<ComVisible(False)>
Public Class SetupDialogForm

    Private focuser_ As Focuser

    Private user_controls_() As UserControlTemperatureSensor


    Public Sub New(t As Focuser)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        focuser_ = t

        updateStatusStrip()
    End Sub

    Private Sub updateStatusStrip()
        Dim current_settings As Focuser.focuserDriverSettings = focuser_.getDriverSettings()
        Dim destination As String = ""

        If focuser_.Connected Then
            Dim message As String = "Connected to: "
            If current_settings.connection_type_ = Focuser.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Disconnect")
        Else
            ToolStripSplitButton1.Text = "Disconnected"
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Connect")
        End If

        ToolStripStatusLabel2.Text = ""
        ToolStripStatusLabel2.Spring = True

        ToolStripStatusLabel1.Alignment = ToolStripItemAlignment.Right
        ToolStripStatusLabel1.Text = "Ready"
    End Sub

    Private Sub connect(value As Boolean)
        Dim current_settings As Focuser.focuserDriverSettings = focuser_.getDriverSettings()

        current_settings.com_port_ = ComboBoxComPort.SelectedItem.ToString
        current_settings.baud_rate_ = ComboBoxBaudRate.SelectedItem.ToString

        current_settings.ip_address_ = TextBoxIPAddress.Text
        current_settings.port_ = CInt(TextBoxPort.Text)

        If RadioButtonSerial.Checked Then
            current_settings.connection_type_ = Focuser.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            current_settings.connection_type_ = Focuser.connection_type_t.NETWORK
        End If

        focuser_.setDriverSettings(current_settings)

        Dim destination As String = ""

        If value Then
            Dim message As String = "Attempting to connect to: "
            If current_settings.connection_type_ = Focuser.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            Application.DoEvents()

            'connect to the arduino and read values from the EEPROM.
            focuser_.Connected = True

        Else
            ToolStripSplitButton1.Text = "Disconnecting..."
            Application.DoEvents()

            focuser_.Connected = False
        End If

        If focuser_.Connected Then
            ToolStripSplitButton1.Text = "Connected to: " & destination
        Else
            ToolStripSplitButton1.Text = "Disconnected"
        End If

        Application.DoEvents()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click ' OK button event handler
        Focuser.focuserDriverSettings.trace_state_ = chkTrace.Checked

        ' Persist new values of user settings to the ASCOM profile
        If Not writeProfileSettings() Then
            Exit Sub
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click 'Cancel button event handler
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ShowAscomWebPage(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.DoubleClick, PictureBox1.Click
        ' Click on ASCOM logo event handler
        Try
            System.Diagnostics.Process.Start("http://ascom-standards.org/")
        Catch noBrowser As System.ComponentModel.Win32Exception
            If noBrowser.ErrorCode = -2147467259 Then
                MessageBox.Show(noBrowser.Message)
            End If
        Catch other As System.Exception
            MessageBox.Show(other.Message)
        End Try
    End Sub

    Private Sub SetupDialogForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load ' Form load event handler
        ' Retrieve current values of user settings from the ASCOM Profile
        InitUI()
    End Sub

    Private Sub InitUI()
        chkTrace.Checked = Focuser.focuserDriverSettings.trace_state_
        ' set the list of com ports to those that are currently available
        ComboBoxComPort.Items.Clear()
        ComboBoxComPort.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames())       ' use System.IO because it's static

        'initialize comboboxes
        ComboBoxMotorDriver.Items.AddRange([Enum].GetNames(GetType(Focuser.motor_driver_t)))
        ComboBoxStepType.Items.AddRange([Enum].GetNames(GetType(Focuser.step_type_t)))

        ' Retrieve current values of user settings from the ASCOM Profil
        ReadProfileSettings()
    End Sub

    Private Sub AddSensorControls(Optional useEEPROM As Boolean = False)
        'clear existing user controls from FlowLayoutPanel
        If Not user_controls_ Is Nothing Then
            For Each user_control In user_controls_
                user_control.Dispose()
            Next
        End If

        'set flowLayoutPanel properties
        With FlowLayoutPanelTemperatureSensors
            '.Location = New Drawing.Point(6, 74)
            '.Size = New Drawing.Size(511, 219)
            .FlowDirection = FlowDirection.TopDown
        End With

        'use settings read from EEPROM if requested
        Dim current_settings As Focuser.focuserDriverSettings = focuser_.getDriverSettings(useEEPROM)

        'redim array to hold Focuser.numTempSensors user control objects.
        ReDim user_controls_(Focuser.numTempSensors - 1)

        'add entries for each temperature sensor
        For i As Integer = 0 To user_controls_.Length - 1
            'add control for temperature sensor.
            user_controls_(i) = New UserControlTemperatureSensor(current_settings.focuser_settings_.temperature_sensors_(i), i)

            'add control to FlowLayoutPanel
            FlowLayoutPanelTemperatureSensors.Controls.Add(user_controls_(i))
        Next
    End Sub

    Private Sub ReadProfileSettings()
        ToolStripStatusLabel1.Text = "Reading Profile Settings"
        Application.DoEvents()

        setMaskToSettings(focuser_.getDriverSettings())

        updateStatusStrip()
    End Sub

    Private Function writeProfileSettings() As Boolean
        Dim success As Boolean = True

        ToolStripStatusLabel1.Text = "Writing Profile Settings"
        Application.DoEvents()

        Try
            focuser_.setDriverSettings(getSettingsFromMask())

            focuser_.WriteProfile()
        Catch ex As Exception
            'when an error occurred during reading the settings from the input fields, display an
            'error message and do not close the window.
            MsgBox("Invalid values detected, some settings have not been saved. Check all input fields with orange background and try again.", MsgBoxStyle.Critical)
            success = False
        End Try

        updateStatusStrip()

        Return success
    End Function

    Private Sub ButtonReadEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonReadEEPROM.Click

        Dim current_settings As Focuser.focuserDriverSettings = focuser_.getDriverSettings()

        Try
            connect(True)

            If focuser_.Connected Then
                ToolStripStatusLabel1.Text = "Reading from EEPROM..."
                Application.DoEvents()

                focuser_.ReadEEPROM()

                setMaskToSettings(focuser_.getDriverSettings(True))
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'catch any other exceptions just in case
            Dim err_msg As String = ""
            err_msg &= "Error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub

    Private Sub ButtonWriteEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonWriteEEPROM.Click
        'save values in ASCOM profile.
        If Not writeProfileSettings() Then
            Exit Sub
        End If

        focuser_.EEPROM_force_write_ = CheckBoxEEPROMForceWrite.Checked

        Try
            connect(True)

            'if the YAAADevice is connected, try to write the EEPROM contents
            If focuser_.Connected Then
                ToolStripStatusLabel1.Text = "Writing to EEPROM..."
                Application.DoEvents()

                focuser_.WriteEEPROM()
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'other exceptions are returned by function ReadEEPROM()
            Dim err_msg As String = ""
            err_msg &= "Unknown error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        focuser_.EEPROM_force_write_ = False
        CheckBoxEEPROMForceWrite.Checked = False

        updateStatusStrip()
    End Sub

    Private Sub ButtonReadSwitchState_Click(sender As Object, e As EventArgs) Handles ButtonReadSwitchState.Click
        Dim switch_state As Short

        Try
            connect(True)

            ToolStripStatusLabel1.Text = "Reading Switch State"
            Application.DoEvents()

            switch_state = CShort(focuser_.Action("Get_Switch_State", ""))

            CheckBoxESLTriggered.Checked = ((switch_state And 1) <> 0)
            CheckBoxESUTriggered.Checked = ((switch_state And 2) <> 0)
            CheckBoxHSTriggered.Checked = ((switch_state And 4) <> 0)

            'focuser_.Connected = False
        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

        updateStatusStrip()
    End Sub

    Private Function getTextBoxValue(Of T As IConvertible)(text_box As TextBox, ByRef value As T) As Boolean
        'reset text box background color
        text_box.BackColor = TextBox.DefaultBackColor

        Try
            'parse text_box.Text to T
            value = DirectCast(Convert.ChangeType(text_box.Text, GetType(T)), T)
            Return True
        Catch ex As Exception
            'change text box color to indicate incorrect value
            text_box.BackColor = Drawing.Color.Orange
            Return False
        End Try
    End Function

    Private Sub ComboBoxMotorDriver_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxMotorDriver.SelectedIndexChanged
        Select Case ComboBoxMotorDriver.SelectedItem.ToString
            Case Focuser.motor_driver_t.ADAFRUIT_MOTORSHIELD_V2.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddress.Enabled = True
                ComboBoxTerminal.Enabled = True

                'disable other options.
                TextBoxPin1.Enabled = False
                CheckBoxPin1Inverted.Enabled = False
                TextBoxPin2.Enabled = False
                CheckBoxPin2Inverted.Enabled = False
                TextBoxPin3.Enabled = False
                CheckBoxPin3Inverted.Enabled = False
                TextBoxPin4.Enabled = False
                CheckBoxPin4Inverted.Enabled = False
                TextBoxPin5.Enabled = False
                CheckBoxPin5Inverted.Enabled = False
                TextBoxPin6.Enabled = False
                CheckBoxPin6Inverted.Enabled = False

            Case Focuser.motor_driver_t.FOUR_WIRE_STEPPER.ToString
                'reenable four wire stepper options.
                TextBoxPin1.Enabled = True
                CheckBoxPin1Inverted.Enabled = True
                TextBoxPin2.Enabled = True
                CheckBoxPin2Inverted.Enabled = True
                TextBoxPin3.Enabled = True
                CheckBoxPin3Inverted.Enabled = True
                TextBoxPin4.Enabled = True
                CheckBoxPin4Inverted.Enabled = True

                'disable other options.
                ComboBoxI2CAddress.Enabled = False
                ComboBoxTerminal.Enabled = False

                TextBoxPin5.Enabled = False
                CheckBoxPin5Inverted.Enabled = False
                TextBoxPin6.Enabled = False
                CheckBoxPin6Inverted.Enabled = False

            Case Focuser.motor_driver_t.DRIVER_A4988.ToString, Focuser.motor_driver_t.DRIVER_DRV8825.ToString
                'disable adafruit motor shield options.
                ComboBoxI2CAddress.Enabled = False
                ComboBoxTerminal.Enabled = False

                'enable other options.
                TextBoxPin1.Enabled = True
                CheckBoxPin1Inverted.Enabled = True
                TextBoxPin2.Enabled = True
                CheckBoxPin2Inverted.Enabled = True
                TextBoxPin3.Enabled = True
                CheckBoxPin3Inverted.Enabled = True
                TextBoxPin4.Enabled = True
                CheckBoxPin4Inverted.Enabled = True
                TextBoxPin5.Enabled = True
                CheckBoxPin5Inverted.Enabled = True
                TextBoxPin6.Enabled = True
                CheckBoxPin6Inverted.Enabled = True
        End Select
    End Sub

    Private Sub setMaskToSettings(settings As Focuser.focuserDriverSettings)
        chkTrace.Checked = Focuser.focuserDriverSettings.trace_state_

        With settings
            Select Case .connection_type_
                Case Focuser.connection_type_t.SERIAL
                    RadioButtonSerial.Checked = True
                Case Focuser.connection_type_t.NETWORK
                    RadioButtonNetwork.Checked = True
            End Select

            'add focuser and baud rate items if they are not in the lists
            If Not ComboBoxComPort.Items.Contains(.com_port_) Then
                ComboBoxComPort.Items.Add(.com_port_)
            End If
            If Not ComboBoxBaudRate.Items.Contains(.baud_rate_) Then
                ComboBoxBaudRate.Items.Add(.baud_rate_)
            End If

            ComboBoxComPort.SelectedItem = .com_port_.ToString
            ComboBoxBaudRate.SelectedItem = .baud_rate_.ToString

            TextBoxIPAddress.Text = .ip_address_
            TextBoxPort.Text = .port_.ToString
        End With

        With settings.focuser_settings_
            CheckBoxAbsolute.Checked = .position_absolute_
            TextBoxMaxIncrement.Text = .max_increment_
            TextBoxMaxStep.Text = .max_step_
            TextBoxMicronsPerRev.Text = .microns_per_rev_

            NumericUpDownTCSensor.Value = .tc_sensor_
            TextBoxTCFactor.Text = .tc_factor_
            CheckBoxTempComp.Checked = .tc_available_

            TextBoxMaxSpeed.Text = .max_speed_
            TextBoxStepsRev.Text = .number_of_steps_
            TextBoxGearRatio.Text = .gear_ratio_

            ComboBoxStepType.SelectedItem = .step_type_.ToString

            'home switch settings
            CheckBoxHSA.Checked = .home_switch_enabled_
            TextBoxHSN.Text = YAAASharedClassLibrary.printPin(.home_switch_pin_)
            CheckBoxHSI.Checked = .home_switch_inverted_
            TextBoxHomePosition.Text = .home_position_
            CheckBoxHPS.Checked = .home_position_sync_

            CheckBoxSAH.Checked = .startup_auto_home_
            CheckBoxASR.Checked = .automatic_stepper_release_

            'endstop settings
            CheckBoxEndStopLowerEnabled.Checked = .endstop_lower_enabled_
            CheckBoxEndstopUpperEnabled.Checked = .endstop_upper_enabled_
            TextBoxEndstopLowerPin.Text = YAAASharedClassLibrary.printPin(.endstop_lower_pin_)
            TextBoxEndstopUpperPin.Text = YAAASharedClassLibrary.printPin(.endstop_upper_pin_)
            CheckBoxEndstopLowerInverted.Checked = .endstop_lower_inverted_
            CheckBoxEndstopUpperInverted.Checked = .endstop_upper_inverted_

            CheckBoxLimitLowerEnabled.Checked = .limit_lower_enabled_
            CheckBoxLimitUpperEnabled.Checked = .limit_upper_enabled_
            TextBoxLimitLower.Text = .limit_lower_
            TextBoxLimitUpper.Text = .limit_upper_

            ComboBoxMotorDriver.SelectedItem = .motor_driver_type_.ToString
            ComboBoxI2CAddress.SelectedItem = "0x" & Hex(.I2C_address_)
            ComboBoxTerminal.SelectedItem = .terminal_.ToString

            TextBoxPin1.Text = YAAASharedClassLibrary.printPin(.pin_1_)
            TextBoxPin2.Text = YAAASharedClassLibrary.printPin(.pin_2_)
            TextBoxPin3.Text = YAAASharedClassLibrary.printPin(.pin_3_)
            TextBoxPin4.Text = YAAASharedClassLibrary.printPin(.pin_4_)
            TextBoxPin5.Text = YAAASharedClassLibrary.printPin(.pin_5_)
            TextBoxPin6.Text = YAAASharedClassLibrary.printPin(.pin_6_)

            CheckBoxPin1Inverted.Checked = .pin_1_inverted_
            CheckBoxPin2Inverted.Checked = .pin_2_inverted_
            CheckBoxPin3Inverted.Checked = .pin_3_inverted_
            CheckBoxPin4Inverted.Checked = .pin_4_inverted_
            CheckBoxPin5Inverted.Checked = .pin_5_inverted_
            CheckBoxPin6Inverted.Checked = .pin_6_inverted_

            CheckBoxMCA.Checked = .mc_available_
            CheckBoxMCLSA.Checked = .mc_lock_available_
            TextBoxMCL.Text = YAAASharedClassLibrary.printPin(.mc_lock_pin_)
            TextBoxMCIn.Text = YAAASharedClassLibrary.printPin(.mc_in_pin_)
            TextBoxMCOut.Text = YAAASharedClassLibrary.printPin(.mc_out_pin_)
            CheckBoxMCLSI.Checked = .mc_lock_inverted_
            CheckBoxMCInI.Checked = .mc_in_inverted_
            CheckBoxMCOutI.Checked = .mc_out_inverted_
        End With

        'add temperature sensor controls
        AddSensorControls()
    End Sub

    Private Function getSettingsFromMask() As Focuser.focuserDriverSettings
        Dim valid As Boolean = True
        Dim return_value As Focuser.focuserDriverSettings = New Focuser.focuserDriverSettings(True)

        Focuser.focuserDriverSettings.trace_state_ = chkTrace.Checked

        'If Not ComboBoxComPort.SelectedItem Is Nothing Then
        return_value.com_port_ = ComboBoxComPort.SelectedItem.ToString
        'End If
        return_value.baud_rate_ = CInt(ComboBoxBaudRate.SelectedItem.ToString)

        valid = valid And getTextBoxValue(TextBoxIPAddress, return_value.ip_address_)
        valid = valid And getTextBoxValue(TextBoxPort, return_value.port_)

        If RadioButtonSerial.Checked Then
            return_value.connection_type_ = Focuser.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            return_value.connection_type_ = Focuser.connection_type_t.NETWORK
        End If

        With return_value.focuser_settings_
            .position_absolute_ = CheckBoxAbsolute.Checked
            valid = valid And getTextBoxValue(TextBoxMicronsPerRev, .microns_per_rev_)
            valid = valid And getTextBoxValue(TextBoxMaxIncrement, .max_increment_)
            valid = valid And getTextBoxValue(TextBoxMaxStep, .max_step_)

            'temperature sensor settings.
            .tc_available_ = CheckBoxTempComp.Checked
            .tc_sensor_ = CByte(NumericUpDownTCSensor.Value)
            valid = valid And getTextBoxValue(TextBoxTCFactor, .tc_factor_)

            valid = valid And getTextBoxValue(TextBoxMaxSpeed, .max_speed_)
            valid = valid And getTextBoxValue(TextBoxGearRatio, .gear_ratio_)
            valid = valid And getTextBoxValue(TextBoxStepsRev, .number_of_steps_)

            .step_type_ = [Enum].Parse(GetType(Focuser.step_type_t), ComboBoxStepType.SelectedItem.ToString)

            .automatic_stepper_release_ = CheckBoxASR.Checked
            .startup_auto_home_ = CheckBoxASR.Checked

            'endstop settings
            .endstop_lower_enabled_ = CheckBoxEndStopLowerEnabled.Checked
            .endstop_lower_inverted_ = CheckBoxEndstopLowerInverted.Checked
            TextBoxEndstopLowerPin.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopLowerPin.Text)
            valid = valid And getTextBoxValue(TextBoxEndstopLowerPin, .endstop_lower_pin_)

            .endstop_upper_enabled_ = CheckBoxEndstopUpperEnabled.Checked
            .endstop_upper_enabled_ = CheckBoxEndstopUpperInverted.Checked
            TextBoxEndstopUpperPin.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopUpperPin.Text)
            valid = valid And getTextBoxValue(TextBoxEndstopUpperPin, .endstop_upper_pin_)

            'movment limit settigns
            .limit_lower_enabled_ = CheckBoxLimitLowerEnabled.Checked
            .limit_upper_enabled_ = CheckBoxLimitUpperEnabled.Checked
            valid = valid And getTextBoxValue(TextBoxLimitLower, .limit_lower_)
            valid = valid And getTextBoxValue(TextBoxLimitUpper, .limit_upper_)

            'motor driver settings
            .motor_driver_type_ = [Enum].Parse(GetType(Focuser.motor_driver_t), ComboBoxMotorDriver.SelectedItem.ToString)
            .I2C_address_ = CByte(Convert.ToInt32(ComboBoxI2CAddress.SelectedItem.ToString, 16))
            .terminal_ = CByte(ComboBoxTerminal.SelectedItem.ToString)

            TextBoxPin1.Text = YAAASharedClassLibrary.parsePin(TextBoxPin1.Text)
            TextBoxPin2.Text = YAAASharedClassLibrary.parsePin(TextBoxPin2.Text)
            TextBoxPin3.Text = YAAASharedClassLibrary.parsePin(TextBoxPin3.Text)
            TextBoxPin4.Text = YAAASharedClassLibrary.parsePin(TextBoxPin4.Text)
            TextBoxPin5.Text = YAAASharedClassLibrary.parsePin(TextBoxPin5.Text)
            TextBoxPin6.Text = YAAASharedClassLibrary.parsePin(TextBoxPin6.Text)
            valid = valid And getTextBoxValue(TextBoxPin1, .pin_1_)
            valid = valid And getTextBoxValue(TextBoxPin2, .pin_2_)
            valid = valid And getTextBoxValue(TextBoxPin3, .pin_3_)
            valid = valid And getTextBoxValue(TextBoxPin4, .pin_4_)
            valid = valid And getTextBoxValue(TextBoxPin5, .pin_5_)
            valid = valid And getTextBoxValue(TextBoxPin6, .pin_6_)

            .pin_1_inverted_ = CheckBoxPin1Inverted.Checked
            .pin_2_inverted_ = CheckBoxPin2Inverted.Checked
            .pin_3_inverted_ = CheckBoxPin3Inverted.Checked
            .pin_4_inverted_ = CheckBoxPin4Inverted.Checked
            .pin_5_inverted_ = CheckBoxPin5Inverted.Checked
            .pin_6_inverted_ = CheckBoxPin6Inverted.Checked

            'home switch and homing settings
            .home_switch_enabled_ = CheckBoxHSA.Checked
            .home_switch_inverted_ = CheckBoxHSI.Checked
            TextBoxHSN.Text = YAAASharedClassLibrary.parsePin(TextBoxHSN.Text)
            valid = valid And getTextBoxValue(TextBoxHSN, .home_switch_pin_)
            valid = valid And getTextBoxValue(TextBoxHomePosition, .home_position_)
            .home_position_sync_ = CheckBoxHPS.Checked
            .startup_auto_home_ = CheckBoxSAH.Checked

            'todo: manual control settings
            .mc_available_ = CheckBoxMCA.Checked
            .mc_lock_available_ = CheckBoxMCLSA.Checked
            TextBoxMCL.Text = YAAASharedClassLibrary.parsePin(TextBoxMCL.Text)
            TextBoxMCIn.Text = YAAASharedClassLibrary.parsePin(TextBoxMCIn.Text)
            TextBoxMCOut.Text = YAAASharedClassLibrary.parsePin(TextBoxMCOut.Text)
            valid = valid And getTextBoxValue(TextBoxMCL, .mc_lock_pin_)
            valid = valid And getTextBoxValue(TextBoxMCIn, .mc_in_pin_)
            valid = valid And getTextBoxValue(TextBoxMCOut, .mc_out_pin_)
            .mc_lock_inverted_ = CheckBoxMCLSI.Checked
            .mc_in_inverted_ = CheckBoxMCInI.Checked
            .mc_out_inverted_ = CheckBoxMCOutI.Checked
        End With

        'update temperature sensor settings
        For i As Integer = 0 To return_value.focuser_settings_.temperature_sensors_.Length - 1
            valid = valid And user_controls_(i).getSensor(return_value.focuser_settings_.temperature_sensors_(i))
        Next

        If Not valid Then
            Throw New ASCOM.InvalidValueException()
        End If

        Return return_value
    End Function

    Private Sub ButtonExportSettings_Click(sender As Object, e As EventArgs) Handles ButtonExportSettings.Click
        Dim SaveFileDialog1 As New SaveFileDialog
        SaveFileDialog1.AddExtension = True
        SaveFileDialog1.OverwritePrompt = True
        SaveFileDialog1.DefaultExt = "xml"
        SaveFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        SaveFileDialog1.InitialDirectory = Focuser.settingsStoreDirectory
        SaveFileDialog1.FileName = YAAASharedClassLibrary.YAAASETTINGS_FILE_DEFAULT

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            If SaveFileDialog1.FileName <> "" Then
                Try
                    focuser_.xmlFromDriverSettings(SaveFileDialog1.FileName, getSettingsFromMask())
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub ButtonImportSettings_Click(sender As Object, e As EventArgs) Handles ButtonImportSettings.Click
        Dim OpenFileDialog1 = New OpenFileDialog
        OpenFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        OpenFileDialog1.InitialDirectory = Focuser.settingsStoreDirectory
        OpenFileDialog1.RestoreDirectory = True

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            If OpenFileDialog1.FileName <> "" Then
                Dim settings As Focuser.focuserDriverSettings = New Focuser.focuserDriverSettings(True)

                Try
                    settings = focuser_.xmlToDriverSettings(OpenFileDialog1.FileName)
                    setMaskToSettings(settings)
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub SetupDialogForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        connect(False)
    End Sub

    Private Sub ToolStripSplitButton1_DropDownItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ToolStripSplitButton1.DropDownItemClicked
        Try
            connect(Not focuser_.Connected)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub
End Class
