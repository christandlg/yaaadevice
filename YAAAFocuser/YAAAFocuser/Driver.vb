'tabs=4
' --------------------------------------------------------------------------------
' TODO fill in this information for your driver, then remove this line!
'
' ASCOM Focuser driver for YAAADevice
'
' Description:	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam 
'				nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam 
'				erat, sed diam voluptua. At vero eos et accusam et justo duo 
'				dolores et ea rebum. Stet clita kasd gubergren, no sea takimata 
'				sanctus est Lorem ipsum dolor sit amet.
'
' Implements:	ASCOM Focuser interface version: 1.0
' Author:		(XXX) Your N. Here <your@email.here>
'
' Edit Log:
'
' Date			Who	Vers	Description
' -----------	---	-----	-------------------------------------------------------
' dd-mmm-yyyy	XXX	1.0.0	Initial edit, from Focuser template
' ---------------------------------------------------------------------------------
'
'
' Your driver's ID is ASCOM.YAAADevice.Focuser
'
' The Guid attribute sets the CLSID for ASCOM.DeviceName.Focuser
' The ClassInterface/None addribute prevents an empty interface called
' _Focuser from being created and used as the [default] interface
'

' This definition is used to select code that's only applicable for one device type
#Const Device = "Focuser"

Imports ASCOM
Imports ASCOM.Astrometry
Imports ASCOM.Astrometry.AstroUtils
Imports ASCOM.DeviceInterface
Imports ASCOM.Utilities

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports System.Text
Imports System.Xml




<Guid("ecda3620-1041-4af5-956f-445a19fe778a")>
<ClassInterface(ClassInterfaceType.None)>
Public Class Focuser

    ' The Guid attribute sets the CLSID for ASCOM.YAAADevice.Focuser
    ' The ClassInterface/None addribute prevents an empty interface called
    ' _YAAADevice from being created and used as the [default] interface

    Implements IFocuserV2

    '
    ' Driver ID and descriptive string that shows in the Chooser
    '
    Friend Shared driverID As String = "ASCOM.YAAADevice.Focuser"
    Private Shared driverDescription As String = "YAAADevice Focuser"
    'Private Shared driverVersion As String = "0.0.1"

    Enum connection_type_t
        SERIAL = 0
        NETWORK = 1
    End Enum

    Enum serial_error_code_t
        ERROR_NONE = 0      'not necessary?
        ERROR_UNKNOWN_DEVICE = 1
        ERROR_UNKNOWN_CMD = 2
        ERROR_UNKNOWN_CMD_TYPE = 4
        ERROR_INVALID_CMD_TYPE = 8
        ERROR_INVALID_PARAMETER = 16
        ERROR_MISSING_PARAMETER = 32
        ERROR_UNKNOWN_OPTION = 64
        ERROR_DEVICE_UNAVAILABLE = 128
        ERROR_DEVICE_LOCKED = 256
    End Enum

    Enum motor_driver_t
        ADAFRUIT_MOTORSHIELD_V2 = 0
        FOUR_WIRE_STEPPER = 1
        DRIVER_A4988 = 2
        DRIVER_DRV8825 = 3
    End Enum

    Enum step_type_t
        STEPTYPE_QUICKSTEP = 0              'for Adafruit Motor Shield V2.
        STEPTYPE_SINGLE = 1                 'for Adafruit Motor Shield V2.
        'STEPTYPE_DOUBLE                    'for Adafruit Motor Shield V2.
        STEPTYPE_FULL = 3                   'for stepper drivers and Adafruit Motor Shield V2 (DOUBLE).
        STEPTYPE_MICROSTEP_2 = 4            'for stepper drivers and Adafruit Motor Shield V2.
        STEPTYPE_MICROSTEP_4 = 5            'for stepper drivers.
        STEPTYPE_MICROSTEP_8 = 6            'for stepper drivers.
        STEPTYPE_MICROSTEP_16 = 7           'for stepper drivers and Adafruit Motor Shield V2.
        STEPTYPE_MICROSTEP_32 = 8           'for stepper drivers.
        STEPTYPE_MICROSTEP_64 = 9           'for stepper drivers.
        STEPTYPE_MICROSTEP_128 = 10         'for stepper drivers.
    End Enum

    Friend Const numTempSensors As Integer = 3

#Region "Constants used for Profile persistence"

    Friend Shared comPortProfileName As String = "COM Port"
    Friend Shared traceStateProfileName As String = "Trace Level"
    Public Shared baudRateProfileName As String = "Baud Rate"

    Public Shared connectionTypeProfileName As String = "Connection Type"
    Public Shared ipAddressProfileName As String = "IP Address"
    Public Shared portProfileName As String = "Port"

    'Private Structure focuserProfileNames
    Public Shared positionAbsoluteProfileName As String = "Absolute positioning"
    Public Shared MPRProfileName As String = "Microns per Revolution"
    Public Shared maxIncrementProfileName As String = "Max Increment"
    Public Shared maxStepProfileName As String = "Max Step"

    Public Shared TCAProfileName As String = "Temperature Compensation available"
    Public Shared TCSProfileName As String = "Temperature Compensation Sensor"
    Public Shared TCFProfileName As String = "Temperature Compensation Factor"

    Public Shared TSTProfileName As String = "Type"
    Public Shared TSDProfileName As String = "Device"
    Public Shared TSUProfileName As String = "Unit"
    Public Shared TSPProfileName As String = "Parameters"

    Public Shared TSSubKeyProfileName As String = "Temperature Sensor"

    Public Shared maxSpeedProfileName As String = "Max Speed"
    Public Shared gearRatioProfileName As String = "Gear Ratio"
    Public Shared NOSProfileName As String = "Number of Steps"

    Public Shared stepTypeProfileName As String = "Step Type"

    Public Shared HSEProfileName As String = "Home Switch available"
    Public Shared HSPProfileName As String = "Home Switch Pin"
    Public Shared HSIProfileName As String = "Home Switch inverted"
    Public Shared HPProfileName As String = "Home Position"

    Public Shared ASRProfileName As String = "Automatic Stepper Release"
    Public Shared SAHProfileName As String = "Startup Auto Home"

    Public Shared ELEProfileName As String = "Lower Endstop enabled"
    Public Shared EUEProfileName As String = "Upper Endstop Enabled"
    Public Shared ELPProfileName As String = "Lower Endstop Pin"
    Public Shared EUPProfileName As String = "Upper Endstop Pin"
    Public Shared ELIProfileName As String = "Lower Endstop inverted"
    Public Shared EUIProfileName As String = "Upper Endstop inverted"

    Public Shared LLEProfileName As String = "Lower Limit Enabled"
    Public Shared LUEProfileName As String = "Upper Limit Enabled"
    Public Shared LLProfileName As String = "Lower Limit"
    Public Shared LUProfileName As String = "Upper Limit"

    Public Shared MDTProfileName As String = "Motor Driver Type"
    Public Shared I2CProfileName As String = "I2C Address"
    Public Shared TerminalProfileName As String = "Terminal"

    Public Shared pin1ProfileName As String = "Pin 1"
    Public Shared pin2ProfileName As String = "Pin 2"
    Public Shared pin3ProfileName As String = "Pin 3"
    Public Shared pin4ProfileName As String = "Pin 4"
    Public Shared pin5ProfileName As String = "Pin 5"
    Public Shared pin6ProfileName As String = "Pin 6"

    Public Shared pin1InvertedProfileName As String = "Pin 1 inverted"
    Public Shared pin2InvertedProfileName As String = "Pin 2 inverted"
    Public Shared pin3InvertedProfileName As String = "Pin 3 inverted"
    Public Shared pin4InvertedProfileName As String = "Pin 4 inverted"
    Public Shared pin5InvertedProfileName As String = "Pin 5 inverted"
    Public Shared pin6InvertedProfileName As String = "Pin 6 inverted"
    'End Structure

    Public Shared MCAProfileName As String = "Manual Control Available"
    Public Shared MCLAProfileName As String = "Manual Control Lock Switch Available"
    Public Shared MCLPProfileName As String = "Manual Control Lock Switch Pin"
    Public Shared MCIPProfileName As String = "Manual Control Inward Pin"
    Public Shared MCOPProfileName As String = "Manual Control Outward Pin"
    Public Shared MCIIProfileName As String = "Manual Control Inward Inverted"
    Public Shared MCOIProfileName As String = "Manual Control Outward Inverted"
    Public Shared MCLIProfileName As String = "Manual Control Lock Inverted"

    Public Shared settingsStoreDirectory = YAAASharedClassLibrary.YAAASETTINGS_DIRECTORY & "/YAAAFocuser"
    'System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

    Private SET_CONNECTED_DELAY As Integer = 5000

    Private TIMEOUT_SERIAL As Integer = 5000
    Private TIMEOUT_TCP As Integer = 5000
#End Region

#Region "Default values"

    Private Structure focuserDriverDefaultValues
        Public Shared com_port_default_ As String = "COM6"
        Public Shared baud_rate_default_ As Integer = 9600
        Public Shared trace_state_default_ As Boolean = True

        Public Shared connection_type_default_ As connection_type_t = connection_type_t.SERIAL
        Public Shared ip_address_default_ As String = "192.168.0.202"
        Public Shared port_default_ As Integer = 23
    End Structure

    Private Structure focuserDefaultValues
        Public Shared position_absolute_default_ As Boolean = True
        Public Shared MPR_default_ As Single = 25000.0
        Public Shared max_increment_default_ As Integer = 32767
        Public Shared max_step_default_ As Integer = 32767

        Public Shared TCA_default_ As Boolean = False
        Public Shared TCS_default_ As Byte = 0
        Public Shared TCF_default_ As Single = 25.0

        Public Shared max_speed_default_ As Single = 100.0
        Public Shared gear_ratio_default_ As Single = 2.0
        Public Shared number_of_steps_default_ As UShort = 200

        Public Shared step_type_default_ As step_type_t = step_type_t.STEPTYPE_FULL

        Public Shared HSE_default_ As Boolean = False
        Public Shared HSP_default_ As Byte = 255
        Public Shared HSI_default_ As Boolean = False
        Public Shared HP_default_ As Integer = 0
        Public Shared HPS_default_ As Boolean = False

        Public Shared ASR_default_ As Boolean = False

        Public Shared SAH_default_ As Boolean = False

        Public Shared ELE_default_ As Boolean = False
        Public Shared EUE_default_ As Boolean = False
        Public Shared ELP_default_ As Byte = 255
        Public Shared EUP_default_ As Byte = 255
        Public Shared ELI_default_ As Boolean = False
        Public Shared EUI_default_ As Boolean = False

        Public Shared LLE_default_ As Boolean = True
        Public Shared LUE_default_ As Boolean = True
        Public Shared LL_default_ As UInteger = 0
        Public Shared LU_default_ As UInteger = 32767

        Public Shared MDT_default_ As motor_driver_t = motor_driver_t.FOUR_WIRE_STEPPER
        Public Shared I2C_default_ As Byte = 98
        Public Shared terminal_default_ As Byte = 1

        Public Shared pin_1_default_ As Byte = 255
        Public Shared pin_2_default_ As Byte = 255
        Public Shared pin_3_default_ As Byte = 255
        Public Shared pin_4_default_ As Byte = 255
        Public Shared pin_5_default_ As Byte = 255
        Public Shared pin_6_default_ As Byte = 255

        Public Shared pin_1_inverted_default_ As Boolean = False
        Public Shared pin_2_inverted_default_ As Boolean = False
        Public Shared pin_3_inverted_default_ As Boolean = False
        Public Shared pin_4_inverted_default_ As Boolean = False
        Public Shared pin_5_inverted_default_ As Boolean = False
        Public Shared pin_6_inverted_default_ As Boolean = False

        Public Shared mc_available_default_ As Boolean = False
        Public Shared mc_lock_available_default_ As Boolean = False
        Public Shared mc_lock_pin_default_ As Byte = 255
        Public Shared mc_in_pin_default_ As Byte = 255
        Public Shared mc_out_pin_default_ As Byte = 255
        Public Shared mc_in_inverted_default_ As Boolean = False
        Public Shared mc_out_inverted_default_ As Boolean = False
        Public Shared mc_lock_inverted_default_ As Boolean = False
    End Structure

#End Region

#Region "Variables to hold the currrent device configuration"

    <DataContract>
    Friend Structure focuserDriverSettings
        Implements ICloneable

        <DataMember()>
        Public Shared trace_state_ As Boolean = focuserDriverDefaultValues.trace_state_default_

        <DataMember()>
        Public com_port_ As String
        <DataMember()>
        Public baud_rate_ As Integer

        <DataMember()>
        Public connection_type_ As connection_type_t
        <DataMember()>
        Public ip_address_ As String
        <DataMember()>
        Public port_ As Integer

        <DataMember()>
        Public focuser_settings_ As focuserSettings

        Public Sub New(Optional initialize As Boolean = True)
            If Not initialize Then
                Exit Sub
            End If

            connection_type_ = focuserDriverDefaultValues.connection_type_default_

            com_port_ = focuserDriverDefaultValues.com_port_default_
            baud_rate_ = focuserDriverDefaultValues.baud_rate_default_

            ip_address_ = focuserDriverDefaultValues.ip_address_default_
            port_ = focuserDriverDefaultValues.port_default_

            focuser_settings_ = New focuserSettings(True)
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As focuserDriverSettings = MemberwiseClone()

            'todo any additional clone code

            Return return_value
        End Function

    End Structure

    <DataContract>
    Friend Structure focuserSettings
        Implements ICloneable

        <DataMember()>
        Public position_absolute_ As Boolean
        <DataMember()>
        Public microns_per_rev_ As Single       'microns per revolutions of the focuser handle
        <DataMember()>
        Public max_increment_ As Integer
        <DataMember()>
        Public max_step_ As Integer

        <DataMember()>
        Public tc_available_ As Boolean
        <DataMember()>
        Public tc_sensor_ As Byte
        <DataMember()>
        Public tc_factor_ As Single
        <DataMember()>
        Public temperature_sensors_() As YAAASharedClassLibrary.YAAASensor

        <DataMember()>
        Public max_speed_ As Single
        <DataMember()>
        Public gear_ratio_ As Single
        <DataMember()>
        Public number_of_steps_ As UShort

        <DataMember()>
        Public step_type_ As step_type_t

        <DataMember()>
        Public home_switch_enabled_ As Boolean
        <DataMember()>
        Public home_switch_pin_ As Byte
        <DataMember()>
        Public home_switch_inverted_ As Boolean
        <DataMember()>
        Public home_position_ As Integer
        <DataMember()>
        Public home_position_sync_ As Boolean

        <DataMember()>
        Public automatic_stepper_release_ As Boolean
        <DataMember()>
        Public startup_auto_home_ As Boolean

        <DataMember()>
        Public endstop_lower_enabled_ As Boolean
        <DataMember()>
        Public endstop_lower_pin_ As Byte
        <DataMember()>
        Public endstop_lower_inverted_ As Boolean

        <DataMember()>
        Public endstop_upper_enabled_ As Boolean
        <DataMember()>
        Public endstop_upper_pin_ As Byte
        <DataMember()>
        Public endstop_upper_inverted_ As Boolean

        <DataMember()>
        Public limit_lower_enabled_ As Boolean
        <DataMember()>
        Public limit_upper_enabled_ As Boolean
        <DataMember()>
        Public limit_lower_ As Integer
        <DataMember()>
        Public limit_upper_ As Integer

        <DataMember()>
        Public motor_driver_type_ As motor_driver_t
        <DataMember()>
        Public I2C_address_ As Byte
        <DataMember()>
        Public terminal_ As Byte

        <DataMember()>
        Public pin_1_ As Byte
        <DataMember()>
        Public pin_2_ As Byte
        <DataMember()>
        Public pin_3_ As Byte
        <DataMember()>
        Public pin_4_ As Byte
        <DataMember()>
        Public pin_5_ As Byte
        <DataMember()>
        Public pin_6_ As Byte

        <DataMember()>
        Public pin_1_inverted_ As Boolean
        <DataMember()>
        Public pin_2_inverted_ As Boolean
        <DataMember()>
        Public pin_3_inverted_ As Boolean
        <DataMember()>
        Public pin_4_inverted_ As Boolean
        <DataMember()>
        Public pin_5_inverted_ As Boolean
        <DataMember()>
        Public pin_6_inverted_ As Boolean

        <DataMember()>
        Public mc_available_ As Boolean
        <DataMember()>
        Public mc_lock_available_ As Boolean
        <DataMember()>
        Public mc_lock_pin_ As Byte
        <DataMember()>
        Public mc_in_pin_ As Byte
        <DataMember()>
        Public mc_out_pin_ As Byte
        <DataMember()>
        Public mc_in_inverted_ As Boolean
        <DataMember()>
        Public mc_out_inverted_ As Boolean
        <DataMember()>
        Public mc_lock_inverted_ As Boolean

        Public Sub New(Optional initialize As Boolean = True)
            If Not initialize Then
                Exit Sub
            End If

            position_absolute_ = focuserDefaultValues.position_absolute_default_
            microns_per_rev_ = focuserDefaultValues.MPR_default_
            max_increment_ = focuserDefaultValues.max_increment_default_
            max_step_ = focuserDefaultValues.max_step_default_

            tc_available_ = focuserDefaultValues.TCA_default_
            tc_sensor_ = focuserDefaultValues.TCS_default_
            tc_factor_ = focuserDefaultValues.TCF_default_
            temperature_sensors_ = {New YAAASharedClassLibrary.YAAASensor(True), New YAAASharedClassLibrary.YAAASensor(True), New YAAASharedClassLibrary.YAAASensor(True)}  'uses initializing constructor

            max_speed_ = focuserDefaultValues.max_speed_default_
            gear_ratio_ = focuserDefaultValues.gear_ratio_default_
            number_of_steps_ = focuserDefaultValues.number_of_steps_default_

            step_type_ = focuserDefaultValues.step_type_default_

            home_switch_enabled_ = focuserDefaultValues.HSE_default_
            home_switch_pin_ = focuserDefaultValues.HSP_default_
            home_switch_inverted_ = focuserDefaultValues.HSI_default_
            home_position_ = focuserDefaultValues.HP_default_
            home_position_sync_ = focuserDefaultValues.HPS_default_

            automatic_stepper_release_ = focuserDefaultValues.ASR_default_
            startup_auto_home_ = focuserDefaultValues.SAH_default_

            endstop_lower_enabled_ = focuserDefaultValues.ELE_default_
            endstop_lower_pin_ = focuserDefaultValues.ELP_default_
            endstop_lower_inverted_ = focuserDefaultValues.ELI_default_

            endstop_upper_enabled_ = focuserDefaultValues.EUE_default_
            endstop_upper_pin_ = focuserDefaultValues.EUP_default_
            endstop_upper_inverted_ = focuserDefaultValues.EUI_default_

            limit_lower_enabled_ = focuserDefaultValues.LLE_default_
            limit_upper_enabled_ = focuserDefaultValues.LUE_default_
            limit_lower_ = focuserDefaultValues.LL_default_
            limit_upper_ = focuserDefaultValues.LU_default_

            motor_driver_type_ = focuserDefaultValues.MDT_default_
            I2C_address_ = focuserDefaultValues.I2C_default_
            terminal_ = focuserDefaultValues.terminal_default_

            pin_1_ = focuserDefaultValues.pin_1_default_
            pin_2_ = focuserDefaultValues.pin_2_default_
            pin_3_ = focuserDefaultValues.pin_3_default_
            pin_4_ = focuserDefaultValues.pin_4_default_
            pin_5_ = focuserDefaultValues.pin_5_default_
            pin_6_ = focuserDefaultValues.pin_6_default_

            pin_1_inverted_ = focuserDefaultValues.pin_1_inverted_default_
            pin_2_inverted_ = focuserDefaultValues.pin_2_inverted_default_
            pin_3_inverted_ = focuserDefaultValues.pin_3_inverted_default_
            pin_4_inverted_ = focuserDefaultValues.pin_4_inverted_default_
            pin_5_inverted_ = focuserDefaultValues.pin_5_inverted_default_
            pin_6_inverted_ = focuserDefaultValues.pin_6_inverted_default_

            mc_available_ = focuserDefaultValues.mc_available_default_
            mc_lock_available_ = focuserDefaultValues.mc_lock_available_default_
            mc_lock_pin_ = focuserDefaultValues.mc_lock_pin_default_
            mc_in_pin_ = focuserDefaultValues.mc_in_pin_default_
            mc_out_pin_ = focuserDefaultValues.mc_out_pin_default_
            mc_in_inverted_ = focuserDefaultValues.mc_in_inverted_default_
            mc_out_inverted_ = focuserDefaultValues.mc_out_inverted_default_
            mc_lock_inverted_ = focuserDefaultValues.mc_lock_inverted_default_
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As focuserSettings = MemberwiseClone()

            'new array for sensors
            Dim sensors(temperature_sensors_.Length - 1) As YAAASharedClassLibrary.YAAASensor

            'copy sensors to new array
            Array.Copy(temperature_sensors_, sensors, temperature_sensors_.Length)

            'replace the reference created by MemberwiseClone() by a deep copy of the array
            return_value.temperature_sensors_ = sensors

            Return return_value
        End Function
    End Structure

    Friend EEPROM_force_write_ As Boolean = False

    Private focuser_driver_settings_ As focuserDriverSettings
    Private focuser_driver_settings_EEPROM_ As focuserDriverSettings

    Private utilities As Util ' Private variable to hold an ASCOM Utilities object
    Private astroUtilities As AstroUtils ' Private variable to hold an AstroUtils object to provide the Range method
    Private TL As TraceLogger ' Private variable to hold the trace logger object (creates a diagnostic log file with information that you specify)

#End Region

    Friend serial_connection_ As ASCOM.Utilities.Serial

    Friend tcp_connection_ As Net.Sockets.TcpClient

    '
    ' Constructor - Must be public for COM registration!
    '
    Public Sub New()
        TL = New TraceLogger("", "YAAAFocuser")

        TL.Enabled = False

        'create focuser driver settings object
        focuser_driver_settings_ = New focuserDriverSettings(True)
        focuser_driver_settings_EEPROM_ = focuser_driver_settings_.Clone()

        ReadProfile() ' Read device configuration from the ASCOM Profile store

        TL.Enabled = focuserDriverSettings.trace_state_

        TL.LogMessage("Focuser", "Starting initialisation")

        utilities = New Util() ' Initialise util object
        astroUtilities = New AstroUtils 'Initialise new astro utiliites object

        serial_connection_ = New ASCOM.Utilities.Serial

        settingsStoreDirectory = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), settingsStoreDirectory)

        'TODO: Implement your additional construction here

        TL.LogMessage("Focuser", "Completed initialisation")
    End Sub

    '
    ' PUBLIC COM INTERFACE IFocuserV2 IMPLEMENTATION
    '

#Region "Common properties and methods"
    ''' <summary>
    ''' Displays the Setup Dialog form.
    ''' If the user clicks the OK button to dismiss the form, then
    ''' the new settings are saved, otherwise the old values are reloaded.
    ''' THIS IS THE ONLY PLACE WHERE SHOWING USER INTERFACE IS ALLOWED!
    ''' </summary>
    Public Sub SetupDialog() Implements IFocuserV2.SetupDialog
        ' consider only showing the setup dialog if not connected
        ' or call a different dialog if connected
        If IsConnected Then
            System.Windows.Forms.MessageBox.Show("Already connected, just press OK")
        End If

        Using F As SetupDialogForm = New SetupDialogForm(Me)
            Dim result As System.Windows.Forms.DialogResult = F.ShowDialog()
            If result = DialogResult.OK Then
                WriteProfile() ' Persist device configuration values to the ASCOM Profile store
            End If
        End Using
    End Sub

    Public ReadOnly Property SupportedActions() As ArrayList Implements IFocuserV2.SupportedActions
        Get
            TL.LogMessage("SupportedActions Get", "Returning array list containing action names")

            Dim actions = New ArrayList()

            'add supported actions.
            actions.Add("Get_Switch_State")
            actions.Add("Measure_Temperature")
            actions.Add("Has_Temperature")
            actions.Add("Get_Temperature")

            Return actions
        End Get
    End Property

    Public Function Action(ByVal ActionName As String, ByVal ActionParameters As String) As String Implements IFocuserV2.Action
        Select Case (ActionName)
            Case "Get_Switch_State"
                Return getSwitchState(ActionParameters)
            Case "Measure_Temperature"
                Return measureTemperature(ActionParameters)
            Case "Has_Temperature"
                Return hasTemperature(ActionParameters)
            Case "Get_Temperature"
                Return getTemperature(ActionParameters)
        End Select

        Throw New ActionNotImplementedException("Action " & ActionName & " is not supported by this driver")
    End Function

    Public Sub CommandBlind(ByVal Command As String, Optional ByVal Raw As Boolean = False) Implements IFocuserV2.CommandBlind
        CheckConnected("CommandBlind")
        ' Call CommandString and return as soon as it finishes
        Me.CommandString(Command, Raw)
        ' or
        Throw New MethodNotImplementedException("CommandBlind")
    End Sub

    Public Function CommandBool(ByVal Command As String, Optional ByVal Raw As Boolean = False) As Boolean _
        Implements IFocuserV2.CommandBool
        CheckConnected("CommandBool")
        Dim ret As String = CommandString(Command, Raw)
        ' TODO decode the return string and return true or false
        ' or
        Throw New MethodNotImplementedException("CommandBool")
    End Function

    Public Function CommandString(ByVal Command As String, Optional ByVal Raw As Boolean = False) As String _
        Implements IFocuserV2.CommandString
        CheckConnected("CommandString")
        ' it's a good idea to put all the low level communication with the device here,
        ' then all communication calls this function
        ' you need something to ensure that only one command is in progress at a time
        Throw New MethodNotImplementedException("CommandString")
    End Function

    Public Property Connected() As Boolean Implements IFocuserV2.Connected
        Get
            Dim cn As Boolean = IsConnected
            TL.LogMessage("Connected Get", cn.ToString())
            Return cn
        End Get

        Set(value As Boolean)
            TL.LogMessage("Connected Set", value.ToString())

            If value Then
                'connect to hardware
                IsConnected = True

                'todo: allow Arduino to send wakeup message before attempting to contact Arduino?

                Dim response As String

                Try
                    response = sendCommand("CONNECTED,GET")
                Catch Exc As System.TimeoutException
                    TL.LogMessage("Connected Set", "Error: Timeout, check connection")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.Utilities.Exceptions.SerialPortInUseException
                    TL.LogMessage("Connected Set", "Error: unable to acquire the serial port")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.NotConnectedException
                    TL.LogMessage("Connected Set", "Error: serial port is not connected")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As Exception
                    TL.LogMessage("Connected Set", "Error: unknown error, check inner exception")   'TODO timeout exceptions are acutally caught here
                    Throw New DriverException(Exc.Message, Exc)
                End Try

                response = response.Replace("CONNECTED,", "").Trim

                If Not IsNumeric(response) Then
                    TL.LogMessage("Connected Set", "Error: could not check connection state because response contained non-numeric elements: " & response)
                    Throw New DriverException("could not check connection state because response contained non-numeric elements: " & response)
                End If

                If CInt(response).Equals(0) Then
                    TL.LogMessage("Connected Set", "Error: Arduino responding, but Telescope is not enabled")

                    'disconnect from hardware
                    IsConnected = False

                    Throw New ASCOM.DriverException("Connected Set Error: Arduino responding, but Focuser is not enabled")
                ElseIf CInt(response).Equals(1) Then
                    TL.LogMessage("Connected Set", "Successfully connected")
                Else
                    TL.LogMessage("Connected Set", "Error: unknown Response: " & response)
                    Throw New ASCOM.DriverException("Connected Set Error: unknown Response: " & response)
                End If

            Else
                IsConnected = False
            End If
        End Set
    End Property

    Public ReadOnly Property Description As String Implements IFocuserV2.Description
        Get
            ' this pattern seems to be needed to allow a public property to return a private field
            Dim d As String = driverDescription
            TL.LogMessage("Description Get", d)
            Return d
        End Get
    End Property

    Public ReadOnly Property DriverInfo As String Implements IFocuserV2.DriverInfo
        Get
            'Dim m_version As Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version
            ' TODO customise this driver description
            'Dim s_driverInfo As String = "Information about the driver itself. Version: " + m_version.Major.ToString() + "." + m_version.Minor.ToString()
            Dim s_driverInfo As String = "YAAADevice Focuser Driver V0.0.1"
            TL.LogMessage("DriverInfo Get", s_driverInfo)
            Return s_driverInfo
        End Get
    End Property

    Public ReadOnly Property DriverVersion() As String Implements IFocuserV2.DriverVersion
        Get
            ' Get our own assembly and report its version number
            TL.LogMessage("DriverVersion Get", Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2))
            Return Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2)
        End Get
    End Property

    Public ReadOnly Property InterfaceVersion() As Short Implements IFocuserV2.InterfaceVersion
        Get
            TL.LogMessage("InterfaceVersion Get", "2")
            Return 2
        End Get
    End Property

    Public ReadOnly Property Name As String Implements IFocuserV2.Name
        Get
            Dim s_name As String = "YAAADevice Focuser Driver"
            TL.LogMessage("Name Get", s_name)
            Return s_name
        End Get
    End Property

    Public Sub Dispose() Implements IFocuserV2.Dispose
        ' Clean up the tracelogger and util objects
        TL.Enabled = False
        TL.Dispose()
        TL = Nothing
        utilities.Dispose()
        utilities = Nothing
        astroUtilities.Dispose()
        astroUtilities = Nothing

        'serial_connection_.Dispose()
    End Sub

#End Region

#Region "IFocuser Implementation"

    Public ReadOnly Property Absolute() As Boolean Implements IFocuserV2.Absolute
        Get
            TL.LogMessage("Absolute Get", focuser_driver_settings_.focuser_settings_.position_absolute_)

            Return focuser_driver_settings_.focuser_settings_.position_absolute_
        End Get
    End Property

    Public Sub Halt() Implements IFocuserV2.Halt
        'throw an exception if the focuser is not connected.
        CheckConnected("Halt")

        Dim response As String
        Dim return_value As UInteger

        'check if the focuser is slewing.
        response = sendCommand("HALT,SET")
        response = response.Replace("HALT,", "").Trim

        return_value = CUInt(response)

        Select Case (return_value)
            Case 0
                TL.LogMessage("Halt", "successful")
            Case 32
                TL.LogMessage("Halt", "Error: Focuser is currently homing")
                Throw New ASCOM.InvalidOperationException("Error: Focuser is currently homing")

        End Select

        'TL.LogMessage("Halt", "Not implemented")
        'Throw New ASCOM.MethodNotImplementedException("Halt")
    End Sub

    Public ReadOnly Property IsMoving() As Boolean Implements IFocuserV2.IsMoving
        Get
            'throw an exception if the focuser is not connected.
            CheckConnected("IsMoving")

            Dim response As String
            Dim return_value As Boolean

            'check if the focuser is slewing.
            response = sendCommand("MOVE,GET")
            response = response.Replace("MOVE,", "").Trim

            return_value = Not CInt(response).Equals(0)

            TL.LogMessage("IsMoving Get", return_value.ToString)

            Return return_value

            'TL.LogMessage("IsMoving Get", False.ToString())
            'Return False ' This focuser always moves instantaneously so no need for IsMoving ever to be True
        End Get
    End Property

    Public Property Link() As Boolean Implements IFocuserV2.Link
        Get
            TL.LogMessage("Link Get", Me.Connected.ToString())
            Return Me.Connected ' Direct function to the connected method, the Link method is just here for backwards compatibility
        End Get
        Set(value As Boolean)
            TL.LogMessage("Link Set", value.ToString())
            Me.Connected = value ' Direct function to the connected method, the Link method is just here for backwards compatibility
        End Set
    End Property

    Public ReadOnly Property MaxIncrement() As Integer Implements IFocuserV2.MaxIncrement
        Get
            TL.LogMessage("MaxIncrement Get", focuser_driver_settings_.focuser_settings_.max_increment_.ToString())
            'Return focuserSteps ' Maximum change in one move
            Return focuser_driver_settings_.focuser_settings_.max_increment_
        End Get
    End Property

    Public ReadOnly Property MaxStep() As Integer Implements IFocuserV2.MaxStep
        Get
            TL.LogMessage("MaxStep Get", focuser_driver_settings_.focuser_settings_.max_step_.ToString())
            'Return focuserSteps ' Maximum extent of the focuser, so position range is 0 to 10,000
            Return focuser_driver_settings_.focuser_settings_.max_step_
        End Get
    End Property

    Public Sub Move(Position As Integer) Implements IFocuserV2.Move
        TL.LogMessage("Move", Position.ToString())
        'focuserPosition = Position ' Set the focuser position

        CheckConnected("Move")

        If TempComp = True Then
            Throw New ASCOM.InvalidOperationException("Move: not allowed while temperature compensation is enabled")
        End If

        Dim response As String
        Dim message As String
        Dim return_value As Integer

        message = "MOVE,SET,"
        message &= Position.ToString

        'check if the focuser is slewing.
        response = sendCommand(message)
        response = response.Replace("MOVE,", "").Trim
        return_value = CInt(response)

        If return_value = 0 Then
            TL.LogMessage("Move", "Success")
        ElseIf (return_value And 1) <> 0 Then
            TL.LogMessage("Move", "Error: Lower Movement Limit Exceeded")
            Throw New ASCOM.InvalidValueException("Move: Error: Lower Movement Limit Exceeded")
        ElseIf (return_value And 2) <> 0 Then
            TL.LogMessage("Move", "Error: Upper  Movement Limit Exceeded")
            Throw New ASCOM.InvalidValueException("Move: Error: Upper  Movement Limit Exceeded")
        ElseIf (return_value And 32) <> 0 Then
            TL.LogMessage("Move", "Error: Focuser is currently homing")
            Throw New ASCOM.InvalidValueException("Move: Error: Focuser is currently homing")
        End If

    End Sub

    Public ReadOnly Property Position() As Integer Implements IFocuserV2.Position
        Get
            CheckConnected("Position Get")

            If Not focuser_driver_settings_.focuser_settings_.position_absolute_ Then
                Throw New ASCOM.PropertyNotImplementedException("Position Get")
            End If

            Dim response As String
            Dim return_value As Integer

            'check if the focuser is slewing.
            response = sendCommand("POSITION,GET")
            response = response.Replace("POSITION,", "").Trim

            return_value = CInt(response)

            TL.LogMessage("Position Get", return_value.ToString)

            Return return_value

            'Return focuserPosition ' Return the focuser position
        End Get
    End Property

    Public ReadOnly Property StepSize() As Double Implements IFocuserV2.StepSize
        Get
            Dim return_value As Double = 0.0

            With focuser_driver_settings_.focuser_settings_
                return_value = .microns_per_rev_ / .number_of_steps_
            End With

            TL.LogMessage("StepSize Get", return_value)
            Return return_value
        End Get
    End Property

    Public Property TempComp() As Boolean Implements IFocuserV2.TempComp
        Get
            CheckConnected("TempComp Get")

            Dim response As String
            Dim return_value As Integer

            'check if the focuser is slewing.
            response = sendCommand("TEMP_COMP,GET")
            response = response.Replace("TEMP_COMP,", "").Trim

            return_value = Not CInt(response).Equals(0)

            TL.LogMessage("TempComp Get", return_value.ToString)

            If Not focuser_driver_settings_.focuser_settings_.tc_available_ And return_value Then
                TL.LogMessage("TempComp Get", "error: Temperature compensation feature is deactivated in driver, but Arduino reports as enabled")
                Throw New ASCOM.DriverException("TempComp get: Temperature compensation feature is deactivated in driver, but Arduino reports as enabled")
            End If

            'check if the correct temperature sensor is used for temperature compensation
            response = sendCommand("TEMP_COMP_SENSOR,GET")
            response = response.Replace("TEMP_COMP_SENSOR,", "").Trim

            'log a warning if an incorrect temperature sensor is being used.
            If Not CByte(response) = focuser_driver_settings_.focuser_settings_.tc_sensor_ Then
                TL.LogMessage("TempComp Get", "error: driver is set to use temperature compensation sensor " & focuser_driver_settings_.focuser_settings_.tc_sensor_ & " but Arduino reports using " & CByte(response))
            End If

            Return return_value
        End Get
        Set(value As Boolean)
            If Not focuser_driver_settings_.focuser_settings_.tc_available_ Then
                TL.LogMessage("TempComp Set", "Not available")
                Throw New ASCOM.PropertyNotImplementedException("Temperature compensation not available", True)
            End If

            CheckConnected("TempComp Set")

            Dim response As String
            Dim return_value As Integer

            'check if the focuser is slewing.
            response = sendCommand("TEMP_COMP,SET," & CByte(value))
            response = response.Replace("TEMP_COMP,", "").Trim

            return_value = Not CInt(response).Equals(0)

            TL.LogMessage("TempComp Set", return_value.ToString)

            If Not return_value Then
                Throw New ASCOM.InvalidOperationException("Temperature Compensation could not be set for Temperature Sensor " & focuser_driver_settings_.focuser_settings_.tc_sensor_ & ". Check the Arduino's configuration.")
            End If
        End Set
    End Property

    Public ReadOnly Property TempCompAvailable() As Boolean Implements IFocuserV2.TempCompAvailable
        Get
            TL.LogMessage("TempCompAvailable Get", focuser_driver_settings_.focuser_settings_.tc_available_.ToString())
            Return focuser_driver_settings_.focuser_settings_.tc_available_
        End Get
    End Property

    Public ReadOnly Property Temperature() As Double Implements IFocuserV2.Temperature
        Get
            If Not focuser_driver_settings_.focuser_settings_.tc_available_ Then
                Throw New ASCOM.PropertyNotImplementedException("Temperature compensation is not activated")
            End If
            CheckConnected("Temperature Set")

            Dim return_value As Double = 0.0

            If Not CBool(measureTemperature(focuser_driver_settings_.focuser_settings_.tc_sensor_)) Then
                Throw New ASCOM.InvalidOperationException("Temperature measurement could not be initiated for sensor " & focuser_driver_settings_.focuser_settings_.tc_sensor_)
            End If

            Do
                'wait until homing process is finished.
                System.Threading.Thread.Sleep(100)
            Loop While Not CBool(hasTemperature(focuser_driver_settings_.focuser_settings_.tc_sensor_))

            return_value = Val(getTemperature(focuser_driver_settings_.focuser_settings_.tc_sensor_))

            'TODO implement check if getTemperature call was successful
            Return return_value
        End Get
    End Property

#End Region

#Region "Private properties And methods"
    ' here are some useful properties and methods that can be used as required
    ' to help with

#Region "ASCOM Registration"

    Private Shared Sub RegUnregASCOM(ByVal bRegister As Boolean)

        Using P As New Profile() With {.DeviceType = "Focuser"}
            If bRegister Then
                P.Register(driverID, driverDescription)
            Else
                P.Unregister(driverID)
            End If
        End Using

    End Sub

    <ComRegisterFunction()>
    Public Shared Sub RegisterASCOM(ByVal T As Type)

        RegUnregASCOM(True)

    End Sub

    <ComUnregisterFunction()>
    Public Shared Sub UnregisterASCOM(ByVal T As Type)

        RegUnregASCOM(False)

    End Sub

#End Region

    ''' <summary>
    ''' Returns true if there is a valid connection to the driver hardware
    ''' </summary>
    Private Property IsConnected As Boolean
        Get
            Dim connected As Boolean = False

            'check that the driver hardware connection exists and is connected to the hardware
            Select Case focuser_driver_settings_.connection_type_
                Case connection_type_t.SERIAL
                    If Not serial_connection_ Is Nothing Then
                        connected = serial_connection_.Connected
                    Else
                        connected = False
                    End If
                Case connection_type_t.NETWORK
                    If Not tcp_connection_ Is Nothing Then
                        connected = tcp_connection_.Connected
                    Else
                        connected = False
                    End If
                Case Else
                    Throw New ASCOM.DriverException("unknown connection state " & focuser_driver_settings_.connection_type_.ToString)
            End Select

            TL.LogMessage("IsConnected Get", connected.ToString() & If(connected, " (connection type: " & focuser_driver_settings_.connection_type_.ToString & ")", ""))

            Return connected
        End Get

        Set(value As Boolean)
            TL.LogMessage("IsConnected Set", value)

            If value = IsConnected Then
                TL.LogMessage("IsConnected Set", "already " & If(value, "connected", "disconnected"))
                Return
            End If

            If value Then
                TL.LogMessage("Connected Set", value)

                Select Case focuser_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        TL.LogMessage("Connected Set", "Connecting to port " & focuser_driver_settings_.com_port_ & "@" & focuser_driver_settings_.baud_rate_)

                        'connect to the device:
                        serial_connection_.PortName = focuser_driver_settings_.com_port_   'use port number read earlier
                        serial_connection_.Speed = focuser_driver_settings_.baud_rate_     'use baud rate read earlier

                        'open serial connection and catch exception if the com port is not available.
                        Try
                            serial_connection_.Connected = True 'enable serial communication.
                            serial_connection_.ClearBuffers()
                        Catch ex As ASCOM.Utilities.Exceptions.InvalidValueException
                            Dim err_msg As String = "Error: COM port does not exist."
                            TL.LogMessage("Connected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception       'TODO catch specific exception types and handle them 
                            Dim err_msg As String = "error: could not open serial connection."
                            TL.LogMessage("Connected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        serial_connection_.ReceiveTimeoutMs = TIMEOUT_SERIAL
                    Case connection_type_t.NETWORK
                        TL.LogMessage("Connected Set", "Connecting to " + focuser_driver_settings_.ip_address_ + ":" & focuser_driver_settings_.port_)

                        Try
                            tcp_connection_ = New Net.Sockets.TcpClient

                            tcp_connection_.Connect(focuser_driver_settings_.ip_address_, focuser_driver_settings_.port_)
                        Catch ex As ArgumentNullException
                            Dim err_msg As String = "error: the host name is null."
                            TL.LogMessage("Connected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ArgumentOutOfRangeException
                            Dim err_msg As String = "error: the port number is outside the valid range of " & System.Net.IPEndPoint.MinPort & " to " & System.Net.IPEndPoint.MaxPort & "."
                            TL.LogMessage("Connected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As System.Net.Sockets.SocketException
                            Dim err_msg As String = "error: an error occured when accessing the socket."
                            TL.LogMessage("Connected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ObjectDisposedException
                            Dim err_msg As String = "error: the TCP client is closed."
                            TL.LogMessage("Connected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception
                            Dim err_msg As String = "error: could not open TCP connection."
                            TL.LogMessage("Connected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        tcp_connection_.SendTimeout = TIMEOUT_TCP
                        tcp_connection_.ReceiveTimeout = TIMEOUT_TCP
                End Select

                'Arduino is reset when opening a Serial Connection. 
                'bootup time might increase in the future. 
                'Wait some time for it to boot up...
                System.Threading.Thread.Sleep(SET_CONNECTED_DELAY)
            Else
                Select Case focuser_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        'disconnected from the device.
                        Try
                            serial_connection_.Connected = False
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("Connected Set", "error: could not close serial connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("Connected Set", "Disconnected from port " + focuser_driver_settings_.com_port_)
                    Case connection_type_t.NETWORK
                        Try
                            tcp_connection_.Close()
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("Connected Set", "error: could not close TCP connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("Connected Set", "Disconnecting from " & focuser_driver_settings_.ip_address_ & ":" & focuser_driver_settings_.port_)
                End Select
            End If
        End Set
    End Property

    ''' <summary>
    ''' Use this function to throw an exception if we aren't connected to the hardware
    ''' </summary>
    ''' <param name="message"></param>
    Private Sub CheckConnected(ByVal message As String)
        If Not IsConnected Then
            Throw New NotConnectedException(message)
        End If
    End Sub

    ''' <summary>
    ''' Read the device configuration from the ASCOM Profile store
    ''' </summary>
    Friend Sub ReadProfile()
        Dim buffer As String = ""

        Using driverProfile As New Profile()
            driverProfile.DeviceType = "Focuser"

            focuserDriverSettings.trace_state_ = Convert.ToBoolean(driverProfile.GetValue(driverID, traceStateProfileName, String.Empty, focuserDriverDefaultValues.trace_state_default_))

            buffer = driverProfile.GetValue(driverID, connectionTypeProfileName, String.Empty, focuserDriverDefaultValues.connection_type_default_)
            focuser_driver_settings_.connection_type_ = [Enum].Parse(GetType(connection_type_t), buffer)

            focuser_driver_settings_.com_port_ = driverProfile.GetValue(driverID, comPortProfileName, String.Empty, focuserDriverDefaultValues.com_port_default_)
            focuser_driver_settings_.baud_rate_ = driverProfile.GetValue(driverID, baudRateProfileName, String.Empty, focuserDriverDefaultValues.baud_rate_default_)

            focuser_driver_settings_.ip_address_ = driverProfile.GetValue(driverID, ipAddressProfileName, String.Empty, focuserDriverDefaultValues.ip_address_default_)
            focuser_driver_settings_.port_ = driverProfile.GetValue(driverID, portProfileName, String.Empty, focuserDriverDefaultValues.port_default_)

            With focuser_driver_settings_.focuser_settings_
                .position_absolute_ = driverProfile.GetValue(driverID, positionAbsoluteProfileName, String.Empty, focuserDefaultValues.position_absolute_default_)
                .max_increment_ = driverProfile.GetValue(driverID, maxIncrementProfileName, String.Empty, focuserDefaultValues.max_increment_default_)
                .microns_per_rev_ = driverProfile.GetValue(driverID, MPRProfileName, String.Empty, focuserDefaultValues.MPR_default_)
                .max_step_ = driverProfile.GetValue(driverID, maxStepProfileName, String.Empty, focuserDefaultValues.max_step_default_)

                .tc_available_ = driverProfile.GetValue(driverID, TCAProfileName, String.Empty, focuserDefaultValues.TCA_default_)
                .tc_sensor_ = driverProfile.GetValue(driverID, TCSProfileName, String.Empty, focuserDefaultValues.TCS_default_)
                .tc_factor_ = driverProfile.GetValue(driverID, TCFProfileName, String.Empty, focuserDefaultValues.TCF_default_)

                'read temperature sensor information from profile store.
                For i As Integer = 0 To numTempSensors - 1
                    .temperature_sensors_(i) = New YAAASharedClassLibrary.YAAASensor(True)

                    With focuser_driver_settings_.focuser_settings_.temperature_sensors_(i).sensor_settings_
                        Try
                            .sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE

                            .sensor_device_ = driverProfile.GetValue(driverID, TSDProfileName, TSSubKeyProfileName & " " & i, 255)
                            .sensor_unit_ = driverProfile.GetValue(driverID, TSUProfileName, TSSubKeyProfileName & " " & i, 1)

                            buffer = driverProfile.GetValue(driverID, TSPProfileName, TSSubKeyProfileName & " " & i, "0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF")
                            .sensor_parameters_ = YAAASharedClassLibrary.hexToByteArray(buffer)
                        Catch exc As Exception
                            TL.LogMessage("ReadProfile", "error reading Temperature Sensor Data of sensor " & i & ": " & exc.ToString)
                        End Try
                    End With
                Next

                .max_speed_ = driverProfile.GetValue(driverID, maxSpeedProfileName, String.Empty, focuserDefaultValues.max_speed_default_)
                .gear_ratio_ = driverProfile.GetValue(driverID, gearRatioProfileName, String.Empty, focuserDefaultValues.gear_ratio_default_)
                .number_of_steps_ = driverProfile.GetValue(driverID, NOSProfileName, String.Empty, focuserDefaultValues.number_of_steps_default_)

                buffer = driverProfile.GetValue(driverID, stepTypeProfileName, String.Empty, focuserDefaultValues.step_type_default_)
                .step_type_ = [Enum].Parse(GetType(Focuser.step_type_t), buffer)

                .home_switch_enabled_ = driverProfile.GetValue(driverID, HSEProfileName, String.Empty, focuserDefaultValues.HSE_default_)
                .home_switch_pin_ = driverProfile.GetValue(driverID, HSPProfileName, String.Empty, focuserDefaultValues.HSP_default_)
                .home_switch_inverted_ = driverProfile.GetValue(driverID, HSIProfileName, String.Empty, focuserDefaultValues.HSI_default_)
                .home_position_ = driverProfile.GetValue(driverID, HPProfileName, String.Empty, focuserDefaultValues.HP_default_)

                .startup_auto_home_ = driverProfile.GetValue(driverID, SAHProfileName, String.Empty, focuserDefaultValues.SAH_default_)
                .automatic_stepper_release_ = driverProfile.GetValue(driverID, ASRProfileName, String.Empty, focuserDefaultValues.ASR_default_)

                .endstop_lower_enabled_ = driverProfile.GetValue(driverID, ELEProfileName, String.Empty, focuserDefaultValues.ELE_default_)
                .endstop_upper_enabled_ = driverProfile.GetValue(driverID, EUEProfileName, String.Empty, focuserDefaultValues.EUE_default_)
                .endstop_lower_pin_ = driverProfile.GetValue(driverID, ELPProfileName, String.Empty, focuserDefaultValues.ELP_default_)
                .endstop_upper_pin_ = driverProfile.GetValue(driverID, EUPProfileName, String.Empty, focuserDefaultValues.EUP_default_)
                .endstop_lower_inverted_ = driverProfile.GetValue(driverID, ELIProfileName, String.Empty, focuserDefaultValues.ELI_default_)

                .limit_lower_enabled_ = driverProfile.GetValue(driverID, LLEProfileName, String.Empty, focuserDefaultValues.LLE_default_)
                .limit_upper_enabled_ = driverProfile.GetValue(driverID, LUEProfileName, String.Empty, focuserDefaultValues.LUE_default_)
                .limit_lower_ = driverProfile.GetValue(driverID, LLProfileName, String.Empty, focuserDefaultValues.LL_default_)
                .limit_upper_ = driverProfile.GetValue(driverID, LUProfileName, String.Empty, focuserDefaultValues.LU_default_)

                buffer = driverProfile.GetValue(driverID, MDTProfileName, String.Empty, focuserDefaultValues.MDT_default_)
                .motor_driver_type_ = [Enum].Parse(GetType(Focuser.motor_driver_t), buffer)

                'I2C Address and Motor Shield Terminal for Adafruit Motor Shield V2
                .I2C_address_ = driverProfile.GetValue(driverID, I2CProfileName, String.Empty, focuserDefaultValues.I2C_default_)
                .terminal_ = driverProfile.GetValue(driverID, TerminalProfileName, String.Empty, focuserDefaultValues.terminal_default_)

                'Pin numbers and Pin Inversion Settings for 4 wire steppers or stepper drivers (big easy etc.)
                .pin_1_ = driverProfile.GetValue(driverID, pin1ProfileName, String.Empty, focuserDefaultValues.pin_1_default_)
                .pin_2_ = driverProfile.GetValue(driverID, pin2ProfileName, String.Empty, focuserDefaultValues.pin_2_default_)
                .pin_3_ = driverProfile.GetValue(driverID, pin3ProfileName, String.Empty, focuserDefaultValues.pin_3_default_)
                .pin_4_ = driverProfile.GetValue(driverID, pin4ProfileName, String.Empty, focuserDefaultValues.pin_4_default_)
                .pin_5_ = driverProfile.GetValue(driverID, pin5ProfileName, String.Empty, focuserDefaultValues.pin_5_default_)
                .pin_6_ = driverProfile.GetValue(driverID, pin6ProfileName, String.Empty, focuserDefaultValues.pin_6_default_)

                .pin_1_inverted_ = driverProfile.GetValue(driverID, pin1InvertedProfileName, String.Empty, focuserDefaultValues.pin_1_inverted_default_)
                .pin_2_inverted_ = driverProfile.GetValue(driverID, pin2InvertedProfileName, String.Empty, focuserDefaultValues.pin_2_inverted_default_)
                .pin_3_inverted_ = driverProfile.GetValue(driverID, pin3InvertedProfileName, String.Empty, focuserDefaultValues.pin_3_inverted_default_)
                .pin_4_inverted_ = driverProfile.GetValue(driverID, pin4InvertedProfileName, String.Empty, focuserDefaultValues.pin_4_inverted_default_)
                .pin_5_inverted_ = driverProfile.GetValue(driverID, pin5InvertedProfileName, String.Empty, focuserDefaultValues.pin_5_inverted_default_)
                .pin_6_inverted_ = driverProfile.GetValue(driverID, pin6InvertedProfileName, String.Empty, focuserDefaultValues.pin_6_inverted_default_)

                .mc_available_ = driverProfile.GetValue(driverID, MCAProfileName, String.Empty, focuserDefaultValues.mc_available_default_)
                .mc_lock_available_ = driverProfile.GetValue(driverID, MCLAProfileName, String.Empty, focuserDefaultValues.mc_lock_available_default_)
                .mc_lock_pin_ = driverProfile.GetValue(driverID, MCLPProfileName, String.Empty, focuserDefaultValues.mc_lock_pin_default_)
                .mc_in_pin_ = driverProfile.GetValue(driverID, MCIPProfileName, String.Empty, focuserDefaultValues.mc_in_pin_default_)
                .mc_out_pin_ = driverProfile.GetValue(driverID, MCOPProfileName, String.Empty, focuserDefaultValues.mc_out_pin_default_)
                .mc_lock_inverted_ = driverProfile.GetValue(driverID, MCLIProfileName, String.Empty, focuserDefaultValues.mc_lock_inverted_default_)
                .mc_in_inverted_ = driverProfile.GetValue(driverID, MCIIProfileName, String.Empty, focuserDefaultValues.mc_in_inverted_default_)
                .mc_out_inverted_ = driverProfile.GetValue(driverID, MCOIProfileName, String.Empty, focuserDefaultValues.mc_out_inverted_default_)
            End With
        End Using
    End Sub

    ''' <summary>
    ''' Write the device configuration to the  ASCOM  Profile store
    ''' </summary>
    Friend Sub WriteProfile()
        Using driverProfile As New Profile()
            driverProfile.DeviceType = "Focuser"
            driverProfile.WriteValue(driverID, traceStateProfileName, focuserDriverSettings.trace_state_.ToString())

            With focuser_driver_settings_

                driverProfile.WriteValue(driverID, connectionTypeProfileName, .connection_type_.ToString)

                driverProfile.WriteValue(driverID, ipAddressProfileName, .ip_address_)
                driverProfile.WriteValue(driverID, portProfileName, .port_)

                driverProfile.WriteValue(driverID, comPortProfileName, .com_port_.ToString())
                driverProfile.WriteValue(driverID, baudRateProfileName, .baud_rate_.ToString())
            End With

            With focuser_driver_settings_.focuser_settings_
                driverProfile.WriteValue(driverID, positionAbsoluteProfileName, .position_absolute_.ToString)
                driverProfile.WriteValue(driverID, maxIncrementProfileName, .max_increment_.ToString)
                driverProfile.WriteValue(driverID, MPRProfileName, .microns_per_rev_.ToString)
                driverProfile.WriteValue(driverID, maxStepProfileName, .max_step_.ToString)

                'temperature compensation settings
                driverProfile.WriteValue(driverID, TCAProfileName, .tc_available_.ToString)
                driverProfile.WriteValue(driverID, TCSProfileName, .tc_sensor_.ToString)
                driverProfile.WriteValue(driverID, TCFProfileName, .tc_factor_.ToString)

                'temperature sensor settings
                For i As Integer = 0 To numTempSensors - 1
                    With focuser_driver_settings_.focuser_settings_.temperature_sensors_(i).sensor_settings_
                        driverProfile.WriteValue(driverID, TSDProfileName, .sensor_device_, TSSubKeyProfileName & " " & i)
                        driverProfile.WriteValue(driverID, TSUProfileName, .sensor_unit_, TSSubKeyProfileName & " " & i)

                        Dim parameters_string As String = ""
                        For j As Integer = 0 To .sensor_parameters_.Length - 1
                            parameters_string += Hex(.sensor_parameters_(j))
                            parameters_string += " "
                        Next
                    End With
                Next


                driverProfile.WriteValue(driverID, maxSpeedProfileName, .max_speed_.ToString)
                driverProfile.WriteValue(driverID, gearRatioProfileName, .gear_ratio_.ToString)
                driverProfile.WriteValue(driverID, NOSProfileName, .number_of_steps_.ToString)

                driverProfile.WriteValue(driverID, stepTypeProfileName, .step_type_.ToString)

                driverProfile.WriteValue(driverID, HSEProfileName, .home_switch_enabled_.ToString)
                driverProfile.WriteValue(driverID, HSPProfileName, .home_switch_pin_.ToString)
                driverProfile.WriteValue(driverID, HSIProfileName, .home_switch_inverted_.ToString)
                driverProfile.WriteValue(driverID, HPProfileName, .home_position_.ToString)

                driverProfile.WriteValue(driverID, SAHProfileName, .startup_auto_home_.ToString)
                driverProfile.WriteValue(driverID, ASRProfileName, .automatic_stepper_release_.ToString)

                driverProfile.WriteValue(driverID, ELEProfileName, .endstop_lower_enabled_.ToString)
                driverProfile.WriteValue(driverID, EUEProfileName, .endstop_upper_enabled_.ToString)
                driverProfile.WriteValue(driverID, ELPProfileName, .endstop_lower_pin_.ToString)
                driverProfile.WriteValue(driverID, EUPProfileName, .endstop_upper_pin_.ToString)
                driverProfile.WriteValue(driverID, ELIProfileName, .endstop_lower_inverted_.ToString)
                driverProfile.WriteValue(driverID, EUIProfileName, .endstop_upper_inverted_.ToString)

                driverProfile.WriteValue(driverID, LLEProfileName, .limit_lower_enabled_.ToString)
                driverProfile.WriteValue(driverID, LUEProfileName, .limit_upper_enabled_.ToString)

                driverProfile.WriteValue(driverID, MDTProfileName, .motor_driver_type_.ToString)

                driverProfile.WriteValue(driverID, I2CProfileName, .I2C_address_.ToString)
                driverProfile.WriteValue(driverID, TerminalProfileName, .terminal_.ToString)

                driverProfile.WriteValue(driverID, pin1ProfileName, .pin_1_.ToString)
                driverProfile.WriteValue(driverID, pin2ProfileName, .pin_2_.ToString)
                driverProfile.WriteValue(driverID, pin3ProfileName, .pin_3_.ToString)
                driverProfile.WriteValue(driverID, pin4ProfileName, .pin_4_.ToString)
                driverProfile.WriteValue(driverID, pin5ProfileName, .pin_5_.ToString)
                driverProfile.WriteValue(driverID, pin6ProfileName, .pin_6_.ToString)

                driverProfile.WriteValue(driverID, pin1InvertedProfileName, .pin_1_inverted_.ToString)
                driverProfile.WriteValue(driverID, pin2InvertedProfileName, .pin_2_inverted_.ToString)
                driverProfile.WriteValue(driverID, pin3InvertedProfileName, .pin_3_inverted_.ToString)
                driverProfile.WriteValue(driverID, pin4InvertedProfileName, .pin_4_inverted_.ToString)
                driverProfile.WriteValue(driverID, pin5InvertedProfileName, .pin_5_inverted_.ToString)
                driverProfile.WriteValue(driverID, pin6InvertedProfileName, .pin_6_inverted_.ToString)

                driverProfile.WriteValue(driverID, MCAProfileName, .mc_available_.ToString)
                driverProfile.WriteValue(driverID, MCLAProfileName, .mc_lock_available_.ToString)
                driverProfile.WriteValue(driverID, MCLPProfileName, .mc_lock_pin_.ToString)
                driverProfile.WriteValue(driverID, MCIPProfileName, .mc_in_pin_.ToString)
                driverProfile.WriteValue(driverID, MCOPProfileName, .mc_out_pin_.ToString)
                driverProfile.WriteValue(driverID, MCLIProfileName, .mc_lock_inverted_.ToString)
                driverProfile.WriteValue(driverID, MCIIProfileName, .mc_in_inverted_.ToString)
                driverProfile.WriteValue(driverID, MCOIProfileName, .mc_out_inverted_.ToString)
            End With
        End Using
    End Sub

#Region "Reading And Writing from / to the Ardunio's EEPROM"

    Friend Sub ReadEEPROM()
        CheckConnected("ReadEEPROM")

        TL.LogMessage("ReadEEPROM", "Start")

        focuser_driver_settings_EEPROM_ = focuser_driver_settings_.Clone()

        Dim buffer As Short

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.position_absolute_, "POSITION_ABSOLUTE")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.microns_per_rev_, "MICRONS_PER_REV")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.max_increment_, "MAX_INCREMENT")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.max_step_, "MAX_STEP")

        'temperature compensation settings
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.tc_available_, "TC_AVAILABLE")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.tc_sensor_, "TC_SENSOR")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.tc_factor_, "TC_FACTOR")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.temperature_sensors_, "TEMPERATURE_SENSOR,")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.max_speed_, "MAX_SPEED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.gear_ratio_, "GEAR_RATIO")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.number_of_steps_, "NUMBER_OF_STEPS")

        readSetting(buffer, "STEP_TYPE")
        focuser_driver_settings_EEPROM_.focuser_settings_.step_type_ = [Enum].Parse(GetType(Focuser.step_type_t), buffer)

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.home_switch_enabled_, "HOME_SWITCH_ENABLED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.home_switch_pin_, "HOME_SWITCH")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.home_switch_inverted_, "HOME_SWITCH_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.home_position_, "HOME_POSITION")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.startup_auto_home_, "STARTUP_AUTO_HOME")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.automatic_stepper_release_, "AUTOMATIC_STEPPER_RELEASE")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.endstop_lower_enabled_, "ENDSTOP_LOWER_ENABLED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.endstop_upper_enabled_, "ENDSTOP_UPPER_ENABLED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.endstop_lower_pin_, "ENDSTOP_LOWER")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.endstop_upper_pin_, "ENDSTOP_UPPER")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.endstop_lower_inverted_, "ENDSTOP_LOWER_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.endstop_upper_inverted_, "ENDSTOP_UPPER_INVERTED")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.limit_lower_enabled_, "LIMIT_LOWER_ENABLED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.limit_upper_enabled_, "LIMIT_UPPER_ENABLED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.limit_lower_, "LIMIT_LOWER")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.limit_upper_, "LIMIT_UPPER")

        readSetting(buffer, "MOTOR_DRIVER_TYPE")
        focuser_driver_settings_EEPROM_.focuser_settings_.motor_driver_type_ = [Enum].Parse(GetType(Focuser.motor_driver_t), buffer)

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.I2C_address_, "I2C_ADDRESS")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.terminal_, "TERMINAL")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_1_, "PIN_1")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_2_, "PIN_2")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_3_, "PIN_3")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_4_, "PIN_4")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_5_, "PIN_5")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_6_, "PIN_6")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_1_inverted_, "PIN_1_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_2_inverted_, "PIN_2_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_3_inverted_, "PIN_3_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_4_inverted_, "PIN_4_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_5_inverted_, "PIN_5_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.pin_6_inverted_, "PIN_6_INVERTED")

        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_available_, "MC_AVAILABLE")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_lock_available_, "MC_BTN_LOCK_AVAILABLE")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_lock_pin_, "MC_BTN_LOCK")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_in_pin_, "MC_BTN_IN")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_out_pin_, "MC_BTN_OUT")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_lock_inverted_, "MC_BTN_LOCK_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_in_inverted_, "MC_BTN_IN_INVERTED")
        readSetting(focuser_driver_settings_EEPROM_.focuser_settings_.mc_out_inverted_, "MC_BTN_OUT_INVERTED")
    End Sub

    Friend Sub WriteEEPROM()
        CheckConnected("WriteEEPROM")

        WriteProfile()

        TL.LogMessage("WriteEEPROM", "Start")

        With focuser_driver_settings_.focuser_settings_
            writeSetting(.position_absolute_, focuser_driver_settings_EEPROM_.focuser_settings_.position_absolute_, "POSITION_ABSOLUTE")
            writeSetting(.microns_per_rev_, focuser_driver_settings_EEPROM_.focuser_settings_.microns_per_rev_, "MICRONS_PER_REV")
            writeSetting(.max_increment_, focuser_driver_settings_EEPROM_.focuser_settings_.max_increment_, "MAX_INCREMENT")
            writeSetting(.max_step_, focuser_driver_settings_EEPROM_.focuser_settings_.max_step_, "MAX_STEP")

            writeSetting(.tc_available_, focuser_driver_settings_EEPROM_.focuser_settings_.tc_available_, "TC_AVAILABLE")
            writeSetting(.tc_sensor_, focuser_driver_settings_EEPROM_.focuser_settings_.tc_sensor_, "TC_SENSOR")
            writeSetting(.tc_available_, focuser_driver_settings_EEPROM_.focuser_settings_.tc_available_, "TC_FACTOR")

            writeSetting(.max_speed_, focuser_driver_settings_EEPROM_.focuser_settings_.max_speed_, "MAX_SPEED")
            writeSetting(.gear_ratio_, focuser_driver_settings_EEPROM_.focuser_settings_.gear_ratio_, "GEAR_RATIO")
            writeSetting(.number_of_steps_, focuser_driver_settings_EEPROM_.focuser_settings_.number_of_steps_, "NUMBER_OF_STEPS")

            writeSetting(CByte(.step_type_), CByte(focuser_driver_settings_EEPROM_.focuser_settings_.step_type_), "STEP_TYPE")

            writeSetting(.home_switch_enabled_, focuser_driver_settings_EEPROM_.focuser_settings_.home_switch_enabled_, "HOME_SWITCH_ENABLED")
            writeSetting(.home_switch_pin_, focuser_driver_settings_EEPROM_.focuser_settings_.home_switch_pin_, "HOME_SWITCH")
            writeSetting(.home_switch_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.home_switch_inverted_, "HOME_SWITCH_INVERTED")
            writeSetting(.home_position_, focuser_driver_settings_EEPROM_.focuser_settings_.home_position_, "HOME_POSITION")

            writeSetting(.startup_auto_home_, focuser_driver_settings_EEPROM_.focuser_settings_.startup_auto_home_, "STARTUP_AUTO_HOME")
            writeSetting(.automatic_stepper_release_, focuser_driver_settings_EEPROM_.focuser_settings_.automatic_stepper_release_, "AUTOMATIC_STEPPER_RELEASE")

            writeSetting(.endstop_lower_enabled_, focuser_driver_settings_EEPROM_.focuser_settings_.endstop_lower_enabled_, "ENDSTOP_LOWER_ENABLED")
            writeSetting(.endstop_upper_enabled_, focuser_driver_settings_EEPROM_.focuser_settings_.endstop_upper_enabled_, "ENDSTOP_UPPER_ENABLED")
            writeSetting(.endstop_lower_pin_, focuser_driver_settings_EEPROM_.focuser_settings_.endstop_lower_pin_, "ENDSTOP_LOWER")
            writeSetting(.endstop_upper_pin_, focuser_driver_settings_EEPROM_.focuser_settings_.endstop_upper_pin_, "ENDSTOP_UPPER")
            writeSetting(.endstop_lower_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.endstop_lower_inverted_, "ENDSTOP_LOWER_INVERTED")
            writeSetting(.endstop_upper_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.endstop_upper_inverted_, "ENDSTOP_UPPER_INVERTED")

            writeSetting(.limit_lower_enabled_, focuser_driver_settings_EEPROM_.focuser_settings_.limit_lower_enabled_, "LIMIT_LOWER_ENABLED")
            writeSetting(.limit_upper_enabled_, focuser_driver_settings_EEPROM_.focuser_settings_.limit_upper_enabled_, "LIMIT_UPPER_ENABLED")
            writeSetting(.limit_lower_, focuser_driver_settings_EEPROM_.focuser_settings_.limit_lower_, "LIMIT_LOWER")
            writeSetting(.limit_upper_, focuser_driver_settings_EEPROM_.focuser_settings_.limit_upper_, "LIMIT_UPPER")

            writeSetting(CByte(.motor_driver_type_), CByte(focuser_driver_settings_EEPROM_.focuser_settings_.motor_driver_type_), "MOTOR_DRIVER_TYPE")

            writeSetting(.I2C_address_, focuser_driver_settings_EEPROM_.focuser_settings_.I2C_address_, "I2C_ADDRESS")
            writeSetting(.terminal_, focuser_driver_settings_EEPROM_.focuser_settings_.terminal_, "TERMINAL")

            writeSetting(.pin_1_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_1_, "PIN_1")
            writeSetting(.pin_2_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_2_, "PIN_2")
            writeSetting(.pin_3_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_3_, "PIN_3")
            writeSetting(.pin_4_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_4_, "PIN_4")
            writeSetting(.pin_5_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_5_, "PIN_5")
            writeSetting(.pin_6_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_6_, "PIN_6")

            writeSetting(.pin_1_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_1_inverted_, "PIN_1_INVERTED")
            writeSetting(.pin_2_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_2_inverted_, "PIN_2_INVERTED")
            writeSetting(.pin_3_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_3_inverted_, "PIN_3_INVERTED")
            writeSetting(.pin_4_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_4_inverted_, "PIN_4_INVERTED")
            writeSetting(.pin_5_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_5_inverted_, "PIN_5_INVERTED")
            writeSetting(.pin_6_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.pin_6_inverted_, "PIN_6_INVERTED")

            writeSetting(.mc_available_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_available_, "MC_AVAILABLE")
            writeSetting(.mc_lock_available_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_lock_available_, "MC_BTN_LOCK_AVAILABLE")
            writeSetting(.mc_lock_pin_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_lock_pin_, "MC_BTN_LOCK")
            writeSetting(.mc_in_pin_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_in_pin_, "MC_BTN_IN")
            writeSetting(.mc_out_pin_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_out_pin_, "MC_BTN_OUT")
            writeSetting(.mc_lock_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_lock_inverted_, "MC_BTN_LOCK_INVERTED")
            writeSetting(.mc_in_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_in_inverted_, "MC_BTN_IN_INVERTED")
            writeSetting(.mc_out_inverted_, focuser_driver_settings_EEPROM_.focuser_settings_.mc_out_inverted_, "MC_BTN_OUT_INVERTED")

            writeSetting(.temperature_sensors_, focuser_driver_settings_EEPROM_.focuser_settings_.temperature_sensors_, "TEMPERATURE_SENSOR")
        End With

        TL.LogMessage("WriteEEPROM", "End")
    End Sub

#Region "reading settings from arduino"

    Private Sub readSetting(ByRef value As Boolean, setting As String)
        value = Not (readSettings(setting).Equals("0"))
    End Sub

    Private Sub readSetting(ByRef value As Byte, setting As String)
        value = CByte(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Short, setting As String)
        value = CShort(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As UShort, setting As String)
        value = CUShort(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Single, setting As String)
        value = Val(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Double, setting As String)
        value = Val(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Integer, setting As String)
        value = CInt(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As UInteger, setting As String)
        value = CUInt(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value() As YAAASharedClassLibrary.YAAASensor, setting As String)
        For i As Integer = 0 To value.Length - 1
            Dim str_buffer As String = readSettings("TEMPERATURE_SENSOR," & i.ToString)
            value(i) = New YAAASharedClassLibrary.YAAASensor(str_buffer)
        Next
    End Sub

    Private Function readSettings(setting As String)
        Dim message As String
        Dim response As String

        message = "SETUP,"          'setup command 
        message &= "GET,"           'read command
        message &= setting           'setting to read

        response = sendCommand(message)
        response = response.Replace("SETUP,", "").Trim      'trim the rest of the message.

        Return response
    End Function

#End Region

#Region "writing settings To arduino"

    Private Sub writeSetting(value As Boolean, ByRef eeprom_value As Boolean, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(Convert.ToInt32(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Byte, ByRef eeprom_value As Byte, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Short, ByRef eeprom_value As Short, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As UShort, ByRef eeprom_value As UShort, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Single, ByRef eeprom_value As Single, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Double, ByRef eeprom_value As Double, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Integer, ByRef eeprom_value As Integer, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value() As YAAASharedClassLibrary.YAAASensor, ByRef eeprom_value() As YAAASharedClassLibrary.YAAASensor, setting As String)
        For i As Integer = 0 To numTempSensors - 1
            If EEPROM_force_write_ Or value(i) <> eeprom_value(i) Then
                writeSettings(i & "," & value(i).toArduinoString(), setting)
                eeprom_value(i) = value(i).Clone
            End If
        Next
    End Sub

    Private Function writeSettings(value As String, setting As String)
        Dim message As String
        Dim response As String

        message = "SETUP,"              'setup command 
        message &= "SET,"               'read command
        message &= setting & ","        'setting to write

        message &= value.ToString()     'value to set

        response = sendCommand(message)
        response = response.Replace("SETUP,", "").Trim      'trim the rest of the message.

        Return response
    End Function

#End Region

#End Region

#Region "additional friend functions"

    'function to return a focuserDriverSettings struct containing the current driver settings.
    '@param eeprom true to get EEPROM settings, false to get normal settings
    Friend Function getDriverSettings(Optional eeprom As Boolean = False) As focuserDriverSettings
        Dim return_value As focuserDriverSettings

        If eeprom = True Then
            return_value = focuser_driver_settings_EEPROM_.Clone
        Else
            return_value = focuser_driver_settings_.Clone
        End If

        Return return_value
    End Function

    'overwrites the driver's telescope settings object with the provided object.
    Friend Sub setDriverSettings(settings As focuserDriverSettings, Optional eeprom As Boolean = False)
        If eeprom = True Then
            focuser_driver_settings_EEPROM_ = settings.Clone
        Else
            focuser_driver_settings_ = settings.Clone
        End If
    End Sub

#Region "Reading / writing settings fron / to .xml files"

    'opens a file and attempts to read a focuserDriverSettings structure from it. 
    '@param file_name full path and file name to file
    '@return driver settings stored in file as focuserDriverSettings
    Friend Function xmlToDriverSettings(file_name As String) As focuserDriverSettings
        Dim return_value As focuserDriverSettings = New focuserDriverSettings(True)

        Dim fs As New FileStream(file_name, FileMode.Open, FileAccess.Read)
        Dim reader As XmlDictionaryReader = XmlDictionaryReader.CreateTextReader(fs, New XmlDictionaryReaderQuotas())
        Dim serializer = New DataContractSerializer(GetType(focuserDriverSettings))

        return_value = CType(serializer.ReadObject(reader, True), focuserDriverSettings)
        reader.Close()
        fs.Close()

        Return return_value
    End Function

    'serializes a focuserDriverSettings struct and saves it to a file.
    '@param file_name full path and file name of file
    '@param settings focuser driver settings as focuserDriverSettings
    Friend Sub xmlFromDriverSettings(file_name As String, settings As focuserDriverSettings)
        Dim serializer As New DataContractSerializer(GetType(focuserDriverSettings))
        Dim writer As New FileStream(file_name, FileMode.Create)

        serializer.WriteObject(writer, settings)
        writer.Close()
    End Sub

#End Region

#End Region

#Region "additional Private functions"

    Private Function sendCommand(command As String) As String
        Dim message As String = ""
        Dim response As String = ""

        'wrap command string.
        message = "<"               'beginning of the message
        message &= "F,"             'device identifier
        message &= command          'command
        message &= ">"              'end of message

        'TL.LogMessage("SERIAL TRANSMIT", message)

        Select Case focuser_driver_settings_.connection_type_
            Case connection_type_t.SERIAL
                'transmit message to arduino
                serial_connection_.Transmit(message)

                response = serial_connection_.ReceiveTerminated(">")

            Case connection_type_t.NETWORK
                'log sent command
                TL.LogMessage("TCP TRANSMIT", message)

                'copy message to buffer and send buffer
                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(message)
                tcp_connection_.GetStream.Write(sendBytes, 0, sendBytes.Length)

                'receive response into receive buffer
                Dim recLength As Integer    'number of bytes received
                Dim recBytes(tcp_connection_.ReceiveBufferSize) As Byte
                recLength = tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))

                'convert receive buffer to string
                response = Encoding.ASCII.GetString(recBytes, 0, recLength)

                'log response from Arduino
                TL.LogMessage("TCP RECEIVE", response)

                'read \cr\lf sent by arduino (the arduino sends these in a seperate packet).
                tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))

        End Select

        response = response.Trim

        'TL.LogMessage("SERIAL RECEIVE", response)

        response = response.Replace("<F,", "")
        response = response.Replace(">", "")

        'check the response for serial communication errors.
        checkMessageError(response)

        Return response
    End Function

    'parses serial error messages (ERROR_code) and throws an appropriate exception.
    Private Sub checkMessageError(message As String)
        Dim error_pattern As String = "ERROR_\d+$"
        Dim match As RegularExpressions.Match = RegularExpressions.Regex.Match(message, error_pattern)

        'do nothing if no error message has been received.
        If Not match.Success Then Exit Sub

        'parse the error code when an error message has been received.
        Dim error_code As serial_error_code_t = serial_error_code_t.ERROR_NONE
        Dim error_code_known As Boolean = False

        Dim error_message As String = match.ToString
        TL.LogMessage("checkMessageError", error_message)

        error_code_known = [Enum].TryParse(error_message.Replace("ERROR_", ""), error_code)

        If Not error_code_known Then
            TL.LogMessage("checkMessageError", "unknown error: " & error_message)
            Throw New ASCOM.DriverException("checkMessageError: unknown error: " & error_message)
        Else
            TL.LogMessage("checkMessageError", error_code.ToString)

            If error_code = serial_error_code_t.ERROR_NONE Then
                Exit Sub
            Else
                Throw New ASCOM.DriverException("Serial Communication Error: " & error_code.ToString)
            End If
        End If
    End Sub

    Private Function getSwitchState(axis As String) As String
        If axis Is "" Then
            Dim response As String
            response = sendCommand("SWITCH_STATE,GET")
            response = response.Replace("SWITCH_STATE,", "").Trim

            Return response
        Else
            Throw New ASCOM.InvalidValueException
        End If
    End Function

    Private Function measureTemperature(sensor As String) As String
        Dim response As String
        response = sendCommand("TEMP_MEASURE,SET," & sensor)
        response = response.Replace("TEMP_MEASURE,", "").Trim

        Return response
    End Function

    Private Function hasTemperature(sensor As String) As String
        Dim response As String
        response = sendCommand("TEMP_MEASURE,GET," & sensor)
        response = response.Replace("TEMP_MEASURE,", "").Trim

        Return response
    End Function

    Private Function getTemperature(sensor As String) As String
        Dim response As String
        response = sendCommand("TEMP,GET," & sensor)
        response = response.Replace("TEMP,", "").Trim

        Return response
    End Function

#End Region

#End Region

End Class
