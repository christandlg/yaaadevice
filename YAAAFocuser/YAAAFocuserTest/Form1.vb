﻿Public Class Form1

    Private driver As ASCOM.DriverAccess.Focuser

    ''' <summary>
    ''' This event is where the driver is choosen. The device ID will be saved in the settings.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub buttonChoose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonChoose.Click
        My.Settings.DriverId = ASCOM.DriverAccess.Focuser.Choose(My.Settings.DriverId)
        SetUIState()
    End Sub

    ''' <summary>
    ''' Connects to the device to be tested.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub buttonConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonConnect.Click
        If (IsConnected) Then
            driver.Connected = False
        Else
            driver = New ASCOM.DriverAccess.Focuser(My.Settings.DriverId)
            driver.Connected = True
        End If
        SetUIState()
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If IsConnected Then
            driver.Connected = False
        End If
        ' the settings are saved automatically when this application is closed.
    End Sub

    ''' <summary>
    ''' Sets the state of the UI depending on the device state
    ''' </summary>
    Private Sub SetUIState()
        buttonConnect.Enabled = Not String.IsNullOrEmpty(My.Settings.DriverId)
        buttonChoose.Enabled = Not IsConnected
        buttonConnect.Text = IIf(IsConnected, "Disconnect", "Connect")
    End Sub

    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>true</c> if this instance is connected; otherwise, <c>false</c>.
    ''' 
    ''' </value>
    Private ReadOnly Property IsConnected() As Boolean
        Get
            If Me.driver Is Nothing Then Return False
            Return driver.Connected
        End Get
    End Property

    Private Sub ButtonAbsolute_Click(sender As Object, e As EventArgs) Handles ButtonAbsolute.Click
        Try
            LabelAbsolute.Text = driver.Absolute()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonIsMoving_Click(sender As Object, e As EventArgs) Handles ButtonIsMoving.Click
        Try
            LabelIsMoving.Text = driver.IsMoving()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonMaxIncrement_Click(sender As Object, e As EventArgs) Handles ButtonMaxIncrement.Click
        Try
            LabelMaxIncrement.Text = driver.MaxIncrement()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonMaxStep_Click(sender As Object, e As EventArgs) Handles ButtonMaxStep.Click
        Try
            LabelMaxStep.Text = driver.MaxStep()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonPosition_Click(sender As Object, e As EventArgs) Handles ButtonPosition.Click
        Try
            LabelPosition.Text = driver.Position()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonStepSize_Click(sender As Object, e As EventArgs) Handles ButtonStepSize.Click
        Try
            LabelStepSize.Text = driver.StepSize()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonTempCompAvailable_Click(sender As Object, e As EventArgs) Handles ButtonTempCompAvailable.Click
        Try
            LabelTempCompAvailable.Text = driver.TempCompAvailable()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonTemperature_Click(sender As Object, e As EventArgs) Handles ButtonTemperature.Click
        Try
            LabelTemperature.Text = driver.Temperature()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonHalt_Click(sender As Object, e As EventArgs) Handles ButtonHalt.Click
        Try
            driver.Halt()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonMove_Click(sender As Object, e As EventArgs) Handles ButtonMove.Click
        Try
            driver.Move(CInt(TextBoxMove.Text))
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonTempCompGet_Click(sender As Object, e As EventArgs) Handles ButtonTempCompGet.Click
        Try
            CheckBoxTempComp.Checked = driver.TempComp()
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    Private Sub ButtonTempCompSet_Click(sender As Object, e As EventArgs) Handles ButtonTempCompSet.Click
        Try
            driver.TempComp = CheckBoxTempComp.Checked
        Catch exc As Exception
            MsgBox(exc.Message)
        End Try
    End Sub

    ' TODO: Add additional UI and controls to test more of the driver being tested.

End Class
