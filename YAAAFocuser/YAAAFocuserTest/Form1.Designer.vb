﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.labelDriverId = New System.Windows.Forms.Label()
        Me.buttonConnect = New System.Windows.Forms.Button()
        Me.buttonChoose = New System.Windows.Forms.Button()
        Me.LabelAbsolute = New System.Windows.Forms.Label()
        Me.ButtonAbsolute = New System.Windows.Forms.Button()
        Me.LabelIsMoving = New System.Windows.Forms.Label()
        Me.ButtonIsMoving = New System.Windows.Forms.Button()
        Me.LabelMaxIncrement = New System.Windows.Forms.Label()
        Me.ButtonMaxIncrement = New System.Windows.Forms.Button()
        Me.LabelMaxStep = New System.Windows.Forms.Label()
        Me.ButtonMaxStep = New System.Windows.Forms.Button()
        Me.LabelPosition = New System.Windows.Forms.Label()
        Me.ButtonPosition = New System.Windows.Forms.Button()
        Me.LabelStepSize = New System.Windows.Forms.Label()
        Me.ButtonStepSize = New System.Windows.Forms.Button()
        Me.LabelTempComp = New System.Windows.Forms.Label()
        Me.ButtonTempCompGet = New System.Windows.Forms.Button()
        Me.LabelTempCompAvailable = New System.Windows.Forms.Label()
        Me.ButtonTempCompAvailable = New System.Windows.Forms.Button()
        Me.LabelTemperature = New System.Windows.Forms.Label()
        Me.ButtonTemperature = New System.Windows.Forms.Button()
        Me.ButtonHalt = New System.Windows.Forms.Button()
        Me.TextBoxMove = New System.Windows.Forms.TextBox()
        Me.ButtonMove = New System.Windows.Forms.Button()
        Me.ButtonTempCompSet = New System.Windows.Forms.Button()
        Me.CheckBoxTempComp = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'labelDriverId
        '
        Me.labelDriverId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.labelDriverId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ASCOM.YAAADevice.My.MySettings.Default, "DriverId", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.labelDriverId.Location = New System.Drawing.Point(12, 37)
        Me.labelDriverId.Name = "labelDriverId"
        Me.labelDriverId.Size = New System.Drawing.Size(291, 21)
        Me.labelDriverId.TabIndex = 5
        Me.labelDriverId.Text = Global.ASCOM.YAAADevice.My.MySettings.Default.DriverId
        Me.labelDriverId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'buttonConnect
        '
        Me.buttonConnect.Location = New System.Drawing.Point(316, 36)
        Me.buttonConnect.Name = "buttonConnect"
        Me.buttonConnect.Size = New System.Drawing.Size(72, 23)
        Me.buttonConnect.TabIndex = 4
        Me.buttonConnect.Text = "Connect"
        Me.buttonConnect.UseVisualStyleBackColor = True
        '
        'buttonChoose
        '
        Me.buttonChoose.Location = New System.Drawing.Point(316, 7)
        Me.buttonChoose.Name = "buttonChoose"
        Me.buttonChoose.Size = New System.Drawing.Size(72, 23)
        Me.buttonChoose.TabIndex = 3
        Me.buttonChoose.Text = "Choose"
        Me.buttonChoose.UseVisualStyleBackColor = True
        '
        'LabelAbsolute
        '
        Me.LabelAbsolute.Location = New System.Drawing.Point(146, 76)
        Me.LabelAbsolute.Name = "LabelAbsolute"
        Me.LabelAbsolute.Size = New System.Drawing.Size(59, 23)
        Me.LabelAbsolute.TabIndex = 17
        Me.LabelAbsolute.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonAbsolute
        '
        Me.ButtonAbsolute.Location = New System.Drawing.Point(12, 76)
        Me.ButtonAbsolute.Name = "ButtonAbsolute"
        Me.ButtonAbsolute.Size = New System.Drawing.Size(128, 23)
        Me.ButtonAbsolute.TabIndex = 16
        Me.ButtonAbsolute.Text = "Absolute"
        Me.ButtonAbsolute.UseVisualStyleBackColor = True
        '
        'LabelIsMoving
        '
        Me.LabelIsMoving.Location = New System.Drawing.Point(146, 105)
        Me.LabelIsMoving.Name = "LabelIsMoving"
        Me.LabelIsMoving.Size = New System.Drawing.Size(59, 23)
        Me.LabelIsMoving.TabIndex = 19
        Me.LabelIsMoving.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonIsMoving
        '
        Me.ButtonIsMoving.Location = New System.Drawing.Point(12, 105)
        Me.ButtonIsMoving.Name = "ButtonIsMoving"
        Me.ButtonIsMoving.Size = New System.Drawing.Size(128, 23)
        Me.ButtonIsMoving.TabIndex = 18
        Me.ButtonIsMoving.Text = "IsMoving"
        Me.ButtonIsMoving.UseVisualStyleBackColor = True
        '
        'LabelMaxIncrement
        '
        Me.LabelMaxIncrement.Location = New System.Drawing.Point(146, 134)
        Me.LabelMaxIncrement.Name = "LabelMaxIncrement"
        Me.LabelMaxIncrement.Size = New System.Drawing.Size(59, 23)
        Me.LabelMaxIncrement.TabIndex = 21
        Me.LabelMaxIncrement.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonMaxIncrement
        '
        Me.ButtonMaxIncrement.Location = New System.Drawing.Point(12, 134)
        Me.ButtonMaxIncrement.Name = "ButtonMaxIncrement"
        Me.ButtonMaxIncrement.Size = New System.Drawing.Size(128, 23)
        Me.ButtonMaxIncrement.TabIndex = 20
        Me.ButtonMaxIncrement.Text = "MaxIncrement"
        Me.ButtonMaxIncrement.UseVisualStyleBackColor = True
        '
        'LabelMaxStep
        '
        Me.LabelMaxStep.Location = New System.Drawing.Point(146, 163)
        Me.LabelMaxStep.Name = "LabelMaxStep"
        Me.LabelMaxStep.Size = New System.Drawing.Size(59, 23)
        Me.LabelMaxStep.TabIndex = 23
        Me.LabelMaxStep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ButtonMaxStep
        '
        Me.ButtonMaxStep.Location = New System.Drawing.Point(12, 163)
        Me.ButtonMaxStep.Name = "ButtonMaxStep"
        Me.ButtonMaxStep.Size = New System.Drawing.Size(128, 23)
        Me.ButtonMaxStep.TabIndex = 22
        Me.ButtonMaxStep.Text = "MaxStep"
        Me.ButtonMaxStep.UseVisualStyleBackColor = True
        '
        'LabelPosition
        '
        Me.LabelPosition.Location = New System.Drawing.Point(146, 192)
        Me.LabelPosition.Name = "LabelPosition"
        Me.LabelPosition.Size = New System.Drawing.Size(59, 23)
        Me.LabelPosition.TabIndex = 25
        Me.LabelPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonPosition
        '
        Me.ButtonPosition.Location = New System.Drawing.Point(12, 192)
        Me.ButtonPosition.Name = "ButtonPosition"
        Me.ButtonPosition.Size = New System.Drawing.Size(128, 23)
        Me.ButtonPosition.TabIndex = 24
        Me.ButtonPosition.Text = "Position"
        Me.ButtonPosition.UseVisualStyleBackColor = True
        '
        'LabelStepSize
        '
        Me.LabelStepSize.Location = New System.Drawing.Point(146, 221)
        Me.LabelStepSize.Name = "LabelStepSize"
        Me.LabelStepSize.Size = New System.Drawing.Size(59, 23)
        Me.LabelStepSize.TabIndex = 27
        Me.LabelStepSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonStepSize
        '
        Me.ButtonStepSize.Location = New System.Drawing.Point(12, 221)
        Me.ButtonStepSize.Name = "ButtonStepSize"
        Me.ButtonStepSize.Size = New System.Drawing.Size(128, 23)
        Me.ButtonStepSize.TabIndex = 26
        Me.ButtonStepSize.Text = "StepSize"
        Me.ButtonStepSize.UseVisualStyleBackColor = True
        '
        'LabelTempComp
        '
        Me.LabelTempComp.Location = New System.Drawing.Point(146, 250)
        Me.LabelTempComp.Name = "LabelTempComp"
        Me.LabelTempComp.Size = New System.Drawing.Size(59, 23)
        Me.LabelTempComp.TabIndex = 29
        Me.LabelTempComp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonTempCompGet
        '
        Me.ButtonTempCompGet.Location = New System.Drawing.Point(214, 157)
        Me.ButtonTempCompGet.Name = "ButtonTempCompGet"
        Me.ButtonTempCompGet.Size = New System.Drawing.Size(55, 23)
        Me.ButtonTempCompGet.TabIndex = 28
        Me.ButtonTempCompGet.Text = "Get"
        Me.ButtonTempCompGet.UseVisualStyleBackColor = True
        '
        'LabelTempCompAvailable
        '
        Me.LabelTempCompAvailable.Location = New System.Drawing.Point(146, 250)
        Me.LabelTempCompAvailable.Name = "LabelTempCompAvailable"
        Me.LabelTempCompAvailable.Size = New System.Drawing.Size(59, 23)
        Me.LabelTempCompAvailable.TabIndex = 31
        Me.LabelTempCompAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonTempCompAvailable
        '
        Me.ButtonTempCompAvailable.Location = New System.Drawing.Point(12, 250)
        Me.ButtonTempCompAvailable.Name = "ButtonTempCompAvailable"
        Me.ButtonTempCompAvailable.Size = New System.Drawing.Size(128, 23)
        Me.ButtonTempCompAvailable.TabIndex = 30
        Me.ButtonTempCompAvailable.Text = "TempCompAvailable"
        Me.ButtonTempCompAvailable.UseVisualStyleBackColor = True
        '
        'LabelTemperature
        '
        Me.LabelTemperature.Location = New System.Drawing.Point(146, 279)
        Me.LabelTemperature.Name = "LabelTemperature"
        Me.LabelTemperature.Size = New System.Drawing.Size(59, 23)
        Me.LabelTemperature.TabIndex = 33
        Me.LabelTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonTemperature
        '
        Me.ButtonTemperature.Location = New System.Drawing.Point(12, 279)
        Me.ButtonTemperature.Name = "ButtonTemperature"
        Me.ButtonTemperature.Size = New System.Drawing.Size(128, 23)
        Me.ButtonTemperature.TabIndex = 32
        Me.ButtonTemperature.Text = "Temperature"
        Me.ButtonTemperature.UseVisualStyleBackColor = True
        '
        'ButtonHalt
        '
        Me.ButtonHalt.Location = New System.Drawing.Point(211, 76)
        Me.ButtonHalt.Name = "ButtonHalt"
        Me.ButtonHalt.Size = New System.Drawing.Size(128, 23)
        Me.ButtonHalt.TabIndex = 34
        Me.ButtonHalt.Text = "Halt"
        Me.ButtonHalt.UseVisualStyleBackColor = True
        '
        'TextBoxMove
        '
        Me.TextBoxMove.Location = New System.Drawing.Point(348, 107)
        Me.TextBoxMove.Name = "TextBoxMove"
        Me.TextBoxMove.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxMove.TabIndex = 37
        '
        'ButtonMove
        '
        Me.ButtonMove.Location = New System.Drawing.Point(211, 105)
        Me.ButtonMove.Name = "ButtonMove"
        Me.ButtonMove.Size = New System.Drawing.Size(128, 23)
        Me.ButtonMove.TabIndex = 36
        Me.ButtonMove.Text = "Move"
        Me.ButtonMove.UseVisualStyleBackColor = True
        '
        'ButtonTempCompSet
        '
        Me.ButtonTempCompSet.Location = New System.Drawing.Point(278, 157)
        Me.ButtonTempCompSet.Name = "ButtonTempCompSet"
        Me.ButtonTempCompSet.Size = New System.Drawing.Size(61, 23)
        Me.ButtonTempCompSet.TabIndex = 39
        Me.ButtonTempCompSet.Text = "Set"
        Me.ButtonTempCompSet.UseVisualStyleBackColor = True
        '
        'CheckBoxTempComp
        '
        Me.CheckBoxTempComp.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxTempComp.Location = New System.Drawing.Point(214, 134)
        Me.CheckBoxTempComp.Name = "CheckBoxTempComp"
        Me.CheckBoxTempComp.Size = New System.Drawing.Size(150, 24)
        Me.CheckBoxTempComp.TabIndex = 40
        Me.CheckBoxTempComp.Text = "TempComp"
        Me.CheckBoxTempComp.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(411, 309)
        Me.Controls.Add(Me.CheckBoxTempComp)
        Me.Controls.Add(Me.ButtonTempCompSet)
        Me.Controls.Add(Me.TextBoxMove)
        Me.Controls.Add(Me.ButtonMove)
        Me.Controls.Add(Me.ButtonHalt)
        Me.Controls.Add(Me.LabelTemperature)
        Me.Controls.Add(Me.ButtonTemperature)
        Me.Controls.Add(Me.LabelTempCompAvailable)
        Me.Controls.Add(Me.ButtonTempCompAvailable)
        Me.Controls.Add(Me.LabelTempComp)
        Me.Controls.Add(Me.ButtonTempCompGet)
        Me.Controls.Add(Me.LabelStepSize)
        Me.Controls.Add(Me.ButtonStepSize)
        Me.Controls.Add(Me.LabelPosition)
        Me.Controls.Add(Me.ButtonPosition)
        Me.Controls.Add(Me.LabelMaxStep)
        Me.Controls.Add(Me.ButtonMaxStep)
        Me.Controls.Add(Me.LabelMaxIncrement)
        Me.Controls.Add(Me.ButtonMaxIncrement)
        Me.Controls.Add(Me.LabelIsMoving)
        Me.Controls.Add(Me.ButtonIsMoving)
        Me.Controls.Add(Me.LabelAbsolute)
        Me.Controls.Add(Me.ButtonAbsolute)
        Me.Controls.Add(Me.labelDriverId)
        Me.Controls.Add(Me.buttonConnect)
        Me.Controls.Add(Me.buttonChoose)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents labelDriverId As System.Windows.Forms.Label
    Private WithEvents buttonConnect As System.Windows.Forms.Button
    Private WithEvents buttonChoose As System.Windows.Forms.Button
    Friend WithEvents LabelAbsolute As Label
    Friend WithEvents ButtonAbsolute As Button
    Friend WithEvents LabelIsMoving As Label
    Friend WithEvents ButtonIsMoving As Button
    Friend WithEvents LabelMaxIncrement As Label
    Friend WithEvents ButtonMaxIncrement As Button
    Friend WithEvents LabelMaxStep As Label
    Friend WithEvents ButtonMaxStep As Button
    Friend WithEvents LabelPosition As Label
    Friend WithEvents ButtonPosition As Button
    Friend WithEvents LabelStepSize As Label
    Friend WithEvents ButtonStepSize As Button
    Friend WithEvents LabelTempComp As Label
    Friend WithEvents ButtonTempCompGet As Button
    Friend WithEvents LabelTempCompAvailable As Label
    Friend WithEvents ButtonTempCompAvailable As Button
    Friend WithEvents LabelTemperature As Label
    Friend WithEvents ButtonTemperature As Button
    Friend WithEvents ButtonHalt As Button
    Friend WithEvents TextBoxMove As TextBox
    Friend WithEvents ButtonMove As Button
    Friend WithEvents ButtonTempCompSet As Button
    Friend WithEvents CheckBoxTempComp As CheckBox
End Class
