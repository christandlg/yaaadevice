﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.labelDriverId = New System.Windows.Forms.Label()
        Me.buttonConnect = New System.Windows.Forms.Button()
        Me.buttonChoose = New System.Windows.Forms.Button()
        Me.ButtonAbortSlew = New System.Windows.Forms.Button()
        Me.ButtonSlewToAltAz = New System.Windows.Forms.Button()
        Me.TextBoxAltitude = New System.Windows.Forms.TextBox()
        Me.TextBoxAzimuth = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ButtonSlewToAltAzAsync = New System.Windows.Forms.Button()
        Me.ButtonAtHome = New System.Windows.Forms.Button()
        Me.ButtonAtPark = New System.Windows.Forms.Button()
        Me.LabelAtHome = New System.Windows.Forms.Label()
        Me.LabelAtPark = New System.Windows.Forms.Label()
        Me.ButtonPark = New System.Windows.Forms.Button()
        Me.ButtonUnPark = New System.Windows.Forms.Button()
        Me.ButtonSetPark = New System.Windows.Forms.Button()
        Me.ButtonFindHome = New System.Windows.Forms.Button()
        Me.ButtonMoveAxis = New System.Windows.Forms.Button()
        Me.TextBoxAxis = New System.Windows.Forms.TextBox()
        Me.TextBoxRate = New System.Windows.Forms.TextBox()
        Me.TextBoxDuration = New System.Windows.Forms.TextBox()
        Me.TextBoxGuideDirection = New System.Windows.Forms.TextBox()
        Me.ButtonPulseGuide = New System.Windows.Forms.Button()
        Me.ButtonSlewtoCoordinatesAsync = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxDec = New System.Windows.Forms.TextBox()
        Me.TextBoxRightAsc = New System.Windows.Forms.TextBox()
        Me.ButtonSlewToCoordinates = New System.Windows.Forms.Button()
        Me.ButtonSlewToTargetAsync = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBoxTargetDec = New System.Windows.Forms.TextBox()
        Me.TextBoxTargetRA = New System.Windows.Forms.TextBox()
        Me.ButtonSlewToTarget = New System.Windows.Forms.Button()
        Me.ButtonSyncToAltAz = New System.Windows.Forms.Button()
        Me.ButtonSyncToCoordinates = New System.Windows.Forms.Button()
        Me.ButtonSyncToTarget = New System.Windows.Forms.Button()
        Me.ButtonGetTarget = New System.Windows.Forms.Button()
        Me.ButtonSetTarget = New System.Windows.Forms.Button()
        Me.ButtonSetSiteData = New System.Windows.Forms.Button()
        Me.TextBoxSiteLatitude = New System.Windows.Forms.TextBox()
        Me.TextBoxSiteLongitude = New System.Windows.Forms.TextBox()
        Me.ButtonIsPulseGuiding = New System.Windows.Forms.Button()
        Me.LabelIsPulseGuiding = New System.Windows.Forms.Label()
        Me.LabelIsTracking = New System.Windows.Forms.Label()
        Me.ButtonIsTracking = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ButtonTrackingEnable = New System.Windows.Forms.Button()
        Me.ButtonTrackingDisable = New System.Windows.Forms.Button()
        Me.LabelIsSlewing = New System.Windows.Forms.Label()
        Me.ButtonIsSlewing = New System.Windows.Forms.Button()
        Me.ButtonRaDecRateSet = New System.Windows.Forms.Button()
        Me.ButtonRaDecRateGet = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxDecRate = New System.Windows.Forms.TextBox()
        Me.TextBoxRightAscRate = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ButtonReadCapabilities = New System.Windows.Forms.Button()
        Me.CheckBoxCanFindHome = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanPark = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanPulseGuide = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSetDeclinationRate = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSetGuideRates = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSetPark = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSetPierSide = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSetRightAscensionRate = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSetTracking = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSlew = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSlewAltAz = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSlewAltAzAsync = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSlewAsync = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSync = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanSyncAltAz = New System.Windows.Forms.CheckBox()
        Me.CheckBoxCanUnpark = New System.Windows.Forms.CheckBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBoxSiteElevation = New System.Windows.Forms.TextBox()
        Me.ButtonGetSiteData = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.LabelDec = New System.Windows.Forms.Label()
        Me.LabelRa = New System.Windows.Forms.Label()
        Me.LabelAz = New System.Windows.Forms.Label()
        Me.LabelAlt = New System.Windows.Forms.Label()
        Me.ButtonGetTrackingRates = New System.Windows.Forms.Button()
        Me.ListBoxTrackingRates = New System.Windows.Forms.ListBox()
        Me.ButtonGetTrackingRate = New System.Windows.Forms.Button()
        Me.ButtonSetTrackingRate = New System.Windows.Forms.Button()
        Me.ButtonGetEnumerator = New System.Windows.Forms.Button()
        Me.LabelSiderealTime = New System.Windows.Forms.Label()
        Me.ButtonSiderealTime = New System.Windows.Forms.Button()
        Me.ListBoxSupportedActions = New System.Windows.Forms.ListBox()
        Me.ButtonAction = New System.Windows.Forms.Button()
        Me.ButtonGetSupportedActions = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'labelDriverId
        '
        Me.labelDriverId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.labelDriverId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ASCOM.YAAADevice.My.MySettings.Default, "DriverId", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.labelDriverId.Location = New System.Drawing.Point(12, 37)
        Me.labelDriverId.Name = "labelDriverId"
        Me.labelDriverId.Size = New System.Drawing.Size(291, 21)
        Me.labelDriverId.TabIndex = 5
        Me.labelDriverId.Text = Global.ASCOM.YAAADevice.My.MySettings.Default.DriverId
        Me.labelDriverId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'buttonConnect
        '
        Me.buttonConnect.Location = New System.Drawing.Point(316, 36)
        Me.buttonConnect.Name = "buttonConnect"
        Me.buttonConnect.Size = New System.Drawing.Size(72, 23)
        Me.buttonConnect.TabIndex = 4
        Me.buttonConnect.Text = "Connect"
        Me.buttonConnect.UseVisualStyleBackColor = True
        '
        'buttonChoose
        '
        Me.buttonChoose.Location = New System.Drawing.Point(316, 7)
        Me.buttonChoose.Name = "buttonChoose"
        Me.buttonChoose.Size = New System.Drawing.Size(72, 23)
        Me.buttonChoose.TabIndex = 3
        Me.buttonChoose.Text = "Choose"
        Me.buttonChoose.UseVisualStyleBackColor = True
        '
        'ButtonAbortSlew
        '
        Me.ButtonAbortSlew.Location = New System.Drawing.Point(201, 79)
        Me.ButtonAbortSlew.Name = "ButtonAbortSlew"
        Me.ButtonAbortSlew.Size = New System.Drawing.Size(102, 23)
        Me.ButtonAbortSlew.TabIndex = 6
        Me.ButtonAbortSlew.Text = "AbortSlew"
        Me.ButtonAbortSlew.UseVisualStyleBackColor = True
        '
        'ButtonSlewToAltAz
        '
        Me.ButtonSlewToAltAz.Location = New System.Drawing.Point(201, 414)
        Me.ButtonSlewToAltAz.Name = "ButtonSlewToAltAz"
        Me.ButtonSlewToAltAz.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSlewToAltAz.TabIndex = 7
        Me.ButtonSlewToAltAz.Text = "SlewToAltAz"
        Me.ButtonSlewToAltAz.UseVisualStyleBackColor = True
        '
        'TextBoxAltitude
        '
        Me.TextBoxAltitude.Location = New System.Drawing.Point(309, 445)
        Me.TextBoxAltitude.Name = "TextBoxAltitude"
        Me.TextBoxAltitude.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxAltitude.TabIndex = 8
        '
        'TextBoxAzimuth
        '
        Me.TextBoxAzimuth.Location = New System.Drawing.Point(377, 445)
        Me.TextBoxAzimuth.Name = "TextBoxAzimuth"
        Me.TextBoxAzimuth.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxAzimuth.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(309, 414)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 23)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Altitude"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(374, 414)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 23)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Azimuth"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonSlewToAltAzAsync
        '
        Me.ButtonSlewToAltAzAsync.Location = New System.Drawing.Point(201, 443)
        Me.ButtonSlewToAltAzAsync.Name = "ButtonSlewToAltAzAsync"
        Me.ButtonSlewToAltAzAsync.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSlewToAltAzAsync.TabIndex = 12
        Me.ButtonSlewToAltAzAsync.Text = "SlewToAltAzAsync"
        Me.ButtonSlewToAltAzAsync.UseVisualStyleBackColor = True
        '
        'ButtonAtHome
        '
        Me.ButtonAtHome.Location = New System.Drawing.Point(447, 143)
        Me.ButtonAtHome.Name = "ButtonAtHome"
        Me.ButtonAtHome.Size = New System.Drawing.Size(65, 23)
        Me.ButtonAtHome.TabIndex = 13
        Me.ButtonAtHome.Text = "AtHome"
        Me.ButtonAtHome.UseVisualStyleBackColor = True
        '
        'ButtonAtPark
        '
        Me.ButtonAtPark.Location = New System.Drawing.Point(447, 172)
        Me.ButtonAtPark.Name = "ButtonAtPark"
        Me.ButtonAtPark.Size = New System.Drawing.Size(65, 23)
        Me.ButtonAtPark.TabIndex = 14
        Me.ButtonAtPark.Text = "AtPark"
        Me.ButtonAtPark.UseVisualStyleBackColor = True
        '
        'LabelAtHome
        '
        Me.LabelAtHome.Location = New System.Drawing.Point(518, 143)
        Me.LabelAtHome.Name = "LabelAtHome"
        Me.LabelAtHome.Size = New System.Drawing.Size(59, 23)
        Me.LabelAtHome.TabIndex = 15
        Me.LabelAtHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelAtPark
        '
        Me.LabelAtPark.Location = New System.Drawing.Point(518, 172)
        Me.LabelAtPark.Name = "LabelAtPark"
        Me.LabelAtPark.Size = New System.Drawing.Size(59, 23)
        Me.LabelAtPark.TabIndex = 16
        Me.LabelAtPark.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonPark
        '
        Me.ButtonPark.Location = New System.Drawing.Point(201, 137)
        Me.ButtonPark.Name = "ButtonPark"
        Me.ButtonPark.Size = New System.Drawing.Size(65, 23)
        Me.ButtonPark.TabIndex = 17
        Me.ButtonPark.Text = "Park"
        Me.ButtonPark.UseVisualStyleBackColor = True
        '
        'ButtonUnPark
        '
        Me.ButtonUnPark.Location = New System.Drawing.Point(343, 136)
        Me.ButtonUnPark.Name = "ButtonUnPark"
        Me.ButtonUnPark.Size = New System.Drawing.Size(65, 23)
        Me.ButtonUnPark.TabIndex = 18
        Me.ButtonUnPark.Text = "UnPark"
        Me.ButtonUnPark.UseVisualStyleBackColor = True
        '
        'ButtonSetPark
        '
        Me.ButtonSetPark.Location = New System.Drawing.Point(272, 136)
        Me.ButtonSetPark.Name = "ButtonSetPark"
        Me.ButtonSetPark.Size = New System.Drawing.Size(65, 23)
        Me.ButtonSetPark.TabIndex = 19
        Me.ButtonSetPark.Text = "SetPark"
        Me.ButtonSetPark.UseVisualStyleBackColor = True
        '
        'ButtonFindHome
        '
        Me.ButtonFindHome.Location = New System.Drawing.Point(201, 108)
        Me.ButtonFindHome.Name = "ButtonFindHome"
        Me.ButtonFindHome.Size = New System.Drawing.Size(102, 23)
        Me.ButtonFindHome.TabIndex = 21
        Me.ButtonFindHome.Text = "FindHome"
        Me.ButtonFindHome.UseVisualStyleBackColor = True
        '
        'ButtonMoveAxis
        '
        Me.ButtonMoveAxis.Location = New System.Drawing.Point(201, 189)
        Me.ButtonMoveAxis.Name = "ButtonMoveAxis"
        Me.ButtonMoveAxis.Size = New System.Drawing.Size(102, 23)
        Me.ButtonMoveAxis.TabIndex = 22
        Me.ButtonMoveAxis.Text = "MoveAxis"
        Me.ButtonMoveAxis.UseVisualStyleBackColor = True
        '
        'TextBoxAxis
        '
        Me.TextBoxAxis.Location = New System.Drawing.Point(312, 191)
        Me.TextBoxAxis.Name = "TextBoxAxis"
        Me.TextBoxAxis.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxAxis.TabIndex = 23
        '
        'TextBoxRate
        '
        Me.TextBoxRate.Location = New System.Drawing.Point(377, 191)
        Me.TextBoxRate.Name = "TextBoxRate"
        Me.TextBoxRate.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxRate.TabIndex = 24
        '
        'TextBoxDuration
        '
        Me.TextBoxDuration.Enabled = False
        Me.TextBoxDuration.Location = New System.Drawing.Point(377, 249)
        Me.TextBoxDuration.Name = "TextBoxDuration"
        Me.TextBoxDuration.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxDuration.TabIndex = 27
        '
        'TextBoxGuideDirection
        '
        Me.TextBoxGuideDirection.Enabled = False
        Me.TextBoxGuideDirection.Location = New System.Drawing.Point(312, 249)
        Me.TextBoxGuideDirection.Name = "TextBoxGuideDirection"
        Me.TextBoxGuideDirection.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxGuideDirection.TabIndex = 26
        '
        'ButtonPulseGuide
        '
        Me.ButtonPulseGuide.Enabled = False
        Me.ButtonPulseGuide.Location = New System.Drawing.Point(201, 247)
        Me.ButtonPulseGuide.Name = "ButtonPulseGuide"
        Me.ButtonPulseGuide.Size = New System.Drawing.Size(102, 23)
        Me.ButtonPulseGuide.TabIndex = 25
        Me.ButtonPulseGuide.Text = "PulseGuide"
        Me.ButtonPulseGuide.UseVisualStyleBackColor = True
        '
        'ButtonSlewtoCoordinatesAsync
        '
        Me.ButtonSlewtoCoordinatesAsync.Location = New System.Drawing.Point(201, 511)
        Me.ButtonSlewtoCoordinatesAsync.Name = "ButtonSlewtoCoordinatesAsync"
        Me.ButtonSlewtoCoordinatesAsync.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSlewtoCoordinatesAsync.TabIndex = 33
        Me.ButtonSlewtoCoordinatesAsync.Text = "SlewToCrdsAsync"
        Me.ButtonSlewtoCoordinatesAsync.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(374, 482)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 23)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Dec"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(309, 482)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 23)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "RightAsc"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBoxDec
        '
        Me.TextBoxDec.Location = New System.Drawing.Point(377, 513)
        Me.TextBoxDec.Name = "TextBoxDec"
        Me.TextBoxDec.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxDec.TabIndex = 30
        '
        'TextBoxRightAsc
        '
        Me.TextBoxRightAsc.Location = New System.Drawing.Point(309, 513)
        Me.TextBoxRightAsc.Name = "TextBoxRightAsc"
        Me.TextBoxRightAsc.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxRightAsc.TabIndex = 29
        '
        'ButtonSlewToCoordinates
        '
        Me.ButtonSlewToCoordinates.Location = New System.Drawing.Point(201, 482)
        Me.ButtonSlewToCoordinates.Name = "ButtonSlewToCoordinates"
        Me.ButtonSlewToCoordinates.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSlewToCoordinates.TabIndex = 28
        Me.ButtonSlewToCoordinates.Text = "SlewToCrds"
        Me.ButtonSlewToCoordinates.UseVisualStyleBackColor = True
        '
        'ButtonSlewToTargetAsync
        '
        Me.ButtonSlewToTargetAsync.Location = New System.Drawing.Point(583, 201)
        Me.ButtonSlewToTargetAsync.Name = "ButtonSlewToTargetAsync"
        Me.ButtonSlewToTargetAsync.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSlewToTargetAsync.TabIndex = 39
        Me.ButtonSlewToTargetAsync.Text = "SlewToTargetAsync"
        Me.ButtonSlewToTargetAsync.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(756, 144)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label5.Size = New System.Drawing.Size(61, 23)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "Target Dec"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(691, 144)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 23)
        Me.Label6.TabIndex = 37
        Me.Label6.Text = "Target RA"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBoxTargetDec
        '
        Me.TextBoxTargetDec.Location = New System.Drawing.Point(759, 175)
        Me.TextBoxTargetDec.Name = "TextBoxTargetDec"
        Me.TextBoxTargetDec.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxTargetDec.TabIndex = 36
        '
        'TextBoxTargetRA
        '
        Me.TextBoxTargetRA.Location = New System.Drawing.Point(691, 175)
        Me.TextBoxTargetRA.Name = "TextBoxTargetRA"
        Me.TextBoxTargetRA.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxTargetRA.TabIndex = 35
        '
        'ButtonSlewToTarget
        '
        Me.ButtonSlewToTarget.Location = New System.Drawing.Point(583, 172)
        Me.ButtonSlewToTarget.Name = "ButtonSlewToTarget"
        Me.ButtonSlewToTarget.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSlewToTarget.TabIndex = 34
        Me.ButtonSlewToTarget.Text = "SlewToTarget"
        Me.ButtonSlewToTarget.UseVisualStyleBackColor = True
        '
        'ButtonSyncToAltAz
        '
        Me.ButtonSyncToAltAz.Location = New System.Drawing.Point(583, 253)
        Me.ButtonSyncToAltAz.Name = "ButtonSyncToAltAz"
        Me.ButtonSyncToAltAz.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSyncToAltAz.TabIndex = 40
        Me.ButtonSyncToAltAz.Text = "SyncToAltAz"
        Me.ButtonSyncToAltAz.UseVisualStyleBackColor = True
        '
        'ButtonSyncToCoordinates
        '
        Me.ButtonSyncToCoordinates.Location = New System.Drawing.Point(583, 282)
        Me.ButtonSyncToCoordinates.Name = "ButtonSyncToCoordinates"
        Me.ButtonSyncToCoordinates.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSyncToCoordinates.TabIndex = 43
        Me.ButtonSyncToCoordinates.Text = "SyncToCrds"
        Me.ButtonSyncToCoordinates.UseVisualStyleBackColor = True
        '
        'ButtonSyncToTarget
        '
        Me.ButtonSyncToTarget.Location = New System.Drawing.Point(583, 311)
        Me.ButtonSyncToTarget.Name = "ButtonSyncToTarget"
        Me.ButtonSyncToTarget.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSyncToTarget.TabIndex = 46
        Me.ButtonSyncToTarget.Text = "SyncToTarget"
        Me.ButtonSyncToTarget.UseVisualStyleBackColor = True
        '
        'ButtonGetTarget
        '
        Me.ButtonGetTarget.Location = New System.Drawing.Point(583, 143)
        Me.ButtonGetTarget.Name = "ButtonGetTarget"
        Me.ButtonGetTarget.Size = New System.Drawing.Size(47, 23)
        Me.ButtonGetTarget.TabIndex = 49
        Me.ButtonGetTarget.Text = "Get"
        Me.ButtonGetTarget.UseVisualStyleBackColor = True
        '
        'ButtonSetTarget
        '
        Me.ButtonSetTarget.Location = New System.Drawing.Point(636, 143)
        Me.ButtonSetTarget.Name = "ButtonSetTarget"
        Me.ButtonSetTarget.Size = New System.Drawing.Size(49, 23)
        Me.ButtonSetTarget.TabIndex = 50
        Me.ButtonSetTarget.Text = "Set"
        Me.ButtonSetTarget.UseVisualStyleBackColor = True
        '
        'ButtonSetSiteData
        '
        Me.ButtonSetSiteData.Location = New System.Drawing.Point(440, 38)
        Me.ButtonSetSiteData.Name = "ButtonSetSiteData"
        Me.ButtonSetSiteData.Size = New System.Drawing.Size(36, 20)
        Me.ButtonSetSiteData.TabIndex = 53
        Me.ButtonSetSiteData.Text = "Set"
        Me.ButtonSetSiteData.UseVisualStyleBackColor = True
        '
        'TextBoxSiteLatitude
        '
        Me.TextBoxSiteLatitude.Location = New System.Drawing.Point(524, 38)
        Me.TextBoxSiteLatitude.Name = "TextBoxSiteLatitude"
        Me.TextBoxSiteLatitude.Size = New System.Drawing.Size(55, 20)
        Me.TextBoxSiteLatitude.TabIndex = 54
        '
        'TextBoxSiteLongitude
        '
        Me.TextBoxSiteLongitude.Location = New System.Drawing.Point(589, 38)
        Me.TextBoxSiteLongitude.Name = "TextBoxSiteLongitude"
        Me.TextBoxSiteLongitude.Size = New System.Drawing.Size(55, 20)
        Me.TextBoxSiteLongitude.TabIndex = 55
        '
        'ButtonIsPulseGuiding
        '
        Me.ButtonIsPulseGuiding.Enabled = False
        Me.ButtonIsPulseGuiding.Location = New System.Drawing.Point(447, 201)
        Me.ButtonIsPulseGuiding.Name = "ButtonIsPulseGuiding"
        Me.ButtonIsPulseGuiding.Size = New System.Drawing.Size(65, 22)
        Me.ButtonIsPulseGuiding.TabIndex = 56
        Me.ButtonIsPulseGuiding.Text = "isPulseGuiding"
        Me.ButtonIsPulseGuiding.UseVisualStyleBackColor = True
        '
        'LabelIsPulseGuiding
        '
        Me.LabelIsPulseGuiding.Location = New System.Drawing.Point(518, 201)
        Me.LabelIsPulseGuiding.Name = "LabelIsPulseGuiding"
        Me.LabelIsPulseGuiding.Size = New System.Drawing.Size(59, 23)
        Me.LabelIsPulseGuiding.TabIndex = 57
        Me.LabelIsPulseGuiding.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelIsTracking
        '
        Me.LabelIsTracking.Location = New System.Drawing.Point(518, 229)
        Me.LabelIsTracking.Name = "LabelIsTracking"
        Me.LabelIsTracking.Size = New System.Drawing.Size(59, 23)
        Me.LabelIsTracking.TabIndex = 59
        Me.LabelIsTracking.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonIsTracking
        '
        Me.ButtonIsTracking.Location = New System.Drawing.Point(447, 229)
        Me.ButtonIsTracking.Name = "ButtonIsTracking"
        Me.ButtonIsTracking.Size = New System.Drawing.Size(65, 22)
        Me.ButtonIsTracking.TabIndex = 58
        Me.ButtonIsTracking.Text = "isTracking"
        Me.ButtonIsTracking.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(654, 462)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 23)
        Me.Label7.TabIndex = 61
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonTrackingEnable
        '
        Me.ButtonTrackingEnable.Location = New System.Drawing.Point(583, 462)
        Me.ButtonTrackingEnable.Name = "ButtonTrackingEnable"
        Me.ButtonTrackingEnable.Size = New System.Drawing.Size(65, 37)
        Me.ButtonTrackingEnable.TabIndex = 60
        Me.ButtonTrackingEnable.Text = "Tracking Enable"
        Me.ButtonTrackingEnable.UseVisualStyleBackColor = True
        '
        'ButtonTrackingDisable
        '
        Me.ButtonTrackingDisable.Location = New System.Drawing.Point(648, 462)
        Me.ButtonTrackingDisable.Name = "ButtonTrackingDisable"
        Me.ButtonTrackingDisable.Size = New System.Drawing.Size(65, 37)
        Me.ButtonTrackingDisable.TabIndex = 62
        Me.ButtonTrackingDisable.Text = "Tracking Disable"
        Me.ButtonTrackingDisable.UseVisualStyleBackColor = True
        '
        'LabelIsSlewing
        '
        Me.LabelIsSlewing.Location = New System.Drawing.Point(518, 257)
        Me.LabelIsSlewing.Name = "LabelIsSlewing"
        Me.LabelIsSlewing.Size = New System.Drawing.Size(59, 23)
        Me.LabelIsSlewing.TabIndex = 64
        Me.LabelIsSlewing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonIsSlewing
        '
        Me.ButtonIsSlewing.Location = New System.Drawing.Point(447, 257)
        Me.ButtonIsSlewing.Name = "ButtonIsSlewing"
        Me.ButtonIsSlewing.Size = New System.Drawing.Size(65, 22)
        Me.ButtonIsSlewing.TabIndex = 63
        Me.ButtonIsSlewing.Text = "isSlewing"
        Me.ButtonIsSlewing.UseVisualStyleBackColor = True
        '
        'ButtonRaDecRateSet
        '
        Me.ButtonRaDecRateSet.Location = New System.Drawing.Point(201, 331)
        Me.ButtonRaDecRateSet.Name = "ButtonRaDecRateSet"
        Me.ButtonRaDecRateSet.Size = New System.Drawing.Size(102, 37)
        Me.ButtonRaDecRateSet.TabIndex = 69
        Me.ButtonRaDecRateSet.Text = "GuideRate RA/DEC set"
        Me.ButtonRaDecRateSet.UseVisualStyleBackColor = True
        '
        'ButtonRaDecRateGet
        '
        Me.ButtonRaDecRateGet.Location = New System.Drawing.Point(201, 288)
        Me.ButtonRaDecRateGet.Name = "ButtonRaDecRateGet"
        Me.ButtonRaDecRateGet.Size = New System.Drawing.Size(102, 37)
        Me.ButtonRaDecRateGet.TabIndex = 67
        Me.ButtonRaDecRateGet.Text = "GuideRate RA/DEC get"
        Me.ButtonRaDecRateGet.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(381, 278)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 23)
        Me.Label8.TabIndex = 71
        Me.Label8.Text = "Dec"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(313, 279)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 23)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "RightAsc"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBoxDecRate
        '
        Me.TextBoxDecRate.Location = New System.Drawing.Point(380, 305)
        Me.TextBoxDecRate.Name = "TextBoxDecRate"
        Me.TextBoxDecRate.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxDecRate.TabIndex = 73
        '
        'TextBoxRightAscRate
        '
        Me.TextBoxRightAscRate.Location = New System.Drawing.Point(312, 305)
        Me.TextBoxRightAscRate.Name = "TextBoxRightAscRate"
        Me.TextBoxRightAscRate.Size = New System.Drawing.Size(58, 20)
        Me.TextBoxRightAscRate.TabIndex = 72
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(377, 165)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 23)
        Me.Label10.TabIndex = 75
        Me.Label10.Text = "Rate"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(312, 165)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 23)
        Me.Label11.TabIndex = 74
        Me.Label11.Text = "Axis"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(377, 223)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(58, 23)
        Me.Label12.TabIndex = 77
        Me.Label12.Text = "Duration"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(312, 223)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 23)
        Me.Label13.TabIndex = 76
        Me.Label13.Text = "Direction"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(586, 12)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 23)
        Me.Label14.TabIndex = 79
        Me.Label14.Text = "Longitude"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(521, 12)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(58, 23)
        Me.Label15.TabIndex = 78
        Me.Label15.Text = "Latitude"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonReadCapabilities
        '
        Me.ButtonReadCapabilities.Location = New System.Drawing.Point(12, 79)
        Me.ButtonReadCapabilities.Name = "ButtonReadCapabilities"
        Me.ButtonReadCapabilities.Size = New System.Drawing.Size(100, 23)
        Me.ButtonReadCapabilities.TabIndex = 80
        Me.ButtonReadCapabilities.Text = "Read Capabilities"
        Me.ButtonReadCapabilities.UseVisualStyleBackColor = True
        '
        'CheckBoxCanFindHome
        '
        Me.CheckBoxCanFindHome.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanFindHome.Enabled = False
        Me.CheckBoxCanFindHome.Location = New System.Drawing.Point(12, 115)
        Me.CheckBoxCanFindHome.Name = "CheckBoxCanFindHome"
        Me.CheckBoxCanFindHome.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanFindHome.TabIndex = 81
        Me.CheckBoxCanFindHome.Text = "CanFindHome"
        Me.CheckBoxCanFindHome.UseVisualStyleBackColor = True
        '
        'CheckBoxCanPark
        '
        Me.CheckBoxCanPark.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanPark.Enabled = False
        Me.CheckBoxCanPark.Location = New System.Drawing.Point(12, 138)
        Me.CheckBoxCanPark.Name = "CheckBoxCanPark"
        Me.CheckBoxCanPark.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanPark.TabIndex = 82
        Me.CheckBoxCanPark.Text = "CanPark"
        Me.CheckBoxCanPark.UseVisualStyleBackColor = True
        '
        'CheckBoxCanPulseGuide
        '
        Me.CheckBoxCanPulseGuide.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanPulseGuide.Enabled = False
        Me.CheckBoxCanPulseGuide.Location = New System.Drawing.Point(12, 161)
        Me.CheckBoxCanPulseGuide.Name = "CheckBoxCanPulseGuide"
        Me.CheckBoxCanPulseGuide.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanPulseGuide.TabIndex = 83
        Me.CheckBoxCanPulseGuide.Text = "CanPulseGuide"
        Me.CheckBoxCanPulseGuide.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSetDeclinationRate
        '
        Me.CheckBoxCanSetDeclinationRate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSetDeclinationRate.Enabled = False
        Me.CheckBoxCanSetDeclinationRate.Location = New System.Drawing.Point(12, 184)
        Me.CheckBoxCanSetDeclinationRate.Name = "CheckBoxCanSetDeclinationRate"
        Me.CheckBoxCanSetDeclinationRate.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSetDeclinationRate.TabIndex = 84
        Me.CheckBoxCanSetDeclinationRate.Text = "CanSetDeclinationRate"
        Me.CheckBoxCanSetDeclinationRate.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSetGuideRates
        '
        Me.CheckBoxCanSetGuideRates.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSetGuideRates.Enabled = False
        Me.CheckBoxCanSetGuideRates.Location = New System.Drawing.Point(12, 207)
        Me.CheckBoxCanSetGuideRates.Name = "CheckBoxCanSetGuideRates"
        Me.CheckBoxCanSetGuideRates.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSetGuideRates.TabIndex = 85
        Me.CheckBoxCanSetGuideRates.Text = "CanSetGuideRates"
        Me.CheckBoxCanSetGuideRates.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSetPark
        '
        Me.CheckBoxCanSetPark.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSetPark.Enabled = False
        Me.CheckBoxCanSetPark.Location = New System.Drawing.Point(12, 230)
        Me.CheckBoxCanSetPark.Name = "CheckBoxCanSetPark"
        Me.CheckBoxCanSetPark.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSetPark.TabIndex = 86
        Me.CheckBoxCanSetPark.Text = "CanSetPark"
        Me.CheckBoxCanSetPark.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSetPierSide
        '
        Me.CheckBoxCanSetPierSide.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSetPierSide.Enabled = False
        Me.CheckBoxCanSetPierSide.Location = New System.Drawing.Point(12, 253)
        Me.CheckBoxCanSetPierSide.Name = "CheckBoxCanSetPierSide"
        Me.CheckBoxCanSetPierSide.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSetPierSide.TabIndex = 87
        Me.CheckBoxCanSetPierSide.Text = "CanSetPierSide"
        Me.CheckBoxCanSetPierSide.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSetRightAscensionRate
        '
        Me.CheckBoxCanSetRightAscensionRate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSetRightAscensionRate.Enabled = False
        Me.CheckBoxCanSetRightAscensionRate.Location = New System.Drawing.Point(12, 276)
        Me.CheckBoxCanSetRightAscensionRate.Name = "CheckBoxCanSetRightAscensionRate"
        Me.CheckBoxCanSetRightAscensionRate.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSetRightAscensionRate.TabIndex = 88
        Me.CheckBoxCanSetRightAscensionRate.Text = "CanSetRightAscensionRate"
        Me.CheckBoxCanSetRightAscensionRate.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSetTracking
        '
        Me.CheckBoxCanSetTracking.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSetTracking.Enabled = False
        Me.CheckBoxCanSetTracking.Location = New System.Drawing.Point(12, 299)
        Me.CheckBoxCanSetTracking.Name = "CheckBoxCanSetTracking"
        Me.CheckBoxCanSetTracking.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSetTracking.TabIndex = 89
        Me.CheckBoxCanSetTracking.Text = "CanSetTracking"
        Me.CheckBoxCanSetTracking.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSlew
        '
        Me.CheckBoxCanSlew.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSlew.Enabled = False
        Me.CheckBoxCanSlew.Location = New System.Drawing.Point(12, 322)
        Me.CheckBoxCanSlew.Name = "CheckBoxCanSlew"
        Me.CheckBoxCanSlew.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSlew.TabIndex = 90
        Me.CheckBoxCanSlew.Text = "CheckBoxCanSlew"
        Me.CheckBoxCanSlew.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSlewAltAz
        '
        Me.CheckBoxCanSlewAltAz.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSlewAltAz.Enabled = False
        Me.CheckBoxCanSlewAltAz.Location = New System.Drawing.Point(12, 345)
        Me.CheckBoxCanSlewAltAz.Name = "CheckBoxCanSlewAltAz"
        Me.CheckBoxCanSlewAltAz.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSlewAltAz.TabIndex = 91
        Me.CheckBoxCanSlewAltAz.Text = "CanSlewAltAz"
        Me.CheckBoxCanSlewAltAz.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSlewAltAzAsync
        '
        Me.CheckBoxCanSlewAltAzAsync.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSlewAltAzAsync.Enabled = False
        Me.CheckBoxCanSlewAltAzAsync.Location = New System.Drawing.Point(12, 368)
        Me.CheckBoxCanSlewAltAzAsync.Name = "CheckBoxCanSlewAltAzAsync"
        Me.CheckBoxCanSlewAltAzAsync.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSlewAltAzAsync.TabIndex = 92
        Me.CheckBoxCanSlewAltAzAsync.Text = "CanSlewAltAzAsync"
        Me.CheckBoxCanSlewAltAzAsync.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSlewAsync
        '
        Me.CheckBoxCanSlewAsync.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSlewAsync.Enabled = False
        Me.CheckBoxCanSlewAsync.Location = New System.Drawing.Point(12, 391)
        Me.CheckBoxCanSlewAsync.Name = "CheckBoxCanSlewAsync"
        Me.CheckBoxCanSlewAsync.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSlewAsync.TabIndex = 93
        Me.CheckBoxCanSlewAsync.Text = "CanSlewAsync"
        Me.CheckBoxCanSlewAsync.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSync
        '
        Me.CheckBoxCanSync.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSync.Enabled = False
        Me.CheckBoxCanSync.Location = New System.Drawing.Point(12, 414)
        Me.CheckBoxCanSync.Name = "CheckBoxCanSync"
        Me.CheckBoxCanSync.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSync.TabIndex = 94
        Me.CheckBoxCanSync.Text = "CanSync"
        Me.CheckBoxCanSync.UseVisualStyleBackColor = True
        '
        'CheckBoxCanSyncAltAz
        '
        Me.CheckBoxCanSyncAltAz.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanSyncAltAz.Enabled = False
        Me.CheckBoxCanSyncAltAz.Location = New System.Drawing.Point(12, 437)
        Me.CheckBoxCanSyncAltAz.Name = "CheckBoxCanSyncAltAz"
        Me.CheckBoxCanSyncAltAz.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanSyncAltAz.TabIndex = 95
        Me.CheckBoxCanSyncAltAz.Text = "CanSyncAltAz"
        Me.CheckBoxCanSyncAltAz.UseVisualStyleBackColor = True
        '
        'CheckBoxCanUnpark
        '
        Me.CheckBoxCanUnpark.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxCanUnpark.Enabled = False
        Me.CheckBoxCanUnpark.Location = New System.Drawing.Point(12, 460)
        Me.CheckBoxCanUnpark.Name = "CheckBoxCanUnpark"
        Me.CheckBoxCanUnpark.Size = New System.Drawing.Size(158, 17)
        Me.CheckBoxCanUnpark.TabIndex = 96
        Me.CheckBoxCanUnpark.Text = "CanUnpark"
        Me.CheckBoxCanUnpark.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(650, 12)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(58, 23)
        Me.Label16.TabIndex = 98
        Me.Label16.Text = "Elevation"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBoxSiteElevation
        '
        Me.TextBoxSiteElevation.Location = New System.Drawing.Point(653, 38)
        Me.TextBoxSiteElevation.Name = "TextBoxSiteElevation"
        Me.TextBoxSiteElevation.Size = New System.Drawing.Size(55, 20)
        Me.TextBoxSiteElevation.TabIndex = 97
        '
        'ButtonGetSiteData
        '
        Me.ButtonGetSiteData.Location = New System.Drawing.Point(482, 38)
        Me.ButtonGetSiteData.Name = "ButtonGetSiteData"
        Me.ButtonGetSiteData.Size = New System.Drawing.Size(36, 20)
        Me.ButtonGetSiteData.TabIndex = 99
        Me.ButtonGetSiteData.Text = "Get"
        Me.ButtonGetSiteData.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(440, 12)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(84, 23)
        Me.Label17.TabIndex = 100
        Me.Label17.Text = "Site Information"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(440, 64)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(78, 49)
        Me.Button1.TabIndex = 101
        Me.Button1.Text = "Get Telescope Alignment"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(589, 64)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 23)
        Me.Label18.TabIndex = 103
        Me.Label18.Text = "Az"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(524, 64)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(58, 23)
        Me.Label19.TabIndex = 102
        Me.Label19.Text = "Alt"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(715, 64)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(58, 23)
        Me.Label20.TabIndex = 105
        Me.Label20.Text = "Dec"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(650, 64)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 23)
        Me.Label21.TabIndex = 104
        Me.Label21.Text = "Ra"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelDec
        '
        Me.LabelDec.Location = New System.Drawing.Point(715, 90)
        Me.LabelDec.Name = "LabelDec"
        Me.LabelDec.Size = New System.Drawing.Size(58, 23)
        Me.LabelDec.TabIndex = 109
        Me.LabelDec.Text = "Dec"
        Me.LabelDec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelRa
        '
        Me.LabelRa.Location = New System.Drawing.Point(650, 90)
        Me.LabelRa.Name = "LabelRa"
        Me.LabelRa.Size = New System.Drawing.Size(58, 23)
        Me.LabelRa.TabIndex = 108
        Me.LabelRa.Text = "Ra"
        Me.LabelRa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelAz
        '
        Me.LabelAz.Location = New System.Drawing.Point(589, 90)
        Me.LabelAz.Name = "LabelAz"
        Me.LabelAz.Size = New System.Drawing.Size(58, 23)
        Me.LabelAz.TabIndex = 107
        Me.LabelAz.Text = "Az"
        Me.LabelAz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelAlt
        '
        Me.LabelAlt.Location = New System.Drawing.Point(524, 90)
        Me.LabelAlt.Name = "LabelAlt"
        Me.LabelAlt.Size = New System.Drawing.Size(58, 23)
        Me.LabelAlt.TabIndex = 106
        Me.LabelAlt.Text = "Alt"
        Me.LabelAlt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonGetTrackingRates
        '
        Me.ButtonGetTrackingRates.Location = New System.Drawing.Point(583, 351)
        Me.ButtonGetTrackingRates.Name = "ButtonGetTrackingRates"
        Me.ButtonGetTrackingRates.Size = New System.Drawing.Size(102, 43)
        Me.ButtonGetTrackingRates.TabIndex = 113
        Me.ButtonGetTrackingRates.Text = "List Supported Tracking Rates"
        Me.ButtonGetTrackingRates.UseVisualStyleBackColor = True
        '
        'ListBoxTrackingRates
        '
        Me.ListBoxTrackingRates.FormattingEnabled = True
        Me.ListBoxTrackingRates.Location = New System.Drawing.Point(694, 351)
        Me.ListBoxTrackingRates.Name = "ListBoxTrackingRates"
        Me.ListBoxTrackingRates.Size = New System.Drawing.Size(123, 69)
        Me.ListBoxTrackingRates.TabIndex = 114
        '
        'ButtonGetTrackingRate
        '
        Me.ButtonGetTrackingRate.Location = New System.Drawing.Point(636, 400)
        Me.ButtonGetTrackingRate.Name = "ButtonGetTrackingRate"
        Me.ButtonGetTrackingRate.Size = New System.Drawing.Size(49, 20)
        Me.ButtonGetTrackingRate.TabIndex = 116
        Me.ButtonGetTrackingRate.Text = "Get"
        Me.ButtonGetTrackingRate.UseVisualStyleBackColor = True
        '
        'ButtonSetTrackingRate
        '
        Me.ButtonSetTrackingRate.Location = New System.Drawing.Point(583, 400)
        Me.ButtonSetTrackingRate.Name = "ButtonSetTrackingRate"
        Me.ButtonSetTrackingRate.Size = New System.Drawing.Size(49, 20)
        Me.ButtonSetTrackingRate.TabIndex = 115
        Me.ButtonSetTrackingRate.Text = "Set"
        Me.ButtonSetTrackingRate.UseVisualStyleBackColor = True
        '
        'ButtonGetEnumerator
        '
        Me.ButtonGetEnumerator.Location = New System.Drawing.Point(583, 426)
        Me.ButtonGetEnumerator.Name = "ButtonGetEnumerator"
        Me.ButtonGetEnumerator.Size = New System.Drawing.Size(102, 20)
        Me.ButtonGetEnumerator.TabIndex = 117
        Me.ButtonGetEnumerator.Text = "Get Enum"
        Me.ButtonGetEnumerator.UseVisualStyleBackColor = True
        '
        'LabelSiderealTime
        '
        Me.LabelSiderealTime.Location = New System.Drawing.Point(691, 505)
        Me.LabelSiderealTime.Name = "LabelSiderealTime"
        Me.LabelSiderealTime.Size = New System.Drawing.Size(126, 23)
        Me.LabelSiderealTime.TabIndex = 119
        Me.LabelSiderealTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonSiderealTime
        '
        Me.ButtonSiderealTime.Location = New System.Drawing.Point(583, 505)
        Me.ButtonSiderealTime.Name = "ButtonSiderealTime"
        Me.ButtonSiderealTime.Size = New System.Drawing.Size(102, 23)
        Me.ButtonSiderealTime.TabIndex = 118
        Me.ButtonSiderealTime.Text = "SiderealTime"
        Me.ButtonSiderealTime.UseVisualStyleBackColor = True
        '
        'ListBoxSupportedActions
        '
        Me.ListBoxSupportedActions.FormattingEnabled = True
        Me.ListBoxSupportedActions.Location = New System.Drawing.Point(201, 566)
        Me.ListBoxSupportedActions.Name = "ListBoxSupportedActions"
        Me.ListBoxSupportedActions.Size = New System.Drawing.Size(225, 95)
        Me.ListBoxSupportedActions.TabIndex = 120
        '
        'ButtonAction
        '
        Me.ButtonAction.Location = New System.Drawing.Point(279, 667)
        Me.ButtonAction.Name = "ButtonAction"
        Me.ButtonAction.Size = New System.Drawing.Size(90, 22)
        Me.ButtonAction.TabIndex = 122
        Me.ButtonAction.Text = "execute action"
        Me.ButtonAction.UseVisualStyleBackColor = True
        '
        'ButtonGetSupportedActions
        '
        Me.ButtonGetSupportedActions.Location = New System.Drawing.Point(201, 667)
        Me.ButtonGetSupportedActions.Name = "ButtonGetSupportedActions"
        Me.ButtonGetSupportedActions.Size = New System.Drawing.Size(72, 22)
        Me.ButtonGetSupportedActions.TabIndex = 121
        Me.ButtonGetSupportedActions.Text = "load actions"
        Me.ButtonGetSupportedActions.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(830, 829)
        Me.Controls.Add(Me.ButtonAction)
        Me.Controls.Add(Me.ButtonGetSupportedActions)
        Me.Controls.Add(Me.ListBoxSupportedActions)
        Me.Controls.Add(Me.LabelSiderealTime)
        Me.Controls.Add(Me.ButtonSiderealTime)
        Me.Controls.Add(Me.ButtonGetEnumerator)
        Me.Controls.Add(Me.ButtonGetTrackingRate)
        Me.Controls.Add(Me.ButtonSetTrackingRate)
        Me.Controls.Add(Me.ListBoxTrackingRates)
        Me.Controls.Add(Me.ButtonGetTrackingRates)
        Me.Controls.Add(Me.LabelDec)
        Me.Controls.Add(Me.LabelRa)
        Me.Controls.Add(Me.LabelAz)
        Me.Controls.Add(Me.LabelAlt)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.ButtonGetSiteData)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.TextBoxSiteElevation)
        Me.Controls.Add(Me.CheckBoxCanUnpark)
        Me.Controls.Add(Me.CheckBoxCanSyncAltAz)
        Me.Controls.Add(Me.CheckBoxCanSync)
        Me.Controls.Add(Me.CheckBoxCanSlewAsync)
        Me.Controls.Add(Me.CheckBoxCanSlewAltAzAsync)
        Me.Controls.Add(Me.CheckBoxCanSlewAltAz)
        Me.Controls.Add(Me.CheckBoxCanSlew)
        Me.Controls.Add(Me.CheckBoxCanSetTracking)
        Me.Controls.Add(Me.CheckBoxCanSetRightAscensionRate)
        Me.Controls.Add(Me.CheckBoxCanSetPierSide)
        Me.Controls.Add(Me.CheckBoxCanSetPark)
        Me.Controls.Add(Me.CheckBoxCanSetGuideRates)
        Me.Controls.Add(Me.CheckBoxCanSetDeclinationRate)
        Me.Controls.Add(Me.CheckBoxCanPulseGuide)
        Me.Controls.Add(Me.CheckBoxCanPark)
        Me.Controls.Add(Me.CheckBoxCanFindHome)
        Me.Controls.Add(Me.ButtonReadCapabilities)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TextBoxDecRate)
        Me.Controls.Add(Me.TextBoxRightAscRate)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.ButtonRaDecRateSet)
        Me.Controls.Add(Me.ButtonRaDecRateGet)
        Me.Controls.Add(Me.LabelIsSlewing)
        Me.Controls.Add(Me.ButtonIsSlewing)
        Me.Controls.Add(Me.ButtonTrackingDisable)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.ButtonTrackingEnable)
        Me.Controls.Add(Me.LabelIsTracking)
        Me.Controls.Add(Me.ButtonIsTracking)
        Me.Controls.Add(Me.LabelIsPulseGuiding)
        Me.Controls.Add(Me.ButtonIsPulseGuiding)
        Me.Controls.Add(Me.TextBoxSiteLongitude)
        Me.Controls.Add(Me.TextBoxSiteLatitude)
        Me.Controls.Add(Me.ButtonSetSiteData)
        Me.Controls.Add(Me.ButtonSetTarget)
        Me.Controls.Add(Me.ButtonGetTarget)
        Me.Controls.Add(Me.ButtonSyncToTarget)
        Me.Controls.Add(Me.ButtonSyncToCoordinates)
        Me.Controls.Add(Me.ButtonSyncToAltAz)
        Me.Controls.Add(Me.ButtonSlewToTargetAsync)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TextBoxTargetDec)
        Me.Controls.Add(Me.TextBoxTargetRA)
        Me.Controls.Add(Me.ButtonSlewToTarget)
        Me.Controls.Add(Me.ButtonSlewtoCoordinatesAsync)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBoxDec)
        Me.Controls.Add(Me.TextBoxRightAsc)
        Me.Controls.Add(Me.ButtonSlewToCoordinates)
        Me.Controls.Add(Me.TextBoxDuration)
        Me.Controls.Add(Me.TextBoxGuideDirection)
        Me.Controls.Add(Me.ButtonPulseGuide)
        Me.Controls.Add(Me.TextBoxRate)
        Me.Controls.Add(Me.TextBoxAxis)
        Me.Controls.Add(Me.ButtonMoveAxis)
        Me.Controls.Add(Me.ButtonFindHome)
        Me.Controls.Add(Me.ButtonSetPark)
        Me.Controls.Add(Me.ButtonUnPark)
        Me.Controls.Add(Me.ButtonPark)
        Me.Controls.Add(Me.LabelAtPark)
        Me.Controls.Add(Me.LabelAtHome)
        Me.Controls.Add(Me.ButtonAtPark)
        Me.Controls.Add(Me.ButtonAtHome)
        Me.Controls.Add(Me.ButtonSlewToAltAzAsync)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxAzimuth)
        Me.Controls.Add(Me.TextBoxAltitude)
        Me.Controls.Add(Me.ButtonSlewToAltAz)
        Me.Controls.Add(Me.ButtonAbortSlew)
        Me.Controls.Add(Me.labelDriverId)
        Me.Controls.Add(Me.buttonConnect)
        Me.Controls.Add(Me.buttonChoose)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents labelDriverId As System.Windows.Forms.Label
    Private WithEvents buttonConnect As System.Windows.Forms.Button
    Private WithEvents buttonChoose As System.Windows.Forms.Button
    Friend WithEvents ButtonAbortSlew As System.Windows.Forms.Button
    Friend WithEvents ButtonSlewToAltAz As System.Windows.Forms.Button
    Friend WithEvents TextBoxAltitude As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxAzimuth As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ButtonSlewToAltAzAsync As System.Windows.Forms.Button
    Friend WithEvents ButtonAtHome As System.Windows.Forms.Button
    Friend WithEvents ButtonAtPark As System.Windows.Forms.Button
    Friend WithEvents LabelAtHome As System.Windows.Forms.Label
    Friend WithEvents LabelAtPark As System.Windows.Forms.Label
    Friend WithEvents ButtonPark As System.Windows.Forms.Button
    Friend WithEvents ButtonUnPark As System.Windows.Forms.Button
    Friend WithEvents ButtonSetPark As System.Windows.Forms.Button
    Friend WithEvents ButtonFindHome As System.Windows.Forms.Button
    Friend WithEvents ButtonMoveAxis As System.Windows.Forms.Button
    Friend WithEvents TextBoxAxis As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxRate As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDuration As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxGuideDirection As System.Windows.Forms.TextBox
    Friend WithEvents ButtonPulseGuide As System.Windows.Forms.Button
    Friend WithEvents ButtonSlewtoCoordinatesAsync As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBoxDec As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxRightAsc As System.Windows.Forms.TextBox
    Friend WithEvents ButtonSlewToCoordinates As System.Windows.Forms.Button
    Friend WithEvents ButtonSlewToTargetAsync As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxTargetDec As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxTargetRA As System.Windows.Forms.TextBox
    Friend WithEvents ButtonSlewToTarget As System.Windows.Forms.Button
    Friend WithEvents ButtonSyncToAltAz As System.Windows.Forms.Button
    Friend WithEvents ButtonSyncToCoordinates As System.Windows.Forms.Button
    Friend WithEvents ButtonSyncToTarget As System.Windows.Forms.Button
    Friend WithEvents ButtonGetTarget As System.Windows.Forms.Button
    Friend WithEvents ButtonSetTarget As System.Windows.Forms.Button
    Friend WithEvents ButtonSetSiteData As System.Windows.Forms.Button
    Friend WithEvents TextBoxSiteLatitude As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxSiteLongitude As System.Windows.Forms.TextBox
    Friend WithEvents ButtonIsPulseGuiding As System.Windows.Forms.Button
    Friend WithEvents LabelIsPulseGuiding As System.Windows.Forms.Label
    Friend WithEvents LabelIsTracking As System.Windows.Forms.Label
    Friend WithEvents ButtonIsTracking As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ButtonTrackingEnable As System.Windows.Forms.Button
    Friend WithEvents ButtonTrackingDisable As System.Windows.Forms.Button
    Friend WithEvents LabelIsSlewing As System.Windows.Forms.Label
    Friend WithEvents ButtonIsSlewing As System.Windows.Forms.Button
    Friend WithEvents ButtonRaDecRateSet As System.Windows.Forms.Button
    Friend WithEvents ButtonRaDecRateGet As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBoxDecRate As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxRightAscRate As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Private WithEvents ButtonReadCapabilities As System.Windows.Forms.Button
    Friend WithEvents CheckBoxCanFindHome As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanPark As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanPulseGuide As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSetDeclinationRate As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSetGuideRates As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSetPark As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSetPierSide As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSetRightAscensionRate As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSetTracking As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSlew As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSlewAltAz As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSlewAltAzAsync As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSlewAsync As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSync As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanSyncAltAz As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxCanUnpark As System.Windows.Forms.CheckBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBoxSiteElevation As System.Windows.Forms.TextBox
    Friend WithEvents ButtonGetSiteData As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents LabelDec As System.Windows.Forms.Label
    Friend WithEvents LabelRa As System.Windows.Forms.Label
    Friend WithEvents LabelAz As System.Windows.Forms.Label
    Friend WithEvents LabelAlt As System.Windows.Forms.Label
    Friend WithEvents ButtonGetTrackingRates As System.Windows.Forms.Button
    Friend WithEvents ListBoxTrackingRates As System.Windows.Forms.ListBox
    Friend WithEvents ButtonGetTrackingRate As System.Windows.Forms.Button
    Friend WithEvents ButtonSetTrackingRate As System.Windows.Forms.Button
    Friend WithEvents ButtonGetEnumerator As System.Windows.Forms.Button
    Friend WithEvents LabelSiderealTime As System.Windows.Forms.Label
    Friend WithEvents ButtonSiderealTime As System.Windows.Forms.Button
    Friend WithEvents ListBoxSupportedActions As System.Windows.Forms.ListBox
    Private WithEvents ButtonAction As System.Windows.Forms.Button
    Private WithEvents ButtonGetSupportedActions As System.Windows.Forms.Button

End Class
