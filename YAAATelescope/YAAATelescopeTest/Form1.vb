﻿Imports ASCOM.DeviceInterface

Public Class Form1

    Private driver As ASCOM.DriverAccess.Telescope
    Private nfi_ As System.Globalization.NumberFormatInfo = New System.Globalization.CultureInfo("en-US", False).NumberFormat

    ''' <summary>
    ''' This event is where the driver is choosen. The device ID will be saved in the settings.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub buttonChoose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonChoose.Click
        My.Settings.DriverId = ASCOM.DriverAccess.Telescope.Choose(My.Settings.DriverId)
        SetUIState()
    End Sub

    ''' <summary>
    ''' Connects to the device to be tested.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub buttonConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonConnect.Click
        If (IsConnected) Then
            driver.Connected = False
        Else
            driver = New ASCOM.DriverAccess.Telescope(My.Settings.DriverId)
            driver.Connected = True
        End If
        SetUIState()
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If IsConnected Then
            driver.Connected = False
        End If
        ' the settings are saved automatically when this application is closed.
    End Sub

    ''' <summary>
    ''' Sets the state of the UI depending on the device state
    ''' </summary>
    Private Sub SetUIState()
        buttonConnect.Enabled = Not String.IsNullOrEmpty(My.Settings.DriverId)
        buttonChoose.Enabled = Not IsConnected
        buttonConnect.Text = IIf(IsConnected, "Disconnect", "Connect")
    End Sub

    ''' <summary>
    ''' Gets a value indicating whether this instance is connected.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>true</c> if this instance is connected; otherwise, <c>false</c>.
    ''' 
    ''' </value>
    Private ReadOnly Property IsConnected() As Boolean
        Get
            If Me.driver Is Nothing Then Return False
            Return driver.Connected
        End Get
    End Property

    ' TODO: Add additional UI and controls to test more of the driver being tested.

    Private Sub ButtonAbortSlew_Click(sender As Object, e As EventArgs) Handles ButtonAbortSlew.Click
        Try
            driver.AbortSlew()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSlewToAltAz_Click(sender As Object, e As EventArgs) Handles ButtonSlewToAltAz.Click
        Try
            driver.SlewToAltAz(Convert.ToDouble(TextBoxAzimuth.Text.Replace(",", "."), nfi_), Convert.ToDouble(TextBoxAltitude.Text.Replace(",", "."), nfi_))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSlewToAltAzAsync_Click(sender As Object, e As EventArgs) Handles ButtonSlewToAltAzAsync.Click
        Try
            driver.SlewToAltAzAsync(Convert.ToDouble(TextBoxAzimuth.Text.Replace(",", "."), nfi_), Convert.ToDouble(TextBoxAltitude.Text.Replace(",", "."), nfi_))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonAtHome_Click(sender As Object, e As EventArgs) Handles ButtonAtHome.Click
        Try
            LabelAtHome.Text = driver.AtHome()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonAtPark_Click(sender As Object, e As EventArgs) Handles ButtonAtPark.Click
        Try
            LabelAtPark.Text = driver.AtPark()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonPark_Click(sender As Object, e As EventArgs) Handles ButtonPark.Click
        Try
            driver.Park()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonUnPark_Click(sender As Object, e As EventArgs) Handles ButtonUnPark.Click
        Try
            driver.Unpark()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSetPark_Click(sender As Object, e As EventArgs) Handles ButtonSetPark.Click
        Try
            driver.SetPark()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonFindHome_Click(sender As Object, e As EventArgs) Handles ButtonFindHome.Click
        Try
            driver.FindHome()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonMoveAxis_Click(sender As Object, e As EventArgs) Handles ButtonMoveAxis.Click
        Try
            driver.MoveAxis(CInt(TextBoxAxis.Text), Convert.ToDouble(TextBoxRate.Text.Replace(",", "."), nfi_))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonPulseGuide_Click(sender As Object, e As EventArgs) Handles ButtonPulseGuide.Click
        Try
            driver.PulseGuide(CInt(TextBoxGuideDirection.Text), CInt(TextBoxDuration.Text))
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetTarget_Click(sender As Object, e As EventArgs) Handles ButtonGetTarget.Click
        Try
            TextBoxTargetRA.Text = driver.TargetRightAscension
            TextBoxTargetDec.Text = driver.TargetDeclination
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSetTarget_Click(sender As Object, e As EventArgs) Handles ButtonSetTarget.Click
        Try
            driver.TargetRightAscension = Convert.ToDouble(TextBoxTargetRA.Text.Replace(",", "."), nfi_)
            driver.TargetDeclination = Convert.ToDouble(TextBoxTargetDec.Text.Replace(",", "."), nfi_)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSlewToTarget_Click(sender As Object, e As EventArgs) Handles ButtonSlewToTarget.Click
        Try
            driver.SlewToTarget()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSlewToTargetAsync_Click(sender As Object, e As EventArgs) Handles ButtonSlewToTargetAsync.Click
        Try
            driver.SlewToTargetAsync()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSyncToAltAz_Click(sender As Object, e As EventArgs) Handles ButtonSyncToAltAz.Click
        Try
            driver.SyncToAltAz(
            Convert.ToDouble(TextBoxAzimuth.Text.Replace(",", "."), nfi_),
            Convert.ToDouble(TextBoxAltitude.Text.Replace(",", "."), nfi_)
            )
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSyncToCoordinates_Click(sender As Object, e As EventArgs) Handles ButtonSyncToCoordinates.Click
        Try
            driver.SyncToCoordinates(
            Convert.ToDouble(TextBoxRightAsc.Text.Replace(",", "."), nfi_),
            Convert.ToDouble(TextBoxDec.Text.Replace(",", "."), nfi_)
            )
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSyncToTarget_Click(sender As Object, e As EventArgs) Handles ButtonSyncToTarget.Click
        Try
            driver.SyncToTarget()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetAltAz_Click(sender As Object, e As EventArgs)
        Try
            TextBoxAltitude.Text = driver.Altitude.ToString
            TextBoxAzimuth.Text = driver.Azimuth.ToString
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSlewToCoordinates_Click(sender As Object, e As EventArgs) Handles ButtonSlewToCoordinates.Click
        Try
            driver.SlewToCoordinates(
            Convert.ToDouble(TextBoxRightAsc.Text.Replace(",", "."), nfi_),
            Convert.ToDouble(TextBoxDec.Text.Replace(",", "."), nfi_)
            )

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSlewtoCoordinatesAsync_Click(sender As Object, e As EventArgs) Handles ButtonSlewtoCoordinatesAsync.Click
        Try
            driver.SlewToCoordinatesAsync(
            Convert.ToDouble(TextBoxRightAsc.Text.Replace(",", "."), nfi_),
            Convert.ToDouble(TextBoxDec.Text.Replace(",", "."), nfi_)
            )
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetSiteData_Click(sender As Object, e As EventArgs) Handles ButtonGetSiteData.Click
        Try
            TextBoxSiteLatitude.Text = driver.SiteLatitude
            TextBoxSiteLongitude.Text = driver.SiteLongitude
            TextBoxSiteElevation.Text = driver.SiteElevation
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSetSiteData_Click(sender As Object, e As EventArgs) Handles ButtonSetSiteData.Click
        Try
            driver.SiteLatitude = Convert.ToDouble(TextBoxSiteLatitude.Text.Replace(",", "."), nfi_)
            driver.SiteLongitude = Convert.ToDouble(TextBoxSiteLongitude.Text.Replace(",", "."), nfi_)
            driver.SiteElevation = Convert.ToDouble(TextBoxSiteElevation.Text.Replace(",", "."), nfi_)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonIsPulseGuiding_Click(sender As Object, e As EventArgs) Handles ButtonIsPulseGuiding.Click
        Try
            LabelIsPulseGuiding.Text = driver.IsPulseGuiding()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonIsTracking_Click(sender As Object, e As EventArgs) Handles ButtonIsTracking.Click
        Try
            LabelIsTracking.Text = driver.Tracking()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonTrackingEnable_Click(sender As Object, e As EventArgs) Handles ButtonTrackingEnable.Click
        Try
            driver.Tracking = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonTrackingDisable_Click(sender As Object, e As EventArgs) Handles ButtonTrackingDisable.Click
        Try
            driver.Tracking = False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonIsSlewing_Click(sender As Object, e As EventArgs) Handles ButtonIsSlewing.Click
        Try
            LabelIsSlewing.Text = driver.Slewing()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetRaDec_Click(sender As Object, e As EventArgs)
        Try
            TextBoxRightAsc.Text = driver.RightAscension()
            TextBoxDec.Text = driver.Declination()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonRaDecRateGet_Click(sender As Object, e As EventArgs) Handles ButtonRaDecRateGet.Click
        Try
            TextBoxRightAscRate.Text = driver.RightAscensionRate()
            TextBoxDecRate.Text = driver.DeclinationRate()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonRaDecRateSet_Click(sender As Object, e As EventArgs) Handles ButtonRaDecRateSet.Click
        Try
            driver.RightAscensionRate() = Convert.ToDouble(TextBoxRightAscRate.Text.Replace(",", "."), nfi_)
            driver.DeclinationRate() = Convert.ToDouble(TextBoxDecRate.Text.Replace(",", "."), nfi_)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles ButtonReadCapabilities.Click
        Try
            CheckBoxCanFindHome.Checked = driver.CanFindHome()
            CheckBoxCanPark.Checked = driver.CanPark()
            CheckBoxCanPulseGuide.Checked = driver.CanPulseGuide()
            CheckBoxCanSetDeclinationRate.Checked = driver.CanSetDeclinationRate
            CheckBoxCanSetGuideRates.Checked = driver.CanSetGuideRates
            CheckBoxCanSetPark.Checked = driver.CanSetPark
            CheckBoxCanSetPierSide.Checked = driver.CanSetPierSide
            CheckBoxCanSetRightAscensionRate.Checked = driver.CanSetRightAscensionRate
            CheckBoxCanSetTracking.Checked = driver.CanSetTracking
            CheckBoxCanSlew.Checked = driver.CanSlew
            CheckBoxCanSlewAltAz.Checked = driver.CanSlewAltAz
            CheckBoxCanSlewAltAzAsync.Checked = driver.CanSlewAltAzAsync
            CheckBoxCanSlewAsync.Checked = driver.CanSlewAsync
            CheckBoxCanSync.Checked = driver.CanSync
            CheckBoxCanSyncAltAz.Checked = driver.CanSyncAltAz
            CheckBoxCanUnpark.Checked = driver.CanUnpark
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            LabelAlt.Text = driver.Altitude.ToString
            LabelAz.Text = driver.Azimuth.ToString
            LabelDec.Text = driver.Declination.ToString
            LabelRa.Text = driver.RightAscension.ToString
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetTrackingRates_Click(sender As Object, e As EventArgs) Handles ButtonGetTrackingRates.Click
        'remove all entries from listbox.
        ListBoxTrackingRates.Items.Clear()

        Try
            Dim tracking_rates As ITrackingRates = driver.TrackingRates

            For x As Integer = 1 To tracking_rates.Count
                ListBoxTrackingRates.Items.Add(tracking_rates.Item(x).ToString)
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetTrackingRate_Click(sender As Object, e As EventArgs) Handles ButtonGetTrackingRate.Click
        Try
            ListBoxTrackingRates.SelectedIndex = driver.TrackingRate
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSetTrackingRate_Click(sender As Object, e As EventArgs) Handles ButtonSetTrackingRate.Click
        Try
            driver.TrackingRate = ListBoxTrackingRates.SelectedIndex
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetEnumerator_Click(sender As Object, e As EventArgs) Handles ButtonGetEnumerator.Click
        Try
            Dim tracking_rates As ITrackingRates = driver.TrackingRates
            tracking_rates.GetEnumerator()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonSiderealTime_Click(sender As Object, e As EventArgs) Handles ButtonSiderealTime.Click
        Try
            Dim time As Double = driver.SiderealTime
            Dim str As String = Int(time) & ":" & (time - Int(time)) * 60.0
            LabelSiderealTime.Text = str
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonGetSupportedActions_Click(sender As Object, e As EventArgs) Handles ButtonGetSupportedActions.Click
        ListBoxSupportedActions.Items.Clear()

        Try
            For Each obj As Object In driver.SupportedActions
                ListBoxSupportedActions.Items.Add(obj)
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub ButtonAction_Click(sender As Object, e As EventArgs) Handles ButtonAction.Click
        Dim opt As String = InputBox("option")
        MsgBox(opt)
        MsgBox(driver.Action(ListBoxSupportedActions.SelectedItem.ToString(), opt))
    End Sub
End Class
