Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports ASCOM.Utilities
Imports ASCOM.YAAADevice

Imports System.ComponentModel


<ComVisible(False)>
Public Class SetupDialogForm

    Private telescope_ As Telescope
    Private nfi_ As System.Globalization.NumberFormatInfo = New System.Globalization.CultureInfo("en-US", False).NumberFormat

    Private user_controls_() As UserControlTemperatureSensor

    Private telescope_driver_settings_ As Telescope.telescopeDriverSettings

    Public Sub New(t As Telescope)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        telescope_ = t

        'load available com ports into com port combobox
        ComboBoxComPorts.Items.AddRange(telescope_.serial_connection_.AvailableCOMPorts)

        'load alignment mode names into mount type combobox.
        ComboBoxMountType.Items.AddRange([Enum].GetNames(GetType(ASCOM.DeviceInterface.AlignmentModes)))

        'load stepper driver names into stepper driver type comboboxes.
        ComboBoxMotorDriverX.Items.AddRange([Enum].GetNames(GetType(Telescope.motor_driver_t)))
        ComboBoxMotorDriverY.Items.AddRange([Enum].GetNames(GetType(Telescope.motor_driver_t)))
        ComboBoxMotorDriverZ.Items.AddRange([Enum].GetNames(GetType(Telescope.motor_driver_t)))

        'load stepper driver names into limit handling type comboboxes.
        ComboBoxLimitHandlingX.Items.AddRange([Enum].GetNames(GetType(Telescope.limit_handling_t)))
        ComboBoxLimitHandlingY.Items.AddRange([Enum].GetNames(GetType(Telescope.limit_handling_t)))
        ComboBoxLimitHandlingZ.Items.AddRange([Enum].GetNames(GetType(Telescope.limit_handling_t)))

        'load steptypes into steptype comboboxes.
        ComboBoxStepTypeFastX.Items.AddRange([Enum].GetNames(GetType(Telescope.step_type_t)))
        ComboBoxStepTypeSlowX.Items.AddRange([Enum].GetNames(GetType(Telescope.step_type_t)))
        ComboBoxStepTypeFastY.Items.AddRange([Enum].GetNames(GetType(Telescope.step_type_t)))
        ComboBoxStepTypeSlowY.Items.AddRange([Enum].GetNames(GetType(Telescope.step_type_t)))
        ComboBoxStepTypeFastZ.Items.AddRange([Enum].GetNames(GetType(Telescope.step_type_t)))
        ComboBoxStepTypeSlowZ.Items.AddRange([Enum].GetNames(GetType(Telescope.step_type_t)))

        'LabelBuildDateTime.Text = System.Reflection.Assembly.GetExecutingAssembly.Location.ToString

        'LabelBuildDateTime.Text = IO.File.GetCreationTime(System.Reflection.Assembly.GetExecutingAssembly().Location)
        'LabelBuildDateTime.Text = IO.File.GetLastWriteTime(Application.ExecutablePath).ToUniversalTime

        updateStatusStrip()
    End Sub

    Private Sub updateStatusStrip()
        Dim current_settings As Telescope.telescopeDriverSettings = telescope_.getDriverSettings()

        Dim destination As String = ""

        If telescope_.Connected Then
            Dim message As String = "Connected to: "
            If current_settings.connection_type_ = Telescope.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Disconnect")
        Else
            ToolStripSplitButton1.Text = "Disconnected"
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Connect")
        End If

        ToolStripStatusLabel2.Text = ""
        ToolStripStatusLabel2.Spring = True

        ToolStripStatusLabel1.Alignment = ToolStripItemAlignment.Right
        ToolStripStatusLabel1.Text = "Ready"
    End Sub

    Private Sub connect(value As Boolean)
        Dim current_settings As Telescope.telescopeDriverSettings = telescope_.getDriverSettings()

        current_settings.com_port_ = ComboBoxComPorts.SelectedItem.ToString
        current_settings.baud_rate_ = ComboBoxBaudRate.SelectedItem.ToString

        current_settings.ip_address_ = TextBoxIPAddress.Text
        current_settings.port_ = CInt(TextBoxPort.Text)

        If RadioButtonSerial.Checked Then
            current_settings.connection_type_ = Telescope.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            current_settings.connection_type_ = Telescope.connection_type_t.NETWORK
        End If

        telescope_.setDriverSettings(current_settings)

        Dim destination As String = ""

        If value Then
            Dim message As String = "Attempting to connect to: "
            If current_settings.connection_type_ = Telescope.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            Application.DoEvents()

            'connect to the arduino and read values from the EEPROM.
            telescope_.Connected = True

        Else
            ToolStripSplitButton1.Text = "Disconnecting..."
            Application.DoEvents()

            telescope_.Connected = False
        End If

        If telescope_.Connected Then
            ToolStripSplitButton1.Text = "Connected to: " & destination
        Else
            ToolStripSplitButton1.Text = "Disconnected"
        End If

        Application.DoEvents()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click ' OK button event handler
        ' Persist new values of user settings to the ASCOM profile

        ' Persist new values of user settings to the ASCOM profile
        If Not WriteProfileSettings() Then
            Exit Sub
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click 'Cancel button event handler
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ShowAscomWebPage(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.DoubleClick, PictureBox1.Click
        ' Click on ASCOM logo event handler
        Try
            System.Diagnostics.Process.Start("http://ascom-standards.org/")
        Catch noBrowser As System.ComponentModel.Win32Exception
            If noBrowser.ErrorCode = -2147467259 Then
                MessageBox.Show(noBrowser.Message)
            End If
        Catch other As System.Exception
            MessageBox.Show(other.Message)
        End Try
    End Sub

    Private Sub AddSensorControls(Optional useEEPROM As Boolean = False)
        'clear existing user controls from FlowLayoutPanel
        If Not user_controls_ Is Nothing Then
            For Each user_control In user_controls_
                user_control.Dispose()
            Next
        End If

        'set flowLayoutPanel properties
        With FlowLayoutPanelTemperatureSensors
            '.Location = New Drawing.Point(6, 74)
            '.Size = New Drawing.Size(511, 219)
            .FlowDirection = FlowDirection.TopDown
        End With

        'use settings read from EEPROM if requested
        Dim current_settings As Telescope.telescopeDriverSettings = telescope_.getDriverSettings(useEEPROM)

        'redim array to hold Telescope.numTempSensors user control objects.
        ReDim user_controls_(current_settings.telescope_settings_.temperature_sensors_.Length - 1)

        'add entries for each temperature sensor
        For i As Integer = 0 To user_controls_.Length - 1
            'add control for temperature sensor.
            user_controls_(i) = New UserControlTemperatureSensor(current_settings.telescope_settings_.temperature_sensors_(i), i)

            'add control to FlowLayoutPanel
            FlowLayoutPanelTemperatureSensors.Controls.Add(user_controls_(i))
        Next
    End Sub

    Private Sub ReadProfileSettings()
        ' Retrieve current values of user settings from the driver
        setMaskToSettings(telescope_.getDriverSettings())
    End Sub

    Private Function WriteProfileSettings() As Boolean
        Dim success As Boolean = True

        ToolStripStatusLabel1.Text = "Writing Profile Settings"
        Application.DoEvents()
        Try
            'set driver settings to user settings in setup dialog input mask
            telescope_.setDriverSettings(getSettingsFromMask())

            'save settings to ASCOM profile store
            telescope_.WriteProfile()
        Catch ex As Exception
            'when an error occurred during reading the settings from the input fields, display an
            'error message and do not close the window.
            MsgBox("Invalid values detected, some settings have not been saved. Check all input fields with orange background and try again.", MsgBoxStyle.Critical)
            success = False
        End Try

        updateStatusStrip()

        Return success
    End Function

    Private Sub SetupDialogForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load ' Form load event handler
        ' Retrieve current values of user settings from the ASCOM Profile 
        ReadProfileSettings()
    End Sub

    Private Sub ButtonReadEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonReadEEPROM.Click
        'when calling ReadEEPROM() current settings stored in the ASCOM profile store are
        'not overwritten immediately. the settings read from the arduino are only accepted 
        'when the user clicks "ok" or "write EEPROM", otherwise the changes are discarded.

        Dim current_settings As Telescope.telescopeDriverSettings = telescope_.getDriverSettings()

        Try
            connect(True)

            If telescope_.Connected Then
                ToolStripStatusLabel1.Text = "Reading from EEPROM..."
                Application.DoEvents()

                telescope_.ReadEEPROM()

                'set setup dialog mask to values read from EEPROM.
                setMaskToSettings(telescope_.getDriverSettings(True))
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'catch any other exceptions just in case
            Dim err_msg As String = ""
            err_msg &= "Error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub

    Private Sub ButtonWriteEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonWriteEEPROM.Click
        'save values in ASCOM profile.
        If Not WriteProfileSettings() Then
            Exit Sub
        End If

        'check if user wants to force EEPROM Write.
        telescope_.EEPROM_force_write_ = CheckBoxEEPROMForceWrite.Checked

        Try
            'connect to the arduino and send over any data the user has changed.
            connect(True)

            'if the YAAADevice is connected, try to write the EEPROM contents
            If telescope_.Connected Then
                ToolStripStatusLabel1.Text = "Writing to EEPROM..."
                Application.DoEvents()

                telescope_.WriteEEPROM()
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'other exceptions are returned by function ReadEEPROM()
            Dim err_msg As String = ""
            err_msg &= "Error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'reset Force EEPROM Write to default.
        telescope_.EEPROM_force_write_ = False
        CheckBoxEEPROMForceWrite.Checked = False

        updateStatusStrip()
    End Sub

    Private Sub ComboBoxMotorDriverX_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxMotorDriverX.SelectedIndexChanged
        Select Case ComboBoxMotorDriverX.SelectedItem.ToString
            Case Telescope.motor_driver_t.ADAFRUIT_MOTORSHIELD_V2.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddressX.Enabled = True
                ComboBoxTerminalX.Enabled = True

                'disable other options.
                TextBoxPin1X.Enabled = False
                CheckBoxPin1InvertedX.Enabled = False
                TextBoxPin2X.Enabled = False
                CheckBoxPin2InvertedX.Enabled = False
                TextBoxPin3X.Enabled = False
                CheckBoxPin3InvertedX.Enabled = False
                TextBoxPin4X.Enabled = False
                CheckBoxPin4InvertedX.Enabled = False
                TextBoxPin5X.Enabled = False
                CheckBoxPin5InvertedX.Enabled = False
                TextBoxPin6X.Enabled = False
                CheckBoxPin6InvertedX.Enabled = False

            Case Telescope.motor_driver_t.FOUR_WIRE_STEPPER.ToString
                'reenable four wire stepper options.
                TextBoxPin1X.Enabled = True
                CheckBoxPin1InvertedX.Enabled = True
                TextBoxPin2X.Enabled = True
                CheckBoxPin2InvertedX.Enabled = True
                TextBoxPin3X.Enabled = True
                CheckBoxPin3InvertedX.Enabled = True
                TextBoxPin4X.Enabled = True
                CheckBoxPin4InvertedX.Enabled = True

                'disable other options.
                ComboBoxI2CAddressX.Enabled = False
                ComboBoxTerminalX.Enabled = False

                TextBoxPin5X.Enabled = False
                CheckBoxPin5InvertedX.Enabled = False
                TextBoxPin6X.Enabled = False
                CheckBoxPin6InvertedX.Enabled = False

            Case Telescope.motor_driver_t.DRIVER_A4988.ToString, Telescope.motor_driver_t.DRIVER_DRV8825.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddressX.Enabled = False
                ComboBoxTerminalX.Enabled = False

                'disable other options.
                TextBoxPin1X.Enabled = True
                CheckBoxPin1InvertedX.Enabled = True
                TextBoxPin2X.Enabled = True
                CheckBoxPin2InvertedX.Enabled = True
                TextBoxPin3X.Enabled = True
                CheckBoxPin3InvertedX.Enabled = True
                TextBoxPin4X.Enabled = True
                CheckBoxPin4InvertedX.Enabled = True
                TextBoxPin5X.Enabled = True
                CheckBoxPin5InvertedX.Enabled = True
                TextBoxPin6X.Enabled = True
                CheckBoxPin6InvertedX.Enabled = True
        End Select
    End Sub

    Private Sub ComboBoxMotorDriverY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxMotorDriverY.SelectedIndexChanged
        Select Case ComboBoxMotorDriverY.SelectedItem.ToString
            Case Telescope.motor_driver_t.ADAFRUIT_MOTORSHIELD_V2.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddressY.Enabled = True
                ComboBoxTerminalY.Enabled = True

                'disable other options.
                TextBoxPin1Y.Enabled = False
                CheckBoxPin1InvertedY.Enabled = False
                TextBoxPin2Y.Enabled = False
                CheckBoxPin2InvertedY.Enabled = False
                TextBoxPin3Y.Enabled = False
                CheckBoxPin3InvertedY.Enabled = False
                TextBoxPin4Y.Enabled = False
                CheckBoxPin4InvertedY.Enabled = False
                TextBoxPin5Y.Enabled = False
                CheckBoxPin5InvertedY.Enabled = False
                TextBoxPin6Y.Enabled = False
                CheckBoxPin6InvertedY.Enabled = False
            Case Telescope.motor_driver_t.FOUR_WIRE_STEPPER.ToString
                'reenable four wire stepper options.
                TextBoxPin1Y.Enabled = True
                CheckBoxPin1InvertedY.Enabled = True
                TextBoxPin2Y.Enabled = True
                CheckBoxPin2InvertedY.Enabled = True
                TextBoxPin3Y.Enabled = True
                CheckBoxPin3InvertedY.Enabled = True
                TextBoxPin4Y.Enabled = True
                CheckBoxPin4InvertedY.Enabled = True

                'disable other options.
                ComboBoxI2CAddressY.Enabled = False
                ComboBoxTerminalY.Enabled = False

                TextBoxPin5Y.Enabled = False
                CheckBoxPin5InvertedY.Enabled = False
                TextBoxPin6Y.Enabled = False
                CheckBoxPin6InvertedY.Enabled = False
            Case Telescope.motor_driver_t.DRIVER_A4988.ToString, Telescope.motor_driver_t.DRIVER_DRV8825.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddressY.Enabled = False
                ComboBoxTerminalY.Enabled = False

                'disable other options.
                TextBoxPin1Y.Enabled = True
                CheckBoxPin1InvertedY.Enabled = True
                TextBoxPin2Y.Enabled = True
                CheckBoxPin2InvertedY.Enabled = True
                TextBoxPin3Y.Enabled = True
                CheckBoxPin3InvertedY.Enabled = True
                TextBoxPin4Y.Enabled = True
                CheckBoxPin4InvertedY.Enabled = True
                TextBoxPin5Y.Enabled = True
                CheckBoxPin5InvertedY.Enabled = True
                TextBoxPin6Y.Enabled = True
                CheckBoxPin6InvertedY.Enabled = True
        End Select
    End Sub

    Private Sub ComboBoxMotorDriverZ_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxMotorDriverZ.SelectedIndexChanged
        Select Case ComboBoxMotorDriverZ.SelectedItem.ToString
            Case Telescope.motor_driver_t.ADAFRUIT_MOTORSHIELD_V2.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddressZ.Enabled = True
                ComboBoxTerminalZ.Enabled = True

                'disable other options.
                TextBoxPin1Z.Enabled = False
                CheckBoxPin1InvertedZ.Enabled = False
                TextBoxPin2Z.Enabled = False
                CheckBoxPin2InvertedZ.Enabled = False
                TextBoxPin3Z.Enabled = False
                CheckBoxPin3InvertedZ.Enabled = False
                TextBoxPin4Z.Enabled = False
                CheckBoxPin4InvertedZ.Enabled = False
                TextBoxPin5Z.Enabled = False
                CheckBoxPin5InvertedZ.Enabled = False
                TextBoxPin6Z.Enabled = False
                CheckBoxPin6InvertedZ.Enabled = False
            Case Telescope.motor_driver_t.FOUR_WIRE_STEPPER.ToString
                'reenable four wire stepper options.
                TextBoxPin1Z.Enabled = True
                CheckBoxPin1InvertedZ.Enabled = True
                TextBoxPin2Z.Enabled = True
                CheckBoxPin2InvertedZ.Enabled = True
                TextBoxPin3Z.Enabled = True
                CheckBoxPin3InvertedZ.Enabled = True
                TextBoxPin4Z.Enabled = True
                CheckBoxPin4InvertedZ.Enabled = True

                'disable other options.
                ComboBoxI2CAddressZ.Enabled = False
                ComboBoxTerminalZ.Enabled = False

                TextBoxPin5Z.Enabled = False
                CheckBoxPin5InvertedZ.Enabled = False
                TextBoxPin6Z.Enabled = False
                CheckBoxPin6InvertedZ.Enabled = False
            Case Telescope.motor_driver_t.DRIVER_A4988.ToString, Telescope.motor_driver_t.DRIVER_DRV8825.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddressZ.Enabled = False
                ComboBoxTerminalZ.Enabled = False

                'disable other options.
                TextBoxPin1Z.Enabled = True
                CheckBoxPin1InvertedZ.Enabled = True
                TextBoxPin2Z.Enabled = True
                CheckBoxPin2InvertedZ.Enabled = True
                TextBoxPin3Z.Enabled = True
                CheckBoxPin3InvertedZ.Enabled = True
                TextBoxPin4Z.Enabled = True
                CheckBoxPin4InvertedZ.Enabled = True
                TextBoxPin5Z.Enabled = True
                CheckBoxPin5InvertedZ.Enabled = True
                TextBoxPin6Z.Enabled = True
                CheckBoxPin6InvertedZ.Enabled = True
        End Select
    End Sub

    Private Sub ButtonReadSwitchState_Click(sender As Object, e As EventArgs) Handles ButtonReadSwitchState.Click
        Dim switch_state As Short

        Try
            connect(True)

            ToolStripStatusLabel1.Text = "Reading Switch State"
            Application.DoEvents()

            switch_state = CShort(telescope_.Action("Get_Switch_State", "X_AXIS"))

            CheckBoxESLTriggeredX.Checked = ((switch_state And 1) <> 0)
            CheckBoxESUTriggeredX.Checked = ((switch_state And 2) <> 0)
            CheckBoxHSTriggeredX.Checked = ((switch_state And 4) <> 0)

            switch_state = CShort(telescope_.Action("Get_Switch_State", "Y_AXIS"))

            CheckBoxESLTriggeredY.Checked = ((switch_state And 1) <> 0)
            CheckBoxESUTriggeredY.Checked = ((switch_state And 2) <> 0)
            CheckBoxHSTriggeredY.Checked = ((switch_state And 4) <> 0)

            switch_state = CShort(telescope_.Action("Get_Switch_State", "Z_AXIS"))

            CheckBoxESLTriggeredZ.Checked = ((switch_state And 1) <> 0)
            CheckBoxESUTriggeredZ.Checked = ((switch_state And 2) <> 0)
            CheckBoxHSTriggeredZ.Checked = ((switch_state And 4) <> 0)

            'throw an exception if 
        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

        updateStatusStrip()
    End Sub

    Private Sub setMaskToSettings(settings As Telescope.telescopeDriverSettings)
        chkTrace.Checked = Telescope.telescopeDriverSettings.trace_state_

        With settings
            Select Case .connection_type_
                Case Telescope.connection_type_t.SERIAL
                    RadioButtonSerial.Checked = True
                Case Telescope.connection_type_t.NETWORK
                    RadioButtonNetwork.Checked = True
            End Select

            If Not ComboBoxComPorts.Items.Contains(.com_port_) Then
                ComboBoxComPorts.Items.Add(.com_port_)
            End If

            ComboBoxComPorts.SelectedItem = .com_port_.ToString
            ComboBoxBaudRate.SelectedItem = .baud_rate_.ToString

            TextBoxIPAddress.Text = .ip_address_
            TextBoxPort.Text = .port_.ToString
        End With

        With settings.telescope_settings_
            'site information.
            TextBoxSiteLatitude.Text = .site_latitude_
            TextBoxSiteLongitude.Text = .site_longitude_
            TextBoxSiteElevation.Text = .site_elevation_

            'general telescope information.
            TextBoxFocalLength.Text = .focal_length_
            TextBoxApertureArea.Text = .aperture_area_
            TextBoxApertureDiameter.Text = .aperture_diameter_
            ComboBoxMountType.SelectedItem = .mount_type_.ToString

            CheckBoxSAH.Checked = .startup_auto_home_
            CheckBoxParkEnabled.Checked = .park_enabled_

            'manual control settings.
            CheckBoxMCEnable.Checked = .mc_available_
            CheckBoxMCTA.Checked = .mc_toggle_available_
            CheckBoxMCCSA.Checked = .mc_cycle_speed_available_
            CheckBoxMCJA.Checked = .mc_joystick_available_

            'TextBoxMCMaxSpeed.Text = .mc_max_speed
            TextBoxMCT.Text = YAAASharedClassLibrary.printPin(.mc_toggle_)
            TextBoxMCCS.Text = YAAASharedClassLibrary.printPin(.mc_cycle_speed_)

            'ComboBoxMCStepType.SelectedItem = .mc_steptype.ToString
            TextBoxMCJoystickAxisX.Text = YAAASharedClassLibrary.printPin(.mc_joystick_x_)
            TextBoxMCJoystickAxisY.Text = YAAASharedClassLibrary.printPin(.mc_joystick_y_)
            TextBoxMCJoystickDeadZone.Text = .mc_joystick_deadzone_


        End With

        'settings for x axis.
        With settings.axis_settings_(0)
            'drive settings.
            TextBoxMaxSpeedX.Text = .max_speed_
            TextBoxAccelX.Text = .acceleration_
            TextBoxStepsRevX.Text = .number_of_steps_
            TextBoxGearRatioX.Text = .gear_ratio_
            ComboBoxStepTypeFastX.SelectedItem = .step_type_fast_.ToString
            ComboBoxStepTypeSlowX.SelectedItem = .step_type_slow_.ToString

            'home switch settings.
            CheckBoxHSAX.Checked = .home_switch_enabled_
            CheckBoxHSIX.Checked = .home_switch_inverted_
            TextBoxHSNX.Text = YAAASharedClassLibrary.printPin(.home_switch_nr_)
            TextBoxHomeAlignmentX.Text = .home_alignment_

            CheckBoxHASX.Checked = .home_alignment_sync_

            CheckBoxASRX.Checked = .automatic_stepper_release_

            TextBoxParkPosX.Text = .park_pos_

            'endstop switch settings.
            CheckBoxEndStopLowerEnabledX.Checked = .endstop_lower_enabled_
            CheckBoxEndstopLowerInvertedX.Checked = .endstop_lower_inverted_
            TextBoxEndstopLowerPinX.Text = YAAASharedClassLibrary.printPin(.endstop_lower_)
            CheckBoxEndstopUpperEnabledX.Checked = .endstop_upper_enabled_
            CheckBoxEndstopUpperInvertedX.Checked = .endstop_upper_inverted_
            TextBoxEndstopUpperPinX.Text = YAAASharedClassLibrary.printPin(.endstop_upper_)
            CheckBoxEndstopHardStopX.Checked = .endstop_hard_stop_

            'software movement limit settings.
            CheckBoxLowerLimitEnabledX.Checked = .limit_lower_enabled_
            TextBoxLowerLimitX.Text = .limit_lower_
            CheckBoxUpperLimitEnabledX.Checked = .limit_upper_enabled_
            TextBoxUpperLimitX.Text = .limit_upper_
            ComboBoxLimitHandlingX.SelectedItem = .limit_handling_.ToString
            CheckBoxHardStopLimitX.Checked = .limit_hard_stop_

            'motor driver settings.
            ComboBoxMotorDriverX.SelectedItem = .motor_driver_.ToString

            ComboBoxI2CAddressX.SelectedItem = "0x" & Hex(.i2c_address_)
            ComboBoxTerminalX.SelectedItem = .terminal_.ToString

            TextBoxPin1X.Text = YAAASharedClassLibrary.printPin(.pin_1_)
            CheckBoxPin1InvertedX.Checked = .pin_1_inverted_
            TextBoxPin2X.Text = YAAASharedClassLibrary.printPin(.pin_2_)
            CheckBoxPin2InvertedX.Checked = .pin_2_inverted_
            TextBoxPin3X.Text = YAAASharedClassLibrary.printPin(.pin_3_)
            CheckBoxPin3InvertedX.Checked = .pin_3_inverted_
            TextBoxPin4X.Text = YAAASharedClassLibrary.printPin(.pin_4_)
            CheckBoxPin4InvertedX.Checked = .pin_4_inverted_
            TextBoxPin5X.Text = YAAASharedClassLibrary.printPin(.pin_5_)
            CheckBoxPin5InvertedX.Checked = .pin_5_inverted_
            TextBoxPin6X.Text = YAAASharedClassLibrary.printPin(.pin_6_)
            CheckBoxPin6InvertedX.Checked = .pin_6_inverted_
        End With

        'settings for y axis.
        With settings.axis_settings_(1)
            'drive settings.
            TextBoxMaxSpeedY.Text = .max_speed_
            TextBoxAccelY.Text = .acceleration_
            TextBoxStepsRevY.Text = .number_of_steps_
            TextBoxGearRatioY.Text = .gear_ratio_
            ComboBoxStepTypeFastY.SelectedItem = .step_type_fast_.ToString
            ComboBoxStepTypeSlowY.SelectedItem = .step_type_slow_.ToString

            'home switch settings.
            CheckBoxHSAY.Checked = .home_switch_enabled_
            CheckBoxHSIY.Checked = .home_switch_inverted_
            TextBoxHSNY.Text = YAAASharedClassLibrary.printPin(.home_switch_nr_)
            TextBoxHomeAlignmentY.Text = .home_alignment_

            CheckBoxHASY.Checked = .home_alignment_sync_

            CheckBoxASRY.Checked = .automatic_stepper_release_

            TextBoxParkPosY.Text = .park_pos_

            'endstop switch settings.
            CheckBoxEndStopLowerEnabledY.Checked = .endstop_lower_enabled_
            CheckBoxEndstopLowerInvertedY.Checked = .endstop_lower_inverted_
            TextBoxEndstopLowerPinY.Text = YAAASharedClassLibrary.printPin(.endstop_lower_)
            CheckBoxEndstopUpperEnabledY.Checked = .endstop_upper_enabled_
            CheckBoxEndstopUpperInvertedY.Checked = .endstop_upper_inverted_
            TextBoxEndstopUpperPinY.Text = YAAASharedClassLibrary.printPin(.endstop_upper_)
            CheckBoxEndstopHardStopY.Checked = .endstop_hard_stop_

            'software movement limit settings.
            CheckBoxLowerLimitEnabledY.Checked = .limit_lower_enabled_
            TextBoxLowerLimitY.Text = .limit_lower_
            CheckBoxUpperLimitEnabledY.Checked = .limit_upper_enabled_
            TextBoxUpperLimitY.Text = .limit_upper_
            ComboBoxLimitHandlingY.SelectedItem = .limit_handling_.ToString
            CheckBoxHardStopLimitY.Checked = .limit_hard_stop_

            'motor driver settings.
            ComboBoxMotorDriverY.SelectedItem = .motor_driver_.ToString

            ComboBoxI2CAddressY.SelectedItem = "0x" & Hex(.i2c_address_)
            ComboBoxTerminalY.SelectedItem = .terminal_.ToString

            TextBoxPin1Y.Text = YAAASharedClassLibrary.printPin(.pin_1_)
            CheckBoxPin1InvertedY.Checked = .pin_1_inverted_
            TextBoxPin2Y.Text = YAAASharedClassLibrary.printPin(.pin_2_)
            CheckBoxPin2InvertedY.Checked = .pin_2_inverted_
            TextBoxPin3Y.Text = YAAASharedClassLibrary.printPin(.pin_3_)
            CheckBoxPin3InvertedY.Checked = .pin_3_inverted_
            TextBoxPin4Y.Text = YAAASharedClassLibrary.printPin(.pin_4_)
            CheckBoxPin4InvertedY.Checked = .pin_4_inverted_
            TextBoxPin5Y.Text = YAAASharedClassLibrary.printPin(.pin_5_)
            CheckBoxPin5InvertedY.Checked = .pin_5_inverted_
            TextBoxPin6Y.Text = YAAASharedClassLibrary.printPin(.pin_6_)
            CheckBoxPin6InvertedY.Checked = .pin_6_inverted_
        End With

        'settings for z axis.
        With settings.axis_settings_(2)
            'drive settings.
            TextBoxMaxSpeedZ.Text = .max_speed_
            TextBoxAccelZ.Text = .acceleration_
            TextBoxStepsRevZ.Text = .number_of_steps_
            TextBoxGearRatioZ.Text = .gear_ratio_
            ComboBoxStepTypeFastZ.SelectedItem = .step_type_fast_.ToString
            ComboBoxStepTypeSlowZ.SelectedItem = .step_type_slow_.ToString

            'home switch settings.
            CheckBoxHSAZ.Checked = .home_switch_enabled_
            CheckBoxHSIZ.Checked = .home_switch_inverted_
            TextBoxHSNZ.Text = YAAASharedClassLibrary.printPin(.home_switch_nr_)
            TextBoxHomeAlignmentZ.Text = .home_alignment_

            CheckBoxHASZ.Checked = .home_alignment_sync_

            CheckBoxASRZ.Checked = .automatic_stepper_release_

            TextBoxParkPosZ.Text = .park_pos_

            'endstop switch settings.
            CheckBoxEndStopLowerEnabledZ.Checked = .endstop_lower_enabled_
            CheckBoxEndstopLowerInvertedZ.Checked = .endstop_lower_inverted_
            TextBoxEndstopLowerPinZ.Text = YAAASharedClassLibrary.printPin(.endstop_lower_)
            CheckBoxEndstopUpperEnabledZ.Checked = .endstop_upper_enabled_
            CheckBoxEndstopUpperInvertedZ.Checked = .endstop_upper_inverted_
            TextBoxEndstopUpperPinZ.Text = YAAASharedClassLibrary.printPin(.endstop_upper_)
            CheckBoxEndstopHardStopZ.Checked = .endstop_hard_stop_

            'software movement limit settings.
            CheckBoxLowerLimitEnabledZ.Checked = .limit_lower_enabled_
            TextBoxLowerLimitZ.Text = .limit_lower_
            CheckBoxUpperLimitEnabledZ.Checked = .limit_upper_enabled_
            TextBoxUpperLimitZ.Text = .limit_upper_
            ComboBoxLimitHandlingZ.SelectedItem = .limit_handling_.ToString
            CheckBoxHardStopLimitZ.Checked = .limit_hard_stop_

            'motor driver settings.
            ComboBoxMotorDriverZ.SelectedItem = .motor_driver_.ToString

            ComboBoxI2CAddressZ.SelectedItem = "0x" & Hex(.i2c_address_)
            ComboBoxTerminalZ.SelectedItem = .terminal_.ToString

            TextBoxPin1Z.Text = YAAASharedClassLibrary.printPin(.pin_1_)
            CheckBoxPin1InvertedZ.Checked = .pin_1_inverted_
            TextBoxPin2Z.Text = YAAASharedClassLibrary.printPin(.pin_2_)
            CheckBoxPin2InvertedZ.Checked = .pin_2_inverted_
            TextBoxPin3Z.Text = YAAASharedClassLibrary.printPin(.pin_3_)
            CheckBoxPin3InvertedZ.Checked = .pin_3_inverted_
            TextBoxPin4Z.Text = YAAASharedClassLibrary.printPin(.pin_4_)
            CheckBoxPin4InvertedZ.Checked = .pin_4_inverted_
            TextBoxPin5Z.Text = YAAASharedClassLibrary.printPin(.pin_5_)
            CheckBoxPin5InvertedZ.Checked = .pin_5_inverted_
            TextBoxPin6Z.Text = YAAASharedClassLibrary.printPin(.pin_6_)
            CheckBoxPin6InvertedZ.Checked = .pin_6_inverted_
        End With

        'add temperature sensor controls
        AddSensorControls()
    End Sub

    Private Function getSettingsFromMask() As Telescope.telescopeDriverSettings
        Dim valid As Boolean = True
        Dim return_value As Telescope.telescopeDriverSettings = New Telescope.telescopeDriverSettings(True)

        ' Persist new values of user settings to the ASCOM profile
        ' Update the state variables with results from the dialogue
        Telescope.telescopeDriverSettings.trace_state_ = chkTrace.Checked

        'only store com port when a com port is selected.
        If Not ComboBoxComPorts.SelectedItem Is Nothing Then
            return_value.com_port_ = ComboBoxComPorts.SelectedItem.ToString               'com port the arduino is connected to.
        End If

        return_value.baud_rate_ = CInt(ComboBoxBaudRate.SelectedItem.ToString)            'baud rate.

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxIPAddress, return_value.ip_address_)
        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPort, return_value.port_)

        If RadioButtonSerial.Checked Then
            return_value.connection_type_ = Telescope.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            return_value.connection_type_ = Telescope.connection_type_t.NETWORK
        End If

        With return_value.telescope_settings_
            .mount_type_ = [Enum].Parse(GetType(ASCOM.DeviceInterface.AlignmentModes), ComboBoxMountType.SelectedItem.ToString)

            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxFocalLength, .focal_length_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxApertureArea, .aperture_area_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxApertureDiameter, .aperture_diameter_)

            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxSiteLatitude, .site_latitude_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxSiteLongitude, .site_longitude_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxSiteElevation, .site_elevation_)

            .startup_auto_home_ = CheckBoxSAH.Checked
            .park_enabled_ = CheckBoxParkEnabled.Checked

            .mc_available_ = CheckBoxMCEnable.Checked
            .mc_toggle_available_ = CheckBoxMCTA.Checked
            .mc_cycle_speed_available_ = CheckBoxMCCSA.Checked
            .mc_joystick_available_ = CheckBoxMCJA.Checked

            '.mc_max_speed = Convert.ToDouble(TextBoxMCMaxSpeed.Text.Replace(",", "."), nfi_)
            TextBoxMCT.Text = YAAASharedClassLibrary.parsePin(TextBoxMCT.Text)
            TextBoxMCCS.Text = YAAASharedClassLibrary.parsePin(TextBoxMCCS.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMCT, .mc_toggle_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMCCS, .mc_cycle_speed_)

            '.mc_steptype = [Enum].Parse(GetType(Telescope.step_type_t), ComboBoxMCStepType.SelectedItem.ToString)
            TextBoxMCJoystickAxisX.Text = YAAASharedClassLibrary.parsePin(TextBoxMCJoystickAxisX.Text)
            TextBoxMCJoystickAxisY.Text = YAAASharedClassLibrary.parsePin(TextBoxMCJoystickAxisY.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMCJoystickAxisX, .mc_joystick_x_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMCJoystickAxisY, .mc_joystick_y_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMCJoystickDeadZone, .mc_joystick_deadzone_)
        End With

        'x axis.
        With return_value.axis_settings_(0)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMaxSpeedX, .max_speed_)                    'stepper speed in steps / second.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAccelX, .acceleration_)                    'acceleration in steps / second ^ 2.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxGearRatioX, .gear_ratio_)                  'gear ratio.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxStepsRevX, .number_of_steps_)              'number of steps per stepper revolution.

            .step_type_fast_ = [Enum].Parse(GetType(Telescope.step_type_t), ComboBoxStepTypeFastX.SelectedItem.ToString)
            .step_type_slow_ = [Enum].Parse(GetType(Telescope.step_type_t), ComboBoxStepTypeSlowX.SelectedItem.ToString)

            .home_switch_enabled_ = CheckBoxHSAX.Checked                                          '
            .home_switch_inverted_ = CheckBoxHSIX.Checked
            TextBoxHSNX.Text = YAAASharedClassLibrary.parsePin(TextBoxHSNX.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxHSNX, .home_switch_nr_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxHomeAlignmentX, .home_alignment_)

            .home_alignment_sync_ = CheckBoxHASX.Checked

            .automatic_stepper_release_ = CheckBoxASRX.Checked

            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxParkPosX, .park_pos_)                      'telescope axis park position.

            .endstop_lower_enabled_ = CheckBoxEndStopLowerEnabledX.Checked
            .endstop_lower_inverted_ = CheckBoxEndstopLowerInvertedX.Checked
            TextBoxEndstopLowerPinX.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopLowerPinX.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxEndstopLowerPinX, .endstop_lower_)

            .endstop_upper_enabled_ = CheckBoxEndstopUpperEnabledX.Checked
            .endstop_upper_inverted_ = CheckBoxEndstopUpperInvertedX.Checked
            TextBoxEndstopUpperPinX.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopUpperPinX.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxEndstopUpperPinX, .endstop_upper_)

            .endstop_hard_stop_ = CheckBoxEndstopHardStopX.Checked

            .limit_lower_enabled_ = CheckBoxLowerLimitEnabledX.Checked
            .limit_upper_enabled_ = CheckBoxUpperLimitEnabledX.Checked
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxLowerLimitX, .limit_lower_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxUpperLimitX, .limit_upper_)
            .limit_handling_ = [Enum].Parse(GetType(Telescope.limit_handling_t), ComboBoxLimitHandlingX.SelectedItem.ToString)
            .limit_hard_stop_ = CheckBoxHardStopLimitX.Checked


            .motor_driver_ = [Enum].Parse(GetType(Telescope.motor_driver_t), ComboBoxMotorDriverX.SelectedItem.ToString)

            .i2c_address_ = CByte(Convert.ToInt32(ComboBoxI2CAddressX.SelectedItem.ToString, 16))
            .terminal_ = CByte(ComboBoxTerminalX.SelectedItem.ToString)

            TextBoxPin1X.Text = YAAASharedClassLibrary.parsePin(TextBoxPin1X.Text)
            TextBoxPin2X.Text = YAAASharedClassLibrary.parsePin(TextBoxPin2X.Text)
            TextBoxPin3X.Text = YAAASharedClassLibrary.parsePin(TextBoxPin3X.Text)
            TextBoxPin4X.Text = YAAASharedClassLibrary.parsePin(TextBoxPin4X.Text)
            TextBoxPin5X.Text = YAAASharedClassLibrary.parsePin(TextBoxPin5X.Text)
            TextBoxPin6X.Text = YAAASharedClassLibrary.parsePin(TextBoxPin6X.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin1X, .pin_1_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin2X, .pin_2_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin3X, .pin_3_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin4X, .pin_4_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin5X, .pin_5_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin6X, .pin_6_)

            .pin_1_inverted_ = CheckBoxPin1InvertedX.Checked
            .pin_2_inverted_ = CheckBoxPin2InvertedX.Checked
            .pin_3_inverted_ = CheckBoxPin3InvertedX.Checked
            .pin_4_inverted_ = CheckBoxPin4InvertedX.Checked
            .pin_5_inverted_ = CheckBoxPin5InvertedX.Checked
            .pin_6_inverted_ = CheckBoxPin6InvertedX.Checked
        End With

        'y axis.
        With return_value.axis_settings_(1)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMaxSpeedY, .max_speed_)                    'stepper speed in steps / second.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAccelY, .acceleration_)                    'acceleration in steps / second ^ 2.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxGearRatioY, .gear_ratio_)                  'gear ratio.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxStepsRevY, .number_of_steps_)              'number of steps per stepper revolution.
            .step_type_fast_ = [Enum].Parse(GetType(Telescope.step_type_t), ComboBoxStepTypeFastY.SelectedItem.ToString)
            .step_type_slow_ = [Enum].Parse(GetType(Telescope.step_type_t), ComboBoxStepTypeSlowY.SelectedItem.ToString)

            .home_switch_enabled_ = CheckBoxHSAY.Checked                                          '
            .home_switch_inverted_ = CheckBoxHSIY.Checked
            TextBoxHSNY.Text = YAAASharedClassLibrary.parsePin(TextBoxHSNY.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxHSNY, .home_switch_nr_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxHomeAlignmentY, .home_alignment_)

            .home_alignment_sync_ = CheckBoxHASY.Checked

            .automatic_stepper_release_ = CheckBoxASRY.Checked

            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxParkPosY, .park_pos_)         'telescope axis park position.

            .endstop_lower_enabled_ = CheckBoxEndStopLowerEnabledY.Checked
            .endstop_lower_inverted_ = CheckBoxEndstopLowerInvertedY.Checked
            TextBoxEndstopLowerPinY.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopLowerPinY.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxEndstopLowerPinY, .endstop_lower_)

            .endstop_upper_enabled_ = CheckBoxEndstopUpperEnabledY.Checked
            .endstop_upper_inverted_ = CheckBoxEndstopUpperInvertedY.Checked
            TextBoxEndstopUpperPinY.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopUpperPinY.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxEndstopUpperPinY, .endstop_upper_)

            .endstop_hard_stop_ = CheckBoxEndstopHardStopY.Checked

            .limit_lower_enabled_ = CheckBoxLowerLimitEnabledY.Checked
            .limit_upper_enabled_ = CheckBoxUpperLimitEnabledY.Checked
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxLowerLimitY, .limit_lower_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxUpperLimitY, .limit_upper_)
            .limit_handling_ = [Enum].Parse(GetType(Telescope.limit_handling_t), ComboBoxLimitHandlingY.SelectedItem.ToString)
            .limit_hard_stop_ = CheckBoxHardStopLimitY.Checked

            .motor_driver_ = [Enum].Parse(GetType(Telescope.motor_driver_t), ComboBoxMotorDriverY.SelectedItem.ToString)

            .i2c_address_ = CByte(Convert.ToInt32(ComboBoxI2CAddressY.SelectedItem.ToString, 16))
            .terminal_ = CByte(ComboBoxTerminalY.SelectedItem.ToString)

            TextBoxPin1Y.Text = YAAASharedClassLibrary.parsePin(TextBoxPin1Y.Text)
            TextBoxPin2Y.Text = YAAASharedClassLibrary.parsePin(TextBoxPin2Y.Text)
            TextBoxPin3Y.Text = YAAASharedClassLibrary.parsePin(TextBoxPin3Y.Text)
            TextBoxPin4Y.Text = YAAASharedClassLibrary.parsePin(TextBoxPin4Y.Text)
            TextBoxPin5Y.Text = YAAASharedClassLibrary.parsePin(TextBoxPin5Y.Text)
            TextBoxPin6Y.Text = YAAASharedClassLibrary.parsePin(TextBoxPin6Y.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin1Y, .pin_1_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin2Y, .pin_2_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin3Y, .pin_3_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin4Y, .pin_4_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin5Y, .pin_5_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin6Y, .pin_6_)

            .pin_1_inverted_ = CheckBoxPin1InvertedY.Checked
            .pin_2_inverted_ = CheckBoxPin2InvertedY.Checked
            .pin_3_inverted_ = CheckBoxPin3InvertedY.Checked
            .pin_4_inverted_ = CheckBoxPin4InvertedY.Checked
            .pin_5_inverted_ = CheckBoxPin5InvertedY.Checked
            .pin_6_inverted_ = CheckBoxPin6InvertedY.Checked
        End With

        'z axis.
        With return_value.axis_settings_(2)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxMaxSpeedZ, .max_speed_)                    'stepper speed in steps / second.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAccelZ, .acceleration_)                    'acceleration in steps / second ^ 2.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxGearRatioZ, .gear_ratio_)                  'gear ratio.
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxStepsRevZ, .number_of_steps_)              'number of steps per stepper revolution.
            .step_type_fast_ = [Enum].Parse(GetType(Telescope.step_type_t), ComboBoxStepTypeFastZ.SelectedItem.ToString)
            .step_type_slow_ = [Enum].Parse(GetType(Telescope.step_type_t), ComboBoxStepTypeSlowZ.SelectedItem.ToString)

            .home_switch_enabled_ = CheckBoxHSAZ.Checked                                          '
            .home_switch_inverted_ = CheckBoxHSIZ.Checked
            TextBoxHSNZ.Text = YAAASharedClassLibrary.parsePin(TextBoxHSNZ.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxHSNZ, .home_switch_nr_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxHomeAlignmentZ, .home_alignment_)

            .home_alignment_sync_ = CheckBoxHASZ.Checked

            .automatic_stepper_release_ = CheckBoxASRZ.Checked

            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxParkPosZ, .park_pos_)         'telescope axis park position.

            .endstop_lower_enabled_ = CheckBoxEndStopLowerEnabledZ.Checked
            .endstop_lower_inverted_ = CheckBoxEndstopLowerInvertedZ.Checked
            TextBoxEndstopLowerPinZ.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopLowerPinZ.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxEndstopLowerPinZ, .endstop_lower_)

            .endstop_upper_enabled_ = CheckBoxEndstopUpperEnabledZ.Checked
            .endstop_upper_inverted_ = CheckBoxEndstopUpperInvertedZ.Checked
            TextBoxEndstopUpperPinZ.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopUpperPinZ.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxEndstopUpperPinZ, .endstop_upper_)

            .endstop_hard_stop_ = CheckBoxEndstopHardStopZ.Checked

            .limit_lower_enabled_ = CheckBoxLowerLimitEnabledZ.Checked
            .limit_upper_enabled_ = CheckBoxUpperLimitEnabledZ.Checked
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxLowerLimitZ, .limit_lower_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxUpperLimitZ, .limit_upper_)
            .limit_handling_ = [Enum].Parse(GetType(Telescope.limit_handling_t), ComboBoxLimitHandlingZ.SelectedItem.ToString)
            .limit_hard_stop_ = CheckBoxHardStopLimitZ.Checked


            .motor_driver_ = [Enum].Parse(GetType(Telescope.motor_driver_t), ComboBoxMotorDriverZ.SelectedItem.ToString)

            .i2c_address_ = CByte(Convert.ToInt32(ComboBoxI2CAddressZ.SelectedItem.ToString, 16))
            .terminal_ = CByte(ComboBoxTerminalZ.SelectedItem.ToString)

            TextBoxPin1Z.Text = YAAASharedClassLibrary.parsePin(TextBoxPin1Z.Text)
            TextBoxPin2Z.Text = YAAASharedClassLibrary.parsePin(TextBoxPin2Z.Text)
            TextBoxPin3Z.Text = YAAASharedClassLibrary.parsePin(TextBoxPin3Z.Text)
            TextBoxPin4Z.Text = YAAASharedClassLibrary.parsePin(TextBoxPin4Z.Text)
            TextBoxPin5Z.Text = YAAASharedClassLibrary.parsePin(TextBoxPin5Z.Text)
            TextBoxPin6Z.Text = YAAASharedClassLibrary.parsePin(TextBoxPin6Z.Text)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin1Z, .pin_1_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin2Z, .pin_2_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin3Z, .pin_3_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin4Z, .pin_4_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin5Z, .pin_5_)
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxPin6Z, .pin_6_)

            .pin_1_inverted_ = CheckBoxPin1InvertedZ.Checked
            .pin_2_inverted_ = CheckBoxPin2InvertedZ.Checked
            .pin_3_inverted_ = CheckBoxPin3InvertedZ.Checked
            .pin_4_inverted_ = CheckBoxPin4InvertedZ.Checked
            .pin_5_inverted_ = CheckBoxPin5InvertedZ.Checked
            .pin_6_inverted_ = CheckBoxPin6InvertedZ.Checked
        End With

        'update temperature sensor settings
        For i As Integer = 0 To return_value.telescope_settings_.temperature_sensors_.Length - 1
            valid = valid And user_controls_(i).getSensor(return_value.telescope_settings_.temperature_sensors_(i))
        Next

        If Not valid Then
            Throw New ASCOM.InvalidValueException()
        End If

        Return return_value
    End Function

    Private Sub ButtonExportSettings_Click(sender As Object, e As EventArgs) Handles ButtonExportSettings.Click
        Dim SaveFileDialog1 As New SaveFileDialog
        SaveFileDialog1.AddExtension = True
        SaveFileDialog1.OverwritePrompt = True
        SaveFileDialog1.DefaultExt = "xml"
        SaveFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        SaveFileDialog1.InitialDirectory = Telescope.settingsStoreDirectory
        SaveFileDialog1.FileName = YAAASharedClassLibrary.YAAASETTINGS_FILE_DEFAULT

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            If SaveFileDialog1.FileName <> "" Then
                Try
                    telescope_.xmlFromDriverSettings(SaveFileDialog1.FileName, getSettingsFromMask())
                Catch ex As Exception
                    MsgBox(ex.Message.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub ButtonImportSettings_Click(sender As Object, e As EventArgs) Handles ButtonImportSettings.Click
        Dim OpenFileDialog1 = New OpenFileDialog
        OpenFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        OpenFileDialog1.InitialDirectory = Telescope.settingsStoreDirectory
        OpenFileDialog1.RestoreDirectory = True

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            If OpenFileDialog1.FileName <> "" Then
                Dim settings As Telescope.telescopeDriverSettings = New Telescope.telescopeDriverSettings(True)

                Try
                    settings = telescope_.xmlToDriverSettings(OpenFileDialog1.FileName)
                    setMaskToSettings(settings)
                Catch ex As Exception
                    MsgBox(ex.Message.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub SetupDialogForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        connect(False)
    End Sub

    Private Sub ToolStripSplitButton1_DropDownItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ToolStripSplitButton1.DropDownItemClicked
        Try
            connect(Not telescope_.Connected)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub

End Class
