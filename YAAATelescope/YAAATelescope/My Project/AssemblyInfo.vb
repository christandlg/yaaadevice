﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ASCOM YAAATelescope.Telescope")> 
<Assembly: AssemblyDescription("ASCOM Telescope driver for YAAATelescope")> 
<Assembly: AssemblyCompany("ASCOM Initiative")> 
<Assembly: AssemblyProduct("ASCOM Telescope driver for YAAATelescope")> 
<Assembly: AssemblyCopyright("Copyright © 2014, The ASCOM Intiative")> 
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(True)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e0188156-b2aa-435f-b38b-02afc8ec6e85")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("6.0.0.0")> 
<Assembly: AssemblyFileVersion("6.0.0.0")> 
