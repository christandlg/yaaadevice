<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SetupDialogForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SetupDialogForm))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ButtonWriteEEPROM = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkTrace = New System.Windows.Forms.CheckBox()
        Me.ComboBoxComPorts = New System.Windows.Forms.ComboBox()
        Me.ButtonReadEEPROM = New System.Windows.Forms.Button()
        Me.CheckBoxEEPROMForceWrite = New System.Windows.Forms.CheckBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.GroupBox19 = New System.Windows.Forms.GroupBox()
        Me.GroupBox25 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopUpperInvertedZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopUpperPinZ = New System.Windows.Forms.TextBox()
        Me.CheckBoxEndstopUpperEnabledZ = New System.Windows.Forms.CheckBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.CheckBoxEndstopHardStopZ = New System.Windows.Forms.CheckBox()
        Me.GroupBox24 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopLowerInvertedZ = New System.Windows.Forms.CheckBox()
        Me.CheckBoxEndStopLowerEnabledZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopLowerPinZ = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.GroupBox28 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopUpperInvertedX = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopUpperPinX = New System.Windows.Forms.TextBox()
        Me.CheckBoxEndstopUpperEnabledX = New System.Windows.Forms.CheckBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.GroupBox29 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopLowerInvertedX = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopLowerPinX = New System.Windows.Forms.TextBox()
        Me.CheckBoxEndStopLowerEnabledX = New System.Windows.Forms.CheckBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.CheckBoxEndstopHardStopX = New System.Windows.Forms.CheckBox()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.GroupBox26 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopUpperInvertedY = New System.Windows.Forms.CheckBox()
        Me.CheckBoxEndstopUpperEnabledY = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopUpperPinY = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.GroupBox27 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopLowerInvertedY = New System.Windows.Forms.CheckBox()
        Me.CheckBoxEndStopLowerEnabledY = New System.Windows.Forms.CheckBox()
        Me.TextBoxEndstopLowerPinY = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.CheckBoxEndstopHardStopY = New System.Windows.Forms.CheckBox()
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.TabPageGeneral = New System.Windows.Forms.TabPage()
        Me.GroupBox23 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.RadioButtonNetwork = New System.Windows.Forms.RadioButton()
        Me.RadioButtonSerial = New System.Windows.Forms.RadioButton()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.GroupBox37 = New System.Windows.Forms.GroupBox()
        Me.TextBoxPort = New System.Windows.Forms.TextBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.TextBoxIPAddress = New System.Windows.Forms.TextBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.GroupBox36 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxBaudRate = New System.Windows.Forms.ComboBox()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxMountType = New System.Windows.Forms.ComboBox()
        Me.TextBoxApertureArea = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.TextBoxFocalLength = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.TextBoxApertureDiameter = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.TextBoxSiteLatitude = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TextBoxSiteElevation = New System.Windows.Forms.TextBox()
        Me.TextBoxSiteLongitude = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TabPageAxisControl = New System.Windows.Forms.TabPage()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox18 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxTerminalZ = New System.Windows.Forms.ComboBox()
        Me.ComboBoxI2CAddressZ = New System.Windows.Forms.ComboBox()
        Me.ComboBoxStepTypeSlowZ = New System.Windows.Forms.ComboBox()
        Me.ComboBoxStepTypeFastZ = New System.Windows.Forms.ComboBox()
        Me.CheckBoxPin5InvertedZ = New System.Windows.Forms.CheckBox()
        Me.ComboBoxMotorDriverZ = New System.Windows.Forms.ComboBox()
        Me.CheckBoxPin4InvertedZ = New System.Windows.Forms.CheckBox()
        Me.CheckBoxASRZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxAccelZ = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin6InvertedZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxMaxSpeedZ = New System.Windows.Forms.TextBox()
        Me.TextBoxStepsRevZ = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin3InvertedZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin1Z = New System.Windows.Forms.TextBox()
        Me.TextBoxGearRatioZ = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin2InvertedZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin6Z = New System.Windows.Forms.TextBox()
        Me.TextBoxPin2Z = New System.Windows.Forms.TextBox()
        Me.TextBoxPin3Z = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin1InvertedZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin5Z = New System.Windows.Forms.TextBox()
        Me.TextBoxPin4Z = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxTerminalY = New System.Windows.Forms.ComboBox()
        Me.ComboBoxI2CAddressY = New System.Windows.Forms.ComboBox()
        Me.ComboBoxStepTypeSlowY = New System.Windows.Forms.ComboBox()
        Me.CheckBoxPin5InvertedY = New System.Windows.Forms.CheckBox()
        Me.ComboBoxStepTypeFastY = New System.Windows.Forms.ComboBox()
        Me.ComboBoxMotorDriverY = New System.Windows.Forms.ComboBox()
        Me.CheckBoxPin4InvertedY = New System.Windows.Forms.CheckBox()
        Me.CheckBoxASRY = New System.Windows.Forms.CheckBox()
        Me.TextBoxAccelY = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin6InvertedY = New System.Windows.Forms.CheckBox()
        Me.TextBoxMaxSpeedY = New System.Windows.Forms.TextBox()
        Me.TextBoxStepsRevY = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin3InvertedY = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin1Y = New System.Windows.Forms.TextBox()
        Me.TextBoxGearRatioY = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin2InvertedY = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin6Y = New System.Windows.Forms.TextBox()
        Me.TextBoxPin2Y = New System.Windows.Forms.TextBox()
        Me.TextBoxPin3Y = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin1InvertedY = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin5Y = New System.Windows.Forms.TextBox()
        Me.TextBoxPin4Y = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBoxTerminalX = New System.Windows.Forms.ComboBox()
        Me.ComboBoxI2CAddressX = New System.Windows.Forms.ComboBox()
        Me.ComboBoxStepTypeSlowX = New System.Windows.Forms.ComboBox()
        Me.ComboBoxStepTypeFastX = New System.Windows.Forms.ComboBox()
        Me.CheckBoxPin5InvertedX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPin4InvertedX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPin6InvertedX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxASRX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPin3InvertedX = New System.Windows.Forms.CheckBox()
        Me.TextBoxAccelX = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin2InvertedX = New System.Windows.Forms.CheckBox()
        Me.TextBoxMaxSpeedX = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin1InvertedX = New System.Windows.Forms.CheckBox()
        Me.TextBoxGearRatioX = New System.Windows.Forms.TextBox()
        Me.TextBoxPin5X = New System.Windows.Forms.TextBox()
        Me.TextBoxStepsRevX = New System.Windows.Forms.TextBox()
        Me.TextBoxPin6X = New System.Windows.Forms.TextBox()
        Me.ComboBoxMotorDriverX = New System.Windows.Forms.ComboBox()
        Me.TextBoxPin3X = New System.Windows.Forms.TextBox()
        Me.TextBoxPin4X = New System.Windows.Forms.TextBox()
        Me.TextBoxPin1X = New System.Windows.Forms.TextBox()
        Me.TextBoxPin2X = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabPageEndstopsLimits = New System.Windows.Forms.TabPage()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.GroupBox20 = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.GroupBox34 = New System.Windows.Forms.GroupBox()
        Me.TextBoxUpperLimitZ = New System.Windows.Forms.TextBox()
        Me.CheckBoxUpperLimitEnabledZ = New System.Windows.Forms.CheckBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.CheckBoxHardStopLimitZ = New System.Windows.Forms.CheckBox()
        Me.GroupBox35 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLowerLimitZ = New System.Windows.Forms.TextBox()
        Me.CheckBoxLowerLimitEnabledZ = New System.Windows.Forms.CheckBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.ComboBoxLimitHandlingZ = New System.Windows.Forms.ComboBox()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.GroupBox31 = New System.Windows.Forms.GroupBox()
        Me.TextBoxUpperLimitX = New System.Windows.Forms.TextBox()
        Me.CheckBoxUpperLimitEnabledX = New System.Windows.Forms.CheckBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.GroupBox30 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLowerLimitX = New System.Windows.Forms.TextBox()
        Me.CheckBoxLowerLimitEnabledX = New System.Windows.Forms.CheckBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.CheckBoxHardStopLimitX = New System.Windows.Forms.CheckBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.ComboBoxLimitHandlingX = New System.Windows.Forms.ComboBox()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.GroupBox32 = New System.Windows.Forms.GroupBox()
        Me.TextBoxUpperLimitY = New System.Windows.Forms.TextBox()
        Me.CheckBoxUpperLimitEnabledY = New System.Windows.Forms.CheckBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.GroupBox33 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLowerLimitY = New System.Windows.Forms.TextBox()
        Me.CheckBoxLowerLimitEnabledY = New System.Windows.Forms.CheckBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.CheckBoxHardStopLimitY = New System.Windows.Forms.CheckBox()
        Me.ComboBoxLimitHandlingY = New System.Windows.Forms.ComboBox()
        Me.TabPageHomingParking = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox21 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxHASZ = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSIZ = New System.Windows.Forms.CheckBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.CheckBoxHSAZ = New System.Windows.Forms.CheckBox()
        Me.TextBoxHomeAlignmentZ = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.TextBoxHSNZ = New System.Windows.Forms.TextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxHASY = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSIY = New System.Windows.Forms.CheckBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.CheckBoxHSAY = New System.Windows.Forms.CheckBox()
        Me.TextBoxHomeAlignmentY = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxHSNY = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxHASX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSIX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSAX = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBoxHSNX = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextBoxHomeAlignmentX = New System.Windows.Forms.TextBox()
        Me.CheckBoxSAH = New System.Windows.Forms.CheckBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBoxParkPosZ = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.CheckBoxParkEnabled = New System.Windows.Forms.CheckBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.TextBoxParkPosY = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.TextBoxParkPosX = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabPageManualControl = New System.Windows.Forms.TabPage()
        Me.CheckBoxHSTriggeredZ = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESUTriggeredZ = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESLTriggeredZ = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSTriggeredY = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESUTriggeredY = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESLTriggeredY = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSTriggeredX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESUTriggeredX = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESLTriggeredX = New System.Windows.Forms.CheckBox()
        Me.ButtonReadSwitchState = New System.Windows.Forms.Button()
        Me.GroupBox22 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxMCJA = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMCCSA = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMCTA = New System.Windows.Forms.CheckBox()
        Me.TextBoxMCJoystickAxisX = New System.Windows.Forms.TextBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.TextBoxMCCS = New System.Windows.Forms.TextBox()
        Me.TextBoxMCJoystickAxisY = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.TextBoxMCJoystickDeadZone = New System.Windows.Forms.TextBox()
        Me.TextBoxMCT = New System.Windows.Forms.TextBox()
        Me.CheckBoxMCEnable = New System.Windows.Forms.CheckBox()
        Me.TabPageTemperatureSensors = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanelTemperatureSensors = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.ToolTip2 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.ButtonExportSettings = New System.Windows.Forms.Button()
        Me.ButtonImportSettings = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox19.SuspendLayout()
        Me.GroupBox25.SuspendLayout()
        Me.GroupBox24.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox28.SuspendLayout()
        Me.GroupBox29.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox26.SuspendLayout()
        Me.GroupBox27.SuspendLayout()
        Me.TabControl.SuspendLayout()
        Me.TabPageGeneral.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox37.SuspendLayout()
        Me.GroupBox36.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.TabPageAxisControl.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPageEndstopsLimits.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox20.SuspendLayout()
        Me.GroupBox34.SuspendLayout()
        Me.GroupBox35.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox31.SuspendLayout()
        Me.GroupBox30.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox32.SuspendLayout()
        Me.GroupBox33.SuspendLayout()
        Me.TabPageHomingParking.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox21.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.TabPageManualControl.SuspendLayout()
        Me.GroupBox22.SuspendLayout()
        Me.TabPageTemperatureSensors.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(415, 585)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = Global.ASCOM.YAAADevice.My.Resources.Resources.ASCOM
        Me.PictureBox1.Location = New System.Drawing.Point(487, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 56)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'ButtonWriteEEPROM
        '
        Me.ButtonWriteEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonWriteEEPROM.Cursor = System.Windows.Forms.Cursors.Default
        Me.ButtonWriteEEPROM.Location = New System.Drawing.Point(70, 3)
        Me.ButtonWriteEEPROM.Name = "ButtonWriteEEPROM"
        Me.ButtonWriteEEPROM.Size = New System.Drawing.Size(62, 34)
        Me.ButtonWriteEEPROM.TabIndex = 84
        Me.ButtonWriteEEPROM.Text = "Write EEPROM"
        Me.ToolTip2.SetToolTip(Me.ButtonWriteEEPROM, "Writes settings and updates the Arduino's EEPROM.")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 78
        Me.Label4.Text = "Com Port"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label3.Location = New System.Drawing.Point(194, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 79
        Me.Label3.Text = "Baud Rate"
        '
        'chkTrace
        '
        Me.chkTrace.AutoSize = True
        Me.chkTrace.Location = New System.Drawing.Point(412, 6)
        Me.chkTrace.Name = "chkTrace"
        Me.chkTrace.Size = New System.Drawing.Size(69, 17)
        Me.chkTrace.TabIndex = 3
        Me.chkTrace.Text = "Trace on"
        Me.chkTrace.UseVisualStyleBackColor = True
        '
        'ComboBoxComPorts
        '
        Me.ComboBoxComPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxComPorts.FormattingEnabled = True
        Me.ComboBoxComPorts.Location = New System.Drawing.Point(99, 13)
        Me.ComboBoxComPorts.Name = "ComboBoxComPorts"
        Me.ComboBoxComPorts.Size = New System.Drawing.Size(89, 21)
        Me.ComboBoxComPorts.TabIndex = 0
        Me.ToolTip2.SetToolTip(Me.ComboBoxComPorts, "COM port the Arduino is connected to.")
        '
        'ButtonReadEEPROM
        '
        Me.ButtonReadEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonReadEEPROM.Location = New System.Drawing.Point(3, 3)
        Me.ButtonReadEEPROM.Name = "ButtonReadEEPROM"
        Me.ButtonReadEEPROM.Size = New System.Drawing.Size(61, 34)
        Me.ButtonReadEEPROM.TabIndex = 83
        Me.ButtonReadEEPROM.Text = "Read EEPROM"
        Me.ToolTip2.SetToolTip(Me.ButtonReadEEPROM, "Reads values stored in the Arduino's EEPROM. Does not overwrite settings.")
        '
        'CheckBoxEEPROMForceWrite
        '
        Me.CheckBoxEEPROMForceWrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxEEPROMForceWrite.AutoSize = True
        Me.CheckBoxEEPROMForceWrite.Location = New System.Drawing.Point(3, 7)
        Me.CheckBoxEEPROMForceWrite.Name = "CheckBoxEEPROMForceWrite"
        Me.CheckBoxEEPROMForceWrite.Size = New System.Drawing.Size(127, 17)
        Me.CheckBoxEEPROMForceWrite.TabIndex = 86
        Me.CheckBoxEEPROMForceWrite.Text = "Force EEPROM write"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEEPROMForceWrite, "Enabled: All settings are written to the Arduino's EEPROM." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Disabled: Only change" &
        "d settings are written to the Arduino's EEPROM.")
        Me.CheckBoxEEPROMForceWrite.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.GroupBox19)
        Me.GroupBox10.Controls.Add(Me.GroupBox12)
        Me.GroupBox10.Controls.Add(Me.GroupBox11)
        Me.GroupBox10.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(517, 199)
        Me.GroupBox10.TabIndex = 87
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Endstops"
        '
        'GroupBox19
        '
        Me.GroupBox19.Controls.Add(Me.GroupBox25)
        Me.GroupBox19.Controls.Add(Me.CheckBoxEndstopHardStopZ)
        Me.GroupBox19.Controls.Add(Me.GroupBox24)
        Me.GroupBox19.Location = New System.Drawing.Point(349, 16)
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.Size = New System.Drawing.Size(163, 175)
        Me.GroupBox19.TabIndex = 4
        Me.GroupBox19.TabStop = False
        Me.GroupBox19.Text = "Z Axis"
        '
        'GroupBox25
        '
        Me.GroupBox25.Controls.Add(Me.CheckBoxEndstopUpperInvertedZ)
        Me.GroupBox25.Controls.Add(Me.TextBoxEndstopUpperPinZ)
        Me.GroupBox25.Controls.Add(Me.CheckBoxEndstopUpperEnabledZ)
        Me.GroupBox25.Controls.Add(Me.Label16)
        Me.GroupBox25.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox25.Name = "GroupBox25"
        Me.GroupBox25.Size = New System.Drawing.Size(149, 60)
        Me.GroupBox25.TabIndex = 21
        Me.GroupBox25.TabStop = False
        Me.GroupBox25.Text = "Upper"
        '
        'CheckBoxEndstopUpperInvertedZ
        '
        Me.CheckBoxEndstopUpperInvertedZ.AutoSize = True
        Me.CheckBoxEndstopUpperInvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperInvertedZ.Location = New System.Drawing.Point(78, 35)
        Me.CheckBoxEndstopUpperInvertedZ.Name = "CheckBoxEndstopUpperInvertedZ"
        Me.CheckBoxEndstopUpperInvertedZ.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperInvertedZ.TabIndex = 55
        Me.CheckBoxEndstopUpperInvertedZ.Text = "Inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopUpperInvertedZ, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopUpperInvertedZ.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopUpperPinZ
        '
        Me.TextBoxEndstopUpperPinZ.Location = New System.Drawing.Point(9, 32)
        Me.TextBoxEndstopUpperPinZ.Name = "TextBoxEndstopUpperPinZ"
        Me.TextBoxEndstopUpperPinZ.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxEndstopUpperPinZ.TabIndex = 53
        Me.ToolTip2.SetToolTip(Me.TextBoxEndstopUpperPinZ, "This axis' upper endstop pin number.")
        '
        'CheckBoxEndstopUpperEnabledZ
        '
        Me.CheckBoxEndstopUpperEnabledZ.AutoSize = True
        Me.CheckBoxEndstopUpperEnabledZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperEnabledZ.Location = New System.Drawing.Point(78, 16)
        Me.CheckBoxEndstopUpperEnabledZ.Name = "CheckBoxEndstopUpperEnabledZ"
        Me.CheckBoxEndstopUpperEnabledZ.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperEnabledZ.TabIndex = 54
        Me.CheckBoxEndstopUpperEnabledZ.Text = "Enabled"
        Me.CheckBoxEndstopUpperEnabledZ.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(62, 13)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "Pin Number"
        '
        'CheckBoxEndstopHardStopZ
        '
        Me.CheckBoxEndstopHardStopZ.AutoSize = True
        Me.CheckBoxEndstopHardStopZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopHardStopZ.Location = New System.Drawing.Point(75, 149)
        Me.CheckBoxEndstopHardStopZ.Name = "CheckBoxEndstopHardStopZ"
        Me.CheckBoxEndstopHardStopZ.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxEndstopHardStopZ.TabIndex = 56
        Me.CheckBoxEndstopHardStopZ.Text = "Hard Stop"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopHardStopZ, "When enabled the axis will stop instantly when the Endstop switch is hit.")
        Me.CheckBoxEndstopHardStopZ.UseVisualStyleBackColor = True
        '
        'GroupBox24
        '
        Me.GroupBox24.Controls.Add(Me.CheckBoxEndstopLowerInvertedZ)
        Me.GroupBox24.Controls.Add(Me.CheckBoxEndStopLowerEnabledZ)
        Me.GroupBox24.Controls.Add(Me.TextBoxEndstopLowerPinZ)
        Me.GroupBox24.Controls.Add(Me.Label15)
        Me.GroupBox24.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox24.Name = "GroupBox24"
        Me.GroupBox24.Size = New System.Drawing.Size(149, 58)
        Me.GroupBox24.TabIndex = 17
        Me.GroupBox24.TabStop = False
        Me.GroupBox24.Text = "Lower"
        '
        'CheckBoxEndstopLowerInvertedZ
        '
        Me.CheckBoxEndstopLowerInvertedZ.AutoSize = True
        Me.CheckBoxEndstopLowerInvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopLowerInvertedZ.Location = New System.Drawing.Point(78, 34)
        Me.CheckBoxEndstopLowerInvertedZ.Name = "CheckBoxEndstopLowerInvertedZ"
        Me.CheckBoxEndstopLowerInvertedZ.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopLowerInvertedZ.TabIndex = 52
        Me.CheckBoxEndstopLowerInvertedZ.Text = "Inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopLowerInvertedZ, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopLowerInvertedZ.UseVisualStyleBackColor = True
        '
        'CheckBoxEndStopLowerEnabledZ
        '
        Me.CheckBoxEndStopLowerEnabledZ.AutoSize = True
        Me.CheckBoxEndStopLowerEnabledZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndStopLowerEnabledZ.Location = New System.Drawing.Point(78, 14)
        Me.CheckBoxEndStopLowerEnabledZ.Name = "CheckBoxEndStopLowerEnabledZ"
        Me.CheckBoxEndStopLowerEnabledZ.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndStopLowerEnabledZ.TabIndex = 51
        Me.CheckBoxEndStopLowerEnabledZ.Text = "Enabled"
        Me.CheckBoxEndStopLowerEnabledZ.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopLowerPinZ
        '
        Me.TextBoxEndstopLowerPinZ.Location = New System.Drawing.Point(9, 31)
        Me.TextBoxEndstopLowerPinZ.Name = "TextBoxEndstopLowerPinZ"
        Me.TextBoxEndstopLowerPinZ.Size = New System.Drawing.Size(56, 20)
        Me.TextBoxEndstopLowerPinZ.TabIndex = 50
        Me.ToolTip2.SetToolTip(Me.TextBoxEndstopLowerPinZ, "This axis' lower endstop pin number.")
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 15)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(62, 13)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "Pin Number"
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.GroupBox28)
        Me.GroupBox12.Controls.Add(Me.GroupBox29)
        Me.GroupBox12.Controls.Add(Me.CheckBoxEndstopHardStopX)
        Me.GroupBox12.Location = New System.Drawing.Point(11, 16)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(163, 175)
        Me.GroupBox12.TabIndex = 0
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "X Axis"
        '
        'GroupBox28
        '
        Me.GroupBox28.Controls.Add(Me.CheckBoxEndstopUpperInvertedX)
        Me.GroupBox28.Controls.Add(Me.TextBoxEndstopUpperPinX)
        Me.GroupBox28.Controls.Add(Me.CheckBoxEndstopUpperEnabledX)
        Me.GroupBox28.Controls.Add(Me.Label22)
        Me.GroupBox28.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox28.Name = "GroupBox28"
        Me.GroupBox28.Size = New System.Drawing.Size(149, 60)
        Me.GroupBox28.TabIndex = 23
        Me.GroupBox28.TabStop = False
        Me.GroupBox28.Text = "Upper"
        '
        'CheckBoxEndstopUpperInvertedX
        '
        Me.CheckBoxEndstopUpperInvertedX.AutoSize = True
        Me.CheckBoxEndstopUpperInvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperInvertedX.Location = New System.Drawing.Point(78, 35)
        Me.CheckBoxEndstopUpperInvertedX.Name = "CheckBoxEndstopUpperInvertedX"
        Me.CheckBoxEndstopUpperInvertedX.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperInvertedX.TabIndex = 15
        Me.CheckBoxEndstopUpperInvertedX.Text = "Inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopUpperInvertedX, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopUpperInvertedX.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopUpperPinX
        '
        Me.TextBoxEndstopUpperPinX.Location = New System.Drawing.Point(6, 34)
        Me.TextBoxEndstopUpperPinX.Name = "TextBoxEndstopUpperPinX"
        Me.TextBoxEndstopUpperPinX.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxEndstopUpperPinX.TabIndex = 13
        Me.ToolTip2.SetToolTip(Me.TextBoxEndstopUpperPinX, "This axis' upper endstop pin number.")
        '
        'CheckBoxEndstopUpperEnabledX
        '
        Me.CheckBoxEndstopUpperEnabledX.AutoSize = True
        Me.CheckBoxEndstopUpperEnabledX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperEnabledX.Location = New System.Drawing.Point(78, 16)
        Me.CheckBoxEndstopUpperEnabledX.Name = "CheckBoxEndstopUpperEnabledX"
        Me.CheckBoxEndstopUpperEnabledX.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperEnabledX.TabIndex = 14
        Me.CheckBoxEndstopUpperEnabledX.Text = "Enabled"
        Me.CheckBoxEndstopUpperEnabledX.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 16)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(62, 13)
        Me.Label22.TabIndex = 20
        Me.Label22.Text = "Pin Number"
        '
        'GroupBox29
        '
        Me.GroupBox29.Controls.Add(Me.CheckBoxEndstopLowerInvertedX)
        Me.GroupBox29.Controls.Add(Me.TextBoxEndstopLowerPinX)
        Me.GroupBox29.Controls.Add(Me.CheckBoxEndStopLowerEnabledX)
        Me.GroupBox29.Controls.Add(Me.Label40)
        Me.GroupBox29.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox29.Name = "GroupBox29"
        Me.GroupBox29.Size = New System.Drawing.Size(149, 58)
        Me.GroupBox29.TabIndex = 22
        Me.GroupBox29.TabStop = False
        Me.GroupBox29.Text = "Lower"
        '
        'CheckBoxEndstopLowerInvertedX
        '
        Me.CheckBoxEndstopLowerInvertedX.AutoSize = True
        Me.CheckBoxEndstopLowerInvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopLowerInvertedX.Location = New System.Drawing.Point(78, 33)
        Me.CheckBoxEndstopLowerInvertedX.Name = "CheckBoxEndstopLowerInvertedX"
        Me.CheckBoxEndstopLowerInvertedX.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopLowerInvertedX.TabIndex = 12
        Me.CheckBoxEndstopLowerInvertedX.Text = "Inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopLowerInvertedX, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopLowerInvertedX.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopLowerPinX
        '
        Me.TextBoxEndstopLowerPinX.Location = New System.Drawing.Point(4, 32)
        Me.TextBoxEndstopLowerPinX.Name = "TextBoxEndstopLowerPinX"
        Me.TextBoxEndstopLowerPinX.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxEndstopLowerPinX.TabIndex = 10
        Me.ToolTip2.SetToolTip(Me.TextBoxEndstopLowerPinX, "This axis' lower endstop pin number.")
        '
        'CheckBoxEndStopLowerEnabledX
        '
        Me.CheckBoxEndStopLowerEnabledX.AutoSize = True
        Me.CheckBoxEndStopLowerEnabledX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndStopLowerEnabledX.Location = New System.Drawing.Point(78, 14)
        Me.CheckBoxEndStopLowerEnabledX.Name = "CheckBoxEndStopLowerEnabledX"
        Me.CheckBoxEndStopLowerEnabledX.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndStopLowerEnabledX.TabIndex = 11
        Me.CheckBoxEndStopLowerEnabledX.Text = "Enabled"
        Me.CheckBoxEndStopLowerEnabledX.UseVisualStyleBackColor = True
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(4, 15)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(62, 13)
        Me.Label40.TabIndex = 15
        Me.Label40.Text = "Pin Number"
        '
        'CheckBoxEndstopHardStopX
        '
        Me.CheckBoxEndstopHardStopX.AutoSize = True
        Me.CheckBoxEndstopHardStopX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopHardStopX.Location = New System.Drawing.Point(75, 149)
        Me.CheckBoxEndstopHardStopX.Name = "CheckBoxEndstopHardStopX"
        Me.CheckBoxEndstopHardStopX.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxEndstopHardStopX.TabIndex = 16
        Me.CheckBoxEndstopHardStopX.Text = "Hard Stop"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopHardStopX, "When enabled the axis will stop instantly when the Endstop switch is hit.")
        Me.CheckBoxEndstopHardStopX.UseVisualStyleBackColor = True
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.GroupBox26)
        Me.GroupBox11.Controls.Add(Me.GroupBox27)
        Me.GroupBox11.Controls.Add(Me.CheckBoxEndstopHardStopY)
        Me.GroupBox11.Location = New System.Drawing.Point(180, 16)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(163, 175)
        Me.GroupBox11.TabIndex = 2
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Y Axis"
        '
        'GroupBox26
        '
        Me.GroupBox26.Controls.Add(Me.CheckBoxEndstopUpperInvertedY)
        Me.GroupBox26.Controls.Add(Me.CheckBoxEndstopUpperEnabledY)
        Me.GroupBox26.Controls.Add(Me.TextBoxEndstopUpperPinY)
        Me.GroupBox26.Controls.Add(Me.Label23)
        Me.GroupBox26.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox26.Name = "GroupBox26"
        Me.GroupBox26.Size = New System.Drawing.Size(149, 60)
        Me.GroupBox26.TabIndex = 23
        Me.GroupBox26.TabStop = False
        Me.GroupBox26.Text = "Upper"
        '
        'CheckBoxEndstopUpperInvertedY
        '
        Me.CheckBoxEndstopUpperInvertedY.AutoSize = True
        Me.CheckBoxEndstopUpperInvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperInvertedY.Location = New System.Drawing.Point(78, 35)
        Me.CheckBoxEndstopUpperInvertedY.Name = "CheckBoxEndstopUpperInvertedY"
        Me.CheckBoxEndstopUpperInvertedY.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperInvertedY.TabIndex = 35
        Me.CheckBoxEndstopUpperInvertedY.Text = "Inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopUpperInvertedY, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopUpperInvertedY.UseVisualStyleBackColor = True
        '
        'CheckBoxEndstopUpperEnabledY
        '
        Me.CheckBoxEndstopUpperEnabledY.AutoSize = True
        Me.CheckBoxEndstopUpperEnabledY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperEnabledY.Location = New System.Drawing.Point(78, 16)
        Me.CheckBoxEndstopUpperEnabledY.Name = "CheckBoxEndstopUpperEnabledY"
        Me.CheckBoxEndstopUpperEnabledY.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperEnabledY.TabIndex = 34
        Me.CheckBoxEndstopUpperEnabledY.Text = "Enabled"
        Me.CheckBoxEndstopUpperEnabledY.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopUpperPinY
        '
        Me.TextBoxEndstopUpperPinY.Location = New System.Drawing.Point(6, 32)
        Me.TextBoxEndstopUpperPinY.Name = "TextBoxEndstopUpperPinY"
        Me.TextBoxEndstopUpperPinY.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxEndstopUpperPinY.TabIndex = 33
        Me.ToolTip2.SetToolTip(Me.TextBoxEndstopUpperPinY, "This axis' upper endstop pin number.")
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(6, 16)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(62, 13)
        Me.Label23.TabIndex = 20
        Me.Label23.Text = "Pin Number"
        '
        'GroupBox27
        '
        Me.GroupBox27.Controls.Add(Me.CheckBoxEndstopLowerInvertedY)
        Me.GroupBox27.Controls.Add(Me.CheckBoxEndStopLowerEnabledY)
        Me.GroupBox27.Controls.Add(Me.TextBoxEndstopLowerPinY)
        Me.GroupBox27.Controls.Add(Me.Label26)
        Me.GroupBox27.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox27.Name = "GroupBox27"
        Me.GroupBox27.Size = New System.Drawing.Size(149, 58)
        Me.GroupBox27.TabIndex = 22
        Me.GroupBox27.TabStop = False
        Me.GroupBox27.Text = "Lower"
        '
        'CheckBoxEndstopLowerInvertedY
        '
        Me.CheckBoxEndstopLowerInvertedY.AutoSize = True
        Me.CheckBoxEndstopLowerInvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopLowerInvertedY.Location = New System.Drawing.Point(78, 34)
        Me.CheckBoxEndstopLowerInvertedY.Name = "CheckBoxEndstopLowerInvertedY"
        Me.CheckBoxEndstopLowerInvertedY.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopLowerInvertedY.TabIndex = 32
        Me.CheckBoxEndstopLowerInvertedY.Text = "Inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopLowerInvertedY, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopLowerInvertedY.UseVisualStyleBackColor = True
        '
        'CheckBoxEndStopLowerEnabledY
        '
        Me.CheckBoxEndStopLowerEnabledY.AutoSize = True
        Me.CheckBoxEndStopLowerEnabledY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndStopLowerEnabledY.Location = New System.Drawing.Point(78, 14)
        Me.CheckBoxEndStopLowerEnabledY.Name = "CheckBoxEndStopLowerEnabledY"
        Me.CheckBoxEndStopLowerEnabledY.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndStopLowerEnabledY.TabIndex = 31
        Me.CheckBoxEndStopLowerEnabledY.Text = "Enabled"
        Me.CheckBoxEndStopLowerEnabledY.UseVisualStyleBackColor = True
        '
        'TextBoxEndstopLowerPinY
        '
        Me.TextBoxEndstopLowerPinY.Location = New System.Drawing.Point(6, 31)
        Me.TextBoxEndstopLowerPinY.Name = "TextBoxEndstopLowerPinY"
        Me.TextBoxEndstopLowerPinY.Size = New System.Drawing.Size(57, 20)
        Me.TextBoxEndstopLowerPinY.TabIndex = 30
        Me.ToolTip2.SetToolTip(Me.TextBoxEndstopLowerPinY, "This axis' lower endstop pin number.")
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(6, 15)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(62, 13)
        Me.Label26.TabIndex = 15
        Me.Label26.Text = "Pin Number"
        '
        'CheckBoxEndstopHardStopY
        '
        Me.CheckBoxEndstopHardStopY.AutoSize = True
        Me.CheckBoxEndstopHardStopY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopHardStopY.Location = New System.Drawing.Point(75, 149)
        Me.CheckBoxEndstopHardStopY.Name = "CheckBoxEndstopHardStopY"
        Me.CheckBoxEndstopHardStopY.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxEndstopHardStopY.TabIndex = 36
        Me.CheckBoxEndstopHardStopY.Text = "Hard Stop"
        Me.ToolTip2.SetToolTip(Me.CheckBoxEndstopHardStopY, "When enabled the axis will stop instantly when the Endstop switch is hit.")
        Me.CheckBoxEndstopHardStopY.UseVisualStyleBackColor = True
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabPageGeneral)
        Me.TabControl.Controls.Add(Me.TabPageAxisControl)
        Me.TabControl.Controls.Add(Me.TabPageEndstopsLimits)
        Me.TabControl.Controls.Add(Me.TabPageHomingParking)
        Me.TabControl.Controls.Add(Me.TabPageManualControl)
        Me.TabControl.Controls.Add(Me.TabPageTemperatureSensors)
        Me.TabControl.Location = New System.Drawing.Point(12, 12)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(549, 523)
        Me.TabControl.TabIndex = 88
        '
        'TabPageGeneral
        '
        Me.TabPageGeneral.Controls.Add(Me.GroupBox23)
        Me.TabPageGeneral.Controls.Add(Me.GroupBox17)
        Me.TabPageGeneral.Controls.Add(Me.GroupBox16)
        Me.TabPageGeneral.Controls.Add(Me.chkTrace)
        Me.TabPageGeneral.Controls.Add(Me.PictureBox1)
        Me.TabPageGeneral.Location = New System.Drawing.Point(4, 22)
        Me.TabPageGeneral.Name = "TabPageGeneral"
        Me.TabPageGeneral.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageGeneral.Size = New System.Drawing.Size(541, 497)
        Me.TabPageGeneral.TabIndex = 0
        Me.TabPageGeneral.Text = "General"
        Me.TabPageGeneral.UseVisualStyleBackColor = True
        '
        'GroupBox23
        '
        Me.GroupBox23.Controls.Add(Me.Panel1)
        Me.GroupBox23.Controls.Add(Me.Label44)
        Me.GroupBox23.Controls.Add(Me.GroupBox37)
        Me.GroupBox23.Controls.Add(Me.GroupBox36)
        Me.GroupBox23.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Size = New System.Drawing.Size(382, 152)
        Me.GroupBox23.TabIndex = 89
        Me.GroupBox23.TabStop = False
        Me.GroupBox23.Text = "Communication"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RadioButtonNetwork)
        Me.Panel1.Controls.Add(Me.RadioButtonSerial)
        Me.Panel1.Location = New System.Drawing.Point(96, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(280, 35)
        Me.Panel1.TabIndex = 90
        '
        'RadioButtonNetwork
        '
        Me.RadioButtonNetwork.AutoSize = True
        Me.RadioButtonNetwork.Location = New System.Drawing.Point(103, 9)
        Me.RadioButtonNetwork.Name = "RadioButtonNetwork"
        Me.RadioButtonNetwork.Size = New System.Drawing.Size(65, 17)
        Me.RadioButtonNetwork.TabIndex = 90
        Me.RadioButtonNetwork.TabStop = True
        Me.RadioButtonNetwork.Text = "Network"
        Me.RadioButtonNetwork.UseVisualStyleBackColor = True
        '
        'RadioButtonSerial
        '
        Me.RadioButtonSerial.AutoSize = True
        Me.RadioButtonSerial.Location = New System.Drawing.Point(14, 9)
        Me.RadioButtonSerial.Name = "RadioButtonSerial"
        Me.RadioButtonSerial.Size = New System.Drawing.Size(51, 17)
        Me.RadioButtonSerial.TabIndex = 90
        Me.RadioButtonSerial.TabStop = True
        Me.RadioButtonSerial.Text = "Serial"
        Me.RadioButtonSerial.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(12, 23)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(78, 13)
        Me.Label44.TabIndex = 90
        Me.Label44.Text = "Connect using:"
        '
        'GroupBox37
        '
        Me.GroupBox37.Controls.Add(Me.TextBoxPort)
        Me.GroupBox37.Controls.Add(Me.Label60)
        Me.GroupBox37.Controls.Add(Me.TextBoxIPAddress)
        Me.GroupBox37.Controls.Add(Me.Label58)
        Me.GroupBox37.Location = New System.Drawing.Point(6, 101)
        Me.GroupBox37.Name = "GroupBox37"
        Me.GroupBox37.Size = New System.Drawing.Size(370, 46)
        Me.GroupBox37.TabIndex = 91
        Me.GroupBox37.TabStop = False
        Me.GroupBox37.Text = "Network"
        '
        'TextBoxPort
        '
        Me.TextBoxPort.Location = New System.Drawing.Point(275, 19)
        Me.TextBoxPort.Name = "TextBoxPort"
        Me.TextBoxPort.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxPort.TabIndex = 91
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(194, 22)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(26, 13)
        Me.Label60.TabIndex = 81
        Me.Label60.Text = "Port"
        '
        'TextBoxIPAddress
        '
        Me.TextBoxIPAddress.Location = New System.Drawing.Point(99, 19)
        Me.TextBoxIPAddress.Name = "TextBoxIPAddress"
        Me.TextBoxIPAddress.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxIPAddress.TabIndex = 90
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(14, 22)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(17, 13)
        Me.Label58.TabIndex = 80
        Me.Label58.Text = "IP"
        '
        'GroupBox36
        '
        Me.GroupBox36.Controls.Add(Me.Label4)
        Me.GroupBox36.Controls.Add(Me.Label3)
        Me.GroupBox36.Controls.Add(Me.ComboBoxBaudRate)
        Me.GroupBox36.Controls.Add(Me.ComboBoxComPorts)
        Me.GroupBox36.Location = New System.Drawing.Point(6, 53)
        Me.GroupBox36.Name = "GroupBox36"
        Me.GroupBox36.Size = New System.Drawing.Size(370, 42)
        Me.GroupBox36.TabIndex = 90
        Me.GroupBox36.TabStop = False
        Me.GroupBox36.Text = "Serial"
        '
        'ComboBoxBaudRate
        '
        Me.ComboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBaudRate.FormattingEnabled = True
        Me.ComboBoxBaudRate.Items.AddRange(New Object() {"300", "600", "1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "115200"})
        Me.ComboBoxBaudRate.Location = New System.Drawing.Point(275, 13)
        Me.ComboBoxBaudRate.Name = "ComboBoxBaudRate"
        Me.ComboBoxBaudRate.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ComboBoxBaudRate.Size = New System.Drawing.Size(89, 21)
        Me.ComboBoxBaudRate.TabIndex = 2
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.ComboBoxMountType)
        Me.GroupBox17.Controls.Add(Me.TextBoxApertureArea)
        Me.GroupBox17.Controls.Add(Me.Label39)
        Me.GroupBox17.Controls.Add(Me.Label36)
        Me.GroupBox17.Controls.Add(Me.TextBoxFocalLength)
        Me.GroupBox17.Controls.Add(Me.Label37)
        Me.GroupBox17.Controls.Add(Me.TextBoxApertureDiameter)
        Me.GroupBox17.Controls.Add(Me.Label38)
        Me.GroupBox17.Location = New System.Drawing.Point(6, 238)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(382, 100)
        Me.GroupBox17.TabIndex = 88
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "Telescope Information"
        '
        'ComboBoxMountType
        '
        Me.ComboBoxMountType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.ComboBoxMountType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ComboBoxMountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMountType.FormattingEnabled = True
        Me.ComboBoxMountType.Location = New System.Drawing.Point(105, 73)
        Me.ComboBoxMountType.Name = "ComboBoxMountType"
        Me.ComboBoxMountType.Size = New System.Drawing.Size(265, 21)
        Me.ComboBoxMountType.TabIndex = 10
        '
        'TextBoxApertureArea
        '
        Me.TextBoxApertureArea.Location = New System.Drawing.Point(281, 47)
        Me.TextBoxApertureArea.Name = "TextBoxApertureArea"
        Me.TextBoxApertureArea.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxApertureArea.TabIndex = 9
        Me.ToolTip2.SetToolTip(Me.TextBoxApertureArea, "The Telescope's Aperture Area in square meters.")
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label39.Location = New System.Drawing.Point(200, 50)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(72, 13)
        Me.Label39.TabIndex = 90
        Me.Label39.Text = "Aperture Area"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label36.Location = New System.Drawing.Point(6, 24)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(69, 13)
        Me.Label36.TabIndex = 86
        Me.Label36.Text = "Focal Length"
        '
        'TextBoxFocalLength
        '
        Me.TextBoxFocalLength.Location = New System.Drawing.Point(105, 21)
        Me.TextBoxFocalLength.Name = "TextBoxFocalLength"
        Me.TextBoxFocalLength.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxFocalLength.TabIndex = 7
        Me.ToolTip2.SetToolTip(Me.TextBoxFocalLength, "The Telescope's Focal Length in meters.")
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label37.Location = New System.Drawing.Point(6, 76)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(64, 13)
        Me.Label37.TabIndex = 82
        Me.Label37.Text = "Mount Type"
        '
        'TextBoxApertureDiameter
        '
        Me.TextBoxApertureDiameter.Location = New System.Drawing.Point(281, 21)
        Me.TextBoxApertureDiameter.Name = "TextBoxApertureDiameter"
        Me.TextBoxApertureDiameter.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxApertureDiameter.TabIndex = 8
        Me.ToolTip2.SetToolTip(Me.TextBoxApertureDiameter, "The Telescope's Aperture Diameter in meters.")
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label38.Location = New System.Drawing.Point(200, 24)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(77, 13)
        Me.Label38.TabIndex = 84
        Me.Label38.Text = "Aperture Diam."
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.Label32)
        Me.GroupBox16.Controls.Add(Me.TextBoxSiteLatitude)
        Me.GroupBox16.Controls.Add(Me.Label30)
        Me.GroupBox16.Controls.Add(Me.TextBoxSiteElevation)
        Me.GroupBox16.Controls.Add(Me.TextBoxSiteLongitude)
        Me.GroupBox16.Controls.Add(Me.Label31)
        Me.GroupBox16.Location = New System.Drawing.Point(6, 164)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(382, 68)
        Me.GroupBox16.TabIndex = 87
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Telescope Site"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label32.Location = New System.Drawing.Point(6, 19)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(45, 13)
        Me.Label32.TabIndex = 86
        Me.Label32.Text = "Latitude"
        '
        'TextBoxSiteLatitude
        '
        Me.TextBoxSiteLatitude.Location = New System.Drawing.Point(105, 16)
        Me.TextBoxSiteLatitude.Name = "TextBoxSiteLatitude"
        Me.TextBoxSiteLatitude.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxSiteLatitude.TabIndex = 4
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label30.Location = New System.Drawing.Point(6, 45)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(51, 13)
        Me.Label30.TabIndex = 82
        Me.Label30.Text = "Elevation"
        '
        'TextBoxSiteElevation
        '
        Me.TextBoxSiteElevation.Location = New System.Drawing.Point(105, 42)
        Me.TextBoxSiteElevation.Name = "TextBoxSiteElevation"
        Me.TextBoxSiteElevation.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxSiteElevation.TabIndex = 6
        '
        'TextBoxSiteLongitude
        '
        Me.TextBoxSiteLongitude.Location = New System.Drawing.Point(281, 16)
        Me.TextBoxSiteLongitude.Name = "TextBoxSiteLongitude"
        Me.TextBoxSiteLongitude.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxSiteLongitude.TabIndex = 5
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label31.Location = New System.Drawing.Point(200, 19)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(54, 13)
        Me.Label31.TabIndex = 84
        Me.Label31.Text = "Longitude"
        '
        'TabPageAxisControl
        '
        Me.TabPageAxisControl.Controls.Add(Me.Label49)
        Me.TabPageAxisControl.Controls.Add(Me.Label47)
        Me.TabPageAxisControl.Controls.Add(Me.Label48)
        Me.TabPageAxisControl.Controls.Add(Me.Label12)
        Me.TabPageAxisControl.Controls.Add(Me.Label46)
        Me.TabPageAxisControl.Controls.Add(Me.Label6)
        Me.TabPageAxisControl.Controls.Add(Me.Label7)
        Me.TabPageAxisControl.Controls.Add(Me.Label45)
        Me.TabPageAxisControl.Controls.Add(Me.Label14)
        Me.TabPageAxisControl.Controls.Add(Me.GroupBox18)
        Me.TabPageAxisControl.Controls.Add(Me.GroupBox4)
        Me.TabPageAxisControl.Controls.Add(Me.GroupBox1)
        Me.TabPageAxisControl.Controls.Add(Me.Label34)
        Me.TabPageAxisControl.Controls.Add(Me.Label9)
        Me.TabPageAxisControl.Controls.Add(Me.Label13)
        Me.TabPageAxisControl.Controls.Add(Me.Label8)
        Me.TabPageAxisControl.Controls.Add(Me.Label10)
        Me.TabPageAxisControl.Controls.Add(Me.Label11)
        Me.TabPageAxisControl.Location = New System.Drawing.Point(4, 22)
        Me.TabPageAxisControl.Name = "TabPageAxisControl"
        Me.TabPageAxisControl.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageAxisControl.Size = New System.Drawing.Size(541, 497)
        Me.TabPageAxisControl.TabIndex = 1
        Me.TabPageAxisControl.Text = "Axis Control"
        Me.TabPageAxisControl.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(3, 220)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(92, 13)
        Me.Label49.TabIndex = 113
        Me.Label49.Text = "Motor Driver Type"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label47.Location = New System.Drawing.Point(3, 427)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(64, 13)
        Me.Label47.TabIndex = 111
        Me.Label47.Text = "Pin 6 / MS3"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(3, 401)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(64, 13)
        Me.Label48.TabIndex = 112
        Me.Label48.Text = "Pin 5 / MS2"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label12.Location = New System.Drawing.Point(3, 375)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 13)
        Me.Label12.TabIndex = 109
        Me.Label12.Text = "Pin 4 / MS1"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(3, 349)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(75, 13)
        Me.Label46.TabIndex = 110
        Me.Label46.Text = "Pin 3 / Enable"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label6.Location = New System.Drawing.Point(3, 323)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 107
        Me.Label6.Text = "Pin 2 / Direction"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 297)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 108
        Me.Label7.Text = "Pin 1 / Step"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(3, 246)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(112, 13)
        Me.Label45.TabIndex = 106
        Me.Label45.Text = "I�C Address / Terminal"
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(3, 177)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(102, 26)
        Me.Label14.TabIndex = 101
        Me.Label14.Text = "Automatic Stepper Release"
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.ComboBoxTerminalZ)
        Me.GroupBox18.Controls.Add(Me.ComboBoxI2CAddressZ)
        Me.GroupBox18.Controls.Add(Me.ComboBoxStepTypeSlowZ)
        Me.GroupBox18.Controls.Add(Me.ComboBoxStepTypeFastZ)
        Me.GroupBox18.Controls.Add(Me.CheckBoxPin5InvertedZ)
        Me.GroupBox18.Controls.Add(Me.ComboBoxMotorDriverZ)
        Me.GroupBox18.Controls.Add(Me.CheckBoxPin4InvertedZ)
        Me.GroupBox18.Controls.Add(Me.CheckBoxASRZ)
        Me.GroupBox18.Controls.Add(Me.TextBoxAccelZ)
        Me.GroupBox18.Controls.Add(Me.CheckBoxPin6InvertedZ)
        Me.GroupBox18.Controls.Add(Me.TextBoxMaxSpeedZ)
        Me.GroupBox18.Controls.Add(Me.TextBoxStepsRevZ)
        Me.GroupBox18.Controls.Add(Me.CheckBoxPin3InvertedZ)
        Me.GroupBox18.Controls.Add(Me.TextBoxPin1Z)
        Me.GroupBox18.Controls.Add(Me.TextBoxGearRatioZ)
        Me.GroupBox18.Controls.Add(Me.CheckBoxPin2InvertedZ)
        Me.GroupBox18.Controls.Add(Me.TextBoxPin6Z)
        Me.GroupBox18.Controls.Add(Me.TextBoxPin2Z)
        Me.GroupBox18.Controls.Add(Me.TextBoxPin3Z)
        Me.GroupBox18.Controls.Add(Me.CheckBoxPin1InvertedZ)
        Me.GroupBox18.Controls.Add(Me.TextBoxPin5Z)
        Me.GroupBox18.Controls.Add(Me.TextBoxPin4Z)
        Me.GroupBox18.Controls.Add(Me.Label42)
        Me.GroupBox18.Controls.Add(Me.Label52)
        Me.GroupBox18.Location = New System.Drawing.Point(401, 6)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(134, 448)
        Me.GroupBox18.TabIndex = 3
        Me.GroupBox18.TabStop = False
        Me.GroupBox18.Text = "Z Axis"
        '
        'ComboBoxTerminalZ
        '
        Me.ComboBoxTerminalZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTerminalZ.DropDownWidth = 49
        Me.ComboBoxTerminalZ.FormattingEnabled = True
        Me.ComboBoxTerminalZ.Items.AddRange(New Object() {"1", "2"})
        Me.ComboBoxTerminalZ.Location = New System.Drawing.Point(79, 238)
        Me.ComboBoxTerminalZ.Name = "ComboBoxTerminalZ"
        Me.ComboBoxTerminalZ.Size = New System.Drawing.Size(49, 21)
        Me.ComboBoxTerminalZ.TabIndex = 89
        Me.ToolTip2.SetToolTip(Me.ComboBoxTerminalZ, "Adafruit Motor Shield V2 only.")
        '
        'ComboBoxI2CAddressZ
        '
        Me.ComboBoxI2CAddressZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxI2CAddressZ.DropDownWidth = 67
        Me.ComboBoxI2CAddressZ.FormattingEnabled = True
        Me.ComboBoxI2CAddressZ.Items.AddRange(New Object() {"0x60", "0x61", "0x62", "0x63", "0x64", "0x65", "0x66", "0x67", "0x68", "0x69", "0x6A", "0x6B", "0x6C", "0x6D", "0x6E", "0x6F"})
        Me.ComboBoxI2CAddressZ.Location = New System.Drawing.Point(6, 238)
        Me.ComboBoxI2CAddressZ.Name = "ComboBoxI2CAddressZ"
        Me.ComboBoxI2CAddressZ.Size = New System.Drawing.Size(67, 21)
        Me.ComboBoxI2CAddressZ.TabIndex = 88
        Me.ToolTip2.SetToolTip(Me.ComboBoxI2CAddressZ, "Adafruit Motor Shield V2 only.")
        '
        'ComboBoxStepTypeSlowZ
        '
        Me.ComboBoxStepTypeSlowZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepTypeSlowZ.DropDownWidth = 160
        Me.ComboBoxStepTypeSlowZ.FormattingEnabled = True
        Me.ComboBoxStepTypeSlowZ.Location = New System.Drawing.Point(6, 143)
        Me.ComboBoxStepTypeSlowZ.Name = "ComboBoxStepTypeSlowZ"
        Me.ComboBoxStepTypeSlowZ.Size = New System.Drawing.Size(120, 21)
        Me.ComboBoxStepTypeSlowZ.TabIndex = 85
        Me.ToolTip2.SetToolTip(Me.ComboBoxStepTypeSlowZ, "Steptype for ""slow"" movements (Tracking, Pulse Guiding).")
        '
        'ComboBoxStepTypeFastZ
        '
        Me.ComboBoxStepTypeFastZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepTypeFastZ.DropDownWidth = 160
        Me.ComboBoxStepTypeFastZ.FormattingEnabled = True
        Me.ComboBoxStepTypeFastZ.Location = New System.Drawing.Point(6, 118)
        Me.ComboBoxStepTypeFastZ.Name = "ComboBoxStepTypeFastZ"
        Me.ComboBoxStepTypeFastZ.Size = New System.Drawing.Size(120, 21)
        Me.ComboBoxStepTypeFastZ.TabIndex = 84
        Me.ToolTip2.SetToolTip(Me.ComboBoxStepTypeFastZ, "Steptype for ""fast"" movements (Slew, MoveAxis)")
        '
        'CheckBoxPin5InvertedZ
        '
        Me.CheckBoxPin5InvertedZ.AutoSize = True
        Me.CheckBoxPin5InvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5InvertedZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5InvertedZ.Location = New System.Drawing.Point(110, 395)
        Me.CheckBoxPin5InvertedZ.Name = "CheckBoxPin5InvertedZ"
        Me.CheckBoxPin5InvertedZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin5InvertedZ.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin5InvertedZ.TabIndex = 100
        Me.CheckBoxPin5InvertedZ.UseVisualStyleBackColor = True
        '
        'ComboBoxMotorDriverZ
        '
        Me.ComboBoxMotorDriverZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMotorDriverZ.DropDownWidth = 200
        Me.ComboBoxMotorDriverZ.FormattingEnabled = True
        Me.ComboBoxMotorDriverZ.Location = New System.Drawing.Point(6, 211)
        Me.ComboBoxMotorDriverZ.Name = "ComboBoxMotorDriverZ"
        Me.ComboBoxMotorDriverZ.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxMotorDriverZ.TabIndex = 87
        Me.ToolTip2.SetToolTip(Me.ComboBoxMotorDriverZ, "This axis' Stepper Motor Driver type.")
        '
        'CheckBoxPin4InvertedZ
        '
        Me.CheckBoxPin4InvertedZ.AutoSize = True
        Me.CheckBoxPin4InvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4InvertedZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4InvertedZ.Location = New System.Drawing.Point(110, 369)
        Me.CheckBoxPin4InvertedZ.Name = "CheckBoxPin4InvertedZ"
        Me.CheckBoxPin4InvertedZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin4InvertedZ.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin4InvertedZ.TabIndex = 99
        Me.CheckBoxPin4InvertedZ.UseVisualStyleBackColor = True
        '
        'CheckBoxASRZ
        '
        Me.CheckBoxASRZ.AutoSize = True
        Me.CheckBoxASRZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASRZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASRZ.Location = New System.Drawing.Point(61, 171)
        Me.CheckBoxASRZ.Name = "CheckBoxASRZ"
        Me.CheckBoxASRZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxASRZ.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxASRZ.TabIndex = 86
        Me.CheckBoxASRZ.Text = "Enabled"
        Me.CheckBoxASRZ.UseVisualStyleBackColor = True
        '
        'TextBoxAccelZ
        '
        Me.TextBoxAccelZ.Location = New System.Drawing.Point(6, 43)
        Me.TextBoxAccelZ.Name = "TextBoxAccelZ"
        Me.TextBoxAccelZ.Size = New System.Drawing.Size(120, 20)
        Me.TextBoxAccelZ.TabIndex = 81
        Me.ToolTip2.SetToolTip(Me.TextBoxAccelZ, "Acceleration of this axis' stepper motor in steps / second�.")
        '
        'CheckBoxPin6InvertedZ
        '
        Me.CheckBoxPin6InvertedZ.AutoSize = True
        Me.CheckBoxPin6InvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6InvertedZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6InvertedZ.Location = New System.Drawing.Point(110, 421)
        Me.CheckBoxPin6InvertedZ.Name = "CheckBoxPin6InvertedZ"
        Me.CheckBoxPin6InvertedZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin6InvertedZ.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin6InvertedZ.TabIndex = 101
        Me.CheckBoxPin6InvertedZ.UseVisualStyleBackColor = True
        '
        'TextBoxMaxSpeedZ
        '
        Me.TextBoxMaxSpeedZ.Location = New System.Drawing.Point(6, 19)
        Me.TextBoxMaxSpeedZ.Name = "TextBoxMaxSpeedZ"
        Me.TextBoxMaxSpeedZ.Size = New System.Drawing.Size(120, 20)
        Me.TextBoxMaxSpeedZ.TabIndex = 80
        Me.ToolTip2.SetToolTip(Me.TextBoxMaxSpeedZ, "The maximum speed of this axis' stepper motor in steps / second.")
        '
        'TextBoxStepsRevZ
        '
        Me.TextBoxStepsRevZ.Location = New System.Drawing.Point(6, 69)
        Me.TextBoxStepsRevZ.Name = "TextBoxStepsRevZ"
        Me.TextBoxStepsRevZ.Size = New System.Drawing.Size(120, 20)
        Me.TextBoxStepsRevZ.TabIndex = 82
        Me.ToolTip2.SetToolTip(Me.TextBoxStepsRevZ, "This axis' stepper motor's number of steps per revolution of the stepper motor.")
        '
        'CheckBoxPin3InvertedZ
        '
        Me.CheckBoxPin3InvertedZ.AutoSize = True
        Me.CheckBoxPin3InvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3InvertedZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3InvertedZ.Location = New System.Drawing.Point(110, 343)
        Me.CheckBoxPin3InvertedZ.Name = "CheckBoxPin3InvertedZ"
        Me.CheckBoxPin3InvertedZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin3InvertedZ.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin3InvertedZ.TabIndex = 98
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin3InvertedZ, "Unchecked: Rising edge initiates step." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Falling edge initiates step.")
        Me.CheckBoxPin3InvertedZ.UseVisualStyleBackColor = True
        '
        'TextBoxPin1Z
        '
        Me.TextBoxPin1Z.Location = New System.Drawing.Point(9, 288)
        Me.TextBoxPin1Z.Name = "TextBoxPin1Z"
        Me.TextBoxPin1Z.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin1Z.TabIndex = 90
        Me.ToolTip2.SetToolTip(Me.TextBoxPin1Z, "This axis' stepper motor pin 1 or stepper driver enable pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper a" &
        "nd A4988 driver only.")
        '
        'TextBoxGearRatioZ
        '
        Me.TextBoxGearRatioZ.Location = New System.Drawing.Point(6, 93)
        Me.TextBoxGearRatioZ.Name = "TextBoxGearRatioZ"
        Me.TextBoxGearRatioZ.Size = New System.Drawing.Size(120, 20)
        Me.TextBoxGearRatioZ.TabIndex = 83
        Me.ToolTip2.SetToolTip(Me.TextBoxGearRatioZ, "This axis drive's gear ratio.")
        '
        'CheckBoxPin2InvertedZ
        '
        Me.CheckBoxPin2InvertedZ.AutoSize = True
        Me.CheckBoxPin2InvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2InvertedZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2InvertedZ.Location = New System.Drawing.Point(110, 317)
        Me.CheckBoxPin2InvertedZ.Name = "CheckBoxPin2InvertedZ"
        Me.CheckBoxPin2InvertedZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin2InvertedZ.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin2InvertedZ.TabIndex = 97
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin2InvertedZ, "Activating this checkbox inverts stepper motor direction." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxPin2InvertedZ.UseVisualStyleBackColor = True
        '
        'TextBoxPin6Z
        '
        Me.TextBoxPin6Z.Location = New System.Drawing.Point(9, 418)
        Me.TextBoxPin6Z.Name = "TextBoxPin6Z"
        Me.TextBoxPin6Z.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin6Z.TabIndex = 95
        Me.ToolTip2.SetToolTip(Me.TextBoxPin6Z, "This axis' stepper driver MS3 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'TextBoxPin2Z
        '
        Me.TextBoxPin2Z.Location = New System.Drawing.Point(9, 314)
        Me.TextBoxPin2Z.Name = "TextBoxPin2Z"
        Me.TextBoxPin2Z.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin2Z.TabIndex = 91
        Me.ToolTip2.SetToolTip(Me.TextBoxPin2Z, "This axis' stepper motor pin 2 or stepper driver direction pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire steppe" &
        "r and A4988 driver only.")
        '
        'TextBoxPin3Z
        '
        Me.TextBoxPin3Z.Location = New System.Drawing.Point(9, 340)
        Me.TextBoxPin3Z.Name = "TextBoxPin3Z"
        Me.TextBoxPin3Z.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin3Z.TabIndex = 92
        Me.ToolTip2.SetToolTip(Me.TextBoxPin3Z, "This axis' stepper motor pin 3 or stepper driver step pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and" &
        " A4988 driver only.")
        '
        'CheckBoxPin1InvertedZ
        '
        Me.CheckBoxPin1InvertedZ.AutoSize = True
        Me.CheckBoxPin1InvertedZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1InvertedZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1InvertedZ.Location = New System.Drawing.Point(110, 291)
        Me.CheckBoxPin1InvertedZ.Name = "CheckBoxPin1InvertedZ"
        Me.CheckBoxPin1InvertedZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin1InvertedZ.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin1InvertedZ.TabIndex = 96
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin1InvertedZ, "Unchecked: Enabled = HIGH" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Enabled = LOW")
        Me.CheckBoxPin1InvertedZ.UseVisualStyleBackColor = True
        '
        'TextBoxPin5Z
        '
        Me.TextBoxPin5Z.Location = New System.Drawing.Point(9, 392)
        Me.TextBoxPin5Z.Name = "TextBoxPin5Z"
        Me.TextBoxPin5Z.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin5Z.TabIndex = 94
        Me.ToolTip2.SetToolTip(Me.TextBoxPin5Z, "This axis' stepper driver MS2 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'TextBoxPin4Z
        '
        Me.TextBoxPin4Z.Location = New System.Drawing.Point(9, 366)
        Me.TextBoxPin4Z.Name = "TextBoxPin4Z"
        Me.TextBoxPin4Z.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin4Z.TabIndex = 93
        Me.ToolTip2.SetToolTip(Me.TextBoxPin4Z, "This axis' stepper motor pin 4 or stepper driver MS1 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and " &
        "A4988 driver only.")
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(6, 272)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(62, 13)
        Me.Label42.TabIndex = 113
        Me.Label42.Text = "Pin Number"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(82, 272)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(46, 13)
        Me.Label52.TabIndex = 113
        Me.Label52.Text = "Inverted"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ComboBoxTerminalY)
        Me.GroupBox4.Controls.Add(Me.ComboBoxI2CAddressY)
        Me.GroupBox4.Controls.Add(Me.ComboBoxStepTypeSlowY)
        Me.GroupBox4.Controls.Add(Me.CheckBoxPin5InvertedY)
        Me.GroupBox4.Controls.Add(Me.ComboBoxStepTypeFastY)
        Me.GroupBox4.Controls.Add(Me.ComboBoxMotorDriverY)
        Me.GroupBox4.Controls.Add(Me.CheckBoxPin4InvertedY)
        Me.GroupBox4.Controls.Add(Me.CheckBoxASRY)
        Me.GroupBox4.Controls.Add(Me.TextBoxAccelY)
        Me.GroupBox4.Controls.Add(Me.CheckBoxPin6InvertedY)
        Me.GroupBox4.Controls.Add(Me.TextBoxMaxSpeedY)
        Me.GroupBox4.Controls.Add(Me.TextBoxStepsRevY)
        Me.GroupBox4.Controls.Add(Me.CheckBoxPin3InvertedY)
        Me.GroupBox4.Controls.Add(Me.TextBoxPin1Y)
        Me.GroupBox4.Controls.Add(Me.TextBoxGearRatioY)
        Me.GroupBox4.Controls.Add(Me.CheckBoxPin2InvertedY)
        Me.GroupBox4.Controls.Add(Me.TextBoxPin6Y)
        Me.GroupBox4.Controls.Add(Me.TextBoxPin2Y)
        Me.GroupBox4.Controls.Add(Me.TextBoxPin3Y)
        Me.GroupBox4.Controls.Add(Me.CheckBoxPin1InvertedY)
        Me.GroupBox4.Controls.Add(Me.TextBoxPin5Y)
        Me.GroupBox4.Controls.Add(Me.TextBoxPin4Y)
        Me.GroupBox4.Controls.Add(Me.Label41)
        Me.GroupBox4.Controls.Add(Me.Label51)
        Me.GroupBox4.Location = New System.Drawing.Point(261, 5)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(134, 448)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Y Axis"
        '
        'ComboBoxTerminalY
        '
        Me.ComboBoxTerminalY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTerminalY.DropDownWidth = 49
        Me.ComboBoxTerminalY.FormattingEnabled = True
        Me.ComboBoxTerminalY.Items.AddRange(New Object() {"1", "2"})
        Me.ComboBoxTerminalY.Location = New System.Drawing.Point(79, 238)
        Me.ComboBoxTerminalY.Name = "ComboBoxTerminalY"
        Me.ComboBoxTerminalY.Size = New System.Drawing.Size(49, 21)
        Me.ComboBoxTerminalY.TabIndex = 59
        Me.ToolTip2.SetToolTip(Me.ComboBoxTerminalY, "Adafruit Motor Shield V2 only.")
        '
        'ComboBoxI2CAddressY
        '
        Me.ComboBoxI2CAddressY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxI2CAddressY.DropDownWidth = 67
        Me.ComboBoxI2CAddressY.FormattingEnabled = True
        Me.ComboBoxI2CAddressY.Items.AddRange(New Object() {"0x60", "0x61", "0x62", "0x63", "0x64", "0x65", "0x66", "0x67", "0x68", "0x69", "0x6A", "0x6B", "0x6C", "0x6D", "0x6E", "0x6F"})
        Me.ComboBoxI2CAddressY.Location = New System.Drawing.Point(6, 238)
        Me.ComboBoxI2CAddressY.Name = "ComboBoxI2CAddressY"
        Me.ComboBoxI2CAddressY.Size = New System.Drawing.Size(67, 21)
        Me.ComboBoxI2CAddressY.TabIndex = 58
        Me.ToolTip2.SetToolTip(Me.ComboBoxI2CAddressY, "Adafruit Motor Shield V2 only.")
        '
        'ComboBoxStepTypeSlowY
        '
        Me.ComboBoxStepTypeSlowY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepTypeSlowY.DropDownWidth = 160
        Me.ComboBoxStepTypeSlowY.FormattingEnabled = True
        Me.ComboBoxStepTypeSlowY.Location = New System.Drawing.Point(6, 144)
        Me.ComboBoxStepTypeSlowY.Name = "ComboBoxStepTypeSlowY"
        Me.ComboBoxStepTypeSlowY.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxStepTypeSlowY.TabIndex = 55
        Me.ToolTip2.SetToolTip(Me.ComboBoxStepTypeSlowY, "Steptype for ""slow"" movements (Tracking, Pulse Guiding).")
        '
        'CheckBoxPin5InvertedY
        '
        Me.CheckBoxPin5InvertedY.AutoSize = True
        Me.CheckBoxPin5InvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5InvertedY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5InvertedY.Location = New System.Drawing.Point(110, 396)
        Me.CheckBoxPin5InvertedY.Name = "CheckBoxPin5InvertedY"
        Me.CheckBoxPin5InvertedY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin5InvertedY.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin5InvertedY.TabIndex = 70
        Me.CheckBoxPin5InvertedY.UseVisualStyleBackColor = True
        '
        'ComboBoxStepTypeFastY
        '
        Me.ComboBoxStepTypeFastY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepTypeFastY.DropDownWidth = 160
        Me.ComboBoxStepTypeFastY.FormattingEnabled = True
        Me.ComboBoxStepTypeFastY.Location = New System.Drawing.Point(6, 119)
        Me.ComboBoxStepTypeFastY.Name = "ComboBoxStepTypeFastY"
        Me.ComboBoxStepTypeFastY.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxStepTypeFastY.TabIndex = 54
        Me.ToolTip2.SetToolTip(Me.ComboBoxStepTypeFastY, "Steptype for ""fast"" movements (Slew, MoveAxis)")
        '
        'ComboBoxMotorDriverY
        '
        Me.ComboBoxMotorDriverY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMotorDriverY.DropDownWidth = 200
        Me.ComboBoxMotorDriverY.FormattingEnabled = True
        Me.ComboBoxMotorDriverY.Location = New System.Drawing.Point(6, 211)
        Me.ComboBoxMotorDriverY.Name = "ComboBoxMotorDriverY"
        Me.ComboBoxMotorDriverY.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxMotorDriverY.TabIndex = 57
        Me.ToolTip2.SetToolTip(Me.ComboBoxMotorDriverY, "This axis' Stepper Motor Driver type.")
        '
        'CheckBoxPin4InvertedY
        '
        Me.CheckBoxPin4InvertedY.AutoSize = True
        Me.CheckBoxPin4InvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4InvertedY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4InvertedY.Location = New System.Drawing.Point(110, 370)
        Me.CheckBoxPin4InvertedY.Name = "CheckBoxPin4InvertedY"
        Me.CheckBoxPin4InvertedY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin4InvertedY.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin4InvertedY.TabIndex = 69
        Me.CheckBoxPin4InvertedY.UseVisualStyleBackColor = True
        '
        'CheckBoxASRY
        '
        Me.CheckBoxASRY.AutoSize = True
        Me.CheckBoxASRY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASRY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASRY.Location = New System.Drawing.Point(63, 170)
        Me.CheckBoxASRY.Name = "CheckBoxASRY"
        Me.CheckBoxASRY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxASRY.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxASRY.TabIndex = 56
        Me.CheckBoxASRY.Text = "Enabled"
        Me.CheckBoxASRY.UseVisualStyleBackColor = True
        '
        'TextBoxAccelY
        '
        Me.TextBoxAccelY.Location = New System.Drawing.Point(6, 43)
        Me.TextBoxAccelY.Name = "TextBoxAccelY"
        Me.TextBoxAccelY.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxAccelY.TabIndex = 51
        Me.ToolTip2.SetToolTip(Me.TextBoxAccelY, "Acceleration of this axis' stepper motor in steps / second�.")
        '
        'CheckBoxPin6InvertedY
        '
        Me.CheckBoxPin6InvertedY.AutoSize = True
        Me.CheckBoxPin6InvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6InvertedY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6InvertedY.Location = New System.Drawing.Point(110, 422)
        Me.CheckBoxPin6InvertedY.Name = "CheckBoxPin6InvertedY"
        Me.CheckBoxPin6InvertedY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin6InvertedY.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin6InvertedY.TabIndex = 71
        Me.CheckBoxPin6InvertedY.UseVisualStyleBackColor = True
        '
        'TextBoxMaxSpeedY
        '
        Me.TextBoxMaxSpeedY.Location = New System.Drawing.Point(6, 19)
        Me.TextBoxMaxSpeedY.Name = "TextBoxMaxSpeedY"
        Me.TextBoxMaxSpeedY.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxMaxSpeedY.TabIndex = 50
        Me.ToolTip2.SetToolTip(Me.TextBoxMaxSpeedY, "The maximum speed of this axis' stepper motor in steps / second.")
        '
        'TextBoxStepsRevY
        '
        Me.TextBoxStepsRevY.Location = New System.Drawing.Point(6, 69)
        Me.TextBoxStepsRevY.Name = "TextBoxStepsRevY"
        Me.TextBoxStepsRevY.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxStepsRevY.TabIndex = 52
        Me.ToolTip2.SetToolTip(Me.TextBoxStepsRevY, "This axis' stepper motor's number of steps per revolution of the stepper motor.")
        '
        'CheckBoxPin3InvertedY
        '
        Me.CheckBoxPin3InvertedY.AutoSize = True
        Me.CheckBoxPin3InvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3InvertedY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3InvertedY.Location = New System.Drawing.Point(110, 344)
        Me.CheckBoxPin3InvertedY.Name = "CheckBoxPin3InvertedY"
        Me.CheckBoxPin3InvertedY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin3InvertedY.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin3InvertedY.TabIndex = 68
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin3InvertedY, "Unchecked: Rising edge initiates step." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Falling edge initiates step.")
        Me.CheckBoxPin3InvertedY.UseVisualStyleBackColor = True
        '
        'TextBoxPin1Y
        '
        Me.TextBoxPin1Y.Location = New System.Drawing.Point(9, 289)
        Me.TextBoxPin1Y.Name = "TextBoxPin1Y"
        Me.TextBoxPin1Y.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin1Y.TabIndex = 60
        Me.ToolTip2.SetToolTip(Me.TextBoxPin1Y, "This axis' stepper motor pin 1 or stepper driver enable pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper a" &
        "nd A4988 driver only.")
        '
        'TextBoxGearRatioY
        '
        Me.TextBoxGearRatioY.Location = New System.Drawing.Point(6, 93)
        Me.TextBoxGearRatioY.Name = "TextBoxGearRatioY"
        Me.TextBoxGearRatioY.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxGearRatioY.TabIndex = 53
        Me.ToolTip2.SetToolTip(Me.TextBoxGearRatioY, "This axis drive's gear ratio.")
        '
        'CheckBoxPin2InvertedY
        '
        Me.CheckBoxPin2InvertedY.AutoSize = True
        Me.CheckBoxPin2InvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2InvertedY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2InvertedY.Location = New System.Drawing.Point(110, 318)
        Me.CheckBoxPin2InvertedY.Name = "CheckBoxPin2InvertedY"
        Me.CheckBoxPin2InvertedY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin2InvertedY.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin2InvertedY.TabIndex = 67
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin2InvertedY, "Activating this checkbox inverts stepper motor direction." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxPin2InvertedY.UseVisualStyleBackColor = True
        '
        'TextBoxPin6Y
        '
        Me.TextBoxPin6Y.Location = New System.Drawing.Point(9, 419)
        Me.TextBoxPin6Y.Name = "TextBoxPin6Y"
        Me.TextBoxPin6Y.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin6Y.TabIndex = 65
        Me.ToolTip2.SetToolTip(Me.TextBoxPin6Y, "This axis' stepper driver MS3 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'TextBoxPin2Y
        '
        Me.TextBoxPin2Y.Location = New System.Drawing.Point(9, 315)
        Me.TextBoxPin2Y.Name = "TextBoxPin2Y"
        Me.TextBoxPin2Y.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin2Y.TabIndex = 61
        Me.ToolTip2.SetToolTip(Me.TextBoxPin2Y, "This axis' stepper motor pin 2 or stepper driver direction pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire steppe" &
        "r and A4988 driver only.")
        '
        'TextBoxPin3Y
        '
        Me.TextBoxPin3Y.Location = New System.Drawing.Point(9, 341)
        Me.TextBoxPin3Y.Name = "TextBoxPin3Y"
        Me.TextBoxPin3Y.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin3Y.TabIndex = 62
        Me.ToolTip2.SetToolTip(Me.TextBoxPin3Y, "This axis' stepper motor pin 3 or stepper driver step pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and" &
        " A4988 driver only.")
        '
        'CheckBoxPin1InvertedY
        '
        Me.CheckBoxPin1InvertedY.AutoSize = True
        Me.CheckBoxPin1InvertedY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1InvertedY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1InvertedY.Location = New System.Drawing.Point(110, 292)
        Me.CheckBoxPin1InvertedY.Name = "CheckBoxPin1InvertedY"
        Me.CheckBoxPin1InvertedY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin1InvertedY.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin1InvertedY.TabIndex = 66
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin1InvertedY, "Unchecked: Enabled = HIGH" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Enabled = LOW")
        Me.CheckBoxPin1InvertedY.UseVisualStyleBackColor = True
        '
        'TextBoxPin5Y
        '
        Me.TextBoxPin5Y.Location = New System.Drawing.Point(9, 393)
        Me.TextBoxPin5Y.Name = "TextBoxPin5Y"
        Me.TextBoxPin5Y.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin5Y.TabIndex = 64
        Me.ToolTip2.SetToolTip(Me.TextBoxPin5Y, "This axis' stepper driver MS2 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'TextBoxPin4Y
        '
        Me.TextBoxPin4Y.Location = New System.Drawing.Point(9, 367)
        Me.TextBoxPin4Y.Name = "TextBoxPin4Y"
        Me.TextBoxPin4Y.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin4Y.TabIndex = 63
        Me.ToolTip2.SetToolTip(Me.TextBoxPin4Y, "This axis' stepper motor pin 4 or stepper driver MS1 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and " &
        "A4988 driver only.")
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(6, 273)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(62, 13)
        Me.Label41.TabIndex = 103
        Me.Label41.Text = "Pin Number"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(82, 273)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(46, 13)
        Me.Label51.TabIndex = 103
        Me.Label51.Text = "Inverted"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBoxTerminalX)
        Me.GroupBox1.Controls.Add(Me.ComboBoxI2CAddressX)
        Me.GroupBox1.Controls.Add(Me.ComboBoxStepTypeSlowX)
        Me.GroupBox1.Controls.Add(Me.ComboBoxStepTypeFastX)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPin5InvertedX)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPin4InvertedX)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPin6InvertedX)
        Me.GroupBox1.Controls.Add(Me.CheckBoxASRX)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPin3InvertedX)
        Me.GroupBox1.Controls.Add(Me.TextBoxAccelX)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPin2InvertedX)
        Me.GroupBox1.Controls.Add(Me.TextBoxMaxSpeedX)
        Me.GroupBox1.Controls.Add(Me.CheckBoxPin1InvertedX)
        Me.GroupBox1.Controls.Add(Me.TextBoxGearRatioX)
        Me.GroupBox1.Controls.Add(Me.TextBoxPin5X)
        Me.GroupBox1.Controls.Add(Me.TextBoxStepsRevX)
        Me.GroupBox1.Controls.Add(Me.TextBoxPin6X)
        Me.GroupBox1.Controls.Add(Me.ComboBoxMotorDriverX)
        Me.GroupBox1.Controls.Add(Me.TextBoxPin3X)
        Me.GroupBox1.Controls.Add(Me.TextBoxPin4X)
        Me.GroupBox1.Controls.Add(Me.TextBoxPin1X)
        Me.GroupBox1.Controls.Add(Me.TextBoxPin2X)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label50)
        Me.GroupBox1.Location = New System.Drawing.Point(121, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(134, 448)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "X Axis"
        '
        'ComboBoxTerminalX
        '
        Me.ComboBoxTerminalX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTerminalX.DropDownWidth = 49
        Me.ComboBoxTerminalX.FormattingEnabled = True
        Me.ComboBoxTerminalX.Items.AddRange(New Object() {"1", "2"})
        Me.ComboBoxTerminalX.Location = New System.Drawing.Point(79, 238)
        Me.ComboBoxTerminalX.Name = "ComboBoxTerminalX"
        Me.ComboBoxTerminalX.Size = New System.Drawing.Size(49, 21)
        Me.ComboBoxTerminalX.TabIndex = 29
        Me.ToolTip2.SetToolTip(Me.ComboBoxTerminalX, "Adafruit Motor Shield V2 only.")
        '
        'ComboBoxI2CAddressX
        '
        Me.ComboBoxI2CAddressX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxI2CAddressX.DropDownWidth = 67
        Me.ComboBoxI2CAddressX.FormattingEnabled = True
        Me.ComboBoxI2CAddressX.Items.AddRange(New Object() {"0x60", "0x61", "0x62", "0x63", "0x64", "0x65", "0x66", "0x67", "0x68", "0x69", "0x6A", "0x6B", "0x6C", "0x6D", "0x6E", "0x6F"})
        Me.ComboBoxI2CAddressX.Location = New System.Drawing.Point(6, 238)
        Me.ComboBoxI2CAddressX.Name = "ComboBoxI2CAddressX"
        Me.ComboBoxI2CAddressX.Size = New System.Drawing.Size(67, 21)
        Me.ComboBoxI2CAddressX.TabIndex = 28
        Me.ToolTip2.SetToolTip(Me.ComboBoxI2CAddressX, "Adafruit Motor Shield V2 only.")
        '
        'ComboBoxStepTypeSlowX
        '
        Me.ComboBoxStepTypeSlowX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepTypeSlowX.DropDownWidth = 160
        Me.ComboBoxStepTypeSlowX.FormattingEnabled = True
        Me.ComboBoxStepTypeSlowX.Location = New System.Drawing.Point(6, 144)
        Me.ComboBoxStepTypeSlowX.Name = "ComboBoxStepTypeSlowX"
        Me.ComboBoxStepTypeSlowX.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxStepTypeSlowX.TabIndex = 25
        Me.ToolTip2.SetToolTip(Me.ComboBoxStepTypeSlowX, "Steptype for ""slow"" movements (Tracking, Pulse Guiding).")
        '
        'ComboBoxStepTypeFastX
        '
        Me.ComboBoxStepTypeFastX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepTypeFastX.DropDownWidth = 160
        Me.ComboBoxStepTypeFastX.FormattingEnabled = True
        Me.ComboBoxStepTypeFastX.Location = New System.Drawing.Point(6, 119)
        Me.ComboBoxStepTypeFastX.Name = "ComboBoxStepTypeFastX"
        Me.ComboBoxStepTypeFastX.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxStepTypeFastX.TabIndex = 24
        Me.ToolTip2.SetToolTip(Me.ComboBoxStepTypeFastX, "Steptype for ""fast"" movements (Slew, MoveAxis)")
        '
        'CheckBoxPin5InvertedX
        '
        Me.CheckBoxPin5InvertedX.AutoSize = True
        Me.CheckBoxPin5InvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5InvertedX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5InvertedX.Location = New System.Drawing.Point(107, 395)
        Me.CheckBoxPin5InvertedX.Name = "CheckBoxPin5InvertedX"
        Me.CheckBoxPin5InvertedX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin5InvertedX.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin5InvertedX.TabIndex = 40
        Me.CheckBoxPin5InvertedX.UseVisualStyleBackColor = True
        '
        'CheckBoxPin4InvertedX
        '
        Me.CheckBoxPin4InvertedX.AutoSize = True
        Me.CheckBoxPin4InvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4InvertedX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4InvertedX.Location = New System.Drawing.Point(107, 369)
        Me.CheckBoxPin4InvertedX.Name = "CheckBoxPin4InvertedX"
        Me.CheckBoxPin4InvertedX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin4InvertedX.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin4InvertedX.TabIndex = 39
        Me.CheckBoxPin4InvertedX.UseVisualStyleBackColor = True
        '
        'CheckBoxPin6InvertedX
        '
        Me.CheckBoxPin6InvertedX.AutoSize = True
        Me.CheckBoxPin6InvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6InvertedX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6InvertedX.Location = New System.Drawing.Point(107, 421)
        Me.CheckBoxPin6InvertedX.Name = "CheckBoxPin6InvertedX"
        Me.CheckBoxPin6InvertedX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin6InvertedX.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin6InvertedX.TabIndex = 41
        Me.CheckBoxPin6InvertedX.UseVisualStyleBackColor = True
        '
        'CheckBoxASRX
        '
        Me.CheckBoxASRX.AutoSize = True
        Me.CheckBoxASRX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASRX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASRX.Location = New System.Drawing.Point(63, 171)
        Me.CheckBoxASRX.Name = "CheckBoxASRX"
        Me.CheckBoxASRX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxASRX.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxASRX.TabIndex = 26
        Me.CheckBoxASRX.Text = "Enabled"
        Me.CheckBoxASRX.UseVisualStyleBackColor = True
        '
        'CheckBoxPin3InvertedX
        '
        Me.CheckBoxPin3InvertedX.AutoSize = True
        Me.CheckBoxPin3InvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3InvertedX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3InvertedX.Location = New System.Drawing.Point(107, 343)
        Me.CheckBoxPin3InvertedX.Name = "CheckBoxPin3InvertedX"
        Me.CheckBoxPin3InvertedX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin3InvertedX.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin3InvertedX.TabIndex = 38
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin3InvertedX, "Unchecked: Rising edge initiates step." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Falling edge initiates step.")
        Me.CheckBoxPin3InvertedX.UseVisualStyleBackColor = True
        '
        'TextBoxAccelX
        '
        Me.TextBoxAccelX.Location = New System.Drawing.Point(6, 43)
        Me.TextBoxAccelX.Name = "TextBoxAccelX"
        Me.TextBoxAccelX.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxAccelX.TabIndex = 21
        Me.ToolTip2.SetToolTip(Me.TextBoxAccelX, "Acceleration of this axis' stepper motor in steps / second�.")
        '
        'CheckBoxPin2InvertedX
        '
        Me.CheckBoxPin2InvertedX.AutoSize = True
        Me.CheckBoxPin2InvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2InvertedX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2InvertedX.Location = New System.Drawing.Point(107, 317)
        Me.CheckBoxPin2InvertedX.Name = "CheckBoxPin2InvertedX"
        Me.CheckBoxPin2InvertedX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin2InvertedX.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin2InvertedX.TabIndex = 37
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin2InvertedX, "Activating this checkbox inverts stepper motor direction." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxPin2InvertedX.UseVisualStyleBackColor = True
        '
        'TextBoxMaxSpeedX
        '
        Me.TextBoxMaxSpeedX.Location = New System.Drawing.Point(6, 19)
        Me.TextBoxMaxSpeedX.Name = "TextBoxMaxSpeedX"
        Me.TextBoxMaxSpeedX.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxMaxSpeedX.TabIndex = 20
        Me.ToolTip2.SetToolTip(Me.TextBoxMaxSpeedX, "The maximum speed of this axis' stepper motor in steps / second.")
        '
        'CheckBoxPin1InvertedX
        '
        Me.CheckBoxPin1InvertedX.AutoSize = True
        Me.CheckBoxPin1InvertedX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1InvertedX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1InvertedX.Location = New System.Drawing.Point(107, 291)
        Me.CheckBoxPin1InvertedX.Name = "CheckBoxPin1InvertedX"
        Me.CheckBoxPin1InvertedX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin1InvertedX.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin1InvertedX.TabIndex = 36
        Me.ToolTip2.SetToolTip(Me.CheckBoxPin1InvertedX, "Unchecked: Enabled = HIGH" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Enabled = LOW")
        Me.CheckBoxPin1InvertedX.UseVisualStyleBackColor = True
        '
        'TextBoxGearRatioX
        '
        Me.TextBoxGearRatioX.Location = New System.Drawing.Point(6, 93)
        Me.TextBoxGearRatioX.Name = "TextBoxGearRatioX"
        Me.TextBoxGearRatioX.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxGearRatioX.TabIndex = 23
        Me.ToolTip2.SetToolTip(Me.TextBoxGearRatioX, "This axis drive's gear ratio.")
        '
        'TextBoxPin5X
        '
        Me.TextBoxPin5X.Location = New System.Drawing.Point(6, 392)
        Me.TextBoxPin5X.Name = "TextBoxPin5X"
        Me.TextBoxPin5X.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin5X.TabIndex = 34
        Me.ToolTip2.SetToolTip(Me.TextBoxPin5X, "This axis' stepper driver MS2 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'TextBoxStepsRevX
        '
        Me.TextBoxStepsRevX.Location = New System.Drawing.Point(6, 69)
        Me.TextBoxStepsRevX.Name = "TextBoxStepsRevX"
        Me.TextBoxStepsRevX.Size = New System.Drawing.Size(122, 20)
        Me.TextBoxStepsRevX.TabIndex = 22
        Me.ToolTip2.SetToolTip(Me.TextBoxStepsRevX, "This axis' stepper motor's number of steps per revolution of the stepper motor.")
        '
        'TextBoxPin6X
        '
        Me.TextBoxPin6X.Location = New System.Drawing.Point(6, 418)
        Me.TextBoxPin6X.Name = "TextBoxPin6X"
        Me.TextBoxPin6X.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin6X.TabIndex = 35
        Me.ToolTip2.SetToolTip(Me.TextBoxPin6X, "This axis' stepper driver MS3 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'ComboBoxMotorDriverX
        '
        Me.ComboBoxMotorDriverX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMotorDriverX.DropDownWidth = 200
        Me.ComboBoxMotorDriverX.FormattingEnabled = True
        Me.ComboBoxMotorDriverX.Location = New System.Drawing.Point(6, 211)
        Me.ComboBoxMotorDriverX.Name = "ComboBoxMotorDriverX"
        Me.ComboBoxMotorDriverX.Size = New System.Drawing.Size(122, 21)
        Me.ComboBoxMotorDriverX.TabIndex = 27
        Me.ToolTip2.SetToolTip(Me.ComboBoxMotorDriverX, "This axis' Stepper Motor Driver type.")
        '
        'TextBoxPin3X
        '
        Me.TextBoxPin3X.Location = New System.Drawing.Point(6, 340)
        Me.TextBoxPin3X.Name = "TextBoxPin3X"
        Me.TextBoxPin3X.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin3X.TabIndex = 32
        Me.ToolTip2.SetToolTip(Me.TextBoxPin3X, "This axis' stepper motor pin 3 or stepper driver step pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and" &
        " A4988 driver only.")
        '
        'TextBoxPin4X
        '
        Me.TextBoxPin4X.Location = New System.Drawing.Point(6, 366)
        Me.TextBoxPin4X.Name = "TextBoxPin4X"
        Me.TextBoxPin4X.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin4X.TabIndex = 33
        Me.ToolTip2.SetToolTip(Me.TextBoxPin4X, "This axis' stepper motor pin 4 or stepper driver MS1 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and " &
        "A4988 driver only.")
        '
        'TextBoxPin1X
        '
        Me.TextBoxPin1X.Location = New System.Drawing.Point(6, 288)
        Me.TextBoxPin1X.Name = "TextBoxPin1X"
        Me.TextBoxPin1X.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin1X.TabIndex = 30
        Me.ToolTip2.SetToolTip(Me.TextBoxPin1X, "This axis' stepper motor pin 1 or stepper driver enable pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper a" &
        "nd A4988 driver only.")
        '
        'TextBoxPin2X
        '
        Me.TextBoxPin2X.Location = New System.Drawing.Point(6, 314)
        Me.TextBoxPin2X.Name = "TextBoxPin2X"
        Me.TextBoxPin2X.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin2X.TabIndex = 31
        Me.ToolTip2.SetToolTip(Me.TextBoxPin2X, "This axis' stepper motor pin 2 or stepper driver direction pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire steppe" &
        "r and A4988 driver only.")
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 272)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(62, 13)
        Me.Label18.TabIndex = 102
        Me.Label18.Text = "Pin Number"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(82, 272)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(46, 13)
        Me.Label50.TabIndex = 102
        Me.Label50.Text = "Inverted"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(3, 153)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(82, 13)
        Me.Label34.TabIndex = 97
        Me.Label34.Text = "Step Type Slow"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 77)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 13)
        Me.Label9.TabIndex = 93
        Me.Label9.Text = "Steps / Rev"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 127)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(79, 13)
        Me.Label13.TabIndex = 96
        Me.Label13.Text = "Step Type Fast"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label8.Location = New System.Drawing.Point(3, 101)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 92
        Me.Label8.Text = "Gear Ratio"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label10.Location = New System.Drawing.Point(2, 51)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 13)
        Me.Label10.TabIndex = 94
        Me.Label10.Text = "Acceleration"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(2, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 13)
        Me.Label11.TabIndex = 95
        Me.Label11.Text = "Max Speed"
        '
        'TabPageEndstopsLimits
        '
        Me.TabPageEndstopsLimits.Controls.Add(Me.GroupBox13)
        Me.TabPageEndstopsLimits.Controls.Add(Me.GroupBox10)
        Me.TabPageEndstopsLimits.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.TabPageEndstopsLimits.Location = New System.Drawing.Point(4, 22)
        Me.TabPageEndstopsLimits.Name = "TabPageEndstopsLimits"
        Me.TabPageEndstopsLimits.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageEndstopsLimits.Size = New System.Drawing.Size(541, 497)
        Me.TabPageEndstopsLimits.TabIndex = 2
        Me.TabPageEndstopsLimits.Text = "Endstops and Limits"
        Me.TabPageEndstopsLimits.UseVisualStyleBackColor = True
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.GroupBox20)
        Me.GroupBox13.Controls.Add(Me.GroupBox14)
        Me.GroupBox13.Controls.Add(Me.GroupBox15)
        Me.GroupBox13.Location = New System.Drawing.Point(6, 211)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(517, 246)
        Me.GroupBox13.TabIndex = 88
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Movement Limits"
        '
        'GroupBox20
        '
        Me.GroupBox20.Controls.Add(Me.Label17)
        Me.GroupBox20.Controls.Add(Me.GroupBox34)
        Me.GroupBox20.Controls.Add(Me.CheckBoxHardStopLimitZ)
        Me.GroupBox20.Controls.Add(Me.GroupBox35)
        Me.GroupBox20.Controls.Add(Me.ComboBoxLimitHandlingZ)
        Me.GroupBox20.Location = New System.Drawing.Point(349, 16)
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.Size = New System.Drawing.Size(163, 224)
        Me.GroupBox20.TabIndex = 5
        Me.GroupBox20.TabStop = False
        Me.GroupBox20.Text = "Z Axis"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(10, 179)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(131, 13)
        Me.Label17.TabIndex = 25
        Me.Label17.Text = "Slew Target Limit Violation"
        '
        'GroupBox34
        '
        Me.GroupBox34.Controls.Add(Me.TextBoxUpperLimitZ)
        Me.GroupBox34.Controls.Add(Me.CheckBoxUpperLimitEnabledZ)
        Me.GroupBox34.Controls.Add(Me.Label28)
        Me.GroupBox34.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox34.Name = "GroupBox34"
        Me.GroupBox34.Size = New System.Drawing.Size(149, 64)
        Me.GroupBox34.TabIndex = 26
        Me.GroupBox34.TabStop = False
        Me.GroupBox34.Text = "Upper"
        '
        'TextBoxUpperLimitZ
        '
        Me.TextBoxUpperLimitZ.Location = New System.Drawing.Point(7, 32)
        Me.TextBoxUpperLimitZ.Name = "TextBoxUpperLimitZ"
        Me.TextBoxUpperLimitZ.Size = New System.Drawing.Size(62, 20)
        Me.TextBoxUpperLimitZ.TabIndex = 59
        Me.ToolTip2.SetToolTip(Me.TextBoxUpperLimitZ, "This axis' upper movement limit (in degrees).")
        '
        'CheckBoxUpperLimitEnabledZ
        '
        Me.CheckBoxUpperLimitEnabledZ.AutoSize = True
        Me.CheckBoxUpperLimitEnabledZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxUpperLimitEnabledZ.Location = New System.Drawing.Point(76, 35)
        Me.CheckBoxUpperLimitEnabledZ.Name = "CheckBoxUpperLimitEnabledZ"
        Me.CheckBoxUpperLimitEnabledZ.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxUpperLimitEnabledZ.TabIndex = 60
        Me.CheckBoxUpperLimitEnabledZ.Text = "Enabled"
        Me.CheckBoxUpperLimitEnabledZ.UseVisualStyleBackColor = True
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(5, 16)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(60, 13)
        Me.Label28.TabIndex = 12
        Me.Label28.Text = "Limit (Deg.)"
        '
        'CheckBoxHardStopLimitZ
        '
        Me.CheckBoxHardStopLimitZ.AutoSize = True
        Me.CheckBoxHardStopLimitZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHardStopLimitZ.Location = New System.Drawing.Point(73, 153)
        Me.CheckBoxHardStopLimitZ.Name = "CheckBoxHardStopLimitZ"
        Me.CheckBoxHardStopLimitZ.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxHardStopLimitZ.TabIndex = 61
        Me.CheckBoxHardStopLimitZ.Text = "Hard Stop"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHardStopLimitZ, "When enabled the axis will stop instantly when the Limit is violated." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxHardStopLimitZ.UseVisualStyleBackColor = True
        '
        'GroupBox35
        '
        Me.GroupBox35.Controls.Add(Me.TextBoxLowerLimitZ)
        Me.GroupBox35.Controls.Add(Me.CheckBoxLowerLimitEnabledZ)
        Me.GroupBox35.Controls.Add(Me.Label29)
        Me.GroupBox35.Location = New System.Drawing.Point(6, 16)
        Me.GroupBox35.Name = "GroupBox35"
        Me.GroupBox35.Size = New System.Drawing.Size(149, 61)
        Me.GroupBox35.TabIndex = 25
        Me.GroupBox35.TabStop = False
        Me.GroupBox35.Text = "Lower"
        '
        'TextBoxLowerLimitZ
        '
        Me.TextBoxLowerLimitZ.Location = New System.Drawing.Point(6, 30)
        Me.TextBoxLowerLimitZ.Name = "TextBoxLowerLimitZ"
        Me.TextBoxLowerLimitZ.Size = New System.Drawing.Size(62, 20)
        Me.TextBoxLowerLimitZ.TabIndex = 57
        Me.ToolTip2.SetToolTip(Me.TextBoxLowerLimitZ, "This axis' lower movement limit (in degrees).")
        '
        'CheckBoxLowerLimitEnabledZ
        '
        Me.CheckBoxLowerLimitEnabledZ.AutoSize = True
        Me.CheckBoxLowerLimitEnabledZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLowerLimitEnabledZ.Location = New System.Drawing.Point(76, 34)
        Me.CheckBoxLowerLimitEnabledZ.Name = "CheckBoxLowerLimitEnabledZ"
        Me.CheckBoxLowerLimitEnabledZ.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxLowerLimitEnabledZ.TabIndex = 58
        Me.CheckBoxLowerLimitEnabledZ.Text = "Enabled"
        Me.CheckBoxLowerLimitEnabledZ.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(4, 15)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(60, 13)
        Me.Label29.TabIndex = 5
        Me.Label29.Text = "Limit (Deg.)"
        '
        'ComboBoxLimitHandlingZ
        '
        Me.ComboBoxLimitHandlingZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxLimitHandlingZ.FormattingEnabled = True
        Me.ComboBoxLimitHandlingZ.Location = New System.Drawing.Point(12, 197)
        Me.ComboBoxLimitHandlingZ.Name = "ComboBoxLimitHandlingZ"
        Me.ComboBoxLimitHandlingZ.Size = New System.Drawing.Size(143, 21)
        Me.ComboBoxLimitHandlingZ.TabIndex = 62
        Me.ToolTip2.SetToolTip(Me.ComboBoxLimitHandlingZ, resources.GetString("ComboBoxLimitHandlingZ.ToolTip"))
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.GroupBox31)
        Me.GroupBox14.Controls.Add(Me.GroupBox30)
        Me.GroupBox14.Controls.Add(Me.CheckBoxHardStopLimitX)
        Me.GroupBox14.Controls.Add(Me.Label43)
        Me.GroupBox14.Controls.Add(Me.ComboBoxLimitHandlingX)
        Me.GroupBox14.Location = New System.Drawing.Point(11, 16)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(163, 224)
        Me.GroupBox14.TabIndex = 1
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "X Axis"
        '
        'GroupBox31
        '
        Me.GroupBox31.Controls.Add(Me.TextBoxUpperLimitX)
        Me.GroupBox31.Controls.Add(Me.CheckBoxUpperLimitEnabledX)
        Me.GroupBox31.Controls.Add(Me.Label25)
        Me.GroupBox31.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox31.Name = "GroupBox31"
        Me.GroupBox31.Size = New System.Drawing.Size(149, 64)
        Me.GroupBox31.TabIndex = 22
        Me.GroupBox31.TabStop = False
        Me.GroupBox31.Text = "Upper"
        '
        'TextBoxUpperLimitX
        '
        Me.TextBoxUpperLimitX.Location = New System.Drawing.Point(4, 32)
        Me.TextBoxUpperLimitX.Name = "TextBoxUpperLimitX"
        Me.TextBoxUpperLimitX.Size = New System.Drawing.Size(62, 20)
        Me.TextBoxUpperLimitX.TabIndex = 19
        Me.ToolTip2.SetToolTip(Me.TextBoxUpperLimitX, "This axis' upper movement limit (in degrees).")
        '
        'CheckBoxUpperLimitEnabledX
        '
        Me.CheckBoxUpperLimitEnabledX.AutoSize = True
        Me.CheckBoxUpperLimitEnabledX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxUpperLimitEnabledX.Location = New System.Drawing.Point(78, 35)
        Me.CheckBoxUpperLimitEnabledX.Name = "CheckBoxUpperLimitEnabledX"
        Me.CheckBoxUpperLimitEnabledX.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxUpperLimitEnabledX.TabIndex = 20
        Me.CheckBoxUpperLimitEnabledX.Text = "Enabled"
        Me.CheckBoxUpperLimitEnabledX.UseVisualStyleBackColor = True
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(6, 16)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(60, 13)
        Me.Label25.TabIndex = 12
        Me.Label25.Text = "Limit (Deg.)"
        '
        'GroupBox30
        '
        Me.GroupBox30.Controls.Add(Me.TextBoxLowerLimitX)
        Me.GroupBox30.Controls.Add(Me.CheckBoxLowerLimitEnabledX)
        Me.GroupBox30.Controls.Add(Me.Label24)
        Me.GroupBox30.Location = New System.Drawing.Point(6, 16)
        Me.GroupBox30.Name = "GroupBox30"
        Me.GroupBox30.Size = New System.Drawing.Size(149, 61)
        Me.GroupBox30.TabIndex = 21
        Me.GroupBox30.TabStop = False
        Me.GroupBox30.Text = "Lower"
        '
        'TextBoxLowerLimitX
        '
        Me.TextBoxLowerLimitX.Location = New System.Drawing.Point(4, 31)
        Me.TextBoxLowerLimitX.Name = "TextBoxLowerLimitX"
        Me.TextBoxLowerLimitX.Size = New System.Drawing.Size(62, 20)
        Me.TextBoxLowerLimitX.TabIndex = 17
        Me.ToolTip2.SetToolTip(Me.TextBoxLowerLimitX, "This axis' lower movement limit (in degrees).")
        '
        'CheckBoxLowerLimitEnabledX
        '
        Me.CheckBoxLowerLimitEnabledX.AutoSize = True
        Me.CheckBoxLowerLimitEnabledX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLowerLimitEnabledX.Location = New System.Drawing.Point(78, 34)
        Me.CheckBoxLowerLimitEnabledX.Name = "CheckBoxLowerLimitEnabledX"
        Me.CheckBoxLowerLimitEnabledX.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxLowerLimitEnabledX.TabIndex = 18
        Me.CheckBoxLowerLimitEnabledX.Text = "Enabled"
        Me.CheckBoxLowerLimitEnabledX.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(6, 15)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(60, 13)
        Me.Label24.TabIndex = 5
        Me.Label24.Text = "Limit (Deg.)"
        '
        'CheckBoxHardStopLimitX
        '
        Me.CheckBoxHardStopLimitX.AutoSize = True
        Me.CheckBoxHardStopLimitX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHardStopLimitX.Location = New System.Drawing.Point(75, 153)
        Me.CheckBoxHardStopLimitX.Name = "CheckBoxHardStopLimitX"
        Me.CheckBoxHardStopLimitX.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxHardStopLimitX.TabIndex = 21
        Me.CheckBoxHardStopLimitX.Text = "Hard Stop"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHardStopLimitX, "When enabled the axis will stop instantly when the Limit is violated.")
        Me.CheckBoxHardStopLimitX.UseVisualStyleBackColor = True
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(8, 178)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(131, 13)
        Me.Label43.TabIndex = 15
        Me.Label43.Text = "Slew Target Limit Violation"
        '
        'ComboBoxLimitHandlingX
        '
        Me.ComboBoxLimitHandlingX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxLimitHandlingX.FormattingEnabled = True
        Me.ComboBoxLimitHandlingX.Location = New System.Drawing.Point(10, 197)
        Me.ComboBoxLimitHandlingX.Name = "ComboBoxLimitHandlingX"
        Me.ComboBoxLimitHandlingX.Size = New System.Drawing.Size(145, 21)
        Me.ComboBoxLimitHandlingX.TabIndex = 22
        Me.ToolTip2.SetToolTip(Me.ComboBoxLimitHandlingX, resources.GetString("ComboBoxLimitHandlingX.ToolTip"))
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.GroupBox32)
        Me.GroupBox15.Controls.Add(Me.Label27)
        Me.GroupBox15.Controls.Add(Me.GroupBox33)
        Me.GroupBox15.Controls.Add(Me.CheckBoxHardStopLimitY)
        Me.GroupBox15.Controls.Add(Me.ComboBoxLimitHandlingY)
        Me.GroupBox15.Location = New System.Drawing.Point(180, 16)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(163, 224)
        Me.GroupBox15.TabIndex = 3
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "Y Axis"
        '
        'GroupBox32
        '
        Me.GroupBox32.Controls.Add(Me.TextBoxUpperLimitY)
        Me.GroupBox32.Controls.Add(Me.CheckBoxUpperLimitEnabledY)
        Me.GroupBox32.Controls.Add(Me.Label33)
        Me.GroupBox32.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox32.Name = "GroupBox32"
        Me.GroupBox32.Size = New System.Drawing.Size(151, 64)
        Me.GroupBox32.TabIndex = 24
        Me.GroupBox32.TabStop = False
        Me.GroupBox32.Text = "Upper"
        '
        'TextBoxUpperLimitY
        '
        Me.TextBoxUpperLimitY.Location = New System.Drawing.Point(4, 32)
        Me.TextBoxUpperLimitY.Name = "TextBoxUpperLimitY"
        Me.TextBoxUpperLimitY.Size = New System.Drawing.Size(62, 20)
        Me.TextBoxUpperLimitY.TabIndex = 39
        Me.ToolTip2.SetToolTip(Me.TextBoxUpperLimitY, "This axis' upper movement limit (in degrees).")
        '
        'CheckBoxUpperLimitEnabledY
        '
        Me.CheckBoxUpperLimitEnabledY.AutoSize = True
        Me.CheckBoxUpperLimitEnabledY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxUpperLimitEnabledY.Location = New System.Drawing.Point(78, 35)
        Me.CheckBoxUpperLimitEnabledY.Name = "CheckBoxUpperLimitEnabledY"
        Me.CheckBoxUpperLimitEnabledY.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxUpperLimitEnabledY.TabIndex = 40
        Me.CheckBoxUpperLimitEnabledY.Text = "Enabled"
        Me.CheckBoxUpperLimitEnabledY.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(5, 16)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(60, 13)
        Me.Label33.TabIndex = 12
        Me.Label33.Text = "Limit (Deg.)"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(10, 179)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(131, 13)
        Me.Label27.TabIndex = 23
        Me.Label27.Text = "Slew Target Limit Violation"
        '
        'GroupBox33
        '
        Me.GroupBox33.Controls.Add(Me.TextBoxLowerLimitY)
        Me.GroupBox33.Controls.Add(Me.CheckBoxLowerLimitEnabledY)
        Me.GroupBox33.Controls.Add(Me.Label35)
        Me.GroupBox33.Location = New System.Drawing.Point(6, 16)
        Me.GroupBox33.Name = "GroupBox33"
        Me.GroupBox33.Size = New System.Drawing.Size(151, 61)
        Me.GroupBox33.TabIndex = 23
        Me.GroupBox33.TabStop = False
        Me.GroupBox33.Text = "Lower"
        '
        'TextBoxLowerLimitY
        '
        Me.TextBoxLowerLimitY.Location = New System.Drawing.Point(4, 31)
        Me.TextBoxLowerLimitY.Name = "TextBoxLowerLimitY"
        Me.TextBoxLowerLimitY.Size = New System.Drawing.Size(62, 20)
        Me.TextBoxLowerLimitY.TabIndex = 37
        Me.ToolTip2.SetToolTip(Me.TextBoxLowerLimitY, "This axis' lower movement limit (in degrees).")
        '
        'CheckBoxLowerLimitEnabledY
        '
        Me.CheckBoxLowerLimitEnabledY.AutoSize = True
        Me.CheckBoxLowerLimitEnabledY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLowerLimitEnabledY.Location = New System.Drawing.Point(78, 34)
        Me.CheckBoxLowerLimitEnabledY.Name = "CheckBoxLowerLimitEnabledY"
        Me.CheckBoxLowerLimitEnabledY.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxLowerLimitEnabledY.TabIndex = 38
        Me.CheckBoxLowerLimitEnabledY.Text = "Enabled"
        Me.CheckBoxLowerLimitEnabledY.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(5, 15)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(60, 13)
        Me.Label35.TabIndex = 5
        Me.Label35.Text = "Limit (Deg.)"
        '
        'CheckBoxHardStopLimitY
        '
        Me.CheckBoxHardStopLimitY.AutoSize = True
        Me.CheckBoxHardStopLimitY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHardStopLimitY.Location = New System.Drawing.Point(75, 153)
        Me.CheckBoxHardStopLimitY.Name = "CheckBoxHardStopLimitY"
        Me.CheckBoxHardStopLimitY.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxHardStopLimitY.TabIndex = 41
        Me.CheckBoxHardStopLimitY.Text = "Hard Stop"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHardStopLimitY, "When enabled the axis will stop instantly when the Limit is violated." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxHardStopLimitY.UseVisualStyleBackColor = True
        '
        'ComboBoxLimitHandlingY
        '
        Me.ComboBoxLimitHandlingY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxLimitHandlingY.FormattingEnabled = True
        Me.ComboBoxLimitHandlingY.Location = New System.Drawing.Point(12, 197)
        Me.ComboBoxLimitHandlingY.Name = "ComboBoxLimitHandlingY"
        Me.ComboBoxLimitHandlingY.Size = New System.Drawing.Size(147, 21)
        Me.ComboBoxLimitHandlingY.TabIndex = 42
        Me.ToolTip2.SetToolTip(Me.ComboBoxLimitHandlingY, resources.GetString("ComboBoxLimitHandlingY.ToolTip"))
        '
        'TabPageHomingParking
        '
        Me.TabPageHomingParking.Controls.Add(Me.GroupBox3)
        Me.TabPageHomingParking.Controls.Add(Me.GroupBox7)
        Me.TabPageHomingParking.Location = New System.Drawing.Point(4, 22)
        Me.TabPageHomingParking.Name = "TabPageHomingParking"
        Me.TabPageHomingParking.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageHomingParking.Size = New System.Drawing.Size(541, 497)
        Me.TabPageHomingParking.TabIndex = 3
        Me.TabPageHomingParking.Text = "Homing & Parking"
        Me.TabPageHomingParking.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.GroupBox21)
        Me.GroupBox3.Controls.Add(Me.GroupBox6)
        Me.GroupBox3.Controls.Add(Me.GroupBox5)
        Me.GroupBox3.Controls.Add(Me.CheckBoxSAH)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(522, 209)
        Me.GroupBox3.TabIndex = 82
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Homing Control"
        '
        'GroupBox21
        '
        Me.GroupBox21.Controls.Add(Me.CheckBoxHASZ)
        Me.GroupBox21.Controls.Add(Me.CheckBoxHSIZ)
        Me.GroupBox21.Controls.Add(Me.Label54)
        Me.GroupBox21.Controls.Add(Me.CheckBoxHSAZ)
        Me.GroupBox21.Controls.Add(Me.TextBoxHomeAlignmentZ)
        Me.GroupBox21.Controls.Add(Me.Label55)
        Me.GroupBox21.Controls.Add(Me.TextBoxHSNZ)
        Me.GroupBox21.Location = New System.Drawing.Point(350, 44)
        Me.GroupBox21.Name = "GroupBox21"
        Me.GroupBox21.Size = New System.Drawing.Size(163, 146)
        Me.GroupBox21.TabIndex = 95
        Me.GroupBox21.TabStop = False
        Me.GroupBox21.Text = "Z Axis"
        '
        'CheckBoxHASZ
        '
        Me.CheckBoxHASZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHASZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHASZ.Location = New System.Drawing.Point(6, 116)
        Me.CheckBoxHASZ.Name = "CheckBoxHASZ"
        Me.CheckBoxHASZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHASZ.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHASZ.TabIndex = 93
        Me.CheckBoxHASZ.Text = "Home Alignment Sync"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHASZ, "When enabled, syncing this axis' alignment while it is in its home position will " &
        "also sync the stored home alignment." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxHASZ.UseVisualStyleBackColor = True
        '
        'CheckBoxHSIZ
        '
        Me.CheckBoxHSIZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSIZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSIZ.Location = New System.Drawing.Point(6, 42)
        Me.CheckBoxHSIZ.Name = "CheckBoxHSIZ"
        Me.CheckBoxHSIZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSIZ.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHSIZ.TabIndex = 31
        Me.CheckBoxHSIZ.Text = "Home Switch inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHSIZ, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxHSIZ.UseVisualStyleBackColor = True
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(7, 94)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(84, 13)
        Me.Label54.TabIndex = 92
        Me.Label54.Text = "Home Alignment"
        '
        'CheckBoxHSAZ
        '
        Me.CheckBoxHSAZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSAZ.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSAZ.Location = New System.Drawing.Point(6, 19)
        Me.CheckBoxHSAZ.Name = "CheckBoxHSAZ"
        Me.CheckBoxHSAZ.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSAZ.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHSAZ.TabIndex = 30
        Me.CheckBoxHSAZ.Text = "Home Switch enabled"
        Me.CheckBoxHSAZ.UseVisualStyleBackColor = True
        '
        'TextBoxHomeAlignmentZ
        '
        Me.TextBoxHomeAlignmentZ.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TextBoxHomeAlignmentZ.Location = New System.Drawing.Point(97, 91)
        Me.TextBoxHomeAlignmentZ.Name = "TextBoxHomeAlignmentZ"
        Me.TextBoxHomeAlignmentZ.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxHomeAlignmentZ.TabIndex = 33
        Me.ToolTip2.SetToolTip(Me.TextBoxHomeAlignmentZ, "This axis' home alignment (in degrees). ")
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(7, 68)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(62, 13)
        Me.Label55.TabIndex = 51
        Me.Label55.Text = "Pin Number"
        '
        'TextBoxHSNZ
        '
        Me.TextBoxHSNZ.Location = New System.Drawing.Point(96, 65)
        Me.TextBoxHSNZ.Name = "TextBoxHSNZ"
        Me.TextBoxHSNZ.Size = New System.Drawing.Size(61, 20)
        Me.TextBoxHSNZ.TabIndex = 32
        Me.ToolTip2.SetToolTip(Me.TextBoxHSNZ, "This axis' home switch pin number.")
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.CheckBoxHASY)
        Me.GroupBox6.Controls.Add(Me.CheckBoxHSIY)
        Me.GroupBox6.Controls.Add(Me.Label19)
        Me.GroupBox6.Controls.Add(Me.CheckBoxHSAY)
        Me.GroupBox6.Controls.Add(Me.TextBoxHomeAlignmentY)
        Me.GroupBox6.Controls.Add(Me.Label1)
        Me.GroupBox6.Controls.Add(Me.TextBoxHSNY)
        Me.GroupBox6.Location = New System.Drawing.Point(181, 44)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(163, 146)
        Me.GroupBox6.TabIndex = 87
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Y Axis"
        '
        'CheckBoxHASY
        '
        Me.CheckBoxHASY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHASY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHASY.Location = New System.Drawing.Point(6, 116)
        Me.CheckBoxHASY.Name = "CheckBoxHASY"
        Me.CheckBoxHASY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHASY.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHASY.TabIndex = 93
        Me.CheckBoxHASY.Text = "Home Alignment Sync"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHASY, "When enabled, syncing this axis' alignment while it is in its home position will " &
        "also sync the stored home alignment." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxHASY.UseVisualStyleBackColor = True
        '
        'CheckBoxHSIY
        '
        Me.CheckBoxHSIY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSIY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSIY.Location = New System.Drawing.Point(6, 42)
        Me.CheckBoxHSIY.Name = "CheckBoxHSIY"
        Me.CheckBoxHSIY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSIY.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHSIY.TabIndex = 21
        Me.CheckBoxHSIY.Text = "Home Switch inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHSIY, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxHSIY.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(7, 94)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(84, 13)
        Me.Label19.TabIndex = 92
        Me.Label19.Text = "Home Alignment"
        '
        'CheckBoxHSAY
        '
        Me.CheckBoxHSAY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSAY.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSAY.Location = New System.Drawing.Point(6, 19)
        Me.CheckBoxHSAY.Name = "CheckBoxHSAY"
        Me.CheckBoxHSAY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSAY.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHSAY.TabIndex = 20
        Me.CheckBoxHSAY.Text = "Home Switch enabled"
        Me.CheckBoxHSAY.UseVisualStyleBackColor = True
        '
        'TextBoxHomeAlignmentY
        '
        Me.TextBoxHomeAlignmentY.ForeColor = System.Drawing.SystemColors.WindowText
        Me.TextBoxHomeAlignmentY.Location = New System.Drawing.Point(97, 91)
        Me.TextBoxHomeAlignmentY.Name = "TextBoxHomeAlignmentY"
        Me.TextBoxHomeAlignmentY.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxHomeAlignmentY.TabIndex = 23
        Me.ToolTip2.SetToolTip(Me.TextBoxHomeAlignmentY, "This axis' home alignment (in degrees). ")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 51
        Me.Label1.Text = "Pin Number"
        '
        'TextBoxHSNY
        '
        Me.TextBoxHSNY.Location = New System.Drawing.Point(96, 65)
        Me.TextBoxHSNY.Name = "TextBoxHSNY"
        Me.TextBoxHSNY.Size = New System.Drawing.Size(61, 20)
        Me.TextBoxHSNY.TabIndex = 22
        Me.ToolTip2.SetToolTip(Me.TextBoxHSNY, "This axis' home switch pin number.")
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.CheckBoxHASX)
        Me.GroupBox5.Controls.Add(Me.CheckBoxHSIX)
        Me.GroupBox5.Controls.Add(Me.CheckBoxHSAX)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Controls.Add(Me.TextBoxHSNX)
        Me.GroupBox5.Controls.Add(Me.Label21)
        Me.GroupBox5.Controls.Add(Me.TextBoxHomeAlignmentX)
        Me.GroupBox5.Location = New System.Drawing.Point(11, 43)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(163, 146)
        Me.GroupBox5.TabIndex = 86
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "X Axis"
        '
        'CheckBoxHASX
        '
        Me.CheckBoxHASX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHASX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHASX.Location = New System.Drawing.Point(6, 117)
        Me.CheckBoxHASX.Name = "CheckBoxHASX"
        Me.CheckBoxHASX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHASX.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHASX.TabIndex = 95
        Me.CheckBoxHASX.Text = "Home Alignment Sync"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHASX, "When enabled, syncing this axis' alignment while it is in its home position will " &
        "also sync the stored home alignment.")
        Me.CheckBoxHASX.UseVisualStyleBackColor = True
        '
        'CheckBoxHSIX
        '
        Me.CheckBoxHSIX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSIX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSIX.Location = New System.Drawing.Point(6, 42)
        Me.CheckBoxHSIX.Name = "CheckBoxHSIX"
        Me.CheckBoxHSIX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSIX.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHSIX.TabIndex = 11
        Me.CheckBoxHSIX.Text = "Home Switch inverted"
        Me.ToolTip2.SetToolTip(Me.CheckBoxHSIX, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxHSIX.UseVisualStyleBackColor = True
        '
        'CheckBoxHSAX
        '
        Me.CheckBoxHSAX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSAX.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSAX.Location = New System.Drawing.Point(6, 19)
        Me.CheckBoxHSAX.Name = "CheckBoxHSAX"
        Me.CheckBoxHSAX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSAX.Size = New System.Drawing.Size(151, 17)
        Me.CheckBoxHSAX.TabIndex = 10
        Me.CheckBoxHSAX.Text = "Home Switch enabled"
        Me.CheckBoxHSAX.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 68)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 13)
        Me.Label5.TabIndex = 51
        Me.Label5.Text = "Pin Number"
        '
        'TextBoxHSNX
        '
        Me.TextBoxHSNX.Location = New System.Drawing.Point(96, 65)
        Me.TextBoxHSNX.Name = "TextBoxHSNX"
        Me.TextBoxHSNX.Size = New System.Drawing.Size(61, 20)
        Me.TextBoxHSNX.TabIndex = 12
        Me.ToolTip2.SetToolTip(Me.TextBoxHSNX, "This axis' home switch pin number.")
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(6, 94)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(84, 13)
        Me.Label21.TabIndex = 94
        Me.Label21.Text = "Home Alignment"
        '
        'TextBoxHomeAlignmentX
        '
        Me.TextBoxHomeAlignmentX.Location = New System.Drawing.Point(96, 91)
        Me.TextBoxHomeAlignmentX.Name = "TextBoxHomeAlignmentX"
        Me.TextBoxHomeAlignmentX.Size = New System.Drawing.Size(61, 20)
        Me.TextBoxHomeAlignmentX.TabIndex = 13
        Me.ToolTip2.SetToolTip(Me.TextBoxHomeAlignmentX, "This axis' home alignment (in degrees). ")
        '
        'CheckBoxSAH
        '
        Me.CheckBoxSAH.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CheckBoxSAH.Location = New System.Drawing.Point(17, 20)
        Me.CheckBoxSAH.Name = "CheckBoxSAH"
        Me.CheckBoxSAH.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxSAH.Size = New System.Drawing.Size(155, 17)
        Me.CheckBoxSAH.TabIndex = 5
        Me.CheckBoxSAH.Text = "Startup Auto home"
        Me.CheckBoxSAH.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.GroupBox2)
        Me.GroupBox7.Controls.Add(Me.CheckBoxParkEnabled)
        Me.GroupBox7.Controls.Add(Me.GroupBox9)
        Me.GroupBox7.Controls.Add(Me.GroupBox8)
        Me.GroupBox7.Location = New System.Drawing.Point(6, 221)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(522, 103)
        Me.GroupBox7.TabIndex = 85
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Park Control"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBoxParkPosZ)
        Me.GroupBox2.Controls.Add(Me.Label53)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(350, 42)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(163, 49)
        Me.GroupBox2.TabIndex = 92
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Z Axis"
        '
        'TextBoxParkPosZ
        '
        Me.TextBoxParkPosZ.Location = New System.Drawing.Point(97, 17)
        Me.TextBoxParkPosZ.Name = "TextBoxParkPosZ"
        Me.TextBoxParkPosZ.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxParkPosZ.TabIndex = 34
        Me.ToolTip2.SetToolTip(Me.TextBoxParkPosZ, "This axis' park position (in degrees).")
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(6, 20)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(83, 13)
        Me.Label53.TabIndex = 90
        Me.Label53.Text = "Park Pos. (deg.)"
        '
        'CheckBoxParkEnabled
        '
        Me.CheckBoxParkEnabled.AutoSize = True
        Me.CheckBoxParkEnabled.Location = New System.Drawing.Point(17, 19)
        Me.CheckBoxParkEnabled.Name = "CheckBoxParkEnabled"
        Me.CheckBoxParkEnabled.Size = New System.Drawing.Size(98, 17)
        Me.CheckBoxParkEnabled.TabIndex = 6
        Me.CheckBoxParkEnabled.Text = "Enable Parking"
        Me.CheckBoxParkEnabled.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.TextBoxParkPosY)
        Me.GroupBox9.Controls.Add(Me.Label20)
        Me.GroupBox9.Location = New System.Drawing.Point(181, 42)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(163, 49)
        Me.GroupBox9.TabIndex = 87
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Y Axis"
        '
        'TextBoxParkPosY
        '
        Me.TextBoxParkPosY.Location = New System.Drawing.Point(97, 17)
        Me.TextBoxParkPosY.Name = "TextBoxParkPosY"
        Me.TextBoxParkPosY.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxParkPosY.TabIndex = 24
        Me.ToolTip2.SetToolTip(Me.TextBoxParkPosY, "This axis' park position (in degrees).")
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 20)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(83, 13)
        Me.Label20.TabIndex = 90
        Me.Label20.Text = "Park Pos. (deg.)"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.TextBoxParkPosX)
        Me.GroupBox8.Controls.Add(Me.Label2)
        Me.GroupBox8.Location = New System.Drawing.Point(11, 42)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(163, 49)
        Me.GroupBox8.TabIndex = 86
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "X Axis"
        '
        'TextBoxParkPosX
        '
        Me.TextBoxParkPosX.Location = New System.Drawing.Point(96, 17)
        Me.TextBoxParkPosX.Name = "TextBoxParkPosX"
        Me.TextBoxParkPosX.Size = New System.Drawing.Size(61, 20)
        Me.TextBoxParkPosX.TabIndex = 14
        Me.ToolTip2.SetToolTip(Me.TextBoxParkPosX, "This axis' park position (in degrees).")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 13)
        Me.Label2.TabIndex = 86
        Me.Label2.Text = "Park Pos. (deg.)"
        '
        'TabPageManualControl
        '
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxHSTriggeredZ)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxESUTriggeredZ)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxESLTriggeredZ)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxHSTriggeredY)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxESUTriggeredY)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxESLTriggeredY)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxHSTriggeredX)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxESUTriggeredX)
        Me.TabPageManualControl.Controls.Add(Me.CheckBoxESLTriggeredX)
        Me.TabPageManualControl.Controls.Add(Me.ButtonReadSwitchState)
        Me.TabPageManualControl.Controls.Add(Me.GroupBox22)
        Me.TabPageManualControl.Location = New System.Drawing.Point(4, 22)
        Me.TabPageManualControl.Name = "TabPageManualControl"
        Me.TabPageManualControl.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageManualControl.Size = New System.Drawing.Size(541, 497)
        Me.TabPageManualControl.TabIndex = 4
        Me.TabPageManualControl.Text = "ManualControl"
        Me.TabPageManualControl.UseVisualStyleBackColor = True
        '
        'CheckBoxHSTriggeredZ
        '
        Me.CheckBoxHSTriggeredZ.AutoSize = True
        Me.CheckBoxHSTriggeredZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSTriggeredZ.Enabled = False
        Me.CheckBoxHSTriggeredZ.Location = New System.Drawing.Point(424, 181)
        Me.CheckBoxHSTriggeredZ.Name = "CheckBoxHSTriggeredZ"
        Me.CheckBoxHSTriggeredZ.Size = New System.Drawing.Size(89, 17)
        Me.CheckBoxHSTriggeredZ.TabIndex = 120
        Me.CheckBoxHSTriggeredZ.TabStop = False
        Me.CheckBoxHSTriggeredZ.Text = "Home Switch"
        Me.CheckBoxHSTriggeredZ.UseVisualStyleBackColor = True
        '
        'CheckBoxESUTriggeredZ
        '
        Me.CheckBoxESUTriggeredZ.AutoSize = True
        Me.CheckBoxESUTriggeredZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESUTriggeredZ.Enabled = False
        Me.CheckBoxESUTriggeredZ.Location = New System.Drawing.Point(416, 158)
        Me.CheckBoxESUTriggeredZ.Name = "CheckBoxESUTriggeredZ"
        Me.CheckBoxESUTriggeredZ.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESUTriggeredZ.TabIndex = 119
        Me.CheckBoxESUTriggeredZ.TabStop = False
        Me.CheckBoxESUTriggeredZ.Text = "Upper Endstop"
        Me.CheckBoxESUTriggeredZ.UseVisualStyleBackColor = True
        '
        'CheckBoxESLTriggeredZ
        '
        Me.CheckBoxESLTriggeredZ.AutoSize = True
        Me.CheckBoxESLTriggeredZ.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESLTriggeredZ.Enabled = False
        Me.CheckBoxESLTriggeredZ.Location = New System.Drawing.Point(416, 135)
        Me.CheckBoxESLTriggeredZ.Name = "CheckBoxESLTriggeredZ"
        Me.CheckBoxESLTriggeredZ.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESLTriggeredZ.TabIndex = 118
        Me.CheckBoxESLTriggeredZ.TabStop = False
        Me.CheckBoxESLTriggeredZ.Text = "Lower Endstop"
        Me.CheckBoxESLTriggeredZ.UseVisualStyleBackColor = True
        '
        'CheckBoxHSTriggeredY
        '
        Me.CheckBoxHSTriggeredY.AutoSize = True
        Me.CheckBoxHSTriggeredY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSTriggeredY.Enabled = False
        Me.CheckBoxHSTriggeredY.Location = New System.Drawing.Point(255, 181)
        Me.CheckBoxHSTriggeredY.Name = "CheckBoxHSTriggeredY"
        Me.CheckBoxHSTriggeredY.Size = New System.Drawing.Size(89, 17)
        Me.CheckBoxHSTriggeredY.TabIndex = 117
        Me.CheckBoxHSTriggeredY.TabStop = False
        Me.CheckBoxHSTriggeredY.Text = "Home Switch"
        Me.CheckBoxHSTriggeredY.UseVisualStyleBackColor = True
        '
        'CheckBoxESUTriggeredY
        '
        Me.CheckBoxESUTriggeredY.AutoSize = True
        Me.CheckBoxESUTriggeredY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESUTriggeredY.Enabled = False
        Me.CheckBoxESUTriggeredY.Location = New System.Drawing.Point(247, 158)
        Me.CheckBoxESUTriggeredY.Name = "CheckBoxESUTriggeredY"
        Me.CheckBoxESUTriggeredY.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESUTriggeredY.TabIndex = 116
        Me.CheckBoxESUTriggeredY.TabStop = False
        Me.CheckBoxESUTriggeredY.Text = "Upper Endstop"
        Me.CheckBoxESUTriggeredY.UseVisualStyleBackColor = True
        '
        'CheckBoxESLTriggeredY
        '
        Me.CheckBoxESLTriggeredY.AutoSize = True
        Me.CheckBoxESLTriggeredY.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESLTriggeredY.Enabled = False
        Me.CheckBoxESLTriggeredY.Location = New System.Drawing.Point(247, 135)
        Me.CheckBoxESLTriggeredY.Name = "CheckBoxESLTriggeredY"
        Me.CheckBoxESLTriggeredY.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESLTriggeredY.TabIndex = 115
        Me.CheckBoxESLTriggeredY.TabStop = False
        Me.CheckBoxESLTriggeredY.Text = "Lower Endstop"
        Me.CheckBoxESLTriggeredY.UseVisualStyleBackColor = True
        '
        'CheckBoxHSTriggeredX
        '
        Me.CheckBoxHSTriggeredX.AutoSize = True
        Me.CheckBoxHSTriggeredX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSTriggeredX.Enabled = False
        Me.CheckBoxHSTriggeredX.Location = New System.Drawing.Point(85, 181)
        Me.CheckBoxHSTriggeredX.Name = "CheckBoxHSTriggeredX"
        Me.CheckBoxHSTriggeredX.Size = New System.Drawing.Size(89, 17)
        Me.CheckBoxHSTriggeredX.TabIndex = 114
        Me.CheckBoxHSTriggeredX.TabStop = False
        Me.CheckBoxHSTriggeredX.Text = "Home Switch"
        Me.CheckBoxHSTriggeredX.UseVisualStyleBackColor = True
        '
        'CheckBoxESUTriggeredX
        '
        Me.CheckBoxESUTriggeredX.AutoSize = True
        Me.CheckBoxESUTriggeredX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESUTriggeredX.Enabled = False
        Me.CheckBoxESUTriggeredX.Location = New System.Drawing.Point(77, 158)
        Me.CheckBoxESUTriggeredX.Name = "CheckBoxESUTriggeredX"
        Me.CheckBoxESUTriggeredX.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESUTriggeredX.TabIndex = 113
        Me.CheckBoxESUTriggeredX.TabStop = False
        Me.CheckBoxESUTriggeredX.Text = "Upper Endstop"
        Me.CheckBoxESUTriggeredX.UseVisualStyleBackColor = True
        '
        'CheckBoxESLTriggeredX
        '
        Me.CheckBoxESLTriggeredX.AutoSize = True
        Me.CheckBoxESLTriggeredX.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESLTriggeredX.Enabled = False
        Me.CheckBoxESLTriggeredX.Location = New System.Drawing.Point(77, 135)
        Me.CheckBoxESLTriggeredX.Name = "CheckBoxESLTriggeredX"
        Me.CheckBoxESLTriggeredX.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESLTriggeredX.TabIndex = 112
        Me.CheckBoxESLTriggeredX.TabStop = False
        Me.CheckBoxESLTriggeredX.Text = "Lower Endstop"
        Me.CheckBoxESLTriggeredX.UseVisualStyleBackColor = True
        '
        'ButtonReadSwitchState
        '
        Me.ButtonReadSwitchState.Location = New System.Drawing.Point(6, 135)
        Me.ButtonReadSwitchState.Name = "ButtonReadSwitchState"
        Me.ButtonReadSwitchState.Size = New System.Drawing.Size(57, 48)
        Me.ButtonReadSwitchState.TabIndex = 110
        Me.ButtonReadSwitchState.Text = "Read Switch State"
        Me.ToolTip2.SetToolTip(Me.ButtonReadSwitchState, "Reads the current state of the home and endstop switches." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Disabled switches will" &
        " never show as triggered.")
        Me.ButtonReadSwitchState.UseVisualStyleBackColor = True
        '
        'GroupBox22
        '
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCJA)
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCCSA)
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCTA)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCJoystickAxisX)
        Me.GroupBox22.Controls.Add(Me.Label56)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCCS)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCJoystickAxisY)
        Me.GroupBox22.Controls.Add(Me.Label59)
        Me.GroupBox22.Controls.Add(Me.Label57)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCJoystickDeadZone)
        Me.GroupBox22.Controls.Add(Me.TextBoxMCT)
        Me.GroupBox22.Controls.Add(Me.CheckBoxMCEnable)
        Me.GroupBox22.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.Size = New System.Drawing.Size(522, 123)
        Me.GroupBox22.TabIndex = 111
        Me.GroupBox22.TabStop = False
        Me.GroupBox22.Text = "Manual Control"
        '
        'CheckBoxMCJA
        '
        Me.CheckBoxMCJA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCJA.Location = New System.Drawing.Point(362, 19)
        Me.CheckBoxMCJA.Name = "CheckBoxMCJA"
        Me.CheckBoxMCJA.Size = New System.Drawing.Size(145, 17)
        Me.CheckBoxMCJA.TabIndex = 112
        Me.CheckBoxMCJA.TabStop = False
        Me.CheckBoxMCJA.Text = "Manual Control Joystick"
        Me.CheckBoxMCJA.UseVisualStyleBackColor = True
        '
        'CheckBoxMCCSA
        '
        Me.CheckBoxMCCSA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCCSA.Location = New System.Drawing.Point(17, 66)
        Me.CheckBoxMCCSA.Name = "CheckBoxMCCSA"
        Me.CheckBoxMCCSA.Size = New System.Drawing.Size(168, 17)
        Me.CheckBoxMCCSA.TabIndex = 111
        Me.CheckBoxMCCSA.TabStop = False
        Me.CheckBoxMCCSA.Text = "Manual Control Speed Switch"
        Me.CheckBoxMCCSA.UseVisualStyleBackColor = True
        '
        'CheckBoxMCTA
        '
        Me.CheckBoxMCTA.AutoSize = True
        Me.CheckBoxMCTA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMCTA.Location = New System.Drawing.Point(17, 41)
        Me.CheckBoxMCTA.Name = "CheckBoxMCTA"
        Me.CheckBoxMCTA.Size = New System.Drawing.Size(168, 17)
        Me.CheckBoxMCTA.TabIndex = 110
        Me.CheckBoxMCTA.TabStop = False
        Me.CheckBoxMCTA.Text = "Manual Control Toggle Switch"
        Me.CheckBoxMCTA.UseVisualStyleBackColor = True
        '
        'TextBoxMCJoystickAxisX
        '
        Me.TextBoxMCJoystickAxisX.Location = New System.Drawing.Point(447, 38)
        Me.TextBoxMCJoystickAxisX.Name = "TextBoxMCJoystickAxisX"
        Me.TextBoxMCJoystickAxisX.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCJoystickAxisX.TabIndex = 15
        Me.ToolTip2.SetToolTip(Me.TextBoxMCJoystickAxisX, "Manual Control Joystick X Axis input pin.")
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(364, 41)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(77, 13)
        Me.Label56.TabIndex = 97
        Me.Label56.Text = "Joystick X Axis"
        '
        'TextBoxMCCS
        '
        Me.TextBoxMCCS.Location = New System.Drawing.Point(204, 62)
        Me.TextBoxMCCS.Name = "TextBoxMCCS"
        Me.TextBoxMCCS.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCCS.TabIndex = 35
        '
        'TextBoxMCJoystickAxisY
        '
        Me.TextBoxMCJoystickAxisY.Location = New System.Drawing.Point(447, 64)
        Me.TextBoxMCJoystickAxisY.Name = "TextBoxMCJoystickAxisY"
        Me.TextBoxMCJoystickAxisY.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCJoystickAxisY.TabIndex = 16
        Me.ToolTip2.SetToolTip(Me.TextBoxMCJoystickAxisY, "Manual Control Joystick Y Axis input pin.")
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(364, 93)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(56, 13)
        Me.Label59.TabIndex = 101
        Me.Label59.Text = "Deadzone"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(364, 67)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(77, 13)
        Me.Label57.TabIndex = 99
        Me.Label57.Text = "Joystick Y Axis"
        '
        'TextBoxMCJoystickDeadZone
        '
        Me.TextBoxMCJoystickDeadZone.Location = New System.Drawing.Point(447, 90)
        Me.TextBoxMCJoystickDeadZone.Name = "TextBoxMCJoystickDeadZone"
        Me.TextBoxMCJoystickDeadZone.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCJoystickDeadZone.TabIndex = 26
        Me.ToolTip2.SetToolTip(Me.TextBoxMCJoystickDeadZone, "Manual Control Joystick deadzone.")
        '
        'TextBoxMCT
        '
        Me.TextBoxMCT.Location = New System.Drawing.Point(204, 39)
        Me.TextBoxMCT.Name = "TextBoxMCT"
        Me.TextBoxMCT.Size = New System.Drawing.Size(60, 20)
        Me.TextBoxMCT.TabIndex = 36
        Me.ToolTip2.SetToolTip(Me.TextBoxMCT, "Manual Control Enable / Disable switch pin number.")
        '
        'CheckBoxMCEnable
        '
        Me.CheckBoxMCEnable.AutoSize = True
        Me.CheckBoxMCEnable.Location = New System.Drawing.Point(17, 19)
        Me.CheckBoxMCEnable.Name = "CheckBoxMCEnable"
        Me.CheckBoxMCEnable.Size = New System.Drawing.Size(133, 17)
        Me.CheckBoxMCEnable.TabIndex = 7
        Me.CheckBoxMCEnable.Text = "Enable Manual Control"
        Me.CheckBoxMCEnable.UseVisualStyleBackColor = True
        '
        'TabPageTemperatureSensors
        '
        Me.TabPageTemperatureSensors.Controls.Add(Me.FlowLayoutPanelTemperatureSensors)
        Me.TabPageTemperatureSensors.Location = New System.Drawing.Point(4, 22)
        Me.TabPageTemperatureSensors.Name = "TabPageTemperatureSensors"
        Me.TabPageTemperatureSensors.Size = New System.Drawing.Size(541, 497)
        Me.TabPageTemperatureSensors.TabIndex = 5
        Me.TabPageTemperatureSensors.Text = "Temperature Sensors"
        Me.TabPageTemperatureSensors.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanelTemperatureSensors
        '
        Me.FlowLayoutPanelTemperatureSensors.AutoScroll = True
        Me.FlowLayoutPanelTemperatureSensors.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanelTemperatureSensors.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanelTemperatureSensors.Name = "FlowLayoutPanelTemperatureSensors"
        Me.FlowLayoutPanelTemperatureSensors.Size = New System.Drawing.Size(511, 426)
        Me.FlowLayoutPanelTemperatureSensors.TabIndex = 101
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonReadEEPROM, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonWriteEEPROM, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 30)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(135, 40)
        Me.TableLayoutPanel2.TabIndex = 89
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBoxEEPROMForceWrite, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(12, 541)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.9863!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.0137!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(141, 73)
        Me.TableLayoutPanel3.TabIndex = 90
        '
        'ToolTip2
        '
        Me.ToolTip2.AutoPopDelay = 500000
        Me.ToolTip2.InitialDelay = 500
        Me.ToolTip2.ReshowDelay = 100
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonExportSettings, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonImportSettings, 1, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(415, 552)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(146, 27)
        Me.TableLayoutPanel4.TabIndex = 96
        '
        'ButtonExportSettings
        '
        Me.ButtonExportSettings.Location = New System.Drawing.Point(3, 3)
        Me.ButtonExportSettings.Name = "ButtonExportSettings"
        Me.ButtonExportSettings.Size = New System.Drawing.Size(67, 21)
        Me.ButtonExportSettings.TabIndex = 93
        Me.ButtonExportSettings.Text = "Export"
        Me.ButtonExportSettings.UseVisualStyleBackColor = True
        '
        'ButtonImportSettings
        '
        Me.ButtonImportSettings.Location = New System.Drawing.Point(76, 3)
        Me.ButtonImportSettings.Name = "ButtonImportSettings"
        Me.ButtonImportSettings.Size = New System.Drawing.Size(67, 21)
        Me.ButtonImportSettings.TabIndex = 94
        Me.ButtonImportSettings.Text = "Import"
        Me.ButtonImportSettings.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSplitButton1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 615)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(573, 22)
        Me.StatusStrip1.TabIndex = 97
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(132, 20)
        Me.ToolStripSplitButton1.Text = "ToolStripSplitButton1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(120, 17)
        Me.ToolStripStatusLabel2.Text = "ToolStripStatusLabel2"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(17, 17)
        Me.ToolStripStatusLabel1.Text = """"""
        '
        'SetupDialogForm
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(573, 637)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel4)
        Me.Controls.Add(Me.TableLayoutPanel3)
        Me.Controls.Add(Me.TabControl)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SetupDialogForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "YAAATelescope Setup"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox19.ResumeLayout(False)
        Me.GroupBox19.PerformLayout()
        Me.GroupBox25.ResumeLayout(False)
        Me.GroupBox25.PerformLayout()
        Me.GroupBox24.ResumeLayout(False)
        Me.GroupBox24.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox28.ResumeLayout(False)
        Me.GroupBox28.PerformLayout()
        Me.GroupBox29.ResumeLayout(False)
        Me.GroupBox29.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox26.ResumeLayout(False)
        Me.GroupBox26.PerformLayout()
        Me.GroupBox27.ResumeLayout(False)
        Me.GroupBox27.PerformLayout()
        Me.TabControl.ResumeLayout(False)
        Me.TabPageGeneral.ResumeLayout(False)
        Me.TabPageGeneral.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        Me.GroupBox23.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox37.ResumeLayout(False)
        Me.GroupBox37.PerformLayout()
        Me.GroupBox36.ResumeLayout(False)
        Me.GroupBox36.PerformLayout()
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        Me.TabPageAxisControl.ResumeLayout(False)
        Me.TabPageAxisControl.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPageEndstopsLimits.ResumeLayout(False)
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox20.ResumeLayout(False)
        Me.GroupBox20.PerformLayout()
        Me.GroupBox34.ResumeLayout(False)
        Me.GroupBox34.PerformLayout()
        Me.GroupBox35.ResumeLayout(False)
        Me.GroupBox35.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox31.ResumeLayout(False)
        Me.GroupBox31.PerformLayout()
        Me.GroupBox30.ResumeLayout(False)
        Me.GroupBox30.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GroupBox32.ResumeLayout(False)
        Me.GroupBox32.PerformLayout()
        Me.GroupBox33.ResumeLayout(False)
        Me.GroupBox33.PerformLayout()
        Me.TabPageHomingParking.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox21.ResumeLayout(False)
        Me.GroupBox21.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.TabPageManualControl.ResumeLayout(False)
        Me.TabPageManualControl.PerformLayout()
        Me.GroupBox22.ResumeLayout(False)
        Me.GroupBox22.PerformLayout()
        Me.TabPageTemperatureSensors.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ButtonWriteEEPROM As System.Windows.Forms.Button
    Private WithEvents Label4 As System.Windows.Forms.Label
    Private WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkTrace As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxComPorts As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonReadEEPROM As System.Windows.Forms.Button
    Friend WithEvents CheckBoxEEPROMForceWrite As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopUpperEnabledX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEndStopLowerEnabledX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEndstopUpperEnabledY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEndStopLowerEnabledY As System.Windows.Forms.CheckBox
    Friend WithEvents TextBoxEndstopUpperPinY As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxEndstopLowerPinY As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxEndstopUpperPinX As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxEndstopLowerPinX As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl As System.Windows.Forms.TabControl
    Friend WithEvents TabPageGeneral As System.Windows.Forms.TabPage
    Friend WithEvents TabPageAxisControl As System.Windows.Forms.TabPage
    Friend WithEvents TabPageEndstopsLimits As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxUpperLimitX As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxLowerLimitX As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxUpperLimitEnabledX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxLowerLimitEnabledX As System.Windows.Forms.CheckBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxLowerLimitY As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxUpperLimitY As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxLowerLimitEnabledY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxUpperLimitEnabledY As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Private WithEvents Label32 As System.Windows.Forms.Label
    Private WithEvents TextBoxSiteLatitude As System.Windows.Forms.TextBox
    Private WithEvents Label30 As System.Windows.Forms.Label
    Private WithEvents TextBoxSiteElevation As System.Windows.Forms.TextBox
    Private WithEvents TextBoxSiteLongitude As System.Windows.Forms.TextBox
    Private WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Private WithEvents Label36 As System.Windows.Forms.Label
    Private WithEvents TextBoxFocalLength As System.Windows.Forms.TextBox
    Private WithEvents Label37 As System.Windows.Forms.Label
    Private WithEvents TextBoxApertureDiameter As System.Windows.Forms.TextBox
    Private WithEvents Label38 As System.Windows.Forms.Label
    Private WithEvents TextBoxApertureArea As System.Windows.Forms.TextBox
    Private WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxMountType As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox20 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxLowerLimitZ As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxUpperLimitZ As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxLowerLimitEnabledZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxUpperLimitEnabledZ As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox19 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBoxEndstopLowerPinZ As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxEndStopLowerEnabledZ As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxLimitHandlingZ As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxLimitHandlingX As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxLimitHandlingY As System.Windows.Forms.ComboBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxEndstopHardStopX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEndstopHardStopY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHardStopLimitZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHardStopLimitX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHardStopLimitY As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxBaudRate As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox25 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxEndstopUpperPinZ As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxEndstopUpperEnabledZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEndstopHardStopZ As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox24 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopLowerInvertedZ As System.Windows.Forms.CheckBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxEndstopUpperInvertedZ As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox26 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopUpperInvertedY As System.Windows.Forms.CheckBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents GroupBox27 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopLowerInvertedY As System.Windows.Forms.CheckBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents GroupBox28 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopUpperInvertedX As System.Windows.Forms.CheckBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox29 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopLowerInvertedX As System.Windows.Forms.CheckBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents GroupBox31 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox30 As System.Windows.Forms.GroupBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents GroupBox32 As System.Windows.Forms.GroupBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents GroupBox33 As System.Windows.Forms.GroupBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents GroupBox34 As System.Windows.Forms.GroupBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents GroupBox35 As System.Windows.Forms.GroupBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Private WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxASRZ As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxAccelZ As System.Windows.Forms.TextBox
    Private WithEvents TextBoxMaxSpeedZ As System.Windows.Forms.TextBox
    Private WithEvents TextBoxStepsRevZ As System.Windows.Forms.TextBox
    Private WithEvents TextBoxGearRatioZ As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxASRY As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxAccelY As System.Windows.Forms.TextBox
    Private WithEvents TextBoxMaxSpeedY As System.Windows.Forms.TextBox
    Private WithEvents TextBoxStepsRevY As System.Windows.Forms.TextBox
    Private WithEvents TextBoxGearRatioY As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxASRX As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxAccelX As System.Windows.Forms.TextBox
    Private WithEvents TextBoxMaxSpeedX As System.Windows.Forms.TextBox
    Private WithEvents TextBoxGearRatioX As System.Windows.Forms.TextBox
    Private WithEvents TextBoxStepsRevX As System.Windows.Forms.TextBox
    Private WithEvents Label34 As System.Windows.Forms.Label
    Private WithEvents Label9 As System.Windows.Forms.Label
    Private WithEvents Label13 As System.Windows.Forms.Label
    Private WithEvents Label8 As System.Windows.Forms.Label
    Private WithEvents Label10 As System.Windows.Forms.Label
    Private WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxStepTypeSlowX As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxStepTypeFastX As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxStepTypeSlowZ As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxStepTypeFastZ As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxStepTypeSlowY As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxStepTypeFastY As System.Windows.Forms.ComboBox
    Private WithEvents Label49 As System.Windows.Forms.Label
    Private WithEvents Label47 As System.Windows.Forms.Label
    Private WithEvents Label48 As System.Windows.Forms.Label
    Private WithEvents Label12 As System.Windows.Forms.Label
    Private WithEvents Label46 As System.Windows.Forms.Label
    Private WithEvents Label6 As System.Windows.Forms.Label
    Private WithEvents Label7 As System.Windows.Forms.Label
    Private WithEvents Label45 As System.Windows.Forms.Label
    Private WithEvents Label42 As System.Windows.Forms.Label
    Private WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxPin5InvertedZ As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxMotorDriverZ As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxPin4InvertedZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin6InvertedZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin3InvertedZ As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin1Z As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxPin2InvertedZ As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin6Z As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin2Z As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin3Z As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxPin1InvertedZ As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin5Z As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin4Z As System.Windows.Forms.TextBox
    Private WithEvents Label41 As System.Windows.Forms.Label
    Private WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxPin5InvertedY As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxMotorDriverY As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxPin4InvertedY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin6InvertedY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin3InvertedY As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin1Y As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxPin2InvertedY As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin6Y As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin2Y As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin3Y As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxPin1InvertedY As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin5Y As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin4Y As System.Windows.Forms.TextBox
    Private WithEvents Label18 As System.Windows.Forms.Label
    Private WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxPin5InvertedX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin4InvertedX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin6InvertedX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin3InvertedX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin2InvertedX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin1InvertedX As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin5X As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin6X As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxMotorDriverX As System.Windows.Forms.ComboBox
    Private WithEvents TextBoxPin3X As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin4X As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin1X As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin2X As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxI2CAddressX As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTerminalX As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTerminalZ As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxI2CAddressZ As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTerminalY As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxI2CAddressY As System.Windows.Forms.ComboBox
    Friend WithEvents ToolTip2 As System.Windows.Forms.ToolTip
    Friend WithEvents TabPageHomingParking As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox21 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxHASZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHSIZ As System.Windows.Forms.CheckBox
    Private WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxHSAZ As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxHomeAlignmentZ As System.Windows.Forms.TextBox
    Private WithEvents Label55 As System.Windows.Forms.Label
    Private WithEvents TextBoxHSNZ As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxHASY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHSIY As System.Windows.Forms.CheckBox
    Private WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxHSAY As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxHomeAlignmentY As System.Windows.Forms.TextBox
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents TextBoxHSNY As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxHASX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHSIX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHSAX As System.Windows.Forms.CheckBox
    Private WithEvents Label5 As System.Windows.Forms.Label
    Private WithEvents TextBoxHSNX As System.Windows.Forms.TextBox
    Private WithEvents Label21 As System.Windows.Forms.Label
    Private WithEvents TextBoxHomeAlignmentX As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxSAH As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Private WithEvents TextBoxParkPosZ As System.Windows.Forms.TextBox
    Private WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxParkEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Private WithEvents TextBoxParkPosY As System.Windows.Forms.TextBox
    Private WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Private WithEvents TextBoxParkPosX As System.Windows.Forms.TextBox
    Private WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabPageManualControl As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxHSTriggeredZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxESUTriggeredZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxESLTriggeredZ As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHSTriggeredY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxESUTriggeredY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxESLTriggeredY As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHSTriggeredX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxESUTriggeredX As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxESLTriggeredX As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonReadSwitchState As System.Windows.Forms.Button
    Friend WithEvents GroupBox22 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxMCJA As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxMCCSA As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxMCTA As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxMCJoystickAxisX As System.Windows.Forms.TextBox
    Private WithEvents Label56 As System.Windows.Forms.Label
    Private WithEvents TextBoxMCCS As System.Windows.Forms.TextBox
    Private WithEvents TextBoxMCJoystickAxisY As System.Windows.Forms.TextBox
    Private WithEvents Label59 As System.Windows.Forms.Label
    Private WithEvents Label57 As System.Windows.Forms.Label
    Private WithEvents TextBoxMCJoystickDeadZone As System.Windows.Forms.TextBox
    Private WithEvents TextBoxMCT As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxMCEnable As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox23 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButtonSerial As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonNetwork As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents GroupBox37 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox36 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxPort As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxIPAddress As System.Windows.Forms.TextBox
    Private WithEvents Label60 As System.Windows.Forms.Label
    Private WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents TabPageTemperatureSensors As TabPage
    Friend WithEvents FlowLayoutPanelTemperatureSensors As FlowLayoutPanel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents ButtonExportSettings As Button
    Friend WithEvents ButtonImportSettings As Button
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripSplitButton1 As ToolStripDropDownButton
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
End Class
