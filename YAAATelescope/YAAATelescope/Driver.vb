'tabs=4
' --------------------------------------------------------------------------------
'
' ASCOM Telescope driver for YAAATelescope
'
' Description:	Version 0.0.1 of the Yet Another Arduino Ascom Telescope driver.
'
' Implements:	ASCOM Telescope interface version: 1.0
' Author:		(GChr) Gregor Christandl <christandlg@yahoo.com>
'
' License: GPL v2 or later (see license.txt)
'
' Edit Log:
'
' Date			Who	    Vers	Description
' -----------	---	    -----	-------------------------------------------------------
' 2015-04-11	GChr	0.0.1	Initial release
' -------------------------------------------------------------------------------------
'
'
' Your driver's ID is ASCOM.YAAATelescope.Telescope
'
' The Guid attribute sets the CLSID for ASCOM.DeviceName.Telescope
' The ClassInterface/None addribute prevents an empty interface called
' _Telescope from being created and used as the [default] interface
'

' This definition is used to select code that's only applicable for one device type
#Const Device = "Telescope"

Imports ASCOM
Imports ASCOM.Astrometry
Imports ASCOM.Astrometry.AstroUtils
Imports ASCOM.DeviceInterface
Imports ASCOM.Utilities

Imports ASCOM.Astrometry.Transform
Imports ASCOM.Astrometry.NOVAS.NOVAS31

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports System.Text
Imports System.Xml

<Guid("6fe6b181-ee1a-40f1-a63d-31f40ec8b609")>
<ClassInterface(ClassInterfaceType.None)>
Public Class Telescope

    ' The Guid attribute sets the CLSID for ASCOM.YAAATelescope.Telescope
    ' The ClassInterface/None addribute prevents an empty interface called
    ' _YAAATelescope from being created and used as the [default] interface

    ' TODO Replace the not implemented exceptions with code to implement the function or
    ' throw the appropriate ASCOM exception.
    '
    Implements ITelescopeV3

    '
    ' Driver ID and descriptive string that shows in the Chooser
    '
    Friend Shared driverID As String = "ASCOM.YAAADevice.Telescope"
    Private Shared driverDescription As String = "YAAADevice Telescope"

    Enum connection_type_t
        SERIAL = 0
        NETWORK = 1
    End Enum

    Enum serial_error_code_t
        ERROR_NONE = 0      'not necessary?
        ERROR_UNKNOWN_DEVICE = 1
        ERROR_UNKNOWN_CMD = 2
        ERROR_UNKNOWN_CMD_TYPE = 4
        ERROR_INVALID_CMD_TYPE = 8
        ERROR_INVALID_PARAMETER = 16
        ERROR_MISSING_PARAMETER = 32
        ERROR_UNKNOWN_OPTION = 64
        ERROR_DEVICE_UNAVAILABLE = 128
        ERROR_DEVICE_LOCKED = 256       'command not accepted because device has been locked for local control.
    End Enum

    Enum limit_handling_t
        LIMIT_HANDLING_IGNORE = 0
        LIMIT_HANDLING_ADJUST = 1
        LIMIT_HANDLING_ERROR = 2
    End Enum

    Enum motor_driver_t
        ADAFRUIT_MOTORSHIELD_V2 = 0
        FOUR_WIRE_STEPPER = 1
        DRIVER_A4988 = 2
        DRIVER_DRV8825 = 3
    End Enum

    Enum step_type_t
        STEPTYPE_QUICKSTEP = 0              'for Adafruit Motor Shield V2.
        STEPTYPE_SINGLE = 1                 'for Adafruit Motor Shield V2.
        'STEPTYPE_DOUBLE                    'for Adafruit Motor Shield V2.
        STEPTYPE_FULL = 3                   'for stepper drivers and Adafruit Motor Shield V2 (DOUBLE).
        STEPTYPE_MICROSTEP_2 = 4            'for stepper drivers and Adafruit Motor Shield V2.
        STEPTYPE_MICROSTEP_4 = 5            'for stepper drivers.
        STEPTYPE_MICROSTEP_8 = 6            'for stepper drivers.
        STEPTYPE_MICROSTEP_16 = 7           'for stepper drivers and Adafruit Motor Shield V2.
        STEPTYPE_MICROSTEP_32 = 8           'for stepper drivers.
        STEPTYPE_MICROSTEP_64 = 9           'for stepper drivers.
        STEPTYPE_MICROSTEP_128 = 10         'for stepper drivers.
    End Enum

    Enum telescope_error_t As UInteger
        ERROR_NONE = 0
        ERROR_OUT_OF_BOUNDS_X = 1   '// x axis target position Is outside the movement limits.
        ERROR_OUT_OF_BOUNDS_Y = 2   '// y axis target position Is outside the movement limits.
        ERROR_PARKED = 4            '// Telescope Is currently parked.
        '//ERROR_MANUAL_CONTROL = 8	'//manual control Is active
        ERROR_INVALID_AXIS = 16     '//invaid axis given
        ERROR_HOMING = 32           '//telescope Is currently homing
        ERROR_NOT_AVAILABLE = 64    '//requested functionality Not available
        ERROR_OTHER = 128           '//todo: exchange for something meaningful.
    End Enum

    Enum telescope_homing_error_t As UInteger
        ERROR_HOMING_NONE = 0                           'no error occurred.
        ERROR_HOMING_LIMIT_LOWER = 8                    'homing position beyond lower movement limit
        ERROR_HOMING_LIMIT_UPPER = 16                   'homing position beyond upper movement limit
        ERROR_HOMING_SWITCH_NOT_AVAILABLE = 32          'no home switch available.
        '//ERROR_HOMING_SWITCH_NOT_TRIGGERED = 64		'homing switch has not been triggered, but both limits or endstops have been hit.	//TODO implement
        ERROR_HOMING_TIMEOUT = 128                      'homing operation timed out.
    End Enum

    Friend Const numTempSensors As Integer = 3

#Region "Constants used for Profile persistence"

    Public Shared comPortProfileName As String = "COM Port"
    Public Shared traceStateProfileName As String = "Trace Level"
    Public Shared baudRateProfileName As String = "Baud Rate"

    Public Shared connectionTypeProfileName As String = "Connection Type"
    Public Shared ipAddressProfileName As String = "IP Address"
    Public Shared portProfileName As String = "Port"

    Private Structure telescopeProfileNames
        Public Shared mountTypeProfileName As String = "Mount Type"

        Public Shared siteLatitudeProfileName As String = "Site Latitude"
        Public Shared siteLongitudeProfileName As String = "Site Longitude"
        Public Shared siteElevationProfileName As String = "Site Elevation"

        Public Shared focalLengthProfileName As String = "Focal Length"
        Public Shared apertureAreaProfileName As String = "Aperture Area"
        Public Shared apertureDiameterProfileName As String = "Aperture Diameter"

        Public Shared ParkEnabledProfileName As String = "Parking Enabled"
        Public Shared SAHProfileName As String = "Startup auto home"

        Public Shared MCAProfileName As String = "Manual Control available"
        Public Shared MCTAProfileName As String = "Manual Control Toggle Available"
        Public Shared MCCSAProfileName As String = "Manual Control Cycle Speed Available"
        Public Shared MCJAProfileName As String = "Manual Control Joystick Available"

        Public Shared MCTProfileName As String = "Manual Control Toggle"
        Public Shared MCCSProfileName As String = "Manual Control Cycle Speed"

        'Public Shared MCMSProfileName As String = "Manual Control Maximum Speed"
        'Public Shared MCSTProfileName As String = "Manual Control Step Type"

        Public Shared MCJXProfileName As String = "Manual Control X Axis"
        Public Shared MCJYProfileName As String = "Manual Control Y Axis"
        Public Shared MCJDProfileName As String = "Manual Control Deadzone"

        Public Shared TSDProfileName As String = "Device"
        'Public Shared TSTProfileName As String = "Type"
        Public Shared TSUProfileName As String = "Unit"
        Public Shared TSPProfileName As String = "Parameters"

        Public Shared TSSubKeyProfileName As String = "Temperature Sensor "
    End Structure

    Private Structure AxisProfileNames
        Public Shared accelerationProfileName As String = "Acceleration"
        Public Shared maxSpeedProfileName As String = "Maximum Speed"
        Public Shared stepTypeFastProfileName As String = "Steptype Fast"
        Public Shared stepTypeSlowProfileName As String = "Steptype Slow"
        Public Shared gearRatioProfileName As String = "Gear Ratio"
        Public Shared numberOfStepsProfileName As String = "Steps per Revolution"

        Public Shared homeAlignmentProfileName As String = "Home Alignment"
        Public Shared HSAProfileName As String = "Home Switch Available"
        Public Shared HSNProfileName As String = "Home Switch Number"
        Public Shared HSIProfileName As String = "Home Switch Inverted"
        Public Shared HDProfileName As String = "Homing Direction"

        Public Shared HASProfileName As String = "Home Alignment Sync"

        'Public Shared SAHProfileName As String = "Startup Auto Home"

        Public Shared ASRProfileName As String = "Automatic Stepper Release"

        Public Shared ParkEnabledProfileName As String = "Parking Enabled"
        Public Shared parkPosProfileName As String = "Park Position"

        Public Shared endStopLowerEnabledProfileName As String = "Lower Endstop Enabled"
        Public Shared endStopUpperEnabledProfileName As String = "Upper Endstop Enabled"
        Public Shared endStopLowerProfileName As String = "Lower Endstop Pin Nr"
        Public Shared endStopUpperProfileName As String = "Upper Endstop Pin Nr"
        Public Shared endstopLowerInvertedProfileName As String = "Lower Endstop Inverted"
        Public Shared endstopUpperInvertedProfileName As String = "Upper Endstop Inverted"
        Public Shared endstopHardStop As String = "Endstop Hard Stop"

        Public Shared limitLowerEnabledProfileName As String = "Lower Limit Enabled"
        Public Shared limitUpperEnabledProfileName As String = "Upper Limit Enabled"
        Public Shared limitLowerProfileName As String = "Lower Limit"
        Public Shared limitUpperProfileName As String = "Upper Limit"
        Public Shared limitHardStop As String = "Limit Hard Stop"

        Public Shared limitHandlingProfileName As String = "Limit Handling"

        Public Shared motorDriverProfileName As String = "Motor Driver Type"

        Public Shared I2CAddressProfileName As String = "I2C Address"
        Public Shared terminalProfileName As String = "Terminal"

        Public Shared pin1ProfileName As String = "Pin 1"
        Public Shared pin2ProfileName As String = "Pin 2"
        Public Shared pin3ProfileName As String = "Pin 3"
        Public Shared pin4ProfileName As String = "Pin 4"
        Public Shared pin5ProfileName As String = "Pin 5"
        Public Shared pin6ProfileName As String = "Pin 6"

        Public Shared pin1InvertedProfileName As String = "Pin 1 Inverted"
        Public Shared pin2InvertedProfileName As String = "Pin 2 Inverted"
        Public Shared pin3InvertedProfileName As String = "Pin 3 Inverted"
        Public Shared pin4InvertedProfileName As String = "Pin 4 Inverted"
        Public Shared pin5InvertedProfileName As String = "Pin 5 Inverted"
        Public Shared pin6InvertedProfileName As String = "Pin 6 Inverted"
    End Structure

    Friend Shared guideRateDecProfileName As String = "Guide Rate Declination"
    Friend Shared guideRateRaProfileName As String = "Guide Rate Right Ascension"

    Public Shared settingsStoreDirectory = YAAASharedClassLibrary.YAAASETTINGS_DIRECTORY & "/YAAATelescope"

    Private SET_CONNECTED_DELAY As Integer = 5000

    Private TIMEOUT_SERIAL As Integer = 5000
    Private TIMEOUT_TCP As UInteger = 5000
#End Region

#Region "default values"

    Friend Shared comPortDefault As String = "COM6"
    Friend Shared baudRateDefault As Integer = 9600
    Friend Shared traceStateDefault As Boolean = True

    Friend Shared connectionTypeDefault As connection_type_t = connection_type_t.SERIAL
    Friend Shared ipAddressDefault As String = "192.168.0.202"
    Friend Shared portDefault As Integer = 23

    Private Structure axisDefaultValues
        Public Shared maxSpeedDefault As Single = 400.0
        Public Shared accelerationDefault As Single = 100.0
        Public Shared gearRatioDefault As Single = 150.0
        Public Shared numberOfStepsDefault As UShort = 200
        Public Shared stepTypeFastDefault As step_type_t = step_type_t.STEPTYPE_FULL
        Public Shared stepTypeSlowDefault As step_type_t = step_type_t.STEPTYPE_FULL

        Public Shared HSADefault As Boolean = False
        Public Shared HSNDefault As Byte = 47
        Public Shared HSIDefault As Boolean = False
        Public Shared HDDefault As Short = 1
        Public Shared homeAlignmentDefault As Single = 0.0

        Public Shared HASDefault As Boolean = False             'true if home alignment is overwritten when syncing in the home position.

        Public Shared SAHDefault As Boolean = False

        Public Shared ASRDefault As Boolean = True

        Public Shared parkEnabledDefault As Boolean = True
        Public Shared parkPosDefault As Single = 0.0

        Public Shared endstopLowerEnabledDefault As Boolean = False
        Public Shared endstopUpperEnabledDefault As Boolean = False
        Public Shared endstopLowerDefault As Byte = 38
        Public Shared endstopUpperDefault As Byte = 39
        Public Shared endstopLowerInvertedDefault As Boolean = False
        Public Shared endstopUpperInvertedDefault As Boolean = False
        Public Shared endstopHardStopDefault As Boolean = True

        Public Shared limitLowerEnabledDefault As Boolean = False
        Public Shared limitUpperEnabledDefault As Boolean = False
        Public Shared limitLowerDefault As Single = 0.0
        Public Shared limitUpperDefault As Single = 90.0
        Public Shared limitHardStopDefault As Boolean = True

        Public Shared limitHandlingDefault As limit_handling_t = limit_handling_t.LIMIT_HANDLING_ERROR

        Public Shared motorDriverDefault As motor_driver_t = motor_driver_t.ADAFRUIT_MOTORSHIELD_V2

        Public Shared I2CAddressDefault As Byte = 96
        Public Shared terminalDefault As Byte = 1

        Public Shared pin1Default As Byte = 255
        Public Shared pin2Default As Byte = 255
        Public Shared pin3Default As Byte = 255
        Public Shared pin4Default As Byte = 255
        Public Shared pin5Default As Byte = 255
        Public Shared pin6Default As Byte = 255

        Public Shared pin1InvertedDefault As Boolean = False
        Public Shared pin2InvertedDefault As Boolean = False
        Public Shared pin3InvertedDefault As Boolean = False
        Public Shared pin4InvertedDefault As Boolean = False
        Public Shared pin5InvertedDefault As Boolean = False
        Public Shared pin6InvertedDefault As Boolean = False
    End Structure

    Private Structure telescopeDefaultValues
        Public Shared mountTypeDefault As ASCOM.DeviceInterface.AlignmentModes = ASCOM.DeviceInterface.AlignmentModes.algAltAz

        Public Shared siteLatitudeDefault As Double = 47.0
        Public Shared siteLongitudeDefault As Double = 15.0
        Public Shared siteElevationDefault As Double = 300.0

        Public Shared focalLengthDefault As Double = 1.2            'for a 10" telescope.
        Public Shared apertureAreaDefault As Double = 0.203         'for a 10" telescope.
        Public Shared apertureDiameterDefault As Double = 0.254     'for a 10" telescope.

        Public Shared parkEnabledDefault As Boolean = True
        Public Shared SAHDefault As Boolean = False

        Public Shared MCADefault As Boolean = False             'manual control available
        Public Shared MCTADefault As Boolean = False            'manual control toggle swtich available
        Public Shared MCCSADefault As Boolean = False           'manaul control cycle speed switch available
        Public Shared MCJADefault As Boolean = False            'manaul control joystick available

        Public Shared MCTDefault As Byte = 255                  'manual control toggle switch pin number
        Public Shared MCCSDefault As Byte = 255                 'manual control cycle speed pin number

        'Public Shared manualControlMaxSpeed As Single = 5.0
        'Public Shared manualControlSteptype As String = step_type_t.STEPTYPE_FULL.ToString

        Public Shared MCJXDefault As Byte = 255                 'manual control joystick x axis pin
        Public Shared MCJYDefault As Byte = 255                 'manual control joystick y axis pin
        Public Shared MCJDDefault As Byte = 10                  'manual control joystick deadzone
    End Structure

    Friend Shared guideRateDecDefault As Double = 0.1
    Friend Shared guideRateRaDefault As Double = 0.1
#End Region

#Region "Variables to hold the currrent device configuration"

    <DataContract>
    Friend Structure telescopeDriverSettings
        Implements ICloneable

        <DataMember()>
        Public Shared trace_state_ As Boolean = traceStateDefault

        <DataMember()>
        Public connection_type_ As connection_type_t

        'serial connection settings
        <DataMember()>
        Public com_port_ As String
        <DataMember()>
        Public baud_rate_ As Integer

        'TCP connection settings
        <DataMember()>
        Public ip_address_ As String
        <DataMember()>
        Public port_ As Integer

        'settings that are shared with the Arduino
        <DataMember()>
        Public telescope_settings_ As telescopeSettings
        <DataMember()>
        Public axis_settings_() As axisSettings

        'Public temperature_sensors_() As temperatureSensorSettings

        Public Sub New(Optional initialize As Boolean = True)
            If Not initialize Then
                Exit Sub
            End If

            connection_type_ = connectionTypeDefault

            'serial connection settings
            com_port_ = comPortDefault
            baud_rate_ = baudRateDefault

            'TCP connection settings
            ip_address_ = ipAddressDefault
            port_ = portDefault

            telescope_settings_ = New telescopeSettings(True)           'uses initializing constructor
            axis_settings_ = {New axisSettings(True, "X Axis"), New axisSettings(True, "Y Axis"), New axisSettings(True, "Z Axis")}                 'uses initializing constructor

            'temperature_sensors_ = {New temperatureSensorSettings(True), New temperatureSensorSettings(True), New temperatureSensorSettings(True)}  'uses initializing constructor
            'ReDim temperature_sensors_(2)
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As telescopeDriverSettings = MemberwiseClone()

            'new array for sensors
            Dim axis_settings(axis_settings_.Length - 1) As axisSettings

            'copy sensors to new array
            Array.Copy(axis_settings_, axis_settings, axis_settings_.Length)

            'replace the reference created by MemberwiseClone() by a deep copy of the array
            return_value.axis_settings_ = axis_settings

            Return return_value
        End Function

    End Structure

    <DataContract>
    Friend Structure axisSettings
        Implements ICloneable

        <DataMember()>
        Public axis_name_ As String
        <DataMember()>
        Public max_speed_ As Single
        <DataMember()>
        Public acceleration_ As Single
        <DataMember()>
        Public gear_ratio_ As Single
        <DataMember()>
        Public number_of_steps_ As UShort
        <DataMember()>
        Public step_type_fast_ As step_type_t       'step type for fast movement (slew and moveAxis)
        <DataMember()>
        Public step_type_slow_ As step_type_t       'step type for slow movement (tracking, pulse guiding)

        <DataMember()>
        Public home_switch_enabled_ As Boolean
        <DataMember()>
        Public home_switch_nr_ As Byte              'home switch input pin
        <DataMember()>
        Public home_alignment_ As Single            'home position alignment in degrees.
        <DataMember()>
        Public homing_direction_ As Short           'homing direction (currently not used)
        <DataMember()>
        Public home_switch_inverted_ As Boolean     'true if home switch is active high.

        <DataMember()>
        Public home_alignment_sync_ As Boolean      'when true, syncing in the home position updates the stored home position as well.

        <DataMember()>
        Public startup_auto_home_ As Boolean        'true if homing sequence should start on Arduino startup

        <DataMember()>
        Public automatic_stepper_release_ As Boolean    'when true steppers are automatically released when movement is stopped.

        <DataMember()>
        Public park_enabled_ As Boolean             'true when parking is enabled.
        <DataMember()>
        Public park_pos_ As Single                  'park position in degrees.

        <DataMember()>
        Public endstop_lower_enabled_ As Boolean    'true if a lower endstop switch is enabled.
        <DataMember()>
        Public endstop_upper_enabled_ As Boolean
        <DataMember()>
        Public endstop_lower_ As Byte               'lower endstop input pin
        <DataMember()>
        Public endstop_upper_ As Byte               'upper endstop input pin
        <DataMember()>
        Public endstop_lower_inverted_ As Boolean   'true if the endstop switch is active high.
        <DataMember()>
        Public endstop_upper_inverted_ As Boolean

        <DataMember()>
        Public endstop_hard_stop_ As Boolean        'when true steppers do not decelerate when an endstop is hit.

        <DataMember()>
        Public limit_lower_enabled_ As Boolean      'true if this axis has an upper movement limit.
        <DataMember()>
        Public limit_upper_enabled_ As Boolean
        <DataMember()>
        Public limit_lower_ As Single               'upper movement limit in degrees.
        <DataMember()>
        Public limit_upper_ As Single               'lower movement limit in degrees.
        <DataMember()>
        Public limit_hard_stop_ As Boolean          'when true steppers do not decelerate when a limit is reached.

        <DataMember()>
        Public limit_handling_ As limit_handling_t  'defines how to treat slew commands that violate movement limits.

        <DataMember()>
        Public motor_driver_ As motor_driver_t   'Type of stepper motor driver.

        <DataMember()>
        Public i2c_address_ As Byte            'Adafruit Motor Shield I2C Address.
        <DataMember()>
        Public terminal_ As Byte               'Adafruit Motor Shield Terminal.

        <DataMember()>
        Public pin_1_ As Byte                  'pin 1 (4 wire stepper) or enable pin.
        <DataMember()>
        Public pin_2_ As Byte                  'pin 2 (4 wire stepper) or direction pin.
        <DataMember()>
        Public pin_3_ As Byte                  'pin 3 (4 wire stepper) or step pin.
        <DataMember()>
        Public pin_4_ As Byte                  'pin 4 (4 wire stepper) or MS1 pin.
        <DataMember()>
        Public pin_5_ As Byte                  'MS2 pin.
        <DataMember()>
        Public pin_6_ As Byte                  'MS3 pin.

        <DataMember()>
        Public pin_1_inverted_ As Boolean       'pin 1 (4 wire stepper) or enable pin.
        <DataMember()>
        Public pin_2_inverted_ As Boolean       'pin 2 (4 wire stepper) or direction pin.
        <DataMember()>
        Public pin_3_inverted_ As Boolean       'pin 3 (4 wire stepper) or step pin.
        <DataMember()>
        Public pin_4_inverted_ As Boolean       'pin 4 (4 wire stepper) or MS1 pin.
        <DataMember()>
        Public pin_5_inverted_ As Boolean       'MS2 pin.
        <DataMember()>
        Public pin_6_inverted_ As Boolean       'MS3 pin.

        Public Sub New(Optional initialize As Boolean = True, Optional name As String = "")
            If Not initialize Then
                Exit Sub
            End If

            axis_name_ = name
            max_speed_ = axisDefaultValues.maxSpeedDefault
            acceleration_ = axisDefaultValues.accelerationDefault
            gear_ratio_ = axisDefaultValues.gearRatioDefault
            number_of_steps_ = axisDefaultValues.numberOfStepsDefault
            step_type_fast_ = axisDefaultValues.stepTypeFastDefault
            step_type_slow_ = axisDefaultValues.stepTypeSlowDefault

            home_switch_enabled_ = axisDefaultValues.HSADefault
            home_switch_nr_ = axisDefaultValues.HSNDefault
            home_switch_inverted_ = axisDefaultValues.HSIDefault
            home_alignment_ = axisDefaultValues.homeAlignmentDefault
            homing_direction_ = axisDefaultValues.HDDefault

            home_alignment_sync_ = axisDefaultValues.HASDefault

            startup_auto_home_ = axisDefaultValues.SAHDefault

            automatic_stepper_release_ = axisDefaultValues.ASRDefault

            park_enabled_ = axisDefaultValues.parkEnabledDefault
            park_pos_ = axisDefaultValues.parkPosDefault

            endstop_lower_enabled_ = axisDefaultValues.endstopLowerEnabledDefault
            endstop_upper_enabled_ = axisDefaultValues.endstopUpperEnabledDefault
            endstop_lower_ = axisDefaultValues.endstopLowerDefault
            endstop_upper_ = axisDefaultValues.endstopUpperDefault
            endstop_lower_inverted_ = axisDefaultValues.endstopLowerInvertedDefault
            endstop_upper_inverted_ = axisDefaultValues.endstopUpperEnabledDefault

            endstop_hard_stop_ = axisDefaultValues.endstopHardStopDefault

            limit_lower_enabled_ = axisDefaultValues.limitLowerEnabledDefault
            limit_upper_enabled_ = axisDefaultValues.limitUpperEnabledDefault
            limit_lower_ = axisDefaultValues.limitLowerDefault
            limit_upper_ = axisDefaultValues.limitUpperDefault
            limit_hard_stop_ = axisDefaultValues.limitHardStopDefault

            limit_handling_ = axisDefaultValues.limitHandlingDefault

            motor_driver_ = axisDefaultValues.motorDriverDefault

            i2c_address_ = axisDefaultValues.I2CAddressDefault
            terminal_ = axisDefaultValues.terminalDefault

            pin_1_ = axisDefaultValues.pin1Default
            pin_2_ = axisDefaultValues.pin2Default
            pin_3_ = axisDefaultValues.pin3Default
            pin_4_ = axisDefaultValues.pin4Default
            pin_5_ = axisDefaultValues.pin5Default
            pin_6_ = axisDefaultValues.pin6Default

            pin_1_inverted_ = axisDefaultValues.pin1InvertedDefault
            pin_2_inverted_ = axisDefaultValues.pin2InvertedDefault
            pin_3_inverted_ = axisDefaultValues.pin3InvertedDefault
            pin_4_inverted_ = axisDefaultValues.pin4InvertedDefault
            pin_5_inverted_ = axisDefaultValues.pin5InvertedDefault
            pin_6_inverted_ = axisDefaultValues.pin6InvertedDefault
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Return MemberwiseClone()
        End Function

    End Structure

    <DataContract>
    Friend Structure telescopeSettings
        Implements ICloneable

        <DataMember()>
        Public mount_type_ As ASCOM.DeviceInterface.AlignmentModes
        <DataMember()>
        Public focal_length_ As Double
        <DataMember()>
        Public aperture_area_ As Double
        <DataMember()>
        Public aperture_diameter_ As Double

        <DataMember()>
        Public site_latitude_ As Double
        <DataMember()>
        Public site_longitude_ As Double
        <DataMember()>
        Public site_elevation_ As Double

        <DataMember()>
        Friend startup_auto_home_ As Boolean         'true if the telescope's axes should automatically home on startup.
        <DataMember()>
        Friend park_enabled_ As Boolean              'true if the telescope can be parked.

        <DataMember()>
        Public mc_available_ As Boolean              'true if manual control is available.
        <DataMember()>
        Public mc_toggle_available_ As Boolean       'manaul control toggle switch available
        <DataMember()>
        Public mc_cycle_speed_available_ As Boolean  'manual control cycle speed switch available
        <DataMember()>
        Public mc_joystick_available_ As Boolean     'manual control joystick available

        <DataMember()>
        Public mc_toggle_ As Byte                    'pin to enable / disable manual control.
        <DataMember()>
        Public mc_cycle_speed_ As Byte               'pin to cycle through steptypes.

        'Public mc_max_speed As Single              'maximum speed (degrees / second) for manual control.
        'Public mc_steptype As step_type_t          'default steptype for manual control.

        <DataMember()>
        Public mc_joystick_x_ As Byte               'analog input pin for joystick x axis.
        <DataMember()>
        Public mc_joystick_y_ As Byte               'analog input pin for joystick y axis.
        <DataMember()>
        Public mc_joystick_deadzone_ As Byte        'joystick deadzone.

        <DataMember()>
        Public temperature_sensors_() As YAAASharedClassLibrary.YAAASensor

        Public Sub New(Optional initialize As Boolean = True)
            If Not initialize Then
                Exit Sub
            End If

            mount_type_ = telescopeDefaultValues.mountTypeDefault
            focal_length_ = telescopeDefaultValues.focalLengthDefault
            aperture_area_ = telescopeDefaultValues.apertureAreaDefault
            aperture_diameter_ = telescopeDefaultValues.apertureDiameterDefault

            site_latitude_ = telescopeDefaultValues.siteLatitudeDefault
            site_longitude_ = telescopeDefaultValues.siteLongitudeDefault
            site_elevation_ = telescopeDefaultValues.siteElevationDefault

            startup_auto_home_ = telescopeDefaultValues.SAHDefault
            park_enabled_ = telescopeDefaultValues.parkEnabledDefault

            mc_available_ = telescopeDefaultValues.MCADefault
            mc_toggle_available_ = telescopeDefaultValues.MCTADefault
            mc_cycle_speed_available_ = telescopeDefaultValues.MCCSADefault
            mc_joystick_available_ = telescopeDefaultValues.MCJADefault

            mc_toggle_ = telescopeDefaultValues.MCTDefault
            mc_cycle_speed_ = telescopeDefaultValues.MCCSDefault

            ' mc_max_speed_ As Single              'maximum speed (degrees / second) for manual control.
            ' mc_steptype_ As step_type_t          'default steptype for manual control.

            mc_joystick_x_ = telescopeDefaultValues.MCJXDefault
            mc_joystick_y_ = telescopeDefaultValues.MCJYDefault
            mc_joystick_deadzone_ = telescopeDefaultValues.MCJDDefault

            temperature_sensors_ = {New YAAASharedClassLibrary.YAAASensor(True), New YAAASharedClassLibrary.YAAASensor(True), New YAAASharedClassLibrary.YAAASensor(True)}  'uses initializing constructor

        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As telescopeSettings = MemberwiseClone()

            'new array for sensors
            Dim sensors(temperature_sensors_.Length - 1) As YAAASharedClassLibrary.YAAASensor

            'copy sensors to new array
            Array.Copy(temperature_sensors_, sensors, temperature_sensors_.Length)

            'replace the reference created by MemberwiseClone() by a deep copy of the array
            return_value.temperature_sensors_ = sensors

            Return return_value
        End Function

    End Structure

    Private telescope_driver_settings_ As telescopeDriverSettings
    Private telescope_driver_settings_EEPROM_ As telescopeDriverSettings

    Private guide_rate_declination_ As Double
    Private guide_rate_rightascension_ As Double

    Private tracking_rate_ As DriveRates = DriveRates.driveSidereal     'default tracking rate is sidereal
    Private declination_rate_ As Double = 0.0       'the last set or read declination tracking rate offset.
    Private right_ascension_rate_ As Double = 0.0   'the last set or read right ascension rate offset.

    Private target_declination_ As Double = Double.NaN
    Private target_right_ascension_ As Double = Double.NaN

    Friend EEPROM_force_write_ As Boolean = False

#End Region

    Private utilities As Util ' Private variable to hold an ASCOM Utilities object
    Private astroUtilities As AstroUtils ' Private variable to hold an AstroUtils object to provide the Range method
    Private TL As TraceLogger ' Private variable to hold the trace logger object (creates a diagnostic log file with information that you specify)
    Private transform_ As ASCOM.Astrometry.Transform.Transform
    Private novas31_ As ASCOM.Astrometry.NOVAS.NOVAS31

    Friend serial_connection_ As ASCOM.Utilities.Serial

    Friend tcp_connection_ As Net.Sockets.TcpClient
    'Friend tcp_read_ As IO.StreamReader
    'Friend tcp_write_ As IO.StreamWriter

    '
    ' Constructor - Must be public for COM registration!
    '
    Public Sub New()
        TL = New TraceLogger("", "YAAATelescope")

        TL.Enabled = False

        'create telescope driver settings object containing driver, telescope and axis settings.
        telescope_driver_settings_ = New telescopeDriverSettings(True)
        telescope_driver_settings_EEPROM_ = New telescopeDriverSettings(True)

        ReadProfile() ' Read device configuration from the ASCOM Profile store

        TL.Enabled = telescopeDriverSettings.trace_state_

        TL.LogMessage("Telescope", "Starting initialisation")

        utilities = New Util() ' Initialise util object
        astroUtilities = New AstroUtils 'Initialise new astro utiliites object

        'TODO: Implement your additional construction here
        serial_connection_ = New ASCOM.Utilities.Serial
        transform_ = New ASCOM.Astrometry.Transform.Transform

        novas31_ = New ASCOM.Astrometry.NOVAS.NOVAS31

        'initialize variables / properties
        guide_rate_declination_ = guideRateDecDefault
        guide_rate_rightascension_ = guideRateRaDefault

        transform_.SiteLatitude = telescope_driver_settings_.telescope_settings_.site_latitude_
        transform_.SiteLongitude = telescope_driver_settings_.telescope_settings_.site_longitude_
        transform_.SiteElevation = telescope_driver_settings_.telescope_settings_.site_elevation_
        transform_.SiteTemperature = 0.0

        settingsStoreDirectory = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), settingsStoreDirectory)

        TL.LogMessage("Telescope", "Completed initialisation")
    End Sub

    '
    ' PUBLIC COM INTERFACE ITelescopeV3 IMPLEMENTATION
    '

#Region "Common properties and methods"
    ''' <summary>
    ''' Displays the Setup Dialog form.
    ''' If the user clicks the OK button to dismiss the form, then
    ''' the new settings are saved, otherwise the old values are reloaded.
    ''' THIS IS THE ONLY PLACE WHERE SHOWING USER INTERFACE IS ALLOWED!
    ''' </summary>
    Public Sub SetupDialog() Implements ITelescopeV3.SetupDialog
        ' consider only showing the setup dialog if not connected
        ' or call a different dialog if connected
        If IsConnected Then
            System.Windows.Forms.MessageBox.Show("Already connected, just press OK")
        End If

        Using F As SetupDialogForm = New SetupDialogForm(Me)
            Dim result As System.Windows.Forms.DialogResult = F.ShowDialog()
            If result = DialogResult.OK Then
                WriteProfile() ' Persist device configuration values to the ASCOM Profile store
            End If
        End Using
    End Sub

    Public ReadOnly Property SupportedActions() As ArrayList Implements ITelescopeV3.SupportedActions
        Get
            TL.LogMessage("SupportedActions Get", "Returning array list containing action names")

            Dim actions = New ArrayList()

            'add supported actions.
            actions.Add("Get_Switch_State")         'return states of limit and home switches
            actions.Add("Measure_Temperature")      'triggers a temperature measurement
            actions.Add("Has_Temperature")          'returns true when a temperature measurement has been completed
            actions.Add("Get_Temperature")          'get last measured temperature
            actions.Add("Move_XAxis")
            actions.Add("Move_YAxis")
            actions.Add("Move_ZAxis")
            actions.Add("Get_Homing_Error")          'get error codes of last homing operation.
            actions.Add("Axis_At_Home")
            Return actions
        End Get
    End Property

    Public Function Action(ByVal ActionName As String, ByVal ActionParameters As String) As String Implements ITelescopeV3.Action

        Select Case (ActionName)
            Case "Get_Switch_State"
                Return getSwitchState(ActionParameters)
            Case "Measure_Temperature"
                Return measureTemperature(ActionParameters)
            Case "Has_Temperature"
                Return hasTemperature(ActionParameters)
            Case "Get_Temperature"
                Return getTemperature(ActionParameters)
            Case "Move_XAxis"
                Return move("X_AXIS", ActionParameters)
            Case "Move_YAxis"
                Return move("Y_AXIS", ActionParameters)
            Case "Move_ZAxis"
                Return move("Z_AXIS", ActionParameters)
            Case "Get_Homing_Error"
                Return getHomingError().ToString
            Case "Axis_At_Home"
                Return axisAtHome(ActionParameters)
        End Select

        Throw New ActionNotImplementedException("Action " & ActionName & " Is Not supported by this driver")
    End Function

    Public Sub CommandBlind(ByVal Command As String, Optional ByVal Raw As Boolean = False) Implements ITelescopeV3.CommandBlind
        CheckConnected("CommandBlind")
        ' Call CommandString and return as soon as it finishes
        Me.CommandString(Command, Raw)
        ' or
        Throw New MethodNotImplementedException("CommandBlind")
    End Sub

    Public Function CommandBool(ByVal Command As String, Optional ByVal Raw As Boolean = False) As Boolean _
        Implements ITelescopeV3.CommandBool
        CheckConnected("CommandBool")
        Dim ret As String = CommandString(Command, Raw)
        ' TODO decode the return string and return true or false
        ' or
        Throw New MethodNotImplementedException("CommandBool")
    End Function

    Public Function CommandString(ByVal Command As String, Optional ByVal Raw As Boolean = False) As String _
        Implements ITelescopeV3.CommandString
        CheckConnected("CommandString")
        ' it's a good idea to put all the low level communication with the device here,
        ' then all communication calls this function
        ' you need something to ensure that only one command is in progress at a time
        Throw New MethodNotImplementedException("CommandString")
    End Function

    Public Property Connected() As Boolean Implements ITelescopeV3.Connected
        Get
            Dim cn As Boolean = IsConnected
            TL.LogMessage("Connected Get", cn.ToString())
            Return cn
        End Get

        Set(value As Boolean)
            TL.LogMessage("Connected Set", value.ToString())

            If value Then
                'connect to hardware
                IsConnected = True

                'todo: allow Arduino to send wakeup message before attempting to contact Arduino?

                'check connection
                Dim response As String

                Try
                    response = sendCommand("CONNECTED,GET")
                Catch Exc As System.TimeoutException
                    TL.LogMessage("Connected Set", "Error: Timeout, check connection")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.Utilities.Exceptions.SerialPortInUseException
                    TL.LogMessage("Connected Set", "Error: unable to acquire the serial port")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.NotConnectedException
                    TL.LogMessage("Connected Set", "Error: serial port is not connected")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As Exception
                    TL.LogMessage("Connected Set", "Error: unknown error, check inner exception")   'TODO timeout exceptions are acutally caught here
                    Throw New DriverException(Exc.Message, Exc)
                End Try

                response = response.Replace("CONNECTED,", "").Trim

                If Not IsNumeric(response) Then
                    TL.LogMessage("Connected Set", "Error: could not check connection state because response contained non-numeric elements: " & response)
                    Throw New DriverException("could not check connection state because response contained non-numeric elements: " & response)
                End If

                If CInt(response).Equals(0) Then
                    TL.LogMessage("Connected Set", "Error: Arduino responding, but Telescope is not enabled")

                    'disconnect from hardware
                    IsConnected = False

                    Throw New ASCOM.DriverException("Connected Set Error: Arduino responding, but Telescope is not enabled")
                ElseIf CInt(response).Equals(1) Then
                    TL.LogMessage("Connected Set", "Successfully connected")
                Else
                    TL.LogMessage("Connected Set", "Error: unknown Response: " & response)
                    Throw New ASCOM.DriverException("Connected Set Error: unknown Response: " & response)
                End If

                'read current alignment of the telescope and update values of transform_.
                'ReadTelescopeAlignment()

            Else
                IsConnected = False
            End If
        End Set
    End Property

    Public ReadOnly Property Description As String Implements ITelescopeV3.Description
        Get
            ' this pattern seems to be needed to allow a public property to return a private field
            Dim d As String = driverDescription
            TL.LogMessage("Description Get", d)
            Return d
        End Get
    End Property

    Public ReadOnly Property DriverInfo As String Implements ITelescopeV3.DriverInfo
        Get
            Dim m_version As Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version
            ' TODO customise this driver description
            Dim s_driverInfo As String = "Information about the driver itself. Version: " + m_version.Major.ToString() + "." + m_version.Minor.ToString()
            TL.LogMessage("DriverInfo Get", s_driverInfo)
            Return s_driverInfo
        End Get
    End Property

    Public ReadOnly Property DriverVersion() As String Implements ITelescopeV3.DriverVersion
        Get
            ' Get our own assembly and report its version number
            TL.LogMessage("DriverVersion Get", Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2))
            Return Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2)
        End Get
    End Property

    Public ReadOnly Property InterfaceVersion() As Short Implements ITelescopeV3.InterfaceVersion
        Get
            TL.LogMessage("InterfaceVersion Get", "3")
            Return 3
        End Get
    End Property

    Public ReadOnly Property Name As String Implements ITelescopeV3.Name
        Get
            Dim s_name As String = "YAAATelescope"
            TL.LogMessage("Name Get", s_name)
            Return s_name
        End Get
    End Property

    Public Sub Dispose() Implements ITelescopeV3.Dispose
        ' Clean up the tracelogger and util objects
        TL.Enabled = False
        TL.Dispose()
        TL = Nothing

        serial_connection_.Dispose()
        serial_connection_ = Nothing

        transform_.Dispose()
        transform_ = Nothing

        utilities.Dispose()
        utilities = Nothing

        astroUtilities.Dispose()
        astroUtilities = Nothing

        novas31_.Dispose()
        novas31_ = Nothing
    End Sub

#End Region

#Region "ITelescope Implementation"

    Public Sub AbortSlew() Implements ITelescopeV3.AbortSlew
        'throw an exception if the telescope is not connected.
        CheckConnected("AbortSlew")

        'throw an exception if the telescope cannot be slewed.
        If Not CanSlew Then
            TL.LogMessage("AbortSlew", "Not implemented ")
            Throw New ASCOM.MethodNotImplementedException("AbortSlew")
        End If

        Dim response As String

        response = sendCommand("ABORT_SLEW,SET")
        response = response.Replace("ABORT_SLEW,", "").Trim

        Dim return_value = CUInt(response)

        logErrorCode("AbortSlew", return_value, GetType(telescope_error_t))

        If return_value.Equals(CUInt(0)) Then
            'no error.
            TL.LogMessage("AbortSlew", "success")

        ElseIf (return_value And telescope_error_t.ERROR_PARKED) <> 0 Then
            'raise an error when the telescope is parked.
            TL.LogMessage("AbortSlew", "Error: Telescope Parked")
            Throw New ASCOM.ParkedException("telescope parked")

        ElseIf (return_value And telescope_error_t.ERROR_HOMING) <> 0 Then
            'raise an error when manual control is active.
            TL.LogMessage("AbortSlew", "Error: telescope is currently homing")
            Throw New ASCOM.InvalidOperationException("telescope is currently homing")

        Else : TL.LogMessage("AbortSlew", "Error: unknown response: " & response)

        End If
    End Sub

    Public ReadOnly Property AlignmentMode() As AlignmentModes Implements ITelescopeV3.AlignmentMode
        Get
            TL.LogMessage("AlignmentMode Get", telescope_driver_settings_.telescope_settings_.mount_type_.ToString) ' telescope_settings_.mount_type.ToString)
            Return telescope_driver_settings_.telescope_settings_.mount_type_  'telescope_settings_.mount_type
        End Get
    End Property

    Public ReadOnly Property Altitude() As Double Implements ITelescopeV3.Altitude
        Get
            CheckConnected("Altitude")

            'read and save current telescope alignment.
            ReadTelescopeAlignment()

            Dim alt = transform_.ElevationTopocentric

            TL.LogMessage("Altitude", "Get - " & utilities.DegreesToDMS(alt))

            Return alt
        End Get
    End Property

    Public ReadOnly Property ApertureArea() As Double Implements ITelescopeV3.ApertureArea
        Get
            TL.LogMessage("ApertureArea Get", telescope_driver_settings_.telescope_settings_.aperture_area_) 'telescope_settings_.aperture_area)
            Return telescope_driver_settings_.telescope_settings_.aperture_area_ 'telescope_settings_.aperture_area
        End Get
    End Property

    Public ReadOnly Property ApertureDiameter() As Double Implements ITelescopeV3.ApertureDiameter
        Get
            TL.LogMessage("ApertureDiameter Get", telescope_driver_settings_.telescope_settings_.aperture_diameter_) 'telescope_settings_.aperture_diameter)
            Return telescope_driver_settings_.telescope_settings_.aperture_diameter_  'telescope_settings_.aperture_diameter
        End Get
    End Property

    Public ReadOnly Property AtHome() As Boolean Implements ITelescopeV3.AtHome
        Get
            'when the telescope is not connected, throw an exception.
            CheckConnected("AtHome")

            'when the telescope cannot find its home position, return false (ASCOM requirement).
            If Not CanFindHome Then
                TL.LogMessage("AtHome Get", "Homing not available")
                Return False
            End If

            'sends at home query to Ardunio. 
            'Arduino will return TRUE ("1") when both axes are at the home position.
            Dim return_value As Boolean
            Dim response As String

            response = sendCommand("AT_HOME,GET")
            response = response.Replace("AT_HOME,", "").Trim

            return_value = Not CInt(response).Equals(0)

            TL.LogMessage("AtHome", "Get - " & return_value.ToString())
            Return return_value
        End Get
    End Property

    Public ReadOnly Property AtPark() As Boolean Implements ITelescopeV3.AtPark
        Get
            'when the telescope is not connected, throw an exception.
            CheckConnected("AtPark")

            'when the telescope cannot be parked, return false (ASCOM requirement).
            If Not CanPark Then
                TL.LogMessage("AtPark Get", "Parking not available")
                Return False
            End If

            Dim response As String
            Dim return_value As Boolean

            response = sendCommand("PARKED,GET")
            response = response.Replace("PARKED,", "").Trim

            return_value = Not CInt(response).Equals(0)

            TL.LogMessage("AtPark", "Get - " & return_value.ToString())
            Return return_value
        End Get
    End Property

    Public Function AxisRates(Axis As TelescopeAxes) As IAxisRates Implements ITelescopeV3.AxisRates
        TL.LogMessage("AxisRates", "Get - " & Axis.ToString())
        Return New AxisRates(Axis)
    End Function

    Public ReadOnly Property Azimuth() As Double Implements ITelescopeV3.Azimuth
        Get
            'when the telescope is not connected, throw an exception.
            CheckConnected("Azimuth")

            'read and save current telescope alignment.
            ReadTelescopeAlignment()

            Dim az = transform_.AzimuthTopocentric

            TL.LogMessage("Azimuth", "Get - " & utilities.DegreesToDMS(az))

            Return az
        End Get
    End Property

    Public ReadOnly Property CanFindHome() As Boolean Implements ITelescopeV3.CanFindHome
        Get
            Dim return_value As Boolean

            'the telescope can find its home position when a home switch is available for at least
            'one axis.
            return_value = telescope_driver_settings_.axis_settings_(0).home_switch_enabled_ Or telescope_driver_settings_.axis_settings_(1).home_switch_enabled_

            TL.LogMessage("CanFindHome", "Get - " & return_value.ToString())
            Return return_value
        End Get
    End Property

    Public Function CanMoveAxis(Axis As TelescopeAxes) As Boolean Implements ITelescopeV3.CanMoveAxis
        TL.LogMessage("CanMoveAxis", "Get - " & Axis.ToString())
        Select Case Axis
            Case TelescopeAxes.axisPrimary
                Return True
            Case TelescopeAxes.axisSecondary
                Return True
            Case TelescopeAxes.axisTertiary
                Return True
            Case Else
                Throw New InvalidValueException("CanMoveAxis", Axis.ToString(), "0 to 2")
        End Select
    End Function

    Public ReadOnly Property CanPark() As Boolean Implements ITelescopeV3.CanPark
        Get
            TL.LogMessage("CanPark", "Get - " & telescope_driver_settings_.telescope_settings_.park_enabled_)
            Return telescope_driver_settings_.telescope_settings_.park_enabled_
        End Get
    End Property

    Public ReadOnly Property CanPulseGuide() As Boolean Implements ITelescopeV3.CanPulseGuide
        Get
            TL.LogMessage("CanPulseGuide", "Get - " & False.ToString())
            Return False
        End Get
    End Property

    Public ReadOnly Property CanSetDeclinationRate() As Boolean Implements ITelescopeV3.CanSetDeclinationRate
        Get
            TL.LogMessage("CanSetDeclinationRate", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSetGuideRates() As Boolean Implements ITelescopeV3.CanSetGuideRates
        Get
            TL.LogMessage("CanSetGuideRates", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSetPark() As Boolean Implements ITelescopeV3.CanSetPark
        Get
            'new park position can be set when the telesope can be parked.
            Dim return_value As Boolean = CanPark

            TL.LogMessage("CanSetPark", "Get - " & return_value.ToString())
            Return return_value
        End Get
    End Property

    Public ReadOnly Property CanSetPierSide() As Boolean Implements ITelescopeV3.CanSetPierSide
        Get
            TL.LogMessage("CanSetPierSide", "Get - " & False.ToString())
            Return False
        End Get
    End Property

    Public ReadOnly Property CanSetRightAscensionRate() As Boolean Implements ITelescopeV3.CanSetRightAscensionRate
        Get
            'TODO disable for alt/az mounts?
            TL.LogMessage("CanSetRightAscensionRate", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSetTracking() As Boolean Implements ITelescopeV3.CanSetTracking
        Get
            TL.LogMessage("CanSetTracking", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSlew() As Boolean Implements ITelescopeV3.CanSlew
        Get
            TL.LogMessage("CanSlew", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSlewAltAz() As Boolean Implements ITelescopeV3.CanSlewAltAz
        Get
            TL.LogMessage("CanSlewAltAz", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSlewAltAzAsync() As Boolean Implements ITelescopeV3.CanSlewAltAzAsync
        Get
            TL.LogMessage("CanSlewAltAzAsync", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSlewAsync() As Boolean Implements ITelescopeV3.CanSlewAsync
        Get
            TL.LogMessage("CanSlewAsync", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSync() As Boolean Implements ITelescopeV3.CanSync
        Get
            TL.LogMessage("CanSync", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanSyncAltAz() As Boolean Implements ITelescopeV3.CanSyncAltAz
        Get
            TL.LogMessage("CanSyncAltAz", "Get - " & True.ToString())
            Return True
        End Get
    End Property

    Public ReadOnly Property CanUnpark() As Boolean Implements ITelescopeV3.CanUnpark
        Get
            'unparking is possible when the telescope can be parked.
            TL.LogMessage("CanUnpark", "Get - " & telescope_driver_settings_.telescope_settings_.park_enabled_.ToString)
            Return telescope_driver_settings_.telescope_settings_.park_enabled_
        End Get
    End Property

    Public ReadOnly Property Declination() As Double Implements ITelescopeV3.Declination
        Get
            'when the telescope is not connected, throw an exception.
            CheckConnected("Declination")

            'read and save current telescope alignment.
            ReadTelescopeAlignment()

            Dim dec = transform_.DECApparent

            TL.LogMessage("Declination", "Get - " & utilities.DegreesToDMS(dec))

            Return dec
        End Get
    End Property

    Public Property DeclinationRate() As Double Implements ITelescopeV3.DeclinationRate
        Get
            CheckConnected("DeclinationRate Get")

            'return 0 if CanSetRightAscensionRate is false. 
            If Not CanSetDeclinationRate Then
                Return 0.0
            End If

            Dim response As String

            response = sendCommand("DEC_RATE,GET")
            response = response.Replace("DEC_RATE,", "").Trim

            declination_rate_ = Val(response)

            TL.LogMessage("DeclinationRate", "Get - " & declination_rate_.ToString())
            Return declination_rate_
        End Get

        Set(value As Double)
            TL.LogMessage("DeclinationRate Set", value.ToString())

            CheckConnected("DeclinationRate Set")

            If Not CanSetDeclinationRate Then
                'when CanSetDeclinationRate is False throw an exception
                TL.LogMessage("DeclinationRate Set", "Not implemented")
                Throw New ASCOM.PropertyNotImplementedException("DeclinationRate", True)
            Else
                'assemble the message.
                Dim response As String
                Dim message As String

                message = "DEC_RATE,SET,"
                message &= value.ToString("F7", CultureInfo.InvariantCulture)  'pass rate offset in arcseconds / second.

                response = sendCommand(message)
                response = response.Replace("DEC_RATE,", "").Trim

                If CUInt(response) = 0 Then
                    TL.LogMessage("DeclinationRate Set", "failed")
                    Throw New ASCOM.InvalidOperationException("DeclinationRate")
                End If

                declination_rate_ = value
                TL.LogMessage("DeclinationRate Set", "successful")
            End If
        End Set
    End Property

    Public Function DestinationSideOfPier(RightAscension As Double, Declination As Double) As PierSide Implements ITelescopeV3.DestinationSideOfPier
        TL.LogMessage("DestinationSideOfPier Get", "Not implemented")
        Throw New ASCOM.PropertyNotImplementedException("DestinationSideOfPier", False)
    End Function

    Public Property DoesRefraction() As Boolean Implements ITelescopeV3.DoesRefraction
        Get
            TL.LogMessage("DoesRefraction Get", "Not implemented")
            Throw New ASCOM.PropertyNotImplementedException("DoesRefraction", False)
        End Get
        Set(value As Boolean)
            TL.LogMessage("DoesRefraction Set", "Not implemented")
            Throw New ASCOM.PropertyNotImplementedException("DoesRefraction", True)
        End Set
    End Property

    Public ReadOnly Property EquatorialSystem() As EquatorialCoordinateType Implements ITelescopeV3.EquatorialSystem
        Get
            Dim equatorialSystem__1 As EquatorialCoordinateType = EquatorialCoordinateType.equTopocentric

            TL.LogMessage("DeclinationRate", "Get - " & equatorialSystem__1.ToString())
            Return equatorialSystem__1
        End Get
    End Property

    Public Sub FindHome() Implements ITelescopeV3.FindHome
        'throw an exception when the telescope is not connected.
        CheckConnected("FindHome")

        'throw an exception �f CanFindHome is False.
        If Not CanFindHome Then
            TL.LogMessage("FindHome", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("FindHome - Not implemented")
        End If

        Dim response As String
        Dim return_value As Integer

        response = sendCommand("FIND_HOME,SET")
        response = response.Replace("FIND_HOME,", "").Trim

        return_value = CInt(response)

        TL.LogMessage("FindHome", "started: " & return_value.Equals(1))

        Do
            'wait until homing process is finished.
            System.Threading.Thread.Sleep(100)
        Loop While isHoming()

        'homing is successful when the telescope ends up at its home position
        'and no errors occurred duing the process.
        Dim successful As Boolean = (Not getHomingError()) AndAlso AtHome()

        TL.LogMessage("FindHome", "finished: " & successful.ToString)

        If Not successful Then
            Throw New ASCOM.DriverException("Error: findHome not successful")
        End If
    End Sub

    Public ReadOnly Property FocalLength() As Double Implements ITelescopeV3.FocalLength
        Get
            TL.LogMessage("FocalLength Get", telescope_driver_settings_.telescope_settings_.focal_length_)
            Return telescope_driver_settings_.telescope_settings_.focal_length_
        End Get
    End Property

    Public Property GuideRateDeclination() As Double Implements ITelescopeV3.GuideRateDeclination
        Get
            CheckConnected("GuideRateDeclination Get")

            TL.LogMessage("GuideRateDeclination Get", guide_rate_declination_)

            'TODO read from Arduino?

            Return guide_rate_declination_
        End Get

        Set(value As Double)
            CheckConnected("GuideRateDeclination Set")

            'throw an exception if CanSetGuideRates is False.
            If Not CanSetGuideRates Then
                TL.LogMessage("GuideRateDeclination Set", "not implemented")
                Throw New ASCOM.PropertyNotImplementedException("GuideRateDeclination Set")
            Else
                guide_rate_declination_ = value
                TL.LogMessage("GuideRateDeclination Set", guide_rate_declination_)
            End If
        End Set
    End Property

    Public Property GuideRateRightAscension() As Double Implements ITelescopeV3.GuideRateRightAscension
        Get
            TL.LogMessage("GuideRateRightAscension Get", guide_rate_rightascension_)

            'TODO read from Arduino?

            Return guide_rate_rightascension_
        End Get

        Set(value As Double)
            'throw an exception if CanSetGuideRates is False.
            If Not CanSetGuideRates Then
                TL.LogMessage("GuideRateRightAscension Set", "not implemented")
                Throw New ASCOM.PropertyNotImplementedException("GuideRateRightAscension Set")
            Else
                guide_rate_rightascension_ = value
                TL.LogMessage("GuideRateRightAscension Set", guide_rate_rightascension_)
            End If
        End Set
    End Property

    Public ReadOnly Property IsPulseGuiding() As Boolean Implements ITelescopeV3.IsPulseGuiding
        Get
            'throw an exception if the telescope is not connected.
            CheckConnected("IsPulseGuiding")

            'throw an exception if CanPulseGuide Is False.
            If Not CanPulseGuide Then
                TL.LogMessage("IsPulseGuiding Get", "Not implemented")
                Throw New ASCOM.PropertyNotImplementedException("IsPulseGuiding Get")
            End If

            Dim response As String
            Dim return_value As Boolean

            response = sendCommand("PULSE_GUIDE,GET")
            response = response.Replace("PULSE_GUIDE,", "").Trim

            return_value = Not CInt(response).Equals(0)

            TL.LogMessage("IsPulseGuiding Get", return_value.ToString)

            Return return_value
        End Get
    End Property

    Public Sub MoveAxis(Axis As TelescopeAxes, Rate As Double) Implements ITelescopeV3.MoveAxis
        CheckConnected("MoveAxis")

        If Not CanMoveAxis(Axis) Then
            'todo: catch exception generated by CanMoveAxis(Axis).
            TL.LogMessage("MoveAxis", "Not implemented for Axis " & Axis.ToString())
            Throw New ASCOM.InvalidValueException("MoveAxis")
        End If

        'throw an exception if an invalid axis rate is given.
        Dim axis_rate As IAxisRates = AxisRates(Axis)

        If Math.Abs(Rate) < axis_rate.Item(1).Minimum Or
            Math.Abs(Rate) > axis_rate.Item(1).Maximum Then
            TL.LogMessage("MoveAxis", "Invalid Rate given for Axis: " & Axis.ToString() & " Rate: " & Rate &
                ". Valid rates: " & axis_rate.Item(1).Minimum & " - " & axis_rate.Item(1).Maximum)
            Throw New ASCOM.InvalidValueException("MoveAxis", Rate, axis_rate.Item(1).Minimum & " - " & axis_rate.Item(1).Maximum)
        End If

        Dim message As String
        Dim response As String
        Dim return_value As Integer

        'assemble the message. 
        message = "MOVEAXIS,SET,"

        If Axis = TelescopeAxes.axisPrimary Then
            message &= "X_AXIS,"
        ElseIf Axis = TelescopeAxes.axisSecondary Then
            message &= "Y_AXIS,"
        ElseIf Axis = TelescopeAxes.axisTertiary Then
            message &= "Z_AXIS,"
        End If

        'rate in degrees / second, 7 decimal places.
        message &= Rate.ToString("F7", CultureInfo.InvariantCulture)

        response = sendCommand(message)
        response = response.Replace("MOVEAXIS,", "").Trim

        return_value = CInt(response)

        logErrorCode("MoveAxis", return_value, GetType(telescope_error_t))

        If return_value.Equals(0) Then
            TL.LogMessage("MoveAxis", Axis.ToString & " " & Rate.ToString & " " & return_value.Equals(0).ToString)

        ElseIf (return_value And telescope_error_t.ERROR_PARKED <> 0) Then
            'when the telescope is parked throw an exception.
            TL.LogMessage("MoveAxis", "Error " & return_value & ": Telescope Parked")
            Throw New ASCOM.ParkedException()

        ElseIf (return_value And telescope_error_t.ERROR_HOMING <> 0) Then
            'when an invalid axis was given throw an exception.
            TL.LogMessage("MoveAxis", "Error " & return_value & ": telescope is currently homing.")
            Throw New ASCOM.InvalidOperationException("MoveAxis")

        ElseIf (return_value And telescope_error_t.ERROR_INVALID_AXIS <> 0) Then
            'when an invalid axis was given throw an exception.
            TL.LogMessage("MoveAxis", "Error " & return_value & ": invalid axis")
            Throw New ASCOM.InvalidValueException("MoveAxis")
        End If
    End Sub

    Public Sub Park() Implements ITelescopeV3.Park
        'throw an exception if the telescope is not connected.
        CheckConnected("Park")

        'throw an exception if the telescope cannot be parked.
        If Not CanPark Then
            TL.LogMessage("Park", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("Park")
        End If

        Dim response As String

        response = sendCommand("PARKED,SET,1")
        response = response.Replace("PARKED,", "").Trim

        Dim return_value = CUInt(response)

        logErrorCode("Park", return_value, GetType(telescope_error_t))

        'check respone from arduino. specific bits are set when an error occured.
        If return_value.Equals(0) Then
            'telescope successfully parked
            TL.LogMessage("Park", "successfully parked")
        ElseIf (return_value And telescope_error_t.ERROR_OUT_OF_BOUNDS_X) <> 0 Then
            'telescope park position beyond x axis movement limits.
            TL.LogMessage("Park", "error: park position beyond x axis movement limits")
            Throw New ASCOM.InvalidOperationException("parking failed: park position beyond x axis movement limits")
        ElseIf (return_value And telescope_error_t.ERROR_OUT_OF_BOUNDS_Y) <> 0 Then
            'telescope park position beyond y axis movement limits.
            TL.LogMessage("Park", "error: park position beyond y axis movement limits")
            Throw New ASCOM.InvalidOperationException("parking failed: park position beyond y axis movement limits")
        ElseIf (return_value And telescope_error_t.ERROR_PARKED) <> 0 Then
            'telescope already parked.
            TL.LogMessage("Park", "info: telescope already parked")
        ElseIf (return_value And telescope_error_t.ERROR_HOMING) <> 0 Then
            'manual control active, raise an error.
            TL.LogMessage("Park", "error: telescope is currently homing")
            Throw New ASCOM.InvalidOperationException("parking failed: telescope is currently homing")
        End If

    End Sub

    Public Sub PulseGuide(Direction As GuideDirections, Duration As Integer) Implements ITelescopeV3.PulseGuide
        'throw an exception if the telescope is not connected.
        CheckConnected("PulseGuide")

        'throw an exception if the telescope cannot be pulse guided.
        If Not CanPulseGuide Then
            TL.LogMessage("PulseGuide", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("PulseGuide")
        End If

        Dim pg_rate As Double

        'choose the correct pulse guide rate. 
        If Direction = GuideDirections.guideNorth Or GuideDirections.guideSouth Then
            pg_rate = GuideRateDeclination
        ElseIf Direction = GuideDirections.guideEast Or GuideDirections.guideWest Then
            pg_rate = GuideRateRightAscension
        End If


        'assemble the message
        Dim response As String
        Dim message As String = "PULSE_GUIDE,"

        message &= Direction            'direction of the movement.
        message &= ","
        message &= Duration             'duration of the movement (in ms).
        message &= ","
        message &= pg_rate.ToString("F7", CultureInfo.InvariantCulture)     'pulse guide rate.

        response = sendCommand(message)
        response.Replace("PULSE_GUIDE,", "").Trim()

        Dim return_value = CUInt(response)

        logErrorCode("PulseGuide", return_value, GetType(telescope_error_t))

        If return_value.Equals(0) Then
            TL.LogMessage("PulseGuide", response)

        ElseIf (return_value And telescope_error_t.ERROR_PARKED) <> 0 Then
            'when the telescope is parked, throw an exception.
            TL.LogMessage("PulseGuide", "Error " & return_value & ": Telescope Parked")
            Throw New ASCOM.ParkedException()

        ElseIf (return_value And telescope_error_t.ERROR_HOMING) <> 0 Then
            'when an invalid axis has been given, throw an exception.
            TL.LogMessage("PulseGuide", "Error " & return_value & ": Telescope is currently homing")
            Throw New ASCOM.InvalidValueException("Telescope is currently homing")

        ElseIf (return_value And telescope_error_t.ERROR_INVALID_AXIS) <> 0 Then
            'when an invalid axis has been given, throw an exception.
            TL.LogMessage("PulseGuide", "Error " & return_value & ": Invalid Axis")
            Throw New ASCOM.InvalidValueException("Invalid Axis")
        End If
    End Sub

    Public ReadOnly Property RightAscension() As Double Implements ITelescopeV3.RightAscension
        Get
            'when the telescope is not connected, throw an exception.
            CheckConnected("RightAscension")

            'read and save current telescope alignment.
            ReadTelescopeAlignment()

            Dim ra = transform_.RAApparent

            TL.LogMessage("RightAscension", "Get - " & utilities.HoursToHMS(ra))

            Return ra
        End Get
    End Property

    Public Property RightAscensionRate() As Double Implements ITelescopeV3.RightAscensionRate
        Get
            'throw an exception if the telescope is not connected.
            CheckConnected("RightAscensionRate")

            'return 0 if CanSetRightAscensionRate is false. 
            If Not CanSetRightAscensionRate Then
                Return 0.0
            End If

            'Dim message As String
            Dim response As String

            response = sendCommand("RA_RATE,GET")
            response = response.Replace("RA_RATE,", "").Trim

            right_ascension_rate_ = Val(response)

            TL.LogMessage("RightAscensionRate", "Get - " & right_ascension_rate_.ToString())
            Return right_ascension_rate_
        End Get

        Set(value As Double)
            TL.LogMessage("RightAscensionRate Set", value.ToString())

            CheckConnected("RightAscensionRate")

            If Not CanSetRightAscensionRate Then
                'when CanRightAscensionRate is False throw an exception
                TL.LogMessage("RightAscensionRate Set", "Not implemented")
                Throw New ASCOM.PropertyNotImplementedException("RightAscensionRate", True)
            Else
                'assemble the message.
                Dim response As String
                Dim message As String

                message = "RA_RATE,SET,"
                message &= value.ToString("F7", CultureInfo.InvariantCulture)  'pass rate offset in seconds of right ascension per sidereal second.

                response = sendCommand(message)
                response = response.Replace("RA_RATE,", "").Trim

                If CUInt(response) = 0 Then
                    TL.LogMessage("RightAscensionRate Set", "failed")
                    Throw New ASCOM.InvalidOperationException("RightAscensionRate")
                End If

                right_ascension_rate_ = value
                TL.LogMessage("RightAscensionRate Set", "successful")
            End If
        End Set
    End Property

    Public Sub SetPark() Implements ITelescopeV3.SetPark
        'throw an exception if the telescope is not connected.
        CheckConnected("SetPark")

        'throw an exception if the telescope cannot be parked.
        If Not CanPark Then
            TL.LogMessage("SetPark", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SetPark")
        End If

        Dim response As String

        response = sendCommand("PARK_POS,SET")
        response = response.Replace("PARK_POS,", "").Trim

        TL.LogMessage("SetPark", CInt(response).Equals(0))
    End Sub

    Public Property SideOfPier() As PierSide Implements ITelescopeV3.SideOfPier
        Get
            TL.LogMessage("SideOfPier Get", "Not implemented")
            Throw New ASCOM.PropertyNotImplementedException("SideOfPier", False)
        End Get
        Set(value As PierSide)
            TL.LogMessage("SideOfPier Set", "Not implemented")
            Throw New ASCOM.PropertyNotImplementedException("SideOfPier", True)
        End Set
    End Property

    Public ReadOnly Property SiderealTime() As Double Implements ITelescopeV3.SiderealTime
        Get
            Dim sidereal_time As Double = 0.0
            Dim greenwich_sidereal_time As Double = 0.0

            'compute greenwich julian date
            Dim date_julian = utilities.DateUTCToJulian(DateTime.UtcNow)
            Dim julian_high = Int(date_julian)          'integer part of julian date
            Dim julian_low = date_julian - julian_high  'fractional part of julian date

            'compute apparent greenwich sidereal time
            Dim error_code As Short = novas31_.SiderealTime(
                julian_high,
                julian_low,
                0,
                1,
                0,
                0,
                greenwich_sidereal_time)

            'Longitude based sidereal time difference (in hours)
            Dim adj_hours = SiteLongitude / 360.0 * 24.0

            'calculate local apparent sidereal time
            sidereal_time = (greenwich_sidereal_time + adj_hours) Mod 24.0

            TL.LogMessage("SiderealTime", "Get - " & sidereal_time.ToString() & " - Error Code: " & error_code)

            Return sidereal_time
        End Get
    End Property

    Public Property SiteElevation() As Double Implements ITelescopeV3.SiteElevation
        Get
            TL.LogMessage("SiteElevation Get", telescope_driver_settings_.telescope_settings_.site_elevation_.ToString)
            Return telescope_driver_settings_.telescope_settings_.site_elevation_
        End Get

        Set(value As Double)
            If value < -300.0 Or value > 10000 Then
                TL.LogMessage("SiteElevation Set", "invalid value " & value.ToString() & " is not within: -300.0m - 10000.0m")
                Throw New ASCOM.InvalidValueException("SiteElevation Set", value.ToString(), "-300.0 - 10000.0")
            End If

            telescope_driver_settings_.telescope_settings_.site_elevation_ = value
            transform_.SiteElevation = value
            TL.LogMessage("SiteElevation Set", value.ToString())
        End Set
    End Property

    Public Property SiteLatitude() As Double Implements ITelescopeV3.SiteLatitude
        Get
            TL.LogMessage("SiteLatitude Get", telescope_driver_settings_.telescope_settings_.site_latitude_.ToString)
            Return telescope_driver_settings_.telescope_settings_.site_latitude_
        End Get

        Set(value As Double)
            If value < -90.0 Or value > 90.0 Then
                TL.LogMessage("SiteLatitude Set", "invalid value " & value.ToString() & " is not within: -90.0� - 90.0�")
                Throw New ASCOM.InvalidValueException("SiteLatitude Set", value.ToString(), "-90.0 - 90.0")
            End If

            telescope_driver_settings_.telescope_settings_.site_latitude_ = value
            transform_.SiteLatitude = value
            TL.LogMessage("SiteLatitude Set", value.ToString())
        End Set
    End Property

    Public Property SiteLongitude() As Double Implements ITelescopeV3.SiteLongitude
        Get
            TL.LogMessage("SiteLongitude Get", telescope_driver_settings_.telescope_settings_.site_longitude_.ToString)
            Return telescope_driver_settings_.telescope_settings_.site_longitude_
        End Get

        Set(value As Double)
            If value < -180.0 Or value > 180.0 Then
                TL.LogMessage("SiteLatitude Set", "invalid value " & value.ToString() & " is not within: -180.0� - 180.0�")
                Throw New ASCOM.InvalidValueException("SiteLongitude Set", value.ToString(), "-180.0 - 180.0")
            End If

            telescope_driver_settings_.telescope_settings_.site_longitude_ = value
            transform_.SiteLongitude = value
            TL.LogMessage("SiteLongitude Set", value.ToString())
        End Set
    End Property

    Public Property SlewSettleTime() As Short Implements ITelescopeV3.SlewSettleTime
        Get
            TL.LogMessage("SlewSettleTime Get", "Not implemented")
            Throw New ASCOM.PropertyNotImplementedException("SlewSettleTime", False)
        End Get
        Set(value As Short)
            TL.LogMessage("SlewSettleTime Set", "Not implemented")
            Throw New ASCOM.PropertyNotImplementedException("SlewSettleTime", True)
        End Set
    End Property

    Public Sub SlewToAltAz(Azimuth As Double, Altitude As Double) Implements ITelescopeV3.SlewToAltAz
        'throw an exception if the telescope is not connected.
        CheckConnected("SlewToAltAz")

        'throw an exception if Canslew is False.
        If Not CanSlew Then
            TL.LogMessage("SlewToAltAz", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SlewToAltAz")
        End If

        'throw an exception if the telescope is currently tracking.
        If Tracking Then
            TL.LogMessage("SlewToAltAz", "error: Telescope tracking")
            Throw New ASCOM.InvalidOperationException("SlewToAltAz")
        End If

        If Azimuth < 0.0 Or Azimuth > 360.0 Then
            TL.LogMessage("SlewToAltAz", "error: invalid Azimuth coordinate: " & Azimuth & ". Valid range: 0.0 - 360.0")
            Throw New ASCOM.InvalidValueException("SlewToAltAz: invalid Azimuth coordinate: ", Azimuth, "0.0 - 360.0")
        End If

        If Altitude < 0.0 Or Altitude > 90.0 Then
            TL.LogMessage("SlewToAltAz", "error: invalid Altitude coordinate: " & Altitude & ". Valid range: 0.0 - 90.0")
            Throw New ASCOM.InvalidValueException("SlewToAltAz: invalid Altitude coordinate.", Altitude, "0.0 - 90.0")
        End If

        TL.LogMessage("SlewToAltAzAsync", "slewing to Alt: " &
                      utilities.DegreesToDMS(Azimuth) & " Az: " &
                      utilities.DegreesToDMS(Altitude))

        transform_.SetAzimuthElevation(Azimuth, Altitude)

        'send command to arduino.
        Slew("SlewToAltAz")

        'wait for telescope to finish movement by polling until the movement has stopped.
        Do
            System.Threading.Thread.Sleep(100)      'wait.
        Loop Until Slewing() = False

        'transform coordinates and send command to arduino.
        TL.LogMessage("SlewToAltAz", "finished")
    End Sub

    Public Sub SlewToAltAzAsync(Azimuth As Double, Altitude As Double) Implements ITelescopeV3.SlewToAltAzAsync
        'throw an exception if the telescope is not connected.
        CheckConnected("SlewToAltAzAsync")

        'throw an exception if Canslew is False.
        If Not CanSlew Then
            TL.LogMessage("SlewToAltAzAsync", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SlewToAltAzAsync")
        End If

        'throw an exception if the telescope is currently tracking.
        If Tracking Then
            TL.LogMessage("SlewToAltAzAsync", "error: Telescope tracking")
            Throw New ASCOM.InvalidOperationException("SlewToAltAzAsync")
        End If

        If Azimuth < 0.0 Or Azimuth > 360.0 Then
            TL.LogMessage("SlewToAltAz", "error: invalid Azimuth coordinate: " & Azimuth & ". Valid range: 0.0 - 360.0")
            Throw New ASCOM.InvalidValueException("SlewToAltAz: invalid Azimuth coordinate.", Azimuth, "0.0 - 360.0")
        End If

        If Altitude < 0.0 Or Altitude > 90.0 Then
            TL.LogMessage("SlewToAltAz", "error: invalid Altitude coordinate: " & Altitude & ". Valid range: 0.0 - 90.0")
            Throw New ASCOM.InvalidValueException("SlewToAltAz: invalid Altitude coordinate.", Altitude, "0.0 - 90.0")
        End If

        TL.LogMessage("SlewToAltAzAsync", "slewing to Az: " &
                      utilities.DegreesToDMS(Azimuth) & " Alt: " &
                      utilities.DegreesToDMS(Altitude))

        transform_.SetAzimuthElevation(Azimuth, Altitude)

        'transform coordinates and send command to arduino.
        Slew("SlewToAltAzAsync")
    End Sub

    Public Sub SlewToCoordinates(RightAscension As Double, Declination As Double) Implements ITelescopeV3.SlewToCoordinates
        'throw an exception if the telescope is not connected.
        CheckConnected("SlewToCoordinates")

        'throw an exception if Canslew is False.
        If Not CanSlew Then
            TL.LogMessage("SlewToCoordinates", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SlewToCoordinates")
        End If

        'throw an exception if the telescope is currently tracking.
        If Not Tracking Then
            TL.LogMessage("SlewToCoordinates", "error: Telescope not tracking")
            Throw New ASCOM.InvalidOperationException("SlewToCoordinates")
        End If

        TL.LogMessage("SlewToCoordinates", "slewing to Ra: " &
                      utilities.HoursToHMS(RightAscension) & " Dec: " &
                      utilities.DegreesToDMS(Declination))

        'set target
        TargetRightAscension = RightAscension
        TargetDeclination = Declination

        'perform slew
        SlewToTarget()

        'transform coordinates and send command to arduino.
        TL.LogMessage("SlewToCoordinates", "finished")
    End Sub

    Public Sub SlewToCoordinatesAsync(RightAscension As Double, Declination As Double) Implements ITelescopeV3.SlewToCoordinatesAsync
        'throw an exception if the telescope is not connected.
        CheckConnected("SlewToCoordinatesAsync")

        'throw an exception if Canslew is False.
        If Not CanSlew Then
            TL.LogMessage("SlewToCoordinatesAsync", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SlewToCoordinatesAsync")
        End If

        'throw an exception if the telescope is currently tracking.
        If Not Tracking Then
            TL.LogMessage("SlewToCoordinatesAsync", "error: Telescope not tracking")
            Throw New ASCOM.InvalidOperationException("SlewToCoordinatesAsync")
        End If

        TL.LogMessage("SlewToCoordinatesAsync", "slewing to Ra: " &
                      utilities.HoursToHMS(RightAscension) & " Dec: " &
                      utilities.DegreesToDMS(Declination))

        'set target
        TargetRightAscension = RightAscension
        TargetDeclination = Declination

        'perform slew
        SlewToTargetAsync()

        'transform coordinates and send command to arduino.
        TL.LogMessage("SlewToCoordinatesAsync", "finished")
    End Sub

    Public Sub SlewToTarget() Implements ITelescopeV3.SlewToTarget
        'throw an exception if the telescope is not connected.
        CheckConnected("SlewToTarget")

        'throw an exception if CanSlew is False.
        If Not CanSlew Then
            TL.LogMessage("SlewToTarget", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SlewToTarget")
        End If

        'throw an exception if the telescope is currently tracking.
        If Not Tracking Then
            TL.LogMessage("SlewToTarget", "error: Telescope not tracking")
            Throw New ASCOM.InvalidOperationException("SlewToTarget")
        End If

        TL.LogMessage("SlewToTarget", "slewing to Ra: " &
                      utilities.HoursToHMS(TargetRightAscension) & " Dec: " &
                      utilities.DegreesToDMS(TargetDeclination))

        'set the target to be transformed.
        transform_.SetApparent(TargetRightAscension, TargetDeclination)

        'send command to arduino.
        Slew("SlewToTarget")

        'wait for telescope to finish movement by polling until the movement has stopped.
        Do
            System.Threading.Thread.Sleep(100)      'wait.
        Loop Until Slewing() = False

        'transform coordinates and send command to arduino.
        TL.LogMessage("SlewToTarget", "finished")
    End Sub

    Public Sub SlewToTargetAsync() Implements ITelescopeV3.SlewToTargetAsync
        'throw an exception if the telescope is not connected.
        CheckConnected("SlewToTargetAsync")

        'throw an exception if CanSlew is False.
        If Not CanSlew Then
            TL.LogMessage("SlewToTargetAsync", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SlewToTargetAsync")
        End If

        'throw an exception if the telescope is currently tracking.
        If Not Tracking Then
            TL.LogMessage("SlewToTargetAsync", "error: Telescope not tracking")
            Throw New ASCOM.InvalidOperationException("SlewToTargetAsync")
        End If

        TL.LogMessage("SlewToTargetAsync", "slewing to Ra: " &
                      utilities.HoursToHMS(TargetRightAscension) & " Dec: " &
                      utilities.DegreesToDMS(TargetDeclination))

        'set the target to be transformed.
        transform_.SetApparent(TargetRightAscension, TargetDeclination)

        'send command to arduino.
        Slew("SlewToTargetAsync")

        'transform coordinates and send command to arduino.
        TL.LogMessage("SlewToTargetAsync", "finished")
    End Sub

    Public ReadOnly Property Slewing() As Boolean Implements ITelescopeV3.Slewing
        Get
            'throw an exception if the telescope is not connected.
            CheckConnected("Slewing")

            'throw an exception if Canslew is False.
            If Not CanSlew Then
                TL.LogMessage("Slewing Get", "Not implemented")
                Throw New ASCOM.PropertyNotImplementedException("Slewing Get")
            End If

            Dim response As String
            Dim return_value As Boolean
            Dim return_value_slewing As Boolean
            Dim return_value_moveaxis As Boolean

            'check if the telescope is slewing.
            response = sendCommand("SLEW,GET")
            response = response.Replace("SLEW,", "").Trim

            return_value_slewing = Not CInt(response).Equals(0)

            'check if a moveaxis command is active.
            response = sendCommand("MOVEAXIS,GET")
            response = response.Replace("MOVEAXIS,", "").Trim

            return_value_moveaxis = Not CInt(response).Equals(0)

            'slewing is true when a slew or moveaxis command is active.
            return_value = return_value_slewing Or return_value_moveaxis

            TL.LogMessage("Slewing Get", return_value.ToString)

            Return return_value
        End Get
    End Property

    Public Sub SyncToAltAz(Azimuth As Double, Altitude As Double) Implements ITelescopeV3.SyncToAltAz
        'throw an exception if the telescope is not connected.
        CheckConnected("SyncToAltAz")

        'throw an exception if CanSyncAltAz is False.
        If Not CanSyncAltAz Then
            TL.LogMessage("SyncToAltAz", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SyncToAltAz")
        End If

        If Azimuth < 0.0 Or Azimuth > 360.0 Then
            TL.LogMessage("SlewToAltAz", "error: invalid Azimuth coordinate: " & Azimuth & " Valid range: 0.0 - 360.0")
            Throw New ASCOM.InvalidValueException("SlewToAltAz: invalid Azimuth coordinate.", Azimuth, "0.0 - 360.0")
        End If

        If Altitude < 0.0 Or Altitude > 90.0 Then
            TL.LogMessage("SlewToAltAz", "error: invalid Altitude coordinate: " & Altitude & " Valid range: 0.0 - 90.0")
            Throw New ASCOM.InvalidValueException("SlewToAltAz: invalid Altitude coordinate.", Altitude, "0.0 - 90.0")
        End If

        TL.LogMessage("SyncToAltAz", "syncing to Alt: " &
                      utilities.DegreesToDMS(Altitude) & " Az: " &
                      utilities.DegreesToDMS(Azimuth))

        'set the coordinates to be transformed.
        transform_.SetAzimuthElevation(Azimuth, Altitude)

        'transform coordinates and send command to arduino.
        Sync("SyncToAltAz")
    End Sub

    Public Sub SyncToCoordinates(RightAscension As Double, Declination As Double) Implements ITelescopeV3.SyncToCoordinates
        'throw an exception if the telescope is not connected.
        CheckConnected("SyncToCoordinates")

        'throw an exception if CanSync is False.
        If Not CanSync Then
            TL.LogMessage("SyncToCoordinates", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SyncToCoordinates")
        End If

        TL.LogMessage("SyncToCoordinates", "syncing to Ra: " &
                      utilities.HoursToHMS(RightAscension) & " Dec: " &
                      utilities.DegreesToDMS(Declination))

        'set target
        TargetRightAscension = RightAscension
        TargetDeclination = Declination

        'perform sync
        SyncToTarget()

        TL.LogMessage("SyncToCoordinates", "finished")
    End Sub

    Public Sub SyncToTarget() Implements ITelescopeV3.SyncToTarget
        'throw an exception if the telescope is not connected.
        CheckConnected("SyncToTarget")

        'throw an exception if CanSync is False.
        If Not CanSync Then
            TL.LogMessage("SyncToTarget", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("SyncToTarget")
        End If

        TL.LogMessage("SyncToTarget", "syncing to Ra: " &
                      utilities.HoursToHMS(TargetRightAscension) & " Dec: " &
                      utilities.DegreesToDMS(TargetDeclination))

        'set the coordinates to be transformed.
        transform_.SetApparent(TargetRightAscension, TargetDeclination)

        'transform coordinates and send command to arduino.
        Sync("SyncToTarget")

        TL.LogMessage("SyncToTarget", "finished")
    End Sub

    Public Property TargetDeclination() As Double Implements ITelescopeV3.TargetDeclination
        Get
            If Double.IsNaN(target_declination_) Then
                TL.LogMessage("TargetDeclination Get", "Not set")
                Throw New ASCOM.ValueNotSetException("TargetDeclination Get")
            End If

            TL.LogMessage("TargetDeclination Get", target_declination_.ToString())
            Return target_declination_
        End Get

        Set(value As Double)
            If value < -90.0 Or value > 90.0 Then
                TL.LogMessage("TargetDeclination Set", "invalid value: " & value.ToString() & " Valid range: -90.0 - 90.0")
                Throw New ASCOM.InvalidValueException("TargetDeclination Set - invalid value", value.ToString(), "-90.0 - 90.0")
            End If

            target_declination_ = value
            TL.LogMessage("TargetDeclination Set", value.ToString())
        End Set
    End Property

    Public Property TargetRightAscension() As Double Implements ITelescopeV3.TargetRightAscension
        Get
            If Double.IsNaN(target_right_ascension_) Then
                TL.LogMessage("TargetRightAscension Get", "Not set")
                Throw New ASCOM.ValueNotSetException("TargetRightAscension Get")
            End If

            TL.LogMessage("TargetRightAscension Get", target_right_ascension_.ToString())
            Return target_right_ascension_
        End Get

        Set(value As Double)
            If value < 0.0 Or value > 24.0 Then
                TL.LogMessage("TargetRightAscension Set", "invalid value: " & value.ToString() & " Valid range: 0.0 - 24.0")
                Throw New ASCOM.InvalidValueException("TargetRightAscension Set - invalid value", value.ToString(), "0.0 - 24.0")
            End If

            target_right_ascension_ = value
            TL.LogMessage("TargetRightAscension Set", value.ToString())
        End Set
    End Property

    Public Property Tracking() As Boolean Implements ITelescopeV3.Tracking
        Get
            'throw an exception if the telescope is not connected.
            CheckConnected("Tracking")

            'return false when CanSetTracking is false.
            If Not CanSetTracking Then
                TL.LogMessage("Tracking", "Not available")
                Return False
            End If

            Dim response As String
            Dim return_value As Boolean

            response = sendCommand("TRACKING,GET")
            response = response.Replace("TRACKING,", "").Trim

            return_value = Not CInt(response).Equals(0)

            TL.LogMessage("Tracking", "Get - " & return_value.ToString())

            Return return_value
        End Get

        Set(value As Boolean)
            'throw an exception if the telescope is not connected.
            CheckConnected("Tracking")

            'return false when CanSetTracking is false.
            If Not CanSetTracking Then
                TL.LogMessage("Tracking", "Not implemented")
                Throw New ASCOM.PropertyNotImplementedException("Tracking")
            End If

            'assemble the message.
            'Dim message As String
            Dim response As String
            Dim return_value As Integer

            response = sendCommand("TRACKING,SET," & Convert.ToInt32(value))
            response = response.Replace("TRACKING,", "").Trim

            return_value = CInt(response)

            logErrorCode("Tracking", return_value, GetType(telescope_error_t))

            If return_value.Equals(0) Then
                TL.LogMessage("Tracking", "set to " & value.ToString() & ": " & return_value.ToString())

            ElseIf (return_value And telescope_error_t.ERROR_HOMING) <> 0 Then
                'when an invalid axis has been given, throw an exception.
                TL.LogMessage("Tracking", "Error: Telescope is currently homing")
                Throw New ASCOM.InvalidValueException("Error: Telescope is currently homing")

            ElseIf (return_value And telescope_error_t.ERROR_PARKED) <> 0 Then
                'when the telescope is parked, throw an exception.
                TL.LogMessage("Tracking", "Error: Telescope Parked")
                Throw New ASCOM.ParkedException("Error: Telescope Parked")
            End If
        End Set
    End Property

    Public Property TrackingRate() As DriveRates Implements ITelescopeV3.TrackingRate
        Get
            CheckConnected("TrackingRate")
            Dim message As String
            Dim response As String

            'send tracking rate query to arduino.
            message = "TRACKING_RATE,GET"

            response = sendCommand(message)
            response = response.Replace("TRACKING_RATE,", "").Trim

            'set drive rate to rate returned by the Arduino.
            tracking_rate_ = CType(CInt(response), DriveRates)

            TL.LogMessage("TrackingRate Get", tracking_rate_.ToString)

            Return tracking_rate_
        End Get

        Set(value As DriveRates)
            CheckConnected("TrackingRate")
            Dim message As String
            Dim response As String

            tracking_rate_ = value

            'send new tracking rate to arduino.
            message = "TRACKING_RATE,SET,"

            message &= Convert.ToInt32(tracking_rate_)

            response = sendCommand(message)
            response = response.Replace("TRACKING_RATE,", "").Trim

            Select Case CInt(response)
                Case 0
                    TL.LogMessage("TrackingRate Set", value.ToString & ": success")
                Case Else
                    TL.LogMessage("TrackingRate Set", value.ToString & ": error " & CInt(response))
                    Throw New ASCOM.DriverException("TrackingRate Set", value.ToString & ": error " & CInt(response))
            End Select
        End Set
    End Property

    Public ReadOnly Property TrackingRates() As ITrackingRates Implements ITelescopeV3.TrackingRates
        Get
            Dim tracking_rates As ITrackingRates = New TrackingRates

            For Each driveRate As DriveRates In tracking_rates
                TL.LogMessage("TrackingRates", "Get - " & driveRate.ToString())
            Next

            Return tracking_rates
        End Get
    End Property

    Public Property UTCDate() As DateTime Implements ITelescopeV3.UTCDate
        Get
            Dim utcDate__1 As DateTime = DateTime.UtcNow
            TL.LogMessage("DateTime", "Get - " & [String].Format("MM/dd/yy HH:mm:ss", utcDate__1))
            Return utcDate__1
        End Get
        Set(value As DateTime)
            Throw New ASCOM.PropertyNotImplementedException("UTCDate", True)
        End Set
    End Property

    Public Sub Unpark() Implements ITelescopeV3.Unpark
        'throw an exception if the telescope is not connected.
        CheckConnected("Unpark")

        'throw an exception if the telescope cannot be parked.
        If Not CanPark Then
            TL.LogMessage("Unpark", "Not implemented")
            Throw New ASCOM.MethodNotImplementedException("Unpark")
        End If

        Dim response As String

        response = sendCommand("PARKED,SET,0")
        response = response.Replace("PARKED,", "").Trim

        Dim return_value = CUInt(response)

        logErrorCode("Unpark", return_value, GetType(telescope_error_t))

        'check respone from arduino. specific bits are set when an error occured.
        If return_value.Equals(0) Then
            'telescope successfully unparked.
            TL.LogMessage("Unpark", "successfully unparked")
        ElseIf (return_value And telescope_error_t.ERROR_PARKED) <> 0 Then
            'telescope was not parked.
            TL.LogMessage("Unpark", "info: telescope not parked")
        ElseIf (return_value And telescope_error_t.ERROR_HOMING) <> 0 Then
            'manual control active, raise an error.
            TL.LogMessage("Unpark", "error: telescope is currently homing")
            Throw New ASCOM.InvalidOperationException("unparking failed: manual control active")
        ElseIf (return_value And telescope_error_t.ERROR_OTHER) <> 0 Then
            'parking not enabed for this telescope, raise an error.
            TL.LogMessage("Unpark", "error: parking not enabled")
            Throw New ASCOM.InvalidOperationException("unparking failed: parking not enabled")
        End If

    End Sub

#End Region

#Region "Private properties and methods"
    ' here are some useful properties and methods that can be used as required
    ' to help with

#Region "ASCOM Registration"

    Private Shared Sub RegUnregASCOM(ByVal bRegister As Boolean)

        Using P As New Profile() With {.DeviceType = "Telescope"}
            If bRegister Then
                P.Register(driverID, driverDescription)
            Else
                P.Unregister(driverID)
            End If
        End Using

    End Sub

    <ComRegisterFunction()>
    Public Shared Sub RegisterASCOM(ByVal T As Type)

        RegUnregASCOM(True)

    End Sub

    <ComUnregisterFunction()>
    Public Shared Sub UnregisterASCOM(ByVal T As Type)

        RegUnregASCOM(False)

    End Sub

#End Region

    ''' <summary>
    ''' Returns true if there is a valid connection to the driver hardware
    ''' </summary>
    Private Property IsConnected As Boolean
        Get
            Dim connected As Boolean = False

            'check that the driver hardware connection exists and is connected to the hardware
            Select Case telescope_driver_settings_.connection_type_
                Case connection_type_t.SERIAL
                    If Not serial_connection_ Is Nothing Then
                        Return serial_connection_.Connected
                    Else
                        Return False
                    End If
                Case connection_type_t.NETWORK
                    If Not tcp_connection_ Is Nothing Then
                        Return tcp_connection_.Connected
                    Else
                        Return False
                    End If
                Case Else
                    Throw New ASCOM.DriverException("unknown connection state " & telescope_driver_settings_.connection_type_.ToString)
            End Select

            TL.LogMessage("IsConnected Get", connected.ToString() & If(connected, " (connection type: " & telescope_driver_settings_.connection_type_.ToString & ")", ""))

            Return connected
        End Get
        Set(value As Boolean)
            TL.LogMessage("IsConnected Set", value)

            If value = IsConnected Then
                TL.LogMessage("IsConnected Set", "already " & If(value, "connected", "disconnected"))
                Return
            End If

            If value Then
                Select Case telescope_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        TL.LogMessage("IsConnected Set", "Connecting to port " & telescope_driver_settings_.com_port_ & "@" & telescope_driver_settings_.baud_rate_)

                        'connect to the device:
                        serial_connection_.PortName = telescope_driver_settings_.com_port_   'use port number read earlier
                        serial_connection_.Speed = telescope_driver_settings_.baud_rate_     'use baud rate read earlier

                        'open serial connection and catch exception if the com port is not available.
                        Try
                            serial_connection_.Connected = True 'enable serial communication.
                            serial_connection_.ClearBuffers()
                        Catch ex As ASCOM.Utilities.Exceptions.InvalidValueException
                            Dim err_msg As String = "Error: COM port does not exist."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception       'TODO catch specific exception types and handle them 
                            Dim err_msg As String = "error: could not open serial connection."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        serial_connection_.ReceiveTimeoutMs = TIMEOUT_SERIAL
                    Case connection_type_t.NETWORK
                        TL.LogMessage("IsConnected Set", "Connecting to " + telescope_driver_settings_.ip_address_ + ": " & telescope_driver_settings_.port_)

                        Try
                            tcp_connection_ = New Net.Sockets.TcpClient

                            tcp_connection_.Connect(telescope_driver_settings_.ip_address_, telescope_driver_settings_.port_)
                        Catch ex As ArgumentNullException
                            Dim err_msg As String = "error: the host name is null."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ArgumentOutOfRangeException
                            Dim err_msg As String = "error: the port number is outside the valid range of " & System.Net.IPEndPoint.MinPort & " to " & System.Net.IPEndPoint.MaxPort & "."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As System.Net.Sockets.SocketException
                            Dim err_msg As String = "error: an error occured when accessing the socket."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ObjectDisposedException
                            Dim err_msg As String = "error: the TCP client is closed."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception
                            Dim err_msg As String = "error: could not open TCP connection."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        tcp_connection_.SendTimeout = TIMEOUT_TCP
                        tcp_connection_.ReceiveTimeout = TIMEOUT_TCP
                End Select

                'Arduino is reset when opening a Serial Connection. 
                'bootup time might increase in the future. 
                'Wait some time for it to boot up...
                System.Threading.Thread.Sleep(SET_CONNECTED_DELAY)
            Else
                Select Case telescope_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        'disconnected from the device.                        
                        Try
                            serial_connection_.Connected = False
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("IsConnected Set", "error: could not close serial connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("IsConnected Set", "Disconnected from port " + telescope_driver_settings_.com_port_)
                    Case connection_type_t.NETWORK
                        Try
                            tcp_connection_.Close()
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("IsConnected Set", "error: could not close TCP connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("IsConnected Set", "Disconnecting from " & telescope_driver_settings_.ip_address_ & ":" & telescope_driver_settings_.port_)
                End Select
            End If
        End Set
    End Property

    ''' <summary>
    ''' Use this function to throw an exception if we aren't connected to the hardware
    ''' </summary>
    ''' <param name="message"></param>
    Private Sub CheckConnected(ByVal message As String)
        If Not IsConnected Then
            Throw New NotConnectedException(message)
        End If
    End Sub

#End Region

#Region "Reading from and writing to the ASCOM Profile Store"
    ''' <summary>
    ''' Read the device configuration from the ASCOM Profile store
    ''' </summary>
    Friend Sub ReadProfile()
        TL.LogMessage("ReadProfile", "Start")

        Using driverProfile As New Profile()
            driverProfile.DeviceType = "Telescope"

            Dim buffer As String    'buffer for strings that are parsed to enum values.

            telescopeDriverSettings.trace_state_ = Convert.ToBoolean(driverProfile.GetValue(driverID, traceStateProfileName, String.Empty, traceStateDefault))

            With telescope_driver_settings_
                buffer = driverProfile.GetValue(driverID, connectionTypeProfileName, String.Empty, connectionTypeDefault)
                .connection_type_ = [Enum].Parse(GetType(connection_type_t), buffer)

                .com_port_ = driverProfile.GetValue(driverID, comPortProfileName, String.Empty, comPortDefault)
                .baud_rate_ = driverProfile.GetValue(driverID, baudRateProfileName, String.Empty, baudRateDefault)

                .ip_address_ = driverProfile.GetValue(driverID, ipAddressProfileName, String.Empty, ipAddressDefault)
                .port_ = driverProfile.GetValue(driverID, portProfileName, String.Empty, portDefault)
            End With

            'general telescope settings.
            With telescope_driver_settings_.telescope_settings_
                buffer = driverProfile.GetValue(driverID, telescopeProfileNames.mountTypeProfileName, String.Empty, telescopeDefaultValues.mountTypeDefault)
                .mount_type_ = [Enum].Parse(GetType(ASCOM.DeviceInterface.AlignmentModes), buffer)

                .focal_length_ = driverProfile.GetValue(driverID, telescopeProfileNames.focalLengthProfileName, String.Empty, telescopeDefaultValues.focalLengthDefault)
                .aperture_area_ = driverProfile.GetValue(driverID, telescopeProfileNames.apertureAreaProfileName, String.Empty, telescopeDefaultValues.apertureAreaDefault)
                .aperture_diameter_ = driverProfile.GetValue(driverID, telescopeProfileNames.apertureDiameterProfileName, String.Empty, telescopeDefaultValues.apertureDiameterDefault)

                .site_latitude_ = driverProfile.GetValue(driverID, telescopeProfileNames.siteLatitudeProfileName, String.Empty, telescopeDefaultValues.siteLatitudeDefault)
                .site_longitude_ = driverProfile.GetValue(driverID, telescopeProfileNames.siteLongitudeProfileName, String.Empty, telescopeDefaultValues.siteLongitudeDefault)
                .site_elevation_ = driverProfile.GetValue(driverID, telescopeProfileNames.siteElevationProfileName, String.Empty, telescopeDefaultValues.siteElevationDefault)

                .startup_auto_home_ = driverProfile.GetValue(driverID, telescopeProfileNames.SAHProfileName, String.Empty, telescopeDefaultValues.SAHDefault)
                .park_enabled_ = driverProfile.GetValue(driverID, telescopeProfileNames.ParkEnabledProfileName, String.Empty, telescopeDefaultValues.parkEnabledDefault)

                .mc_available_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCAProfileName, String.Empty, telescopeDefaultValues.MCADefault)
                .mc_toggle_available_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCTAProfileName, String.Empty, telescopeDefaultValues.MCTADefault)
                .mc_cycle_speed_available_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCCSAProfileName, String.Empty, telescopeDefaultValues.MCCSADefault)
                .mc_joystick_available_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCJAProfileName, String.Empty, telescopeDefaultValues.MCJADefault)

                .mc_toggle_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCTProfileName, String.Empty, telescopeDefaultValues.MCTDefault)
                .mc_cycle_speed_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCCSProfileName, String.Empty, telescopeDefaultValues.MCCSDefault)

                '.mc_max_speed_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCMSProfileName, String.Empty, telescopeDefaultValues.manualControlMaxSpeed)
                'buffer = driverProfile.GetValue(driverID, telescopeProfileNames.MCSTProfileName, String.Empty, telescopeDefaultValues.manualControlSteptype)
                '.mc_steptype_ = [Enum].Parse(GetType(step_type_t), buffer)

                .mc_joystick_x_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCJXProfileName, String.Empty, telescopeDefaultValues.MCJXDefault)
                .mc_joystick_y_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCJYProfileName, String.Empty, telescopeDefaultValues.MCJYDefault)
                .mc_joystick_deadzone_ = driverProfile.GetValue(driverID, telescopeProfileNames.MCJDProfileName, String.Empty, telescopeDefaultValues.MCJDDefault)

                'read temperature sensor information from profile store.
                For i As Integer = 0 To numTempSensors - 1
                    .temperature_sensors_(i) = New YAAASharedClassLibrary.YAAASensor(True)
                    With telescope_driver_settings_.telescope_settings_.temperature_sensors_(i).sensor_settings_
                        Try
                            .sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE

                            .sensor_device_ = driverProfile.GetValue(driverID, telescopeProfileNames.TSDProfileName, telescopeProfileNames.TSSubKeyProfileName & " " & i, 255)
                            .sensor_unit_ = driverProfile.GetValue(driverID, telescopeProfileNames.TSUProfileName, telescopeProfileNames.TSSubKeyProfileName & " " & i, 1)

                            buffer = driverProfile.GetValue(driverID, telescopeProfileNames.TSPProfileName, telescopeProfileNames.TSSubKeyProfileName & " " & i, "0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF")
                            .sensor_parameters_ = YAAASharedClassLibrary.hexToByteArray(buffer)
                        Catch exc As Exception
                            TL.LogMessage("ReadProfile", "error reading Temperature Sensor Data of sensor " & i & ": " & exc.ToString)
                        End Try
                    End With
                Next
            End With

            'telescope axis settings.
            For i As Integer = 0 To 2

                Dim path As String = telescope_driver_settings_.axis_settings_(i).axis_name_

                telescope_driver_settings_.axis_settings_(i).max_speed_ = driverProfile.GetValue(driverID, AxisProfileNames.maxSpeedProfileName, path, axisDefaultValues.maxSpeedDefault)
                telescope_driver_settings_.axis_settings_(i).acceleration_ = driverProfile.GetValue(driverID, AxisProfileNames.accelerationProfileName, path, axisDefaultValues.accelerationDefault)
                telescope_driver_settings_.axis_settings_(i).gear_ratio_ = driverProfile.GetValue(driverID, AxisProfileNames.gearRatioProfileName, path, axisDefaultValues.gearRatioDefault)
                telescope_driver_settings_.axis_settings_(i).number_of_steps_ = driverProfile.GetValue(driverID, AxisProfileNames.numberOfStepsProfileName, path, axisDefaultValues.numberOfStepsDefault)

                buffer = driverProfile.GetValue(driverID, AxisProfileNames.stepTypeFastProfileName, path, axisDefaultValues.stepTypeFastDefault)
                telescope_driver_settings_.axis_settings_(i).step_type_fast_ = [Enum].Parse(GetType(Telescope.step_type_t), buffer)
                buffer = driverProfile.GetValue(driverID, AxisProfileNames.stepTypeSlowProfileName, path, axisDefaultValues.stepTypeSlowDefault)
                telescope_driver_settings_.axis_settings_(i).step_type_slow_ = [Enum].Parse(GetType(Telescope.step_type_t), buffer)

                telescope_driver_settings_.axis_settings_(i).automatic_stepper_release_ = driverProfile.GetValue(driverID, AxisProfileNames.ASRProfileName, path, axisDefaultValues.ASRDefault)

                telescope_driver_settings_.axis_settings_(i).park_enabled_ = driverProfile.GetValue(driverID, AxisProfileNames.ParkEnabledProfileName, path, axisDefaultValues.parkEnabledDefault)
                telescope_driver_settings_.axis_settings_(i).park_pos_ = driverProfile.GetValue(driverID, AxisProfileNames.parkPosProfileName, path, axisDefaultValues.parkPosDefault)

                path = telescope_driver_settings_.axis_settings_(i).axis_name_ & "\Home"

                telescope_driver_settings_.axis_settings_(i).home_switch_enabled_ = driverProfile.GetValue(driverID, AxisProfileNames.HSAProfileName, path, axisDefaultValues.HSADefault)
                telescope_driver_settings_.axis_settings_(i).home_switch_nr_ = driverProfile.GetValue(driverID, AxisProfileNames.HSNProfileName, path, axisDefaultValues.HSNDefault)
                telescope_driver_settings_.axis_settings_(i).home_switch_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.HSIProfileName, path, axisDefaultValues.HSIDefault)
                telescope_driver_settings_.axis_settings_(i).homing_direction_ = driverProfile.GetValue(driverID, AxisProfileNames.HDProfileName, path, axisDefaultValues.HDDefault)
                telescope_driver_settings_.axis_settings_(i).home_alignment_ = driverProfile.GetValue(driverID, AxisProfileNames.homeAlignmentProfileName, path, axisDefaultValues.homeAlignmentDefault)

                telescope_driver_settings_.axis_settings_(i).home_alignment_sync_ = driverProfile.GetValue(driverID, AxisProfileNames.HASProfileName, path, axisDefaultValues.HASDefault)

                'axis_settings_(i).startup_auto_home_ = driverProfile.GetValue(driverID, AxisProfileNames.SAHProfileName, path, axisDefaultValues.SAHDefault)

                path = telescope_driver_settings_.axis_settings_(i).axis_name_ & "\Endstops"

                telescope_driver_settings_.axis_settings_(i).endstop_lower_enabled_ = driverProfile.GetValue(driverID, AxisProfileNames.endStopLowerEnabledProfileName, path, axisDefaultValues.endstopLowerEnabledDefault)
                telescope_driver_settings_.axis_settings_(i).endstop_upper_enabled_ = driverProfile.GetValue(driverID, AxisProfileNames.endStopUpperEnabledProfileName, path, axisDefaultValues.endstopUpperEnabledDefault)
                telescope_driver_settings_.axis_settings_(i).endstop_lower_ = driverProfile.GetValue(driverID, AxisProfileNames.endStopLowerProfileName, path, axisDefaultValues.endstopLowerDefault)
                telescope_driver_settings_.axis_settings_(i).endstop_upper_ = driverProfile.GetValue(driverID, AxisProfileNames.endStopUpperProfileName, path, axisDefaultValues.endstopUpperDefault)
                telescope_driver_settings_.axis_settings_(i).endstop_lower_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.endstopLowerInvertedProfileName, path, axisDefaultValues.endstopUpperInvertedDefault)
                telescope_driver_settings_.axis_settings_(i).endstop_upper_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.endstopUpperInvertedProfileName, path, axisDefaultValues.endstopLowerInvertedDefault)
                telescope_driver_settings_.axis_settings_(i).endstop_hard_stop_ = driverProfile.GetValue(driverID, AxisProfileNames.endstopHardStop, path, axisDefaultValues.endstopHardStopDefault)

                path = telescope_driver_settings_.axis_settings_(i).axis_name_ & "\Limits"

                telescope_driver_settings_.axis_settings_(i).limit_lower_enabled_ = driverProfile.GetValue(driverID, AxisProfileNames.limitLowerEnabledProfileName, path, axisDefaultValues.limitLowerEnabledDefault)
                telescope_driver_settings_.axis_settings_(i).limit_upper_enabled_ = driverProfile.GetValue(driverID, AxisProfileNames.limitUpperEnabledProfileName, path, axisDefaultValues.limitUpperEnabledDefault)
                telescope_driver_settings_.axis_settings_(i).limit_lower_ = driverProfile.GetValue(driverID, AxisProfileNames.limitLowerProfileName, path, axisDefaultValues.limitLowerDefault)
                telescope_driver_settings_.axis_settings_(i).limit_upper_ = driverProfile.GetValue(driverID, AxisProfileNames.limitUpperProfileName, path, axisDefaultValues.limitUpperDefault)
                telescope_driver_settings_.axis_settings_(i).limit_hard_stop_ = driverProfile.GetValue(driverID, AxisProfileNames.limitHardStop, path, axisDefaultValues.limitHardStopDefault)

                'read limit handling setting as String and parse string to get Enum value.
                buffer = driverProfile.GetValue(driverID, AxisProfileNames.limitHandlingProfileName, path, axisDefaultValues.limitHandlingDefault)
                telescope_driver_settings_.axis_settings_(i).limit_handling_ = [Enum].Parse(GetType(Telescope.limit_handling_t), buffer)

                path = telescope_driver_settings_.axis_settings_(i).axis_name_

                buffer = driverProfile.GetValue(driverID, AxisProfileNames.motorDriverProfileName, path, axisDefaultValues.motorDriverDefault)
                telescope_driver_settings_.axis_settings_(i).motor_driver_ = [Enum].Parse(GetType(Telescope.motor_driver_t), buffer)

                'I2C Address and Motor Shield Terminal for Adafruit Motor Shield V2
                telescope_driver_settings_.axis_settings_(i).i2c_address_ = driverProfile.GetValue(driverID, AxisProfileNames.I2CAddressProfileName, path, axisDefaultValues.I2CAddressDefault)
                telescope_driver_settings_.axis_settings_(i).terminal_ = driverProfile.GetValue(driverID, AxisProfileNames.terminalProfileName, path, axisDefaultValues.terminalDefault)

                'Pin numbers and Pin Inversion Settings for 4 wire steppers or stepper drivers (big easy etc.)
                telescope_driver_settings_.axis_settings_(i).pin_1_ = driverProfile.GetValue(driverID, AxisProfileNames.pin1ProfileName, path, axisDefaultValues.pin1Default)
                telescope_driver_settings_.axis_settings_(i).pin_2_ = driverProfile.GetValue(driverID, AxisProfileNames.pin2ProfileName, path, axisDefaultValues.pin2Default)
                telescope_driver_settings_.axis_settings_(i).pin_3_ = driverProfile.GetValue(driverID, AxisProfileNames.pin3ProfileName, path, axisDefaultValues.pin3Default)
                telescope_driver_settings_.axis_settings_(i).pin_4_ = driverProfile.GetValue(driverID, AxisProfileNames.pin4ProfileName, path, axisDefaultValues.pin4Default)
                telescope_driver_settings_.axis_settings_(i).pin_5_ = driverProfile.GetValue(driverID, AxisProfileNames.pin5ProfileName, path, axisDefaultValues.pin5Default)
                telescope_driver_settings_.axis_settings_(i).pin_6_ = driverProfile.GetValue(driverID, AxisProfileNames.pin6ProfileName, path, axisDefaultValues.pin6Default)

                telescope_driver_settings_.axis_settings_(i).pin_1_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.pin1InvertedProfileName, path, axisDefaultValues.pin1InvertedDefault)
                telescope_driver_settings_.axis_settings_(i).pin_2_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.pin2InvertedProfileName, path, axisDefaultValues.pin2InvertedDefault)
                telescope_driver_settings_.axis_settings_(i).pin_3_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.pin3InvertedProfileName, path, axisDefaultValues.pin3InvertedDefault)
                telescope_driver_settings_.axis_settings_(i).pin_4_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.pin4InvertedProfileName, path, axisDefaultValues.pin4InvertedDefault)
                telescope_driver_settings_.axis_settings_(i).pin_5_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.pin5InvertedProfileName, path, axisDefaultValues.pin5InvertedDefault)
                telescope_driver_settings_.axis_settings_(i).pin_6_inverted_ = driverProfile.GetValue(driverID, AxisProfileNames.pin6InvertedProfileName, path, axisDefaultValues.pin6InvertedDefault)

            Next i

        End Using

        TL.LogMessage("ReadProfile", "End")
    End Sub

    ''' <summary>
    ''' Write the device configuration to the  ASCOM  Profile store
    ''' </summary>
    Friend Sub WriteProfile()
        TL.LogMessage("WriteProfile", "Start")

        Using driverProfile As New Profile()
            driverProfile.DeviceType = "Telescope"

            driverProfile.WriteValue(driverID, traceStateProfileName, telescopeDriverSettings.trace_state_.ToString())

            With telescope_driver_settings_
                driverProfile.WriteValue(driverID, connectionTypeProfileName, .connection_type_.ToString)

                driverProfile.WriteValue(driverID, ipAddressProfileName, .ip_address_)
                driverProfile.WriteValue(driverID, portProfileName, .port_)

                driverProfile.WriteValue(driverID, comPortProfileName, .com_port_.ToString())
                driverProfile.WriteValue(driverID, baudRateProfileName, .baud_rate_.ToString())
            End With

            With telescope_driver_settings_.telescope_settings_ 'telescope_settings_
                driverProfile.WriteValue(driverID, telescopeProfileNames.mountTypeProfileName, .mount_type_.ToString)
                driverProfile.WriteValue(driverID, telescopeProfileNames.focalLengthProfileName, .focal_length_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.apertureAreaProfileName, .aperture_area_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.apertureDiameterProfileName, .aperture_diameter_)

                driverProfile.WriteValue(driverID, telescopeProfileNames.siteLatitudeProfileName, .site_latitude_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.siteLongitudeProfileName, .site_longitude_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.siteElevationProfileName, .site_elevation_)

                driverProfile.WriteValue(driverID, telescopeProfileNames.SAHProfileName, .startup_auto_home_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.ParkEnabledProfileName, .park_enabled_)

                driverProfile.WriteValue(driverID, telescopeProfileNames.MCAProfileName, .mc_available_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.MCTAProfileName, .mc_toggle_available_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.MCCSAProfileName, .mc_cycle_speed_available_)

                driverProfile.WriteValue(driverID, telescopeProfileNames.MCTProfileName, .mc_toggle_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.MCCSProfileName, .mc_cycle_speed_)

                'driverProfile.WriteValue(driverID, telescopeProfileNames.MCMSProfileName, .mc_max_speed_)
                'driverProfile.WriteValue(driverID, telescopeProfileNames.MCSTProfileName, .mc_steptype.ToString_)

                driverProfile.WriteValue(driverID, telescopeProfileNames.MCJXProfileName, .mc_joystick_x_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.MCJYProfileName, .mc_joystick_y_)
                driverProfile.WriteValue(driverID, telescopeProfileNames.MCJDProfileName, .mc_joystick_deadzone_)

                'temperature sensor settings
                For i As Integer = 0 To numTempSensors - 1
                    With telescope_driver_settings_.telescope_settings_.temperature_sensors_(i).sensor_settings_
                        .sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE

                        driverProfile.WriteValue(driverID, telescopeProfileNames.TSDProfileName, .sensor_device_, telescopeProfileNames.TSSubKeyProfileName & " " & i)
                        driverProfile.WriteValue(driverID, telescopeProfileNames.TSUProfileName, .sensor_unit_, telescopeProfileNames.TSSubKeyProfileName & " " & i)

                        Dim parameters_string As String = ""
                        For j As Integer = 0 To .sensor_parameters_.Length - 1
                            parameters_string += Hex(.sensor_parameters_(j))
                            parameters_string += " "
                        Next

                        driverProfile.WriteValue(driverID, telescopeProfileNames.TSPProfileName, parameters_string, telescopeProfileNames.TSSubKeyProfileName & " " & i)
                    End With
                Next
            End With

            For Each axisSettings In telescope_driver_settings_.axis_settings_
                Dim path As String = axisSettings.axis_name_

                driverProfile.WriteValue(driverID, AxisProfileNames.maxSpeedProfileName, axisSettings.max_speed_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.accelerationProfileName, axisSettings.acceleration_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.stepTypeFastProfileName, axisSettings.step_type_fast_.ToString, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.stepTypeSlowProfileName, axisSettings.step_type_slow_.ToString, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.gearRatioProfileName, axisSettings.gear_ratio_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.numberOfStepsProfileName, axisSettings.number_of_steps_, path)

                driverProfile.WriteValue(driverID, AxisProfileNames.ASRProfileName, axisSettings.automatic_stepper_release_, path)

                driverProfile.WriteValue(driverID, AxisProfileNames.ParkEnabledProfileName, axisSettings.park_enabled_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.parkPosProfileName, axisSettings.park_pos_, path)

                path = axisSettings.axis_name_ & "\Home"

                driverProfile.WriteValue(driverID, AxisProfileNames.HSAProfileName, axisSettings.home_switch_enabled_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.HSNProfileName, axisSettings.home_switch_nr_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.HSIProfileName, axisSettings.home_switch_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.HDProfileName, axisSettings.homing_direction_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.homeAlignmentProfileName, axisSettings.home_alignment_, path)

                driverProfile.WriteValue(driverID, AxisProfileNames.HASProfileName, axisSettings.home_alignment_sync_, path)

                'driverProfile.WriteValue(driverID, AxisProfileNames.SAHProfileName, axisSettings.startup_auto_home_, path)

                path = axisSettings.axis_name_ & "\Endstops"

                driverProfile.WriteValue(driverID, AxisProfileNames.endStopLowerEnabledProfileName, axisSettings.endstop_lower_enabled_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.endStopUpperEnabledProfileName, axisSettings.endstop_upper_enabled_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.endStopLowerProfileName, axisSettings.endstop_lower_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.endStopUpperProfileName, axisSettings.endstop_upper_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.endstopLowerInvertedProfileName, axisSettings.endstop_lower_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.endstopUpperInvertedProfileName, axisSettings.endstop_upper_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.endstopHardStop, axisSettings.endstop_hard_stop_, path)

                path = axisSettings.axis_name_ & "\Limits"

                driverProfile.WriteValue(driverID, AxisProfileNames.limitLowerEnabledProfileName, axisSettings.limit_lower_enabled_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.limitUpperEnabledProfileName, axisSettings.limit_upper_enabled_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.limitLowerProfileName, axisSettings.limit_lower_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.limitUpperProfileName, axisSettings.limit_upper_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.limitHardStop, axisSettings.limit_hard_stop_, path)

                'write limit handling setting as String.
                driverProfile.WriteValue(driverID, AxisProfileNames.limitHandlingProfileName, axisSettings.limit_handling_.ToString, path)

                path = axisSettings.axis_name_

                driverProfile.WriteValue(driverID, AxisProfileNames.motorDriverProfileName, axisSettings.motor_driver_.ToString, path)

                driverProfile.WriteValue(driverID, AxisProfileNames.I2CAddressProfileName, axisSettings.i2c_address_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.terminalProfileName, axisSettings.terminal_, path)

                driverProfile.WriteValue(driverID, AxisProfileNames.pin1ProfileName, axisSettings.pin_1_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin2ProfileName, axisSettings.pin_2_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin3ProfileName, axisSettings.pin_3_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin4ProfileName, axisSettings.pin_4_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin5ProfileName, axisSettings.pin_5_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin6ProfileName, axisSettings.pin_6_, path)

                driverProfile.WriteValue(driverID, AxisProfileNames.pin1InvertedProfileName, axisSettings.pin_1_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin2InvertedProfileName, axisSettings.pin_2_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin3InvertedProfileName, axisSettings.pin_3_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin4InvertedProfileName, axisSettings.pin_4_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin5InvertedProfileName, axisSettings.pin_5_inverted_, path)
                driverProfile.WriteValue(driverID, AxisProfileNames.pin6InvertedProfileName, axisSettings.pin_6_inverted_, path)
            Next
        End Using

        TL.LogMessage("WriteProfile", "End")
    End Sub

#End Region

#Region "Reading and Writing from / to the Ardunio's EEPROM"

    Friend Sub ReadEEPROM()
        CheckConnected("ReadEEPROM")

        TL.LogMessage("ReadEEPROM", "Start")

        telescope_driver_settings_EEPROM_ = telescope_driver_settings_.Clone()

        Dim buffer As Short

        'read configuration from arduino EEPROM.

        'read general telescope settings.
        readSetting(buffer, "MOUNT_TYPE")
        telescope_driver_settings_EEPROM_.telescope_settings_.mount_type_ = [Enum].Parse(GetType(ASCOM.DeviceInterface.AlignmentModes), buffer)

        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.site_latitude_, "SITE_LATITUDE")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.site_longitude_, "SITE_LONGITUDE")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.site_elevation_, "SITE_ELEVATION")

        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.focal_length_, "FOCAL_LENGTH")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.aperture_diameter_, "APERTURE_DIAMETER")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.aperture_area_, "APERTURE_AREA")

        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.startup_auto_home_, "STARTUP_AUTO_HOME")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.park_enabled_, "PARK_ENABLED")

        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_available_, "MC_AVAILABLE")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_toggle_available_, "MC_BTN_TOGGLE_AVAILABLE")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_cycle_speed_available_, "MC_BTN_SPEED_AVAILABLE")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_available_, "MC_JST_AVAILABLE")

        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_toggle_, "MC_BTN_TOGGLE")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_cycle_speed_, "MC_BTN_SPEED")

        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_x_, "MC_JST_INPUT_X")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_y_, "MC_JST_INPUT_Y")
        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_deadzone_, "MC_JST_DEADZONE")

        readSetting(telescope_driver_settings_EEPROM_.telescope_settings_.temperature_sensors_, "TEMPERATURE_SENSOR,")

        'read telescope axis specific settings.
        For i As Integer = 0 To 2
            Dim axis As String = telescope_driver_settings_EEPROM_.axis_settings_(i).axis_name_

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).max_speed_, "MAX_SPEED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).acceleration_, "ACCELERATION", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).gear_ratio_, "GEAR_RATIO", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).number_of_steps_, "NUMBER_OF_STEPS", axis)
            readSetting(buffer, "STEPTYPE_FAST", axis)
            telescope_driver_settings_EEPROM_.axis_settings_(i).step_type_fast_ = [Enum].Parse(GetType(Telescope.step_type_t), buffer)
            readSetting(buffer, "STEPTYPE_SLOW", axis)
            telescope_driver_settings_EEPROM_.axis_settings_(i).step_type_slow_ = [Enum].Parse(GetType(Telescope.step_type_t), buffer)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).home_switch_enabled_, "HOME_SWITCH_ENABLED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).home_switch_nr_, "HOME_SWITCH", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).home_switch_inverted_, "HOME_SWITCH_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).home_alignment_, "HOME_ALIGNMENT", axis)
            'readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).homing_direction_, "HOMING_DIRECTION", axis)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).home_alignment_sync_, "HOME_ALIGNMENT_SYNC", axis)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).automatic_stepper_release_, "AUTOMATIC_STEPPER_RELEASE", axis)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).park_pos_, "PARK_POS", axis)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_lower_enabled_, "ENDSTOP_LOWER_ENABLED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_upper_enabled_, "ENDSTOP_UPPER_ENABLED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_lower_, "ENDSTOP_LOWER", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_upper_, "ENDSTOP_UPPER", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_lower_inverted_, "ENDSTOP_LOWER_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_upper_inverted_, "ENDSTOP_UPPER_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_hard_stop_, "ENDSTOP_HARD_STOP", axis)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).limit_lower_enabled_, "LIMIT_LOWER_ENABLED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).limit_upper_enabled_, "LIMIT_UPPER_ENABLED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).limit_lower_, "LIMIT_LOWER", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).limit_upper_, "LIMIT_UPPER", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).limit_hard_stop_, "LIMIT_HARD_STOP", axis)
            readSetting(buffer, "LIMIT_HANDLING", axis)
            telescope_driver_settings_EEPROM_.axis_settings_(i).limit_handling_ = [Enum].Parse(GetType(Telescope.limit_handling_t), buffer)
            readSetting(buffer, "MOTOR_DRIVER_TYPE", axis)
            telescope_driver_settings_EEPROM_.axis_settings_(i).motor_driver_ = [Enum].Parse(GetType(Telescope.motor_driver_t), buffer)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).i2c_address_, "I2C_ADDRESS", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).terminal_, "TERMINAL", axis)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_1_, "PIN_1", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_2_, "PIN_2", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_3_, "PIN_3", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_4_, "PIN_4", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_5_, "PIN_5", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_6_, "PIN_6", axis)

            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_1_inverted_, "PIN_1_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_2_inverted_, "PIN_2_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_3_inverted_, "PIN_3_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_4_inverted_, "PIN_4_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_5_inverted_, "PIN_5_INVERTED", axis)
            readSetting(telescope_driver_settings_EEPROM_.axis_settings_(i).pin_6_inverted_, "PIN_6_INVERTED", axis)
        Next

        TL.LogMessage("ReadEEPROM", "End")
    End Sub

    Friend Sub WriteEEPROM()
        CheckConnected("WriteEEPROM")

        WriteProfile()

        TL.LogMessage("WriteEEPROM", "Start")

        'write configuration to the Arduino's EEPROM.
        With telescope_driver_settings_.telescope_settings_

            writeSetting(CShort(.mount_type_), CShort(telescope_driver_settings_EEPROM_.telescope_settings_.mount_type_), "MOUNT_TYPE")
            telescope_driver_settings_EEPROM_.telescope_settings_.mount_type_ = .mount_type_

            writeSetting(.site_latitude_, telescope_driver_settings_EEPROM_.telescope_settings_.site_latitude_, "SITE_LATITUDE")
            writeSetting(.site_longitude_, telescope_driver_settings_EEPROM_.telescope_settings_.site_longitude_, "SITE_LONGITUDE")
            writeSetting(.site_elevation_, telescope_driver_settings_EEPROM_.telescope_settings_.site_elevation_, "SITE_ELEVATION")

            writeSetting(.focal_length_, telescope_driver_settings_EEPROM_.telescope_settings_.focal_length_, "FOCAL_LENGTH")
            writeSetting(.aperture_diameter_, telescope_driver_settings_EEPROM_.telescope_settings_.aperture_diameter_, "APERTURE_DIAMETER")
            writeSetting(.aperture_area_, telescope_driver_settings_EEPROM_.telescope_settings_.aperture_area_, "APERTURE_AREA")

            writeSetting(.startup_auto_home_, telescope_driver_settings_EEPROM_.telescope_settings_.startup_auto_home_, "STARTUP_AUTO_HOME")
            writeSetting(.park_enabled_, telescope_driver_settings_EEPROM_.telescope_settings_.park_enabled_, "PARK_ENABLED")

            writeSetting(.mc_available_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_available_, "MC_AVAILABLE")
            writeSetting(.mc_toggle_available_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_toggle_available_, "MC_BTN_TOGGLE_AVAILABLE")
            writeSetting(.mc_cycle_speed_available_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_cycle_speed_available_, "MC_BTN_SPEED_AVAILABLE")
            writeSetting(.mc_joystick_available_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_available_, "MC_JST_AVAILABLE")

            writeSetting(.mc_toggle_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_toggle_, "MC_BTN_TOGGLE")
            writeSetting(.mc_cycle_speed_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_cycle_speed_, "MC_BTN_SPEED")

            writeSetting(.mc_joystick_x_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_x_, "MC_JST_INPUT_X")
            writeSetting(.mc_joystick_y_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_y_, "MC_JST_INPUT_Y")
            writeSetting(.mc_joystick_deadzone_, telescope_driver_settings_EEPROM_.telescope_settings_.mc_joystick_deadzone_, "MC_JST_DEADZONE")

            writeSetting(.temperature_sensors_, telescope_driver_settings_EEPROM_.telescope_settings_.temperature_sensors_, "TEMPERATURE_SENSOR")
        End With


        'For Each axisSettings In axis_settings_
        For i As Integer = 0 To 2
            With telescope_driver_settings_.axis_settings_(i)
                Dim axis As String = .axis_name_

                writeSetting(.i2c_address_, telescope_driver_settings_EEPROM_.axis_settings_(i).i2c_address_, "I2C_ADDRESS", axis)
                writeSetting(.acceleration_, telescope_driver_settings_EEPROM_.axis_settings_(i).acceleration_, "ACCELERATION", axis)
                writeSetting(.max_speed_, telescope_driver_settings_EEPROM_.axis_settings_(i).max_speed_, "MAX_SPEED", axis)
                writeSetting(.gear_ratio_, telescope_driver_settings_EEPROM_.axis_settings_(i).gear_ratio_, "GEAR_RATIO", axis)
                writeSetting(.number_of_steps_, telescope_driver_settings_EEPROM_.axis_settings_(i).number_of_steps_, "NUMBER_OF_STEPS", axis)

                writeSetting(CShort(.step_type_fast_), CShort(telescope_driver_settings_EEPROM_.axis_settings_(i).step_type_fast_), "STEPTYPE_FAST", axis)
                telescope_driver_settings_EEPROM_.axis_settings_(i).step_type_fast_ = .step_type_fast_
                writeSetting(CShort(.step_type_slow_), CShort(telescope_driver_settings_EEPROM_.axis_settings_(i).step_type_slow_), "STEPTYPE_SLOW", axis)
                telescope_driver_settings_EEPROM_.axis_settings_(i).step_type_slow_ = .step_type_slow_

                writeSetting(.home_switch_enabled_, telescope_driver_settings_EEPROM_.axis_settings_(i).home_switch_enabled_, "HOME_SWITCH_ENABLED", axis)
                writeSetting(.home_switch_nr_, telescope_driver_settings_EEPROM_.axis_settings_(i).home_switch_nr_, "HOME_SWITCH", axis)
                writeSetting(.home_switch_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).home_switch_inverted_, "HOME_SWITCH_INVERTED", axis)
                writeSetting(.home_alignment_, telescope_driver_settings_EEPROM_.axis_settings_(i).home_alignment_, "HOME_ALIGNMENT", axis)
                'writeSetting(.home_direction_, telescope_driver_settings_EEPROM_.axis_settings_(i).home_direction_, "HOMING_DIRECTION", axis)
                writeSetting(.home_alignment_sync_, telescope_driver_settings_EEPROM_.axis_settings_(i).home_alignment_sync_, "HOME_ALIGNMENT_SYNC", axis)

                writeSetting(.automatic_stepper_release_, telescope_driver_settings_EEPROM_.axis_settings_(i).automatic_stepper_release_, "AUTOMATIC_STEPPER_RELEASE", axis)
                writeSetting(.park_pos_, telescope_driver_settings_EEPROM_.axis_settings_(i).park_pos_, "PARK_POS", axis)

                writeSetting(.endstop_lower_enabled_, telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_lower_enabled_, "ENDSTOP_LOWER_ENABLED", axis)
                writeSetting(.endstop_upper_enabled_, telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_upper_enabled_, "ENDSTOP_UPPER_ENABLED", axis)
                writeSetting(.endstop_lower_, telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_lower_, "ENDSTOP_LOWER", axis)
                writeSetting(.endstop_upper_, telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_upper_, "ENDSTOP_UPPER", axis)
                writeSetting(.endstop_lower_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_lower_inverted_, "ENDSTOP_LOWER_INVERTED", axis)
                writeSetting(.endstop_upper_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_upper_inverted_, "ENDSTOP_UPPER_INVERTED", axis)
                writeSetting(.endstop_hard_stop_, telescope_driver_settings_EEPROM_.axis_settings_(i).endstop_hard_stop_, "ENDSTOP_HARD_STOP", axis)

                writeSetting(.limit_lower_enabled_, telescope_driver_settings_EEPROM_.axis_settings_(i).limit_lower_enabled_, "LIMIT_LOWER_ENABLED", axis)
                writeSetting(.limit_upper_enabled_, telescope_driver_settings_EEPROM_.axis_settings_(i).limit_upper_enabled_, "LIMIT_UPPER_ENABLED", axis)
                writeSetting(.limit_lower_, telescope_driver_settings_EEPROM_.axis_settings_(i).limit_lower_, "LIMIT_LOWER", axis)
                writeSetting(.limit_upper_, telescope_driver_settings_EEPROM_.axis_settings_(i).limit_upper_, "LIMIT_UPPER", axis)
                writeSetting(.limit_hard_stop_, telescope_driver_settings_EEPROM_.axis_settings_(i).limit_hard_stop_, "LIMIT_HARD_STOP", axis)

                writeSetting(CShort(.limit_handling_), CShort(telescope_driver_settings_EEPROM_.axis_settings_(i).limit_handling_), "LIMIT_HANDLING", axis)
                telescope_driver_settings_EEPROM_.axis_settings_(i).limit_handling_ = .limit_handling_

                writeSetting(CShort(.motor_driver_), CShort(telescope_driver_settings_EEPROM_.axis_settings_(i).motor_driver_), "MOTOR_DRIVER_TYPE", axis)
                telescope_driver_settings_EEPROM_.axis_settings_(i).motor_driver_ = .motor_driver_

                writeSetting(.i2c_address_, telescope_driver_settings_EEPROM_.axis_settings_(i).i2c_address_, "I2C_ADDRESS", axis)
                writeSetting(.terminal_, telescope_driver_settings_EEPROM_.axis_settings_(i).terminal_, "TERMINAL", axis)

                writeSetting(.pin_1_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_1_, "PIN_1", axis)
                writeSetting(.pin_2_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_2_, "PIN_2", axis)
                writeSetting(.pin_3_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_3_, "PIN_3", axis)
                writeSetting(.pin_4_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_4_, "PIN_4", axis)
                writeSetting(.pin_5_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_5_, "PIN_5", axis)
                writeSetting(.pin_6_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_6_, "PIN_6", axis)

                writeSetting(.pin_1_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_1_inverted_, "PIN_1_INVERTED", axis)
                writeSetting(.pin_2_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_2_inverted_, "PIN_2_INVERTED", axis)
                writeSetting(.pin_3_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_3_inverted_, "PIN_3_INVERTED", axis)
                writeSetting(.pin_4_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_4_inverted_, "PIN_4_INVERTED", axis)
                writeSetting(.pin_5_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_5_inverted_, "PIN_5_INVERTED", axis)
                writeSetting(.pin_6_inverted_, telescope_driver_settings_EEPROM_.axis_settings_(i).pin_6_inverted_, "PIN_6_INVERTED", axis)
            End With
        Next

        TL.LogMessage("WriteEEPROM", "End")
    End Sub

#Region "reading settings from arduino"

    Private Sub readSetting(ByRef value As Boolean, setting As String, Optional axis As String = Nothing)
        value = Not (readSettings(setting, axis).Equals("0"))
    End Sub

    Private Sub readSetting(ByRef value As Byte, setting As String, Optional axis As String = Nothing)
        value = CByte(readSettings(setting, axis))
    End Sub

    Private Sub readSetting(ByRef value As Short, setting As String, Optional axis As String = Nothing)
        value = CShort(readSettings(setting, axis))
    End Sub

    Private Sub readSetting(ByRef value As UShort, setting As String, Optional axis As String = Nothing)
        value = CUShort(readSettings(setting, axis))
    End Sub

    Private Sub readSetting(ByRef value As Single, setting As String, Optional axis As String = Nothing)
        value = Val(readSettings(setting, axis))
    End Sub

    Private Sub readSetting(ByRef value As Double, setting As String, Optional axis As String = Nothing)
        value = Val(readSettings(setting, axis))
    End Sub

    Private Sub readSetting(ByRef value As Integer, setting As String, Optional axis As String = Nothing)
        value = CInt(readSettings(setting, axis))
    End Sub

    Private Sub readSetting(ByRef value As UInteger, setting As String, Optional axis As String = Nothing)
        value = CUInt(readSettings(setting, axis))
    End Sub

    Private Sub readSetting(ByRef value() As YAAASharedClassLibrary.YAAASensor, setting As String)
        For i As Integer = 0 To value.Length - 1
            Dim str_buffer As String = readSettings("TEMPERATURE_SENSOR," & i.ToString)
            value(i) = New YAAASharedClassLibrary.YAAASensor(str_buffer)
        Next
    End Sub

    Private Function readSettings(setting As String, Optional axis As String = Nothing)
        Dim message As String
        Dim response As String

        'format the axis string (if given)
        If Not axis Is Nothing Then axis = formatAxisString(axis)

        message = "SETUP,"          'setup command 
        message &= "GET,"           'read command
        message &= setting          'setting to read
        If Not axis Is Nothing Then
            message &= ","
            message &= axis
        End If

        response = sendCommand(message)
        response = response.Replace("SETUP,", "").Trim      'trim the rest of the message.

        Return response
    End Function

#End Region

#Region "writing settings to arduino"

    Private Sub writeSetting(value As Boolean, ByRef eeprom_value As Boolean, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(Convert.ToByte(value), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Byte, ByRef eeprom_value As Byte, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(Convert.ToByte(value), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Short, ByRef eeprom_value As Short, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As UShort, ByRef eeprom_value As UShort, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Single, ByRef eeprom_value As Single, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Double, ByRef eeprom_value As Double, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Integer, ByRef eeprom_value As Integer, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As UInteger, ByRef eeprom_value As UInteger, setting As String, Optional axis As String = Nothing)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting, axis)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value() As YAAASharedClassLibrary.YAAASensor, ByRef eeprom_value() As YAAASharedClassLibrary.YAAASensor, setting As String)
        For i As Integer = 0 To numTempSensors - 1
            If EEPROM_force_write_ Or value(i) <> eeprom_value(i) Then
                writeSettings(i & "," & value(i).toArduinoString(), setting)
                eeprom_value(i) = value(i).Clone
            End If
        Next
    End Sub

    Private Function writeSettings(value As String, setting As String, Optional axis As String = Nothing)
        Dim message As String
        Dim response As String

        'format the axis string (if given)
        If Not axis Is Nothing Then axis = formatAxisString(axis)

        message = "SETUP,"              'setup command 
        message &= "SET,"               'read command
        message &= setting & ","        'setting to write
        If Not axis Is Nothing Then
            message &= axis & ","
        End If
        message &= value.ToString()     'value to set

        response = sendCommand(message)
        response = response.Replace("SETUP,", "").Trim      'trim the rest of the message.

        Return response
    End Function

#End Region

#End Region

    'encapsulates and sends a command to the Arduino.
    '@param command command to send.
    '@return response from Arduino.
    Private Function sendCommand(command As String) As String
        Dim message As String = ""
        Dim response As String = ""

        'wrap command string.
        message = "<"               'beginning of the message
        message &= "T,"             'device identifier
        message &= command          'command
        message &= ">"              'end of message

        Select Case telescope_driver_settings_.connection_type_
            Case connection_type_t.SERIAL
                'log sent command
                TL.LogMessage("SERIAL TRANSMIT", message)

                'transmit message to arduino
                serial_connection_.Transmit(message)

                response = serial_connection_.ReceiveTerminated(">")

                'log response from Arduino
                TL.LogMessage("SERIAL RECEIVE", response)

            Case connection_type_t.NETWORK
                'log sent command
                TL.LogMessage("TCP TRANSMIT", message)

                'copy message to buffer and send buffer
                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(message)
                tcp_connection_.GetStream.Write(sendBytes, 0, sendBytes.Length)

                'receive response into receive buffer
                Dim recLength As Integer    'number of bytes received
                Dim recBytes(tcp_connection_.ReceiveBufferSize) As Byte
                recLength = tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))

                'convert receive buffer to string
                response = Encoding.ASCII.GetString(recBytes, 0, recLength)

                'log response from Arduino
                TL.LogMessage("TCP RECEIVE", response)

                'read \cr\lf sent by arduino (the arduino sends these in a seperate packet).
                tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))

        End Select

        response = response.Trim

        response = response.Replace("<T,", "")
        response = response.Replace(">", "")

        'check the response for serial communication errors. throws an exception if a communication error occurred.
        checkMessageError(response)

        Return response
    End Function

    'checks a response for a serial communication error. throws an exception if an error occurred.
    '@param response response from Arduino.
    Private Sub checkMessageError(response As String)
        Dim error_pattern As String = "ERROR_\d+$"
        Dim match As RegularExpressions.Match = RegularExpressions.Regex.Match(response, error_pattern)

        'do nothing if no error message has been received.
        If Not match.Success Then Exit Sub

        'parse the error code when an error message has been received.
        Dim error_code As serial_error_code_t = serial_error_code_t.ERROR_NONE
        Dim error_code_known As Boolean = False

        Dim error_message As String = match.ToString
        TL.LogMessage("checkMessageError", error_message)

        error_code_known = [Enum].TryParse(error_message.Replace("ERROR_", ""), error_code)

        If Not error_code_known Then
            TL.LogMessage("checkMessageError", "unknown error: " & error_message)
            Throw New ASCOM.DriverException("checkMessageError: unknown error: " & error_message)
        Else
            TL.LogMessage("checkMessageError", error_code.ToString)

            If error_code = serial_error_code_t.ERROR_NONE Then
                Exit Sub
            Else
                Throw New ASCOM.DriverException("Serial Communication Error: " & error_code.ToString)
            End If
        End If
    End Sub

    Private Function formatAxisString(axis As String)
        axis = UCase(axis)
        axis = axis.Replace(" ", "_")

        Return axis
    End Function

#Region "additional friend functions"

    'function to return a telescopeDriverSettings struct containing the current driver settings.
    '@param eeprom true to get EEPROM settings, false to get normal settings
    Friend Function getDriverSettings(Optional eeprom As Boolean = False) As telescopeDriverSettings
        Dim return_value As telescopeDriverSettings

        If eeprom = True Then
            return_value = telescope_driver_settings_EEPROM_.Clone
        Else
            return_value = telescope_driver_settings_.Clone
        End If

        Return return_value
    End Function

    'overwrites the driver's telescope settings object with the provided object.
    Friend Sub setDriverSettings(settings As telescopeDriverSettings, Optional eeprom As Boolean = False)
        If eeprom = True Then
            telescope_driver_settings_EEPROM_ = settings.Clone
        Else
            telescope_driver_settings_ = settings.Clone
        End If
    End Sub

#Region "Reading / writing settings fron / to .xml files"

    'opens a file and attempts to read a telescopeDriverSettings structure from it. 
    '@param file_name full path and file name to file
    '@return driver settings stored in file as telescopeDriverSettings
    Friend Function xmlToDriverSettings(file_name As String) As telescopeDriverSettings
        Dim return_value As telescopeDriverSettings = New telescopeDriverSettings(True)

        Dim fs As New FileStream(file_name, FileMode.Open, FileAccess.Read)
        Dim reader As XmlDictionaryReader = XmlDictionaryReader.CreateTextReader(fs, New XmlDictionaryReaderQuotas())
        Dim serializer = New DataContractSerializer(GetType(telescopeDriverSettings))

        return_value = CType(serializer.ReadObject(reader, True), telescopeDriverSettings)
        reader.Close()
        fs.Close()

        Return return_value
    End Function

    'serializes a telescopeDriverSettings struct and saves it to a file.
    '@param file_name full path and file name of file
    '@param settings focuser driver settings as telescopeDriverSettings
    Friend Sub xmlFromDriverSettings(file_name As String, settings As telescopeDriverSettings)
        Dim serializer As New DataContractSerializer(GetType(telescopeDriverSettings))
        Dim writer As New FileStream(file_name, FileMode.Create)

        serializer.WriteObject(writer, settings)
        writer.Close()
    End Sub

#End Region

#End Region

#Region "additional private functions"

    Private Function axisAtHome(axis As String) As Boolean
        Dim return_value As String = ""

        'check user input
        If Not (axis Is "X_AXIS" Or axis Is "Y_AXIS" Or axis Is "Z_AXIS") Then
            TL.LogMessage("axisAtHome", "Error: invalid axis " & axis)
            Throw New ASCOM.InvalidValueException("axisAtHome: invalid axis " & axis)
        End If

        Dim response As String
        response = sendCommand("AT_HOME,GET," & axis)
        response = response.Replace("AT_HOME,", "").Trim

        return_value = Not CInt(response).Equals(0)

        TL.LogMessage("axisAtHome", "Get - " & return_value.ToString())

        Return return_value
    End Function

    'queries homing errors from Arduino.
    'return true if errors occurred, false otherwise.
    Private Function getHomingError() As Boolean
        CheckConnected("getHomingError")

        Dim return_value As Boolean = False
        Dim response As String = ""
        Dim error_code As UInteger = 0

        response = sendCommand("HOMING_ERROR,GET")
        response = response.Replace("HOMING_ERROR,", "").Trim

        'error code is transmitted as a 4 byte unsigned integer
        error_code = CUInt(response)

        TL.LogMessage("getHomingError", "Error code: " & Hex(error_code))

        If logErrorCode("getHomingError", error_code, GetType(telescope_homing_error_t)) Then
            return_value = True
        End If

        response = sendCommand("HOMING_ERROR,GET,X_AXIS")
        response = response.Replace("HOMING_ERROR,", "").Trim

        'error code is transmitted as a 4 byte unsigned integer
        error_code = CUInt(response)

        TL.LogMessage("getHomingError", "Error code (x axis): - " & Hex(error_code))

        If logErrorCode("getHomingError - x axis", error_code, GetType(telescope_homing_error_t)) Then
            return_value = True
        End If

        response = sendCommand("HOMING_ERROR,GET,Y_AXIS")
        response = response.Replace("HOMING_ERROR,", "").Trim

        'error code is transmitted as a 4 byte unsigned integer
        error_code = CUInt(response)

        TL.LogMessage("getHomingError", "Error code (y axis): - " & Hex(error_code))

        If logErrorCode("getHomingError - y axis", error_code, GetType(telescope_homing_error_t)) Then
            return_value = True
        End If

        response = sendCommand("HOMING_ERROR,GET,Z_AXIS")
        response = response.Replace("HOMING_ERROR,", "").Trim

        'error code is transmitted as a 4 byte unsigned integer
        error_code = CUInt(response)

        TL.LogMessage("getHomingError", "Error code (z axis): - " & Hex(error_code))

        If logErrorCode("getHomingError - z axis", error_code, GetType(telescope_homing_error_t)) Then
            return_value = True
        End If

        Return return_value
    End Function

    'queries Arduino for homing state.
    Private Function isHoming() As Boolean
        CheckConnected("IsHoming")

        'sends at home query to Ardunio. 
        'Arduino will return TRUE ("1") when both axes are at the home position.
        Dim return_value As Boolean
        Dim response As String

        response = sendCommand("FIND_HOME,GET")
        response = response.Replace("FIND_HOME,", "").Trim

        return_value = Not CInt(response).Equals(0)

        TL.LogMessage("IsHoming", "Get - " & return_value.ToString())
        Return return_value
    End Function

    'queries the Arduino for the current alignment and updates the transform_ object.
    Private Sub ReadTelescopeAlignment()
        'throw an exception if the telescope is not connected.
        CheckConnected("ReadTelescopeAlignment")

        TL.LogMessage("ReadTelescopeAlignment", "begin")

        Dim x_val As Double
        Dim y_val As Double
        Dim response As String

        response = sendCommand("ALIGNMENT,GET,X_AXIS")
        response = response.Replace("ALIGNMENT,", "").Trim
        x_val = Val(response)

        response = sendCommand("ALIGNMENT,GET,Y_AXIS")
        response = response.Replace("ALIGNMENT,", "").Trim
        y_val = Val(response)

        'save values to transform_ according to mount type.
        If AlignmentMode = AlignmentModes.algAltAz Then
            'values read from arduino are the telescope's topocentric altitude and azimuth.
            transform_.SetAzimuthElevation(x_val, y_val)

            TL.LogMessage("ReadTelescopeAlignment", "Azimuth: " & x_val & " Altitude: " & y_val)

        ElseIf AlignmentMode = AlignmentModes.algPolar Or AlignmentMode = AlignmentModes.algGermanPolar Then
            'values read from telescope are the telescopes topocetric right ascension and declination.
            transform_.SetTopocentric(x_val, y_val)

            TL.LogMessage("ReadTelescopeAlignment", "Right Ascension: " & x_val & " Declination: " & y_val)
        End If

        'recalculate.
        transform_.Refresh()
    End Sub

    'sends a slew command to the Arduino. do not call directly, should only be called by 
    'SlewToAltAz(Async), SlewtoCoordinates(Async) and SlewToTarget(Async). 
    '@param command name of the calling function, for logging.
    Private Sub Slew(command As String)

        Dim x_val As Double = 0.0
        Dim y_val As Double = 0.0

        Dim response As String
        Dim return_value As Integer

        'recalculate.
        transform_.Refresh()

        'convert target coordinates depending on telescope mount type / alignment mode.
        If AlignmentMode = AlignmentModes.algAltAz Then
            x_val = transform_.AzimuthTopocentric
            y_val = transform_.ElevationTopocentric

            TL.LogMessage(command, "Slewing to Az: " & utilities.DegreesToDMS(x_val) & " Alt: " & utilities.DegreesToDMS(y_val))

        ElseIf AlignmentMode = AlignmentModes.algPolar Or AlignmentMode = AlignmentModes.algGermanPolar Then
            x_val = transform_.RATopocentric
            y_val = transform_.DECTopocentric

            TL.LogMessage(command, "Slewing to Ra: " & utilities.HoursToHMS(x_val) & " Dec: " & utilities.DegreesToDMS(y_val))
        End If

        Dim message As String

        'assemble the slew command.
        message = "SLEW,SET,"
        message &= x_val.ToString("F7", CultureInfo.InvariantCulture)
        message &= ","
        message &= y_val.ToString("F7", CultureInfo.InvariantCulture)

        response = sendCommand(message)
        response = response.Replace("SLEW,", "").Trim

        return_value = CInt(response)

        logErrorCode(command, return_value, GetType(telescope_error_t))

        'check the response.
        If return_value.Equals(0) Then
            TL.LogMessage(command, "Success")

        ElseIf (return_value And telescope_error_t.ERROR_OUT_OF_BOUNDS_X) <> 0 Then
            'when a destination beyond the x axis limits has been given, throw an exception.
            TL.LogMessage(command, "Error " & return_value & ": Destination " &
                x_val.ToString("F7", CultureInfo.InvariantCulture) & " beyond limits of X Axis. Valid range: " &
                telescope_driver_settings_.axis_settings_(0).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(0).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))
            Throw New ASCOM.InvalidValueException(
                "Destination beyond limits of X Axis",
                x_val.ToString("F7", CultureInfo.InvariantCulture),
                telescope_driver_settings_.axis_settings_(0).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(0).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))

        ElseIf (return_value And telescope_error_t.ERROR_OUT_OF_BOUNDS_Y) <> 0 Then
            'when a destination beyond the y axis limits has been given, throw an exception.
            TL.LogMessage(command, "Error " & return_value & ": Destination " &
                y_val.ToString("F7", CultureInfo.InvariantCulture) & " beyond limits of Y Axis. Valid range: " &
                telescope_driver_settings_.axis_settings_(1).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(1).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))
            Throw New ASCOM.InvalidValueException(
                "Destination beyond limits of Y Axis",
                y_val.ToString("F7", CultureInfo.InvariantCulture),
                telescope_driver_settings_.axis_settings_(1).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(1).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))

        ElseIf (return_value And telescope_error_t.ERROR_PARKED) <> 0 Then
            'when the telescope is parked, throw an exception.
            TL.LogMessage(command, "Error " & return_value & ": Telescope Parked")
            Throw New ASCOM.ParkedException()

        ElseIf (return_value And telescope_error_t.ERROR_HOMING) <> 0 Then
            'when the telescope is tracking, throw an exception.
            TL.LogMessage(command, "Error " & return_value & ": manual control currently active")
            Throw New ASCOM.InvalidOperationException("manual control active")
        End If

    End Sub

    'attempts to sync the Arduino's axes to the coordinates set in the transform_ object. 
    'do not call directly, should only be called by SyncToAltAz, SynctoCoordinates and SyncToTarget. 
    '@param command name of the calling function, for logging.
    Private Sub Sync(command As String)

        Dim x_val As Double = 0.0
        Dim y_val As Double = 0.0

        Dim response As String

        'recalculate.
        transform_.Refresh()

        'convert target coordinates depending on telescope mount type / alignment mode.
        If AlignmentMode = AlignmentModes.algAltAz Then
            x_val = transform_.AzimuthTopocentric
            y_val = transform_.ElevationTopocentric

            TL.LogMessage(command, "Sync to Az: " & utilities.DegreesToDMS(x_val) & " Alt: " & utilities.DegreesToDMS(y_val))

        ElseIf AlignmentMode = AlignmentModes.algPolar Or AlignmentMode = AlignmentModes.algGermanPolar Then
            x_val = transform_.RATopocentric
            y_val = transform_.DECTopocentric

            TL.LogMessage(command, "Sync to Ra: " &
                          utilities.HoursToHMS(x_val) & " Dec: " &
                          utilities.DegreesToDMS(y_val))
        End If

        Dim message As String
        Dim return_value As Short

        'throw an exception if the telescope is parked.
        If AtPark Then
            TL.LogMessage(command, "Error " & return_value & ": Telescope Parked")
            Throw New ASCOM.ParkedException()
        End If

        'send new x axis alignment.
        message = "ALIGNMENT,SET,X_AXIS,"
        message &= x_val.ToString("F7", CultureInfo.InvariantCulture)

        response = sendCommand(message)
        response = response.Replace("ALIGNMENT,", "").Trim

        return_value = CInt(response)

        logErrorCode(command, return_value, GetType(telescope_error_t))

        If return_value.Equals(0) Then
            TL.LogMessage(command, "Success")

        ElseIf (return_value And 128) <> 0 Then
            'when the telescope is parked, throw an exception.
            TL.LogMessage(command, "Error " & return_value & ": invalid axis")
            Throw New ASCOM.InvalidOperationException("invalid axis")
        End If

        'send new y axis alignment.
        message = "ALIGNMENT,SET,Y_AXIS,"
        message &= y_val.ToString("F7", CultureInfo.InvariantCulture)

        response = sendCommand(message)
        response = response.Replace("ALIGNMENT,", "").Trim

        return_value = CInt(response)

        logErrorCode(command, return_value, GetType(telescope_error_t))

        If return_value.Equals(0) Then
            TL.LogMessage(command, "Success")

        ElseIf (return_value And 128) <> 0 Then
            'when the telescope is parked, throw an exception.
            TL.LogMessage(command, "Error " & return_value & ": invalid axis")
            Throw New ASCOM.InvalidOperationException("invalid axis")
        End If

    End Sub

    'sends a move command to the Arduino, allowing slewing about a single axis.
    '@param axis telescope axis 
    '@param target target position in the axis' units (depends on mount type)
    '@return error code returned by Arduino as String.
    Private Function move(axis As String, target As String) As String
        'check user input
        If Not (axis Is "X_AXIS" Or axis Is "Y_AXIS" Or axis Is "Z_AXIS") Then
            Throw New ASCOM.InvalidValueException("Move: invalid axis " & axis)
        End If

        If Not IsNumeric(target) Then
            Throw New ASCOM.InvalidValueException("Move: " & target & " can not be evaluated as a number")
        End If

        Dim response As String

        response = sendCommand("MOVE,SET," & axis & "," & target)
        response = response.Replace("MOVE,", "").Trim

        Dim return_value = CUInt(response)

        logErrorCode("move", return_value, GetType(telescope_error_t))

        'Todo:
        'check the response.
        'if an error occured, the return value will not be 0.
        'possible return values are:
        ' MSB - - - - - - LSB
        '	x x x x x x x x
        '	| | | | | | | |
        '	| | | | | | | - target position beyond limits.
        '	| | | | | | - Not used.
        '	| | | | | -	telescope Is currently parked.
        '	| | | | - Not used.
        '	| | | - invalid axis given.
        '	| | - telescope Is currently homing.
        '	| - Not used.
        '	- Not used.
        If return_value.Equals(0) Then
            TL.LogMessage("move", "Success")

        ElseIf (return_value And telescope_error_t.ERROR_OUT_OF_BOUNDS_X) <> 0 Then
            'when a destination beyond the x axis limits has been given, throw an exception.
            TL.LogMessage(Command, "Error " & return_value & ": Destination " &
                target & " beyond limits of X Axis. Valid range: " &
                telescope_driver_settings_.axis_settings_(0).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(0).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))
            Throw New ASCOM.InvalidValueException(
                "Destination beyond limits of X Axis",
                target,
                telescope_driver_settings_.axis_settings_(0).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(0).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))

        ElseIf (return_value And telescope_error_t.ERROR_OUT_OF_BOUNDS_Y) <> 0 Then
            'when a destination beyond the y axis limits has been given, throw an exception.
            TL.LogMessage(Command, "Error " & return_value & ": Destination " &
                target & " beyond limits of Y Axis. Valid range: " &
                telescope_driver_settings_.axis_settings_(1).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(1).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))
            Throw New ASCOM.InvalidValueException(
                "Destination beyond limits of Y Axis",
                target,
                telescope_driver_settings_.axis_settings_(1).limit_lower_.ToString("F7", CultureInfo.InvariantCulture) & " - " &
                telescope_driver_settings_.axis_settings_(1).limit_upper_.ToString("F7", CultureInfo.InvariantCulture))

            'ElseIf (return_value And telescope_error_t.ERROR_OUT_OF_BOUNDS_X) <> 0 Then
            '    'when a destination beyond the axis limits has been given, throw an exception.
            '    TL.LogMessage("move", "Error " & CInt(response) & ": Destination beyond limits of Axis")
            '    Throw New ASCOM.InvalidValueException("Destination beyond limits of Axis")
            '
            'ElseIf (return_value And telescope_error_t.ERROR_PARKED) <> 0 Then
            '    'when the telescope is parked, throw an exception.
            '    TL.LogMessage("move", "Error " & CInt(response) & ": Telescope Parked")
            '    Throw New ASCOM.ParkedException()

        ElseIf (return_value And telescope_error_t.ERROR_INVALID_AXIS) <> 0 Then
            'when an invalid axis has been given, throw an exception.
            TL.LogMessage("move", "Error " & CInt(response) & ": invalid axis " & axis)
            Throw New ASCOM.InvalidValueException("invalid axis " & axis)

        ElseIf (return_value And telescope_error_t.ERROR_HOMING) <> 0 Then
            'when the telescope is currently homing, throw an exception.
            TL.LogMessage("move", "Error " & CInt(response) & ": telescope is currently homing")
            Throw New ASCOM.InvalidOperationException("telescope is currently homing")
        End If

        Return response
    End Function

    Private Function measureTemperature(sensor As String) As String
        Dim response As String
        response = sendCommand("TEMP_MEASURE,SET," & sensor)
        response = response.Replace("TEMP_MEASURE,", "").Trim

        Return response
    End Function

    Private Function hasTemperature(sensor As String) As String
        Dim response As String
        response = sendCommand("TEMP_MEASURE,GET," & sensor)
        response = response.Replace("TEMP_MEASURE,", "").Trim

        Return response
    End Function

    Private Function getTemperature(sensor As String) As String
        Dim response As String
        response = sendCommand("TEMP,GET," & sensor)
        response = response.Replace("TEMP,", "").Trim

        Return response
    End Function

    Private Function getSwitchState(axis As String) As String
        'check user input
        If Not (axis Is "X_AXIS" Or axis Is "Y_AXIS" Or axis Is "Z_AXIS") Then
            TL.LogMessage("getSwitchState", "Error: invalid axis " & axis)
            Throw New ASCOM.InvalidValueException("getSwitchState: invalid axis " & axis)
        End If

        Dim response As String
        response = sendCommand("SWITCH_STATE,GET," & axis)
        response = response.Replace("SWITCH_STATE,", "").Trim

        TL.LogMessage("getSwitchState", response)

        Return response
    End Function

    'analyzes error code and uses tracelogger to log error. 
    '@param command name of the calling function
    '@param errorCode error code returned by Arduino.
    '@param e enum to check error code against.
    '@return true if an error was logged, false otherwise.
    Private Function logErrorCode(command As String, errorCode As Integer, ByVal e As Type) As Boolean
        Dim return_value = False
        Dim eValues As Array = System.Enum.GetValues(e)

        For Each value In eValues
            If errorCode And value Then
                return_value = True
                TL.LogMessage(command, "Error code " & value & " - " & value.ToString())
            End If
        Next

        Return return_value
    End Function

#End Region

End Class
