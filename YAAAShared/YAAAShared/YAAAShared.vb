﻿'a module containing functions and constants to be used in all YAAADevice drivers.
Public Module YAAAShared
    'default directory for YAAADevice driver settings files
    Public YAAASETTINGS_DIRECTORY = "ASCOM/YAAADevice/"
    'load / save dialog filter for YAAADevice driver settings files
    Public YAAASETTINGS_FILE_DIALOG_FILTER = "eXtensible Markup Language file|*.xml"
    'default file name for YAAADevice driver settings files
    Public YAAASETTINGS_FILE_DEFAULT = "default.xml"

    'function to parse values stored in ASCOM profile store to a byte array
    Public Function hexToByteArray(hex As String, Optional size As Integer = -1) As Byte()
        Dim separators() As String = {" ", ","}

        'split string
        Dim strArr() As String = hex.Split(separators, StringSplitOptions.RemoveEmptyEntries)

        'convert whole string if no length was given
        If size = -1 Then
            size = strArr.Length
        End If

        Dim return_value(size - 1) As Byte

        'convert substrings to byte
        For i As Integer = 0 To size - 1
            return_value(i) = Convert.ToByte(strArr(i), 16)
        Next

        Return return_value
    End Function

    'function to convert a byte array to a string
    Public Function byteArrayToString(arr As Byte()) As String
        Dim return_value As String = ""

        For i As Integer = 0 To arr.Length() - 1
            return_value += "0x"
            return_value += Hex(arr(i))
            return_value += " "
        Next

        Return return_value
    End Function
End Module
