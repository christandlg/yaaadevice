﻿' a module containing functions to print and parse Arduino I/O pins
Module YAAAPins

    'parses a string to an Arduino I/O pin number.
    '@param pin pin number as string.
    '@return byte parsed I/O pin or 255 if parsing failed.
    Public Function parsePin(pin As String) As Byte
        Dim pin_number As Byte = 255
        Dim analog_pin As Boolean = False   'pin number was given as analog pin number

        'analog pin
        'check if the first letter is 'A' and there are 1 or 2 numbers following
        Dim match As RegularExpressions.Match = RegularExpressions.Regex.Match(pin, "A\d{1,2}$")
        If match.Success Then
            pin = pin.Replace("A", "")    'remove the "A" from the string to allow parsing
            analog_pin = True
        End If

        'digital pin
        'check if the forst letter is 'D', and there are numbers following
        match = RegularExpressions.Regex.Match(pin, "D\d{1,3}$")
        If match.Success Then
            pin = pin.Replace("D", "")    'remove the "D" from the string to allow parsing
        End If

        If Byte.TryParse(pin, pin_number) Then
            If analog_pin Then
                pin_number += 54    'analog pins start at pin number 54
            End If

            'no pin numbers > 69 exist, replace with 255 in this case.
            If pin_number > 69 Then
                pin_number = 255
            End If
        Else
            pin_number = 255        'default to 255 if parsing failed
        End If

        Return pin_number
    End Function

    ''resolves an Arduino I/O pin number. returns the original value if resolving failed.
    ''@param pin pin number as string.
    ''@return resolved pin number as string
    'Public Function resolvePinNumber(pin As String) As String
    '    Dim analog_pin As Boolean = False   'pin number was given as analog pin number
    '    Dim original_value = pin
    '
    '    'check if the first letter is 'A' and there are 1 or 2 numbers following
    '    Dim match As RegularExpressions.Match = RegularExpressions.Regex.Match(pin, "A\d{1,2}$")
    '    If match.Success Then
    '        'analog pin
    '        pin = pin.Replace("A", "")      'remove the "A" from the string
    '        analog_pin = True               'further processing necessary
    '    Else
    '        'check if the first letter is 'D', and there are 1 or 2 numbers following
    '        match = RegularExpressions.Regex.Match(pin, "D\d{1,3}$")
    '        If match.Success Then
    '            'digital pin
    '            pin = pin.Replace("D", "")    'remove the "D" from the string
    '        End If
    '    End If
    '
    '    'in case of an analog pin, calculate common pin number
    '    If analog_pin Then
    '        Dim pin_number As Byte = 255
    '
    '        'no analog input pins with index >= 16 exist
    '        If Byte.TryParse(pin, pin_number) AndAlso pin_number < 16 Then
    '            pin_number += 54    'analog pins start at pin number 54
    '            pin = pin_number.ToString
    '        Else
    '            pin = original_value
    '        End If
    '    End If
    '
    '    Return pin
    'End Function

    'converts an Arduino I/O pin number to a nice string.
    'param pin pin number
    '@return string representing pin
    Public Function printPin(pin As Byte) As String
        Dim return_value As String = ""

        If pin >= 54 And pin <= 69 Then
            return_value = "A" & (pin - 54).ToString
        Else
            return_value = pin.ToString()
        End If

        Return return_value
    End Function

End Module
