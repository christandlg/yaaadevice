﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorRainFC37
    Implements IUserControlSensor

    Enum input_mode_t
        DIGITAL = False
        ANALOG = True
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone

        ComboBoxInputMode.Items.AddRange([Enum].GetNames(GetType(UserControlSensorRainFC37.input_mode_t)))

        TextBoxPinNr.Text = YAAASharedClassLibrary.printPin(sensor.sensor_settings_.sensor_parameters_(0))

        If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorRainFC37.input_mode_t), sensor.sensor_settings_.sensor_parameters_(1))) Then
            ComboBoxInputMode.SelectedItem = UserControlSensorRainFC37.input_mode_t.DIGITAL.ToString
        Else
            ComboBoxInputMode.SelectedItem = [Enum].GetName(GetType(UserControlSensorRainFC37.input_mode_t), sensor.sensor_settings_.sensor_parameters_(1))
        End If

        CheckBoxInputInverted.Checked = CBool(sensor.sensor_settings_.sensor_parameters_(2))

        'get analog threshold value from sensor parameters array
        Dim buffer As Byte() = {&HFF, &HFF}
        Array.Copy(sensor_.sensor_settings_.sensor_parameters_, 4, buffer, 0, 2)

        'Arduino is big endian
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        TextBoxAnalogThreshold.Text = BitConverter.ToUInt16(buffer, 0)
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        With sensor_.sensor_settings_
            If YAAASharedClassLibrary.tryParsePin(TextBoxPinNr.Text, .sensor_parameters_(0)) Then
                TextBoxPinNr.BackColor = SystemColors.Window
            Else
                valid = False
                TextBoxPinNr.BackColor = Drawing.Color.Orange
            End If

            Dim input_mode As Byte = &H0
            If [Enum].Parse(GetType(UserControlSensorRainFC37.input_mode_t), ComboBoxInputMode.SelectedItem.ToString) = 0 Then
                input_mode = False
            Else
                input_mode = True
            End If
            .sensor_parameters_(1) = input_mode

            .sensor_parameters_(2) = Convert.ToByte(CheckBoxInputInverted.Checked)

            'read analog threshold value
            Dim threshold As UInt16 = 0
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxAnalogThreshold, threshold)

            Dim buffer As Byte() = BitConverter.GetBytes(threshold)

            'Arduino is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            Array.Copy(buffer, 0, .sensor_parameters_, 4, 2)
        End With


        sensor = sensor_.Clone

        Return valid
    End Function
End Class
