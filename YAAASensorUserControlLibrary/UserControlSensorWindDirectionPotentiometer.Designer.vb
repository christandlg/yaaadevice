﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorWindDirectionPotentiometer
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxPin = New System.Windows.Forms.TextBox()
        Me.TextBoxAngleOffset = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxAnalogMin = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxAnalogMax = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Pin"
        '
        'TextBoxPin
        '
        Me.TextBoxPin.Location = New System.Drawing.Point(109, 3)
        Me.TextBoxPin.Name = "TextBoxPin"
        Me.TextBoxPin.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPin.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.TextBoxPin, "Input pin, must be an Analog Input pin.")
        '
        'TextBoxAngleOffset
        '
        Me.TextBoxAngleOffset.Location = New System.Drawing.Point(109, 29)
        Me.TextBoxAngleOffset.Name = "TextBoxAngleOffset"
        Me.TextBoxAngleOffset.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAngleOffset.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.TextBoxAngleOffset, "Offset between sensor's north and true north, in degrees.")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(20, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Angle offset"
        '
        'TextBoxAnalogMin
        '
        Me.TextBoxAnalogMin.Location = New System.Drawing.Point(362, 3)
        Me.TextBoxAnalogMin.Name = "TextBoxAnalogMin"
        Me.TextBoxAnalogMin.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAnalogMin.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.TextBoxAnalogMin, "Minimum analog input value the sensor can produce")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(303, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Analog min"
        '
        'TextBoxAnalogMax
        '
        Me.TextBoxAnalogMax.Location = New System.Drawing.Point(362, 29)
        Me.TextBoxAnalogMax.Name = "TextBoxAnalogMax"
        Me.TextBoxAnalogMax.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAnalogMax.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.TextBoxAnalogMax, "Maximum analog input value this sensor can produce.")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(303, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Analog max"
        '
        'UserControlSensorWindDirectionPotentiometer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TextBoxAnalogMax)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBoxAnalogMin)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxAngleOffset)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxPin)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorWindDirectionPotentiometer"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxPin As TextBox
    Friend WithEvents TextBoxAngleOffset As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBoxAnalogMin As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxAnalogMax As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
