﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorTemperatureOneWireDS18
    Implements IUserControlSensor

    Enum onewire_sensor_family_t
        DS18S20 = &H10          'sensor Is a DS1820 Or DS18S20
        DS18B20 = &H28          'sensor Is a DS18B20
        DS1822 = &H22           'sensor Is a DS1822
    End Enum

    Enum onewire_sensor_resolution_t
        RESOLUTION_9_BIT = &H1F    'Default
        RESOLUTION_10_BIT = &H3F
        RESOLUTION_11_BIT = &H5F
        RESOLUTION_12_BIT = &H7F
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone()

        With sensor_.sensor_settings_
            'set default value for resolution if given resolution is invalid.
            If Not [Enum].IsDefined(GetType(UserControlSensorTemperatureOneWireDS18.onewire_sensor_resolution_t), CInt(.sensor_parameters_(9))) Then
                .sensor_parameters_(9) = UserControlSensorTemperatureOneWireDS18.onewire_sensor_resolution_t.RESOLUTION_9_BIT
            End If

            'add and set resolutions to resolutions combobox
            ComboBoxSensorResolution.Items.AddRange([Enum].GetNames(GetType(UserControlSensorTemperatureOneWireDS18.onewire_sensor_resolution_t)))
            ComboBoxSensorResolution.SelectedItem = [Enum].Parse(GetType(UserControlSensorTemperatureOneWireDS18.onewire_sensor_resolution_t), CInt(.sensor_parameters_(9))).ToString

            'copy temperature sensor address to address text box
            Dim address As Byte() = Array.CreateInstance(GetType(System.Byte), 8)
            Array.Copy(.sensor_parameters_, 0, address, 0, 8)
            TextBoxOneWireAddress.Text = YAAASharedClassLibrary.byteArrayToString(address)

            'set sensor family label context
            LabelSensorFamily.Text = getSensorFamilyString(.sensor_parameters_(0))
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        Dim addressArray As Byte() = YAAASharedClassLibrary.hexToByteArray(TextBoxOneWireAddress.Text)

        If addressArray.Length = 8 Then
            Array.Copy(addressArray, 0, sensor_.sensor_settings_.sensor_parameters_, 0, 8)   'copy address to bytes 0 - 7
        Else
            valid = False
            TextBoxOneWireAddress.BackColor = Drawing.Color.Orange
        End If

        'set byte 8 to 0 (RFU)
        'sensor_.sensor_settings_.sensor_parameters_(8) = 0

        'check resolution
        Dim resolution As Byte
        If [Enum].IsDefined(GetType(UserControlSensorTemperatureOneWireDS18.onewire_sensor_resolution_t), ComboBoxSensorResolution.SelectedItem.ToString) Then
            resolution = [Enum].Parse(GetType(UserControlSensorTemperatureOneWireDS18.onewire_sensor_resolution_t), ComboBoxSensorResolution.SelectedItem.ToString)
        Else
            resolution = UserControlSensorTemperatureOneWireDS18.onewire_sensor_resolution_t.RESOLUTION_9_BIT
        End If
        sensor_.sensor_settings_.sensor_parameters_(9) = resolution                      'save resolution to byte 9

        sensor = sensor_.Clone()

        Return valid
    End Function

    Private Function getSensorFamilyString(family As Byte) As String
        'check if the given family identifier is defined in the onewire_sensor_family_t enum
        If [Enum].IsDefined(GetType(UserControlSensorTemperatureOneWireDS18.onewire_sensor_family_t), CInt(family)) Then
            Return [Enum].Parse(GetType(UserControlSensorTemperatureOneWireDS18.onewire_sensor_family_t), CInt(family)).ToString
        Else
            Return "unknown"
        End If
    End Function

    Private Sub ButtonCheckAddress_Click(sender As Object, e As EventArgs) Handles ButtonCheckAddress.Click
        Dim address As Byte() = YAAASharedClassLibrary.hexToByteArray(TextBoxOneWireAddress.Text)

        LabelSensorFamily.Text = getSensorFamilyString(address(0))

        If checkOneWireAddress(address) Then
            ButtonCheckAddress.Text = "address valid"
        Else
            ButtonCheckAddress.Text = "address invalid"
        End If
    End Sub

    Private Function checkOneWireAddress(address As Byte()) As Boolean
        'return false if address length is not 8 bytes
        If address.Length <> 8 Then
            Return False
        End If

        'return false if CRC check fails
        If checkCRC(address) = False Then
            Return False
        End If

        'return false if sensor family is unkown
        If getSensorFamilyString(address(0)) = "unknown" Then
            Return False
        End If

        Return True
    End Function

    Private Function checkCRC(address As Byte()) As Boolean
        'the last byte of address contains the CRC to check against
        Dim testCRC As Byte = address(address.Length - 1)

        'copy address without CRC
        Dim addressNoCRC As Byte() = Array.CreateInstance(GetType(System.Byte), 7)
        Array.Copy(address, 0, addressNoCRC, 0, 7)

        'todo: implement CRC check



        Return True
    End Function
End Class
