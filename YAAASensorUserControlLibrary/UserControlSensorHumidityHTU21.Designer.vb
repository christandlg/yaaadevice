﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorHumidityHTU21
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBoxI2CAddress = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ComboBoxResolution = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TextBoxI2CAddress
        '
        Me.TextBoxI2CAddress.Location = New System.Drawing.Point(99, 3)
        Me.TextBoxI2CAddress.Name = "TextBoxI2CAddress"
        Me.TextBoxI2CAddress.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxI2CAddress.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.TextBoxI2CAddress, "I2C address of HTU21 sensor")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "I2C Address"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'ComboBoxResolution
        '
        Me.ComboBoxResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxResolution.DropDownWidth = 150
        Me.ComboBoxResolution.FormattingEnabled = True
        Me.ComboBoxResolution.Location = New System.Drawing.Point(99, 29)
        Me.ComboBoxResolution.Name = "ComboBoxResolution"
        Me.ComboBoxResolution.Size = New System.Drawing.Size(100, 21)
        Me.ComboBoxResolution.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.ComboBoxResolution, "Measurement Resolution of sensor. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "First group: Relative Humidity measurement re" &
        "solution. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Second group: Temperature measurement resolution." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Recommended value" &
        ": RESOLUTION_12BIT_14BIT")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Resolution"
        '
        'UserControlSensorHumidityHTU21
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBoxResolution)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBoxI2CAddress)
        Me.Name = "UserControlSensorHumidityHTU21"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBoxI2CAddress As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents ComboBoxResolution As ComboBox
    Friend WithEvents Label2 As Label
End Class
