﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorWindSpeedImpulse
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBox1HzWindSpeed = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxInputPin = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TextBoxCountPeriod = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxDebounceDelay = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TextBox1HzWindSpeed
        '
        Me.TextBox1HzWindSpeed.Location = New System.Drawing.Point(94, 29)
        Me.TextBox1HzWindSpeed.Name = "TextBox1HzWindSpeed"
        Me.TextBox1HzWindSpeed.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1HzWindSpeed.TabIndex = 15
        Me.ToolTip1.SetToolTip(Me.TextBox1HzWindSpeed, "Wind speed that causes the sensor to output exactly 1 pulse / second.")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "1Hz Wind Speed"
        '
        'TextBoxInputPin
        '
        Me.TextBoxInputPin.Location = New System.Drawing.Point(94, 3)
        Me.TextBoxInputPin.Name = "TextBoxInputPin"
        Me.TextBoxInputPin.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxInputPin.TabIndex = 13
        Me.ToolTip1.SetToolTip(Me.TextBoxInputPin, "Input pin, must support an external Interrupt.")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Pin"
        '
        'TextBoxCountPeriod
        '
        Me.TextBoxCountPeriod.Location = New System.Drawing.Point(362, 29)
        Me.TextBoxCountPeriod.Name = "TextBoxCountPeriod"
        Me.TextBoxCountPeriod.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxCountPeriod.TabIndex = 18
        Me.ToolTip1.SetToolTip(Me.TextBoxCountPeriod, "Measurement period in seconds. Value must be a positive Integer. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Values returne" &
        "d by the sensor are mean of values measured during the last measurement period." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Default: 10")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(273, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Count Period"
        '
        'TextBoxDebounceDelay
        '
        Me.TextBoxDebounceDelay.Location = New System.Drawing.Point(362, 3)
        Me.TextBoxDebounceDelay.Name = "TextBoxDebounceDelay"
        Me.TextBoxDebounceDelay.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxDebounceDelay.TabIndex = 20
        Me.ToolTip1.SetToolTip(Me.TextBoxDebounceDelay, "Software Debouncing delay in us. Set to 0 to deactivate." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If no external debounci" &
        "ng circuitry is in place a debouncing delay should be set to avoid incorrect pul" &
        "se counting. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Default: 10")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(273, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Debounce Delay"
        '
        'UserControlSensorWindSpeedImpulse
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TextBoxDebounceDelay)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBoxCountPeriod)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBox1HzWindSpeed)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxInputPin)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorWindSpeedImpulse"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox1HzWindSpeed As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBoxInputPin As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents TextBoxCountPeriod As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxDebounceDelay As TextBox
    Friend WithEvents Label4 As Label
End Class
