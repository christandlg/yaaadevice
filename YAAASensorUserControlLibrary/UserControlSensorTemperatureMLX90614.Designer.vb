﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorTemperatureMLX90614
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxZone = New System.Windows.Forms.ComboBox()
        Me.TextBoxI2CAddress = New System.Windows.Forms.TextBox()
        Me.TextBoxECC = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "I2C Address"
        '
        'ComboBoxZone
        '
        Me.ComboBoxZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxZone.FormattingEnabled = True
        Me.ComboBoxZone.Location = New System.Drawing.Point(341, 3)
        Me.ComboBoxZone.Name = "ComboBoxZone"
        Me.ComboBoxZone.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxZone.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.ComboBoxZone, "Temperature reported by sensor." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TEMP_AMBIENT: sensor's ambient temperature" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TEMP" &
        "_OBJ1: Zone 1 object temperature (recommended)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TEMP_OBJ2: Zone 2 object tempera" &
        "ture ")
        '
        'TextBoxI2CAddress
        '
        Me.TextBoxI2CAddress.Location = New System.Drawing.Point(85, 3)
        Me.TextBoxI2CAddress.Name = "TextBoxI2CAddress"
        Me.TextBoxI2CAddress.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxI2CAddress.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.TextBoxI2CAddress, "I2C Address of sensor")
        '
        'TextBoxECC
        '
        Me.TextBoxECC.Enabled = False
        Me.TextBoxECC.Location = New System.Drawing.Point(85, 29)
        Me.TextBoxECC.Name = "TextBoxECC"
        Me.TextBoxECC.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxECC.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.TextBoxECC, "Emissivity Correction Coefficient. Valid Range: 0.0 - 1.0; Default: 1.0")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(277, 6)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Zone"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "ECC"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'UserControlSensorTemperatureMLX90614
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxECC)
        Me.Controls.Add(Me.TextBoxI2CAddress)
        Me.Controls.Add(Me.ComboBoxZone)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorTemperatureMLX90614"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents ComboBoxZone As ComboBox
    Friend WithEvents TextBoxI2CAddress As TextBox
    Friend WithEvents TextBoxECC As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
