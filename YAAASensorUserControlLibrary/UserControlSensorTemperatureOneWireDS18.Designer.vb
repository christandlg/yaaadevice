﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorTemperatureOneWireDS18
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBoxSensorResolution = New System.Windows.Forms.ComboBox()
        Me.ButtonCheckAddress = New System.Windows.Forms.Button()
        Me.TextBoxOneWireAddress = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LabelSensorFamily = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(205, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Resolution"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Sensor Family"
        '
        'ComboBoxSensorResolution
        '
        Me.ComboBoxSensorResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxSensorResolution.FormattingEnabled = True
        Me.ComboBoxSensorResolution.Location = New System.Drawing.Point(268, 28)
        Me.ComboBoxSensorResolution.Name = "ComboBoxSensorResolution"
        Me.ComboBoxSensorResolution.Size = New System.Drawing.Size(194, 21)
        Me.ComboBoxSensorResolution.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.ComboBoxSensorResolution, "Sensor Resolution, affects temperature conversion duration. DS1820 and DS18S20 wi" &
        "ll always return values with 12bit resolution.")
        '
        'ButtonCheckAddress
        '
        Me.ButtonCheckAddress.Location = New System.Drawing.Point(323, 3)
        Me.ButtonCheckAddress.Name = "ButtonCheckAddress"
        Me.ButtonCheckAddress.Size = New System.Drawing.Size(139, 20)
        Me.ButtonCheckAddress.TabIndex = 9
        Me.ButtonCheckAddress.Text = "Check Address"
        Me.ButtonCheckAddress.UseVisualStyleBackColor = True
        '
        'TextBoxOneWireAddress
        '
        Me.TextBoxOneWireAddress.Location = New System.Drawing.Point(93, 3)
        Me.TextBoxOneWireAddress.Name = "TextBoxOneWireAddress"
        Me.TextBoxOneWireAddress.Size = New System.Drawing.Size(224, 20)
        Me.TextBoxOneWireAddress.TabIndex = 8
        Me.ToolTip1.SetToolTip(Me.TextBoxOneWireAddress, "Sensor's OneWire address, including Family Identifier and CRC.")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "OneWire Address"
        '
        'LabelSensorFamily
        '
        Me.LabelSensorFamily.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelSensorFamily.Location = New System.Drawing.Point(93, 28)
        Me.LabelSensorFamily.Name = "LabelSensorFamily"
        Me.LabelSensorFamily.Size = New System.Drawing.Size(100, 20)
        Me.LabelSensorFamily.TabIndex = 14
        Me.LabelSensorFamily.Text = "Label4"
        Me.LabelSensorFamily.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'UserControlSensorTemperatureOneWireDS18
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LabelSensorFamily)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ComboBoxSensorResolution)
        Me.Controls.Add(Me.ButtonCheckAddress)
        Me.Controls.Add(Me.TextBoxOneWireAddress)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorTemperatureOneWireDS18"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ComboBoxSensorResolution As ComboBox
    Friend WithEvents ButtonCheckAddress As Button
    Friend WithEvents TextBoxOneWireAddress As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents LabelSensorFamily As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
