﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorHumidityHTU21
    Implements IUserControlSensor

    Enum measurement_resolution_t
        RESOLUTION_12BIT_14BIT = &H0    '0b00000000
        RESOLUTION_08BIT_12BIT = &H1    '0b00000001
        RESOLUTION_10BIT_13BIT = &H80   '0b10000000
        RESOLUTION_11BIT_11BIT = &H81   '0b10000001
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        'ToolTip1.AutomaticDelay = 500
        'ToolTip1.AutoPopDelay = 50000

        sensor_ = sensor.Clone()

        TextBoxI2CAddress.Text = "0x" & Hex(sensor_.sensor_settings_.sensor_parameters_(0))

        ComboBoxResolution.Items.AddRange([Enum].GetNames(GetType(UserControlSensorHumidityHTU21.measurement_resolution_t)))
        '
        If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorHumidityHTU21.measurement_resolution_t), sensor_.sensor_settings_.sensor_parameters_(1))) Then
            ComboBoxResolution.SelectedItem = UserControlSensorHumidityHTU21.measurement_resolution_t.RESOLUTION_12BIT_14BIT.ToString
        Else
            ComboBoxResolution.SelectedItem = [Enum].GetName(GetType(UserControlSensorHumidityHTU21.measurement_resolution_t), sensor_.sensor_settings_.sensor_parameters_(1))
        End If
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxI2CAddress, sensor_.sensor_settings_.sensor_parameters_(0))

        sensor_.sensor_settings_.sensor_parameters_(1) = [Enum].Parse(GetType(UserControlSensorHumidityHTU21.measurement_resolution_t), ComboBoxResolution.SelectedItem.ToString)

        sensor = sensor_.Clone()

        Return valid
    End Function

End Class
