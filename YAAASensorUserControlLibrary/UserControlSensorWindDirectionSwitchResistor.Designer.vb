﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorWindDirectionSwitchResistor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBoxAngleOffset = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxPin = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'TextBoxAngleOffset
        '
        Me.TextBoxAngleOffset.Location = New System.Drawing.Point(95, 29)
        Me.TextBoxAngleOffset.Name = "TextBoxAngleOffset"
        Me.TextBoxAngleOffset.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAngleOffset.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.TextBoxAngleOffset, "Offset between sensor's north and true north, in degrees.")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Angle offset"
        '
        'TextBoxPin
        '
        Me.TextBoxPin.Location = New System.Drawing.Point(95, 3)
        Me.TextBoxPin.Name = "TextBoxPin"
        Me.TextBoxPin.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPin.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.TextBoxPin, "Input pin, must be an Analog Input pin.")
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(22, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Pin"
        '
        'UserControlSensorWindDirectionSwitchResistor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TextBoxAngleOffset)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxPin)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorWindDirectionSwitchResistor"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBoxAngleOffset As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBoxPin As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
