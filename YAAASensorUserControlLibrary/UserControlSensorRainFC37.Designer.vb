﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControlSensorRainFC37
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TextBoxPinNr = New System.Windows.Forms.TextBox()
        Me.ComboBoxInputMode = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxAnalogThreshold = New System.Windows.Forms.TextBox()
        Me.CheckBoxInputInverted = New System.Windows.Forms.CheckBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'TextBoxPinNr
        '
        Me.TextBoxPinNr.Location = New System.Drawing.Point(113, 3)
        Me.TextBoxPinNr.Name = "TextBoxPinNr"
        Me.TextBoxPinNr.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPinNr.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.TextBoxPinNr, "Sensor's input pin. ")
        '
        'ComboBoxInputMode
        '
        Me.ComboBoxInputMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxInputMode.FormattingEnabled = True
        Me.ComboBoxInputMode.Location = New System.Drawing.Point(113, 29)
        Me.ComboBoxInputMode.Name = "ComboBoxInputMode"
        Me.ComboBoxInputMode.Size = New System.Drawing.Size(100, 21)
        Me.ComboBoxInputMode.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Input Pin"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Input Mode"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(252, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Analog Threshold"
        '
        'TextBoxAnalogThreshold
        '
        Me.TextBoxAnalogThreshold.Location = New System.Drawing.Point(362, 29)
        Me.TextBoxAnalogThreshold.Name = "TextBoxAnalogThreshold"
        Me.TextBoxAnalogThreshold.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxAnalogThreshold.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.TextBoxAnalogThreshold, "Analog threshold value, only used with ANALOG input mode.")
        '
        'CheckBoxInputInverted
        '
        Me.CheckBoxInputInverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxInputInverted.Location = New System.Drawing.Point(255, 1)
        Me.CheckBoxInputInverted.Name = "CheckBoxInputInverted"
        Me.CheckBoxInputInverted.Size = New System.Drawing.Size(207, 24)
        Me.CheckBoxInputInverted.TabIndex = 6
        Me.CheckBoxInputInverted.Text = "Input Inverted"
        Me.ToolTip1.SetToolTip(Me.CheckBoxInputInverted, "False: digital high / analog value above threshold value is 'wet'." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "True: digital" &
        " low / analog value below threshold is 'wet'." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        Me.CheckBoxInputInverted.UseVisualStyleBackColor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'UserControlSensorRainFC37
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.CheckBoxInputInverted)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxAnalogThreshold)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ComboBoxInputMode)
        Me.Controls.Add(Me.TextBoxPinNr)
        Me.Name = "UserControlSensorRainFC37"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBoxPinNr As TextBox
    Friend WithEvents ComboBoxInputMode As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxAnalogThreshold As TextBox
    Friend WithEvents CheckBoxInputInverted As CheckBox
    Friend WithEvents ToolTip1 As ToolTip
End Class
