﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorTemperatureMLX90614
    Implements IUserControlSensor

    Enum temperature_t
        TEMP_AMBIENT = &H6                 'ambient temperature
        TEMP_OBJ1 = &H7                    'channel 1 linearized temperature
        TEMP_OBJ2 = &H8                    'channel 2 linearized temperature
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ComboBoxZone.Items.AddRange([Enum].GetNames(GetType(UserControlSensorTemperatureMLX90614.temperature_t)))
    End Sub

    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone

        With sensor_.sensor_settings_
            TextBoxI2CAddress.Text = "0x" & Hex(.sensor_parameters_(0))

            Dim buffer As Byte() = {&HFF, &HFF}
            Array.Copy(.sensor_parameters_, 2, buffer, 0, 2)

            'Arduino is big endian
            If BitConverter.IsLittleEndian Then
                Array.Reverse(buffer)
            End If

            TextBoxECC.Text = CDbl(BitConverter.ToUInt16(buffer, 0)) / 65535.0

            If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorTemperatureMLX90614.temperature_t), sensor_.sensor_settings_.sensor_parameters_(1))) Then
                ComboBoxZone.SelectedItem = UserControlSensorTemperatureMLX90614.temperature_t.TEMP_OBJ1.ToString
            Else
                ComboBoxZone.SelectedItem = [Enum].GetName(GetType(UserControlSensorTemperatureMLX90614.temperature_t), sensor_.sensor_settings_.sensor_parameters_(1))
            End If

            'Throw New System.NotImplementedException()
        End With
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True
        Dim ecc As Double = 0.0
        Dim buffer As Byte() = {&HFF, &HFF}

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxECC, ecc)

        If ecc < 0.0 Or ecc > 1.0 Then
            ecc = 1.0
            valid = False
            TextBoxECC.BackColor = Drawing.Color.Orange
        Else
            TextBoxECC.BackColor = SystemColors.Window
        End If

        'represent emissivity correction coefficient as unsigned 16bit integer
        buffer = BitConverter.GetBytes(CUShort(ecc * 65535.0))

        'reverse byte order if we are on a little-endian system (Arduino is big-endian)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(buffer)
        End If

        With sensor_.sensor_settings_
            valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxI2CAddress, .sensor_parameters_(0))

            .sensor_parameters_(1) = [Enum].Parse(GetType(UserControlSensorTemperatureMLX90614.temperature_t), ComboBoxZone.SelectedItem.ToString)

            Array.Copy(buffer, 0, .sensor_parameters_, 2, 2)
        End With

        sensor = sensor_.Clone

        Return valid
    End Function
End Class
