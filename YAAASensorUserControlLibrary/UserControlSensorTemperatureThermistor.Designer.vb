﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UserControlSensorTemperatureThermistor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxInputPin = New System.Windows.Forms.TextBox()
        Me.TextBoxPullupResistor = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxRefTemperature = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxRefResistance = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBoxCoefficient = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Input Pin"
        '
        'TextBoxInputPin
        '
        Me.TextBoxInputPin.Location = New System.Drawing.Point(69, 3)
        Me.TextBoxInputPin.Name = "TextBoxInputPin"
        Me.TextBoxInputPin.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxInputPin.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.TextBoxInputPin, "Sensor's input pin. Must be an analog input pin.")
        '
        'TextBoxPullupResistor
        '
        Me.TextBoxPullupResistor.Location = New System.Drawing.Point(69, 29)
        Me.TextBoxPullupResistor.Name = "TextBoxPullupResistor"
        Me.TextBoxPullupResistor.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxPullupResistor.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Pullup Res."
        '
        'TextBoxRefTemperature
        '
        Me.TextBoxRefTemperature.Location = New System.Drawing.Point(276, 3)
        Me.TextBoxRefTemperature.Name = "TextBoxRefTemperature"
        Me.TextBoxRefTemperature.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxRefTemperature.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.TextBoxRefTemperature, "Reference Temperature")
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(155, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Reference Temperature"
        '
        'TextBoxRefResistance
        '
        Me.TextBoxRefResistance.Location = New System.Drawing.Point(276, 29)
        Me.TextBoxRefResistance.Name = "TextBoxRefResistance"
        Me.TextBoxRefResistance.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxRefResistance.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.TextBoxRefResistance, "Resistance of thermistor at reference temperature.")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(155, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Reference Resistance"
        '
        'TextBoxCoefficient
        '
        Me.TextBoxCoefficient.Location = New System.Drawing.Point(382, 3)
        Me.TextBoxCoefficient.Name = "TextBoxCoefficient"
        Me.TextBoxCoefficient.Size = New System.Drawing.Size(80, 20)
        Me.TextBoxCoefficient.TabIndex = 9
        Me.ToolTip1.SetToolTip(Me.TextBoxCoefficient, "Steinhart-Hart parameter B")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(362, 6)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(14, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "B"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 50000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'UserControlSensorTemperatureThermistor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.TextBoxCoefficient)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TextBoxRefResistance)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBoxRefTemperature)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxPullupResistor)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxInputPin)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UserControlSensorTemperatureThermistor"
        Me.Size = New System.Drawing.Size(465, 60)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents TextBoxInputPin As TextBox
    Friend WithEvents TextBoxPullupResistor As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBoxRefTemperature As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxRefResistance As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBoxCoefficient As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
