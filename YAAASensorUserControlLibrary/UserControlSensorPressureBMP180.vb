﻿'Imports ASCOM.YAAADevice

Public Class UserControlSensorPressureBMP180
    Implements IUserControlSensor

    Enum accuracy_mode_t
        MODE_ULP = &H34         'ultra low power mode, 1 sample
        MODE_STD = &H74         'standard mode, 2 samples
        MODE_HR = &HB4          'high resolution mode, 4 samples
        MODE_UHR = &HF4         'ultra high resolution mode, 8 samples
    End Enum

    Private sensor_ As YAAASharedClassLibrary.YAAASensor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub
    Public Sub setSensor(sensor As YAAASharedClassLibrary.YAAASensor) Implements IUserControlSensor.setSensor
        sensor_ = sensor.Clone

        ComboBoxMode.Items.AddRange([Enum].GetNames(GetType(UserControlSensorPressureBMP180.accuracy_mode_t)))
        '
        If String.IsNullOrEmpty([Enum].GetName(GetType(UserControlSensorPressureBMP180.accuracy_mode_t), sensor_.sensor_settings_.sensor_parameters_(1))) Then
            ComboBoxMode.SelectedItem = UserControlSensorPressureBMP180.accuracy_mode_t.MODE_UHR.ToString
        Else
            ComboBoxMode.SelectedItem = [Enum].GetName(GetType(UserControlSensorPressureBMP180.accuracy_mode_t), sensor_.sensor_settings_.sensor_parameters_(1))
        End If

        TextBoxI2CAddress.Text = "0x" & Hex(sensor_.sensor_settings_.sensor_parameters_(0))
    End Sub

    Public Function getSensor(ByRef sensor As YAAASharedClassLibrary.YAAASensor) As Boolean Implements IUserControlSensor.getSensor
        Dim valid As Boolean = True

        valid = valid And YAAASharedClassLibrary.getTextBoxValue(TextBoxI2CAddress, sensor_.sensor_settings_.sensor_parameters_(0))

        sensor_.sensor_settings_.sensor_parameters_(1) = [Enum].Parse(GetType(UserControlSensorPressureBMP180.accuracy_mode_t), ComboBoxMode.SelectedItem.ToString)

        sensor = sensor_.Clone()

        Return valid
    End Function
End Class
