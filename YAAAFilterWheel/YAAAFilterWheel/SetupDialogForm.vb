Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports ASCOM.Utilities
Imports ASCOM.YAAADevice
Imports System.Drawing

<ComVisible(False)>
Public Class SetupDialogForm

    Private filter_wheel_ As FilterWheel

    Private user_controls_() As UserControlTemperatureSensor

    Private nfi_ As System.Globalization.NumberFormatInfo = New System.Globalization.CultureInfo("en-US", False).NumberFormat
    Private baud_rates_ As String() = {"4800", "9600", "19200", "28800", "38400", "57600", "115200"}

    Private filters_() As FilterWheel.filterSettings

    Private is_initializing_ As Boolean

    Public Sub New(fw As FilterWheel)

        ' This call is required by the designer.
        is_initializing_ = True
        InitializeComponent()
        is_initializing_ = False

        ' Add any initialization after the InitializeComponent() call.
        filter_wheel_ = fw

        updateStatusStrip()
    End Sub

    Private Sub updateStatusStrip()
        Dim current_settings As FilterWheel.filterWheelDriverSettings = filter_wheel_.getDriverSettings()

        Dim destination As String = ""

        If filter_wheel_.Connected Then
            Dim message As String = "Connected to: "
            If current_settings.connection_type_ = FilterWheel.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Disconnect")
        Else
            ToolStripSplitButton1.Text = "Disconnected"
            ToolStripSplitButton1.DropDownItems.Clear()
            ToolStripSplitButton1.DropDownItems.Add("Connect")
        End If

        ToolStripStatusLabel2.Text = ""
        ToolStripStatusLabel2.Spring = True

        ToolStripStatusLabel1.Alignment = ToolStripItemAlignment.Right
        ToolStripStatusLabel1.Text = "Ready"
    End Sub

    Private Sub connect(value As Boolean)
        Dim current_settings As FilterWheel.filterWheelDriverSettings = filter_wheel_.getDriverSettings()

        current_settings.com_port_ = ComboBoxComPorts.SelectedItem.ToString
        current_settings.baud_rate_ = ComboBoxBaudRate.SelectedItem.ToString

        current_settings.ip_address_ = TextBoxIPAddress.Text
        current_settings.port_ = CInt(TextBoxPort.Text)

        If RadioButtonSerial.Checked Then
            current_settings.connection_type_ = FilterWheel.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            current_settings.connection_type_ = FilterWheel.connection_type_t.NETWORK
        End If

        filter_wheel_.setDriverSettings(current_settings)

        Dim destination As String = ""

        If value Then
            Dim message As String = "Attempting to connect to: "
            If current_settings.connection_type_ = FilterWheel.connection_type_t.SERIAL Then
                destination = current_settings.com_port_.ToString() & " | "
                destination &= current_settings.baud_rate_.ToString() & "b/s"
            Else
                destination = current_settings.ip_address_ & ":" & current_settings.port_
            End If

            ToolStripSplitButton1.Text = message & destination
            Application.DoEvents()

            'connect to the arduino and read values from the EEPROM.
            filter_wheel_.Connected = True

        Else
            ToolStripSplitButton1.Text = "Disconnecting..."
            Application.DoEvents()

            filter_wheel_.Connected = False
        End If

        If filter_wheel_.Connected Then
            ToolStripSplitButton1.Text = "Connected to: " & destination
        Else
            ToolStripSplitButton1.Text = "Disconnected"
        End If

        Application.DoEvents()
    End Sub

    Private Sub AddSensorControls(Optional useEEPROM As Boolean = False)
        'clear existing user controls from FlowLayoutPanel
        If Not user_controls_ Is Nothing Then
            For Each user_control In user_controls_
                user_control.Dispose()
            Next
        End If

        'set flowLayoutPanel properties
        With FlowLayoutPanelTemperatureSensors
            '.Location = New Drawing.Point(6, 74)
            '.Size = New Drawing.Size(511, 219)
            .FlowDirection = FlowDirection.TopDown
        End With

        'use settings read from EEPROM if requested
        Dim current_settings As FilterWheel.filterWheelDriverSettings = filter_wheel_.getDriverSettings(useEEPROM)

        'redim array to hold FilterWheel.numTempSensors user control objects.
        ReDim user_controls_(current_settings.filter_wheel_settings_.temperature_sensors_.Length - 1)

        'add entries for each temperature sensor
        For i As Integer = 0 To user_controls_.Length - 1
            'add control for temperature sensor.
            user_controls_(i) = New UserControlTemperatureSensor(current_settings.filter_wheel_settings_.temperature_sensors_(i), i)

            'add control to FlowLayoutPanel
            FlowLayoutPanelTemperatureSensors.Controls.Add(user_controls_(i))
        Next
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click ' OK button event handler
        FilterWheel.filterWheelDriverSettings.trace_state_ = chkTrace.Checked

        ' Persist new values of user settings to the ASCOM profile
        If Not WriteProfileSettings() Then
            Exit Sub
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click 'Cancel button event handler
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ShowAscomWebPage(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.DoubleClick, PictureBox1.Click
        ' Click on ASCOM logo event handler
        Try
            System.Diagnostics.Process.Start("http://ascom-standards.org/")
        Catch noBrowser As System.ComponentModel.Win32Exception
            If noBrowser.ErrorCode = -2147467259 Then
                MessageBox.Show(noBrowser.Message)
            End If
        Catch other As System.Exception
            MessageBox.Show(other.Message)
        End Try
    End Sub

    Private Function WriteProfileSettings() As Boolean
        Dim success As Boolean = True

        ToolStripStatusLabel1.Text = "Writing Profile Settings"
        Application.DoEvents()
        Try

            filter_wheel_.setDriverSettings(getSettingsFromMask())

            filter_wheel_.WriteProfile()
        Catch ex As Exception
            'when an error occurred during reading the settings from the input fields, display an
            'error message and do not close the window.
            MsgBox("Invalid values detected, some settings have not been saved. Check all input fields with orange background and try again.", MsgBoxStyle.Critical)
            success = False
        End Try

        updateStatusStrip()

        Return success
    End Function

    Private Sub ReadProfileSettings()
        ToolStripStatusLabel1.Text = "Reading Profile Settings"
        Application.DoEvents()

        ' Retrieve current values of user settings from the ASCOM Profile
        setMaskToSettings(filter_wheel_.getDriverSettings())

        updateStatusStrip()
    End Sub

    Private Sub SetupDialogForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load ' Form load event handler

        With DataGridViewFilters
            .RowHeadersVisible = False

            .ColumnCount = 5

            With .Columns(0)
                .Name = "#"
                .DisplayIndex = 0
                .Width = 20
            End With

            With .Columns(1)
                .Name = "Name"
                .DisplayIndex = 1
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End With

            With .Columns(2)
                .Name = "Type"
                .DisplayIndex = 2
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End With

            With .Columns(3)
                .Name = "Focus Offset"
                .DisplayIndex = 3
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End With

            With .Columns(4)
                .Name = "Position Offset"
                .DisplayIndex = 4
                .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            End With

            .Width = Panel1.ClientSize.Width
        End With


        chkTrace.Checked = FilterWheel.filterWheelDriverSettings.trace_state_

        ' set the list of com ports to those that are currently available
        ComboBoxComPorts.Items.Clear()
        ComboBoxComPorts.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames())

        'initialize comboboxes
        ComboBoxStepType.Items.AddRange([Enum].GetNames(GetType(FilterWheel.step_type_t)))
        ComboBoxMotorDriver.Items.AddRange([Enum].GetNames(GetType(FilterWheel.motor_driver_t)))

        ComboBoxFilterHolderType.Items.AddRange([Enum].GetNames(GetType(FilterWheel.filter_holder_type_t)))

        ' Retrieve current values of user settings from the ASCOM Profile 
        ReadProfileSettings()
    End Sub

    Private Sub ButtonReadEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonReadEEPROM.Click
        'when calling ReadEEPROM() current settings stored in the ASCOM profile store are
        'not overwritten immediately. the settings read from the arduino are only accepted 
        'when the user clicks "ok" or "write EEPROM", otherwise the changes are discarded.

        Dim current_settings As FilterWheel.filterWheelDriverSettings = filter_wheel_.getDriverSettings()

        Try
            connect(True)

            If filter_wheel_.Connected Then
                ToolStripStatusLabel1.Text = "Reading from EEPROM..."
                Application.DoEvents()

                filter_wheel_.readEEPROM()

                setMaskToSettings(filter_wheel_.getDriverSettings(True))
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'catch any other exceptions just in case
            Dim err_msg As String = ""
            err_msg &= "Error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub

    Private Sub ButtonWriteEEPROM_Click(sender As Object, e As EventArgs) Handles ButtonWriteEEPROM.Click
        'save values in ASCOM profile.
        If Not WriteProfileSettings() Then
            Exit Sub
        End If

        'check if user wants to force EEPROM Write.
        filter_wheel_.EEPROM_force_write_ = CheckBoxEEPROMForceWrite.Checked

        Try
            'connect to the arduino and send over any data the user has changed.
            connect(True)

            'if the YAAADevice is connected, try to write the EEPROM contents
            If filter_wheel_.Connected Then
                ToolStripStatusLabel1.Text = "Writing to EEPROM..."
                Application.DoEvents()

                filter_wheel_.writeEEPROM()
            End If
        Catch ex As DriverException     'ASCOM.DriverAccess exceptions are returned by Connected Property
            Dim err_msg As String = ""
            err_msg &= "Connecting to YAAADevice failed. " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex As Exception           'other exceptions are returned by function ReadEEPROM()
            Dim err_msg As String = ""
            err_msg &= "Unknown error occurred: " & Environment.NewLine
            err_msg &= ex.Message & Environment.NewLine
            If Not ex.InnerException Is Nothing Then    'add inner exception message if available
                err_msg &= ex.InnerException.Message & Environment.NewLine
            End If

            MessageBox.Show(err_msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'reset Force EEPROM Write to default.
        filter_wheel_.EEPROM_force_write_ = False
        CheckBoxEEPROMForceWrite.Checked = False

        updateStatusStrip()
    End Sub

    Private Sub NumericUpDownNrFilters_ValueChanged(sender As Object, e As EventArgs) Handles NumericUpDownNrFilters.ValueChanged
        If is_initializing_ Then
            Exit Sub
        End If

        Dim number_of_filters As Short = NumericUpDownNrFilters.Value

        'resize filters array
        ReDim Preserve filters_(number_of_filters - 1)

        'TODO use non-default constructor when resizing array

        'reinitialize filter list.
        InitializeFilterList()
    End Sub

    Public Sub ChangeFilterEntry(index As Short, name As String, offset As Integer)
        'filters(index).name = name
        'filters(index).focus_offset = offset
    End Sub

    Private Sub InitializeFilterList()

        With DataGridViewFilters
            .Rows.Clear()       'delete all rows in the DataGridView.

            'repopulate DataGridView with filter data.
            For i As Integer = 0 To filters_.Length - 1

                Dim row As String() = {
                    i,
                    filters_(i).name_,
                    filters_(i).type_.ToString,
                    filters_(i).focus_offset_,
                    filters_(i).position_offset_
                    }

                'add filter data to DataGridView.
                .Rows.Add(row)
                .Rows(i).Cells(0).ReadOnly = True
            Next

        End With
    End Sub

    Private Function readFilterList() As FilterWheel.filterSettings()
        Dim number_of_filters = DataGridViewFilters.RowCount
        Dim filters(number_of_filters - 1) As FilterWheel.filterSettings

        For i As Integer = 0 To filters.Length - 1
            With DataGridViewFilters.Rows(i)
                Dim filter_name = .Cells(1).Value.ToString
                Dim filter_type = .Cells(2).Value.ToString
                Dim filter_focus_offset = CInt(.Cells(3).Value)
                Dim filter_position_offset = CInt(.Cells(4).Value)

                filters(i) = New FilterWheel.filterSettings(filter_name, filter_type, 0, filter_focus_offset, filter_position_offset)
            End With
        Next

        Return filters
    End Function

    'TODO remove?
    Private Sub DataGridViewFilters_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs)
        Dim row As Integer = e.RowIndex
        Dim column As Integer = e.ColumnIndex

        'Select Case column
        '    Case 1
        '        filters(row).name = DataGridViewFilters.Rows(row).Cells(column).Value
        '    Case 2
        '        filters(row).focus_offset = CInt(DataGridViewFilters.Rows(row).Cells(column).Value)
        'End Select

    End Sub

    Private Sub ComboBoxMotorDriver_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxMotorDriver.SelectedIndexChanged
        Select Case ComboBoxMotorDriver.SelectedItem.ToString
            Case FilterWheel.motor_driver_t.ADAFRUIT_MOTORSHIELD_V2.ToString
                'reenable adafruit motor shield options.
                ComboBoxI2CAddress.Enabled = True
                ComboBoxTerminal.Enabled = True

                'disable other options.
                TextBoxPin1.Enabled = False
                CheckBoxPin1Inverted.Enabled = False
                TextBoxPin2.Enabled = False
                CheckBoxPin2Inverted.Enabled = False
                TextBoxPin3.Enabled = False
                CheckBoxPin3Inverted.Enabled = False
                TextBoxPin4.Enabled = False
                CheckBoxPin4Inverted.Enabled = False
                TextBoxPin5.Enabled = False
                CheckBoxPin5Inverted.Enabled = False
                TextBoxPin6.Enabled = False
                CheckBoxPin6Inverted.Enabled = False

            Case FilterWheel.motor_driver_t.FOUR_WIRE_STEPPER.ToString
                'reenable four wire stepper options.
                TextBoxPin1.Enabled = True
                CheckBoxPin1Inverted.Enabled = True
                TextBoxPin2.Enabled = True
                CheckBoxPin2Inverted.Enabled = True
                TextBoxPin3.Enabled = True
                CheckBoxPin3Inverted.Enabled = True
                TextBoxPin4.Enabled = True
                CheckBoxPin4Inverted.Enabled = True

                'disable other options.
                ComboBoxI2CAddress.Enabled = False
                ComboBoxTerminal.Enabled = False

                TextBoxPin5.Enabled = False
                CheckBoxPin5Inverted.Enabled = False
                TextBoxPin6.Enabled = False
                CheckBoxPin6Inverted.Enabled = False

            Case FilterWheel.motor_driver_t.DRIVER_A4988.ToString, FilterWheel.motor_driver_t.DRIVER_DRV8825.ToString
                'disable adafruit motor shield options.
                ComboBoxI2CAddress.Enabled = False
                ComboBoxTerminal.Enabled = False

                'enable other options.
                TextBoxPin1.Enabled = True
                CheckBoxPin1Inverted.Enabled = True
                TextBoxPin2.Enabled = True
                CheckBoxPin2Inverted.Enabled = True
                TextBoxPin3.Enabled = True
                CheckBoxPin3Inverted.Enabled = True
                TextBoxPin4.Enabled = True
                CheckBoxPin4Inverted.Enabled = True
                TextBoxPin5.Enabled = True
                CheckBoxPin5Inverted.Enabled = True
                TextBoxPin6.Enabled = True
                CheckBoxPin6Inverted.Enabled = True
        End Select
    End Sub

    Private Sub ButtonReadSwitchState_Click(sender As Object, e As EventArgs) Handles ButtonReadSwitchState.Click
        Dim switch_state As Short

        Try
            connect(True)

            ToolStripStatusLabel1.Text = "Reading Switch State"
            Application.DoEvents()

            switch_state = CShort(filter_wheel_.Action("Get_Switch_State", ""))

            CheckBoxESLTriggered.Checked = ((switch_state And 1) <> 0)
            CheckBoxESUTriggered.Checked = ((switch_state And 2) <> 0)
            CheckBoxHSTriggered.Checked = ((switch_state And 4) <> 0)

            'filter_wheel_.Connected = False
        Catch ex As Exception
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical)
        End Try

        updateStatusStrip()
    End Sub

    Private Function getTextBoxValue(Of T As IConvertible)(text_box As TextBox, ByRef value As T) As Boolean
        'reset text box background color
        text_box.BackColor = TextBox.DefaultBackColor

        Try
            'parse text_box.Text to T
            value = DirectCast(Convert.ChangeType(text_box.Text, GetType(T)), T)
            Return True
        Catch ex As Exception
            'change text box color to indicate incorrect value
            text_box.BackColor = Drawing.Color.Orange
            Return False
        End Try
    End Function

    Private Function getDataGridViewCellValue(Of T As IconConverter)(cell As DataGridViewCell, ByRef value As T) As Boolean
        'reset cell background color
        'cell.BackColor = DataGridViewCell.DefaultBackColor

        Try
            'parse text_box.Text to T
            value = DirectCast(Convert.ChangeType(cell.Value.ToString, GetType(T)), T)
            Return True
        Catch ex As Exception
            'change text box color to indicate incorrect value
            'cell.BackColor = Drawing.Color.Orange
            Return False
        End Try
    End Function

    Private Sub DataGridViewFilters_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewFilters.CellDoubleClick
        Dim filter_index = e.RowIndex

        'todo: spawn edit filter window
        Dim dialog = New EditFilter(filters_(filter_index))

        Dim result As DialogResult = dialog.ShowDialog(Me)

        If result = Windows.Forms.DialogResult.OK Then
            'save return value to filters array
            filters_(filter_index) = dialog.Filter
        End If

        'reinitialize filter list
        InitializeFilterList()
    End Sub

    Private Sub setMaskToSettings(settings As FilterWheel.filterWheelDriverSettings)
        chkTrace.Checked = FilterWheel.filterWheelDriverSettings.trace_state_

        With settings
            If Not ComboBoxComPorts.Items.Contains(.com_port_) Then
                ComboBoxComPorts.Items.Add(.com_port_)
            End If

            ComboBoxComPorts.SelectedItem = .com_port_.ToString
            ComboBoxBaudRate.SelectedItem = .baud_rate_.ToString

            TextBoxIPAddress.Text = .ip_address_
            TextBoxPort.Text = .port_.ToString

            RadioButtonSerial.Checked = (.connection_type_ = FilterWheel.connection_type_t.SERIAL)
            RadioButtonNetwork.Checked = (.connection_type_ = FilterWheel.connection_type_t.NETWORK)
        End With

        With settings.filter_wheel_settings_
            'exchange values in the form for values read from EEPROM.
            TextBoxAccel.Text = .acceleration_
            TextBoxMaxSpeed.Text = .max_speed_
            TextBoxGearRatio.Text = .gear_ratio_
            TextBoxStepsRev.Text = .number_of_steps_
            ComboBoxStepType.SelectedItem = .step_type_.ToString

            CheckBoxHSA.Checked = .home_switch_avail_
            TextBoxHSN.Text = YAAASharedClassLibrary.printPin(.home_switch_nr_)
            CheckBoxSAH.Checked = .startup_auto_home_
            CheckBoxASR.Checked = .automatic_stepper_release_
            TextBoxHTO.Text = .home_timout_

            CheckBoxEndStopLowerEnabled.Checked = .endstop_lower_enabled_
            CheckBoxEndstopLowerInverted.Checked = .endstop_lower_inverted_
            TextBoxEndstopLowerPin.Text = YAAASharedClassLibrary.printPin(.endstop_lower_)
            CheckBoxEndstopUpperEnabled.Checked = .endstop_upper_enabled_
            CheckBoxEndstopUpperInverted.Checked = .endstop_upper_inverted_
            TextBoxEndstopUpperPin.Text = YAAASharedClassLibrary.printPin(.endstop_upper_)
            CheckBoxEndstopHardStop.Checked = .endstop_hard_stop_

            CheckBoxLowerLimitEnabled.Checked = .limit_lower_enabled_
            TextBoxLowerLimit.Text = .limit_lower_
            CheckBoxUpperLimitEnabled.Checked = .limit_upper_enabled_
            TextBoxUpperLimit.Text = .limit_upper_
            CheckBoxHardStopLimit.Checked = .limit_hard_stop_


            ComboBoxMotorDriver.SelectedItem = .motor_driver_.ToString

            ComboBoxI2CAddress.SelectedItem = "0x" & Hex(.i2c_address_)
            ComboBoxTerminal.SelectedItem = .terminal_.ToString

            TextBoxPin1.Text = YAAASharedClassLibrary.printPin(.pin_1_)
            CheckBoxPin1Inverted.Checked = .pin_1_inverted_
            TextBoxPin2.Text = YAAASharedClassLibrary.printPin(.pin_2_)
            CheckBoxPin2Inverted.Checked = .pin_2_inverted_
            TextBoxPin3.Text = YAAASharedClassLibrary.printPin(.pin_3_)
            CheckBoxPin3Inverted.Checked = .pin_3_inverted_
            TextBoxPin4.Text = YAAASharedClassLibrary.printPin(.pin_4_)
            CheckBoxPin4Inverted.Checked = .pin_4_inverted_
            TextBoxPin5.Text = YAAASharedClassLibrary.printPin(.pin_5_)
            CheckBoxPin5Inverted.Checked = .pin_5_inverted_
            TextBoxPin6.Text = YAAASharedClassLibrary.printPin(.pin_6_)
            CheckBoxPin6Inverted.Checked = .pin_6_inverted_


            ComboBoxI2CAddress.SelectedItem = "0x" & Hex$(.i2c_address_)
            ComboBoxTerminal.SelectedItem = .terminal_.ToString

            ComboBoxFilterHolderType.SelectedIndex = .filter_holder_type_

            filters_ = .filters_.Clone
            NumericUpDownNrFilters.Value = filters_.Length

            InitializeFilterList()
        End With

        'add temperature sensor controls
        AddSensorControls()
    End Sub

    Private Function getSettingsFromMask() As FilterWheel.filterWheelDriverSettings
        Dim valid As Boolean = True
        Dim return_value As FilterWheel.filterWheelDriverSettings = New FilterWheel.filterWheelDriverSettings(True)

        FilterWheel.filterWheelDriverSettings.trace_state_ = chkTrace.Checked

        If Not ComboBoxComPorts.SelectedItem Is Nothing Then
            return_value.com_port_ = ComboBoxComPorts.SelectedItem.ToString
        End If

        return_value.baud_rate_ = CInt(ComboBoxBaudRate.SelectedItem.ToString)

        valid = valid And getTextBoxValue(TextBoxIPAddress, return_value.ip_address_)
        valid = valid And getTextBoxValue(TextBoxPort, return_value.port_)

        If RadioButtonSerial.Checked Then
            return_value.connection_type_ = FilterWheel.connection_type_t.SERIAL
        End If
        If RadioButtonNetwork.Checked Then
            return_value.connection_type_ = FilterWheel.connection_type_t.NETWORK
        End If

        With return_value.filter_wheel_settings_
            'filter wheel drive settings.
            valid = valid And getTextBoxValue(TextBoxMaxSpeed, .max_speed_)
            valid = valid And getTextBoxValue(TextBoxAccel, .acceleration_)
            valid = valid And getTextBoxValue(TextBoxGearRatio, .gear_ratio_)
            valid = valid And getTextBoxValue(TextBoxStepsRev, .number_of_steps_)

            .step_type_ = [Enum].Parse(GetType(FilterWheel.step_type_t), ComboBoxStepType.SelectedItem.ToString)

            .automatic_stepper_release_ = CheckBoxASR.Checked

            'home switch settings.
            .home_switch_avail_ = CheckBoxHSA.Checked
            .home_switch_inverted_ = CheckBoxHSI.Checked
            .startup_auto_home_ = CheckBoxSAH.Checked
            valid = valid And getTextBoxValue(TextBoxHTO, .home_timout_)

            TextBoxHSN.Text = YAAASharedClassLibrary.parsePin(TextBoxHSN.Text)
            valid = valid And getTextBoxValue(TextBoxHSN, .home_switch_nr_)

            'endstop switch settings.
            .endstop_lower_enabled_ = CheckBoxEndStopLowerEnabled.Checked
            .endstop_lower_inverted_ = CheckBoxEndstopLowerInverted.Checked
            .endstop_upper_enabled_ = CheckBoxEndstopUpperEnabled.Checked
            .endstop_upper_inverted_ = CheckBoxEndstopUpperInverted.Checked
            .endstop_hard_stop_ = CheckBoxEndstopHardStop.Checked

            TextBoxEndstopLowerPin.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopLowerPin.Text)
            valid = valid And getTextBoxValue(TextBoxEndstopLowerPin, .endstop_lower_)
            TextBoxEndstopUpperPin.Text = YAAASharedClassLibrary.parsePin(TextBoxEndstopUpperPin.Text)
            valid = valid And getTextBoxValue(TextBoxEndstopUpperPin, .endstop_upper_)

            'software movement limt settings.
            .limit_lower_enabled_ = CheckBoxLowerLimitEnabled.Checked
            .limit_upper_enabled_ = CheckBoxUpperLimitEnabled.Checked
            .limit_hard_stop_ = CheckBoxHardStopLimit.Checked
            valid = valid And getTextBoxValue(TextBoxLowerLimit, .limit_lower_)
            valid = valid And getTextBoxValue(TextBoxUpperLimit, .limit_upper_)

            'motor driver settings.
            .motor_driver_ = [Enum].Parse(GetType(FilterWheel.motor_driver_t), ComboBoxMotorDriver.SelectedItem.ToString)

            .i2c_address_ = Convert.ToInt32(ComboBoxI2CAddress.SelectedItem.ToString, 16)
            .terminal_ = CShort(ComboBoxTerminal.SelectedItem.ToString)

            .pin_1_inverted_ = CheckBoxPin1Inverted.Checked
            .pin_2_inverted_ = CheckBoxPin2Inverted.Checked
            .pin_3_inverted_ = CheckBoxPin3Inverted.Checked
            .pin_4_inverted_ = CheckBoxPin4Inverted.Checked
            .pin_5_inverted_ = CheckBoxPin5Inverted.Checked
            .pin_6_inverted_ = CheckBoxPin6Inverted.Checked

            TextBoxPin1.Text = YAAASharedClassLibrary.parsePin(TextBoxPin1.Text)
            TextBoxPin2.Text = YAAASharedClassLibrary.parsePin(TextBoxPin2.Text)
            TextBoxPin3.Text = YAAASharedClassLibrary.parsePin(TextBoxPin3.Text)
            TextBoxPin4.Text = YAAASharedClassLibrary.parsePin(TextBoxPin4.Text)
            TextBoxPin5.Text = YAAASharedClassLibrary.parsePin(TextBoxPin5.Text)
            TextBoxPin6.Text = YAAASharedClassLibrary.parsePin(TextBoxPin6.Text)
            valid = valid And getTextBoxValue(TextBoxPin1, .pin_1_)
            valid = valid And getTextBoxValue(TextBoxPin2, .pin_2_)
            valid = valid And getTextBoxValue(TextBoxPin3, .pin_3_)
            valid = valid And getTextBoxValue(TextBoxPin4, .pin_4_)
            valid = valid And getTextBoxValue(TextBoxPin5, .pin_5_)
            valid = valid And getTextBoxValue(TextBoxPin6, .pin_6_)

            'filter holder and filter settings.
            .filter_holder_type_ = [Enum].Parse(GetType(FilterWheel.filter_holder_type_t), ComboBoxFilterHolderType.SelectedItem.ToString)

            .filters_ = filters_.Clone

            'update temperature sensor settings
            For i As Integer = 0 To return_value.filter_wheel_settings_.temperature_sensors_.Length - 1
                valid = valid And user_controls_(i).getSensor(return_value.filter_wheel_settings_.temperature_sensors_(i))
            Next
        End With

        If Not valid Then
            Throw New ASCOM.InvalidValueException()
        End If

        Return return_value
    End Function

    Private Sub ButtonExportSettings_Click(sender As Object, e As EventArgs) Handles ButtonExportSettings.Click
        Dim SaveFileDialog1 As New SaveFileDialog
        SaveFileDialog1.AddExtension = True
        SaveFileDialog1.OverwritePrompt = True
        SaveFileDialog1.DefaultExt = "xml"
        SaveFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        SaveFileDialog1.InitialDirectory = FilterWheel.settingsStoreDirectory
        SaveFileDialog1.FileName = YAAASharedClassLibrary.YAAASETTINGS_FILE_DEFAULT

        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            If SaveFileDialog1.FileName <> "" Then
                Try
                    filter_wheel_.xmlFromDriverSettings(SaveFileDialog1.FileName, getSettingsFromMask())
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub ButtonImportSettings_Click(sender As Object, e As EventArgs) Handles ButtonImportSettings.Click
        Dim OpenFileDialog1 = New OpenFileDialog
        OpenFileDialog1.Filter = YAAASharedClassLibrary.YAAASETTINGS_FILE_DIALOG_FILTER
        OpenFileDialog1.InitialDirectory = FilterWheel.settingsStoreDirectory
        OpenFileDialog1.RestoreDirectory = True

        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            If OpenFileDialog1.FileName <> "" Then
                Dim settings As FilterWheel.filterWheelDriverSettings = New FilterWheel.filterWheelDriverSettings(True)

                Try
                    settings = filter_wheel_.xmlToDriverSettings(OpenFileDialog1.FileName)
                    setMaskToSettings(settings)
                Catch ex As Exception
                    MsgBox(ex.ToString, MsgBoxStyle.Critical, "error")
                End Try
            End If
        End If
    End Sub

    Private Sub SetupDialogForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            connect(False)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ToolStripSplitButton1_DropDownItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ToolStripSplitButton1.DropDownItemClicked
        Try
            connect(Not filter_wheel_.Connected)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        updateStatusStrip()
    End Sub
End Class