'tabs=4
' --------------------------------------------------------------------------------
'
' ASCOM FilterWheel driver for YAAAFilterWheel
'
' Description:	Version 0.0.1 of the Yet Another Arduino Ascom FilterWheel driver.
'
' Implements:	ASCOM FilterWheel interface version: 1.0
' Author:		(GChr) Gregor Christandl <christandlg@yahoo.com>
'
' License: GPL v2 or later (see license.txt)
'
' Edit Log:
'
' Date			Who	    Vers	Description
' -----------	---	    -----	-------------------------------------------------------
' 2015-04-11	GChr	0.0.1	Initial release
' -------------------------------------------------------------------------------------
'
'
' Your driver's ID is ASCOM.YAAAFilterWheel.FilterWheel
'
' The Guid attribute sets the CLSID for ASCOM.DeviceName.FilterWheel
' The ClassInterface/None addribute prevents an empty interface called
' _FilterWheel from being created and used as the [default] interface
'

' This definition is used to select code that's only applicable for one device type
#Const Device = "FilterWheel"

Imports ASCOM
Imports ASCOM.Astrometry
Imports ASCOM.Astrometry.AstroUtils
Imports ASCOM.DeviceInterface
Imports ASCOM.Utilities

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports System.Text
Imports System.Xml

<Guid("53b1c08b-a6d6-48a3-9318-234130f667c2")> _
<ClassInterface(ClassInterfaceType.None)> _
Public Class FilterWheel

    ' The Guid attribute sets the CLSID for ASCOM.YAAAFilterWheel.FilterWheel
    ' The ClassInterface/None addribute prevents an empty interface called
    ' _YAAAFilterWheel from being created and used as the [default] interface

    Implements IFilterWheelV2

    '
    ' Driver ID and descriptive string that shows in the Chooser
    '
    Friend Shared driverID As String = "ASCOM.YAAADevice.FilterWheel"
    Private Shared driverDescription As String = "YAAADevice FilterWheel"

    Enum connection_type_t
        SERIAL = 0
        NETWORK = 1
    End Enum

    Enum serial_error_code_t
        ERROR_NONE = 0      'not necessary?
        ERROR_UNKNOWN_DEVICE = 1
        ERROR_UNKNOWN_CMD = 2
        ERROR_UNKNOWN_CMD_TYPE = 4
        ERROR_INVALID_CMD_TYPE = 8
        ERROR_INVALID_PARAMETER = 16
        ERROR_MISSING_PARAMETER = 32
        ERROR_UNKNOWN_OPTION = 64
        ERROR_DEVICE_UNAVAILABLE = 128
        ERROR_DEVICE_LOCKED = 256
    End Enum

    Enum filter_holder_type_t
        SLIDER = 0
        WHEEL = 1
    End Enum

    Enum filter_type_t
        FILTER_NONE = 0     'no filter in slot
        FILTER_CLEAR = 1

        'color filters
        FILTER_VIOLET = 10
        FILTER_BLUE = 11
        FILTER_GREEN = 12
        FILTER_YELLOW = 13
        FILTER_ORANGE = 14
        FILTER_RED = 15

        FILTER_DARK_VIOLET = 20
        FILTER_DARK_BLUE = 21
        FILTER_DARK_GREEN = 22
        FILTER_DARK_YELLOW = 23
        FILTER_DARK_ORANGE = 24
        FILTER_DARK_RED = 25

        FILTER_LIGHT_VIOLET = 30
        FILTER_LIGHT_BLUE = 31
        FILTER_LIGHT_GREEN = 32
        FILTER_LIGHT_YELLOW = 33
        FILTER_LIGHT_ORANGE = 34
        FILTER_LIGHT_RED = 35

        'color mixtures
        FILTER_BLUE_GREEN = 40
		FILTER_GEEN_YELLOW = 41

        'polarization filters
        FILTER_POLARIZATION = 110

		'celestial objects filters
		FILTER_MOON = 130
		FILTER_SUN = 131

        'other filters
        FILTER_ARTIFICIAL = 140
		FILTER_PHOTOMETRIC = 141

        'bandpass filters
        FILTER_LUMINANCE = 150      'allows whole visible spectrum to pass

        'high pass filters
        FILTER_IR = 170

        'low pass filters
        FILTER_UV = 180

		'unkown filter
		FILTER_UNKNOWN = 255
    End Enum

    Enum motor_driver_t
        ADAFRUIT_MOTORSHIELD_V2 = 0
        FOUR_WIRE_STEPPER = 1
        DRIVER_A4988 = 2
        DRIVER_DRV8825 = 3
    End Enum

    Enum step_type_t
        STEPTYPE_QUICKSTEP = 0              'for Adafruit Motor Shield V2.
        STEPTYPE_SINGLE = 1                 'for Adafruit Motor Shield V2.
        'STEPTYPE_DOUBLE                    'for Adafruit Motor Shield V2.
        STEPTYPE_FULL = 3                   'for stepper drivers and Adafruit Motor Shield V2 (DOUBLE).
        STEPTYPE_MICROSTEP_2 = 4            'for stepper drivers and Adafruit Motor Shield V2.
        STEPTYPE_MICROSTEP_4 = 5            'for stepper drivers.
        STEPTYPE_MICROSTEP_8 = 6            'for stepper drivers.
        STEPTYPE_MICROSTEP_16 = 7           'for stepper drivers and Adafruit Motor Shield V2.
        STEPTYPE_MICROSTEP_32 = 8           'for stepper drivers.
        STEPTYPE_MICROSTEP_64 = 9           'for stepper drivers.
        STEPTYPE_MICROSTEP_128 = 10         'for stepper drivers.
    End Enum

    Friend Const numTempSensors As Integer = 3

#Region "Constants used for Profile persistence"
    'Constants used for Profile persistence
    Private Structure filterWheelProfileNames
        Public Shared comPortProfileName As String = "COM Port"
        Public Shared traceStateProfileName As String = "Trace Level"
        Public Shared baudRateProfileName As String = "Baud Rate"

        Public Shared connectionTypeProfileName As String = "Connection Type"
        Public Shared ipAddressProfileName As String = "IP Address"
        Public Shared portProfileName As String = "Port"

        Public Shared gearRatioProfileName As String = "Gear Ratio"
        Public Shared accelerationProfileName As String = "Acceleration"
        Public Shared maxSpeedProfileName As String = "Max Speed"
        Public Shared stepsPerRevProfileName As String = "Steps per Revolution"

        Public Shared ASRProfileName As String = "automatic stepper release"
        Public Shared FilterHolderTypeProfileName As String = "Filter Holder Type"
        Public Shared HSAProfileName As String = "Home Switch Available"
        Public Shared HSNProfileName As String = "Home Switch Number"
        Public Shared SAHProfileName As String = "Startup auto home"

        Public Shared HTOProfileName As String = "Homing Timeout"

        Public Shared endStopLowerEnabledProfileName As String = "Lower Endstop Enabled"
        Public Shared endStopUpperEnabledProfileName As String = "Upper Endstop Enabled"
        Public Shared endStopLowerProfileName As String = "Lower Endstop Pin Nr"
        Public Shared endStopUpperProfileName As String = "Upper Endstop Pin Nr"
        Public Shared endstopLowerInvertedProfileName As String = "Lower Endstop Inverted"
        Public Shared endstopUpperInvertedProfileName As String = "Upper Endstop Inverted"
        Public Shared endstopHardStop As String = "Endstop Hard Stop"

        Public Shared numberOfFiltersProfileName As String = "Number of Filters"
        Public Shared fwNamesProfileName = "Name"
        Public Shared fwTypesProfileName = "Type"
        Public Shared fwRFUProfileName = "RFU"
        Public Shared fwFocusOffsetsProfileName As String = "Focus Offset"
        Public Shared fwPositionOffsetsProfileName As String = "Position Offset"

        Public Shared stepTypeProfileName As String = "Step Type"

        Public Shared motorDriverProfileName As String = "Motor Driver Type"

        Public Shared I2CProfileName As String = "I2C Address"
        Public Shared terminalProfileName As String = "Terminal"

        Public Shared pin1ProfileName As String = "Pin 1"
        Public Shared pin2ProfileName As String = "Pin 2"
        Public Shared pin3ProfileName As String = "Pin 3"
        Public Shared pin4ProfileName As String = "Pin 4"
        Public Shared pin5ProfileName As String = "Pin 5"
        Public Shared pin6ProfileName As String = "Pin 6"

        Public Shared pin1InvertedProfileName As String = "Pin 1 Inverted"
        Public Shared pin2InvertedProfileName As String = "Pin 2 Inverted"
        Public Shared pin3InvertedProfileName As String = "Pin 3 Inverted"
        Public Shared pin4InvertedProfileName As String = "Pin 4 Inverted"
        Public Shared pin5InvertedProfileName As String = "Pin 5 Inverted"
        Public Shared pin6InvertedProfileName As String = "Pin 6 Inverted"

        Public Shared TSTProfileName As String = "Type"
        Public Shared TSDProfileName As String = "Device"
        Public Shared TSUProfileName As String = "Unit"
        Public Shared TSPProfileName As String = "Parameters"
        Public Shared TSAProfileName As String = "Average Samples"

        Public Shared TSSubKeyProfileName As String = "Temperature Sensor"
        Public Shared FilterSubKeyProfileName As String = "Filter"
    End Structure

    Public Shared settingsStoreDirectory = YAAASharedClassLibrary.YAAASETTINGS_DIRECTORY & "YAAAFilterWheel"

    Private SET_CONNECTED_DELAY As Integer = 5000

    Private TIMEOUT_SERIAL As Integer = 5000
    Private TIMEOUT_TCP As Integer = 5000
#End Region

#Region "default values"

    Private Structure filterWheelDefaultValues
        Public Shared comPortDefault As String = "COM6"
        Public Shared traceStateDefault As Boolean = True
        Public Shared baudRateDefault As Integer = 9600

        Public Shared connectionTypeDefault As connection_type_t = connection_type_t.SERIAL
        Public Shared ipAddressDefault As String = "192.168.0.202"
        Public Shared portDefault As Integer = 23

        Public Shared accelerationDefault As Single = 100.0
        Public Shared maxSpeedDefault As Single = 200.0
        Public Shared gearRatioDefault As Single = 5.0
        'Public Shared stepsPerRevDefault As Short = 200
        Public Shared numberOfStepsDefault As UShort = 200
        Public Shared stepTypeDefault As step_type_t = step_type_t.STEPTYPE_FULL

        Public Shared ASRDefault As Boolean = True
        Public Shared HSADefault As Boolean = False     'home switch available
        Public Shared HSNDefault As Short = 69          'home switch pin number
        Public Shared HSIDefault As Boolean = False     'home switch inverted
        Public Shared SAHDefault As Boolean = False
        Public Shared HTODefault As UInteger = 0        'find home timeout in ms. 0 to disable.

        Public Shared endstopLowerEnabledDefault As Boolean = False
        Public Shared endstopUpperEnabledDefault As Boolean = False
        Public Shared endstopLowerDefault As Short = 67
        Public Shared endstopUpperDefault As Short = 68
        Public Shared endstopLowerInvertedDefault As Boolean = False
        Public Shared endstopUpperInvertedDefault As Boolean = False
        Public Shared endstopHardStopDefault As Boolean = True

        Public Shared limitLowerEnabledDefault As Boolean = False
        Public Shared limitUpperEnabledDefault As Boolean = False
        Public Shared limitLowerDefault As Single = 0.0
        Public Shared limitUpperDefault As Single = 90.0
        Public Shared limitHardStopDefault As Boolean = True

        Public Shared I2CAddressDefault As Short = 98
        Public Shared terminalDefault As Short = 1

        Public Shared pin1Default As Short = 255
        Public Shared pin2Default As Short = 255
        Public Shared pin3Default As Short = 255
        Public Shared pin4Default As Short = 255
        Public Shared pin5Default As Short = 255
        Public Shared pin6Default As Short = 255

        Public Shared pin1InvertedDefault As Boolean = False
        Public Shared pin2InvertedDefault As Boolean = False
        Public Shared pin3InvertedDefault As Boolean = False
        Public Shared pin4InvertedDefault As Boolean = False
        Public Shared pin5InvertedDefault As Boolean = False
        Public Shared pin6InvertedDefault As Boolean = False

        Public Shared filterHolderTypeDefault As filter_holder_type_t = filter_holder_type_t.WHEEL
        Public Shared nrOfFiltersDefault As Short = 4
        Public Shared fwNamesDefault As String = "unknown"
        Public Shared fwTypesDefault As filter_type_t = filter_type_t.FILTER_UNKNOWN
        Public Shared fwPositionOffsetDefault As Integer = 0
        Public Shared fwFocusOffsetsDefault As Integer = 0
        Public Shared fwRFUDefault As Byte = 0

        Public Shared motorDriverDefault As String = motor_driver_t.ADAFRUIT_MOTORSHIELD_V2
    End Structure

#End Region

#Region "Variables to hold the currrent device configuration"

    <DataContract>
    Friend Structure filterWheelDriverSettings
        Implements ICloneable

        <DataMember()>
        Public Shared trace_state_ As Boolean = filterWheelDefaultValues.traceStateDefault

        <DataMember()>
        Public connection_type_ As connection_type_t

        <DataMember()>
        Public com_port_ As String
        <DataMember()>
        Friend baud_rate_ As Integer

        <DataMember()>
        Public ip_address_ As String
        <DataMember()>
        Public port_ As Integer

        <DataMember()>
        Dim filter_wheel_settings_ As filterWheelSettings

        Public Sub New(Optional initialze As Boolean = True)
            If Not initialze Then
                Exit Sub
            End If

            connection_type_ = filterWheelDefaultValues.connectionTypeDefault

            com_port_ = filterWheelDefaultValues.comPortDefault
            baud_rate_ = filterWheelDefaultValues.baudRateDefault

            ip_address_ = filterWheelDefaultValues.ipAddressDefault
            port_ = filterWheelDefaultValues.portDefault

            filter_wheel_settings_ = New filterWheelSettings(True)
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As filterWheelDriverSettings = MemberwiseClone()

            Return return_value
        End Function
    End Structure

    <DataContract>
    Friend Structure filterWheelSettings
        Implements ICloneable

        <DataMember()>
        Public acceleration_ As Single
        <DataMember()>
        Public max_speed_ As Single
        <DataMember()>
        Public gear_ratio_ As Single
        <DataMember()>
        Public number_of_steps_ As UShort
        <DataMember()>
        Public step_type_ As step_type_t

        <DataMember()>
        Public automatic_stepper_release_ As Boolean
        <DataMember()>
        Public home_switch_avail_ As Boolean
        <DataMember()>
        Public home_switch_nr_ As Byte
        <DataMember()>
        Public home_switch_inverted_ As Boolean
        <DataMember()>
        Public startup_auto_home_ As Boolean
        <DataMember()>
        Public home_timout_ As UInteger

        <DataMember()>
        Public endstop_lower_enabled_ As Boolean
        <DataMember()>
        Public endstop_lower_ As Byte
        <DataMember()>
        Public endstop_lower_inverted_ As Boolean
        <DataMember()>
        Public endstop_upper_enabled_ As Boolean
        <DataMember()>
        Public endstop_upper_ As Byte
        <DataMember()>
        Public endstop_upper_inverted_ As Boolean
        <DataMember()>
        Public endstop_hard_stop_ As Boolean

        <DataMember()>
        Public limit_lower_enabled_ As Boolean
        <DataMember()>
        Public limit_lower_ As Single
        <DataMember()>
        Public limit_upper_enabled_ As Boolean
        <DataMember()>
        Public limit_upper_ As Single
        <DataMember()>
        Public limit_hard_stop_ As Boolean

        <DataMember()>
        Public motor_driver_ As motor_driver_t   'Type of stepper motor driver.

        <DataMember()>
        Public terminal_ As Byte
        <DataMember()>
        Public i2c_address_ As Byte

        <DataMember()>
        Public pin_1_ As Byte                  'pin 1 (4 wire stepper) or enable pin.
        <DataMember()>
        Public pin_2_ As Byte                  'pin 2 (4 wire stepper) or direction pin.
        <DataMember()>
        Public pin_3_ As Byte                  'pin 3 (4 wire stepper) or step pin.
        <DataMember()>
        Public pin_4_ As Byte                  'pin 4 (4 wire stepper) or MS1 pin.
        <DataMember()>
        Public pin_5_ As Byte                  'MS2 pin.
        <DataMember()>
        Public pin_6_ As Byte                  'MS3 pin.

        <DataMember()>
        Public pin_1_inverted_ As Boolean       'pin 1 (4 wire stepper) or enable pin.
        <DataMember()>
        Public pin_2_inverted_ As Boolean       'pin 2 (4 wire stepper) or direction pin.
        <DataMember()>
        Public pin_3_inverted_ As Boolean       'pin 3 (4 wire stepper) or step pin.
        <DataMember()>
        Public pin_4_inverted_ As Boolean       'pin 4 (4 wire stepper) or MS1 pin.
        <DataMember()>
        Public pin_5_inverted_ As Boolean       'MS2 pin.
        <DataMember()>
        Public pin_6_inverted_ As Boolean       'MS3 pin.

        <DataMember()>
        Public filter_holder_type_ As filter_holder_type_t
        'Public number_of_filters_ As Byte
        <DataMember()>
        Public filters_() As filterSettings

        <DataMember()>
        Public temperature_sensors_() As YAAASharedClassLibrary.YAAASensor  'As temperatureSensorSettings

        Public Sub New(Optional initialze As Boolean = True)
            If Not initialze Then
                Exit Sub
            End If

            acceleration_ = filterWheelDefaultValues.accelerationDefault
            max_speed_ = filterWheelDefaultValues.maxSpeedDefault
            gear_ratio_ = filterWheelDefaultValues.gearRatioDefault
            number_of_steps_ = filterWheelDefaultValues.numberOfStepsDefault
            step_type_ = filterWheelDefaultValues.stepTypeDefault

            automatic_stepper_release_ = filterWheelDefaultValues.ASRDefault
            home_switch_avail_ = filterWheelDefaultValues.HSADefault
            home_switch_nr_ = filterWheelDefaultValues.HSNDefault
            home_switch_inverted_ = filterWheelDefaultValues.HSIDefault
            startup_auto_home_ = filterWheelDefaultValues.SAHDefault
            home_timout_ = filterWheelDefaultValues.HTODefault

            endstop_lower_enabled_ = filterWheelDefaultValues.endstopLowerEnabledDefault
            endstop_lower_ = filterWheelDefaultValues.endstopLowerDefault
            endstop_lower_inverted_ = filterWheelDefaultValues.endstopLowerInvertedDefault
            endstop_upper_enabled_ = filterWheelDefaultValues.endstopUpperEnabledDefault
            endstop_upper_ = filterWheelDefaultValues.endstopUpperDefault
            endstop_upper_inverted_ = filterWheelDefaultValues.endstopUpperInvertedDefault
            endstop_hard_stop_ = filterWheelDefaultValues.endstopHardStopDefault

            limit_lower_enabled_ = filterWheelDefaultValues.limitLowerEnabledDefault
            limit_lower_ = filterWheelDefaultValues.limitLowerDefault
            limit_upper_enabled_ = filterWheelDefaultValues.limitUpperEnabledDefault
            limit_upper_ = filterWheelDefaultValues.limitUpperDefault
            limit_hard_stop_ = filterWheelDefaultValues.limitHardStopDefault

            motor_driver_ = filterWheelDefaultValues.motorDriverDefault

            terminal_ = filterWheelDefaultValues.terminalDefault
            i2c_address_ = filterWheelDefaultValues.I2CAddressDefault

            pin_1_ = filterWheelDefaultValues.pin1Default
            pin_2_ = filterWheelDefaultValues.pin2Default
            pin_3_ = filterWheelDefaultValues.pin3Default
            pin_4_ = filterWheelDefaultValues.pin4Default
            pin_5_ = filterWheelDefaultValues.pin5Default
            pin_6_ = filterWheelDefaultValues.pin6Default

            pin_1_inverted_ = filterWheelDefaultValues.pin1InvertedDefault
            pin_2_inverted_ = filterWheelDefaultValues.pin2InvertedDefault
            pin_3_inverted_ = filterWheelDefaultValues.pin3InvertedDefault
            pin_4_inverted_ = filterWheelDefaultValues.pin4InvertedDefault
            pin_5_inverted_ = filterWheelDefaultValues.pin5InvertedDefault
            pin_6_inverted_ = filterWheelDefaultValues.pin6InvertedDefault

            filter_holder_type_ = filterWheelDefaultValues.filterHolderTypeDefault

            filters_ = {New filterSettings(True), New filterSettings(True), New filterSettings(True), New filterSettings(True)}

            temperature_sensors_ = {New YAAASharedClassLibrary.YAAASensor(True), New YAAASharedClassLibrary.YAAASensor(True), New YAAASharedClassLibrary.YAAASensor(True)}
        End Sub

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As filterWheelSettings = MemberwiseClone()

            'new array for sensors
            Dim sensors(temperature_sensors_.Length - 1) As YAAASharedClassLibrary.YAAASensor

            'copy sensors to new array
            Array.Copy(temperature_sensors_, sensors, temperature_sensors_.Length)

            'replace the reference created by MemberwiseClone() by a deep copy of the array
            return_value.temperature_sensors_ = sensors

            'new array for filters
            Dim filters(filters_.Length - 1) As filterSettings

            'copy filters to new array
            Array.Copy(filters_, filters, filters_.Length)

            'replace the reference created by MemberwiseClone() by a deep copy of the array
            return_value.filters_ = filters

            Return return_value
        End Function
    End Structure

    <DataContract>
    Friend Structure filterSettings
        Implements ICloneable

        <DataMember()>
        Public name_ As String
        <DataMember()>
        Public type_ As filter_type_t
        <DataMember()>
        Public RFU_ As Byte
        <DataMember()>
        Public focus_offset_ As Integer
        <DataMember()>
        Public position_offset_ As Integer

        Public Sub New(initialize As Boolean)
            If Not initialize Then
                Exit Sub
            End If

            name_ = filterWheelDefaultValues.fwNamesDefault
            type_ = filterWheelDefaultValues.fwTypesDefault
            RFU_ = filterWheelDefaultValues.fwRFUDefault
            focus_offset_ = filterWheelDefaultValues.fwFocusOffsetsDefault
            position_offset_ = filterWheelDefaultValues.fwPositionOffsetDefault
        End Sub

        Public Sub New(name As String, type As String, rfu As String, focus_offset As Integer, position_offset As Integer)
            name_ = name

            'save filter type.
            If [Enum].IsDefined(GetType(FilterWheel.filter_type_t), type) Then
                type_ = [Enum].Parse(GetType(FilterWheel.filter_type_t), type)
            Else
                type_ = filterWheelDefaultValues.fwTypesDefault
            End If

            RFU_ = rfu
            focus_offset_ = focus_offset
            position_offset_ = position_offset
        End Sub

        'creates a new filterSettings object from a string sent by the arduino.
        '@param string filter data as string as sent by the arduino.
        Public Sub New(data As String)
            Dim separators() As String = {" ", ","}
            Dim values() As String

            'split strings 
            values = data.Split(separators, StringSplitOptions.RemoveEmptyEntries)

            If Not values.Length = 4 Then
                Throw New ArgumentException("error: expected 4 parameters in string, received " + values.Length)
            End If

            name_ = ""

            If [Enum].IsDefined(GetType(FilterWheel.filter_type_t), CInt(values(0))) Then
                type_ = [Enum].Parse(GetType(FilterWheel.filter_type_t), values(0))
            Else
                type_ = FilterWheel.filter_type_t.FILTER_UNKNOWN
            End If

            RFU_ = CByte(values(1))
            focus_offset_ = CInt(values(2))
            position_offset_ = CInt(values(3))
        End Sub

        'returns this filter's settings as a string that can be sent to the arduino.
        Public Function toArduinoString() As String
            Dim return_value As String = ""

            return_value &= CByte(type_)
            return_value &= " "

            return_value &= RFU_.ToString
            return_value &= " "

            return_value &= focus_offset_.ToString
            return_value &= " "

            return_value &= position_offset_.ToString

            Return return_value
        End Function

        Public Function Clone() As Object Implements ICloneable.Clone
            Dim return_value As filterSettings = MemberwiseClone()

            'name_ is a reference type, needs to be copied manually
            return_value.name_ = String.Copy(name_)

            Return return_value
        End Function

        Public Overloads Shared Operator =(ByVal s1 As filterSettings, ByVal s2 As filterSettings) As Boolean
            Return s1.type_ = s2.type_ AndAlso s1.focus_offset_ = s2.focus_offset_ AndAlso s1.position_offset_ = s2.position_offset_ 'AndAlso s1.RFU_ = s2.RFU_
        End Operator

        Public Overloads Shared Operator <>(ByVal s1 As filterSettings, ByVal s2 As filterSettings) As Boolean
            Return s1.type_ <> s2.type_ Or s1.focus_offset_ <> s2.focus_offset_ Or s1.position_offset_ <> s2.position_offset_ 'Or s1.RFU_ <> s2.RFU_
        End Operator
    End Structure

    Friend EEPROM_force_write_ As Boolean = False

    Private filter_wheel_driver_settings_ As filterWheelDriverSettings
    Private filter_wheel_driver_settings_EEPROM_ As filterWheelDriverSettings
#End Region

    Private com_port_list_() As String

    'Private connectedState As Boolean ' Private variable to hold the connected state
    Private utilities As Util ' Private variable to hold an ASCOM Utilities object
    Private astroUtilities As AstroUtils ' Private variable to hold an AstroUtils object to provide the Range method
    Private TL As TraceLogger ' Private variable to hold the trace logger object (creates a diagnostic log file with information that you specify)

    Friend serial_connection_ As ASCOM.Utilities.Serial

    Friend tcp_connection_ As Net.Sockets.TcpClient

    '
    ' Constructor - Must be public for COM registration!
    '
    Public Sub New()
        TL = New TraceLogger("", "YAAAFilterWheel")

        TL.Enabled = False

        filter_wheel_driver_settings_ = New filterWheelDriverSettings(True)
        filter_wheel_driver_settings_EEPROM_ = filter_wheel_driver_settings_.Clone()

        ReadProfile() ' Read device configuration from the ASCOM Profile store

        TL.Enabled = filterWheelDriverSettings.trace_state_

        TL.LogMessage("FilterWheel", "Starting initialisation")

        utilities = New Util() ' Initialise util object
        astroUtilities = New AstroUtils 'Initialise new astro utilites object

        'TODO: Implement your additional construction here

        'open serial connection
        serial_connection_ = New ASCOM.Utilities.Serial

        settingsStoreDirectory = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), settingsStoreDirectory)

        TL.LogMessage("FilterWheel", "Completed initialisation")
    End Sub

    '
    ' PUBLIC COM INTERFACE IFilterWheelV2 IMPLEMENTATION
    '

#Region "Common properties and methods"
    ''' <summary>
    ''' Displays the Setup Dialog form.
    ''' If the user clicks the OK button to dismiss the form, then
    ''' the new settings are saved, otherwise the old values are reloaded.
    ''' THIS IS THE ONLY PLACE WHERE SHOWING USER INTERFACE IS ALLOWED!
    ''' </summary>
    Public Sub SetupDialog() Implements IFilterWheelV2.SetupDialog
        ' consider only showing the setup dialog if not connected
        ' or call a different dialog if connected
        If IsConnected Then
            System.Windows.Forms.MessageBox.Show("Already connected, just press OK")
        End If

        Using F As SetupDialogForm = New SetupDialogForm(Me)
            Dim result As System.Windows.Forms.DialogResult = F.ShowDialog()
            If result = DialogResult.OK Then
                WriteProfile() ' Persist device configuration values to the ASCOM Profile store
            End If
        End Using
    End Sub

    Public ReadOnly Property SupportedActions() As ArrayList Implements IFilterWheelV2.SupportedActions
        Get
            TL.LogMessage("SupportedActions Get", "Returning empty arraylist")

            Dim actions = New ArrayList()

            'add supported actions.
            actions.Add("Get_Switch_State")
            actions.Add("Measure_Home_Offset")
            actions.Add("At_Home")
            actions.Add("Find_Home")
            actions.Add("Is_Homing")
            actions.Add("Is_Measuring_Home_Offset")
            actions.Add("Set_Current_Filter")       'needs a parameter (curren filter)

            Return actions
        End Get
    End Property

    Public Function Action(ByVal ActionName As String, ByVal ActionParameters As String) As String Implements IFilterWheelV2.Action
        If ActionName = "Get_Switch_State" Then
            Return getSwitchState()

        ElseIf ActionName = "Measure_Home_Offset" Then
            measureHomeOffset()
            Return "True"

        ElseIf ActionName = "At_Home" Then
            Return atHome.ToString()

        ElseIf ActionName = "Find_Home" Then
            findHome()
            Return "True"

        ElseIf ActionName = "Is_Homing" Then
            Return isHoming().ToString()

        ElseIf ActionName = "Is_Measuring_Home_Offset" Then
            Return isMeasuringHomeOffset().ToString()

        ElseIf ActionName = "Set_Current_Filter" Then
            setCurrentFilter(CInt(ActionParameters))
            Return True

        Else : Throw New ActionNotImplementedException("Action " & ActionName & " is not supported by this driver")

        End If
    End Function

    Public Sub CommandBlind(ByVal Command As String, Optional ByVal Raw As Boolean = False) Implements IFilterWheelV2.CommandBlind
        CheckConnected("CommandBlind")
        ' Call CommandString and return as soon as it finishes
        Me.CommandString(Command, Raw)
        ' or
        Throw New MethodNotImplementedException("CommandBlind")
    End Sub

    Public Function CommandBool(ByVal Command As String, Optional ByVal Raw As Boolean = False) As Boolean _
        Implements IFilterWheelV2.CommandBool
        CheckConnected("CommandBool")
        Dim ret As String = CommandString(Command, Raw)
        ' TODO decode the return string and return true or false
        ' or
        Throw New MethodNotImplementedException("CommandBool")
    End Function

    Public Function CommandString(ByVal Command As String, Optional ByVal Raw As Boolean = False) As String _
        Implements IFilterWheelV2.CommandString
        CheckConnected("CommandString")
        ' it's a good idea to put all the low level communication with the device here,
        ' then all communication calls this function
        ' you need something to ensure that only one command is in progress at a time
        Throw New MethodNotImplementedException("CommandString")
    End Function

    Public Property Connected() As Boolean Implements IFilterWheelV2.Connected
        Get
            Dim cn As Boolean = IsConnected
            TL.LogMessage("Connected Get", cn.ToString())
            Return cn
        End Get

        Set(value As Boolean)
            TL.LogMessage("Connected Set", value.ToString())

            If value Then
                'connect to arduino
                IsConnected = True

                'check connection
                Dim response As String

                Try
                    response = sendCommand("CONNECTED,GET")
                Catch Exc As System.TimeoutException
                    TL.LogMessage("Connected Set", "Error: Timeout, check connection")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.Utilities.Exceptions.SerialPortInUseException
                    TL.LogMessage("Connected Set", "Error: unable to acquire the serial port")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As ASCOM.NotConnectedException
                    TL.LogMessage("Connected Set", "Error: serial port is not connected")
                    Throw New DriverException(Exc.Message, Exc)
                Catch Exc As Exception
                    TL.LogMessage("Connected Set", "Error: unknown error, check inner exception")   'TODO timeout exceptions are acutally caught here
                    Throw New DriverException(Exc.Message, Exc)
                End Try

                'check response
                response = response.Replace("CONNECTED,", "").Trim

                If Not IsNumeric(response) Then
                    TL.LogMessage("Connected Set", "Error: could not check connection state because response contained non-numeric elements: " & response)
                    Throw New DriverException("could not check connection state because response contained non-numeric elements: " & response)
                End If

                If CInt(response).Equals(0) Then
                    TL.LogMessage("Connected Set", "Error: Arduino responding, but Filter Wheel is not enabled")

                    'disconnect from hardware
                    IsConnected = False

                    Throw New ASCOM.InvalidOperationException("Connected Set Error: Arduino responding, but Filter Wheel is not enabled")
                ElseIf CInt(response).Equals(1) Then
                    TL.LogMessage("Connected Set", "Successfully connected")
                Else
                    TL.LogMessage("Connected Set", "Error: unknown Response: " & response)
                    Throw New ASCOM.DriverException("Connected Set Error: unknown Response: " & response)
                End If

            Else
                IsConnected = False
            End If
        End Set
    End Property

    Public ReadOnly Property Description As String Implements IFilterWheelV2.Description
        Get
            ' this pattern seems to be needed to allow a public property to return a private field
            Dim d As String = driverDescription
            TL.LogMessage("Description Get", d)
            Return d
        End Get
    End Property

    Public ReadOnly Property DriverInfo As String Implements IFilterWheelV2.DriverInfo
        Get
            Dim m_version As Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version
            ' TODO customise this driver description
            Dim s_driverInfo As String = "Information about the driver itself. Version: " + m_version.Major.ToString() + "." + m_version.Minor.ToString()
            TL.LogMessage("DriverInfo Get", s_driverInfo)
            Return s_driverInfo
        End Get
    End Property

    Public ReadOnly Property DriverVersion() As String Implements IFilterWheelV2.DriverVersion
        Get
            ' Get our own assembly and report its version number
            TL.LogMessage("DriverVersion Get", Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2))
            Return Reflection.Assembly.GetExecutingAssembly.GetName.Version.ToString(2)
        End Get
    End Property

    Public ReadOnly Property InterfaceVersion() As Short Implements IFilterWheelV2.InterfaceVersion
        Get
            TL.LogMessage("InterfaceVersion Get", "2")
            Return 2
        End Get
    End Property

    Public ReadOnly Property Name As String Implements IFilterWheelV2.Name
        Get
            Dim s_name As String = "YAAADevice Filter Wheel Driver"
            TL.LogMessage("Name Get", s_name)
            Return s_name
        End Get
    End Property

    Public Sub Dispose() Implements IFilterWheelV2.Dispose
        ' Clean up the tracelogger and util objects
        TL.Enabled = False
        TL.Dispose()
        TL = Nothing

        'destroy serial_connection_
        'necessary?
        serial_connection_.Dispose()
        serial_connection_ = Nothing

        utilities.Dispose()
        utilities = Nothing
        astroUtilities.Dispose()
        astroUtilities = Nothing
    End Sub

#End Region

#Region "IFilterWheel Implementation"
    'Private fwOffsets As Integer() = New Integer(3) {0, 0, 0, 0} 'class level variable to hold focus offsets
    'Private fwNames As String() = New String(3) {"Red", "Green", "Blue", "Clear"} 'class level variable to hold the filter names
    Private fwPosition As Short = 0 'class level variable to retain the current filterwheel position

    Public ReadOnly Property FocusOffsets() As Integer() Implements IFilterWheelV2.FocusOffsets
        Get
            'For Each fwOffset As Integer In fwOffsets ' Write filter offsets to the log
            '    TL.LogMessage("FocusOffsets Get", fwOffset.ToString())
            'Next

            Dim fwOffsets() As Integer

            ReDim fwOffsets(filter_wheel_driver_settings_.filter_wheel_settings_.filters_.Length - 1)

            For i As Integer = 0 To filter_wheel_driver_settings_.filter_wheel_settings_.filters_.Length - 1
                fwOffsets(i) = filter_wheel_driver_settings_.filter_wheel_settings_.filters_(i).focus_offset_
            Next

            Return fwOffsets
        End Get
    End Property

    Public ReadOnly Property Names As String() Implements IFilterWheelV2.Names
        Get
            'For Each fwName As String In fwNames ' Write filter names to the log
            '    TL.LogMessage("Names Get", fwName)
            'Next

            Dim fwNames() As String
            ReDim fwNames(filter_wheel_driver_settings_.filter_wheel_settings_.filters_.Length - 1)

            For i As Integer = 0 To filter_wheel_driver_settings_.filter_wheel_settings_.filters_.Length - 1
                fwNames(i) = filter_wheel_driver_settings_.filter_wheel_settings_.filters_(i).name_
            Next

            Return fwNames
        End Get
    End Property

    Public Property Position() As Short Implements IFilterWheelV2.Position
        Get
            'throw an exception if the filter wheel is not connected.
            CheckConnected("Position Get")

            Dim response As String
            response = sendCommand("POSITION,GET")
            response = response.Replace("POSITION,", "")

            fwPosition = CShort(response)

            TL.LogMessage("Position Get", fwPosition.ToString())
            Return fwPosition
        End Get

        Set(value As Short)
            CheckConnected("Position Set")

            ''throw an exception if an invalid filter number is given.            
            'If (value < 0) Then
            '    TL.LogMessage("Position Set", "Invalid value: " & value)
            '    Throw New InvalidValueException("FilterWheel Error: invalid filter number: " & value)
            'End If
            'If ((value < 0) Or (value > fwNames.Length - 1)) Then
            '    TL.LogMessage("Position Set", "Throwing InvalidValueException")
            '    Throw New InvalidValueException("Position", value.ToString(), "0 to " & (fwNames.Length - 1).ToString())
            'End If

            TL.LogMessage("Position Set", value.ToString())

            Dim response As String
            Dim message As String

            message = "POSITION,SET,"
            message &= value.ToString

            response = sendCommand(message)
            response = response.Replace("POSITION,", "")

            Dim return_value As Integer = CInt(response)

            If (response = 0) Then
                TL.LogMessage("Position Set", "successful")

            ElseIf (response = 1) Then
                'when an error occured, throw an exception
                TL.LogMessage("Position Set", "Invalid value: " & value)
                'Throw New DriverException("FilterWheel Error: invalid filter number: " & value)
                Throw New InvalidValueException("FilterWheel Error: invalid filter number: " & value)

            ElseIf (response = 4) Then
                TL.LogMessage("Position Set", "successful: already at requested position")
            End If

            fwPosition = value
        End Set
    End Property

#End Region

#Region "Private properties and methods"
    ' here are some useful properties and methods that can be used as required
    ' to help with

#Region "ASCOM Registration"

    Private Shared Sub RegUnregASCOM(ByVal bRegister As Boolean)

        Using P As New Profile() With {.DeviceType = "FilterWheel"}
            If bRegister Then
                P.Register(driverID, driverDescription)
            Else
                P.Unregister(driverID)
            End If
        End Using

    End Sub

    <ComRegisterFunction()>
    Public Shared Sub RegisterASCOM(ByVal T As Type)

        RegUnregASCOM(True)

    End Sub

    <ComUnregisterFunction()>
    Public Shared Sub UnregisterASCOM(ByVal T As Type)

        RegUnregASCOM(False)

    End Sub

#End Region

    ''' <summary>
    ''' Returns true if there is a valid connection to the driver hardware 
    ''' set: attempts to enable or disable the connection to the hardware, throws an exception if it fails.
    ''' </summary>
    Private Property IsConnected As Boolean
        Get
            Dim connected As Boolean = False

            'check that the driver hardware connection exists and is connected to the hardware
            Select Case filter_wheel_driver_settings_.connection_type_
                Case connection_type_t.SERIAL
                    If Not serial_connection_ Is Nothing Then
                        Return serial_connection_.Connected
                    Else
                        Return False
                    End If
                Case connection_type_t.NETWORK
                    If Not tcp_connection_ Is Nothing Then
                        Return tcp_connection_.Connected
                    Else
                        Return False
                    End If
                Case Else
                    Throw New ASCOM.DriverException("unknown connection state " & filter_wheel_driver_settings_.connection_type_.ToString)
            End Select

            TL.LogMessage("IsConnected Get", connected.ToString() & If(connected, " (connection type: " & filter_wheel_driver_settings_.connection_type_.ToString & ")", ""))

            Return connected
        End Get
        Set(value As Boolean)
            TL.LogMessage("IsConnected Set", value.ToString())

            If value = IsConnected Then
                TL.LogMessage("IsConnected Set", "already " & If(value, "connected", "disconnected"))
                Return
            End If

            If value Then
                Select Case filter_wheel_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        TL.LogMessage("IsConnected Set", "Connecting to port " & filter_wheel_driver_settings_.com_port_ & "@" & filter_wheel_driver_settings_.baud_rate_)

                        'connect to the device:
                        serial_connection_.PortName = filter_wheel_driver_settings_.com_port_   'use port number read earlier
                        serial_connection_.Speed = filter_wheel_driver_settings_.baud_rate_     'use baud rate read earlier

                        'open serial connection.
                        'catch exception if the com port is not available.
                        Try
                            serial_connection_.Connected = True 'enable serial communication.
                            serial_connection_.ClearBuffers()
                        Catch ex As ASCOM.Utilities.Exceptions.InvalidValueException
                            Dim err_msg As String = "Error: COM port does not exist."
                            TL.LogMessage("HardwareConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception       'TODO catch specific exception types and handle them 
                            Dim err_msg As String = "error: could not open serial connection."
                            TL.LogMessage("HardwareConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        serial_connection_.ReceiveTimeoutMs = TIMEOUT_SERIAL
                    Case connection_type_t.NETWORK
                        TL.LogMessage("IsConnected Set", "Connecting to " + filter_wheel_driver_settings_.ip_address_ + ":" & filter_wheel_driver_settings_.port_)

                        Try
                            tcp_connection_ = New Net.Sockets.TcpClient

                            tcp_connection_.Connect(filter_wheel_driver_settings_.ip_address_, filter_wheel_driver_settings_.port_)
                        Catch ex As ArgumentNullException
                            Dim err_msg As String = "error: the host name is null."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ArgumentOutOfRangeException
                            Dim err_msg As String = "error: the port number is outside the valid range of " & System.Net.IPEndPoint.MinPort & " to " & System.Net.IPEndPoint.MaxPort & "."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As System.Net.Sockets.SocketException
                            Dim err_msg As String = "error: an error occured when accessing the socket."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As ObjectDisposedException
                            Dim err_msg As String = "error: the TCP client is closed."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        Catch ex As Exception
                            Dim err_msg As String = "error: could not open TCP connection."
                            TL.LogMessage("IsConnected Set", err_msg)
                            Throw New ASCOM.DriverException(err_msg, ex)
                        End Try

                        tcp_connection_.SendTimeout = TIMEOUT_TCP
                        tcp_connection_.ReceiveTimeout = TIMEOUT_TCP
                End Select

                'Arduino is reset when opening a Serial Connection. 
                'bootup time might increase in the future. 
                'Wait for it to boot up...
                System.Threading.Thread.Sleep(SET_CONNECTED_DELAY)
            Else
                Select Case filter_wheel_driver_settings_.connection_type_
                    Case connection_type_t.SERIAL
                        'disconnected from the device.
                        Try
                            serial_connection_.Connected = False
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("IsConnected Set", "error: could not close serial connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("IsConnected Set", "Disconnected from port " + filter_wheel_driver_settings_.com_port_)
                    Case connection_type_t.NETWORK
                        Try
                            tcp_connection_.Close()
                        Catch Exc As System.TimeoutException
                            TL.LogMessage("IsConnected Set", "error: could not close TCP connection")
                            Throw New DriverException(Exc.Message, Exc)
                        End Try

                        TL.LogMessage("IsConnected Set", "Disconnecting from " & filter_wheel_driver_settings_.ip_address_ & ":" & filter_wheel_driver_settings_.port_)
                End Select
            End If
        End Set
    End Property

    ''' <summary>
    ''' Use this function to throw an exception if we aren't connected to the hardware
    ''' </summary>
    ''' <param name="message"></param>
    Private Sub CheckConnected(ByVal message As String)
        If Not IsConnected Then
            Throw New NotConnectedException(message)
        End If
    End Sub

    ''' <summary>
    ''' Read the device configuration from the ASCOM Profile store
    ''' </summary>
    Friend Sub ReadProfile()
        TL.LogMessage("ReadProfile", "Start")

        Using driverProfile As New Profile()
            driverProfile.DeviceType = "FilterWheel"
            Dim buffer As String    'buffer for strings that are parsed to enum values.

            'communication and traceState.
            filterWheelDriverSettings.trace_state_ = Convert.ToBoolean(driverProfile.GetValue(driverID, filterWheelProfileNames.traceStateProfileName, String.Empty, filterWheelDefaultValues.traceStateDefault))

            buffer = driverProfile.GetValue(driverID, filterWheelProfileNames.connectionTypeProfileName, String.Empty, filterWheelDefaultValues.connectionTypeDefault)
            filter_wheel_driver_settings_.connection_type_ = [Enum].Parse(GetType(connection_type_t), buffer)

            filter_wheel_driver_settings_.com_port_ = driverProfile.GetValue(driverID, filterWheelProfileNames.comPortProfileName, String.Empty, filterWheelDefaultValues.comPortDefault)
            filter_wheel_driver_settings_.baud_rate_ = driverProfile.GetValue(driverID, filterWheelProfileNames.baudRateProfileName, String.Empty, filterWheelDefaultValues.baudRateDefault)

            filter_wheel_driver_settings_.ip_address_ = driverProfile.GetValue(driverID, filterWheelProfileNames.ipAddressProfileName, String.Empty, filterWheelDefaultValues.ipAddressDefault)
            filter_wheel_driver_settings_.port_ = driverProfile.GetValue(driverID, filterWheelProfileNames.portProfileName, String.Empty, filterWheelDefaultValues.portDefault)

            With filter_wheel_driver_settings_.filter_wheel_settings_
                'filter wheel drive settings.
                .acceleration_ = driverProfile.GetValue(driverID, filterWheelProfileNames.accelerationProfileName, String.Empty, filterWheelDefaultValues.accelerationDefault)
                .max_speed_ = driverProfile.GetValue(driverID, filterWheelProfileNames.maxSpeedProfileName, String.Empty, filterWheelDefaultValues.maxSpeedDefault)
                .gear_ratio_ = driverProfile.GetValue(driverID, filterWheelProfileNames.gearRatioProfileName, String.Empty, filterWheelDefaultValues.gearRatioDefault)
                .number_of_steps_ = driverProfile.GetValue(driverID, filterWheelProfileNames.stepsPerRevProfileName, String.Empty, filterWheelDefaultValues.numberOfStepsDefault)

                buffer = driverProfile.GetValue(driverID, filterWheelProfileNames.stepTypeProfileName, String.Empty, filterWheelDefaultValues.stepTypeDefault)
                .step_type_ = [Enum].Parse(GetType(step_type_t), buffer)

                'home switches and automatic stepper release.
                .home_switch_avail_ = Convert.ToBoolean(driverProfile.GetValue(driverID, filterWheelProfileNames.HSAProfileName, String.Empty, filterWheelDefaultValues.HSADefault))
                .home_switch_nr_ = driverProfile.GetValue(driverID, filterWheelProfileNames.HSNProfileName, String.Empty, filterWheelDefaultValues.HSNDefault)
                .startup_auto_home_ = Convert.ToBoolean(driverProfile.GetValue(driverID, filterWheelProfileNames.SAHProfileName, String.Empty, filterWheelDefaultValues.SAHDefault))
                .home_timout_ = driverProfile.GetValue(driverID, filterWheelProfileNames.HTOProfileName, String.Empty, filterWheelDefaultValues.HTODefault)
                .automatic_stepper_release_ = Convert.ToBoolean(driverProfile.GetValue(driverID, filterWheelProfileNames.ASRProfileName, String.Empty, filterWheelDefaultValues.ASRDefault))

                'endstop switch and endstop hard stop settings.
                .endstop_lower_enabled_ = driverProfile.GetValue(driverID, filterWheelProfileNames.endStopLowerEnabledProfileName, String.Empty, filterWheelDefaultValues.endstopLowerEnabledDefault)
                .endstop_lower_ = driverProfile.GetValue(driverID, filterWheelProfileNames.endStopLowerProfileName, String.Empty, filterWheelDefaultValues.endstopLowerDefault)
                .endstop_lower_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.endstopLowerInvertedProfileName, String.Empty, filterWheelDefaultValues.endstopLowerInvertedDefault)
                .endstop_upper_enabled_ = driverProfile.GetValue(driverID, filterWheelProfileNames.endStopUpperEnabledProfileName, String.Empty, filterWheelDefaultValues.endstopUpperEnabledDefault)
                .endstop_upper_ = driverProfile.GetValue(driverID, filterWheelProfileNames.endStopUpperProfileName, String.Empty, filterWheelDefaultValues.endstopUpperDefault)
                .endstop_upper_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.endstopUpperInvertedProfileName, String.Empty, filterWheelDefaultValues.endstopUpperInvertedDefault)
                .endstop_hard_stop_ = driverProfile.GetValue(driverID, filterWheelProfileNames.endstopHardStop, String.Empty, filterWheelDefaultValues.endstopHardStopDefault)

                'stepper driver settings.
                buffer = driverProfile.GetValue(driverID, filterWheelProfileNames.motorDriverProfileName, String.Empty, filterWheelDefaultValues.motorDriverDefault)
                .motor_driver_ = [Enum].Parse(GetType(motor_driver_t), buffer)

                .i2c_address_ = driverProfile.GetValue(driverID, filterWheelProfileNames.I2CProfileName, String.Empty, filterWheelDefaultValues.I2CAddressDefault)
                .terminal_ = driverProfile.GetValue(driverID, filterWheelProfileNames.terminalProfileName, String.Empty, filterWheelDefaultValues.terminalDefault)

                .pin_1_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin1ProfileName, String.Empty, filterWheelDefaultValues.pin1Default)
                .pin_2_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin2ProfileName, String.Empty, filterWheelDefaultValues.pin2Default)
                .pin_3_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin3ProfileName, String.Empty, filterWheelDefaultValues.pin3Default)
                .pin_4_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin4ProfileName, String.Empty, filterWheelDefaultValues.pin4Default)
                .pin_5_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin5ProfileName, String.Empty, filterWheelDefaultValues.pin5Default)
                .pin_6_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin6ProfileName, String.Empty, filterWheelDefaultValues.pin6Default)

                .pin_1_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin1InvertedProfileName, String.Empty, filterWheelDefaultValues.pin1InvertedDefault)
                .pin_2_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin2InvertedProfileName, String.Empty, filterWheelDefaultValues.pin2InvertedDefault)
                .pin_3_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin3InvertedProfileName, String.Empty, filterWheelDefaultValues.pin3InvertedDefault)
                .pin_4_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin4InvertedProfileName, String.Empty, filterWheelDefaultValues.pin4InvertedDefault)
                .pin_5_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin5InvertedProfileName, String.Empty, filterWheelDefaultValues.pin5InvertedDefault)
                .pin_6_inverted_ = driverProfile.GetValue(driverID, filterWheelProfileNames.pin6InvertedProfileName, String.Empty, filterWheelDefaultValues.pin6InvertedDefault)

                'filter holder and filter information.
                buffer = driverProfile.GetValue(driverID, filterWheelProfileNames.FilterHolderTypeProfileName, String.Empty, filterWheelDefaultValues.filterHolderTypeDefault)
                .filter_holder_type_ = [Enum].Parse(GetType(filter_holder_type_t), buffer)

                'read temperature sensor information from profile store.
                For i As Integer = 0 To numTempSensors - 1
                    .temperature_sensors_(i) = New YAAASharedClassLibrary.YAAASensor(True)

                    With filter_wheel_driver_settings_.filter_wheel_settings_.temperature_sensors_(i).sensor_settings_
                        Try
                            .sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE

                            .sensor_device_ = driverProfile.GetValue(driverID, filterWheelProfileNames.TSDProfileName, filterWheelProfileNames.TSSubKeyProfileName & " " & i, 255)
                            .sensor_unit_ = driverProfile.GetValue(driverID, filterWheelProfileNames.TSUProfileName, filterWheelProfileNames.TSSubKeyProfileName & " " & i, 1)

                            buffer = driverProfile.GetValue(driverID, filterWheelProfileNames.TSPProfileName, filterWheelProfileNames.TSSubKeyProfileName & " " & i, "0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF")
                            .sensor_parameters_ = YAAASharedClassLibrary.hexToByteArray(buffer)
                        Catch exc As Exception
                            TL.LogMessage("ReadProfile", "error reading Temperature Sensor Data of sensor " & i & ": " & exc.ToString)
                        End Try
                    End With
                Next

                'read number of filters
                Dim number_of_filters As Short = driverProfile.GetValue(driverID, filterWheelProfileNames.numberOfFiltersProfileName, String.Empty, filterWheelDefaultValues.nrOfFiltersDefault)

                Dim filters(number_of_filters - 1) As FilterWheel.filterSettings

                For i As Integer = 0 To number_of_filters - 1
                    Dim name As String = driverProfile.GetValue(driverID, filterWheelProfileNames.fwNamesProfileName, filterWheelProfileNames.FilterSubKeyProfileName & " " & i, filterWheelDefaultValues.fwNamesDefault)
                    Dim type As String = driverProfile.GetValue(driverID, filterWheelProfileNames.fwTypesProfileName, filterWheelProfileNames.FilterSubKeyProfileName & " " & i, filterWheelDefaultValues.fwTypesDefault)
                    Dim rfu As String = driverProfile.GetValue(driverID, filterWheelProfileNames.fwRFUProfileName, filterWheelProfileNames.FilterSubKeyProfileName & " " & i, filterWheelDefaultValues.fwRFUDefault)
                    Dim focus_offset As Integer = driverProfile.GetValue(driverID, filterWheelProfileNames.fwFocusOffsetsProfileName, filterWheelProfileNames.FilterSubKeyProfileName & " " & i, filterWheelDefaultValues.fwFocusOffsetsDefault)
                    Dim position_offset As Integer = driverProfile.GetValue(driverID, filterWheelProfileNames.fwPositionOffsetsProfileName, filterWheelProfileNames.FilterSubKeyProfileName & " " & i, filterWheelDefaultValues.fwPositionOffsetDefault)

                    filters(i) = New filterSettings(name, type, rfu, focus_offset, position_offset)
                Next

                .filters_ = filters.Clone
            End With
        End Using

        TL.LogMessage("ReadProfile", "End")
    End Sub

    ''' <summary>
    ''' Write the device configuration to the  ASCOM  Profile store
    ''' </summary>
    Friend Sub WriteProfile()
        Using driverProfile As New Profile()
            driverProfile.DeviceType = "FilterWheel"

            driverProfile.WriteValue(driverID, filterWheelProfileNames.traceStateProfileName, filterWheelDriverSettings.trace_state_.ToString())

            driverProfile.WriteValue(driverID, filterWheelProfileNames.connectionTypeProfileName, filter_wheel_driver_settings_.connection_type_.ToString)

            driverProfile.WriteValue(driverID, filterWheelProfileNames.ipAddressProfileName, filter_wheel_driver_settings_.ip_address_)
            driverProfile.WriteValue(driverID, filterWheelProfileNames.portProfileName, filter_wheel_driver_settings_.port_)

            driverProfile.WriteValue(driverID, filterWheelProfileNames.comPortProfileName, filter_wheel_driver_settings_.com_port_.ToString())
            driverProfile.WriteValue(driverID, filterWheelProfileNames.baudRateProfileName, filter_wheel_driver_settings_.baud_rate_.ToString())

            With filter_wheel_driver_settings_.filter_wheel_settings_                'filter wheel drive settings.
                driverProfile.WriteValue(driverID, filterWheelProfileNames.accelerationProfileName, .acceleration_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.gearRatioProfileName, .gear_ratio_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.maxSpeedProfileName, .max_speed_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.stepsPerRevProfileName, .number_of_steps_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.stepTypeProfileName, .step_type_.ToString())

                'home switches and automatic stepper release.
                driverProfile.WriteValue(driverID, filterWheelProfileNames.ASRProfileName, .automatic_stepper_release_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.HSAProfileName, .home_switch_avail_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.HSNProfileName, .home_switch_nr_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.SAHProfileName, .startup_auto_home_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.HTOProfileName, .home_timout_.ToString())

                'endstop switch and endstop hard stop settings.
                driverProfile.WriteValue(driverID, filterWheelProfileNames.endStopLowerEnabledProfileName, .endstop_lower_enabled_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.endStopLowerProfileName, .endstop_lower_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.endstopLowerInvertedProfileName, .endstop_lower_inverted_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.endStopUpperEnabledProfileName, .endstop_upper_enabled_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.endStopUpperProfileName, .endstop_upper_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.endstopUpperInvertedProfileName, .endstop_upper_inverted_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.endstopHardStop, .endstop_hard_stop_.ToString())

                'stepper driver settings.
                driverProfile.WriteValue(driverID, filterWheelProfileNames.motorDriverProfileName, .motor_driver_)

                driverProfile.WriteValue(driverID, filterWheelProfileNames.I2CProfileName, .i2c_address_.ToString())
                driverProfile.WriteValue(driverID, filterWheelProfileNames.terminalProfileName, .terminal_.ToString())

                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin1ProfileName, .pin_1_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin2ProfileName, .pin_2_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin3ProfileName, .pin_3_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin4ProfileName, .pin_4_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin5ProfileName, .pin_5_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin6ProfileName, .pin_6_)

                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin1InvertedProfileName, .pin_1_inverted_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin2InvertedProfileName, .pin_2_inverted_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin3InvertedProfileName, .pin_3_inverted_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin4InvertedProfileName, .pin_4_inverted_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin5InvertedProfileName, .pin_5_inverted_)
                driverProfile.WriteValue(driverID, filterWheelProfileNames.pin6InvertedProfileName, .pin_6_inverted_)

                'filter holder and filter information.
                driverProfile.WriteValue(driverID, filterWheelProfileNames.FilterHolderTypeProfileName, .filter_holder_type_.ToString())

                driverProfile.WriteValue(driverID, filterWheelProfileNames.numberOfFiltersProfileName, .filters_.Length.ToString())

                'TODO fix filter saving
                For i As Integer = 0 To .filters_.Length - 1
                    driverProfile.WriteValue(driverID, filterWheelProfileNames.fwNamesProfileName, .filters_(i).name_, filterWheelProfileNames.FilterSubKeyProfileName & " " & i)
                    driverProfile.WriteValue(driverID, filterWheelProfileNames.fwTypesProfileName, .filters_(i).type_.ToString, filterWheelProfileNames.FilterSubKeyProfileName & " " & i)
                    driverProfile.WriteValue(driverID, filterWheelProfileNames.fwFocusOffsetsProfileName, .filters_(i).focus_offset_, filterWheelProfileNames.FilterSubKeyProfileName & " " & i)
                    driverProfile.WriteValue(driverID, filterWheelProfileNames.fwPositionOffsetsProfileName, .filters_(i).position_offset_, filterWheelProfileNames.FilterSubKeyProfileName & " " & i)
                    driverProfile.WriteValue(driverID, filterWheelProfileNames.fwRFUProfileName, .filters_(i).RFU_, filterWheelProfileNames.FilterSubKeyProfileName & " " & i)
                Next

                'temperature sensor settings
                For i As Integer = 0 To numTempSensors - 1
                    With filter_wheel_driver_settings_.filter_wheel_settings_.temperature_sensors_(i).sensor_settings_
                        .sensor_type_ = YAAASharedClassLibrary.YAAASensor.sensor_type_t.SENSOR_TEMPERATURE

                        driverProfile.WriteValue(driverID, filterWheelProfileNames.TSDProfileName, .sensor_device_, filterWheelProfileNames.TSSubKeyProfileName & " " & i)
                        driverProfile.WriteValue(driverID, filterWheelProfileNames.TSUProfileName, .sensor_unit_, filterWheelProfileNames.TSSubKeyProfileName & " " & i)

                        Dim parameters_string As String = ""
                        For j As Integer = 0 To .sensor_parameters_.Length - 1
                            parameters_string += Hex(.sensor_parameters_(j))
                            parameters_string += " "
                        Next

                        driverProfile.WriteValue(driverID, filterWheelProfileNames.TSPProfileName, parameters_string, filterWheelProfileNames.TSSubKeyProfileName & " " & i)
                    End With
                Next
            End With
        End Using
    End Sub

#End Region

#Region "Reading and Writing from / to the Ardunio's EEPROM"

    Public Sub readEEPROM()
        CheckConnected("readEEPROM")

        TL.LogMessage("ReadEEPROM", "Start")

        filter_wheel_driver_settings_EEPROM_ = filter_wheel_driver_settings_.Clone()

        Dim buffer As Short

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.acceleration_, "ACCELERATION")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.max_speed_, "MAX_SPEED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.gear_ratio_, "GEAR_RATIO")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.number_of_steps_, "NUMBER_OF_STEPS")
        readSetting(buffer, "STEP_TYPE")
        filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.step_type_ = [Enum].Parse(GetType(step_type_t), buffer)

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.automatic_stepper_release_, "AUTOMATIC_STEPPER_RELEASE")

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_switch_avail_, "HOME_SWITCH_ENABLED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_switch_nr_, "HOME_SWITCH")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_switch_inverted_, "HOME_SWITCH_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.startup_auto_home_, "STARTUP_AUTO_HOME")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_timout_, "HOME_TIMEOUT")

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_lower_enabled_, "ENDSTOP_LOWER_ENABLED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_lower_, "ENDSTOP_LOWER")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_lower_inverted_, "ENDSTOP_LOWER_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_upper_enabled_, "ENDSTOP_UPPER_ENABLED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_upper_, "ENDSTOP_UPPER")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_upper_inverted_, "ENDSTOP_UPPER_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_hard_stop_, "ENDSTOP_HARD_STOP")

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_lower_enabled_, "LIMIT_LOWER_ENABLED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_lower_, "LIMIT_LOWER")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_upper_enabled_, "LIMIT_UPPER_ENABLED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_upper_, "LIMIT_UPPER")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_hard_stop_, "LIMIT_HARD_STOP")

        readSetting(buffer, "MOTOR_DRIVER_TYPE")
        filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.motor_driver_ = [Enum].Parse(GetType(motor_driver_t), buffer)

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.i2c_address_, "I2C_ADDRESS")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.terminal_, "TERMINAL")

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_1_, "PIN_1")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_2_, "PIN_2")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_3_, "PIN_3")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_4_, "PIN_4")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_5_, "PIN_5")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_6_, "PIN_6")

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_1_inverted_, "PIN_1_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_2_inverted_, "PIN_2_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_3_inverted_, "PIN_3_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_4_inverted_, "PIN_4_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_5_inverted_, "PIN_5_INVERTED")
        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_6_inverted_, "PIN_6_INVERTED")

        readSetting(buffer, "FILTER_HOLDER_TYPE")
        filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.filter_holder_type_ = [Enum].Parse(GetType(filter_holder_type_t), buffer)

        Dim number_of_filters As Short
        readSetting(number_of_filters, "NUMBER_OF_FILTERS")

        ReDim Preserve filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.filters_(number_of_filters - 1)

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.filters_, "FILTER_INFO")

        readSetting(filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.temperature_sensors_, "TEMPERATURE_SENSOR,")
    End Sub

    Public Sub writeEEPROM()
        CheckConnected("writeEEPROM")

        WriteProfile()

        TL.LogMessage("WriteEEPROM", "Start")

        With filter_wheel_driver_settings_.filter_wheel_settings_
            writeSetting(.acceleration_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.acceleration_, "ACCELERATION")
            writeSetting(.max_speed_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.max_speed_, "MAX_SPEED")
            writeSetting(.gear_ratio_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.gear_ratio_, "GEAR_RATIO")
            writeSetting(.number_of_steps_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.number_of_steps_, "NUMBER_OF_STEPS")
            writeSetting(.step_type_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.step_type_, "STEP_TYPE")

            writeSetting(.automatic_stepper_release_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.automatic_stepper_release_, "AUTOMATIC_STEPPER_RELEASE")
            writeSetting(.home_switch_avail_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_switch_avail_, "HOME_SWITCH_ENABLED")
            writeSetting(.home_switch_nr_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_switch_nr_, "HOME_SWITCH")
            writeSetting(.home_switch_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_switch_inverted_, "HOME_SWITCH_INVERTED")
            writeSetting(.startup_auto_home_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.startup_auto_home_, "STARTUP_AUTO_HOME")
            writeSetting(.home_timout_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.home_timout_, "HOME_TIMEOUT")

            writeSetting(.endstop_lower_enabled_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_lower_enabled_, "ENDSTOP_LOWER_ENABLED")
            writeSetting(.endstop_lower_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_lower_, "ENDSTOP_LOWER")
            writeSetting(.endstop_lower_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_lower_inverted_, "ENDSTOP_LOWER_INVERTED")
            writeSetting(.endstop_upper_enabled_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_upper_enabled_, "ENDSTOP_UPPER_ENABLED")
            writeSetting(.endstop_upper_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_upper_, "ENDSTOP_UPPER")
            writeSetting(.endstop_upper_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_upper_inverted_, "ENDSTOP_UPPER_INVERTED")
            writeSetting(.endstop_hard_stop_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.endstop_hard_stop_, "ENDSTOP_HARD_STOP")

            writeSetting(.limit_lower_enabled_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_lower_enabled_, "LIMIT_LOWER_ENABLED")
            writeSetting(.limit_lower_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_lower_, "LIMIT_LOWER")
            writeSetting(.limit_upper_enabled_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_upper_enabled_, "LIMIT_UPPER_ENABLED")
            writeSetting(.limit_upper_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_upper_, "LIMIT_UPPER")
            writeSetting(.limit_hard_stop_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.limit_hard_stop_, "LIMIT_HARD_STOP")

            writeSetting(.motor_driver_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.motor_driver_, "MOTOR_DRIVER_TYPE")

            writeSetting(.i2c_address_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.i2c_address_, "I2C_ADDRESS")
            writeSetting(.terminal_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.terminal_, "TERMINAL")

            writeSetting(.pin_1_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_1_, "PIN_1")
            writeSetting(.pin_2_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_2_, "PIN_2")
            writeSetting(.pin_3_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_3_, "PIN_3")
            writeSetting(.pin_4_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_4_, "PIN_4")
            writeSetting(.pin_5_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_5_, "PIN_5")
            writeSetting(.pin_6_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_6_, "PIN_6")

            writeSetting(.pin_1_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_1_inverted_, "PIN_1_INVERTED")
            writeSetting(.pin_2_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_2_inverted_, "PIN_2_INVERTED")
            writeSetting(.pin_3_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_3_inverted_, "PIN_3_INVERTED")
            writeSetting(.pin_4_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_4_inverted_, "PIN_4_INVERTED")
            writeSetting(.pin_5_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_5_inverted_, "PIN_5_INVERTED")
            writeSetting(.pin_6_inverted_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.pin_6_inverted_, "PIN_6_INVERTED")

            writeSetting(.filter_holder_type_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.filter_holder_type_, "FILTER_HOLDER_TYPE")

            Dim buffer As Integer
            writeSetting(.filters_.Length, buffer, "NUMBER_OF_FILTERS")
            writeSetting(.filters_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.filters_, "FILTER_INFO")

            writeSetting(.temperature_sensors_, filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.temperature_sensors_, "TEMPERATURE_SENSOR")
        End With

        filter_wheel_driver_settings_EEPROM_ = filter_wheel_driver_settings_.Clone
    End Sub

#Region "reading settings from arduino"

    Private Sub readSetting(ByRef value As Boolean, setting As String)
        value = Not (readSettings(setting).Equals("0"))
    End Sub

    Private Sub readSetting(ByRef value As Byte, setting As String)
        value = CByte(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Short, setting As String)
        value = CShort(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As UShort, setting As String)
        value = CUShort(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Integer, setting As String)
        value = CInt(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As UInteger, setting As String)
        value = CUInt(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Single, setting As String)
        value = Val(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As Double, setting As String)
        value = Val(readSettings(setting))
    End Sub

    Private Sub readSetting(ByRef value As String, setting As String)
        value = readSettings(setting)
    End Sub

    Private Sub readSetting(ByRef value() As filterSettings, setting As String)
        For i As Integer = 0 To value.Length - 1
            Dim strBuffer As String = ""

            readSetting(strBuffer, setting & "," & i)

            Dim filter As filterSettings = New filterSettings(strBuffer) 'stringToFilter(strBuffer)

            'copy new values "by hand" do avoid overwriting filter names.
            value(i).type_ = filter.type_
            value(i).RFU_ = filter.RFU_
            value(i).focus_offset_ = filter.focus_offset_
            value(i).position_offset_ = filter.position_offset_
        Next
    End Sub

    Private Sub readSetting(ByRef value() As YAAASharedClassLibrary.YAAASensor, setting As String)
        For i As Integer = 0 To value.Length - 1
            Dim str_buffer As String = readSettings("TEMPERATURE_SENSOR," & i.ToString)
            value(i) = New YAAASharedClassLibrary.YAAASensor(str_buffer)
        Next
    End Sub

    Private Function readSettings(setting As String)
        Dim message As String
        Dim response As String

        message = "SETUP,"          'setup command 
        message &= "GET,"           'read command
        message &= setting          'setting to read

        response = sendCommand(message)

        response = response.Replace("SETUP,", "")      'trim the rest of the message.

        Return response
    End Function

#End Region

#Region "writing settings to arduino"

    Private Sub writeSetting(value As Boolean, ByRef eeprom_value As Boolean, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(Convert.ToInt32(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Byte, ByRef eeprom_value As Byte, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Short, ByRef eeprom_value As Short, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As UShort, ByRef eeprom_value As UShort, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Integer, ByRef eeprom_value As Integer, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As UInteger, ByRef eeprom_value As UInteger, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString(), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Single, ByRef eeprom_value As Single, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As Double, ByRef eeprom_value As Double, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(value.ToString("F7", CultureInfo.InvariantCulture), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As filter_holder_type_t, ByRef eeprom_value As filter_holder_type_t, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(CByte(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As motor_driver_t, ByRef eeprom_value As motor_driver_t, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(CByte(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value As step_type_t, ByRef eeprom_value As step_type_t, setting As String)
        If EEPROM_force_write_ Or value <> eeprom_value Then
            writeSettings(CByte(value), setting)
            eeprom_value = value
        End If
    End Sub

    Private Sub writeSetting(value() As filterSettings, ByRef eeprom_value() As filterSettings, setting As String)
        For i As Integer = 0 To value.Length - 1
            'checks (among others) if filters have been added
            If EEPROM_force_write_ OrElse i > UBound(eeprom_value) OrElse value(i) <> eeprom_value(i) Then
                'writeSettings(filterToString(value(i)), setting & "," & i)
                writeSettings(value(i).toArduinoString(), setting & "," & i)

                'resize eeprom_value array if necessary
                If i > UBound(eeprom_value) Then
                    ReDim Preserve eeprom_value(i)
                End If

                'copy current values to EEPROM filter
                eeprom_value(i) = value(i).Clone
            End If
        Next
    End Sub

    Private Sub writeSetting(value() As YAAASharedClassLibrary.YAAASensor, ByRef eeprom_value() As YAAASharedClassLibrary.YAAASensor, setting As String)
        For i As Integer = 0 To numTempSensors - 1
            If EEPROM_force_write_ Or value(i) <> eeprom_value(i) Then
                writeSettings(i & "," & value(i).toArduinoString(), setting)
                eeprom_value(i) = value(i).Clone
            End If
        Next
    End Sub

    Private Function writeSettings(value As String, setting As String)
        Dim message As String
        Dim response As String

        message = "SETUP,"              'setup command 
        message &= "SET,"               'read command
        message &= setting & ","        'setting to write
        message &= value.ToString()     'value to set

        response = sendCommand(message)

        response = response.Replace("SETUP,", "")      'trim the rest of the message.

        Return response
    End Function

#End Region

#Region "additional friend functions"

    'Public Sub changeFilterNumber(number_of_filters As Short)
    '    ReDim Preserve filter_wheel_driver_settings_.filter_wheel_settings_.filters_(number_of_filters - 1)
    '    ReDim Preserve filter_wheel_driver_settings_EEPROM_.filter_wheel_settings_.filters_(number_of_filters - 1)
    '    'TODO 2nd redim necessary?
    'End Sub

    'function to return a filterWheelDriverSettings struct containing the current driver settings.
    '@param eeprom true to get EEPROM settings, false to get normal settings
    Friend Function getDriverSettings(Optional eeprom As Boolean = False) As filterWheelDriverSettings
        Dim return_value As filterWheelDriverSettings

        If eeprom = True Then
            return_value = filter_wheel_driver_settings_EEPROM_.Clone
        Else
            return_value = filter_wheel_driver_settings_.Clone
        End If

        Return return_value
    End Function

    'overwrites the driver's settings object with the provided object.
    '@param settings as filterWheelDriverSettings
    '@param eeprom true to set EEPROM settings, false to set normal settings
    Friend Sub setDriverSettings(settings As filterWheelDriverSettings, Optional eeprom As Boolean = False)
        If eeprom = True Then
            filter_wheel_driver_settings_EEPROM_ = settings.Clone
        Else
            filter_wheel_driver_settings_ = settings.Clone
        End If
    End Sub

#Region "Reading / writing settings fron / to .xml files"

    'opens a file and attempts to read a filterWheelDriverSettings structure from it. 
    '@param file_name full path and file name to file
    '@return driver settings stored in file as filterWheelDriverSettings
    Friend Function xmlToDriverSettings(file_name As String) As filterWheelDriverSettings
        Dim return_value As filterWheelDriverSettings = New filterWheelDriverSettings(True)

        Dim fs As New FileStream(file_name, FileMode.Open, FileAccess.Read)
        Dim reader As XmlDictionaryReader = XmlDictionaryReader.CreateTextReader(fs, New XmlDictionaryReaderQuotas())
        Dim serializer = New DataContractSerializer(GetType(filterWheelDriverSettings))

        return_value = CType(serializer.ReadObject(reader, True), filterWheelDriverSettings)
        reader.Close()
        fs.Close()

        Return return_value
    End Function

    'serializes a filterWheelDriverSettings struct and saves it to a file.
    '@param file_name full path and file name of file
    '@param settings focuser driver settings as filterWheelDriverSettings
    Friend Sub xmlFromDriverSettings(file_name As String, settings As filterWheelDriverSettings)
        Dim serializer As New DataContractSerializer(GetType(filterWheelDriverSettings))
        Dim writer As New FileStream(file_name, FileMode.Create)

        serializer.WriteObject(writer, settings)
        writer.Close()
    End Sub

#End Region

#End Region

#End Region

    Private Function sendCommand(command As String) As String
        Dim message As String = ""
        Dim response As String = ""

        'wrap command string.
        message = "<"               'beginning of the message
        message &= "W,"             'device identifier
        message &= command          'command
        message &= ">"              'end of message

        'TL.LogMessage("SERIAL TRANSMIT", message)

        Select Case filter_wheel_driver_settings_.connection_type_
            Case connection_type_t.SERIAL
                'transmit message to arduino
                serial_connection_.Transmit(message)

                response = serial_connection_.ReceiveTerminated(">")

            Case connection_type_t.NETWORK
                'log sent command
                TL.LogMessage("TCP TRANSMIT", message)

                'copy message to buffer and send buffer
                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(message)
                tcp_connection_.GetStream.Write(sendBytes, 0, sendBytes.Length)

                'receive response into receive buffer
                Dim recLength As Integer    'number of bytes received
                Dim recBytes(tcp_connection_.ReceiveBufferSize) As Byte
                recLength = tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))

                'convert receive buffer to string
                response = Encoding.ASCII.GetString(recBytes, 0, recLength)

                'log response from Arduino
                TL.LogMessage("TCP RECEIVE", response)

                'read \cr\lf sent by arduino (the arduino sends these in a seperate packet).
                tcp_connection_.GetStream.Read(recBytes, 0, CInt(tcp_connection_.ReceiveBufferSize))

        End Select

        response = response.Trim

        'TL.LogMessage("SERIAL RECEIVE", response)

        response = response.Replace("<W,", "")
        response = response.Replace(">", "")

        'check the response for serial communication errors.
        checkMessageError(response)

        Return response
    End Function

    Private Sub checkMessageError(message As String)
        Dim error_pattern As String = "ERROR_\d+$"
        Dim match As RegularExpressions.Match = RegularExpressions.Regex.Match(message, error_pattern)

        'do nothing if no error message has been received.
        If Not match.Success Then Exit Sub

        'parse the error code when an error message has been received.
        Dim error_code As serial_error_code_t = serial_error_code_t.ERROR_NONE
        Dim error_code_known As Boolean = False

        Dim error_message As String = match.ToString
        TL.LogMessage("checkMessageError", error_message)

        error_code_known = [Enum].TryParse(error_message.Replace("ERROR_", ""), error_code)

        If Not error_code_known Then
            TL.LogMessage("checkMessageError", "unknown error: " & error_message)
            Throw New ASCOM.DriverException()
        Else
            Select Case error_code
                Case serial_error_code_t.ERROR_NONE
                    TL.LogMessage("checkMessageError", serial_error_code_t.ERROR_NONE.ToString)
                    Exit Sub
                Case Else
                    TL.LogMessage("checkMessageError", error_code.ToString)
                    Throw New ASCOM.DriverException("Serial Communication Error: " & error_code.ToString)
            End Select
        End If
    End Sub

#Region "additional private functions"

    Private Function atHome() As Boolean
        CheckConnected("AtHome")

        'sends at home query to Ardunio. 
        'Arduino will return TRUE ("1") when both axes are at the home position.
        Dim return_value As Boolean
        Dim response As String

        response = sendCommand("AT_HOME,GET")
        response = response.Replace("AT_HOME,", "")

        return_value = Not CInt(response).Equals(0)

        TL.LogMessage("AtHome", "Get - " & return_value.ToString())
        Return return_value
    End Function

    Private Function isHoming() As Boolean
        CheckConnected("IsHoming")

        'sends at home query to Ardunio. 
        'Arduino will return TRUE ("1") when both axes are at the home position.
        Dim return_value As Boolean
        Dim response As String

        response = sendCommand("FIND_HOME,GET")
        response = response.Replace("FIND_HOME,", "")

        return_value = Not CInt(response).Equals(0)

        TL.LogMessage("IsHoming", "Get - " & return_value.ToString())
        Return return_value
    End Function

    Private Sub findHome()
        CheckConnected("Find Home")

        If Not filter_wheel_driver_settings_.filter_wheel_settings_.home_switch_avail_ Then
            TL.LogMessage("findHome", "Error: no home switch available")
            Return
        End If

        Dim response As String
        response = sendCommand("FIND_HOME,SET")
        response = response.Replace("FIND_HOME,", "")

        'While Not atHome()
        While isHoming()
            'wait until homing process is finished.
            System.Threading.Thread.Sleep(100)

            'todo: implement timeout / error handling?
        End While

        'homing is successful when the filter wheel ends up at its home position.
        Dim successful As Boolean = atHome()

        TL.LogMessage("findHome", "finished: " & successful.ToString)

        If Not successful Then
            Throw New ASCOM.DriverException("Error: findHome not successful")
        End If
    End Sub

    Private Function getSwitchState() As String
        Dim response As String
        response = sendCommand("SWITCH_STATE,GET")
        response = response.Replace("SWITCH_STATE,", "")

        Return response
    End Function

    Private Function isMeasuringHomeOffset() As Boolean
        CheckConnected("Is Measuring Home Offset")

        'sends at home query to Ardunio. 
        'Arduino will return TRUE ("1") when both axes are at the home position.
        Dim return_value As Boolean
        Dim response As String

        response = sendCommand("MEASURE_HOME_OFFSET,GET")
        response = response.Replace("MEASURE_HOME_OFFSET,", "")

        return_value = Not CInt(response).Equals(0)

        TL.LogMessage("isMeasuringHomeOffset", "Get - " & return_value.ToString())
        Return return_value
    End Function

    Private Sub measureHomeOffset()
        CheckConnected("Measure Home Offset")

        'If Not home_switch_avail_ Then
        '    TL.LogMessage("measureHomeOffset", "Error: no home switch available")
        '    Return
        'End If

        Dim response As String
        response = sendCommand("MEASURE_HOME_OFFSET,SET")
        response = response.Replace("MEASURE_HOME_OFFSET,", "")

        While isMeasuringHomeOffset()
            'wait until homing process is finished.
            System.Threading.Thread.Sleep(100)

            'todo: implement timeout / error handling?
        End While

        TL.LogMessage("measureHomeOffset", "finished")
    End Sub

    Private Sub setCurrentFilter(value As Short)
        CheckConnected("setCurrentFilter")

        ''throw an exception if an invalid filter number is given.
        'If ((value < 0) Or (value > fwNames.Length - 1)) Then
        '    TL.LogMessage("Position Set", "Throwing InvalidValueException")
        '    Throw New InvalidValueException("Position", value.ToString(), "0 to " & (fwNames.Length - 1).ToString())
        'End If

        TL.LogMessage("setCurrentFilter", value.ToString())

        Dim response As String
        Dim message As String

        message = "CURRENT_FILTER,SET,"
        message &= value.ToString

        response = sendCommand(message)
        response = response.Replace("CURRENT_FILTER,", "")

        Dim return_value As Integer = CInt(response)

        If (response = 0) Then
            TL.LogMessage("setCurrentFilter", "successful")

        ElseIf (response = 1) Then
            'when an error occured, throw an exception
            TL.LogMessage("setCurrentFilter", "Error: invalid filter number: " & value)
            'Throw New DriverException("setFilter Error: invalid filter number: " & value)
            Throw New InvalidValueException("setFilter Error: invalid filter number: " & value)

        ElseIf (response = 4) Then
            TL.LogMessage("setCurrentFilter", "successful: already at requested position")
        End If

        fwPosition = value
    End Sub

#End Region

End Class
