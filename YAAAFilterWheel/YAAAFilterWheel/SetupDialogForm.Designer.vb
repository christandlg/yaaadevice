<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SetupDialogForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.ButtonWriteEEPROM = New System.Windows.Forms.Button()
        Me.ButtonReadEEPROM = New System.Windows.Forms.Button()
        Me.chkTrace = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.ComboBoxComPorts = New System.Windows.Forms.ComboBox()
        Me.ComboBoxBaudRate = New System.Windows.Forms.ComboBox()
        Me.CheckBoxEEPROMForceWrite = New System.Windows.Forms.CheckBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBoxFilterHolderType = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DataGridViewFilters = New System.Windows.Forms.DataGridView()
        Me.NumericUpDownNrFilters = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.RadioButtonNetwork = New System.Windows.Forms.RadioButton()
        Me.RadioButtonSerial = New System.Windows.Forms.RadioButton()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBoxPort = New System.Windows.Forms.TextBox()
        Me.TextBoxIPAddress = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxStepType = New System.Windows.Forms.ComboBox()
        Me.CheckBoxPin6Inverted = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin2 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBoxPin5 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBoxAccel = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.TextBoxMaxSpeed = New System.Windows.Forms.TextBox()
        Me.TextBoxStepsRev = New System.Windows.Forms.TextBox()
        Me.TextBoxPin1 = New System.Windows.Forms.TextBox()
        Me.TextBoxPin6 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ComboBoxTerminal = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.CheckBoxPin2Inverted = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPin3Inverted = New System.Windows.Forms.CheckBox()
        Me.ComboBoxMotorDriver = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.CheckBoxPin1Inverted = New System.Windows.Forms.CheckBox()
        Me.ComboBoxI2CAddress = New System.Windows.Forms.ComboBox()
        Me.CheckBoxASR = New System.Windows.Forms.CheckBox()
        Me.TextBoxPin3 = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin4Inverted = New System.Windows.Forms.CheckBox()
        Me.TextBoxGearRatio = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxPin4 = New System.Windows.Forms.TextBox()
        Me.CheckBoxPin5Inverted = New System.Windows.Forms.CheckBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.GroupBox31 = New System.Windows.Forms.GroupBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBoxUpperLimit = New System.Windows.Forms.TextBox()
        Me.CheckBoxUpperLimitEnabled = New System.Windows.Forms.CheckBox()
        Me.GroupBox30 = New System.Windows.Forms.GroupBox()
        Me.TextBoxLowerLimit = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.CheckBoxLowerLimitEnabled = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHardStopLimit = New System.Windows.Forms.CheckBox()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.GroupBox28 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopUpperInverted = New System.Windows.Forms.CheckBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBoxEndstopUpperPin = New System.Windows.Forms.TextBox()
        Me.CheckBoxEndstopUpperEnabled = New System.Windows.Forms.CheckBox()
        Me.GroupBox29 = New System.Windows.Forms.GroupBox()
        Me.CheckBoxEndstopLowerInverted = New System.Windows.Forms.CheckBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.TextBoxEndstopLowerPin = New System.Windows.Forms.TextBox()
        Me.CheckBoxEndStopLowerEnabled = New System.Windows.Forms.CheckBox()
        Me.CheckBoxEndstopHardStop = New System.Windows.Forms.CheckBox()
        Me.TabHomingManualControl = New System.Windows.Forms.TabPage()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.ButtonReadSwitchState = New System.Windows.Forms.Button()
        Me.CheckBoxHSTriggered = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESLTriggered = New System.Windows.Forms.CheckBox()
        Me.CheckBoxESUTriggered = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TextBoxHTO = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.CheckBoxHSI = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHSA = New System.Windows.Forms.CheckBox()
        Me.TextBoxHSN = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CheckBoxSAH = New System.Windows.Forms.CheckBox()
        Me.TabPageTemperatureSensors = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanelTemperatureSensors = New System.Windows.Forms.FlowLayoutPanel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.ButtonImportSettings = New System.Windows.Forms.Button()
        Me.ButtonExportSettings = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridViewFilters, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDownNrFilters, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox31.SuspendLayout()
        Me.GroupBox30.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox28.SuspendLayout()
        Me.GroupBox29.SuspendLayout()
        Me.TabHomingManualControl.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPageTemperatureSensors.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(383, 457)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'ButtonWriteEEPROM
        '
        Me.ButtonWriteEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonWriteEEPROM.Location = New System.Drawing.Point(70, 3)
        Me.ButtonWriteEEPROM.Name = "ButtonWriteEEPROM"
        Me.ButtonWriteEEPROM.Size = New System.Drawing.Size(62, 34)
        Me.ButtonWriteEEPROM.TabIndex = 65
        Me.ButtonWriteEEPROM.Text = "Write EEPROM"
        '
        'ButtonReadEEPROM
        '
        Me.ButtonReadEEPROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonReadEEPROM.Location = New System.Drawing.Point(3, 3)
        Me.ButtonReadEEPROM.Name = "ButtonReadEEPROM"
        Me.ButtonReadEEPROM.Size = New System.Drawing.Size(61, 34)
        Me.ButtonReadEEPROM.TabIndex = 64
        Me.ButtonReadEEPROM.Text = "Read EEPROM"
        '
        'chkTrace
        '
        Me.chkTrace.AutoSize = True
        Me.chkTrace.Location = New System.Drawing.Point(392, 6)
        Me.chkTrace.Name = "chkTrace"
        Me.chkTrace.Size = New System.Drawing.Size(69, 17)
        Me.chkTrace.TabIndex = 41
        Me.chkTrace.Text = "Trace on"
        Me.chkTrace.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label3.Location = New System.Drawing.Point(180, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 45
        Me.Label3.Text = "Baud Rate"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(6, 23)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(50, 13)
        Me.label2.TabIndex = 44
        Me.label2.Text = "Com Port"
        '
        'ComboBoxComPorts
        '
        Me.ComboBoxComPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxComPorts.FormattingEnabled = True
        Me.ComboBoxComPorts.Location = New System.Drawing.Point(81, 19)
        Me.ComboBoxComPorts.Name = "ComboBoxComPorts"
        Me.ComboBoxComPorts.Size = New System.Drawing.Size(89, 21)
        Me.ComboBoxComPorts.TabIndex = 67
        Me.ToolTip1.SetToolTip(Me.ComboBoxComPorts, "COM port the Arduino is connected to.")
        '
        'ComboBoxBaudRate
        '
        Me.ComboBoxBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxBaudRate.FormattingEnabled = True
        Me.ComboBoxBaudRate.Items.AddRange(New Object() {"300", "600", "1200", "2400", "4800", "9600", "14400", "19200", "28800", "38400", "57600", "115200"})
        Me.ComboBoxBaudRate.Location = New System.Drawing.Point(244, 20)
        Me.ComboBoxBaudRate.Name = "ComboBoxBaudRate"
        Me.ComboBoxBaudRate.Size = New System.Drawing.Size(71, 21)
        Me.ComboBoxBaudRate.TabIndex = 75
        '
        'CheckBoxEEPROMForceWrite
        '
        Me.CheckBoxEEPROMForceWrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBoxEEPROMForceWrite.AutoSize = True
        Me.CheckBoxEEPROMForceWrite.Location = New System.Drawing.Point(3, 7)
        Me.CheckBoxEEPROMForceWrite.Name = "CheckBoxEEPROMForceWrite"
        Me.CheckBoxEEPROMForceWrite.Size = New System.Drawing.Size(130, 17)
        Me.CheckBoxEEPROMForceWrite.TabIndex = 76
        Me.CheckBoxEEPROMForceWrite.Text = "Force EEPROM Write"
        Me.CheckBoxEEPROMForceWrite.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabHomingManualControl)
        Me.TabControl1.Controls.Add(Me.TabPageTemperatureSensors)
        Me.TabControl1.Location = New System.Drawing.Point(7, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(529, 404)
        Me.TabControl1.TabIndex = 77
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.chkTrace)
        Me.TabPage1.Controls.Add(Me.PictureBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(521, 378)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.ComboBoxFilterHolderType)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.NumericUpDownNrFilters)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 197)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(509, 175)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filter Control"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label4.Location = New System.Drawing.Point(9, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 13)
        Me.Label4.TabIndex = 79
        Me.Label4.Text = "Filter Holder Type"
        '
        'ComboBoxFilterHolderType
        '
        Me.ComboBoxFilterHolderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterHolderType.FormattingEnabled = True
        Me.ComboBoxFilterHolderType.Location = New System.Drawing.Point(167, 19)
        Me.ComboBoxFilterHolderType.Name = "ComboBoxFilterHolderType"
        Me.ComboBoxFilterHolderType.Size = New System.Drawing.Size(71, 21)
        Me.ComboBoxFilterHolderType.TabIndex = 78
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label12.Location = New System.Drawing.Point(258, 22)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(86, 13)
        Me.Label12.TabIndex = 62
        Me.Label12.Text = "Number of Filters"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.DataGridViewFilters)
        Me.Panel1.Location = New System.Drawing.Point(6, 46)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(497, 123)
        Me.Panel1.TabIndex = 78
        '
        'DataGridViewFilters
        '
        Me.DataGridViewFilters.AllowUserToAddRows = False
        Me.DataGridViewFilters.AllowUserToDeleteRows = False
        Me.DataGridViewFilters.AllowUserToResizeColumns = False
        Me.DataGridViewFilters.AllowUserToResizeRows = False
        Me.DataGridViewFilters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewFilters.Location = New System.Drawing.Point(3, 3)
        Me.DataGridViewFilters.Name = "DataGridViewFilters"
        Me.DataGridViewFilters.Size = New System.Drawing.Size(491, 117)
        Me.DataGridViewFilters.TabIndex = 77
        '
        'NumericUpDownNrFilters
        '
        Me.NumericUpDownNrFilters.Location = New System.Drawing.Point(416, 20)
        Me.NumericUpDownNrFilters.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownNrFilters.Name = "NumericUpDownNrFilters"
        Me.NumericUpDownNrFilters.Size = New System.Drawing.Size(71, 20)
        Me.NumericUpDownNrFilters.TabIndex = 70
        Me.NumericUpDownNrFilters.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Controls.Add(Me.GroupBox5)
        Me.GroupBox2.Controls.Add(Me.GroupBox4)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(333, 185)
        Me.GroupBox2.TabIndex = 76
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Communication"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 13)
        Me.Label7.TabIndex = 77
        Me.Label7.Text = "Connect Using:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.RadioButtonNetwork)
        Me.Panel2.Controls.Add(Me.RadioButtonSerial)
        Me.Panel2.Location = New System.Drawing.Point(104, 19)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(223, 28)
        Me.Panel2.TabIndex = 2
        '
        'RadioButtonNetwork
        '
        Me.RadioButtonNetwork.AutoSize = True
        Me.RadioButtonNetwork.Location = New System.Drawing.Point(99, 3)
        Me.RadioButtonNetwork.Name = "RadioButtonNetwork"
        Me.RadioButtonNetwork.Size = New System.Drawing.Size(65, 17)
        Me.RadioButtonNetwork.TabIndex = 79
        Me.RadioButtonNetwork.TabStop = True
        Me.RadioButtonNetwork.Text = "Network"
        Me.RadioButtonNetwork.UseVisualStyleBackColor = True
        '
        'RadioButtonSerial
        '
        Me.RadioButtonSerial.AutoSize = True
        Me.RadioButtonSerial.Location = New System.Drawing.Point(3, 3)
        Me.RadioButtonSerial.Name = "RadioButtonSerial"
        Me.RadioButtonSerial.Size = New System.Drawing.Size(51, 17)
        Me.RadioButtonSerial.TabIndex = 78
        Me.RadioButtonSerial.TabStop = True
        Me.RadioButtonSerial.Text = "Serial"
        Me.RadioButtonSerial.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBoxPort)
        Me.GroupBox5.Controls.Add(Me.TextBoxIPAddress)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Location = New System.Drawing.Point(6, 124)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(321, 55)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Network"
        '
        'TextBoxPort
        '
        Me.TextBoxPort.Location = New System.Drawing.Point(244, 19)
        Me.TextBoxPort.Name = "TextBoxPort"
        Me.TextBoxPort.Size = New System.Drawing.Size(71, 20)
        Me.TextBoxPort.TabIndex = 78
        '
        'TextBoxIPAddress
        '
        Me.TextBoxIPAddress.Location = New System.Drawing.Point(81, 19)
        Me.TextBoxIPAddress.Name = "TextBoxIPAddress"
        Me.TextBoxIPAddress.Size = New System.Drawing.Size(89, 20)
        Me.TextBoxIPAddress.TabIndex = 77
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label19.Location = New System.Drawing.Point(180, 22)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(26, 13)
        Me.Label19.TabIndex = 76
        Me.Label19.Text = "Port"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 22)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(58, 13)
        Me.Label17.TabIndex = 76
        Me.Label17.Text = "IP Address"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ComboBoxComPorts)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.label2)
        Me.GroupBox4.Controls.Add(Me.ComboBoxBaudRate)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 63)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(321, 55)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Serial"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = Global.ASCOM.YAAADevice.My.Resources.Resources.ASCOM
        Me.PictureBox1.Location = New System.Drawing.Point(467, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 56)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.ComboBoxStepType)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin6Inverted)
        Me.TabPage2.Controls.Add(Me.TextBoxPin2)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.TextBoxPin5)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.Label47)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.TextBoxAccel)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.Label50)
        Me.TabPage2.Controls.Add(Me.TextBoxMaxSpeed)
        Me.TabPage2.Controls.Add(Me.TextBoxStepsRev)
        Me.TabPage2.Controls.Add(Me.TextBoxPin1)
        Me.TabPage2.Controls.Add(Me.TextBoxPin6)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.ComboBoxTerminal)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin2Inverted)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin3Inverted)
        Me.TabPage2.Controls.Add(Me.ComboBoxMotorDriver)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label46)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin1Inverted)
        Me.TabPage2.Controls.Add(Me.ComboBoxI2CAddress)
        Me.TabPage2.Controls.Add(Me.CheckBoxASR)
        Me.TabPage2.Controls.Add(Me.TextBoxPin3)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin4Inverted)
        Me.TabPage2.Controls.Add(Me.TextBoxGearRatio)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.TextBoxPin4)
        Me.TabPage2.Controls.Add(Me.CheckBoxPin5Inverted)
        Me.TabPage2.Controls.Add(Me.Label48)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(521, 378)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Drive Control"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 78)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(92, 13)
        Me.Label6.TabIndex = 135
        Me.Label6.Text = "Motor Driver Type"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label10.Location = New System.Drawing.Point(6, 9)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 13)
        Me.Label10.TabIndex = 60
        Me.Label10.Text = "Acceleration"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label1.Location = New System.Drawing.Point(6, 285)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 129
        Me.Label1.Text = "Pin 4 / MS1"
        '
        'ComboBoxStepType
        '
        Me.ComboBoxStepType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxStepType.DropDownWidth = 200
        Me.ComboBoxStepType.FormattingEnabled = True
        Me.ComboBoxStepType.Location = New System.Drawing.Point(141, 125)
        Me.ComboBoxStepType.Name = "ComboBoxStepType"
        Me.ComboBoxStepType.Size = New System.Drawing.Size(200, 21)
        Me.ComboBoxStepType.TabIndex = 134
        '
        'CheckBoxPin6Inverted
        '
        Me.CheckBoxPin6Inverted.AutoSize = True
        Me.CheckBoxPin6Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin6Inverted.Location = New System.Drawing.Point(243, 336)
        Me.CheckBoxPin6Inverted.Name = "CheckBoxPin6Inverted"
        Me.CheckBoxPin6Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin6Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin6Inverted.TabIndex = 122
        Me.CheckBoxPin6Inverted.UseVisualStyleBackColor = True
        '
        'TextBoxPin2
        '
        Me.TextBoxPin2.Location = New System.Drawing.Point(142, 229)
        Me.TextBoxPin2.Name = "TextBoxPin2"
        Me.TextBoxPin2.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin2.TabIndex = 114
        Me.ToolTip1.SetToolTip(Me.TextBoxPin2, "Stepper motor pin 2 or stepper driver direction pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and A4988" &
        " driver only.")
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 155)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(112, 13)
        Me.Label16.TabIndex = 133
        Me.Label16.Text = "I�C Address / Terminal"
        '
        'TextBoxPin5
        '
        Me.TextBoxPin5.Location = New System.Drawing.Point(142, 307)
        Me.TextBoxPin5.Name = "TextBoxPin5"
        Me.TextBoxPin5.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin5.TabIndex = 117
        Me.ToolTip1.SetToolTip(Me.TextBoxPin5, "Stepper driver Microstep Select 2 pin. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label15.Location = New System.Drawing.Point(6, 232)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(84, 13)
        Me.Label15.TabIndex = 127
        Me.Label15.Text = "Pin 2 / Direction"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label47.Location = New System.Drawing.Point(6, 337)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(64, 13)
        Me.Label47.TabIndex = 131
        Me.Label47.Text = "Pin 6 / MS3"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 206)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 13)
        Me.Label14.TabIndex = 128
        Me.Label14.Text = "Pin 1 / Step"
        '
        'TextBoxAccel
        '
        Me.TextBoxAccel.Location = New System.Drawing.Point(94, 6)
        Me.TextBoxAccel.Name = "TextBoxAccel"
        Me.TextBoxAccel.Size = New System.Drawing.Size(71, 20)
        Me.TextBoxAccel.TabIndex = 50
        Me.ToolTip1.SetToolTip(Me.TextBoxAccel, "Stepper motor acceleration in steps / second^2.")
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 128)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 63
        Me.Label13.Text = "Step Type"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(223, 187)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(46, 13)
        Me.Label50.TabIndex = 125
        Me.Label50.Text = "Inverted"
        '
        'TextBoxMaxSpeed
        '
        Me.TextBoxMaxSpeed.Location = New System.Drawing.Point(94, 30)
        Me.TextBoxMaxSpeed.Name = "TextBoxMaxSpeed"
        Me.TextBoxMaxSpeed.Size = New System.Drawing.Size(71, 20)
        Me.TextBoxMaxSpeed.TabIndex = 49
        Me.ToolTip1.SetToolTip(Me.TextBoxMaxSpeed, "Maximum stepper motor speed in steps / second.")
        '
        'TextBoxStepsRev
        '
        Me.TextBoxStepsRev.Location = New System.Drawing.Point(270, 6)
        Me.TextBoxStepsRev.Name = "TextBoxStepsRev"
        Me.TextBoxStepsRev.Size = New System.Drawing.Size(71, 20)
        Me.TextBoxStepsRev.TabIndex = 46
        Me.ToolTip1.SetToolTip(Me.TextBoxStepsRev, "Number of steps per revolution of the stepper motor.")
        '
        'TextBoxPin1
        '
        Me.TextBoxPin1.Location = New System.Drawing.Point(142, 203)
        Me.TextBoxPin1.Name = "TextBoxPin1"
        Me.TextBoxPin1.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin1.TabIndex = 113
        Me.ToolTip1.SetToolTip(Me.TextBoxPin1, "Stepper motor pin 1 or Stepper Driver Enable pin. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and A4988 d" &
        "river only.")
        '
        'TextBoxPin6
        '
        Me.TextBoxPin6.Location = New System.Drawing.Point(142, 333)
        Me.TextBoxPin6.Name = "TextBoxPin6"
        Me.TextBoxPin6.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin6.TabIndex = 118
        Me.ToolTip1.SetToolTip(Me.TextBoxPin6, "Stepper driver Microstep Select 3 pin. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988 driver only.")
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 33)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 13)
        Me.Label11.TabIndex = 61
        Me.Label11.Text = "Max Speed"
        '
        'ComboBoxTerminal
        '
        Me.ComboBoxTerminal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTerminal.DropDownWidth = 49
        Me.ComboBoxTerminal.FormattingEnabled = True
        Me.ComboBoxTerminal.Items.AddRange(New Object() {"1", "2"})
        Me.ComboBoxTerminal.Location = New System.Drawing.Point(215, 152)
        Me.ComboBoxTerminal.Name = "ComboBoxTerminal"
        Me.ComboBoxTerminal.Size = New System.Drawing.Size(49, 21)
        Me.ComboBoxTerminal.TabIndex = 108
        Me.ToolTip1.SetToolTip(Me.ComboBoxTerminal, "Adarfruit Motor Shield V2 only.")
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(147, 187)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(62, 13)
        Me.Label18.TabIndex = 126
        Me.Label18.Text = "Pin Number"
        '
        'CheckBoxPin2Inverted
        '
        Me.CheckBoxPin2Inverted.AutoSize = True
        Me.CheckBoxPin2Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin2Inverted.Location = New System.Drawing.Point(243, 232)
        Me.CheckBoxPin2Inverted.Name = "CheckBoxPin2Inverted"
        Me.CheckBoxPin2Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin2Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin2Inverted.TabIndex = 120
        Me.ToolTip1.SetToolTip(Me.CheckBoxPin2Inverted, "Setting has no effect when using four wire steppers." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Activating this c" &
        "heckbox inverts stepper motor direction.")
        Me.CheckBoxPin2Inverted.UseVisualStyleBackColor = True
        '
        'CheckBoxPin3Inverted
        '
        Me.CheckBoxPin3Inverted.AutoSize = True
        Me.CheckBoxPin3Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin3Inverted.Location = New System.Drawing.Point(243, 258)
        Me.CheckBoxPin3Inverted.Name = "CheckBoxPin3Inverted"
        Me.CheckBoxPin3Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin3Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin3Inverted.TabIndex = 121
        Me.ToolTip1.SetToolTip(Me.CheckBoxPin3Inverted, "Setting has no effect when using four wire steppers." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: Rising" &
        " edge initiates step." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Falling edge initiates step.")
        Me.CheckBoxPin3Inverted.UseVisualStyleBackColor = True
        '
        'ComboBoxMotorDriver
        '
        Me.ComboBoxMotorDriver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxMotorDriver.DropDownWidth = 200
        Me.ComboBoxMotorDriver.FormattingEnabled = True
        Me.ComboBoxMotorDriver.Location = New System.Drawing.Point(141, 75)
        Me.ComboBoxMotorDriver.Name = "ComboBoxMotorDriver"
        Me.ComboBoxMotorDriver.Size = New System.Drawing.Size(200, 21)
        Me.ComboBoxMotorDriver.TabIndex = 106
        Me.ToolTip1.SetToolTip(Me.ComboBoxMotorDriver, "Type of stepper motor or stepper motor driver.")
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label8.Location = New System.Drawing.Point(196, 33)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Gear Ratio"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(6, 259)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(75, 13)
        Me.Label46.TabIndex = 130
        Me.Label46.Text = "Pin 3 / Enable"
        '
        'CheckBoxPin1Inverted
        '
        Me.CheckBoxPin1Inverted.AutoSize = True
        Me.CheckBoxPin1Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin1Inverted.Location = New System.Drawing.Point(243, 206)
        Me.CheckBoxPin1Inverted.Name = "CheckBoxPin1Inverted"
        Me.CheckBoxPin1Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin1Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin1Inverted.TabIndex = 119
        Me.ToolTip1.SetToolTip(Me.CheckBoxPin1Inverted, "Setting has no effect when using four wire steppers." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A4988:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: Enable" &
        "d = HIGH" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Checked: Enabled = LOW")
        Me.CheckBoxPin1Inverted.UseVisualStyleBackColor = True
        '
        'ComboBoxI2CAddress
        '
        Me.ComboBoxI2CAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxI2CAddress.DropDownWidth = 67
        Me.ComboBoxI2CAddress.FormattingEnabled = True
        Me.ComboBoxI2CAddress.Items.AddRange(New Object() {"0x60", "0x61", "0x62", "0x63", "0x64", "0x65", "0x66", "0x67", "0x68", "0x69", "0x6A", "0x6B", "0x6C", "0x6D", "0x6E", "0x6F"})
        Me.ComboBoxI2CAddress.Location = New System.Drawing.Point(142, 152)
        Me.ComboBoxI2CAddress.Name = "ComboBoxI2CAddress"
        Me.ComboBoxI2CAddress.Size = New System.Drawing.Size(67, 21)
        Me.ComboBoxI2CAddress.TabIndex = 107
        Me.ToolTip1.SetToolTip(Me.ComboBoxI2CAddress, "Adarfruit Motor Shield V2 only.")
        '
        'CheckBoxASR
        '
        Me.CheckBoxASR.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASR.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxASR.Location = New System.Drawing.Point(6, 102)
        Me.CheckBoxASR.Name = "CheckBoxASR"
        Me.CheckBoxASR.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxASR.Size = New System.Drawing.Size(252, 17)
        Me.CheckBoxASR.TabIndex = 54
        Me.CheckBoxASR.Text = "Automatic Stepper Release"
        Me.ToolTip1.SetToolTip(Me.CheckBoxASR, "enabling automatic stepper release decreases power consumption and makes manually" &
        " moving the filter holder possible.")
        Me.CheckBoxASR.UseVisualStyleBackColor = True
        '
        'TextBoxPin3
        '
        Me.TextBoxPin3.Location = New System.Drawing.Point(142, 255)
        Me.TextBoxPin3.Name = "TextBoxPin3"
        Me.TextBoxPin3.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin3.TabIndex = 115
        Me.ToolTip1.SetToolTip(Me.TextBoxPin3, "Stepper motor pin 3 or stepper driver direction pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper and A4988" &
        " driver only.")
        '
        'CheckBoxPin4Inverted
        '
        Me.CheckBoxPin4Inverted.AutoSize = True
        Me.CheckBoxPin4Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin4Inverted.Location = New System.Drawing.Point(243, 284)
        Me.CheckBoxPin4Inverted.Name = "CheckBoxPin4Inverted"
        Me.CheckBoxPin4Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin4Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin4Inverted.TabIndex = 123
        Me.ToolTip1.SetToolTip(Me.CheckBoxPin4Inverted, "Setting has no effect when using four wire steppers.")
        Me.CheckBoxPin4Inverted.UseVisualStyleBackColor = True
        '
        'TextBoxGearRatio
        '
        Me.TextBoxGearRatio.Location = New System.Drawing.Point(270, 30)
        Me.TextBoxGearRatio.Name = "TextBoxGearRatio"
        Me.TextBoxGearRatio.Size = New System.Drawing.Size(71, 20)
        Me.TextBoxGearRatio.TabIndex = 47
        Me.ToolTip1.SetToolTip(Me.TextBoxGearRatio, "Filter Wheel drive gear ratio.")
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(196, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 13)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "Steps / Rev"
        '
        'TextBoxPin4
        '
        Me.TextBoxPin4.Location = New System.Drawing.Point(142, 281)
        Me.TextBoxPin4.Name = "TextBoxPin4"
        Me.TextBoxPin4.Size = New System.Drawing.Size(67, 20)
        Me.TextBoxPin4.TabIndex = 116
        Me.ToolTip1.SetToolTip(Me.TextBoxPin4, "Stepper motor pin 4 or stepper driver Microstep Select 1 pin." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Four wire stepper " &
        "and A4988 driver only.")
        '
        'CheckBoxPin5Inverted
        '
        Me.CheckBoxPin5Inverted.AutoSize = True
        Me.CheckBoxPin5Inverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5Inverted.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxPin5Inverted.Location = New System.Drawing.Point(243, 310)
        Me.CheckBoxPin5Inverted.Name = "CheckBoxPin5Inverted"
        Me.CheckBoxPin5Inverted.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxPin5Inverted.Size = New System.Drawing.Size(15, 14)
        Me.CheckBoxPin5Inverted.TabIndex = 124
        Me.CheckBoxPin5Inverted.UseVisualStyleBackColor = True
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(6, 310)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(64, 13)
        Me.Label48.TabIndex = 132
        Me.Label48.Text = "Pin 5 / MS2"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox14)
        Me.TabPage3.Controls.Add(Me.GroupBox12)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(521, 378)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Endstops and Limits"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.GroupBox31)
        Me.GroupBox14.Controls.Add(Me.GroupBox30)
        Me.GroupBox14.Controls.Add(Me.CheckBoxHardStopLimit)
        Me.GroupBox14.Location = New System.Drawing.Point(6, 185)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(175, 170)
        Me.GroupBox14.TabIndex = 17
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Movement Limits"
        '
        'GroupBox31
        '
        Me.GroupBox31.Controls.Add(Me.Label25)
        Me.GroupBox31.Controls.Add(Me.TextBoxUpperLimit)
        Me.GroupBox31.Controls.Add(Me.CheckBoxUpperLimitEnabled)
        Me.GroupBox31.Location = New System.Drawing.Point(6, 83)
        Me.GroupBox31.Name = "GroupBox31"
        Me.GroupBox31.Size = New System.Drawing.Size(159, 61)
        Me.GroupBox31.TabIndex = 22
        Me.GroupBox31.TabStop = False
        Me.GroupBox31.Text = "Upper"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(6, 38)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(60, 13)
        Me.Label25.TabIndex = 12
        Me.Label25.Text = "Limit (Deg.)"
        '
        'TextBoxUpperLimit
        '
        Me.TextBoxUpperLimit.Location = New System.Drawing.Point(85, 35)
        Me.TextBoxUpperLimit.Name = "TextBoxUpperLimit"
        Me.TextBoxUpperLimit.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxUpperLimit.TabIndex = 11
        '
        'CheckBoxUpperLimitEnabled
        '
        Me.CheckBoxUpperLimitEnabled.AutoSize = True
        Me.CheckBoxUpperLimitEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxUpperLimitEnabled.Location = New System.Drawing.Point(6, 18)
        Me.CheckBoxUpperLimitEnabled.Name = "CheckBoxUpperLimitEnabled"
        Me.CheckBoxUpperLimitEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxUpperLimitEnabled.TabIndex = 7
        Me.CheckBoxUpperLimitEnabled.Text = "Enabled"
        Me.CheckBoxUpperLimitEnabled.UseVisualStyleBackColor = True
        '
        'GroupBox30
        '
        Me.GroupBox30.Controls.Add(Me.TextBoxLowerLimit)
        Me.GroupBox30.Controls.Add(Me.Label24)
        Me.GroupBox30.Controls.Add(Me.CheckBoxLowerLimitEnabled)
        Me.GroupBox30.Location = New System.Drawing.Point(6, 16)
        Me.GroupBox30.Name = "GroupBox30"
        Me.GroupBox30.Size = New System.Drawing.Size(159, 61)
        Me.GroupBox30.TabIndex = 21
        Me.GroupBox30.TabStop = False
        Me.GroupBox30.Text = "Lower"
        '
        'TextBoxLowerLimit
        '
        Me.TextBoxLowerLimit.Location = New System.Drawing.Point(85, 31)
        Me.TextBoxLowerLimit.Name = "TextBoxLowerLimit"
        Me.TextBoxLowerLimit.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxLowerLimit.TabIndex = 10
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(6, 34)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(60, 13)
        Me.Label24.TabIndex = 5
        Me.Label24.Text = "Limit (Deg.)"
        '
        'CheckBoxLowerLimitEnabled
        '
        Me.CheckBoxLowerLimitEnabled.AutoSize = True
        Me.CheckBoxLowerLimitEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxLowerLimitEnabled.Location = New System.Drawing.Point(6, 14)
        Me.CheckBoxLowerLimitEnabled.Name = "CheckBoxLowerLimitEnabled"
        Me.CheckBoxLowerLimitEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxLowerLimitEnabled.TabIndex = 6
        Me.CheckBoxLowerLimitEnabled.Text = "Enabled"
        Me.CheckBoxLowerLimitEnabled.UseVisualStyleBackColor = True
        '
        'CheckBoxHardStopLimit
        '
        Me.CheckBoxHardStopLimit.AutoSize = True
        Me.CheckBoxHardStopLimit.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHardStopLimit.Location = New System.Drawing.Point(6, 150)
        Me.CheckBoxHardStopLimit.Name = "CheckBoxHardStopLimit"
        Me.CheckBoxHardStopLimit.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxHardStopLimit.TabIndex = 19
        Me.CheckBoxHardStopLimit.Text = "Hard Stop"
        Me.ToolTip1.SetToolTip(Me.CheckBoxHardStopLimit, "Enabled: Stops the axis instantly." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Disabled: Smooth decelleration.")
        Me.CheckBoxHardStopLimit.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.GroupBox28)
        Me.GroupBox12.Controls.Add(Me.GroupBox29)
        Me.GroupBox12.Controls.Add(Me.CheckBoxEndstopHardStop)
        Me.GroupBox12.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(175, 173)
        Me.GroupBox12.TabIndex = 16
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Endstop Switches"
        '
        'GroupBox28
        '
        Me.GroupBox28.Controls.Add(Me.CheckBoxEndstopUpperInverted)
        Me.GroupBox28.Controls.Add(Me.Label22)
        Me.GroupBox28.Controls.Add(Me.TextBoxEndstopUpperPin)
        Me.GroupBox28.Controls.Add(Me.CheckBoxEndstopUpperEnabled)
        Me.GroupBox28.Location = New System.Drawing.Point(6, 85)
        Me.GroupBox28.Name = "GroupBox28"
        Me.GroupBox28.Size = New System.Drawing.Size(159, 60)
        Me.GroupBox28.TabIndex = 23
        Me.GroupBox28.TabStop = False
        Me.GroupBox28.Text = "Upper"
        '
        'CheckBoxEndstopUpperInverted
        '
        Me.CheckBoxEndstopUpperInverted.AutoSize = True
        Me.CheckBoxEndstopUpperInverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperInverted.Location = New System.Drawing.Point(88, 17)
        Me.CheckBoxEndstopUpperInverted.Name = "CheckBoxEndstopUpperInverted"
        Me.CheckBoxEndstopUpperInverted.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperInverted.TabIndex = 21
        Me.CheckBoxEndstopUpperInverted.Text = "Inverted"
        Me.ToolTip1.SetToolTip(Me.CheckBoxEndstopUpperInverted, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopUpperInverted.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 37)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(62, 13)
        Me.Label22.TabIndex = 20
        Me.Label22.Text = "Pin Number"
        '
        'TextBoxEndstopUpperPin
        '
        Me.TextBoxEndstopUpperPin.Location = New System.Drawing.Point(85, 34)
        Me.TextBoxEndstopUpperPin.Name = "TextBoxEndstopUpperPin"
        Me.TextBoxEndstopUpperPin.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxEndstopUpperPin.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.TextBoxEndstopUpperPin, "Upper endstop switch pin number.")
        '
        'CheckBoxEndstopUpperEnabled
        '
        Me.CheckBoxEndstopUpperEnabled.AutoSize = True
        Me.CheckBoxEndstopUpperEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopUpperEnabled.Location = New System.Drawing.Point(6, 17)
        Me.CheckBoxEndstopUpperEnabled.Name = "CheckBoxEndstopUpperEnabled"
        Me.CheckBoxEndstopUpperEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopUpperEnabled.TabIndex = 7
        Me.CheckBoxEndstopUpperEnabled.Text = "Enabled"
        Me.CheckBoxEndstopUpperEnabled.UseVisualStyleBackColor = True
        '
        'GroupBox29
        '
        Me.GroupBox29.Controls.Add(Me.CheckBoxEndstopLowerInverted)
        Me.GroupBox29.Controls.Add(Me.Label40)
        Me.GroupBox29.Controls.Add(Me.TextBoxEndstopLowerPin)
        Me.GroupBox29.Controls.Add(Me.CheckBoxEndStopLowerEnabled)
        Me.GroupBox29.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox29.Name = "GroupBox29"
        Me.GroupBox29.Size = New System.Drawing.Size(159, 60)
        Me.GroupBox29.TabIndex = 22
        Me.GroupBox29.TabStop = False
        Me.GroupBox29.Text = "Lower"
        '
        'CheckBoxEndstopLowerInverted
        '
        Me.CheckBoxEndstopLowerInverted.AutoSize = True
        Me.CheckBoxEndstopLowerInverted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopLowerInverted.Location = New System.Drawing.Point(88, 16)
        Me.CheckBoxEndstopLowerInverted.Name = "CheckBoxEndstopLowerInverted"
        Me.CheckBoxEndstopLowerInverted.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndstopLowerInverted.TabIndex = 16
        Me.CheckBoxEndstopLowerInverted.Text = "Inverted"
        Me.ToolTip1.SetToolTip(Me.CheckBoxEndstopLowerInverted, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxEndstopLowerInverted.UseVisualStyleBackColor = True
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(6, 36)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(62, 13)
        Me.Label40.TabIndex = 15
        Me.Label40.Text = "Pin Number"
        '
        'TextBoxEndstopLowerPin
        '
        Me.TextBoxEndstopLowerPin.Location = New System.Drawing.Point(85, 33)
        Me.TextBoxEndstopLowerPin.Name = "TextBoxEndstopLowerPin"
        Me.TextBoxEndstopLowerPin.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxEndstopLowerPin.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.TextBoxEndstopLowerPin, "Lower endstop switch pin number.")
        '
        'CheckBoxEndStopLowerEnabled
        '
        Me.CheckBoxEndStopLowerEnabled.AutoSize = True
        Me.CheckBoxEndStopLowerEnabled.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndStopLowerEnabled.Location = New System.Drawing.Point(6, 16)
        Me.CheckBoxEndStopLowerEnabled.Name = "CheckBoxEndStopLowerEnabled"
        Me.CheckBoxEndStopLowerEnabled.Size = New System.Drawing.Size(65, 17)
        Me.CheckBoxEndStopLowerEnabled.TabIndex = 6
        Me.CheckBoxEndStopLowerEnabled.Text = "Enabled"
        Me.CheckBoxEndStopLowerEnabled.UseVisualStyleBackColor = True
        '
        'CheckBoxEndstopHardStop
        '
        Me.CheckBoxEndstopHardStop.AutoSize = True
        Me.CheckBoxEndstopHardStop.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxEndstopHardStop.Location = New System.Drawing.Point(6, 151)
        Me.CheckBoxEndstopHardStop.Name = "CheckBoxEndstopHardStop"
        Me.CheckBoxEndstopHardStop.Size = New System.Drawing.Size(74, 17)
        Me.CheckBoxEndstopHardStop.TabIndex = 18
        Me.CheckBoxEndstopHardStop.Text = "Hard Stop"
        Me.ToolTip1.SetToolTip(Me.CheckBoxEndstopHardStop, "Enabled: Stops the axis instantly." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Disabled: Smooth decelleration.")
        Me.CheckBoxEndstopHardStop.UseVisualStyleBackColor = True
        '
        'TabHomingManualControl
        '
        Me.TabHomingManualControl.Controls.Add(Me.GroupBox7)
        Me.TabHomingManualControl.Controls.Add(Me.GroupBox6)
        Me.TabHomingManualControl.Controls.Add(Me.GroupBox3)
        Me.TabHomingManualControl.Location = New System.Drawing.Point(4, 22)
        Me.TabHomingManualControl.Name = "TabHomingManualControl"
        Me.TabHomingManualControl.Size = New System.Drawing.Size(521, 378)
        Me.TabHomingManualControl.TabIndex = 5
        Me.TabHomingManualControl.Text = "Homing and ManualControl"
        Me.TabHomingManualControl.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Location = New System.Drawing.Point(3, 140)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(168, 131)
        Me.GroupBox7.TabIndex = 80
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Manual Control"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.ButtonReadSwitchState)
        Me.GroupBox6.Controls.Add(Me.CheckBoxHSTriggered)
        Me.GroupBox6.Controls.Add(Me.CheckBoxESLTriggered)
        Me.GroupBox6.Controls.Add(Me.CheckBoxESUTriggered)
        Me.GroupBox6.Location = New System.Drawing.Point(3, 277)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(168, 89)
        Me.GroupBox6.TabIndex = 115
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Switch State"
        '
        'ButtonReadSwitchState
        '
        Me.ButtonReadSwitchState.Location = New System.Drawing.Point(6, 19)
        Me.ButtonReadSwitchState.Name = "ButtonReadSwitchState"
        Me.ButtonReadSwitchState.Size = New System.Drawing.Size(57, 58)
        Me.ButtonReadSwitchState.TabIndex = 79
        Me.ButtonReadSwitchState.Text = "Read Switch State"
        Me.ToolTip1.SetToolTip(Me.ButtonReadSwitchState, "Reads the current state of the home and endstop switches." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Disabled switches will" &
        " never show as triggered.")
        Me.ButtonReadSwitchState.UseVisualStyleBackColor = True
        '
        'CheckBoxHSTriggered
        '
        Me.CheckBoxHSTriggered.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSTriggered.Enabled = False
        Me.CheckBoxHSTriggered.Location = New System.Drawing.Point(65, 65)
        Me.CheckBoxHSTriggered.Name = "CheckBoxHSTriggered"
        Me.CheckBoxHSTriggered.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxHSTriggered.TabIndex = 82
        Me.CheckBoxHSTriggered.TabStop = False
        Me.CheckBoxHSTriggered.Text = "Home Switch"
        Me.CheckBoxHSTriggered.UseVisualStyleBackColor = True
        '
        'CheckBoxESLTriggered
        '
        Me.CheckBoxESLTriggered.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESLTriggered.Enabled = False
        Me.CheckBoxESLTriggered.Location = New System.Drawing.Point(65, 19)
        Me.CheckBoxESLTriggered.Name = "CheckBoxESLTriggered"
        Me.CheckBoxESLTriggered.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESLTriggered.TabIndex = 80
        Me.CheckBoxESLTriggered.TabStop = False
        Me.CheckBoxESLTriggered.Text = "Lower Endstop"
        Me.CheckBoxESLTriggered.UseVisualStyleBackColor = True
        '
        'CheckBoxESUTriggered
        '
        Me.CheckBoxESUTriggered.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxESUTriggered.Enabled = False
        Me.CheckBoxESUTriggered.Location = New System.Drawing.Point(65, 42)
        Me.CheckBoxESUTriggered.Name = "CheckBoxESUTriggered"
        Me.CheckBoxESUTriggered.Size = New System.Drawing.Size(97, 17)
        Me.CheckBoxESUTriggered.TabIndex = 81
        Me.CheckBoxESUTriggered.TabStop = False
        Me.CheckBoxESUTriggered.Text = "Upper Endstop"
        Me.CheckBoxESUTriggered.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBoxHTO)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.CheckBoxHSI)
        Me.GroupBox3.Controls.Add(Me.CheckBoxHSA)
        Me.GroupBox3.Controls.Add(Me.TextBoxHSN)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.CheckBoxSAH)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(168, 131)
        Me.GroupBox3.TabIndex = 79
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Homing Control"
        '
        'TextBoxHTO
        '
        Me.TextBoxHTO.Location = New System.Drawing.Point(94, 104)
        Me.TextBoxHTO.Name = "TextBoxHTO"
        Me.TextBoxHTO.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxHTO.TabIndex = 69
        Me.ToolTip1.SetToolTip(Me.TextBoxHTO, "Homing operation timeout in seconds.")
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 107)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(84, 13)
        Me.Label20.TabIndex = 68
        Me.Label20.Text = "Homing Timeout"
        '
        'CheckBoxHSI
        '
        Me.CheckBoxHSI.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSI.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSI.Location = New System.Drawing.Point(7, 35)
        Me.CheckBoxHSI.Name = "CheckBoxHSI"
        Me.CheckBoxHSI.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSI.Size = New System.Drawing.Size(155, 17)
        Me.CheckBoxHSI.TabIndex = 67
        Me.CheckBoxHSI.Text = "Home Switch inverted"
        Me.ToolTip1.SetToolTip(Me.CheckBoxHSI, "Checked: triggering switch pulls input pin HIGH." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Unchecked: triggering switch pu" &
        "lls input pin LOW.")
        Me.CheckBoxHSI.UseVisualStyleBackColor = True
        '
        'CheckBoxHSA
        '
        Me.CheckBoxHSA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSA.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxHSA.Location = New System.Drawing.Point(6, 15)
        Me.CheckBoxHSA.Name = "CheckBoxHSA"
        Me.CheckBoxHSA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxHSA.Size = New System.Drawing.Size(156, 17)
        Me.CheckBoxHSA.TabIndex = 53
        Me.CheckBoxHSA.Text = "Home Switch available"
        Me.CheckBoxHSA.UseVisualStyleBackColor = True
        '
        'TextBoxHSN
        '
        Me.TextBoxHSN.Location = New System.Drawing.Point(94, 78)
        Me.TextBoxHSN.Name = "TextBoxHSN"
        Me.TextBoxHSN.Size = New System.Drawing.Size(68, 20)
        Me.TextBoxHSN.TabIndex = 52
        Me.ToolTip1.SetToolTip(Me.TextBoxHSN, "Home switch pin number.")
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 81)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 51
        Me.Label5.Text = "Home Switch Nr"
        '
        'CheckBoxSAH
        '
        Me.CheckBoxSAH.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSAH.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxSAH.Location = New System.Drawing.Point(6, 54)
        Me.CheckBoxSAH.Name = "CheckBoxSAH"
        Me.CheckBoxSAH.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CheckBoxSAH.Size = New System.Drawing.Size(156, 18)
        Me.CheckBoxSAH.TabIndex = 66
        Me.CheckBoxSAH.Text = "Startup Auto home"
        Me.CheckBoxSAH.UseVisualStyleBackColor = True
        '
        'TabPageTemperatureSensors
        '
        Me.TabPageTemperatureSensors.Controls.Add(Me.FlowLayoutPanelTemperatureSensors)
        Me.TabPageTemperatureSensors.Location = New System.Drawing.Point(4, 22)
        Me.TabPageTemperatureSensors.Name = "TabPageTemperatureSensors"
        Me.TabPageTemperatureSensors.Size = New System.Drawing.Size(521, 378)
        Me.TabPageTemperatureSensors.TabIndex = 4
        Me.TabPageTemperatureSensors.Text = "Temperature Sensors"
        Me.TabPageTemperatureSensors.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanelTemperatureSensors
        '
        Me.FlowLayoutPanelTemperatureSensors.AutoScroll = True
        Me.FlowLayoutPanelTemperatureSensors.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanelTemperatureSensors.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanelTemperatureSensors.Name = "FlowLayoutPanelTemperatureSensors"
        Me.FlowLayoutPanelTemperatureSensors.Size = New System.Drawing.Size(515, 372)
        Me.FlowLayoutPanelTemperatureSensors.TabIndex = 101
        Me.FlowLayoutPanelTemperatureSensors.WrapContents = False
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 500000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonImportSettings, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.ButtonExportSettings, 0, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(383, 425)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel2.TabIndex = 78
        '
        'ButtonImportSettings
        '
        Me.ButtonImportSettings.Location = New System.Drawing.Point(76, 3)
        Me.ButtonImportSettings.Name = "ButtonImportSettings"
        Me.ButtonImportSettings.Size = New System.Drawing.Size(67, 23)
        Me.ButtonImportSettings.TabIndex = 1
        Me.ButtonImportSettings.Text = "Import"
        Me.ButtonImportSettings.UseVisualStyleBackColor = True
        '
        'ButtonExportSettings
        '
        Me.ButtonExportSettings.Location = New System.Drawing.Point(3, 3)
        Me.ButtonExportSettings.Name = "ButtonExportSettings"
        Me.ButtonExportSettings.Size = New System.Drawing.Size(67, 23)
        Me.ButtonExportSettings.TabIndex = 0
        Me.ButtonExportSettings.Text = "Export"
        Me.ButtonExportSettings.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSplitButton1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 492)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(542, 22)
        Me.StatusStrip1.TabIndex = 79
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(132, 20)
        Me.ToolStripSplitButton1.Text = "ToolStripSplitButton1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(120, 17)
        Me.ToolStripStatusLabel2.Text = "ToolStripStatusLabel2"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(17, 17)
        Me.ToolStripStatusLabel1.Text = """"""
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel4, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.CheckBoxEEPROMForceWrite, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(7, 418)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.9863!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.0137!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(141, 73)
        Me.TableLayoutPanel3.TabIndex = 80
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonReadEEPROM, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.ButtonWriteEEPROM, 1, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 30)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(135, 40)
        Me.TableLayoutPanel4.TabIndex = 81
        '
        'SetupDialogForm
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(542, 514)
        Me.Controls.Add(Me.TableLayoutPanel3)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SetupDialogForm"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "YAAAFilterWheel Setup"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.DataGridViewFilters, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDownNrFilters, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox31.ResumeLayout(False)
        Me.GroupBox31.PerformLayout()
        Me.GroupBox30.ResumeLayout(False)
        Me.GroupBox30.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox28.ResumeLayout(False)
        Me.GroupBox28.PerformLayout()
        Me.GroupBox29.ResumeLayout(False)
        Me.GroupBox29.PerformLayout()
        Me.TabHomingManualControl.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPageTemperatureSensors.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ButtonWriteEEPROM As System.Windows.Forms.Button
    Friend WithEvents ButtonReadEEPROM As System.Windows.Forms.Button
    Friend WithEvents chkTrace As System.Windows.Forms.CheckBox
    Private WithEvents Label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxComPorts As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxBaudRate As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxEEPROMForceWrite As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox31 As System.Windows.Forms.GroupBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents TextBoxUpperLimit As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxUpperLimitEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox30 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxLowerLimit As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxLowerLimitEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxHardStopLimit As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox28 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopUpperInverted As System.Windows.Forms.CheckBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TextBoxEndstopUpperPin As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxEndstopUpperEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox29 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBoxEndstopLowerInverted As System.Windows.Forms.CheckBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents TextBoxEndstopLowerPin As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxEndStopLowerEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxEndstopHardStop As System.Windows.Forms.CheckBox
    Private WithEvents Label6 As System.Windows.Forms.Label
    Private WithEvents Label10 As System.Windows.Forms.Label
    Private WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxStepType As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxPin6Inverted As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin2 As System.Windows.Forms.TextBox
    Private WithEvents Label16 As System.Windows.Forms.Label
    Private WithEvents TextBoxPin5 As System.Windows.Forms.TextBox
    Private WithEvents Label15 As System.Windows.Forms.Label
    Private WithEvents Label47 As System.Windows.Forms.Label
    Private WithEvents Label14 As System.Windows.Forms.Label
    Private WithEvents TextBoxAccel As System.Windows.Forms.TextBox
    Private WithEvents Label13 As System.Windows.Forms.Label
    Private WithEvents Label50 As System.Windows.Forms.Label
    Private WithEvents TextBoxMaxSpeed As System.Windows.Forms.TextBox
    Private WithEvents TextBoxStepsRev As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin1 As System.Windows.Forms.TextBox
    Private WithEvents TextBoxPin6 As System.Windows.Forms.TextBox
    Private WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxTerminal As System.Windows.Forms.ComboBox
    Private WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxPin2Inverted As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPin3Inverted As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxMotorDriver As System.Windows.Forms.ComboBox
    Private WithEvents Label8 As System.Windows.Forms.Label
    Private WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxPin1Inverted As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBoxI2CAddress As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBoxASR As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxPin3 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxPin4Inverted As System.Windows.Forms.CheckBox
    Private WithEvents TextBoxGearRatio As System.Windows.Forms.TextBox
    Private WithEvents Label9 As System.Windows.Forms.Label
    Private WithEvents TextBoxPin4 As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxPin5Inverted As System.Windows.Forms.CheckBox
    Private WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Private WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents RadioButtonNetwork As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonSerial As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Private WithEvents Label19 As System.Windows.Forms.Label
    Private WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBoxPort As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxIPAddress As System.Windows.Forms.TextBox
    Friend WithEvents TabPageTemperatureSensors As TabPage
    Friend WithEvents FlowLayoutPanelTemperatureSensors As FlowLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents ButtonImportSettings As Button
    Friend WithEvents ButtonExportSettings As Button
    Friend WithEvents TabHomingManualControl As TabPage
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents ButtonReadSwitchState As Button
    Friend WithEvents CheckBoxHSTriggered As CheckBox
    Friend WithEvents CheckBoxESLTriggered As CheckBox
    Friend WithEvents CheckBoxESUTriggered As CheckBox
    Friend WithEvents GroupBox3 As GroupBox
    Private WithEvents TextBoxHTO As TextBox
    Private WithEvents Label20 As Label
    Friend WithEvents CheckBoxHSI As CheckBox
    Friend WithEvents CheckBoxHSA As CheckBox
    Private WithEvents TextBoxHSN As TextBox
    Private WithEvents Label5 As Label
    Friend WithEvents CheckBoxSAH As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Private WithEvents Label4 As Label
    Friend WithEvents ComboBoxFilterHolderType As ComboBox
    Private WithEvents Label12 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents DataGridViewFilters As DataGridView
    Friend WithEvents NumericUpDownNrFilters As NumericUpDown
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents ToolStripSplitButton1 As ToolStripDropDownButton
End Class
