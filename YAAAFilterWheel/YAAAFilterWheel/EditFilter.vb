﻿Imports ASCOM.YAAADevice.SetupDialogForm

Public Class EditFilter

    Private filter_ As FilterWheel.filterSettings

    Friend Sub New(filter As FilterWheel.filterSettings)

        ' This call is required by the designer.
        InitializeComponent()

        filter_ = filter

        'populate Filter Type combobox
        Me.ComboBoxFilterType.Items.AddRange([Enum].GetNames(GetType(FilterWheel.filter_type_t)))

        Me.TextBoxName.Text = filter_.name_
        Me.ComboBoxFilterType.SelectedItem = filter_.type_.ToString
        Me.TextBoxFocusOffset.Text = filter_.focus_offset_.ToString
        Me.TextBoxPositionOffset.Text = filter.position_offset_.ToString
        Me.TextBoxRFU.Text = filter.RFU_.ToString
    End Sub

    Private Sub EditFilter_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub OK_Button_Click(sender As Object, e As EventArgs) Handles OK_Button.Click
        acceptAndClose()
    End Sub

    Private Sub Cancel_Button_Click(sender As Object, e As EventArgs) Handles Cancel_Button.Click
        discardAndClose()
    End Sub

    Private Function getTextBoxValue(Of T As IConvertible)(text_box As TextBox, ByRef value As T) As Boolean
        'reset text box background color
        text_box.BackColor = TextBox.DefaultBackColor

        Try
            'parse text_box.Text to T
            value = DirectCast(Convert.ChangeType(text_box.Text, GetType(T)), T)
            Return True
        Catch ex As Exception
            'change text box color to indicate incorrect value
            text_box.BackColor = Drawing.Color.Orange
            Return False
        End Try
    End Function

    Friend Property Filter As FilterWheel.filterSettings
        Get
            Return filter_
        End Get
        Set(value As FilterWheel.filterSettings)
            Throw New System.NotImplementedException()
        End Set
    End Property

    Private Sub EditFilter_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            discardAndClose()
        End If

        If e.KeyCode = Keys.Return Then
            acceptAndClose()
        End If
    End Sub

    Private Sub acceptAndClose()
        Dim valid As Boolean = True

        filter_.name_ = TextBoxName.Text
        filter_.type_ = [Enum].Parse(GetType(FilterWheel.filter_type_t), ComboBoxFilterType.SelectedItem.ToString)

        valid = valid And getTextBoxValue(TextBoxFocusOffset, filter_.focus_offset_)
        valid = valid And getTextBoxValue(TextBoxPositionOffset, filter_.position_offset_)

        If Not valid Then
            MsgBox("Invalid values detected, settings have not been saved. Check all input fields with orange background and try again.", MsgBoxStyle.Critical)
            Return
        End If


        '' Persist new values of user settings to the ASCOM profile
        'If TextBoxName.Text <> "" Then name_ = TextBoxName.Text
        'If TextBoxFocusOffset.Text <> "" Then offset_ = CInt(TextBoxFocusOffset.Text)
        '
        'sdf_.ChangeFilterEntry(index_, name_, offset_)

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub discardAndClose()
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

End Class