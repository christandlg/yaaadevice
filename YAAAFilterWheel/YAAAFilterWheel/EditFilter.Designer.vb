﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditFilter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxName = New System.Windows.Forms.TextBox()
        Me.TextBoxFocusOffset = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBoxPositionOffset = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBoxFilterType = New System.Windows.Forms.ComboBox()
        Me.LabelRFU = New System.Windows.Forms.Label()
        Me.TextBoxRFU = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Filter Name"
        '
        'TextBoxName
        '
        Me.TextBoxName.Location = New System.Drawing.Point(122, 5)
        Me.TextBoxName.Name = "TextBoxName"
        Me.TextBoxName.Size = New System.Drawing.Size(149, 20)
        Me.TextBoxName.TabIndex = 1
        Me.TextBoxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBoxFocusOffset
        '
        Me.TextBoxFocusOffset.Location = New System.Drawing.Point(122, 58)
        Me.TextBoxFocusOffset.Name = "TextBoxFocusOffset"
        Me.TextBoxFocusOffset.Size = New System.Drawing.Size(149, 20)
        Me.TextBoxFocusOffset.TabIndex = 2
        Me.TextBoxFocusOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Focus Offset [µm]"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(128, 136)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 6
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 87)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Position Offset [steps]"
        '
        'TextBoxPositionOffset
        '
        Me.TextBoxPositionOffset.Location = New System.Drawing.Point(122, 84)
        Me.TextBoxPositionOffset.Name = "TextBoxPositionOffset"
        Me.TextBoxPositionOffset.Size = New System.Drawing.Size(149, 20)
        Me.TextBoxPositionOffset.TabIndex = 7
        Me.TextBoxPositionOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Filter Type"
        '
        'ComboBoxFilterType
        '
        Me.ComboBoxFilterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFilterType.FormattingEnabled = True
        Me.ComboBoxFilterType.Location = New System.Drawing.Point(122, 31)
        Me.ComboBoxFilterType.Name = "ComboBoxFilterType"
        Me.ComboBoxFilterType.Size = New System.Drawing.Size(149, 21)
        Me.ComboBoxFilterType.TabIndex = 10
        '
        'LabelRFU
        '
        Me.LabelRFU.AutoSize = True
        Me.LabelRFU.Enabled = False
        Me.LabelRFU.Location = New System.Drawing.Point(12, 113)
        Me.LabelRFU.Name = "LabelRFU"
        Me.LabelRFU.Size = New System.Drawing.Size(29, 13)
        Me.LabelRFU.TabIndex = 12
        Me.LabelRFU.Text = "RFU"
        '
        'TextBoxRFU
        '
        Me.TextBoxRFU.Enabled = False
        Me.TextBoxRFU.Location = New System.Drawing.Point(122, 110)
        Me.TextBoxRFU.Name = "TextBoxRFU"
        Me.TextBoxRFU.Size = New System.Drawing.Size(149, 20)
        Me.TextBoxRFU.TabIndex = 11
        Me.TextBoxRFU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'EditFilter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(286, 177)
        Me.Controls.Add(Me.LabelRFU)
        Me.Controls.Add(Me.TextBoxRFU)
        Me.Controls.Add(Me.ComboBoxFilterType)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TextBoxPositionOffset)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxFocusOffset)
        Me.Controls.Add(Me.TextBoxName)
        Me.Controls.Add(Me.Label1)
        Me.KeyPreview = True
        Me.Name = "EditFilter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit Filter"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxName As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxFocusOffset As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents Label3 As Label
    Friend WithEvents TextBoxPositionOffset As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents ComboBoxFilterType As ComboBox
    Friend WithEvents LabelRFU As Label
    Friend WithEvents TextBoxRFU As TextBox
End Class
