﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.labelDriverId = New System.Windows.Forms.Label()
        Me.buttonConnect = New System.Windows.Forms.Button()
        Me.buttonChoose = New System.Windows.Forms.Button()
        Me.ButtonSetFilter = New System.Windows.Forms.Button()
        Me.ButtonGetFilter = New System.Windows.Forms.Button()
        Me.TextBoxSetFilter = New System.Windows.Forms.TextBox()
        Me.LabelFilterNr = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'labelDriverId
        '
        Me.labelDriverId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.labelDriverId.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ASCOM.YAAADevice.My.MySettings.Default, "DriverId", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.labelDriverId.Location = New System.Drawing.Point(12, 37)
        Me.labelDriverId.Name = "labelDriverId"
        Me.labelDriverId.Size = New System.Drawing.Size(291, 21)
        Me.labelDriverId.TabIndex = 5
        Me.labelDriverId.Text = Global.ASCOM.YAAADevice.My.MySettings.Default.DriverId
        Me.labelDriverId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'buttonConnect
        '
        Me.buttonConnect.Location = New System.Drawing.Point(316, 36)
        Me.buttonConnect.Name = "buttonConnect"
        Me.buttonConnect.Size = New System.Drawing.Size(72, 23)
        Me.buttonConnect.TabIndex = 4
        Me.buttonConnect.Text = "Connect"
        Me.buttonConnect.UseVisualStyleBackColor = True
        '
        'buttonChoose
        '
        Me.buttonChoose.Location = New System.Drawing.Point(316, 7)
        Me.buttonChoose.Name = "buttonChoose"
        Me.buttonChoose.Size = New System.Drawing.Size(72, 23)
        Me.buttonChoose.TabIndex = 3
        Me.buttonChoose.Text = "Choose"
        Me.buttonChoose.UseVisualStyleBackColor = True
        '
        'ButtonSetFilter
        '
        Me.ButtonSetFilter.Location = New System.Drawing.Point(12, 93)
        Me.ButtonSetFilter.Name = "ButtonSetFilter"
        Me.ButtonSetFilter.Size = New System.Drawing.Size(72, 21)
        Me.ButtonSetFilter.TabIndex = 9
        Me.ButtonSetFilter.Text = "set filter"
        Me.ButtonSetFilter.UseVisualStyleBackColor = True
        '
        'ButtonGetFilter
        '
        Me.ButtonGetFilter.Location = New System.Drawing.Point(12, 65)
        Me.ButtonGetFilter.Name = "ButtonGetFilter"
        Me.ButtonGetFilter.Size = New System.Drawing.Size(72, 22)
        Me.ButtonGetFilter.TabIndex = 8
        Me.ButtonGetFilter.Text = "get filter"
        Me.ButtonGetFilter.UseVisualStyleBackColor = True
        '
        'TextBoxSetFilter
        '
        Me.TextBoxSetFilter.Location = New System.Drawing.Point(90, 94)
        Me.TextBoxSetFilter.Name = "TextBoxSetFilter"
        Me.TextBoxSetFilter.Size = New System.Drawing.Size(39, 20)
        Me.TextBoxSetFilter.TabIndex = 41
        '
        'LabelFilterNr
        '
        Me.LabelFilterNr.AutoSize = True
        Me.LabelFilterNr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelFilterNr.Location = New System.Drawing.Point(90, 70)
        Me.LabelFilterNr.Name = "LabelFilterNr"
        Me.LabelFilterNr.Size = New System.Drawing.Size(41, 15)
        Me.LabelFilterNr.TabIndex = 42
        Me.LabelFilterNr.Text = "Label1"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(318, 102)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(53, 40)
        Me.Button1.TabIndex = 43
        Me.Button1.Text = "unregister"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 233)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.LabelFilterNr)
        Me.Controls.Add(Me.TextBoxSetFilter)
        Me.Controls.Add(Me.ButtonSetFilter)
        Me.Controls.Add(Me.ButtonGetFilter)
        Me.Controls.Add(Me.labelDriverId)
        Me.Controls.Add(Me.buttonConnect)
        Me.Controls.Add(Me.buttonChoose)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents labelDriverId As System.Windows.Forms.Label
    Private WithEvents buttonConnect As System.Windows.Forms.Button
    Private WithEvents buttonChoose As System.Windows.Forms.Button
    Private WithEvents ButtonSetFilter As System.Windows.Forms.Button
    Private WithEvents ButtonGetFilter As System.Windows.Forms.Button
    Private WithEvents TextBoxSetFilter As System.Windows.Forms.TextBox
    Friend WithEvents LabelFilterNr As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
