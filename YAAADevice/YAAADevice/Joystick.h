//simple Joystick class. 
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef JOYSTICK_H_
#define JOYSTICK_H_

#include "Button.h"

#define MAX_VALUE 512

//simple joystick class.

class Joystick
{
public:
	enum joystick_axis_t
	{
		X_AXIS = 0,
		Y_AXIS = 1
	};
	
	//constructor for joystick class
	//@params: x and y axis input pins, deadzone, button pin.
	Joystick(uint8_t xAxis, uint8_t yAxis, uint8_t dz, uint8_t btnPin = 255);

	~Joystick();

	//shifts the input from value range 0 - 1023 to -512 - 512 and applies deadzone. 
	//@return: the queried joystick axis' position in -512 - 512 range
	int16_t getAxisValue(joystick_axis_t axis);

	//passthrough function for Button::getEvent().
	//@return: 
	// 0 - no event
	// 1 - button changed from UP to DOWN (pin changed from HIGH to LOW)
	// 2 - button changed from DOWN to UP (pin changed from LOW to HIGH)
	uint8_t getButtonEvent();

	//passthrough function for Button::getState().
	//@return:
	// 0 - button not pressed.
	// 1 - button pressed.
	bool getButtonState();

	//reads x axis, y axis and button.
	void read(int16_t& x_axis, int16_t& y_axis);
	void read(int16_t& x_axis, int16_t& y_axis, int16_t& button);

	//sets the deadzone to the value provided.
	//@param: deadzone of the joystick.
	void setDeadzone(uint8_t dz);

private:
	//reads the analog joystick pins and saves the x / y axis values in the xOffset
	//and yOffset variables. joystick is assumed to be in the center position.
	void readOffset();

	uint8_t x_axis_pin_;
	uint8_t y_axis_pin_;
	uint8_t deadzone_;

	//x / y axis center offset
	int16_t x_offset_;
	int16_t y_offset_;

	Button *button_;
};

#endif /* JOYSICK_H_ */

