//Matrix type Keypad class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "YAAAKeypad_Matrix.h"

YAAAKeypad_Matrix::YAAAKeypad_Matrix(YAAAKeypad::KeypadSettings settings) :
	YAAAKeypad(YAAAKeypad::TYPE_MATRIX),
	num_cols_(0), num_rows_(0),
	col_pins_(NULL), row_pins_(NULL), 
	matrix_(NULL),
	layout_(LAYOUT3x4STD),
	active_high_(settings.active_high_)
{
	read_interval_ = settings.read_interval_;

	//read keypad layout
	layout_ = settings.parameters_[0];

	//key matrix setup
	switch (layout_)
	{
		case LAYOUT3x4STD:
		{
			num_cols_ = 3;
			num_rows_ = 4;

			uint8_t matrix[] =
			{
				YAAAKey::KEY_1, YAAAKey::KEY_2, YAAAKey::KEY_3,
				YAAAKey::KEY_4, YAAAKey::KEY_5, YAAAKey::KEY_6,
				YAAAKey::KEY_7, YAAAKey::KEY_8, YAAAKey::KEY_9,
				YAAAKey::KEY_ESC, YAAAKey::KEY_0, YAAAKey::KEY_ENTER
			};

			matrix_ = new uint8_t[num_rows_ * num_cols_];
			memcpy(matrix_, matrix, 12);
			break;
		}
		case LAYOUT4x4STD:
		{
			num_cols_ = 4;
			num_rows_ = 4;

			uint8_t matrix[] =
			{
				YAAAKey::KEY_1, YAAAKey::KEY_2, YAAAKey::KEY_3, YAAAKey::KEY_UP,
				YAAAKey::KEY_4, YAAAKey::KEY_5, YAAAKey::KEY_6, YAAAKey::KEY_DOWN,
				YAAAKey::KEY_7, YAAAKey::KEY_8, YAAAKey::KEY_9, YAAAKey::KEY_RIGHT,
				YAAAKey::KEY_ESC, YAAAKey::KEY_0, YAAAKey::KEY_ENTER, YAAAKey::KEY_LEFT,
			};

			matrix_ = new uint8_t[num_rows_ * num_cols_];
			memcpy(matrix_, matrix, 16);
			break;
		}
	}

	row_pins_ = new uint8_t[num_rows_];
	col_pins_ = new uint8_t[num_cols_];


	//read pin numbers
	for (uint8_t i = 0; i < num_rows_; i++)
	{
		row_pins_[i] = settings.parameters_[2 + i];

		pinMode(row_pins_[i], INPUT_PULLUP);
	}

	for (uint8_t i = 0; i < num_cols_; i++)
	{
		col_pins_[i] = settings.parameters_[6 + i];

		pinMode(col_pins_[i], INPUT_PULLUP);
	}
}

YAAAKeypad_Matrix::~YAAAKeypad_Matrix()
{
	delete row_pins_;
	delete col_pins_;
	delete matrix_;
}

YAAAKey YAAAKeypad_Matrix::getKey()
{
	read_last_ = millis();

	//read currently pressed key
	uint8_t key_code = getKeyCode(read());

	updateKey(key_code);

	return key_;
}

uint16_t YAAAKeypad_Matrix::read()
{
	uint16_t return_value = 0;

	//set row and column pins to INPUT_PULLUP
	for (uint8_t i = 0; i < num_cols_; i++)
		pinMode(col_pins_[i], INPUT_PULLUP);

	for (uint8_t i = 0; i < num_rows_; i++)
		pinMode(row_pins_[i], INPUT_PULLUP);

	//cycle through row pins, setting them one by one to OUTPUT, LOW
	for (uint8_t i = 0; i < num_rows_; i++)
	{
		pinMode(row_pins_[i], OUTPUT);
		digitalWrite(row_pins_[i], LOW);

		//check signal on each column pin to find pressed button
		for (uint8_t j = 0; j < num_cols_; j++)
		{
			uint8_t index = i * num_rows_ + j;
			return_value |= (((digitalRead(col_pins_[j]) == LOW) ^ active_high_) << index);
		}

		//reset row pin
		pinMode(row_pins_[i], INPUT_PULLUP);
	}

	return return_value;
}

uint8_t YAAAKeypad_Matrix::getKeyCode(uint16_t bits)
{
	uint8_t key = YAAAKey::KEY_NONE;

	uint8_t num_set_bits = 0;

	//count set bits. keypress is only valid when exactly 1 bit is set.
	for (uint8_t i = 0; i < 16; i++)
		num_set_bits += (bits >> i & 1);

	//determine return value
	if (num_set_bits == 1)
	{
		uint8_t index = 0;

		for (uint8_t i = 0; i < 16; i++)
			if (bits & (1 << i))
				index = i;

		key = matrix_[index];
	}
	else if (num_set_bits == 0)
		key = YAAAKey::KEY_NONE;
	else
		key = YAAAKey::KEY_INVALID;

	return key;
}

