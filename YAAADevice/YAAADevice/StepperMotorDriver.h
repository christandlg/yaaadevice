//Driver type Stepper motor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef STEPPERMOTOR_DRIVER_H_
#define STEPPERMOTOR_DRIVER_H_

#include "StepperMotor.h"

class StepperMotorDriver :
	public StepperMotor
{
public:
	//constructor for class using A4988 stepper driver.
	//@param:
	// - digital output pins step, diection and enable_pin controlling the stepper motor.
	// - digital output pins ms1, ms2, ms3 to set microstepping steptype.
	// - if the output pins will be enabled at construction time. defaults to false.
	StepperMotorDriver(uint8_t driver, uint8_t step, uint8_t direction, uint8_t enable_pin, uint8_t ms1, uint8_t ms2, uint8_t ms3, bool enable = false);

	~StepperMotorDriver();

	//sets the current steptype to the steptype provided and recalculates stepper position when
	//steptype has changed. 
	//@return: 
	// - true when the step type was set successfully.
	// - false when an invalid step type has been given for this stepper driver.
	bool setStepType(uint8_t step_type);

	//sets the inversion settings for the microstepping steptype pins.
	void setMicroStepPinsInverted(bool ms1_inv = false, bool ms2_inv = false, bool ms3_inv = false);

private:
	uint8_t pin_ms1_;						//pin 1 to set microstepping.
	uint8_t pin_ms2_;						//pin 2 to set microstepping.
	uint8_t pin_ms3_;						//pin 3 to set microstepping.

	uint8_t pin_ms1_inverted_;				//pin 1 inversion setting.
	uint8_t pin_ms2_inverted_;				//pin 2 inversion setting.
	uint8_t pin_ms3_inverted_;				//pin 3 inversion setting.
};

#endif /*STEPPERMOTOR_DRIVER_H_*/

