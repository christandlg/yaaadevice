//Filter Wheel class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#include "FilterWheel.h"

FilterWheel::FilterWheel() :
	current_filter_(0), filter_holder_type_(SLIDER),

	moving_(false),

	homing_(false), startup_auto_home_(false),
	home_found_(true), home_cleared_(false),
	home_first_pos_found_(false), home_first_pos_(0),
	home_offset_(0), measuring_home_offset_(false),
	home_start_time_(0), home_timeout_(0),
	homing_error_(FilterWheel::ERROR_NONE),

	endstop_lower_(NULL), endstop_upper_(NULL),

	home_switch_(NULL),

	endstop_state_(0), limit_state_(0)
{
	uint8_t stepper_driver;
	readEEPROM(EEPROMHandler::filter_wheel_variable_::MOTOR_DRIVER_TYPE, stepper_driver);

	//read number of steps.
	uint16_t number_of_steps;
	readEEPROM(EEPROMHandler::filter_wheel_variable_::NUMBER_OF_STEPS, number_of_steps);

	switch (stepper_driver)
	{
#ifndef ARDUINO_SAM_DUE
	case StepperMotor::ADAFRUIT_MOTORSHIELD_V2:
	{
		//read Motor Shield I2C Address and used terminal.
		uint8_t i2c_adress;
		uint8_t terminal;
		readEEPROM(EEPROMHandler::filter_wheel_variable_::I2C_ADDRESS, i2c_adress);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::TERMINAL, terminal);

		stepper_motor_ = new StepperMotorAFMSV2(i2c_adress, terminal, number_of_steps);
	}
	break;
#endif /*ARDUINO_SAM_DUE */
	case StepperMotor::FOUR_WIRE_STEPPER:
	{
		//read four wire stepper pins.
		uint8_t pin1, pin2, pin3, pin4;
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_1, pin1);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_2, pin2);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_3, pin3);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_4, pin4);

		stepper_motor_ = new StepperMotorFourWire(pin1, pin2, pin3, pin4, true);

		//read motor driver pin inversion settings.
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_1_INVERTED, pin1);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_2_INVERTED, pin2);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_3_INVERTED, pin3);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_4_INVERTED, pin4);

		stepper_motor_->setPinsInverted(pin1, pin2, pin3, pin4, false);
	}
	break;
	case StepperMotor::DRIVER_A4988:
	case StepperMotor::DRIVER_DRV8825:
	{
		uint8_t step, direction, enable, ms1, ms2, ms3;

		//read motor driver pins.
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_1, step);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_2, direction);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_3, enable);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_4, ms1);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_5, ms2);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_6, ms3);

		stepper_motor_ = new StepperMotorDriver(stepper_driver, step, direction, enable, ms1, ms2, ms3);

		//read motor driver pin inversion settings.
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_1_INVERTED, step);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_2_INVERTED, direction);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_3_INVERTED, enable);

		stepper_motor_->setPinsInverted(direction, step, enable);

		//read microstep select pin inversion settings.
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_4_INVERTED, ms1);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_5_INVERTED, ms2);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_6_INVERTED, ms3);

		stepper_motor_->setMicroStepPinsInverted(ms1, ms2, ms3);
	}
	break;
	default:
		stepper_motor_ = new StepperMotorFourWire(255, 255, 255, 255, false);
		break;
	}

	//failsafe.
	//call default constructor when an invalid driver type has been read from EEPROM.
	if (!stepper_motor_)
		return;

	stepper_motor_->release();

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	float max_speed;
	float acceleration;

	readEEPROM(EEPROMHandler::filter_wheel_variable_::MAX_SPEED, max_speed);
	readEEPROM(EEPROMHandler::filter_wheel_variable_::ACCELERATION, acceleration);

	//set default stepper motor parameters. 
	//when using an Adafruit Motor Shield, maximum stepper speed is limited by I2C communication 
	//speed. maximum speed for all Adafruit Motorshield driven steppers combined is 700 - 1200 
	//steps / second (depending on steptype and I2C communication speed).  
	//independent of used stepper drivers, stepper speed should not exceed 1000 steps / second for
	//each individual stepper according to AccelStepper documentation ("Speeds of more than 1000 
	//steps per second are unreliable").
	//setting the maximum speed too high will result in skipped steps. 
	//acceleration is limited to prevent the motor from skipping steps when accelerating.
	stepper_motor_->setMaxSpeed(max_speed);				//speed in steps / second
	stepper_motor_->setAcceleration(acceleration);		//acceleration in steps / second^2

	//read automatic stepper release setting.
	bool automatic_stepper_release;
	readEEPROM(EEPROMHandler::filter_wheel_variable_::AUTOMATIC_STEPPER_RELEASE, automatic_stepper_release);
	stepper_motor_->setAutomaticRelease(automatic_stepper_release);

	//read and set step type.
	uint8_t steptype;
	readEEPROM(EEPROMHandler::filter_wheel_variable_::STEPTYPE, steptype);
	stepper_motor_->setStepType(steptype);

	//stepper_motor_->enableOutputs();
	//stepper_motor_->move(1000);
	//stepper_motor_->runToPosition();

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	//read filter holder type (wheel or slider)
	readEEPROM(EEPROMHandler::filter_wheel_variable_::FILTER_HOLDER_TYPE, filter_holder_type_);

	//read number of filters in the filter holder.
	readEEPROM(EEPROMHandler::filter_wheel_variable_::NUMBER_OF_FILTERS, number_of_filters_);

	//read gear ratio.
	float gear_ratio;
	readEEPROM(EEPROMHandler::filter_wheel_variable_::GEAR_RATIO, gear_ratio);

	//calculate number of steps between filters.
	steps_between_filters_ = static_cast<float>(number_of_steps)* gear_ratio / static_cast<float>(number_of_filters_);

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	//read movement limit settings.
	readEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_LOWER_ENABLED, limit_lower_enabled_);
	readEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_UPPER_ENABLED, limit_upper_enabled_);
	readEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_LOWER, limit_lower_);
	readEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_UPPER, limit_upper_);

	//limits are stored in full steps, apply microstep factor.
	long microstep_factor = stepper_motor_->getMicroStepFactor();
	limit_lower_ *= microstep_factor;
	limit_upper_ *= microstep_factor;

	//read enstop settings.
	uint8_t pin_nr = 255;
	bool inverted = false;
	bool enabled = false;

	readEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER, pin_nr);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER_INVERTED, inverted);
		endstop_lower_ = new Button(pin_nr, inverted);
	}

	readEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER, pin_nr);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER_INVERTED, inverted);
		endstop_upper_ = new Button(pin_nr, inverted);
	}

	//read home position settings.
	readEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_SWITCH_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_SWITCH, pin_nr);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_SWITCH_INVERTED, inverted);
		readEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_OFFSET, home_offset_);

		//if the pin number is the same as the lower endstops pin number, the home switch is
		//the lower endstop switch.
		if (endstop_lower_ && (pin_nr == endstop_lower_->getPinNumber()))
			home_switch_ = endstop_lower_;

		//if the pin number is the same as the upper endstops pin number, the home switch is
		//the upper endstop switch.
		else if (endstop_upper_ && (pin_nr == endstop_upper_->getPinNumber()))
			home_switch_ = endstop_upper_;

		//if none of the above, create a new button instance.
		else
			home_switch_ = new Button(pin_nr, inverted);
	}

	//read timout for homing operation.
	readEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_TIMEOUT, home_timeout_);

	//read filter position offsets from EEPROM.	
	position_offsets_ = new int32_t[number_of_filters_];

	FilterInfo filter_info;

	for (uint8_t i = 0; i < number_of_filters_; i++) {
		readEEPROM(EEPROMHandler::filter_wheel_variable_::FILTER_INFO, filter_info, i);

		position_offsets_[i] = filter_info.position_offset;
	}

	//if startup_auto_home_ is true, activate homing process.
	if (startup_auto_home_) findHome();
}

FilterWheel::~FilterWheel()
{
	delete position_offsets_;
}

void FilterWheel::run()
{
	//check endstops and movement limits.
	endstop_state_ = checkEndstops();
	limit_state_ = checkLimits();

	if (homing_)
		home();

	//stepper will stay released until it actually steps
	moving_ = stepper_motor_->run();
}

void FilterWheel::stop()
{
	homing_ = false;					//disable homing
	measuring_home_offset_ = false;		//disable measuring home offset

	stop(false);						//stop the stepper
}

void FilterWheel::stop(bool hard_stop)
{
	if (hard_stop)
		stepper_motor_->hardStop();

	else
		stepper_motor_->stop();
}

bool FilterWheel::isMoving()
{
	return moving_;
}

void FilterWheel::setHome()
{
	//set the current position to the home position.
	stepper_motor_->setCurrentPosition(home_offset_);
	stepper_motor_->moveTo(0);	//move to position of filter #0.

	current_filter_ = 0;
}

bool FilterWheel::canFindHome()
{
	if (!home_switch_)
		return false;

	return true;
}

bool FilterWheel::findHome()
{
	stop();

	homing_error_ = FilterWheel::ERROR_HOMING_NONE;

	home_found_ = false;
	home_cleared_ = true;
	home_first_pos_found_ = false;

	//do nothing if no home switch is available.
	if (!canFindHome())
		homing_error_ |= FilterWheel::ERROR_HOMING_SWITCH_NOT_AVAILABLE;

	//check if the home position is beyond the filter wheel's movement limits.
	if (limit_lower_enabled_ && (home_offset_ < limit_lower_))
		homing_error_ |= FilterWheel::ERROR_HOMING_LIMIT_LOWER;

	if (limit_upper_enabled_ && (home_offset_ > limit_upper_))
		homing_error_ |= FilterWheel::ERROR_HOMING_LIMIT_UPPER;

	if (homing_error_ != FilterWheel::ERROR_HOMING_NONE)
		return false;

	//decide in which direction to go to find the home switch.
	//default homing direction is upwards.
	int32_t direction = 1L;

	//if the home switch is the lower endstop switch, move downwards to find the 
	//home position.
	if (endstop_lower_ && (home_switch_->getPinNumber() == endstop_lower_->getPinNumber()))
		direction = -1L;

	//if the home switch is the upper endstop switch, move upwards to find the 
	//home position.
	else if (endstop_upper_ && (home_switch_->getPinNumber() == endstop_upper_->getPinNumber()))
		direction = 1L;

	//if the home switch is not an endstop switch, move towards the (assumed) home position.
	else if (stepper_motor_->currentPosition() >= 0)
		direction = -1L;

	//when the home switch is triggered, it needs to be cleared first. to do
	//this, the direction the stepper moves in is inverted.
	if (home_switch_->getState() == true)
	{
		home_cleared_ = false;

		//invert direction
		direction *= -1L;
	}

	//set new target position for the stepper and reset stepper speed.
	stepper_motor_->moveTo(direction * LONG_MAX / 2);

	//enable homing mode. 
	homing_ = true;

	home_start_time_ = millis();

	return true;
}

void FilterWheel::clearHome()
{
	bool home_triggered = home_switch_->getState();

	//when the switch is no longer pressed the home position has been cleared.
	if (!home_triggered)
	{
		//invert direction of the stepper.
		stepper_motor_->moveTo(-1L * stepper_motor_->targetPosition());

		home_cleared_ = true;
	}
}

void FilterWheel::home()
{
	//find home timeout check
	if ((home_timeout_ != 0L) && (millis() - home_start_time_ > home_timeout_))
	{
		homing_ = false;
		measuring_home_offset_ = false;
		homing_error_ = FilterWheel::ERROR_HOMING_TIMEOUT;
		stop(hard_stop_limit_);
		return;
	}

	if (!home_cleared_)
		clearHome();

	else if (!home_found_)
	{
		bool home_triggered = home_switch_->getState();

		bool endstop_home_switch = (endstop_lower_ && (home_switch_->getPinNumber() == endstop_lower_->getPinNumber())) ||
			(endstop_upper_ && (home_switch_->getPinNumber() == endstop_upper_->getPinNumber()));

		//if the home switch is an endstop switch:
		if (endstop_home_switch)
		{
			if (home_triggered)
			{
				stop(hard_stop_endstop_);		//stop the stepper.

				stepper_motor_->move(0);		//set current position as the new target position.

				home_found_ = true;
			}
		}

		//if the home switch is not an endstop switch, the home position is the position 
		//between the two positions where the switch changes state.
		else
		{
			long location = stepper_motor_->currentPosition();

			//when the home switch is triggered and the first position where the switch 
			//is pressed has not yet been found:
			if (home_triggered && !home_first_pos_found_)
			{
				home_first_pos_found_ = true;
				home_first_pos_ = location;
			}

			//when the switch is released and the first position where the switch 
			//is pressed has been found:
			if (!home_triggered && home_first_pos_found_)
			{
				//move to the home position.
				stepper_motor_->move(location - home_offset_);

				home_found_ = true;
			}
		}
	}

	//when the home position has been found but the filter wheel has not yet reached the home 
	//position: do nothing here, just continue moving the filter wheel in the main loop.
	else if (moving_) {}

	//when the home position has been found and the filter wheel has reached the home position, 
	//synchronize the home position and set homing_ to false.
	else if (!moving_)
	{
		//when home offset measuring is activated update the home offset. 
		if (measuring_home_offset_)
		{
			home_offset_ = stepper_motor_->currentPosition();
			writeEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_OFFSET, home_offset_);
		}

		//home position reached, update stepper position.
		else
			stepper_motor_->setCurrentPosition(home_offset_);

		homing_ = false;						//disable homing
		measuring_home_offset_ = false;

		stepper_motor_->moveTo(0);				//move to filter #0. 
	}
}

bool FilterWheel::atHome()
{
	bool return_value = true;

	//return false if homing is still in progress.
	if (homing_) return_value = false;

	else if (stepper_motor_->currentPosition() != home_offset_) return_value = false;

	return return_value;
}

bool FilterWheel::isHoming()
{
	return homing_;
}

uint8_t FilterWheel::getHomingError()
{
	return homing_error_;
}

bool FilterWheel::measureHomeOffset()
{
	if (!canFindHome())
		return false;

	//start at filter 0. the user is expected to have moved the filter wheel to the first filter.
	stepper_motor_->setCurrentPosition(0);
	current_filter_ = 0;

	//enable home offset measuring mode.
	measuring_home_offset_ = true;

	//start homing. when measuring_home_offset_ is set to true the function home() will update 
	//the home offset to the stepper position instead of the stepper position to the  home
	//offset.
	return findHome();
}

bool FilterWheel::isMeasuringHomeOffset()
{
	return measuring_home_offset_;
}

uint8_t FilterWheel::moveFilter(int16_t filter)
{
	uint8_t return_value = FilterWheel::ERROR_NONE;

	//set error bit if an invalid filter number is specified.
	if ((filter < 0) || (filter > number_of_filters_ - 1))
		return_value |= FilterWheel::ERROR_INVALID_FILTER;

	//set error bit if filter holder is already at or moving towards the requestes position.
	if (filter == current_filter_)
		return_value |= FilterWheel::ERROR_CURRENT_FILTER;

	//set error bit if the filterwheel is currently homing.
	if (homing_)
		return_value |= FilterWheel::ERROR_HOMING;

	//when no errors occurred, initialte filter wheel movement
	if (return_value == FilterWheel::ERROR_NONE) {
		//change the filter.
		long filter_location;

		//when the filter wheel is a wheel: calculate the shortest possible way.
		if (filter_holder_type_ == WHEEL)
		{
			long sign;
			long current_position = stepper_motor_->currentPosition();

			//calculate distance to requested filter (might not be the shortest way).
			long steps_direct =
				static_cast<long>(filter)*static_cast<long>(steps_between_filters_) - current_position;

			//position offset
			//steps_direct += position_offsets_[filter];

			(steps_direct > 0) ? sign = 1 : sign = -1;

			//calculate distance to requested filter when going in the inverse direction.
			long steps_indirect =
				-sign * static_cast<long>(number_of_filters_)*steps_between_filters_ + steps_direct;

			//calculate nearest filter position.
			if (abs(steps_direct) <= abs(steps_indirect))
				filter_location = current_position + steps_direct;
			else
				filter_location = current_position + steps_indirect;
		}

		//when the filter wheel is a slider: calculate the distance.
		else //if (filter_holder_type_ == SLIDER)
		{
			//calculate new destination for stepper motors.
			//location of filter (in steps):
			filter_location = static_cast<long>(steps_between_filters_ * static_cast<float>(filter));
		}

		//read filter location offset and add to filter location.
		filter_location += getLocationOffset(filter);

		//set new target for stepper motor.
		stepper_motor_->moveTo(filter_location);

		//set the new filter as the currently active filter
		current_filter_ = filter;
	}

	return return_value;
}

uint8_t FilterWheel::moveNextFilter()
{
	uint8_t new_filter = 0;

	//if the current filter is not the last filter of the filter holder, 
	//move to the next filter
	if (current_filter_ < number_of_filters_ - 1)
		new_filter = current_filter_ + 1;

	//if the filter is the last filter move to filter 0 (first filter).
	else
		new_filter = 0;

	return moveFilter(new_filter);

}

uint8_t FilterWheel::movePrevFilter()
{
	uint8_t new_filter = 0;

	//if the current filter is not the first filter of the filter holder, 
	//move to the next filter
	if (current_filter_ > 0)
		new_filter = current_filter_ - 1;

	//if the filter is the first filter move to the last filter.
	else
		new_filter = number_of_filters_ - 1;

	return moveFilter(new_filter);
}

int16_t FilterWheel::getFilter()
{
	if (!isMoving())
		return static_cast<int16_t>(current_filter_);			//when the filter wheel is stationary.

	else return -1;												//when the filter wheel is moving.
}

long FilterWheel::getFocusOffset(int16_t filter)
{
	//return 0 when an invalid filter number is given.
	if ((filter < 0) || (filter > number_of_filters_ - 1))
		return 0L;

	FilterInfo filter_info;

	readEEPROM(
		EEPROMHandler::filter_wheel_variable_::FILTER_INFO,
		filter_info,
		filter);

	return filter_info.focus_offset;
}

long FilterWheel::getLocationOffset(int16_t filter)
{
	//return 0 when an invalid filter number is given.
	if ((filter < 0) || (filter > number_of_filters_ - 1))
		return 0L;

	FilterInfo filter_info;

	readEEPROM(
		EEPROMHandler::filter_wheel_variable_::FILTER_INFO,
		filter_info,
		filter);

	return filter_info.position_offset;
}

uint8_t FilterWheel::setCurrentFilter(int16_t filter)
{
	uint8_t return_value = FilterWheel::ERROR_NONE;

	//set error bit if an invalid filter number is specified.
	if ((filter < 0) || (filter > number_of_filters_ - 1))
		return_value |= FilterWheel::ERROR_INVALID_FILTER;

	//set error bit if filter wheel is currently moving.
	if (isMoving())
		return_value |= FilterWheel::ERROR_MOVING;

	//set error bit if filter holder is already at or moving towards the requestes position.
	if (filter == current_filter_)
		return_value |= FilterWheel::ERROR_CURRENT_FILTER;

	//set error bit if the filterwheel is currently homing.
	if (homing_)
		return_value |= FilterWheel::ERROR_HOMING;

	if (return_value == FilterWheel::ERROR_NONE) {
		//set the new filter as the currently active filter
		current_filter_ = filter;

		//update stepper motor location.
		long new_position = static_cast<long>(filter)*static_cast<long>(steps_between_filters_);
		stepper_motor_->setCurrentPosition(new_position);
	}

	return return_value;
}

uint8_t FilterWheel::getNumberOfFilters()
{
	return number_of_filters_;
}

uint8_t FilterWheel::reportSwitches()
{
	uint8_t return_value = 0;

	//bit 1 contains the state of the lower endstop switch.
	if (endstop_lower_) return_value += static_cast<uint8_t>(endstop_lower_->getState());

	//bit 2 contains the state of the upper endstop switch.
	if (endstop_upper_) return_value += static_cast<uint8_t>(endstop_upper_->getState()) << 1;

	//bit 3 contains the state of the home switch.
	if (home_switch_) return_value += static_cast<uint8_t>(home_switch_->getState()) << 2;

	return return_value;
}

uint8_t FilterWheel::getLocked()
{
	return locked_;
}

void FilterWheel::setLocked(uint8_t source)
{
	switch (source)
	{
	case 0:
		locked_ = LOCK_NONE;
		break;
	case 1:
		locked_ = LOCK_LOCAL;
		break;
	case 2:
		locked_ = LOCK_REMOTE;
		break;
	default:
		break;
	}
}

float FilterWheel::getTemperature(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkSensor(sensor))
		return NAN;

	//return the last measured temperature from the sensor.
	return temperature_sensors_[sensor]->getValue();
}

bool FilterWheel::getTemperatureComplete(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkSensor(sensor))
		return false;

	//return true if the sensor has finished the mesaurement, false otherwise.
	return temperature_sensors_[sensor]->hasValue();
}

bool FilterWheel::measureTemperature(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkSensor(sensor))
		return false;

	//start a temperature measurement and return the result.
	return temperature_sensors_[sensor]->measure();
}

bool FilterWheel::atDestination()
{
	return (stepper_motor_->distanceToGo() == 0L) && (stepper_motor_->speed() == 0.0f);
}

uint8_t FilterWheel::checkEndstops()
{
	uint8_t return_value = 0;
	bool harmless = true;

	//if the lower endstop switch is triggered:
	if (endstop_lower_ && (endstop_lower_->getState() == true))
	{
		return_value += 1;

		//if the filter wheel is moving to a position below the current position
		//an action is required.
		if (stepper_motor_->distanceToGo() < 0)
			harmless = false;

		//if the filter wheel is moving downwards and hard stopping is enabled
		//an action is required.
		if (hard_stop_endstop_ && stepper_motor_->speed() < 0.0)
			harmless = false;
	}

	//if the upper endstop switch is triggered:
	if (endstop_upper_ && (endstop_upper_->getState() == true))
	{
		return_value += 2;

		//if the filter wheel is moving to a position above the current position
		//an action is required.
		if (stepper_motor_->distanceToGo() > 0)
			harmless = false;

		//if the filter wheel is moving upwards and hard stopping is enabled
		//an action is required.
		if (hard_stop_endstop_ && stepper_motor_->speed() > 0.0)
			harmless = false;
	}

	if (!harmless)
	{
		//when homing is active and the home position has not yet been found, reverse the 
		//homing direction.
		if (homing_ && !home_found_)
		{
			//hard stop prevents "swinging" beyond endstop position.
			if (hard_stop_endstop_)
				stepper_motor_->setSpeed(0.0);

			stepper_motor_->moveTo(-1L * stepper_motor_->targetPosition());
		}
		else
		{
			stop();		//disables homing and move

			if (hard_stop_endstop_)
				stop(hard_stop_endstop_);	//stops the stepper instantly.

			else
				stepper_motor_->move(0);	//sends the stepper to the current position.
		}
	}

	return return_value;
}

uint8_t FilterWheel::checkLimits()
{
	uint8_t return_value = 0;
	bool harmless = true;

	long limit = 0;

	//when the current position is at or below the lower limit:
	if (limit_lower_enabled_ && (stepper_motor_->currentPosition() < limit_lower_))
	{
		return_value += 1;
		limit = limit_lower_;

		//if the filter wheel moves downwards an action is required.
		//if (stepper_motor_->targetPosition() < stepper_motor_->currentPosition())
		if (stepper_motor_->distanceToGo() < 0)
			harmless = false;

		//if the filter wheel is moving downwards and hard stopping is enabled
		//an action is required.
		if (hard_stop_limit_ && stepper_motor_->speed() < 0.0)
			harmless = false;
	}

	//when the current position is at or above the upper limit:
	if (limit_upper_enabled_ && (stepper_motor_->currentPosition() > limit_upper_))
	{
		return_value += 2;
		limit = limit_upper_;

		//if the filter wheel moves upwards an action is required.
		//if (stepper_motor_->targetPosition() > stepper_motor_->currentPosition())
		if (stepper_motor_->distanceToGo() > 0)
			harmless = false;

		//if the filter wheel is moving upwards and hard stopping is enabled
		//an action is required.
		if (hard_stop_limit_ && stepper_motor_->speed() > 0.0)
			harmless = false;
	}

	if (!harmless)
	{
		//when homing is active and the home position has not yet been found, reverse the 
		//homing direction.
		if (homing_ && !home_found_)
		{
			//hard stop prevents "swinging" beyond endstop position.
			if (hard_stop_endstop_)
				stepper_motor_->setSpeed(0.0);

			stepper_motor_->moveTo(-1L * stepper_motor_->targetPosition());
		}
		else
		{
			stop();		//disables homing and move

			if (hard_stop_endstop_)
				stop(hard_stop_endstop_);		//stops the stepper instantly.

			else
				stepper_motor_->moveTo(limit);	//sends the stepper to the movement limit.
		}


		//when homing is active and the home position has not yet been found, reverse the 
		//homing direction.
		if (homing_ && !home_found_)
		{
			//hard stop prevents "swinging" beyond endstop position.
			if (hard_stop_endstop_)
				stepper_motor_->setSpeed(0.0);

			stepper_motor_->moveTo(-1L * stepper_motor_->targetPosition());
		}
		else
		{
			stop();		//disables homing and move

			if (hard_stop_endstop_)
				stop(hard_stop_endstop_);	//stops the stepper instantly.

			else
				stepper_motor_->move(0);	//sends the stepper to the current position.
		}
	}

	return return_value;
}

bool FilterWheel::checkSensor(uint8_t sensor)
{
	//check the sensor number
	if (sensor > TEMP_SENSORS)
		return false;

	//check if the sensor was intialized
	if (!temperature_sensors_[sensor])
		return false;

	return true;
}

void FilterWheel::writeDefaults()
{
	//write default maximum speed.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MAX_SPEED, DEF_W_MAX_SPEED);

	//write default acceleration. 
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ACCELERATION, DEF_W_ACCELERATION);

	//write default step type.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::STEPTYPE, static_cast<uint8_t>(DEF_W_STEP_TYPE));

	//write default number of steps.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::NUMBER_OF_STEPS, static_cast<uint16_t>(DEF_W_NUMBER_OF_STEPS));

	//write default gear ratio.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::GEAR_RATIO, DEF_W_GEAR_RATIO);

	//write default automatic stepper release behaviour.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::AUTOMATIC_STEPPER_RELEASE, static_cast<uint8_t>(DEF_W_AUTO_STEPPER_RELEASE));

	//write default home switch information.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_SWITCH_ENABLED, static_cast<uint8_t>(DEF_W_HOME_SWITCH_ENABLED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_SWITCH, static_cast<uint8_t>(DEF_W_HOME_SWITCH_PIN));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_SWITCH_INVERTED, static_cast<byte>(DEF_W_HOME_SWITCH_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_OFFSET, static_cast<int32_t>(DEF_W_HOME_OFFSET));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::HOME_TIMEOUT, static_cast<uint32_t>(DEF_W_HOME_TIMEOUT));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::STARTUP_AUTO_HOME, static_cast<uint8_t>(DEF_W_STARTUP_AUTO_HOME));

	//write default endstop information.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER_ENABLED, static_cast<uint8_t>(DEF_W_ENDSTOP_LOWER_ENABLED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER, static_cast<uint8_t>(DEF_W_ENDSTOP_LOWER_PIN));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER_INVERTED, static_cast<uint8_t>(DEF_W_ENDSTOP_LOWER_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER_ENABLED, static_cast<uint8_t>(DEF_W_ENDSTOP_UPPER_ENABLED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER, static_cast<uint8_t>(DEF_W_ENDSTOP_UPPER_PIN));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER_INVERTED, static_cast<uint8_t>(DEF_W_ENDSTOP_UPPER_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::ENDSTOP_HARD_STOP, static_cast<uint8_t>(DEF_W_ENDSTOP_HARD_STOP));

	//write default limit information.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_LOWER_ENABLED, static_cast<uint8_t>(DEF_W_LIMIT_LOWER_ENDBLED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_LOWER, static_cast<int32_t>(DEF_W_LIMIT_LOWER));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_UPPER_ENABLED, static_cast<uint8_t>(DEF_W_LIMIT_UPPER_ENBALED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_UPPER, static_cast<int32_t>(DEF_W_LIMIT_UPPER));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::LIMIT_HARD_STOP, static_cast<uint8_t>(DEF_W_LIMIT_HARD_STOP));

	//write default motor driver type.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MOTOR_DRIVER_TYPE, static_cast<uint8_t>(DEF_W_MOTOR_DRIVER_TYPE));

	//write default Adafruit Motor Shield settings.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::I2C_ADDRESS, static_cast<uint8_t>(DEF_W_I2C_ADDRESS));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::TERMINAL, static_cast<uint8_t>(DEF_W_TERMINAL));

	//write default Four Wire Stepper / Stepper Driver settings.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_1, static_cast<uint8_t>(DEF_W_PIN_1));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_2, static_cast<uint8_t>(DEF_W_PIN_2));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_3, static_cast<uint8_t>(DEF_W_PIN_3));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_4, static_cast<uint8_t>(DEF_W_PIN_4));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_5, static_cast<uint8_t>(DEF_W_PIN_5));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_6, static_cast<uint8_t>(DEF_W_PIN_6));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_1_INVERTED, static_cast<uint8_t>(DEF_W_PIN_1_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_2_INVERTED, static_cast<uint8_t>(DEF_W_PIN_2_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_3_INVERTED, static_cast<uint8_t>(DEF_W_PIN_3_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_4_INVERTED, static_cast<uint8_t>(DEF_W_PIN_4_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_5_INVERTED, static_cast<uint8_t>(DEF_W_PIN_5_INVERTED));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::PIN_6_INVERTED, static_cast<uint8_t>(DEF_W_PIN_6_INVERTED));

	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MC_AVAILABLE, static_cast<uint8_t>(DEF_W_MC_AVAILABLE));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MC_BTN_LOCK_AVAILABLE, static_cast<uint8_t>(DEF_W_MC_BTN_LOCK_AVAILABLE));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MC_BTN_LOCK, static_cast<uint8_t>(DEF_W_MC_BTN_LOCK));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MC_BTN_NEXT_AVAILABLE, static_cast<uint8_t>(DEF_W_MC_BTN_NEXT_AVAILABLE));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MC_BTN_NEXT, static_cast<uint8_t>(DEF_W_MC_BTN_NEXT));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MC_BTN_PREVIOUS_AVAILABLE, static_cast<uint8_t>(DEF_W_MC_BTN_PREVIOUS_AVAILABLE));
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::MC_BTN_PREVIOUS, static_cast<uint8_t>(DEF_W_MC_BTN_PREVIOUS));

	//write default filter holder type.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::FILTER_HOLDER_TYPE, static_cast<uint8_t>(DEF_W_FILTER_HOLDER_TYPE));

	//write default number of filters.
	writeEEPROM(EEPROMHandler::filter_wheel_variable_::NUMBER_OF_FILTERS, static_cast<uint8_t>(DEF_W_NUMBER_OF_FILTERS));

	//write default filter info.
	FilterInfo filter_info = {
		DEF_W_FILTER_TYPE,
		DEF_W_FILTER_RFU,
		DEF_W_FILTER_FOCUS_OFFSET,
		DEF_W_FILTER_POSITION_OFFSET
	};

	for (uint8_t i = 0; i < DEF_W_NUMBER_OF_FILTERS; i++)
		writeEEPROM(EEPROMHandler::filter_wheel_variable_::FILTER_INFO, filter_info, i);

	//write default temperature senso settings
	YAAASensor::SensorSettings sensor_settings = {
		DEF_W_TEMPERATURE_SENSOR_TYPE,
		DEF_W_TEMPERATURE_SENSOR_DEVICE,
		DEF_W_TEMPERATURE_SENSOR_UNIT,
		DEF_W_TEMPERATURE_SENSOR_PARAMETERS
	};

	for (uint8_t i = 0; i < TEMP_SENSORS; i++)
		writeEEPROM(EEPROMHandler::filter_wheel_variable_::TEMPERATURE_SENSOR, sensor_settings, i);
}

