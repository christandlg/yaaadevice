//OTA class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef OTAHANDLER_H_
#define OTAHANDLER_H_

#if defined(ESP8266) || defined(ESP32)

#include <Arduino.h>

//includes for ESP8266 based boards. 
#if defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#endif /* defined(ESP8266) */

//includes for ESP32 based boards. 
#if defined(ESP32)
#include <WiFi.h>
#include <ESPmDNS.h>
#endif /* defined(ESP32) */

#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#include "EEPROMHandler.h"

#include "YAAALog.h"
extern YAAALog log_;

#include "defaults.h"

class OTAHandler
{
public:
	enum mode_t : uint8_t
	{
		MODE_DISABLED = 0,    //OTA is disabled.
		MODE_ARDUINOOTA = 1,  //OTA via ArduinoOTA
		MODE_WEBBROWSER = 2,  //OTA via webbrowser
		MODE_HTTPSERVER = 4,  //OTA via HTTP server
	};

	OTAHandler();

	~OTAHandler();

	/*
	initilaizes a OTAHandler instance.
	@return true on success, false otherwise. */
	static bool begin();

	static void run();

	static uint8_t getOTAMode();

	static bool getOTAModeEnabled(uint8_t mode);

	static void writeDefaults();


private:
	static uint8_t mode_;   //OTA mode. set to MODE_DISABLED if OTA class initialization failed. 

	static uint32_t httpserver_update_last_;      //time of last update via HTTPServer 
	static uint32_t httpserver_update_interval_;  //interval between update checks via HTTPServer

#if defined(ESP8266)
	static ESP8266WebServer *httpServer;
	static ESP8266HTTPUpdateServer *httpUpdater;
#endif /* defined(ESP8266) */
};

#endif /* defined(ESP8266) || defined(ESP32) */

#endif /* OTAHANDLER_H_ */

