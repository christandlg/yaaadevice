//Telescope class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef TELESCOPE_H_
#define TELESCOPE_H_

#include <Arduino.h>

#include "defaults.h"

#include "TelescopeAxis.h"
#include "EEPROMHandler.h"

#include "YAAASensor.h"
#include "SensorTemperature.h"
#include "SensorTemperatureOneWire.h"
#include "SensorTemperatureAnalogIC.h"
#include "SensorTemperatureThermistor.h"
#include "SensorTemperatureMLX90614.h"

#define TEMP_SENSORS 3

class Telescope 
{
public:
	//directions for pulseGuide commands.
	enum direction_t : uint8_t
	{
		NORTH = 0,		//positive Alt / Dec
		SOUTH = 1,		//negative Alt / Dec
		EAST = 2,		//positive Az / RA
		WEST = 3		//negative Az / RA
	};

	enum error_code_t : uint8_t //todo: add more error codes & change return value of most functions to uint16_t?
	{
		ERROR_NONE = 0,
		ERROR_OUT_OF_BOUNDS_X = 1,	//x axis target position is outside the movement limits.
		ERROR_OUT_OF_BOUNDS_Y = 2,	//y axis target position is outside the movement limits.
		ERROR_PARKED = 4,			//telescope is currently parked.
		//ERROR_MANUAL_CONTROL = 8,	//manual control is active
		ERROR_INVALID_AXIS = 16,	//invaid axis given
		ERROR_HOMING = 32,			//telescope is currently homing
		ERROR_NOT_AVAILABLE = 64,	//requested functionality not available
		ERROR_OTHER = 128			//todo: exchange for something meaningful.
	};

	enum lock_source_t : uint8_t
	{
		LOCK_NONE = 0,		//device currently not locked.
		LOCK_LOCAL = 1,		//device currently locally locked
		LOCK_REMOTE = 2		//device currently remote locked.
	};

	//type of telescope mount.
	enum mount_type_t : uint8_t
	{
		ALT_AZ = 0,
		EQUATORIAL = 1,
		GERMAN_EQUATORIAL = 2,
	};

	//enum precision_t : uint8_t
	//{
	//	PRECISION_DEGREE = 0,
	//	PRECISION_MINUTE = 1,
	//	PRECISiON_SECOND = 2,
	//	PRECISION_FRAC = 3
	//};

	enum speed_unit_t : uint8_t
	{
		//STEPS_PER_SECOND = 0,				
		DEGREES_PER_SECOND = 1,					//moveAxis, pulseGuide, manual control.
		ARCSECONDS_PER_SECOND = 2,				//Lunar, Solar, King drive rates.
		ARCSECONDS_PER_SIDEREAL_SECOND = 3,		//Sidereal drive rate, RightAscensionRate, DeclinationRate.
		SECONDS_OF_RA_PER_SIDEREAL_SECOND = 4,	//seconds of right ascension per sidereal second
	};

	//enum telescope_axis_t : uint8_t
	//{
	//	X_AXIS = 0,
	//	Y_AXIS = 1,
	//	Z_AXIS = 2
	//};

	enum tracking_rates_t : uint8_t	//a collection of well-known tracking rates.
	{
		TR_SIDEREAL = 0,	//15.0 arcseconds / sidereal second (15.0411 arcseconds / second)
		TR_LUNAR = 1,		//14.685 arcseconds / seconds
		TR_SOLAR = 2,		//15.0 arcseconds / second
		TR_KING = 3			//15.0369 arcseconds / second
	};
	
	/*
	constructor for Mount class. 
	 - most of the Mount class' member variables are initialized to default values. 
	 - some values are read from the EEPROM, like movement limits, home alignment, gear ratio, 
		 and number of stepper motor steps.
	 - StepperMotor instances are created. 
	 - manual control settings are read and inputs initialized.*/
	Telescope();
	~Telescope();

	/*
	this method handles axis movement. 
	 - calls manualControl() to handle manual contron.
	 - calls each axis' function run() to move the axis.*/
	void run();

	/*
	checks if a given target is within the given axis' limits.
	@param target: target
	@param axis: axis
	@return: false if an invalid axis is given or the target is outside the axis' 
	limits, true otherwise. */
	bool withinLimits(uint8_t axis, float target);

	/*
	converts an angle given in degrees to an angle in DMS format.
	@param angle the angle to convert
	@param precision: defines output format. true: sDD*MM'SS | false: sDD*MM
	@param long_degree: number of digits for degree. true: 3 digits | false: 2 digits
	@return DMS String representation of the angle.*/
	static String degreesToDMS(float angle, bool precision, bool long_degree = false);

	/*
	converts an angle given in degrees to an angle in HMS format.
	@param angle the angle to convert
	@param precision: defines output format. true: sHH*MM'SS | false: sDD*MM.T
	@param long_degree: number of digits for degree. true: 3 digits | false: 2 digits
	@return HMS String representation of the angle.*/
	static String degreesToHMS(float angle, bool precision);

	/*
	converts an angle given in DMS format to fractional degree format. 
	@param DMS angle in sDD*MM or sDD*MM'SS format. 
	@param precision: true when the DMS string is given in sDD*MM'SS format, 
	false if given in sDD*MM format.
	@return: angle in degrees.*/
	static float dmsToDegrees(String DMS, bool precision);

	/*
	converts an angle given in HMS format to fractional degree format.
	@param HMS: angle in HH:MM.T or HH:MM:SS format.
	@param precision: true if the HMS string is given in HH:MM:SS format, 
	false if given in HH:MM.T format.
	@return: angle in degrees.*/
	static float hmsToDegrees(String HMS, bool precision);

	/*
	converts the given speed from one unit to another.
	@params:
		speed - the speed in any valid unit.
		unit_src - the unit of the given speed.
		unit_dst - the unit to convert the speed into.
	@return the speed in the requested unit. */
	static float convertSpeed(float speed, speed_unit_t unit_src, speed_unit_t unit_dst = speed_unit_t::DEGREES_PER_SECOND);

	/*@return: true if any of the axes is currently moving, false otherwise.*/ 
	bool isMoving();

	/*@return: true if a pulse guiding command is in progress for the x and / or y axis.*/
	bool isPulseGuiding();

	//checks if the telescope is moving about its x and / or y axis in response to a Slew command.
	// - returns false when the telescope is parked. 
	//@return: 
	// - true if a slew command is active fot the x and / or y axis.
	// - false if the telescope is parked or homing or no slew command is active  for the x
	//   or y axis.
	bool isSlewing();

	//@return: true if the telescope is moving about its x, y or z axis in response to a 
	//moveAxis command.
	bool isMoveAxis();

	/*
	@param axis: telescope axis to check.
	@return: if a moveAxis command is active for the given axis or false if an invalid
	axis is given.*/
	bool isMoveAxis(uint8_t axis);

	//@return: true if tracking is enabled for the telescope's x and / or y axis.
	bool isTracking();

	//initiates a slewing operation. target is given in degrees and / or hour angles. 
	//this function will be called when the driver issues a slew command.
	//@param x: x axis (RA / AZ) slew target.
	//@param y: y axis (DEC / ALT) slew target.
	// - checks if the telescope is: 
	// - - parked, 
	// - - homing, 
	// - - manual control is active, 
	// - - targets are outside the respective axis' movement limits  
	//	 and sets return error codes if necessary.
	// - when no errors occured sends the new slew targets to the telescope's 
	//	 x and y axes.
	// - when the telescope mount is equatorial or german equatorial, converts the 
	//	 right ascension axis target from hour angles to degrees before sending.
	//@return:
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - x axis destination beyond limits.
	//	| | | | | | - y axis destination beyond limits.
	//	| | | | | -	telescope is currently parked.
	//	| | | | - manual control active.
	//	| | | -	not used.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- not used.
	uint8_t slew(float x, float y);

	//initiates a slew to targets set using setTarget().
	//@return:
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - x axis destination beyond limits.
	//	| | | | | | - y axis destination beyond limits.
	//	| | | | | -	telescope is currently parked.
	//	| | | | - manual control active.
	//	| | | -	not used.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- not used.
	uint8_t slewToTarget();

	//aborts any slew and moveAxis commands for all axes. 
	// - checks if the telescope is: 
	// - - parked, 
	// - - homing, 
	// - - manual control is active, 
	// - returns an error if the telescope is parked.
	//	 and sets return error codes if necessary.
	// - when no errors occured, calls abortSlew() for all axes.
	//@return:
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	telescope is currently parked.
	//	| | | | - manual control active.
	//	| | | - not used.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- not used.
	uint8_t abortSlew();

	//returns the current alignment of the given telescope axis. value is returned in degrees or 
	//hour angle (only x axis when using an equatorial or german equatorial mount).
	// - calls the given axis' getCurrentAlignment() function.
	// - when the x axis is queried and mount type is equatorial or german equatorial, converts 
	//	 the alignment to hour angle.
	//@params:
	// - queried axis.
	//@return:
	// - queried axis' alignment. returns NAN if an invalid axis is given.
	float getCurrentAlignment(uint8_t axis);

	//sets the current alignment of the telescope. does not move the steppers.
	// - when the mount type is equatorial or german equatorial converts the given alignment of
	//	 the x axis from hour angle to degrees.
	// - calls setCurrentAlignment(float alignment) for both axes.
	//@param: alignment of the axes in degrees or hour angles.
	//@return: 
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	telescope is currently parked.
	//	| | | | - not used.
	//	| | | - invalid axis given.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- not used.
	uint8_t setCurrentAlignment(uint8_t axis, float alignment);

	//returns the current target of the given telescope axis (in degrees).
	//@params:
	// - queried axis.
	//@return: 
	// - the given axis' target position (in degrees). returns NAN when an invalid axis is given.
	float getCurrentTarget(uint8_t axis);

	/*
	@return: the mounts site latitude.*/
	float getSiteLatitude();

	/*
	@return: the mounts site longitude.*/
	float getSiteLongitude();
	
	//@return: the given axis' target or 0.0f if an invalid axis was given.
	float getTarget(uint8_t axis);

	//sets a target for the telescope. use with slewToTarget().
	//@param axis: telescope axis as telesocpe_axis_t
	//@param target: target
	//@return: true if the target was set successfully, false if an in valid axis was given.
	bool setTarget(uint8_t axis, float target);

	//@return: the currently set mount type as mount_type_t.
	uint8_t getMountType();

	//parks the telescope.
	// - returns an error code when the telescope is already parked, manual control is activated 
	//	 or parking is not enabled.
	// - calls park() for the telescope's x and y.
	//@return:
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - x axis parking position is beyond the x axis' movement limits.
	//	| | | | | | - y axis parking position is beyond the y axis' movement limits.
	//	| | | | | -	telescope is already parked.
	//	| | | | - manual control active.
	//	| | | -	not used.
	//	| | - telescope is currently homing.
	//	| - parking not enabled.
	//	- not used.
	uint8_t park();

	//unparks the telescope. 
	// - returns an error code when the telescope is not parked, manual control is 
	//	 active or parking is not enabled. 
	// - unparks the telescope when no errors occured.
	//@return:
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	telescope is not parked.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - telescope is currently homing.
	//	| - parking not enabled.
	//	- not used.
	uint8_t unpark();

	//@return: the given axis' park position (in degrees) or 0.0f if the given axis
	//was invalid.
	float getPark(uint8_t axis);

	//sets the telescopes parking position to be the telescope's current position.
	// - calls the x and y axes' setPark() functions.
	void setPark();

	//@return: true if the telescope has been put into the parked state.
	bool atPark();

	/*
	@return bit mask indicating which axis (if any) can be homed.*/
	uint8_t canFindHome();

	//initiates homing process.
	// - returns an error code if the telescope is parked or manual control is active.
	// - when no error occurred, initiates homing for the telescope's x and y axes.
	//@return true if homing procedure could be started, false otherwise.
	bool findHome();

	/*
	returns the given axis' error code of the last homing operation. returns telescope's error code
	if axis is invalid or not given.
	@param (optional) axis 
	@return error code */
	uint8_t getHomingError(uint8_t axis = 255);

	//todo: atHome should only report true when - after a homing operation - the telescope
	//has not yet been slewed.
	//@return: true if the telescope is at the home position (x and y axes), false otherwise.
	bool atHome();

	//todo: atHome should only report true when - after a homing operation - the telescope
	//has not yet been slewed.
	//@param axis as Telescope::telescope_axis_t.
	//@return: true if the given axis is valid and at the home position, false otherwise.
	bool atHome(uint8_t axis);

	//@return: true if homing is in progress for any axis.
	bool isHoming();

	//@param: tracking rate for the x axis (right ascension axis).
	uint8_t setTrackingRate(uint8_t tracking_rate);

	//@return: the currently set tracking rate for the x axis (right ascension axis).
	uint8_t getTrackingRate();

	//@param: requested tracking rate as tracking_rate_t.
	//@return: tracking rate in arcseconds / second or NAN if an invalid rate is given.
	float getWellKnownTrackingRate(uint8_t rate);

	//enables or disables tracking for the telescope's x and y axes.
	//@params:
	// - requested tracking state.
	//@return:
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	telescope is parked.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- not used.
	uint8_t tracking(bool enable);

	//slews the given axis to the given target position.
	//@param axis: axis as Telescope::telescope_axis_t
	//@param target: target position in degrees.
	//return:	
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - target position beyond limits.
	//	| | | | | | - not used.
	//	| | | | | -	telescope is currently parked.
	//	| | | | - not used.
	//	| | | - invalid axis given.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- not used.
	uint8_t move(uint8_t axis, float target);

	//activates moveAxis command at the given rate for the specified axis. 
	// - returns error codes when the telescope is parked, manual control is active or 
	//	 an invalid axis was given.
	// - when no error occured, activates a moveAxis command for the given axis at the given rate.
	//@params: 
	// - telescope axis. 
	// - the rate in degrees / second. positive speed = CW, negative speed = CCW.
	//@return: 
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	telescope is parked.
	//	| | | | - not used.
	//	| | | - invalid axis given.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- not used.
	uint8_t moveAxis(uint8_t axis, float speed);

	//todo:
	//rework whole pulse guiding code.
	//activates pulse guiding.
	// - returns immediately if an invalid direction has been provided.
	// - returns immediately if the telescope is parked.
	// - disables moveAxis commands for x and y axes.
	// - tries to set a new speed using setNewSpeed(), when successful:
	// - - sets an arbitrary stepper target according to the given pulseGuide direction.
	// - - sets pulseGuide end time and sets pulse_guiding_(axis)_ to true.
	//@params:
	// - direction: 0 - north; 1 - south; 2 - east; 3 - west
	// - duration: duration of the movement in ms.
	// - rate: rate of motion in degrees / second.
	//@return:
	// - when encountering errors, sets specific bits in the return value:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	telescope is parked.
	//	| | | | - manual control active.
	//	| | | - not used.
	//	| | - telescope is currently homing.
	//	| - not used.
	//	- invalid direction given.
	uint8_t pulseGuide(uint8_t direction, uint32_t duration, float rate);
	
	//param: the requested axis.
	//@return: pointer to the requested axis or NULL if an invalid axis was given.
	TelescopeAxis *getAxis(uint8_t axis);

	/*
	@param: axis to query.
	@return: currently set moveAxis rate or NAN if an invalid axis was given.*/
	float getMoveAxisRate(uint8_t axis);

	/*
	@param axis: axis to query.
	@return: currently set moveAxis offset speed of the given axis (in degrees / second) 
	or NAN if an invalid axis was given.*/
	float getMoveAxisOffset(uint8_t axis);

	/*
	sets the moveAxis offset speed for the given axis to the given value.
	@param axis: telescope axis.
	@param offset: moveAxis offset speed in degrees / second.
	@return: true if the offset speed was correctly set.*/
	bool setMoveAxisOffset(uint8_t axis, float offset);

	//@return: right ascension tracking rate offset.
	float getRightAscensionRate();
	
	//@param: right ascension tracking rate offset.
	bool setRightAscensionRate(float rate);

	//@return: declination tracking rate offset.
	float getDeclinationRate();

	//@param: declination tracking rate offset.
	bool setDeclinationRate(float rate);

	//@return: endstop / home switch information for the given axis. returns an error when 
	//invalid axis was given.
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - lower endstop triggered.
	//	| | | | | | - upper endstop triggered.
	//	| | | | | -	home switch triggered.
	//	| | | | - not used.
	//	| | | -	invalid axis given.
	//	| | - not used.
	//	| - not used.
	//	- not used.
	uint8_t reportSwitches(uint8_t axis);

	////@return: true if manual control is available.
	//bool getManualControlAvailable();

	//@param axis: telescope axis
	//@return the maximum speed in degrees / second for the given axis or NAN if an invalid axis was given. 
	float getMaxSpeedDeg(uint8_t axis);

	//returns lock source (none, local, remote).
	uint8_t getLocked();

	//sets lock state (none, local, remote).
	void setLocked(uint8_t state);

	/*returns the temperature measured by the given sensor.
	@param sensor: sensor number to use (0 - n-1)
	@return: temperature in the requested temperature unit or 999.0 to indicate a
	failed measurement.*/
	float getTemperature(uint8_t sensor);

	//@param temperature sensor to check
	//@return: true when the given sensor has completed a temperature measurement.
	bool getTemperatureComplete(uint8_t sensor);

	//initiates a temperature measurement.
	//@param sensor using to measure
	//@return true if the measurement was started successfully, false otherwise.
	bool measureTemperature(uint8_t sensor);

	//writes default values to the Arduino's EEPROM.
	//this function will be called when the eeprom control number check in the main setup function
	//fails or the user/driver sends a restore defaults command.
	static void writeDefaults();
	
private:
	//checks if the given sensor is available.
	//@param sensor: sensor to check.
	//@return: true if the sensor has been successfully initialized, false otherwise.
	bool checkSensor(uint8_t sensor);

	//stops the steppers and all movement commands.
	void stop();

	//stops movement of the given axis. when hard_stop is true, stops the stepper
	//immediately, bypassing AccelSteppers stop() function (no acceleration). does nothing
	//if an invalid axis is given.
	//@param: 
	// - the axis to be stopped.
	// - (optional) if a hard stop should be executed or not.
	void stop(uint8_t axis, bool hard_stop = false);

	uint8_t mount_type_;			//type of the mount (AltAz / EQ / GerEQ)

	TelescopeAxis *x_axis_;			//the telescope's x axis.
	TelescopeAxis *y_axis_;			//the telescope's y axis.
	TelescopeAxis *z_axis_;			//the telescope's z axis. (e.g. imager rotator/de-rotator). 

	uint8_t homing_error_;

	//variables for parking.	
	bool park_enabled_;				//if parking is enabled for the telescope.
	bool parked_;					//true when the telescope is parked.

	//variables for homing.
	bool startup_auto_home_;		//true if the telescope automatically homes on startup.

	uint8_t tracking_rate_;			//current tracking rate as tracking_rates_t.

	uint8_t locked_;				//locked state of the telescope (LOCK_NONE, LOCK_LOCAL or LOCK_REMOTE)

	SensorTemperature *temperature_sensors_[TEMP_SENSORS];

	//----------------------------------------------------------------------------------------------
	//templates for reading and writing to/from the EEPROM.
	template <class T> static void readEEPROM(EEPROMHandler::telescope_variable_::TelescopeVariable_t_ variable, T& value, uint8_t index = 0)
	{
		EEPROMHandler::readValue
			(
			EEPROMHandler::device_type_::TELESCOPE,
			variable,
			index,
			value
			);
	};

	template <class T> static void writeEEPROM(EEPROMHandler::telescope_variable_::TelescopeVariable_t_ variable, const T& value, uint8_t index = 0)
	{
		EEPROMHandler::writeValue
			(
			EEPROMHandler::device_type_::TELESCOPE,
			variable,
			index,
			value
			);
	};
};

#endif /* TELESCOPE_H_ */

