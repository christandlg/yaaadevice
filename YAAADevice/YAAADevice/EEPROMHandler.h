//EEPROMHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef EEPROMHANDLER_H_
#define EEPROMHANDLER_H_

#include "Arduino.h"

#include "features.h"

#ifdef ARDUINO_SAM_DUE
#include <DueFlashStorage.h>
#else
#include <EEPROM.h>
#endif

#define TEMP_SENSORS 3		//maximum number of temperature sensors per device.

class EEPROMHandler
{
public:
	struct device_type_
	{
		enum DeviceType_t_ : uint16_t
		{
			GENERAL = 0,				//general variables (e.g. serial communication connection speed).
			CAMERA_ROTATOR = 512,		//variables for the camera rotator.
			DOME = 1024,				//variables for the dome.
			DOME_AXIS = 1152,			//variables for the dome's axes.
			FILTER_WHEEL = 1536,		//variabled for the filter wheel.
			FOCUSER = 2048,				//variables for the focuser.
			SAFETY_MONITOR = 2560,		//variables for the safety monitor.
			TELESCOPE = 3072,			//variables for the telescope.
			TELESCOPE_AXIS = 3200,		//variables for the telescope's axes.
			OBSERVING_CONDITIONS = 3584 //variables for observing conditions class.
		};
	};

	struct general_variable_
	{
		enum GeneralVariable_t_ : uint16_t
		{
			EEPROM_CONTROL = 0,				//(4 byte) magic number to determine if EERPOM needs to be initialized.
			SERIAL_BAUD_RATE = 4,			//(4 byte) baud rate of the serial interface.

			CAMERA_ROTATOR_AVAILABLE = 10,			//(1 byte) if a camera rotator is available or not.
			DOME_AVAILABLE = 11,					//(1 byte) if a dome is available or not.
			FILTER_WHEEL_AVAILABLE = 12,			//(1 byte) if a filter wheel is available or not.
			FOCUSER_AVAILABLE = 13,					//(1 byte) if a focuser is available or not.
			SAFETY_MONITOR_AVAILABLE = 15,			//(1 byte) if a safety monitor is available or not.
			TELESCOPE_AVAILABLE = 16,				//(1 byte) if a telescope is available or not.

			DISPLAY_TYPE = 30,				//(1 byte) display type.
			DISPLAY_RES_X = 32, 			//(2 byte) display resolution (x axis).
			DISPLAY_RES_Y = 34,				//(2 byte) display resolution (y axis).
			DISPLAY_UPDATE_INTERVAL = 36,	//(4 byte) display update interval in milliseconds.
			DISPLAY_CONTRAST = 40,			//(1 byte) display contrast setting
			DISPLAY_BRIGHTNESS = 41,		//(1 byte) display brightness setting

			DISPLAY_INTERFACE_TYPE = 50,	//(1 byte) display interface type (parallel, I2C, SPI, ...)
			DISPLAY_INTERFACE_PIN0 = 51,	//(1 byte) display interface pin 0.
			DISPLAY_INTERFACE_PIN1 = 52,	//(1 byte) display interface pin 1.
			DISPLAY_INTERFACE_PIN2 = 53,	//(1 byte) display interface pin 2.
			DISPLAY_INTERFACE_PIN3 = 54,	//(1 byte) display interface pin 3.
			DISPLAY_INTERFACE_PIN4 = 55,	//(1 byte) display interface pin 4.
			DISPLAY_INTERFACE_PIN5 = 56,	//(1 byte) display interface pin 5.
			DISPLAY_INTERFACE_PIN6 = 57,	//(1 byte) display interface pin 6.
			DISPLAY_INTERFACE_PIN7 = 58,	//(1 byte) display interface pin 7.
			DISPLAY_INTERFACE_PIN8 = 59,	//(1 byte) display interface pin 8.
			DISPLAY_INTERFACE_PIN9 = 60,	//(1 byte) display interface pin 9.
			DISPLAY_INTERFACE_PIN10 = 61,	//(1 byte) display interface pin 10.
			DISPLAY_INTERFACE_PIN11 = 62,	//(1 byte) display interface pin 11.
			DISPLAY_INTERFACE_PIN12 = 63,	//(1 byte) display interface pin 12.
			DISPLAY_INTERFACE_PIN13 = 64,	//(1 byte) display interface pin 13.
			DISPLAY_INTERFACE_PIN14 = 65,	//(1 byte) display interface pin 14 (Brightness).
			DISPLAY_INTERFACE_PIN15 = 66,	//(1 byte) display interface pin 15 (Contrast). 

			ETHERNET_ENABLED = 80,			//(1 byte) true if ethernet connection is enabled.
			ETHERNET_DHCP = 81,				//(1 byte) true if DHCP should be used.
			ETHERNET_MAC = 82,				//(6 byte) MAC address of the ethernet shield
			ETHERNET_IP = 88,				//(4 byte) ip address (if not using DHCP)
			ETHERNET_GATEWAY = 92,			//(4 byte) gateway (if not using DHCP)
			ETHERNET_DNS = 96,				//(4 byte) DNS server to use
			ETHERNET_SUBNET = 100,			//(4 byte) subnet mask (if not using DHCP)
			ETHERNET_PORT = 104,			//(2 byte) port number to use.

			SD_CS_PIN = 110,				//(1 byte) SD card chip select pin.
			SD_LOG = 111,					//(1 byte) true if commands and responses should be logged to the SD card.

			ONEWIRE_PIN = 120,				//(1 byte) One Wire interface pin.

			I2C_PIN_SCL = 130,				//(1 byte) I2C SCL pin
			I2C_PIN_SDA = 131,				//(1 byte) I2C SDA pin

			TIME_UTC_OFFSET = 140,			//(4 byte) offset (in hours) from UTC.

			//INPUT_INFODISPLAY_UP = 140,		//(2 byte) infodisplay "up" button settings
			//INPUT_INFODISPLAY_DOWN = 142,
			//INPUT_INFODISPLAY_LEFT = 144,
			//INPUT_INFODISPLAY_RIGHT = 146,
			//INPUT_INFODISPLAY_PLUS = 148,
			//INPUT_INFODISPLAY_MINUS = 150,

			INPUT_INFODISPLAY_KEYPAD = 160,			//(18 bytes) keypad settings

			WIFI_ENABLED = 180,		  				//(1 byte)
			WIFI_DHCP = 181,			    		//(1 byte)
			WIFI_MAC = 182,				     		//(6 bytes) 
			WIFI_IP = 188,				  			//(4 bytes) 
			WIFI_GATEWAY = 192,			  			//(4 bytes) 
			WIFI_DNS = 196,							//(4 bytes) 
			WIFI_SUBNET = 200,						//(4 bytes) 
			WIFI_PORT = 204,						//(2 bytes)
			WIFI_CHANNEL = 206,						//(1 byte) WIFI channel. 0 to automatically select.
			WIFI_SSID_LENGTH = 208,					//(1 byte)
			WIFI_PASSWORD_LENGTH = 209,				//(1 byte)
			WIFI_SSID = 210,						//(32 bytes max) c string containing the SSID (excluding '\0').
			WIFI_PASSWORD = 250,					//(63 bytes max) c string containing the password (excluding '\0').
			WIFI_HOSTNAME_LENGTH = 314,				//(1 byte) host name length
			WIFI_HOSTNAME = 315,					//(63 bytes max) c string containing the host name (excluding '\0').
#if defined(ESP8266) || defined(ESP32)
			OTA_MODE = 400,							//OTA mode as OTAHandler::mode_t.
			OTA_HOSTNAME_LENGTH = 401,				//(1 byte) OTA host name length
			OTA_HOSTNAME = 402,						//(63 bytes max) c string containing OTA hostname

			OTA_ARDUINOOTA_PORT = 476,				//port for OTA     

			OTA_ARDUINOOTA_PASSWORD_HASH = 478,		//(1 byte) true if password is provided as a MD5 hash.
			OTA_ARDUINOOTA_PASSWORD_LENGTH = 479,	//(1 byte) OTA OTA password length 
			OTA_ARDUINOOTA_PASSWORD = 480,			//(63 bytes max) c string containing password for OTA

			OTA_WEBBROWSER_PORT = 550,				//(2 bytes) port to use for Webbrowser based OTA update.
			OTA_WEBBROWSER_PATH_LENGTH = 552,		//(1 byte) length of path to Webbrowser OTA update page. 
			OTA_WEBBROWSER_PATH = 553,				//(63 bytes max) path to Webbrowser OTA update page. 
			OTA_WEBBROWSER_USERNAME_LENGTH = 616,	//(1 byte) length of Webbrowser OTA update page username.
			OTA_WEBBROWSER_USERNAME = 617,			//(63 bytes max) Webbrowser OTA update page username.
			OTA_WEBBROWSER_PASSWORD_LENGTH = 680,	//(1 byte) length of Webbrowser OTA update page password.
			OTA_WEBBROWSER_PASSWORD = 681,			//(63 bytes max) Webbrowser OTA update page password.

			OTA_HTTPSERVER_UPDATE_INTERVAL = 750,	//(4 bytes) checks for new versions every n milliseconds. set to 0 to disable. 
			OTA_HTTPSERVER_ADDRESS_LENGTH = 752,
			OTA_HTTPSERVER_ADDRESS = 753,
			OTA_HTTPSERVER_PATH_LENGTH = 816,
			OTA_HTTPSERVER_PATH = 817,
			OTA_HTTPSERVER_PORT = 880,
#endif /* defined(ESP8266) || defined(ESP32) */

				//#ifdef FEATURE_LoRa
				LoRa_ENABLED = 1024,					//(1 byte) true to enable LoRa 
				LoRa_SECURE = 1025,						//(1 byte) true to secure LoRa
				LoRa_CRC = 1026,						//(1 byte) true to enable CRC
				LoRa_INVERTIQ = 1027,					//(1 byte) true to enable invert IQ

				LoRa_PIN_CS = 1030,						//(1 byte) CS pin
				LoRa_PIN_RST = 1031,					//(1 byte) RST pin. set to 255 if the reset pin is not MCU controlled
				LoRa_PIN_DIO0 = 1032,					//(1 byte) DIO0 pin
				LoRa_PIN_DIO1 = 1033,					//(1 byte) DIO1 pin
				LoRa_PIN_DIO2 = 1034,					//(1 byte) DIO2 pin
				LoRa_SPI_FREQUENCY = 1035,				//(4 bytes) SPI frequency

#if defined(ESP8266) || defined(ESP32)
			//SPI pins are only used on ESP8266/ESP32.
				LoRa_SPI_SCK = 1040,					//(1 byte) SPI SCK pin
				LoRa_SPI_MISO = 1041,					//(1 byte) SPI MISO pin
				LoRa_SPI_MOSI = 1042,					//(1 byte) SPI MOSI pin
#endif /* defined(ESP8266) || defined(ESP32) */

				LoRa_PIN_OUTPUT = 1050,					//(2 bytes) RF output pin of LoRa frontend
				LoRa_TX_POWER = 1052,					//(2 bytes) tx power
				LoRa_SPREADING_FACTOR = 1054,			//(2 bytes) spreading factor
				LoRa_BAND = 1056,						//(4 bytes) LoRa band, in Hz (as float)
				LoRa_BANDWIDTH = 1060,					//(4 bytes) signal bandwidth 
				LoRa_CODING_RATE = 1064,				//(2 bytes) coding rate 
				LoRa_SYNCWORD = 1066,					//(2 bytes) sync word
				LoRa_PREAMBLE_LENGTH = 1068,			//(2 bytes) preamble length
				//#endif /* FEATURE_LoRa */

				//#ifdef FEATURE_MQTT
				MQTT_ENABLED = 1536,					//(1 byte) set to true to enable MQTT. 
				MQTT_SECURE = 1537,						//(1 byte) set to true to enable MQTT via SSL/TLS. 
				MQTT_SERVER = 1538,						//(63 bytes max) MQTT server, ip (as string) or domain name.
				MQTT_PORT = 1602,						//(2 bytes) MQTT port
				MQTT_CLIENT_ID = 1604,					//(63 bytes max) MQTT client ID
				MQTT_USER = 1668,						//(63 bytes max) MQTT user name
				MQTT_PASSWORD = 1732,					//(63 bytes max) MQTT password
				MQTT_TOPIC_PUBLISHED = 1796,			//(63 bytes max) MQTT published topic root.
				MQTT_TOPIC_SUBSCRIBED = 1860,			//(63 bytes max) MQTT subscribed topic. set to "" to disable subscription.
				//#endif /* FEATURE_MQTT */

				//DEVICE_NAME = 190,					//(32 bytes) device name
				//
				//DEVICE_INFO = 222						//(32 bytes) additional device information
		};
	};

	struct camera_rotator_variable_
	{
		enum CameraRotatorVariable_t_ : uint16_t
		{
		};
	};

	struct dome_variable_
	{
		enum DomeVariable_t_ : uint16_t
		{
			SHUTTER_POS_OPENED = 12,			//altitude of shutter in "opened" position. 
			SHUTTER_POS_CLOSED = 16,			//altitude of shutter in "closed" position.
		};
	};

	struct dome_axis_variable_
	{
		enum DomeAxisVariable_t_ : uint16_t
		{
			HOME_ALIGNMENT = 55,
		};
	};

	struct filter_wheel_variable_
	{
		enum FilterWheelVariable_t_ : uint16_t
		{
			//TODO add homing timout variable
			MAX_SPEED = 0,						//(4 byte) maximum stepper speed.
			ACCELERATION = 4,					//(4 byte) stepper acceleration.

			STEPTYPE = 8,						//(1 byte) used steptype.

			NUMBER_OF_STEPS = 9,				//(2 byte) number of steps per revolution of the stepper.
			GEAR_RATIO = 11,					//(4 byte) gear ratio of filter wheel drive.

			HOME_SWITCH_ENABLED = 15,			//(1 byte) if a home position switch is available or not.
			HOME_SWITCH = 16,					//(1 byte) digital input number of the home switch.
			HOME_SWITCH_INVERTED = 17,			//(1 byte) if the home switch logic is inverted.
			HOME_OFFSET = 18,					//(4 byte) home position offset from filter 0 (in steps).

			STARTUP_AUTO_HOME = 22,				//(1 byte) if the filter wheel should auto home on startup or not.
			AUTOMATIC_STEPPER_RELEASE = 23,		//(1 byte) if stepper auto release is enabled or not.

			ENDSTOP_LOWER_ENABLED = 24,			//(1 byte) if an endstop switch is available.
			ENDSTOP_UPPER_ENABLED = 25,			//(1 byte) if an endstop switch is available.
			ENDSTOP_UPPER = 26,					//(1 byte) digital input number of the endstop switch.
			ENDSTOP_LOWER = 27,					//(1 byte) digital input number of the endstop switch.
			ENDSTOP_LOWER_INVERTED = 28,		//(1 byte) if the lower endstop logic is inverted.
			ENDSTOP_UPPER_INVERTED = 29,		//(1 byte) if the upper endstop logic is inverted.
			ENDSTOP_HARD_STOP = 30,				//(1 byte) if a hard stop should be executed when an endstop is hit.

			LIMIT_LOWER_ENABLED = 31,			//(1 byte) if a lower movement limit is enabled or not.
			LIMIT_UPPER_ENABLED = 32,			//(1 byte) if a upper movement limit is enabled or not.
			LIMIT_LOWER = 33,					//(4 byte) lower movement limit.
			LIMIT_UPPER = 37,					//(4 byte) upper movement limit.
			LIMIT_HARD_STOP = 41,				//(1 byte) if a hard stop should be executed when a limit is exceeded.

			MOTOR_DRIVER_TYPE = 42,				//(1 byte) motor driver type.

			I2C_ADDRESS = 43,					//(1 byte) I2C Address of the motor shield.
			TERMINAL = 44,						//(1 byte) motor shield terminal the stepper is connected to.

			PIN_1 = 45,							//(1 byte) pin 1 (for 4 wire stepper / step pin).
			PIN_2 = 46,							//(1 byte) pin 2 (for 4 wire stepper / direction pin).
			PIN_3 = 47,							//(1 byte) pin 3 (for 4 wire stepper / enable pin).
			PIN_4 = 48,							//(1 byte) pin 4 (for 4 wire stepper / MS1).
			PIN_5 = 49,							//(1 byte) pin 5 (MS2).
			PIN_6 = 50,							//(1 byte) pin 6 (MS3).

			PIN_1_INVERTED = 51,				//(1 byte) if pin 1 is inverted (4 wire stepper / enable pin).
			PIN_2_INVERTED = 52,				//(1 byte) if pin 2 is inverted (4 wire stepper / direction pin).
			PIN_3_INVERTED = 53,				//(1 byte) if pin 3 is inverted (4 wire stepper / step pin).
			PIN_4_INVERTED = 54,				//(1 byte) if pin 4 is inverted (4 wire stepper / MS1).
			PIN_5_INVERTED = 55,				//(1 byte) if pin 5 is inverted (MS2).
			PIN_6_INVERTED = 56,				//(1 byte) if pin 6 is inverted (MS3).

			MC_AVAILABLE = 57,					//(1 byte) if manual control is available or not.
			MC_BTN_LOCK_AVAILABLE = 58,			//(1 byte) if manual control toggle button is available.
			MC_BTN_NEXT_AVAILABLE = 59,			//(1 byte) if manual control next filter button is available.
			MC_BTN_PREVIOUS_AVAILABLE = 60,		//(1 byte) if manual control previous filter button is available.
			MC_BTN_LOCK = 61,					//(1 byte) input pin number for manual control toggle button.
			MC_BTN_NEXT = 62,					//(1 byte) input pin number for next filter button.
			MC_BTN_PREVIOUS = 63,				//(1 byte) input pin number for previous filter button.	

			NUMBER_OF_FILTERS = 64,				//(1 byte) number of filters in the filter wheel.
			FILTER_HOLDER_TYPE = 65,			//(1 byte) type of filter holder (slider or wheel)
			//FOCUS_OFFSET = 66,					//(4 byte) filter offsets for the filters in microns.

			HOME_TIMEOUT = 66,					//(4 bytes) homing timeout in milliseconds	

			FILTER_INFO = 110,					//(20*10 bytes) structs containing filter information.

			TEMPERATURE_SENSOR = 451			//(3*20 bytes) temperature sensor settings as TemperatureSensor::SensorSettings Struct	
		};
	};

	struct focuser_variable_
	{
		enum FocuserVariable_t_ : uint16_t
		{
			MAX_SPEED = 0,						//(4 byte) maximum stepper speed.
			//ACCELERATION = 4,					//(4 byte) stepper acceleration.

			GEAR_RATIO = 10,					//(4 byte) gear ratio of focuser stepper to focuser knob.
			NUMBER_OF_STEPS = 14,				//(2 byte) number of steps of the stepper motor.

			STEP_TYPE = 18,						//(1 byte) used steptype.
			//STEP_TYPE_TEMP_COMP = 19,			//(1 byte) step type used during temperature compensation.

			HOME_SWITCH_ENABLED = 20,			//(1 byte) if a home position switch is available or not.
			HOME_SWITCH = 21,					//(1 byte) digital input number of the home switch.
			HOME_SWITCH_INVERTED = 22,			//(1 byte) digital input number of the home switch.
			HOME_POSITION = 24,					//(4 byte) home position in steps.

			STARTUP_AUTO_HOME = 28,				//(1 byte) if the focuser should auto home on startup or not.

			AUTOMATIC_STEPPER_RELEASE = 29,		//(1 byte) if stepper auto release is enabled or not.

			ENDSTOP_LOWER_ENABLED = 30,			//(1 byte) if a lower endstop is available or not.
			ENDSTOP_UPPER_ENABLED = 31,			//(1 byte) if an upper endstop is available or not.
			ENDSTOP_LOWER = 32,					//(1 byte) digital input number of the lower endstop.
			ENDSTOP_UPPER = 33,					//(1 byte) digital input number of the upper endstop.
			ENDSTOP_LOWER_INVERTED = 34,		//(1 byte) if the lower endstop logic is inverted.
			ENDSTOP_UPPER_INVERTED = 35,		//(1 byte) if the upper endstop logic is inverted.
			//ENDSTOP_HARD_STOP = 36,				//(1 byte) if a hard stop should be executed when an endstop is hit.

			LIMIT_LOWER_ENABLED = 40,			//(1 byte) if a lower movement limit for the is enabled or not.
			LIMIT_UPPER_ENABLED = 41,			//(1 byte) if a upper movement limit for the is enabled or not.
			LIMIT_LOWER = 42,					//(4 byte) lower movement limit.	(most outward position - should be 0).
			LIMIT_UPPER = 46,					//(4 byte) upper movement limit.
			//LIMIT_HARD_STOP = 44,				//(1 byte) if a hard stop should be executed when a limit is exceeded.
			//LIMIT_HANDLING = 45,				//(1 byte) how movement limit violations when setting targets is to be handled. IGNORE (dangerous?), ADJUST, ERROR.

			MOTOR_DRIVER_TYPE = 51,				//(1 byte) type of stepper motor driver.  

			I2C_ADDRESS = 52, 					//(1 byte) I2C Address of the motor shield (for Adafruit Motor Shield). 
			TERMINAL = 53,						//(1 byte) motor shield terminal the stepper is connected to (for Adafruit Motor Shield). 

			PIN_1 = 54,							//(1 byte) pin 1 (for 4 wire stepper / step pin).
			PIN_2 = 55,							//(1 byte) pin 2 (for 4 wire stepper / direction pin).
			PIN_3 = 56,							//(1 byte) pin 3 (for 4 wire stepper / enable pin).
			PIN_4 = 57,							//(1 byte) pin 4 (for 4 wire stepper / MS1).
			PIN_5 = 58,							//(1 byte) pin 5 (MS2).	
			PIN_6 = 59,							//(1 byte) pin 6 (MS3).

			PIN_1_INVERTED = 60,				//(1 byte) if pin 1 is inverted (4 wire stepper / enable pin).
			PIN_2_INVERTED = 61,				//(1 byte) if pin 2 is inverted (4 wire stepper / direction pin).
			PIN_3_INVERTED = 62,				//(1 byte) if pin 3 is inverted (4 wire stepper / step pin).
			PIN_4_INVERTED = 63,				//(1 byte) if pin 4 is inverted (4 wire stepper / MS1).
			PIN_5_INVERTED = 64,				//(1 byte) if pin 5 is inverted (MS2).
			PIN_6_INVERTED = 65,				//(1 byte) if pin 6 is inverted (MS3).

			MC_AVAILABLE = 70,					//(1 byte) if manual control is available or not.
			MC_BTN_LOCK_AVAILABLE = 71,			//(1 byte) if lock button is available.
			MC_BTN_LOCK = 72,					//(1 byte) input pin number for lock button.
			MC_BTN_IN = 73,						//(1 byte) input pin number for "in" button.
			MC_BTN_OUT = 74,					//(1 byte) input pin number for "out" button.	
			MC_BTN_LOCK_INVERTED = 75,			//(1 byte) if input pin number for lock button is inverted.
			MC_BTN_IN_INVERTED = 76,			//(1 byte) if input pin number for "in" button is inverted.
			MC_BTN_OUT_INVERTED = 77,			//(1 byte) if input pin number for "out" button is inverted.	

			POSITION_ABSOLUTE = 80,				//(1 byte) true if the focuser uses absolute positioning.
			MICRONS_PER_REV = 82,				//(4 byte) number of microns per 1 revolution of the focuser knob.
			MAX_INCREMENT = 86,					//(4 byte) maximum increment size allowed (in steps). 
			MAX_STEP = 90,						//(4 byte) maximum number of steps.

			HOME_TIMEOUT = 94,					//(4 bytes) homing timeout in milliseconds

			TC_AVAILABLE = 100,					//(1 byte) true if temperature compensation is available.
			TC_SENSOR = 101,					//(1 byte) sensor number to use for temperature compensation.
			TC_FACTOR = 102,					//(4 byte) temperature compensation factor in micros / degree.

			TEMPERATURE_SENSOR = 451			//(3*20 bytes) temperature sensor settings as TemperatureSensor::SensorSettings Struct	
		};
	};

	struct safety_monitor_variable_
	{
		enum SafetyMonitorVariable_t_ : uint16_t
		{
		};
	};

	struct telescope_variable_
	{
		enum TelescopeVariable_t_ : uint16_t
		{
			MOUNT_TYPE = 0,							//(1 byte) type of the mount (AltAz, Eq, GerEq)

			SITE_LATITUDE = 4,						//(4 byte) telescope site latitude.
			SITE_LONGITUDE = 8,						//(4 byte) telescope site longitude.
			SITE_ELEVATION = 12,					//(4 byte) telescope site elevation.

			FOCAL_LENGTH = 16,						//(4 byte) focal length of the telescope (in meters)
			APERTURE_DIAMETER = 20,					//(4 byte) aperture diameter of the telescope (in meters)
			APERTURE_AREA = 24,						//(4 byte) aperture area of the telescope (in meters^2)

			PARK_ENABLED = 30,						//(1 byte) if the telescope can be parked or not.
			//PARK_POS_X,							//(4 byte) x axis park position (in steps). 
			//PARK_POS_Y,							//(4 byte) y axis park position (in steps).

			STARTUP_AUTO_HOME = 31,					//(1 byte) if the telescope should auto home on startup or not.

			MC_AVAILABLE = 32,						//(1 byte) if manual control is available for this telescope.
			MC_BTN_TOGGLE_AVAILABLE = 33,			//(1 byte) true if the manual control enable / disable button is available.
			MC_BTN_SPEED_AVAILABLE = 34,			//(1 byte) true if the manual control speed select button is available.
			MC_JST_AVAILABLE = 35,					//(1 byte) true if the manual control joystick is available.
			MC_BTN_TOGGLE = 36,						//(1 byte) enable manual control button input pin.
			MC_BTN_SPEED = 37,						//(1 byte) manual control steptype toggle button input pin.
			MC_JST_INPUT_X = 38,					//(1 byte) joystick x axis input pin.
			MC_JST_INPUT_Y = 39,					//(1 byte) joystick y axis input pin.
			MC_JST_DEADZONE = 40,					//(1 byte) joystick deadzone.
			//MANUAL_CONTROL_MAX_SPEED,				//(4 byte) maximum axis speed for manual control (in degrees / second)
			//MANUAL_CONTROL_DEFAULT_STEPTYPE,		//(1 byte) default manual control steptype.

			TEMPERATURE_SENSOR = 67					//(3*20 bytes) temperature sensor settings as TemperatureSensor::SensorSettings Struct	

		};
	};

	struct telescope_axis_variable_
	{
		enum TelescopeAxisVariable_t_ : uint16_t
		{
			MAX_SPEED = 0,						//(4 byte) maximum stepper speed. 
			ACCELERATION = 4, 					//(4 byte) stepper acceleration. 

			STEPTYPE_FAST = 8,					//(1 byte) steptype for fast movements (slewing, homing,..). 
			STEPTYPE_SLOW = 9,					//(1 byte) steptype for slow movements (tracking, pulseguiding). 

			GEAR_RATIO = 10,					//(4 byte) gear ratio of this axis' drive. 
			NUMBER_OF_STEPS = 14,				//(2 byte) number of steps per revolution of the stepper. 

			HOME_SWITCH_ENABLED = 16,			//(1 byte) if a home position switch is available or not. 
			HOME_SWITCH = 17,					//(1 byte) digital input number of the home switch. 
			HOME_SWITCH_INVERTED = 18,			//(1 byte) if the home switch logic is inverted.
			HOMING_DIRECTION = 19,				//(2 byte) homing direction.
			HOME_ALIGNMENT = 21,				//(4 byte) home orientation (in degrees).
			HOME_ALIGNMENT_SYNC = 25,			//(1 byte) true if home position can be synced.

			//STARTUP_AUTO_HOME,				//(1 byte) if the axis should auto home on startup or not.
			AUTOMATIC_STEPPER_RELEASE = 26,		//(1 byte) if stepper auto release is enabled or not.
			//PARK_ENABLED,					//(1 byte) if the axis can be parked or not.
			PARK_POS = 28,						//(4 byte) park position (in degrees). 

			ENDSTOP_LOWER_ENABLED = 32,			//(1 byte) if a lower endstop is available or not.
			ENDSTOP_UPPER_ENABLED = 33,			//(1 byte) if an upper endstop is available or not.
			ENDSTOP_LOWER = 34,					//(1 byte) digital input number of the lower endstop.
			ENDSTOP_UPPER = 35,					//(1 byte) digital input number of the upper endstop.
			ENDSTOP_LOWER_INVERTED = 36,		//(1 byte) if the lower endstop logic is inverted.
			ENDSTOP_UPPER_INVERTED = 37,		//(1 byte) if the upper endstop logic is inverted.
			ENDSTOP_HARD_STOP = 38,				//(1 byte) if a hard stop should be executed when an endstop is hit.

			LIMIT_LOWER_ENABLED = 39,			//(1 byte) if a lower movement limit for the is enabled or not.
			LIMIT_UPPER_ENABLED = 40,			//(1 byte) if a upper movement limit for the is enabled or not.
			LIMIT_LOWER = 41,					//(4 byte) lower movement limit.
			LIMIT_UPPER = 45,					//(4 byte) upper movement limit.
			LIMIT_HARD_STOP = 49,				//(1 byte) if a hard stop should be executed when a limit is exceeded.
			LIMIT_HANDLING = 50,				//(1 byte) how movement limit violations when setting targets is to be handled. IGNORE (dangerous?), ADJUST, ERROR.

			MOTOR_DRIVER_TYPE = 51,				//(1 byte) type of stepper motor driver.  

			I2C_ADDRESS = 52, 					//(1 byte) I2C Address of the motor shield (for Adafruit Motor Shield). 
			TERMINAL = 53,						//(1 byte) motor shield terminal the stepper is connected to (for Adafruit Motor Shield). 

			PIN_1 = 54,							//(1 byte) pin 1 (for 4 wire stepper / step pin).
			PIN_2 = 55,							//(1 byte) pin 2 (for 4 wire stepper / direction pin).
			PIN_3 = 56,							//(1 byte) pin 3 (for 4 wire stepper / enable pin).
			PIN_4 = 57,							//(1 byte) pin 4 (for 4 wire stepper / MS1).
			PIN_5 = 58,							//(1 byte) pin 5 (MS2).	
			PIN_6 = 59,							//(1 byte) pin 6 (MS3).

			PIN_1_INVERTED = 60,				//(1 byte) if pin 1 is inverted (4 wire stepper / enable pin).
			PIN_2_INVERTED = 61,				//(1 byte) if pin 2 is inverted (4 wire stepper / direction pin).
			PIN_3_INVERTED = 62,				//(1 byte) if pin 3 is inverted (4 wire stepper / step pin).
			PIN_4_INVERTED = 63,				//(1 byte) if pin 4 is inverted (4 wire stepper / MS1).
			PIN_5_INVERTED = 64,				//(1 byte) if pin 5 is inverted (MS2).
			PIN_6_INVERTED = 65,				//(1 byte) if pin 6 is inverted (MS3).

			HOME_TIMEOUT = 70,					//(4 bytes) homing timeout in milliseconds	

			//TEMPERATURE_SENSOR = 91				//(3*20 bytes) temperature sensor settings as TemperatureSensor::SensorSettings Struct
		};
	};

	enum data_type_t
	{
		UINT8_T_,		//8bit unsigned integer.
		UINT16_T_,		//16bit unsigned integer.
		UINT32_T_,		//32bit unsigned integer.
		INT16_T_,		//16bit signed integer.
		INT32_T_,		//32bit signed integer.
		FLOAT_,			//32bit float.
		ARRAY_,
		SENSOR_SETTINGS_,	//YAAASensor::SensorSettings struct.
		FILTER_INFO_,	//FilterWheel::FilterInfo struct.
		BUTTON_INFO_,
		KEYPAD_INFO_,
		STRING_,		//null terminated c string.
		UNKNOWN_ = 255
	};

public:
	EEPROMHandler();
	~EEPROMHandler();

	/*
	initializes the device's EEPROM, if necessary. */
	static bool begin();

	/*returns the variable type for the given variable of the given device.
	@param:
	- device identifier (EEPROMHandler::device_type_)
	- option identifier (EEPROMHandler::[device_]variable_)
	@return:
	- variable size as variable_size_t.*/
	static uint8_t getVariableType(uint16_t device, uint16_t variable);

	/*
	parses a string to a variable.
	@param device:
	@param string: string containing variable name
	@param variable:
	@return: true if the variable was parsed correctly, false otherwise.*/
	static uint16_t stringToVariable(uint16_t device, char* string);

	/*
	gets the length of a string stored in EEPROM.
	@param device
	@param variable
	@return length of String in EEPROM. */
	static uint8_t getEEPROMStringLength(uint16_t device, uint16_t variable);

	/*
	sets the length of a string stored in EEPROM.
	@param device
	@param variable
	@param length to set
	@return true on success, false otherwise. */
	static bool setEEPROMStringLength(uint16_t device, uint16_t variable, uint8_t length);

	/*
	gets the maximum length of a string stored in EEPROM.
	@param device
	@param variable
	@return maximum length of String in EEPROM. */
	static uint8_t getEEPROMMaxStringLength(uint16_t device, uint16_t variable);

	/*
	writes a given variable to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param value: value to write.*/
	template <class T> static void writeValue(uint16_t device, uint16_t variable, uint16_t index, const T& value)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
		//use Due Flash Storage library
		uint16_t size = sizeof(value);
		byte content[size];

		memcpy(content, &value, size);

		//does not overwrite same value
		for (uint8_t i = 0; i < size; i++)
		{
			if (content[i] != dfs_.read(address + i))
				dfs_.write(address + i, content[i]);
		}
#elif defined(ESP8266) || defined(ESP32)
		EEPROM.put(address, value);

		EEPROM.commit();

		yield();
#else 
		EEPROM.put(address, value);
#endif
#endif /* EEPROM_WRITE_LOCK */
	}

	/*
	reads a given variable to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param value: value to read (by reference).*/
	template <class T> static void readValue(uint16_t device, uint16_t variable, uint16_t index, T& value)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

#ifdef ARDUINO_SAM_DUE
		//use Due Flash Storage library
		byte* content = dfs_.readAddress(address);
		memcpy(&value, content, sizeof(value));
#elif defined(ESP8266) || defined(ESP32)
		EEPROM.get(address, value);

		yield();
#else 
		EEPROM.get(address, value);
#endif
	}

	/*
	reads a given array from the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to array to read to.
	@param size: number of bytes to read.*/
	static void readArray(uint16_t device, uint16_t variable, uint16_t index, byte *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifdef ARDUINO_SAM_DUE
			content[i] = dfs_.read(address + i);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.get(address + i, content[i]);

			yield();
#else
			EEPROM.get(address + i, content[i]);
#endif
		}
	}

	/*
	writes a given array to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to array to write.
	@param size: number of bytes to write.*/
	static void writeArray(uint16_t device, uint16_t variable, uint16_t index, byte *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
			if (content[i] != dfs_.read(address + i))
				dfs_.write(address + i, content[i]);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.put(address + i, content[i]);
#else
			EEPROM.put(address + i, content[i]);
#endif
		}

#if defined(ESP8266) || defined(ESP32)
		EEPROM.commit();

		yield();
#endif
#endif /* EEPROM_WRITE_LOCK */
	}

	/*
	reads a given array from the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to char array to read to.
	@param size: number of chars to read.*/
	static void readArray(uint16_t device, uint16_t variable, uint16_t index, char *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifdef ARDUINO_SAM_DUE
			content[i] = dfs_.read(address + i);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.get(address + i, content[i]);

			yield();
#else
			EEPROM.get(address + i, content[i]);
#endif
		}
	}

	/*
	writes a given array to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to char array to write.
	@param size: number of chars to write.*/
	static void writeArray(uint16_t device, uint16_t variable, uint16_t index, char *content, uint16_t size)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		for (uint8_t i = 0; i < size; i++)
		{
#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
			if (content[i] != dfs_.read(address + i))
				dfs_.write(address + i, content[i]);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.put(address + i, content[i]);
#else
			EEPROM.put(address + i, content[i]);
#endif
		}

#if defined(ESP8266) || defined(ESP32)
		EEPROM.commit();

		yield();
#endif
#endif /* EEPROM_WRITE_LOCK */
	}

	/*
	reads a given array from the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: pointer to char array to read to.
	@param size: number of chars to read.*/
	static void readString(uint16_t device, uint16_t variable, uint16_t index, String &content)
	{
		uint16_t address = device + variable + calculateOffset(device, variable, index);

		char buffer[64];

		for (uint8_t i = 0; i < 64; i++)
		{
#ifdef ARDUINO_SAM_DUE
			buffer[i] = dfs_.read(address + i);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.get(address + i, buffer[i]);
#else
			EEPROM.get(address + i, buffer[i]);
#endif
			//stop reading if a '\0' char was read
			if (buffer[i] == '\0')
				break;
		}

		//add a '\0' character at the end, just in case
		buffer[63] == '\0';

		//create string
		content = String(buffer);

		yield();
	}

	/*
	writes a string to the Arduinos EEPROM.
	@param device: device as DeviceType_t_
	@param variable: variable as [device]Variable_t
	@param index: additional index (i.e. filter number)
	@param content: String to write. */
	static void writeString(uint16_t device, uint16_t variable, uint16_t index, String content)
	{
		//cut string to size
#if defined (ESP8266) || defined(ESP32)
		content.remove(_min(static_cast<uint8_t>(63), getEEPROMMaxStringLength(device, variable)));
#else
		content.remove(min(static_cast<uint8_t>(63), getEEPROMMaxStringLength(device, variable)));
#endif

		uint16_t address = device + variable + calculateOffset(device, variable, index);

		//make sure to save the '\0' character as well
		for (uint8_t i = 0; i <= content.length(); i++)
		{
#ifndef EEPROM_WRITE_LOCK
#ifdef ARDUINO_SAM_DUE
			if (content.c_str()[i] != dfs_.read(address + i))
				dfs_.write(address + i, content.c_str()[i]);
#elif defined(ESP8266) || defined(ESP32)
			EEPROM.put(address + i, content.c_str()[i]);
#else
			EEPROM.put(address + i, content.c_str()[i]);
#endif
		}

#if defined(ESP8266) || defined(ESP32)
		EEPROM.commit();

		yield();
#endif
#endif /* EEPROM_WRITE_LOCK */
	}

private:
	/*
	calculates a device and variable dependent offset for a given variable of a given
	device in the Arduinos EEPROM.
	@param device: device as DeviceType_t.
	@param variable: variable as [device]Variable_t.
	@param index: additional index (i.e. filter number).
	@return: offset from address 0 in bytes.*/
	static uint16_t calculateOffset(uint16_t device, uint16_t variable, uint16_t index);

#ifdef ARDUINO_SAM_DUE
	static DueFlashStorage dfs_;
#endif
};

#endif /* EEPROMHANDLER_H_ */

