//Generic Keypad class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "YAAAKeypad_Generic.h"

YAAAKeypad_Generic::YAAAKeypad_Generic(YAAAKeypad::KeypadSettings settings) :
	YAAAKeypad(YAAAKeypad::TYPE_GENERIC)
{
	read_interval_ = settings.read_interval_;

	uint8_t pins[YAAAKEYPAD_GENERIC_BUTTONS];

	for (uint8_t i = 0; i < YAAAKEYPAD_GENERIC_BUTTONS; i++)
	{
		pins[i] = settings.parameters_[i * 2];
		key_codes_[i] = settings.parameters_[i * 2 + 1];

		if (pins[i] != 255)
			buttons_[i] = new Button(pins[i], settings.active_high_);
		else
			buttons_[i] = NULL;
	}
}

YAAAKeypad_Generic::~YAAAKeypad_Generic()
{
	for (uint8_t i = 0; i < YAAAKEYPAD_GENERIC_BUTTONS; i++)
		delete buttons_[i];
}

YAAAKey YAAAKeypad_Generic::getKey()
{
	read_last_ = millis();

	//read currently pressed key
	uint8_t key_code = getKeyCode(read());

	updateKey(key_code);

	return key_;
}

uint16_t YAAAKeypad_Generic::read()
{
	uint16_t return_value = 0;

	for (uint8_t i = 0; i < YAAAKEYPAD_GENERIC_BUTTONS; i++)
	{
		if (buttons_[i])
			return_value |= buttons_[i]->getState() << i;
	}

	return return_value;
}

uint8_t YAAAKeypad_Generic::getKeyCode(uint16_t bits)
{
	uint8_t key_code = YAAAKey::KEY_NONE;

	uint8_t num_set_bits = 0;

	//count set bits. keypress is only valid when exactly 1 bit is set.
	for (uint8_t i = 0; i < 16; i++)
		num_set_bits += (bits >> i & 1);

	if (num_set_bits == 1)
	{
		uint8_t index = 0;

		for (uint8_t i = 0; i < YAAAKEYPAD_GENERIC_BUTTONS; i++)
			if (bits && (1 << i))
				index = i;

		key_code = key_codes_[index];
	}
	else if (num_set_bits == 0)
		key_code = YAAAKey::KEY_NONE;

	else
		key_code = YAAAKey::KEY_INVALID;

	return key_code;
}

