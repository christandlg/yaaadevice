//Generic Keypad class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAAKEYPAD_GENERIC_H_
#define YAAAKEYPAD_GENERIC_H_

#include <Arduino.h>
#include "YAAAKeypad.h"

#include "Button.h"

#define YAAAKEYPAD_GENERIC_BUTTONS 6

class YAAAKeypad_Generic :
	public YAAAKeypad
{
public:
	YAAAKeypad_Generic(YAAAKeypad::KeypadSettings settings);
	~YAAAKeypad_Generic();

	/*
	@return the pressed key as YAAAKeypad::key_t*/
	YAAAKey getKey();

private:
	/*
	reads the inputs pins of the keypad and returns 2 bytes indicating the state of
	the input pins.
	@return: 2 bytes containing indices of pressed buttons.
	*/
	uint16_t read();

	uint8_t getKeyCode(uint16_t bits);

	Button *buttons_[YAAAKEYPAD_GENERIC_BUTTONS];

	uint8_t key_codes_[YAAAKEYPAD_GENERIC_BUTTONS];

	bool active_high_;
};

#endif /* YAAAKEYPAD_GENERIC_H_ */

