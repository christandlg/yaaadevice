//Four Wire Stepper motor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef STEPPERMOTOR_FOUR_WIRE_H_
#define STEPPERMOTOR_FOUR_WIRE_H_

#include "StepperMotor.h"

class StepperMotorFourWire :
	public StepperMotor
{
public:
	StepperMotorFourWire(uint8_t pin1 , uint8_t pin2, uint8_t pin3, uint8_t pin4, bool enable = false);

	~StepperMotorFourWire();

	////@return: the currently set step type as step_type_t step type.
	//uint8_t getStepType();

	//sets the current steptype to the steptype provided and recalculates stepper position when
	//steptype has changed. 
	//@return: 
	 //- true when the step type was set successfully.
	 //- false when an invalid step type has been given for this stepper driver.
	bool setStepType(uint8_t step_type);
};

#endif /* STEPPERMOTOR_FOUR_WIRE_H_ */

