//OneWire Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorTemperatureOneWire.h"

SensorTemperatureOneWire::SensorTemperatureOneWire(YAAASensor::SensorSettings sensor_settings) :
SensorTemperature(sensor_settings),
resolution_(SensorTemperatureOneWire::RESOLUTION_9_BIT),
th_register_(0), tl_register_(0),
raw_value_(0)
{
	memcpy(serial_number_, sensor_settings.parameters_, 8);

	//set the sensor to the given resolution.
	setResolution(sensor_settings.parameters_[9]);

	setUnit(sensor_settings.unit_);
}

SensorTemperatureOneWire::~SensorTemperatureOneWire()
{
	//nothing to do here...
}

bool SensorTemperatureOneWire::begin()
{
	//todo:
	//this function causes the arduino to hang when a serial number consisting of only 0x00s is given.

	//return false if the OneWire object has not been created.
	if (!one_wire_)
		return false;

	//abort when no one wire device responds with a presence pulse.
	if (!one_wire_->reset())
	{
		return false;
	}

	//check if the crc matches.
	if (OneWire::crc8(serial_number_, 7) != serial_number_[7])
	{
		return false;
	}

	//check if the one wire device is a temperature sensor.
	if (!checkFamily(serial_number_[0]))
	{
		return false;
	}

	//check communication by saving TH / TL registers.
	uint8_t data[9];
	if (!readScratchpad(data))
	{
		return false;
	}

	th_register_ = data[2];
	tl_register_ = data[3];
	//resolution_ = static_cast<resolution_t>(data[4]);		//not necessary

	return true;
}

uint8_t SensorTemperatureOneWire::getNumSensors()
{
	//return 0 if the One Wire object has not been created yet.
	if (!one_wire_)
		return 0;

	uint8_t num_sensors = 0;
	byte addr[8];

	one_wire_->reset_search();

	//count number of sensors found
	while (one_wire_->search(addr))
		if (checkFamily(addr[0]))
			num_sensors++;

	return num_sensors;
}

bool SensorTemperatureOneWire::getSensorSerial(uint8_t sensor, uint8_t *addr)
{
	//return false if the One Wire object has not been created yet.
	if (!one_wire_)
		return false;

	//return false if the sensor number is invalid.
	if (sensor > getNumSensors() - 1)
		return false;

	uint8_t curr_index = 0;

	one_wire_->reset_search();

	//get the address of the n-th temperature sensor.
	while (curr_index <= sensor)
	{
		one_wire_->search(addr);

		if (checkFamily(addr[0]))
			curr_index++;
	}

	//check CRC
	if (OneWire::crc8(addr, 7) == addr[7])
		return true;

	return false;
}

bool SensorTemperatureOneWire::checkFamily(uint8_t family)
{
	switch (family)
	{
		case DS18S20:
		case DS18B20:
		case DS1822:
			return true;
		default:
			return false;
	}
}

bool SensorTemperatureOneWire::measure()
{
	if (!one_wire_)
		return false;

	//start temperature conversion.
	if (!one_wire_->reset())
		return false;

	one_wire_->select(serial_number_);
	one_wire_->write(0x44);

	updateMeasurementTime();

	return true;
}

bool SensorTemperatureOneWire::hasValue()
{
	//return false if temperature sensor initialization failed.
	if (!one_wire_)
		return false;

	one_wire_->select(serial_number_);

	//return false if the temperature conversion is not finished yet.
	if (one_wire_->read() == 0)
		return false;

	return readTemperature();
}

float SensorTemperatureOneWire::getValue()
{
	float temp_celsius = static_cast<float>(raw_value_) / 16.0f;

	//convert temperature and return
	return(convertTemperature(
		temp_celsius,
		SensorTemperature::UNIT_CELSIUS,
		temperature_unit_));
}

bool SensorTemperatureOneWire::readTemperature()
{
	uint8_t data[9];

	one_wire_->select(serial_number_);

	//read scratchpad
	if (!readScratchpad(data))
		return false;

	//assemble 16bit integer from 2 bytes of data
	raw_value_ = ((data[1] << 8) | data[0]);

	//DS18S20 and DS1820 sensors need "special attention":
	//on DS18S20 and DS1820 higher resolutions than 9bit are achieved by reading 
	//COUNT_REMAIN from the scratchpad.
	if (serial_number_[0] == 0x10)
	{
		//shift by 3 bits to convert from DS18S20/DS1820 to the format all other DS18X20 sensors use.
		raw_value_ = raw_value_ << 3;

		//to get the higher resolution value:
		// - set bit 4 (former LSB) of raw_value to 0 (raw_value_ & 0xFFF0)
		raw_value_ = (raw_value_ & 0xFFF0);

		// - add 12 - COUNT_REMAIN to raw_value_ (calculation copied from OneWire example file)
		raw_value_ += 12 - data[6];
	}

	//all other sensors report the temperature in 12bit format, but with a number of
	//LSBs undefined (depending on resolution setting). 
	//TODO while we're at it lets also update the resolution setting
	else
	{
		uint8_t res_setting = data[4];// &0x60;

		switch (res_setting)
		{
			//case 0x00:
			case SensorTemperatureOneWire::RESOLUTION_9_BIT:
				raw_value_ &= 0xFFF8;		//trim 3 undefined bits
				//resolution_ = SensorTemperatureOneWire::RESOLUTION_9_BIT;
				break;
				//case 0x20:
			case SensorTemperatureOneWire::RESOLUTION_10_BIT:
				raw_value_ &= 0xFFFC;		//trim 2 undefined bits
				//resolution_ = SensorTemperatureOneWire::RESOLUTION_10_BIT;
				break;
				//case 0x40:
			case SensorTemperatureOneWire::RESOLUTION_11_BIT:
				raw_value_ &= 0xFFFE;		//trim 1 undefined bit
				//resolution_ = SensorTemperatureOneWire::RESOLUTION_11_BIT;
				break;
				//case 0x60:
			case SensorTemperatureOneWire::RESOLUTION_12_BIT:
				//resolution_ = SensorTemperatureOneWire::RESOLUTION_12_BIT;
				break;
			default:
				return false;
		}
	}

	return true;
}

uint8_t SensorTemperatureOneWire::getResolution()
{
	uint8_t return_value = 9;

	switch (resolution_)
	{
		case SensorTemperatureOneWire::RESOLUTION_9_BIT:
			return_value = 9;
		case SensorTemperatureOneWire::RESOLUTION_10_BIT:
			return_value = 10;
		case SensorTemperatureOneWire::RESOLUTION_11_BIT:
			return_value = 11;
		case SensorTemperatureOneWire::RESOLUTION_12_BIT:
			return_value = 12;
		default:
			return_value = 9;
	}

	return return_value;
}

void SensorTemperatureOneWire::setResolution(uint8_t resolution)
{
	if (serial_number_[0] == DS18S20)
		resolution = SensorTemperatureOneWire::RESOLUTION_12_BIT;

	else
	{
		switch (resolution)
		{
			case 10:
				resolution_ = SensorTemperatureOneWire::RESOLUTION_10_BIT;
				break;
			case 11:
				resolution_ = SensorTemperatureOneWire::RESOLUTION_11_BIT;
				break;
			case 12:
				resolution_ = SensorTemperatureOneWire::RESOLUTION_12_BIT;
				break;
			default:
				resolution_ = SensorTemperatureOneWire::RESOLUTION_9_BIT;
				break;
		}
	}

	//write scratchpad.
	writeScratchpad();
}

bool SensorTemperatureOneWire::readScratchpad(uint8_t *data)
{
	if (!one_wire_)
		return false;

	//read scratchpad
	one_wire_->reset();
	one_wire_->select(serial_number_);
	one_wire_->write(0xBE);

	for (uint8_t i = 0; i < 9; i++)
		data[i] = one_wire_->read();

	//abort if the CRC does not match.
	if (OneWire::crc8(data, 8) != data[8])
		return false;

	return true;
}

void SensorTemperatureOneWire::writeScratchpad()
{
	if (!one_wire_)
		return;

	//write scratchpad
	one_wire_->reset();
	one_wire_->select(serial_number_);

	one_wire_->write(0x4E);					//WRITE SCRATCHPAD command

	one_wire_->write(th_register_);			//write TH register
	one_wire_->write(tl_register_);			//write TL register

	if (serial_number_[0] != DS18S20)		//resolution setting cannot be written on DS1820 / DS18S20 devices.
		one_wire_->write(resolution_);		//write resolution setting

	one_wire_->reset();
	one_wire_->select(serial_number_);

	one_wire_->write(0x48);					//send COPY SCRATCHPAD command
	//delay(15);								//EEPROM write takes 10ms (parasitic mode), wait 15ms to be sure the write was successful
}
