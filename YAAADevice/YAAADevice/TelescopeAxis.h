//Telescope Axis class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef TELESCOPEAXIS_H_
#define TELESCOPEAXIS_H_

#include <Arduino.h>

#include <limits.h>

#include "defaults.h"

#include "EEPROMHandler.h"
#include "Button.h"

//#include "StepperMotor.h
#include "StepperMotorDriver.h"
#include "StepperMotorAFMSV2.h"
#include "StepperMotorFourWire.h"

class TelescopeAxis
{
public:
	enum direction_t	//direction for pulseGuide.
	{
		DIR_DOWN = 0,
		DIR_UP = 1
	};

	enum error_code_t : uint8_t //todo: add more error codes & change return value of most functions to uint16_t?
	{
		ERROR_NONE = 0,
		ERROR_OUT_OF_BOUNDS = 1,
		//ERROR_OUT_OF_BOUNDS_Y = 2,
		//ERROR_PARKED = 4,
		ERROR_MANUAL_CONTROL = 8,
		ERROR_PULSE_GUIDING = 16,	//todo: remove
		ERROR_HOMING = 32,
		ERROR_NOT_SLEWING = 64,
		ERROR_OTHER = 128		//todo: exchange for something meaningful.
	};

	enum error_code_homing_t : uint8_t
	{
		ERROR_HOMING_NONE = 0,							//no error occurred.
		ERROR_HOMING_LIMIT_LOWER = 8,					//homing position beyond lower movement limit
		ERROR_HOMING_LIMIT_UPPER = 16,					//homing position beyond upper movement limit
		ERROR_HOMING_SWITCH_NOT_AVAILABLE = 32,			//no home switch available.
		//ERROR_HOMING_SWITCH_NOT_TRIGGERED = 64,		//homing switch has not been triggered, but both limits or endstops have been hit.	//TODO implement
		ERROR_HOMING_TIMEOUT = 128						//homing operation timed out.
	};

	enum limit_handling_t : uint8_t		//what to do when trying to slew outside the movement limits.
	{
		LIMIT_HANDLING_IGNORE = 0,		//set slew target anyway. 
		LIMIT_HANDLING_ADJUST = 1,		//adjust slew target to limits.
		LIMIT_HANDLING_ERROR = 2		//return error.
	};

	enum telescope_axis_t : uint8_t
	{
		X_AXIS = 0,
		Y_AXIS = 1,
		Z_AXIS = 2
	};

	//constructor for telescope class.
	//does lots of things.
	TelescopeAxis(uint8_t axis);

	~TelescopeAxis();

	//this method handles automatic stepper movement. behaves differently depending on active 
	//movement type. 
	// - endstops and limits are checked.
	// - when the telescope is homing, calls clearHome() and home() (depending on homing state).
	// - when a slew command is enabled, calls slewing() (restores tracking when slew is finshed).
	// - when pulse guiding is enabled, calls pulseGuiding() to check if the pulse guiding duation
	//	 has expired.
	// - when tracking, pulseGuiding or moveAxis is active, calls x and y axis AccelStepper 
	//	 wrapper's runSpeed() to move the steppers at a constant speed. sets moving_ to true when
	//	 the stepper speed is not 0.0.
	// - when slewing, homing, parking, ... calls run() to move the steppers, implementing 
	//	 acceleration / deceleration.
	void run();

	//stops the stepper and all movement commands.
	// - sets homing_, tracking_, moveaxis_, slewing_ and pulse_guiding_ to false.
	// - calls stop(bool false) to stop the stepper (soft). 
	void stop();

	//stops the stepper. when hard_stop is true, stops the stepper immediately, bypassing
	//AccelStepper's stop() function (no acceleration).
	// - when hard_stop is true, sets the stepper's speed to 0 and the target to the current 
	//	 stepper position.
	// - when hard_stop is false, calls the AccelStepper wrapper's stop() function which 
	//	 calculates a new destination, taking into account the acceleration set for 
	//	 the stepper.
	//@param: 
	// - if a hard stop should be executed.
	void stop(bool hard_stop);

	//@return: returns true if the stepper is moving (both automatic or manual control). 
	// - returns true if the stepper is currently moving towards a targetor  at a specific rate.
	bool isMoving();

	//returns the current speed of the stepper (passthrough function for AccelStepper's speed(). 
	//at high speeds, this reading is inaccurate  due to the cpu not being able to initiate steps 
	//quickly enough.
	// - calls the AccelStepper wrapper's speed() function.
	//@return: (assumed) speed of the steppers in steps / second.
	float getSpeed();

	//initiates a slewing operation. target is given in degrees. 
	//this function will be called when the driver issues a slew command.
	// - converts given target to steps.
	// - adjusts target, returns error or does nothing depending on limit_handling_ setting.
	// - sets a new target for the stepper and sets slewing_ to true.
	uint8_t slew(float destination);

	/*
	checks if a slew to the given target is possible. 
	@param target
	@return true if a slew to the given target is possible, false otherwise. */
	bool canSlewTo(float target);

	//aborts a slewing operation.
	// - disables moveAxis command by calling moveAxis(0.0), this automatically restores tracking. 
	//	 does nothing when moveAxis was not enabled.
	// - when the telescope is slewing about this axis, stops the slew command and restores tracking.
	void abortSlew();

	//@param: the target to check.
	//@return: true if the target is within this axis' movement limits.
	bool withinLimits(float target);

	//returns the current alignment of the axis (in degrees).
	// - calls the AccelStepper wrapper's currentPosition() function (returns current stepper 
	//	 position in steps), and converts the position to degrees. 
	//@return:
	// - the axis' current alignment.
	float getCurrentAlignment();

	//returns the current slew target (in degrees).
	// - calls the AccelStepper wrapper's targetPosition() funtion (returns current stepper target
	//	 in steps) and converts the target to degrees.
	//@return: 
	// - the axis' current target position (in degrees).
	float getCurrentTarget();

	//sets the current alignment of this axis. does not move the steppers.
	// - converts degrees to steps and calls setCurrentPosition() so set new stepper position.
	//@param: alignment of the axis in degrees.
	void setCurrentAlignment(float alignment);

	//@return: the set target (not necessarily the target the telescope is slewing to)
	float getTarget();

	//@param target: slew target for this axis. for use with Telescope::slewToTarget().
	bool setTarget(float target);

	//stops all movement commands and sends the axis to its park position.
	//will fail when the park position is outside the telescope's movement limits.
	void park();

	//returns this axis' current park position.
	float getPark();

	//sets the axis' parking position to its current position and saves the position to EEPROM.
	// - updates the park position and saves the current stepper position to the arduino's EEPROM.
	void setPark();

	//checks if this axis' park position is within the movement limits.
	//@ return: 
	// - true if the park position is within the movement limits or limits are deactivated.
	// - false otherwise.
	bool canPark();

	/*
	checks if this axis can find its home position.*/
	bool canFindHome();

	//stops all movement commands and initiates homing process. if called during a homing operation, homing is restarted.
	// - returns immediately if the telescope is parked.
	// - disables moveAxis, pulseGuide and tracking movements.
	// - sets some variables needed for the homing process.
	// - when no home position switch is availalbe:
	// - - set the current position as the home position (failsafe mechanism).
	// - when a home position switch is available:
	// - - set homing direction depending on type of home position switch (upper / lower endstop, 
	//	   dedicated home position switch)
	// - - when the home position switch is triggered, reverse the homing direction to clear the 
	//	   switch and set home_cleared_ to false.
	// - - set an arbitrary target for the stepper.
	// - - enable homing mode by setting homing_ to true.
	//@return:
	// - true - homing activated.
	// - false - homing not activated or already finished.
	void findHome();

	//@return: 
	// - true if the axis is at its home position, false otherwise.
	bool atHome();

	//@return: true if homing is in progress for this axis.
	bool isHoming();

	/*
	@return: error codes as error_code_homing_t generated during the last find home operation.*/
	uint8_t getHomingError();

	//@return: home position in degrees.
	float getHomePosition();

	//sets tracking rate.
	// - set the tracking rate variable to the provided value. 
	//@params:
	// - tracking rate in degrees / second.
	void setTrackingRate(float rate);

	//@return: the currently set tracking rate in degrees / second.
	float getTrackingRate();

	//enables or disables tracking.
	//activating a Slewing operation, moveAxis or pulseGuide command will override the tracking 
	//command but will not reset tracking state (enabled / disabled).
	// - returns immediately when the telescope is parked.
	// - when enabling tracking:
	// - - disable moveAxis and pulseGuide commands and enable tracking.
	// - - try to set stepper speed. when it fails, return the error code. 
	// - - set an arbitrary stepper target according to the tracking rate.
	// - - return the result of setMaxSpeed().
	// - when disabling tracking:
	// - - disable tracking command.
	// - - stop the steppers' movement.
	//@params:
	// - requested tracking state.
	//@return:
	// 0 - no error.
	// 4 - telescope parked.
	void tracking(bool enable);

	//restores tracking to the state last set by the user / driver when no slewing or moveAxis 
	//commands are enabled. 
	void restoreTracking();

	//moves the axis at the given rate. 
	// - when the given speed is 0.0 and moveAxis command was active disables the moveAxis command.
	//	 restores tracking. does nothing when no moveAxis command was active.
	// - when the given speed != 0.0 activates moveAxis command and sets requested speed.
	//@params: 
	// - the speed in degrees / second. positive speed = CW, negative speed = CCW.
	// - if the function should set the step type to step_type_fast_.
	void moveAxis(float speed, bool set_step_type = true);

	//todo:
	//rework whole pulse guiding code.
	//activates pulse guiding.
	// - activates pulse giuding, sets pulse guiding end time and converts speed.
	// - - sets pulseGuide end time and sets pulse_guiding_ to true.
	//@params:
	// - direction: DIR_UP or DIR_DOWN
	// - duration: duration of the movement in ms.
	// - rate: rate of motion in degrees / second.
	//uint8_t pulseGuide(uint8_t direction, unsigned long duration, float rate);
	void pulseGuide(uint8_t direction, uint32_t duration, float rate);

	//@return: true if a pulse guiding command is in progress for this axis.
	bool isPulseGuiding();

	//@return: true if a slewing command is enabled for this axis.
	bool isSlewing();

	//@return: true if a moveAxis command is enabled for this axis.
	bool isMoveAxis();

	//@return: true if tracking is currently enabled (returns true if tracking is suspended due to 
	//slew or moveAxis command).
	bool isTracking();

	//necessary?
	//returns the step type the stepper is set to.
	//@return: 
	// - the currently set step type.
	//uint8_t getStepType();

	//sets the steptype of the stepper motor to the given value.
	//@param: steptype.
	//@return: true if changing step type was successful, false otherwise.
	bool setStepType(uint8_t step_type);

	//converts angular positions from steps to degrees.
	//@param: 
	// - the value to be converted.
	//@return: 
	// - the converted value.
	float stepsToDegrees(int32_t steps);

	//converts angular positions from degrees to steps.
	//@param: 
	// - the value to be converted .
	// - (optional) the step type
	//@return: 
	// - the converted value.
	int32_t degreesToSteps(float degrees);

	//@param: the tracking rate offset for this axis (ASCOM: RightAscensionRate / DeclinationRate)
	//in degrees / second.
	void setTrackingRateOffset(float rate);

	//@return: the currently set tracking rate offset for this axis (ASCOM: RightAscensionRate / DeclinationRate)
	//in degrees / second.
	float getTrackingRateOffset();

	//@param axis: telescope axis
	//@return the maximum speed in degrees / second for this axis
	float getMaxSpeedDeg();

	/*
	@return: the currently set moveAxis speed offset. */
	float getMoveAxisOffset();

	/*
	when a moveAxis command is active, sets the moveAxis speed offset to the provided
	value and updates the stepper motor speed.
	@param offset: speed offset in degrees / second.
	@return: true if the offset was correctly set, false otherwise (e.g. no moveAxis
	command was active)*/
	bool setMoveAxisOffset(float offset);

	//@return: endstop / home switch information for the given axis.
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - lower endstop switch triggered.
	//	| | | | | | - upper endstop switch triggered.
	//	| | | | | -	home switch triggered.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - not used.
	//	| - not used.
	//	- not used.
	uint8_t reportSwitches();

	//writes default values to the Arduino's EEPROM.
	//this function will be called when the eeprom control number check in the main setup function
	//fails or the user/driver sends a restore defaults command.
	static void writeDefaults(uint8_t axis);

private:
	uint8_t axis_;		//the axis (X / Y / Z). needed for EEPROM read / write.

	StepperMotor *stepper_motor_;

	//mount properties.
	float moveaxis_offset_;				//currently active moveaxis offset (in deg / second).
	float moveaxis_rate_;				//currently set moveAxis rate (in deg / second).
	float steps_per_degree_;			//number of stepper motor steps to move telescope 1 degree about the axis.
	float tracking_rate_;				//currently active tracking rate (in degrees / second) for the axis. sign determines direction (positive: CW, negative: CCW).
	float tracking_rate_offset_;		//currently active tracking rate offset (in degrees / second) for the axis.
	uint8_t step_type_fast_;			//the steptype for fast movements (slewing, moveAxis, homing, parking, ...)
	uint8_t step_type_slow_;			//the steptype for slow movements (tracking, pulseGuide) 

	//returns true if the distance to the target is 0 and the stepper is not moving any more.
	bool atDestination();

	//checks if the given target is outside the telescope's limits and adjusts the target if 
	//necessary.
	//param: the target destination to be checked.
	//@return: the (adjusted) target.
	float constrainToLimits(float target);

	//clears the home switch.
	// - checks the state of the home switch.
	// - when the home switch has been cleared, reverses the homing direction and enables
	//	 normal homing.
	void clearHome();

	//finds the home position. depending on the home switch, this happens using 2 different methods:
	//1) when the home switch is one of the endstop switches the home position is the position 
	//	 where the home switch was triggered for the first time.
	//2) when the home switch is a dedicated switch (no endstop) the home position is in 
	//	 between the position where the home switch was triggered the first time and where it was 
	//	 released the first time.
	// - when the home switch is an endstop switch and the home switch is pressed, sets  
	//	 home_found_ to true and sets the current position as the new target. 
	// - when the home switch is not an endstop switch and the home switch is pressed, saves the 
	//	 current position to home_first_pos_ and sets home_first_pos_found_ 
	//	 to true. 
	// - when the home switch is released and home_first_pos_found_ is true, calculates
	//	 the home position using the current location and the value stored in 
	//	 home_first_pos_ and sets home_found_ to true.
	//3) initiates a slew to the home position.
	void home();

	//sets the target of the stepper to the given value. 
	//@param: target in steps.
	void setNewTarget(int32_t target);

	//sets a (constant) speed. bypasses acceleration / deceleration.
	//used when setting a Tracking rate, MoveAxis speed or PulseGuide Rate.
	// - calls the AccelStepper wrapper's setSpeed() function.
	//@params: 
	// - desired speed in steps / second. 
	void setSpeed(float speed);

	//sets a (constant) speed. bypasses acceleration / deceleration.
	//used when setting a Tracking rate, MoveAxis speed or PulseGuide Rate. 
	// - converts speed to steps / second
	// - calls TelscopeAxis::setSpeed() function. 
	//@params:
	// - desired speed in degrees / second. 
	void setSpeedDeg(float speed);

	bool hard_stop_endstop_;				//if a hard stop should be executed when an endstop is hit.
	bool hard_stop_limit_;					//if a hard stop should be executed when a limit is exceeded.

	bool moving_;							//true if the stepper is currently moving.
	bool tracking_;							//true if the tracking is enabled.

	//variables for homing.
	bool homing_;							//if homing is in progress or not.
	bool home_found_;						//if the home position has beed found in the current homing process.	
	bool home_cleared_;						//if the home switch has been cleared in the current homing process.
	uint8_t homing_error_;

	uint32_t home_start_time_;				//system time of homing procedure start.
	uint32_t home_timeout_;					//home procedure timeout in ms.

	bool home_alignment_sync_;				//if the home alignment can be synced.
	//int16_t home_direction_;					//direction (positive / negative) to the home switch.

	//varaibles for homing with dedicated home switch.
	bool home_first_pos_found_;				//if the position of first home switch triggering has been found.
	int32_t home_first_pos_;					//position where the home switch has been triggered for the first time.

	//home switches.
	//bool home_switch_available_;			//true if home switch is available,
	bool endstop_home_switch_;				//true if the home switch is an endstop switch.

	Button *home_switch_;					//home switch.

	//----------------------------------------------------------------------------------------------
	////variables for endstop switches.
	Button *endstop_lower_;			//lower endstop switch.
	Button *endstop_upper_;			//upper endstop switch.

	//checks and returns the state of the endstop switches.
	// - when an endstop is hit:
	// - - sets bits of the return value depending on triggered endstop (see below).
	// - - when the stepper moves away from the endstop, does nothing.
	// - - when the stepper moves towards the endstop:
	// - - - when homing is active reverses stepper direction but does not stop the stepper. 
	//		 executes a hard stop if hard_stop_endstop_ is true.
	// - - when homing is not active:
	// - - - when the axis is slewing to a position away from the endstop (is currently 
	//		 decelerating), does nothing.
	//@return: byte indicating if (and which) endstop has been hit.
	//possible return values are:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - lower endstop hit.
	//	| | | | | | - upper endstop hit.
	//	| | | | | -	not used.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - not used.
	//	| - not used.
	//	- not used.
	uint8_t checkEndstops();
	uint8_t endstop_state_;		//last read state of the endstops.

	//checks and returns the state of the software movement limits.
	// - when a limit is triggered:
	// - - sets bits of the return value depending on endstop (see below).
	// - - when the stepper moves away from the limit, does nothing.
	// - - when the stepper moves towards the limit:
	// - - - when homing is active, does not stop but reverses stepper direction. 
	// - - when homing is not active:
	// - - - calls stop() to stop the stepper.
	//@return: byte indicating if (and which) limit was triggered.
	//possible return values are:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - lower limit triggered.
	//	| | | | | | - upper limit triggered.
	//	| | | | | -	not used.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - not used.
	//	| - not used.
	//	- not used.
	uint8_t checkLimits();
	uint8_t limit_state_;		//last checked state of the limits.

	//varialbes for movement limits.
	bool limit_lower_enabled_;			//if a lower limit is enabled.
	bool limit_upper_enabled_;			//if an upper limit is enabled.
	float limit_lower_deg_;				//lower movement limit in degrees.
	float limit_upper_deg_;				//upper movement limit in degrees.
	int32_t limit_lower_;					//lower movement limit.
	int32_t limit_upper_;					//upper movement limit.

	//limit_handling_t limit_handling_;
	uint8_t limit_handling_;

	//----------------------------------------------------------------------------------------------

	bool moveaxis_;					//true if moveAxis command is active.

	//checks if the steppers have reachted their target and if yes, ends tracking process.
	void slewing();

	bool slewing_;					//true if the telescope is currently slewing.

	//checks pulse guiding command. ends pulse guiding process when the time is up.
	// - when pulse guide duration has expired: 
	// - - calls stop(). 
	// - - resets stepper maximum speed.
	// - - disables pulseGuide command.
	// - if the pulseGuide command has been disabled, restores tracking by calling 
	//	 restoreTracking().
	void pulseGuiding();

	bool pulse_guiding_;					//true if a pulse guide command is in progress for the x axis.
	uint32_t pulse_guide_end_;			//end time (in milliseconds) of the pulse guide command.

	bool automatic_stepper_release_;	//true if automatic stepper release is enabled (power saving).

	//bool manual_control_;				//true if manual control is activated.

	float park_position_;				//park position (in degrees).

	float home_alignment_;				//home position alignment (in degrees).

	float target_;						//target position set using setTarget().

	//----------------------------------------------------------------------------------------------
	//templates for reading and writing to/from the EEPROM.
	template <class T> void readEEPROM(EEPROMHandler::telescope_axis_variable_::TelescopeAxisVariable_t_ variable, T& value)
	{
		readEEPROM(variable, axis_, value);
	};

	template <class T> void writeEEPROM(EEPROMHandler::telescope_axis_variable_::TelescopeAxisVariable_t_ variable, const T& value)
	{
		writeEEPROM(variable, axis_, value);
	};

	//templates for reading and writing to/from the EEPROM.
	template <class T> static void readEEPROM(EEPROMHandler::telescope_axis_variable_::TelescopeAxisVariable_t_ variable, uint8_t axis, T& value)
	{
		EEPROMHandler::readValue
		(
			EEPROMHandler::device_type_::TELESCOPE_AXIS,
			variable,
			axis,
			value
		);
	};

	template <class T> static void writeEEPROM(EEPROMHandler::telescope_axis_variable_::TelescopeAxisVariable_t_ variable, uint8_t axis, const T& value)
	{
		EEPROMHandler::writeValue
		(
			EEPROMHandler::device_type_::TELESCOPE_AXIS,
			variable,
			axis,
			value
		);
	};
};

#endif /* TELESCOPEAXIS_H_ */

