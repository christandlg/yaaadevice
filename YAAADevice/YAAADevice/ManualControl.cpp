//Manual Control class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "ManualControl.h"

ManualControl::ManualControl() :
	filter_wheel_mc_available_(false),
	btn_filter_wheel_mc_lock_(NULL),
	btn_filter_wheel_mc_next_(NULL), btn_filter_wheel_mc_prev_(NULL),

	focuser_mc_available_(false),
	btn_focuser_mc_lock_(NULL),
	btn_focuser_mc_in_(NULL), btn_focuser_mc_out_(NULL),
	focuser_locked_state_(false),

	telescope_mc_available_(false),
	btn_telescope_mc_lock_(NULL), btn_telescope_mc_speed_(NULL),
	jst_telescope_mc_(NULL),

	telescope_mc_speed_(0),
	telescope_mc_speed_x_(0), telescope_mc_speed_y_(0),
	telescope_locked_state_(false),

	keypad_(NULL),

	btn_infodisplay_up_(NULL), btn_infodisplay_down_(NULL),
	btn_infodisplay_left_(NULL), btn_infodisplay_right_(NULL),
	btn_infodisplay_plus_(NULL), btn_infodisplay_minus_(NULL)
{
}

ManualControl::~ManualControl()
{
}

void ManualControl::init()
{
	//todo input device settings
	//display_contrast_encoder_ = new ClickEncoder(52, 53, 50, 4);

	//keypad_ = new Keypad_TTP229B(11, 12, 250, false);

	//filter wheel manual control section
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::FILTER_WHEEL,
		EEPROMHandler::filter_wheel_variable_::MC_AVAILABLE,
		0,
		filter_wheel_mc_available_
	);

	if (filter_wheel_mc_available_)
	{
		//read if buttons are enabled
		bool btn_mc_lock_available, btn_mc_next_available, btn_mc_prev_available;

		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::FILTER_WHEEL,
			EEPROMHandler::filter_wheel_variable_::MC_BTN_LOCK_AVAILABLE,
			0,
			btn_mc_lock_available
		);

		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::FILTER_WHEEL,
			EEPROMHandler::filter_wheel_variable_::MC_BTN_NEXT_AVAILABLE,
			0,
			btn_mc_next_available
		);

		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::FILTER_WHEEL,
			EEPROMHandler::filter_wheel_variable_::MC_BTN_PREVIOUS_AVAILABLE,
			0,
			btn_mc_prev_available
		);

		//read pin numbers and create button instances
		if (btn_mc_lock_available)
		{
			uint8_t btn_mc_lock;

			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::FILTER_WHEEL,
				EEPROMHandler::filter_wheel_variable_::MC_BTN_LOCK,
				0,
				btn_mc_lock
			);

			btn_filter_wheel_mc_lock_ = new Button(btn_mc_lock);
		}

		if (btn_mc_next_available)
		{
			uint8_t btn_mc_next;

			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::FILTER_WHEEL,
				EEPROMHandler::filter_wheel_variable_::MC_BTN_NEXT,
				0,
				btn_mc_next
			);

			btn_filter_wheel_mc_next_ = new Button(btn_mc_next);
		}

		if (btn_mc_prev_available)
		{
			uint8_t btn_mc_prev;

			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::FILTER_WHEEL,
				EEPROMHandler::filter_wheel_variable_::MC_BTN_PREVIOUS,
				0,
				btn_mc_prev
			);

			btn_filter_wheel_mc_prev_ = new Button(btn_mc_prev);
		}
	}

	//-----------------------------------------------------------------------------------

	//focuser manual control section
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::FOCUSER,
		EEPROMHandler::focuser_variable_::MC_AVAILABLE,
		0,
		focuser_mc_available_
	);

	if (focuser_mc_available_)
	{
		bool mc_lock_available/*, mc_in_available, mc_out_available*/;

		//read if buttons are enabled
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::FOCUSER,
			EEPROMHandler::focuser_variable_::MC_BTN_LOCK_AVAILABLE,
			0,
			mc_lock_available
		);
		//EEPROMHandler::readValue(
		//	EEPROMHandler::device_type_::FOCUSER,
		//	EEPROMHandler::focuser_variable_::MC_BTN_IN_AVAILABLE,
		//	0,
		//	mc_in_available
		//	);
		//EEPROMHandler::readValue(
		//	EEPROMHandler::device_type_::FOCUSER,
		//	EEPROMHandler::focuser_variable_::MC_BTN_OUT_AVAILABLE,
		//	0,
		//	mc_out_available
		//	);

		if (mc_lock_available)
		{
			uint8_t btn_mc_lock;

			//read focuser enable manual control pin from EEPROM.
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::FOCUSER,
				EEPROMHandler::focuser_variable_::MC_BTN_LOCK,
				0,
				btn_mc_lock
			);

			btn_focuser_mc_lock_ = new Button(btn_mc_lock);
		}

		uint8_t btn_mc_in = 255, btn_mc_out = 255;

		//read focuser move inward pin from EEPROM.
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::FOCUSER,
			EEPROMHandler::focuser_variable_::MC_BTN_IN,
			0,
			btn_mc_in
		);

		btn_focuser_mc_in_ = new Button(btn_mc_in);

		//read focuser emove outward pin from EEPROM.
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::FOCUSER,
			EEPROMHandler::focuser_variable_::MC_BTN_OUT,
			0,
			btn_mc_out
		);

		btn_focuser_mc_out_ = new Button(btn_mc_out);
	}

	//-----------------------------------------------------------------------------------

	//telescope manual control section
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::TELESCOPE,
		EEPROMHandler::telescope_variable_::MC_AVAILABLE,
		0,
		telescope_mc_available_
	);

	if (telescope_mc_available_)
	{
		bool mc_lock_available_, mc_speed_available_, mc_joystick_available_;
		//read if buttons are enabled
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::TELESCOPE,
			EEPROMHandler::telescope_variable_::MC_BTN_TOGGLE_AVAILABLE,
			0,
			mc_lock_available_
		);
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::TELESCOPE,
			EEPROMHandler::telescope_variable_::MC_BTN_SPEED_AVAILABLE,
			0,
			mc_speed_available_
		);
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::TELESCOPE,
			EEPROMHandler::telescope_variable_::MC_JST_AVAILABLE,
			0,
			mc_joystick_available_
		);

		if (mc_lock_available_)
		{
			uint8_t btn_mc_lock;

			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::TELESCOPE,
				EEPROMHandler::telescope_variable_::MC_BTN_TOGGLE,
				0,
				btn_mc_lock
			);

			btn_telescope_mc_lock_ = new Button(btn_mc_lock);
		}

		if (mc_speed_available_)
		{
			uint8_t btn_mc_speed;

			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::TELESCOPE,
				EEPROMHandler::telescope_variable_::MC_BTN_SPEED,
				0,
				btn_mc_speed
			);

			btn_telescope_mc_speed_ = new Button(btn_mc_speed);
		}

		if (mc_joystick_available_)
		{
			uint8_t jst_mc_x, jst_mc_y, jst_mc_deadzone;

			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::TELESCOPE,
				EEPROMHandler::telescope_variable_::MC_JST_INPUT_X,
				0,
				jst_mc_x
			);
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::TELESCOPE,
				EEPROMHandler::telescope_variable_::MC_JST_INPUT_Y,
				0,
				jst_mc_y
			);
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::TELESCOPE,
				EEPROMHandler::telescope_variable_::MC_JST_DEADZONE,
				0,
				jst_mc_deadzone
			);

			jst_telescope_mc_ = new Joystick(jst_mc_x, jst_mc_y, jst_mc_deadzone);
		}
	}

	//initialize info display inputs
	if (info_display_.available())
	{
		//read info display input device settings from EEPROM
		YAAAKeypad::KeypadSettings keypad_settings;
	
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::INPUT_INFODISPLAY_KEYPAD,
			0,
			keypad_settings
		);
	
		//initialize keypad
		switch (keypad_settings.type_)
		{
			case YAAAKeypad::TYPE_MATRIX:
				keypad_ = new YAAAKeypad_Matrix(keypad_settings);
				break;
			case YAAAKeypad::TYPE_TWI:
				keypad_ = new YAAAKeypad_TTP229B(keypad_settings);
				break;
			case YAAAKeypad::TYPE_GENERIC:
				keypad_ = new YAAAKeypad_Generic(keypad_settings);
				break;
			default:
				keypad_ = NULL;
		}
	}
}

void ManualControl::run()
{
	//if (telescope_available && telescope_mc_available_)
	telescopeManualControl();

	//if (filter_wheel_available && filter_wheel_mc_available_)
	filterWheelManualControl();

	focuserManualControl();

	infoDisplayManualControl();
}

void ManualControl::infoDisplayManualControl()
{
	if (!info_display_.available())
		return;

	if (!keypad_)
		return;

	if (!keypad_->isReadDue())
		return;

	info_display_.input(keypad_->getKey());
}

void ManualControl::filterWheelManualControl()
{
	//do nothing if the filter wheel is not enabled.
	if (!filter_wheel_)
		return;

	//do nothing if manual control is not enabled for the telescope
	if (!filter_wheel_mc_available_)
		return;

	//lock / unlock device for local operation.
	//if device is locked for remote operation, it may be force locked for local
	//operation by double pressing the lock button.
	if (btn_filter_wheel_mc_lock_)
	{
		switch (btn_filter_wheel_mc_lock_->getEvent())
		{
			case Button::EVENT_PRESS:		//normal press switches enables / disabled manual control.
				switch (filter_wheel_->getLocked())
				{
					case Focuser::LOCK_NONE:
						filter_wheel_->setLocked(FilterWheel::LOCK_LOCAL);
						break;
					case Focuser::LOCK_LOCAL:
						filter_wheel_->setLocked(FilterWheel::LOCK_NONE);
						break;
					default:
						return;
				}
				break;
			case Button::EVENT_HOLD_LONG:		//long press forces change from remote to local lock.
				if (filter_wheel_->getLocked() == FilterWheel::LOCK_REMOTE)
					filter_wheel_->setLocked(FilterWheel::LOCK_LOCAL);
				break;
			default:
				break;
		}
	}

	//do nothing if remote lock is enabled.
	if (filter_wheel_->getLocked() == FilterWheel::LOCK_REMOTE)
		return;

	//handle "move to next filter" button press.
	if (btn_filter_wheel_mc_next_)
	{
		if (btn_filter_wheel_mc_next_->getEvent() == Button::EVENT_PRESS)
			filter_wheel_->moveNextFilter();
	}

	//handle "move to previous filter" button press.
	if (btn_filter_wheel_mc_prev_)
	{
		if (btn_filter_wheel_mc_prev_->getEvent() == Button::EVENT_PRESS)
			filter_wheel_->movePrevFilter();
	}
}

void ManualControl::focuserManualControl()
{
	//do nothing if the focuser is not enabled.
	if (!focuser_)
		return;

	//do nothing if manual control is not enabled for the focuser.
	if (!focuser_mc_available_)
		return;

	//lock / unlock device for local operation.
	//if device is locked for remote operation, it may be force locked for local
	//operation by double pressing the lock button.
	if (btn_focuser_mc_lock_)
	{
		switch (btn_focuser_mc_lock_->getEvent())
		{
			case Button::EVENT_PRESS:		//normal press switches enables / disables manual control.
				switch (focuser_->getLocked())
				{
					case Focuser::LOCK_NONE:
						focuser_->setLocked(Focuser::LOCK_LOCAL);
						break;
					case Focuser::LOCK_LOCAL:
						focuser_->setLocked(Focuser::LOCK_NONE);
						break;
					default:
						return;
				}
				break;
			case Button::EVENT_HOLD_LONG:		//long press forces change from remote to local lock.
				if (focuser_->getLocked() == Focuser::LOCK_REMOTE)
					focuser_->setLocked(Focuser::LOCK_LOCAL);
				break;
			default:
				break;
		}
	}

	//do nothing if the focuser is locked for remote operation.
	if (focuser_->getLocked() == Focuser::LOCK_REMOTE)
		return;

	//handle "move in" button press.
	if (btn_focuser_mc_in_)
	{
		switch (btn_focuser_mc_in_->getEvent())
		{
			case Button::EVENT_PRESS:
				focuser_locked_state_ = focuser_->getLocked();	//remember locked state.
				focuser_->moveDirection(true);
				focuser_->setLocked(Focuser::LOCK_LOCAL);		//lock focuser while moving.
				break;
			case Button::EVENT_RELEASE:
				focuser_->halt();
				focuser_->setLocked(focuser_locked_state_);		//reset locked state.
				break;
			default:
				break;
		}

		return;
	}

	//handle "move out" button press.
	if (btn_focuser_mc_out_)
	{
		switch (btn_focuser_mc_out_->getEvent())
		{
			case Button::EVENT_PRESS:
				focuser_locked_state_ = focuser_->getLocked();	//remember locked state.
				focuser_->moveDirection(false);
				focuser_->setLocked(Focuser::LOCK_LOCAL);		//lock focuser while moving.
				return;
			case Button::EVENT_RELEASE:
				focuser_->halt();
				focuser_->setLocked(focuser_locked_state_);		//reset locked state.
				return;
			default:
				break;
		}

		return;
	}
}

void ManualControl::telescopeManualControl()
{
	//do nothing if the telescope is not enabled.
	if (!telescope_)
		return;

	//do nothing if manual control is not available for the telescope
	if (!telescope_mc_available_)
		return;

	//toggle manual control on / off
	if (btn_telescope_mc_lock_)
	{
		switch (btn_telescope_mc_lock_->getEvent())
		{
			case Button::EVENT_PRESS:
				switch (telescope_->getLocked())
				{
					case Telescope::LOCK_NONE:
						telescope_->setLocked(Telescope::LOCK_LOCAL);

						telescope_mc_speed_ = 0;
						telescope_mc_speed_x_ = 0;
						telescope_mc_speed_y_ = 0;
						break;
					case Telescope::LOCK_LOCAL:
						telescope_->setLocked(Telescope::LOCK_NONE);

						//disable moveaxis command
						if (telescope_->isMoveAxis())
						{
							telescope_->moveAxis(TelescopeAxis::X_AXIS, 0.0f);
							telescope_->moveAxis(TelescopeAxis::Y_AXIS, 0.0f);
						}
						break;
					default:
						break;
				}
				break;
			case Button::EVENT_HOLD_LONG:
				if (telescope_->getLocked() == Telescope::LOCK_REMOTE)
				{
					telescope_->setLocked(Telescope::LOCK_LOCAL);

					telescope_mc_speed_ = 0;
					telescope_mc_speed_x_ = 0;
					telescope_mc_speed_y_ = 0;
				}
				break;
			default:
				break;
		}
	}

	//do nothing if remote lock is enabled.
	if (telescope_->getLocked() == Telescope::LOCK_REMOTE)
		return;

	//do nothing if the telescope is currently parked or homing.
	//if (telescope_->atPark() || telescope_->isHoming())
	if (telescope_->isHoming())
		return;

	//exit function if telescope is slewing or pulse guiding.
	if (telescope_->isSlewing() || telescope_->isPulseGuiding())
		return;

	//change speed setting
	if (btn_telescope_mc_speed_)
	{
		if (btn_telescope_mc_speed_->getEvent() == Button::EVENT_PRESS)
		{
			telescope_mc_speed_++;

			if (telescope_mc_speed_ > 3)
				telescope_mc_speed_ = 0;
		}
	}

	//todo: test

	if (jst_telescope_mc_) 
	{
		//read the joystick input.
		int16_t speed_x = 0, speed_y = 0;
		jst_telescope_mc_->read(speed_x, speed_y);

		if (speed_x != telescope_mc_speed_x_)
		{
			telescope_mc_speed_x_ = speed_x;

			//normalize to (-1, 1)
			float rate_x = static_cast<float>(speed_x) / 512.0;

			//when tracking is enabled set rate as offset from current tracking rate.
			if (telescope_->isTracking())
			{
				rate_x *= pow(10.0, static_cast<float>(telescope_mc_speed_));

				telescope_->setRightAscensionRate(rate_x);
			}

			//when telescope is not moving or a moveAxis command is active
			else
			{
				//unpark the telescope if necessary.
				if (telescope_->atPark())
					telescope_->unpark();

				rate_x *= telescope_->getMaxSpeedDeg(TelescopeAxis::X_AXIS);
				rate_x *= pow(10.0, static_cast<float>(telescope_mc_speed_) - 3.0f);

				//when a moveAxis command is active, set moveAxis offset speed.
				if (telescope_->isMoveAxis(TelescopeAxis::X_AXIS))
					telescope_->setMoveAxisOffset(TelescopeAxis::X_AXIS, rate_x);
				else
					telescope_->moveAxis(TelescopeAxis::X_AXIS, rate_x);
			}
		}

		if (speed_y != telescope_mc_speed_y_)
		{
			telescope_mc_speed_y_ = speed_y;

			//normalize to (-1, 1)
			float rate_y = static_cast<float>(speed_y) / 512.0;

			//when tracking is enabled set rate as offset from current tracking rate.
			if (telescope_->isTracking())
			{
				rate_y *= pow(10.0, static_cast<float>(telescope_mc_speed_));

				telescope_->setDeclinationRate(rate_y);
			}

			//when telescope is not moving or a moveAxis command is active
			else
			{
				//unpark the telescope if necessary.
				if (telescope_->atPark())
					telescope_->unpark();

				rate_y *= telescope_->getMaxSpeedDeg(TelescopeAxis::Y_AXIS);
				rate_y *= pow(10.0, static_cast<float>(telescope_mc_speed_) - 3.0f);

				//when a moveAxis command is active, set moveAxis offset speed.
				if (telescope_->isMoveAxis(TelescopeAxis::Y_AXIS))
					telescope_->setMoveAxisOffset(TelescopeAxis::Y_AXIS, rate_y);
				else
					telescope_->moveAxis(TelescopeAxis::Y_AXIS, rate_y);
			}
		}	
	}
}

void ManualControl::writeDefaults()
{
	//Filter Wheel Section


	//Telescope section

	//InfoDisplay Section
	YAAAKeypad::KeypadSettings keypad_settings = {
		DEF_G_KEYPAD_TYPE,
		DEF_G_KEYPAD_ACTIVE_HIGH,
		DEF_G_KEYPAD_PARAMETERS,
		DEF_G_KEYPAD_READ_INTERVAL
	};

	writeEEPROM(EEPROMHandler::general_variable_::INPUT_INFODISPLAY_KEYPAD, keypad_settings);
}

ManualControl manual_control_;

