//Focuser class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef FOCUSER_H_
#define FOCUSER_H_

#include <Arduino.h>

#include <limits.h>

#include "defaults.h"

#include "EEPROMHandler.h"
#include "Button.h"

#include "YAAASensor.h"
#include "SensorTemperature.h"
#include "SensorTemperatureOneWire.h"
#include "SensorTemperatureAnalogIC.h"
#include "SensorTemperatureThermistor.h"
#include "SensorTemperatureMLX90614.h"

//#include "StepperMotor.h"
#include "StepperMotorDriver.h"
#include "StepperMotorAFMSV2.h"
#include "StepperMotorFourWire.h"

#define TEMP_SENSORS 3

class Focuser
{
public:
	enum error_code_t //todo: add more error codes & change return value of most functions to uint16_t?
	{
		ERROR_NONE = 0,
		ERROR_LIMIT_LOWER = 1,
		ERROR_LIMIT_UPPER = 2,
		ERROR_MAX_INCREMENT = 4,
		ERROR_TEMP_COMP = 8,	//temperature compensation currently active. todo: remove?
		ERROR_ABSOLUTE = 16,
		ERROR_HOMING = 32,
		ERROR_NOT_MOVING = 64,	//todo: remove?
		ERROR_OTHER = 128		//todo: exchange for something meaningful.
	};

	enum error_code_homing_t : uint8_t
	{
		ERROR_HOMING_NONE = 0,						//no error occurred.
		ERROR_HOMING_LIMIT_LOWER = 8,				//homing position beyond lower movement limit
		ERROR_HOMING_LIMIT_UPPER = 16,				//homing position beyond upper movement limit
		ERROR_HOMING_SWITCH_NOT_AVAILABLE = 32,		//no home switch available.
		//ERROR_HOMING_SWITCH_NOT_TRIGGERED = 64,	//homing switch has not been triggered, but both limits or endstops have been hit.	//TODO implement
		ERROR_HOMING_TIMEOUT = 128					//homing operation timed out.
	};

	enum lock_source_t
	{
		LOCK_NONE = 0,				//device currently not locked.
		LOCK_LOCAL = 1,				//device currently locally locked
		LOCK_REMOTE = 2				//device currently remote locked.
	};

	enum speed_t
	{
		SPEED_SLOWEST = 1,		//1 micron / second
		SPEED_SLOW = 2,			//10 microns / second
		SPEED_FAST = 3,			//100 microns / second
		SPEED_FASTEST = 4		//1000 microns / second.
	};

	enum temp_comp_state_t
	{
		TC_DISABLED = 0,		//temperature compensation disabled.
		TC_INITIALIZE = 1,		//temperature compensation is being initialized.
		TC_READY = 2,			//temperature compensation is waiting for next scheduled temperature measurement.
		TC_MEASURE = 3,			//temperature measurement currently in progress.
		TC_WAIT_MOVEMENT = 4	//temperature compensation is waiting for focuser movement to stop.
	};

	Focuser();

	~Focuser();

	/*checks limits and endstops, moves the stepper*/
	void run();

	/*@return: true if the focuser is operating in absolute positioning mode.*/
	bool getAbsolute();

	/*@return: the current focuser position in steps.*/
	int32_t getPosition();

	/*
	sets the focusers position to the provided value.
	@param position: new position in steps.
	@return: bits are set on errors:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - lower limit exceeded.
	//	| | | | | | - upper limit exceeded.
	//	| | | | | -	not used.
	//	| | | | - not used.
	//	| | | -	focuser not in absolute mode.
	//	| | - focuser is currently homing.
	//	| - not used.
	//	- not used.	*/
	uint8_t setPosition(int32_t position);

	/*sets the focusers movement speed to the given value.
	@param speed: desired focuser movement speed in steps / second.
	@return: true if setting the speed was successful, false otherwise.*/
	//void setSpeed(float speed);

	/*stops any focuser movement started by a move command. does not stop movements 
	due to activated temperature compensation. */
	uint8_t halt();

	/*sets the focusers target position. depending on the value of absolute_, target
	is either an absolute target or relative to the current focuser position.
	does not stop temperature compensation.
	@param target: desired focuser target in steps - relative or absolute
	@return: bits are set on errors:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - lower limit exceeded.
	//	| | | | | | - upper limit exceeded.
	//	| | | | | -	not used.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - Focuer is currently homing.
	//	| - not used.
	//	- not used.	*/
	uint8_t move(int32_t target);

	/*
	moves the focuser inwards (true) or outwards (false) by setting the target to 
	max_step_ (inwards) or 0 (outwards). does not stop temperature compensation.
	@param direction: true - inward; false - outward.
	@return bits are set on errors:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	not used.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - focuser is currently homing.
	//	| - not used.
	//	- not used.	*/
	uint8_t moveDirection(bool direction);

	/*move the focuser at the currently set speed for the given time by calculating a
	target position that will be reached after the given time when moving at the current
	speed.
	@param time: movement duration in milliseconds.
	@return: bits are set on errors:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - not used.
	//	| | | | | | - not used.
	//	| | | | | -	not used.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - focuser is currently homing.
	//	| - not used.
	//	- not used.	*/
	uint8_t moveTime(int32_t time);

	/*
	@return: true if the Focuser can find its home position.*/
	bool canFindhome();

	/*
	initiates homing. if called during a homing operation, homing is restarted.
	@return: true on success, false otherwise. on false, call getHomingError to get more information 
	on why it failed. */
	bool findHome();

	/*
	@return: true if the focuser is currently homing, false otherwise.*/
	bool isHoming();

	/*
	@return: error codes as error_code_homing_t generated during the last find home operation.*/
	uint8_t getHomingError();

	/*@return: true if the focuser is currently moving, false otherwise.*/
	bool isMoving();

	/*returns the temperature measured by the given sensor.
	@param sensor: sensor number to use (0 - n-1)
	@return: temperature in the requested temperature unit or 999.0 to indicate a 
	failed measurement.*/
	float getTemperature(uint8_t sensor);

	/*checks if a temperature measurement is complete.
	@param sensor: sensor number to use (0 - n-1)
	@return: true if the temperature mesurement is complete, false otherwise.*/
	bool getTemperatureComplete(uint8_t sensor);

	/*initiates a temperature measurement.
	@param sensor: sensor number to use (0 - n-1)
	@return: true if the measurement was started successfully, false otherwise.*/
	bool measureTemperature(uint8_t sensor);

	/*returns true if temperature compensation is enabled
	@return: true if temperature compensation is active based on the given temperature
	sensors readings, false otherwise.*/
	bool getTempCompensation();

	/*
	@param enable: enable or disable temperature compensation.
	@param sensor: temperature sensor to use. ignored when enable is false.
	@return: true if enabling temperature compensation was successful, false otherwise.*/
	bool setTempCompensation(bool enable);

	/*
	@return: currently set temperature sensor for temperature compensation. */
	uint8_t getTempCompSensor();

	/*
	@param sensor: temperature sensor to use for temperature compensation.
	@return: true if a valid sensor number was given, false otherwise.*/
	bool setTempCompSensor(uint8_t sensor);

	/*
	@return: the focusers step size in microns.*/
	float getStepSize();

	/*
	returns the state of the endstop and home switches.*/
	uint8_t reportSwitches();

	/*returns lock source (none, local, remote).*/
	uint8_t getLocked();

	/*sets lock source (none, local, remote).*/
	void setLocked(uint8_t source);

	/*
	sets the stepper motor speed to toe given value.
	@param speed: speed as speed_t.*/
	void setSpeed(Focuser::speed_t speed);

	/*
	writes (safe) default values to the Arduino's EEPROM.*/
	static void writeDefaults();

private:
	/*
	converts distance or positino from steps to microns.*/
	float stepsToMicrons(int32_t steps);

	/*
	converts distance from microns to steps. */
	int32_t micronsToSteps(float microns);

	/*
	checks the focuser's software movement limits. stops focuser movement when limits
	are violated.*/
	uint8_t checkLimits();

	/*
	checks the focuser's hardware limit switches. stops focuser movement when endstop
	switches are triggered. */
	uint8_t checkEndstops();
	
	/*
	@param: sensor number to check (0 - TEMP_SENSORS-1)
	@return: true if the number given is valid and the temperature sensor has been 
	initialized.*/
	bool checkTempSensor(uint8_t sensor);

	/*
	clears the home switch during homing.	*/
	void clearHome();

	/*
	checks the focuser's home switch to find the home position.	*/
	void home();

	/*
	sets a new stepper target and sets the stepper speed.*/
	void moveTo(int32_t target);

	/*
	returns a speed based on the maximum speed setting stored in EEPROM and the 
	given speed setting. 
	@return: speed in steps / second. returned speed ranges from MAX_SPEED to 
	MAX_SPEED / 1000.*/
	float getSpeedFromEnum(Focuser::speed_t speed);

	/*
	this function handles temperature compensation of the focuser.
	state description:
	TC_DISABLED: temperature compensation is disabled, the function returns immediately.
	TC_INITIALIZE: after temperature compensation has been started or resumed the 
	temperature compensation remains in this state until the initial temperature 
	measurement is finished. once the initial temperature measurement is finished, 
	the next temperature measurement is scheduled and state TC_READY is activated.
	TC_READY: temperature compensation remains in this state until the next 
	temperature measurement is due. when the temperature measurement time interval has 
	passed a temperature measurement is initiated and the state TC_MEASURE is activated.
	TC_MEASURE: temperature compensation remains in this state until the temperature 
	measurement is finished. once the measurement is fnished and the temperature has 
	changed a new focuser position is calculated. a new measurement is scheduled and 
	state TC_READY is activated.
	TC_WAIT_MOVEMENT: while the temperature sensor is moving (following a call to 
	move(), moveDirection() or moveTime()) temperature compensation is suspended,
	after the movement has finished temperature compensation is reactivated and 
	state TC_INITIALIZE is activated.*/
	void temperatureCompensation();

	StepperMotor *stepper_motor_;	

	float speed_;				//speed the stepper will move at.

	uint16_t number_of_steps_;		//number of steps ofthe stepper motor.

	float gear_ratio_;				//gear ratio motor revolutions : knob revolutions

	bool automatic_stepper_release_;	//true if steppers are automatically released when not moving.

	float microns_per_rev_;			//number of microns per knob revolution.

	float step_size_;				//size of a step in microns.

	uint8_t step_type_;				//step type.

	bool limit_lower_enabled_;		
	bool limit_upper_enabled_;		
	int32_t limit_lower_;			//lower limit of movement range. should be 0
	int32_t limit_upper_;			//upper limit of movement range. equal to ASCOM Max Step.

	bool endstop_home_switch_;		//true if the home switch is an endstop switch.

	Button* home_switch_;
	Button* endstop_lower_;
	Button* endstop_upper_;

	uint8_t limit_state_;			//state of the limits.

	uint8_t endstop_state_;			//state of the endstops.


	bool homing_;					//true if homing is currently in progress.

	bool home_cleared_;				//true if the home position is cleared during a homing operation.

	bool home_found_;				//true if the home position was found.

	bool home_first_pos_found_;		//true if the home switch was triggered.

	int32_t home_first_pos_;		//position where the home switch was triggered the first time.

	int32_t home_position_;			//home position in steps.

	uint8_t homing_error_;

	uint32_t home_start_time_;
	uint32_t home_timeout_;


	bool absolute_;					//true if the focuser is in absolute mode, false otherwise.

	int32_t max_step_;

	int32_t max_increment_;			//maximum increment of focuser position (in steps).

	uint8_t temp_comp_sensor_;		//sensor number to use for temperature compensation.

	uint32_t temp_comp_interval_;	//interval between temperature measurements.

	uint32_t temp_comp_last_;		//time of last temperature measurement.

	float temp_comp_factor_;		//temperature compensation factor in microns / degree kelvin.

	uint32_t temp_comp_ref_pos_;	//start position of temperature compensation.

	float temp_comp_ref_temp_;		//temperature at which the temperature compensation was started.

	temp_comp_state_t temp_comp_state_;		//state of temperature compensation

	SensorTemperature *temperature_sensors_[TEMP_SENSORS];

	uint8_t locked_;

	//----------------------------------------------------------------------------------------------
	//templates for reading and writing to/from the EEPROM.
	template <class T> static void readEEPROM(EEPROMHandler::focuser_variable_::FocuserVariable_t_ variable, T& value, uint8_t index = 0)
	{
		EEPROMHandler::readValue
		(
			EEPROMHandler::device_type_::FOCUSER,
			variable,
			index,
			value
		);
	};

	template <class T> static void writeEEPROM(EEPROMHandler::focuser_variable_::FocuserVariable_t_ variable, const T& value, uint8_t index = 0)
	{
		EEPROMHandler::writeValue
		(
			EEPROMHandler::device_type_::FOCUSER,
			variable,
			index,
			value
		);
	};
};

#endif /*FOCUSER_H_*/

