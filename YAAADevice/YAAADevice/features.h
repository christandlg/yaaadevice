//features.h file
//this file contains constants definitions.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#ifndef FEATURES_H_
#define FEATURES_H_

//#define FEATURE_LX200

#if !defined(ESP8266) && !defined(ESP32)
#define FEATURE_ETHERNET		//commenting this line removes ethernet support. 
#endif

#if defined(ESP8266) || defined(ESP32)
#define FEATURE_WIFI			//commenting this line removes wifi support. 
#endif /* defined(ESP8266) || defined(ESP32) */

//#define FEATURE_MQTT			//commenting this line removes MQTT support. 

//#define FEATURE_LoRa			//commenting this lien removes LoRa support. 

#define FEATURE_DISPLAY_U8G2LIB	//commenting this line removes U8G2Lib display support. 

#endif /* FEATURES_H_ */