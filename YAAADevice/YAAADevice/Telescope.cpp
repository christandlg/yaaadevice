//Telescope class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Telescope.h"

Telescope::Telescope() :

	mount_type_(Telescope::ALT_AZ),

	x_axis_(NULL), y_axis_(NULL), z_axis_(NULL),

	homing_error_(Telescope::ERROR_NONE),

	park_enabled_(false), parked_(false),
	startup_auto_home_(false),

	tracking_rate_(Telescope::TR_SIDEREAL),

	locked_(Telescope::LOCK_NONE)
	 
	//temperature_sensors_(NULL)
{
	//read information about the Telescope from EEPROM:
	readEEPROM(EEPROMHandler::telescope_variable_::MOUNT_TYPE, mount_type_);
	if (mount_type_ > 2)
		mount_type_ = Telescope::ALT_AZ;

	//read if the telescope can be parked / if a park position is available for the telescope.
	readEEPROM(EEPROMHandler::telescope_variable_::PARK_ENABLED, park_enabled_);

	//read if automatic homing on startup is enabled for the telescope.
	readEEPROM(EEPROMHandler::telescope_variable_::STARTUP_AUTO_HOME, startup_auto_home_);

	//initialize TelescopeAxis instances:	
	x_axis_ = new TelescopeAxis(TelescopeAxis::X_AXIS);
	y_axis_ = new TelescopeAxis(TelescopeAxis::Y_AXIS);
	z_axis_ = new TelescopeAxis(TelescopeAxis::Z_AXIS);

	//set tracking rate to 15 arcseconds / second (sidereal tracking rate) when the telescope
	//mount is not an Alt/Az mount.
	if (mount_type_ == Telescope::EQUATORIAL || mount_type_ == Telescope::GERMAN_EQUATORIAL)
		setTrackingRate(tracking_rate_);

	//temperature sensors 
	//-----------------------------------------------------------------------------------
	for (uint8_t i = 0; i < TEMP_SENSORS; i++)
	{
		temperature_sensors_[i] = NULL;
	
		YAAASensor::SensorSettings sensor_settings;
	
		//read temperature sensor settings from EEPROM.
		EEPROMHandler::readValue(
			EEPROMHandler::device_type_::TELESCOPE,
			EEPROMHandler::telescope_variable_::TEMPERATURE_SENSOR,
			i,
			sensor_settings);
	
		//skip sensor creation if sensor is not a temperature sensor
		if (sensor_settings.sensor_type_ != YAAASensor::SENSOR_TEMPERATURE)
			continue;
	
		//select correct SensorTemperature object to create.
		switch (sensor_settings.sensor_device_)
		{
			case SensorTemperature::DEVICE_ANALOG_IC:
				temperature_sensors_[i] = new SensorTemperatureAnalogIC(sensor_settings);
				break;
			case SensorTemperature::DEVICE_THERMISTOR:
				temperature_sensors_[i] = new SensorTemperatureThermistor(sensor_settings);
				break;
			case SensorTemperature::DEVICE_ONEWIRE_DS18:
				temperature_sensors_[i] = new SensorTemperatureOneWire(sensor_settings);
				break;
			case SensorTemperature::DEVICE_MLX90614:
				temperature_sensors_[i] = new SensorTemperatureMLX90614(sensor_settings);
				break;
			default:
				break;
		}
	
		//attempt to initialize the temperature sensor. if it fails, delete the sensor.
		if (temperature_sensors_[i])
		{
			if (!temperature_sensors_[i]->begin())
			{
				delete temperature_sensors_[i];
				temperature_sensors_[i] = NULL;
			}
		}
	}

	//if startup_auto_home_ is true, activate homing process.
	if (startup_auto_home_)
		findHome();
}

Telescope::~Telescope()
{
	delete x_axis_;
	delete y_axis_;
	delete z_axis_;

	for (uint8_t i = 0; i < TEMP_SENSORS; i++)
		if (temperature_sensors_[i])
			delete temperature_sensors_[i];
}

void Telescope::run()
{
	//move the telescope.
	x_axis_->run();
	y_axis_->run();
	z_axis_->run();
}

bool Telescope::withinLimits(uint8_t axis, float target)
{
	if (TelescopeAxis* curr_axis = getAxis(axis))
		return curr_axis->withinLimits(target);

	return false;
}

String Telescope::degreesToDMS(float angle, bool precision, bool long_degree)
{
	long degrees = 0;
	long minutes = 0;
	long seconds = 0;

	String dms = "";

	//add sign to DMS string.
	if (angle >= 0.0f)
		dms = "+";
	else
		dms = "-";

	//make angle positive
	angle = abs(angle);

	//extract integer part of angle
	degrees = static_cast<long>(angle);

	//extract fractal part of angle
	float fractal = angle - static_cast<float>(degrees);

	//compute minutes from fractal part
	minutes = static_cast<long>(fractal * 60.0f);

	//compute seconds from fractal part
	if (precision)
	{
		fractal = fractal * 60.0f - static_cast<float>(minutes);
		seconds = static_cast<long>(fractal * 60.0f);
	}

	//add degrees to DMS string
	if (long_degree && degrees < 100)
		dms += "0";

	if (degrees < 10)
		dms += "0";

	dms += String(degrees);

	dms += "*";

	//add minutes to DMS string
	if (minutes < 10)
		dms += "0";

	dms += String(minutes);

	//add seconds to DMS string
	if (precision)
	{
		dms += "\'";

		if (seconds < 10)
			dms += "0";

		dms += String(seconds);
	}

	return dms;
}

String Telescope::degreesToHMS(float angle, bool precision)
{
	long degrees = 0;
	long minutes = 0;
	long seconds = 0;

	String hms = "";

	//add sign to HMS string.
	if (angle >= 0.0f)
		hms = "+";
	else
		hms = "-";

	//make angle positive
	angle = abs(angle);

	//extract integer part of angle
	degrees = static_cast<long>(angle);

	//extract fractal part of angle
	float fractal = angle - static_cast<float>(degrees);

	//compute minutes from fractal part
	minutes = static_cast<long>(fractal * 60.0f);

	//compute seconds from fractal part
	fractal = fractal * 60.0f - static_cast<float>(minutes);

	if (precision)
		seconds = static_cast<long>(fractal * 60.0f);
	else
		seconds = static_cast<long>(fractal * 10.0f);

	//add degrees to HMS string
	if (degrees < 10)
		hms += "0";

	hms += String(degrees);

	hms += '*';

	//add minutes to HMS string
	if (minutes < 10)
		hms += "0";

	hms += String(minutes);

	//add seconds to HMS string
	if (precision)
	{
		hms += "\'";

		if (seconds < 10)
			hms += "0";
	}
	else
		hms += '.';

	hms += String(seconds);

	return hms;
}

float Telescope::dmsToDegrees(String DMS, bool precision)
{
	float dms_deg = 0.0f;
	float dms_min = 0.0f;
	float dms_sec = 0.0f;

	dms_deg = DMS.substring(0, 3).toFloat();
	dms_min = DMS.substring(4, 6).toFloat();

	if (precision)
		dms_sec = DMS.substring(7, 9).toFloat();

	return dms_deg + dms_min / 60.0f + dms_sec / 3600.0f;
}

float Telescope::hmsToDegrees(String HMS, bool precision)
{
	float hms_deg = 0.0f;
	float hms_min = 0.0f;
	float hms_sec = 0.0f;

	hms_deg = HMS.substring(0, 2).toFloat() * 15.0f;
	hms_min = HMS.substring(3, 5).toFloat();

	if (precision)
		hms_sec = HMS.substring(6, 8).toFloat();			//high precision: last part of HMS is seconds
	else
		hms_sec = HMS.substring(6, 7).toFloat() * 6.0f;		//low precision: last part of HMS is fraction of minutes, convert to seconds.

	return hms_deg + hms_min / 60.0f + hms_sec / 3600.0f;
}

TelescopeAxis *Telescope::getAxis(uint8_t axis)
{
	if (axis == TelescopeAxis::X_AXIS) return x_axis_;
	if (axis == TelescopeAxis::Y_AXIS) return y_axis_;
	if (axis == TelescopeAxis::Z_AXIS) return z_axis_;

	//return NULL when an invalid axis has been requested.
	return NULL;
}

float Telescope::getCurrentTarget(uint8_t axis)
{
	float target = NAN;

	if (TelescopeAxis* current_axis = getAxis(axis))
	{
		target = current_axis->getCurrentTarget();

		if (axis == TelescopeAxis::X_AXIS)
		{
			//when the telescope mount is (german) equatorial, convert from degrees to hour angle.
			if (mount_type_ == Telescope::EQUATORIAL || mount_type_ == Telescope::GERMAN_EQUATORIAL) target /= 15.0;
		}
	}

	return target;
}

float Telescope::getSiteLatitude()
{
	float latitude = 0.0f;

	readEEPROM(EEPROMHandler::telescope_variable_::SITE_LATITUDE, latitude);

	return latitude;
}

float Telescope::getSiteLongitude()
{
	float longitude = 0.0f;

	readEEPROM(EEPROMHandler::telescope_variable_::SITE_LONGITUDE, longitude);

	return longitude;
}

float Telescope::getTarget(uint8_t axis)
{
	if (TelescopeAxis *currentAxis = getAxis(axis))
		return currentAxis->getTarget();

	return 0.0f;
}

bool Telescope::setTarget(uint8_t axis, float target)
{
	if (TelescopeAxis *currentAxis = getAxis(axis))
		return currentAxis->setTarget(target);

	//if (!currentAxis->withinLimits(target))
	//	return false;

	//return false if an invalid axis was given.
	return false;
}

uint8_t Telescope::getMountType()
{
	return mount_type_;
}

uint8_t Telescope::move(uint8_t axis, float target)
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//check if the telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	TelescopeAxis* current_axis = getAxis(axis);

	//return an error when an invalid axis was given.
	if (!current_axis)
		return_value |= Telescope::ERROR_INVALID_AXIS;

	//return an error if the target is outside the given axis' limits.
	else if (!current_axis->canSlewTo(target))
		return_value |= Telescope::ERROR_OUT_OF_BOUNDS_X;

	//TODO check if target is within this axis' movement limits.

	if (return_value == Telescope::ERROR_NONE)
	{
		current_axis->slew(target);
	}

	return return_value;
}

//--------------------------------------------------------------------------------------------------
//ASCOM functions.
uint8_t Telescope::moveAxis(uint8_t axis, float speed)
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//check if the telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	TelescopeAxis* current_axis = getAxis(axis);

	//return an error when an invalid axis was given.
	if (!current_axis)
		return_value |= Telescope::ERROR_INVALID_AXIS;

	if (return_value == Telescope::ERROR_NONE)
	{
		current_axis->moveAxis(speed);
	}

	return return_value;
}

uint8_t Telescope::pulseGuide(uint8_t direction, uint32_t duration, float rate)
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//check if the telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	//check if the direction is valid. valid directions are  0, 1, 2, 3 (see
	//direction_t enum)
	if (direction >= 4)
		return_value |= Telescope::ERROR_OTHER;

	if (return_value == Telescope::ERROR_NONE)
	{
		//uint8_t axis;
		TelescopeAxis* current_axis;

		//move the telescope north or south.
		if (direction == Telescope::NORTH || direction == Telescope::SOUTH)
		{
			//axis = Y_AXIS;
			current_axis = x_axis_;
		}

		//move the telescope east or west.
		else if (direction == Telescope::EAST || direction == Telescope::WEST)
		{
			//axis = X_AXIS;
			current_axis = y_axis_;

			direction -= 2;		//convert EAST / WEST to DIR_DOWN / DIR_UP
		}

		current_axis->pulseGuide(direction, duration, rate);
	}

	return return_value;
}

uint8_t Telescope::slew(float x, float y)
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//when the telescope mount is (german) equatorial, x is given in hour angles, convert to 
	//degrees.
	if (mount_type_ == Telescope::EQUATORIAL || mount_type_ == Telescope::GERMAN_EQUATORIAL) 
		x *= 15.0;

	//check if the telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	//check if the target is within the axis' movement limits.
	if (x_axis_->canSlewTo(x) == false)
		return_value |= Telescope::ERROR_OUT_OF_BOUNDS_X;

	if (y_axis_->canSlewTo(y) == false)
		return_value |= Telescope::ERROR_OUT_OF_BOUNDS_Y;

	//when no error occured, activate the slew command.
	if (return_value == Telescope::ERROR_NONE)
	{
		x_axis_->slew(x);
		y_axis_->slew(y);
	}

	return return_value;
}

uint8_t Telescope::slewToTarget()
{
	return slew(x_axis_->getTarget(), y_axis_->getTarget());
}

uint8_t Telescope::tracking(bool enable)
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//check if the telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	if (return_value == Telescope::ERROR_NONE)
	{
		//activate tracking for x and y axes.
		x_axis_->tracking(enable);
		y_axis_->tracking(enable);
	}

	return return_value;
}

uint8_t Telescope::abortSlew()
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//check if the telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	//stop the telescope and restore tracking.
	if (return_value == Telescope::ERROR_NONE)
	{
		x_axis_->abortSlew();
		y_axis_->abortSlew();
		z_axis_->abortSlew();
	}

	return return_value;
}

uint8_t Telescope::canFindHome()
{
	uint8_t return_value = 0;

	return_value += static_cast<uint8_t>(x_axis_->canFindHome());
	return_value += static_cast<uint8_t>(y_axis_->canFindHome()) << 1;
	return_value += static_cast<uint8_t>(z_axis_->canFindHome()) << 2;

	return return_value;
}

bool Telescope::findHome()
{
	homing_error_ = Telescope::ERROR_NONE;

	//check if the telescope is parked.
	if (parked_)
		homing_error_ |= Telescope::ERROR_PARKED;

	//check if homing is already in progress.
	if (isHoming())
		homing_error_ |= Telescope::ERROR_HOMING;

	////check if x axis home position is beyond axis limits.
	//if (x_axis_->withinLimits(x_axis_->getHomePosition()))
	//	homing_error_ |= Telescope::ERROR_OUT_OF_BOUNDS_X;

	////check if y axis home position is beyond axis limits.
	//if (y_axis_->withinLimits(y_axis_->getHomePosition()))
	//	homing_error_ |= Telescope::ERROR_OUT_OF_BOUNDS_Y;

	////todo: check if z axis home position is beyond axis limits.
	//if (z_axis_->withinLimits(x_axis_->getHomePosition()))
	//	homing_error_ |= Telescope::ERROR_OUT_OF_BOUNDS_Z;

	//TODO add more error conditions 

	//return false if an error occurred.
	if (homing_error_ != Telescope::ERROR_NONE)
		return false;

	//if no error occurred, initiate homing of the telescope's axes.
	x_axis_->findHome();
	y_axis_->findHome();
	//z_axis_->findHome();

	return true;
}

uint8_t Telescope::getHomingError(uint8_t axis)
{
	if (TelescopeAxis *current_axis = getAxis(axis))
		return current_axis->getHomingError();

	return homing_error_;
}

uint8_t Telescope::park()
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//check if telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	//when parking is not enabled for the telescope, return an error code.
	if (!park_enabled_)
		return_value |= Telescope::ERROR_NOT_AVAILABLE;

	//check if the park position is within the movement limits (if enabled).
	if (!x_axis_->canPark())
		return_value |= Telescope::ERROR_OUT_OF_BOUNDS_X;

	if (!y_axis_->canPark())
		return_value |= Telescope::ERROR_OUT_OF_BOUNDS_Y;

	//when no error occured, send the telescope to its park position.
	if (return_value == Telescope::ERROR_NONE)
	{
		x_axis_->park();
		y_axis_->park();

		z_axis_->stop();		//stop z axis movement.

		parked_ = true;
	}

	return return_value;
}

uint8_t Telescope::unpark()
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//check if telescope is parked.
	if (!parked_) return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	//when parking is not enabled for the telescope, return an error code.
	if (!park_enabled_)
		return_value |= Telescope::ERROR_NOT_AVAILABLE;

	//when no error occured, unpark the telescope.
	if (return_value == Telescope::ERROR_NONE)
	{
		x_axis_->abortSlew();
		y_axis_->abortSlew();
		y_axis_->abortSlew();

		parked_ = false;
	}

	return return_value;
}

bool Telescope::atPark()
{
	//return the current state of parked_. 
	return parked_;
}

float Telescope::getPark(uint8_t axis)
{
	if (TelescopeAxis* current_axis = getAxis(axis))
		return current_axis->getPark();

	return 0.0f;
}

void Telescope::setPark()
{
	x_axis_->setPark();
	y_axis_->setPark();
}

bool Telescope::atHome()
{
	//todo: atHome() should only return true when the telescope is at its home position and 
	//has not been moved.
	//return x_axis_->atHome() && y_axis_->atHome();
	return (atHome(TelescopeAxis::X_AXIS) && atHome(TelescopeAxis::Y_AXIS));
}

bool Telescope::atHome(uint8_t axis)
{
	if (TelescopeAxis* current_axis = getAxis(axis))
		return current_axis->atHome();

	return false;
}

bool Telescope::isHoming()
{
	return (x_axis_->isHoming() || y_axis_->isHoming()); // || z_axis_->isHoming()) 
}

float Telescope::convertSpeed(float speed, speed_unit_t unit_src, speed_unit_t unit_dst)
{
	if (unit_src == unit_dst)
		return speed;

	float factor = 1.0;

	//convert the given speed from the given unit to steps/second.
	switch (unit_src)
	{
	case SECONDS_OF_RA_PER_SIDEREAL_SECOND:
		//if the speed unit is seconds of right ascension per sidereal second, convert to arcseconds per sidereal second:
		factor *= 15.04106864;
	case ARCSECONDS_PER_SIDEREAL_SECOND:
		//if the speed unit is arcseconds / sidereal second, convert to arcseconds / second:
		factor *= 0.9972695677;	//1 sidereal second = 0.9972695677 seconds.
	case ARCSECONDS_PER_SECOND:
		//if the speed unit is arcseconds / second, convert to degrees / second.
		factor /= 3600.0;		//1 degree = 3600 arcseconds.
	default:
		break;
	}

	//speed is now in degrees / second

	switch (unit_dst)
	{
	case SECONDS_OF_RA_PER_SIDEREAL_SECOND:
		//if the speed unit is seconds of right ascension per sidereal second, convert to arcseconds per sidereal second:
		factor /= 15.04106864;
	case ARCSECONDS_PER_SIDEREAL_SECOND:
		//if the speed unit is arcseconds / sidereal second, convert to arcseconds / second:
		factor /= 0.9972695677;	//1 sidereal second = 0.9972695677 seconds.
	case ARCSECONDS_PER_SECOND:
		//if the speed unit is arcseconds / second, convert to degrees / second.
		factor *= 3600.0;		//1 degree = 3600 arcseconds.
	default:
		break;
	}

	return speed * factor;
}

bool Telescope::isMoving()
{
	return x_axis_->isMoving() || y_axis_->isMoving() || z_axis_->isMoving();
}

bool Telescope::isMoveAxis()
{
	//check if the telescope axes are moving in response to a moveAxis command.
	return (x_axis_->isMoveAxis() || y_axis_->isMoveAxis() || z_axis_->isMoveAxis());
}

bool Telescope::isMoveAxis(uint8_t axis)
{
	if (TelescopeAxis *current_axis = getAxis(axis))
		return current_axis->isMoveAxis();

	return false;
}

bool Telescope::isPulseGuiding()
{
	//return true if pulse guiding is activated for any axis.
	return (x_axis_->isPulseGuiding() || y_axis_->isPulseGuiding());
}

bool Telescope::isSlewing()
{
	//slewing is disabled when the telescope is parked or when the telescope is homing.
	//necessary because internally a slew operation is initiated to move the telescope to its 
	//park position and during homing.
	if (parked_)
		return false;

	if (isHoming())
		return false;

	//check if the telescope axes are slewing.	
	return (x_axis_->isSlewing() || y_axis_->isSlewing()); // || z_axis_->isSlewing()); //z Axis cannot slew.
}

bool Telescope::isTracking()
{
	//telescope is tracking when tracking is enabled for any axis.
	return  (x_axis_->isTracking() || y_axis_->isTracking());
}

float Telescope::getCurrentAlignment(uint8_t axis)
{
	float alignment = NAN;

	if (TelescopeAxis* current_axis = getAxis(axis))
	{
		alignment = current_axis->getCurrentAlignment();

		if (axis == TelescopeAxis::X_AXIS)
		{
			//when the telescope mount is (german) equatorial, convert from degrees to hour angle.
			if (mount_type_ == Telescope::EQUATORIAL || mount_type_ == Telescope::GERMAN_EQUATORIAL) alignment /= 15.0;
		}
	}

	return alignment;
}

uint8_t Telescope::setCurrentAlignment(uint8_t axis, float alignment)
{
	uint8_t return_value = Telescope::ERROR_NONE;

	//return an error if the telescope is parked.
	if (parked_)
		return_value |= Telescope::ERROR_PARKED;

	//check if the telescope is currently homing.
	if (isHoming())
		return_value |= Telescope::ERROR_HOMING;

	if (axis == TelescopeAxis::X_AXIS)
	{
		//when the telescope mount is (german) equatorial, x is given in hour angles, convert to 
		//degrees.
		if (mount_type_ == Telescope::EQUATORIAL || mount_type_ == Telescope::GERMAN_EQUATORIAL)
			alignment *= 15.0;
	}

	//check if an invalid axis was given.
	TelescopeAxis* current_axis = getAxis(axis);
	if (!current_axis)
		return_value |= Telescope::ERROR_INVALID_AXIS;

	//check if the given alignment is within the axis' movement limits.
	else if (!current_axis->withinLimits(alignment))
		return_value |= Telescope::ERROR_OUT_OF_BOUNDS_X;

	if (return_value == Telescope::ERROR_NONE)
	{
		current_axis->setCurrentAlignment(alignment);
	}

	return return_value;
}

float Telescope::getMoveAxisRate(uint8_t axis)
{
	float rate = NAN;

	if (TelescopeAxis* current_axis = getAxis(axis))
		if (current_axis->isMoveAxis())
			rate = current_axis->getSpeed();

	return rate;
}

float Telescope::getMoveAxisOffset(uint8_t axis)
{
	float offset = NAN;
	
	if (TelescopeAxis* current_axis = getAxis(axis))
		offset = current_axis->getMoveAxisOffset();

	return offset;
}

bool Telescope::setMoveAxisOffset(uint8_t axis, float offset)
{
	if (TelescopeAxis* current_axis = getAxis(axis))
		return current_axis->setMoveAxisOffset(offset);
	
	return false;
}

float Telescope::getRightAscensionRate()
{
	if (!x_axis_)
		return NAN;

	return Telescope::convertSpeed(x_axis_->getTrackingRateOffset(), Telescope::DEGREES_PER_SECOND, Telescope::SECONDS_OF_RA_PER_SIDEREAL_SECOND);
}

bool Telescope::setRightAscensionRate(float rate)
{
	if (!y_axis_)
		return false;

	x_axis_->setTrackingRateOffset(Telescope::convertSpeed(rate, Telescope::SECONDS_OF_RA_PER_SIDEREAL_SECOND, Telescope::DEGREES_PER_SECOND));

	return true;
}

float Telescope::getDeclinationRate()
{
	if (!y_axis_)
		return NAN;

	return Telescope::convertSpeed(y_axis_->getTrackingRateOffset(), Telescope::DEGREES_PER_SECOND, Telescope::ARCSECONDS_PER_SECOND);
}

bool Telescope::setDeclinationRate(float rate)
{
	if (!y_axis_)
		return false;

	y_axis_->setTrackingRateOffset(Telescope::convertSpeed(rate, Telescope::ARCSECONDS_PER_SECOND, Telescope::DEGREES_PER_SECOND));

	return true;
}

uint8_t Telescope::getTrackingRate()
{
	return tracking_rate_;
}

uint8_t Telescope::setTrackingRate(uint8_t tracking_rate)
{
	//get the requested tracking rate in arcseconds / sidereal seconds
	float rate = getWellKnownTrackingRate(tracking_rate);

	if (isnan(rate))
		return Telescope::ERROR_OTHER;

	x_axis_->setTrackingRate(Telescope::convertSpeed(rate, Telescope::ARCSECONDS_PER_SECOND, Telescope::DEGREES_PER_SECOND));

	tracking_rate_ = tracking_rate;

	return 0;
}

float Telescope::getWellKnownTrackingRate(uint8_t rate)
{
	//return the requested tracking rate in arcseconds / sidereal second.
	switch (rate)
	{
		case Telescope::TR_SIDEREAL:
			return -15.0411f;
		case Telescope::TR_LUNAR:
			return -14.685f;
		case Telescope::TR_SOLAR:
			return -15.0f;
		case Telescope::TR_KING:
			return -15.0369;
	}

	return NAN;	//return 0.0f when an invalid rate has been given.
}

uint8_t Telescope::reportSwitches(uint8_t axis)
{
	if (TelescopeAxis *current_axis = getAxis(axis))
		return current_axis->reportSwitches();

	//return an error when an invalid axis was given.
	return Telescope::ERROR_INVALID_AXIS;
}

float Telescope::getMaxSpeedDeg(uint8_t axis)
{
	float max_speed = NAN;

	if (TelescopeAxis *current_axis = getAxis(axis))
		max_speed = current_axis->getMaxSpeedDeg();

	return max_speed;
}

uint8_t Telescope::getLocked()
{
	return locked_;
}

void Telescope::setLocked(uint8_t state)
{
	if (state > 2)
		return;

	locked_ = state;

	//switch (source)
	//{
	//	case 0:
	//		locked_ = LOCK_NONE;
	//		break;
	//	case 1:
	//		locked_ = LOCK_LOCAL;
	//		break;
	//	case 2:
	//		locked_ = LOCK_REMOTE;
	//		break;
	//	default:
	//		break;
	//}
}

float Telescope::getTemperature(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkSensor(sensor))
		return NAN;

	//return the last measured temperature from the sensor.
	return temperature_sensors_[sensor]->getValue();
}

bool Telescope::getTemperatureComplete(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkSensor(sensor))
		return false;

	//return true if the sensor has finished the mesaurement, false otherwise.
	return temperature_sensors_[sensor]->hasValue();
}

bool Telescope::measureTemperature(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkSensor(sensor))
		return false;

	//start a temperature measurement and return the result.
	return temperature_sensors_[sensor]->measure();
}

//--------------------------------------------------------------------------------------------------
//private functions:

bool Telescope::checkSensor(uint8_t sensor)
{
	//check the sensor number
	if (sensor > TEMP_SENSORS)
		return false;

	//check if the sensor was intialized
	if (!temperature_sensors_[sensor])
		return false;

	return true;
}

void Telescope::stop()
{
	x_axis_->stop();
	y_axis_->stop();
	z_axis_->stop();
}

void Telescope::stop(uint8_t axis, bool hard_stop)
{
	if (TelescopeAxis* current_axis = getAxis(axis))
		current_axis->stop(hard_stop);
}

//--------------------------------------------------------------------------------------------------
void Telescope::writeDefaults()
{
	//write default mount type.
	writeEEPROM(EEPROMHandler::telescope_variable_::MOUNT_TYPE, static_cast<uint8_t>(DEF_T_MOUNT_TYPE));

	//write default site information.
	writeEEPROM(EEPROMHandler::telescope_variable_::SITE_LATITUDE, DEF_T_SITE_LATITUDE);
	writeEEPROM(EEPROMHandler::telescope_variable_::SITE_LONGITUDE, DEF_T_SITE_LONGITIDE);
	writeEEPROM(EEPROMHandler::telescope_variable_::SITE_ELEVATION, DEF_T_SITE_ELEVATION);

	//write default telescope information.
	writeEEPROM(EEPROMHandler::telescope_variable_::FOCAL_LENGTH, DEF_T_FOCAL_LENGTH);
	writeEEPROM(EEPROMHandler::telescope_variable_::APERTURE_DIAMETER, DEF_T_APERTURE_DIAMETER);
	writeEEPROM(EEPROMHandler::telescope_variable_::APERTURE_AREA, DEF_T_APERTURE_AREA);

	//write default park information.
	writeEEPROM(EEPROMHandler::telescope_variable_::PARK_ENABLED, DEF_T_PARK_ENABLED);

	//write default automatic homing behaviour.
	writeEEPROM(EEPROMHandler::telescope_variable_::STARTUP_AUTO_HOME, DEF_T_STARTUP_AUTO_HOME);

	//write default manual control settings.
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_AVAILABLE, DEF_T_MC_AVAILABLE);
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_BTN_TOGGLE_AVAILABLE, DEF_T_MC_BTN_TOGGLE_AVAILABLE);
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_BTN_TOGGLE, static_cast<uint8_t>(DEF_T_MC_BTN_TOGGLE_PIN));
	//writeEEPROM(EEPROMHandler::telescope_variable_::MC_BTN_TOGGLE_INVERTED, static_cast<uint8_t>(DEF_T_MC_BTN_TOGGLE_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_BTN_SPEED_AVAILABLE, DEF_T_MC_BTN_SPEED_AVAILABLE);
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_BTN_SPEED, static_cast<uint8_t>(DEF_T_MC_BTN_SPEED_PIN));
	//writeEEPROM(EEPROMHandler::telescope_variable_::MC_BTN_SPEED_INVERTED, static_cast<uint8_t>(DEF_T_MC_BTN_SPEED_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_JST_INPUT_X, static_cast<uint8_t>(DEF_T_MC_JST_INPUT_X));
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_JST_INPUT_Y, static_cast<uint8_t>(DEF_T_MC_JST_INPUT_Y));
	writeEEPROM(EEPROMHandler::telescope_variable_::MC_JST_DEADZONE, static_cast<uint8_t>(DEF_T_MC_JST_DEADZONE));

	//write default values for all axes.
	for (uint8_t i = 0; i <= 2; i++) 
		TelescopeAxis::writeDefaults(i);

	YAAASensor::SensorSettings sensor_settings = {
		DEF_T_TEMPERATURE_SENSOR_TYPE,
		DEF_T_TEMPERATURE_SENSOR_DEVICE,
		DEF_T_TEMPERATURE_SENSOR_UNIT,
		DEF_T_TEMPERATURE_SENSOR_PARAMETERS
	};

	for (uint8_t i = 0; i < TEMP_SENSORS; i++)
		writeEEPROM
		(
			EEPROMHandler::telescope_variable_::TEMPERATURE_SENSOR,
			sensor_settings,
			i
		);
}

