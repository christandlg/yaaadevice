//Adafruit Motor Shield V2 (wrapper) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef ARDUINO_SAM_DUE

#include "StepperMotorAFMSV2.h"

//initialize static member variables.
uint8_t StepperMotorAFMSV2::current_step_type_ = 0;
Adafruit_StepperMotor *StepperMotorAFMSV2::current_stepper_ = NULL;

StepperMotorAFMSV2::StepperMotorAFMSV2(uint8_t I2CAddress, uint8_t terminal, uint16_t number_of_steps) :
StepperMotor(stepperForwardStep, stepperBackwardStep)
{
	//initialize Adafruit Motor Shield.
	AFMS_ = new Adafruit_MotorShield(I2CAddress);

	//initialize stepper motor.
	stepper_ = AFMS_->getStepper(number_of_steps, terminal);

	//start motor shield. 
	AFMS_->begin();

	step_type_ = STEPTYPE_SINGLE;

	setStepType(STEPTYPE_SINGLE);
}

StepperMotorAFMSV2::~StepperMotorAFMSV2()
{
	delete AFMS_;
}

void StepperMotorAFMSV2::activate()
{
	//stepper_->quickstepInit();

	stepper_released_ = false;
}

void StepperMotorAFMSV2::release()
{
	stepper_->release();

	stepper_released_ = true;
}

bool StepperMotorAFMSV2::run()
{
	current_step_type_ = step_type_;
	current_stepper_ = stepper_;

	return StepperMotor::run();
}

bool StepperMotorAFMSV2::runSpeed()
{
	current_step_type_ = step_type_;
	current_stepper_ = stepper_;

	return StepperMotor::runSpeed();
}

bool StepperMotorAFMSV2::runSpeedToPosition()
{
	current_step_type_ = step_type_;
	current_stepper_ = stepper_;

	return StepperMotor::runSpeedToPosition();
}

uint8_t StepperMotorAFMSV2::getStepType()
{
	switch (step_type_)
	{
	case QUICKSTEP:
		return STEPTYPE_QUICKSTEP;
	case SINGLE:
		return STEPTYPE_SINGLE;
	case DOUBLE:
		return STEPTYPE_FULL;
	case INTERLEAVE:
		return STEPTYPE_MICROSTEP_2;
	case MICROSTEP:
		return STEPTYPE_MICROSTEP_16;
	default:
		return STEPTYPE_FULL;
	}
}

bool StepperMotorAFMSV2::setStepType(uint8_t step_type)
{
	//preserve currently set step type (for stepper position adjustment).
	uint8_t prev_step_type = getStepType();

	if (step_type == prev_step_type)
		return true;

	switch (step_type)
	{
	case STEPTYPE_QUICKSTEP:
		step_type_ = QUICKSTEP;
		break;
	case STEPTYPE_SINGLE:
		step_type_ = SINGLE;
		break;
	case STEPTYPE_FULL:
		step_type_ = DOUBLE;
		break;
	case STEPTYPE_MICROSTEP_2:
		step_type_ = INTERLEAVE;
		break;
	case STEPTYPE_MICROSTEP_16:
		step_type_ = MICROSTEP;
		break;
	default:
		//return false when an invalid steptype was given.
		return false;
	}

	//recalculate stepper position.
	//changing from a higher microstep factor to a lower factor might result in loss of pointing 
	//accuracy.
	long next_factor = getMicroStepFactor(step_type);
	long prev_factor = getMicroStepFactor(prev_step_type);

	long updated_position = AccelStepper::currentPosition() * next_factor / prev_factor;
	AccelStepper::setCurrentPosition(updated_position);

	return true;
}

//--------------------------------------------------------------------------------------------------
//static member functions.
void StepperMotorAFMSV2::stepperForwardStep()
{
	//if (StepperMotorAFMSV2::current_step_type_ == QUICKSTEP)
	//	StepperMotorAFMSV2::current_stepper_->quickstep(FORWARD);
	//else
		StepperMotorAFMSV2::current_stepper_->onestep(FORWARD, StepperMotorAFMSV2::current_step_type_);
}

void StepperMotorAFMSV2::stepperBackwardStep()
{
	//if (StepperMotorAFMSV2::current_step_type_ == QUICKSTEP)
	//	StepperMotorAFMSV2::current_stepper_->quickstep(BACKWARD);
	//else
		StepperMotorAFMSV2::current_stepper_->onestep(BACKWARD, StepperMotorAFMSV2::current_step_type_);
}

#endif /* ARDUINO_SAM_DUE */
