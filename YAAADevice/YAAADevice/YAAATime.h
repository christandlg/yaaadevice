//a class providing some time functions, i.e. for printing the system time.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#ifndef YAAATIME_H_
#define YAAATIME_H_

#include <Arduino.h>

#include <Time.h>
#include <TimeLib.h>
#if !defined(ARDUINO_SAM_DUE) && !defined (ESP8266) && !defined(ESP32)
#include <DS1307RTC.h>
#endif

//#include "EEPROMHandler.h"

class YAAATime
{
public:
	/*
	attempts to set the RTC as the clock source. 
	@return true on success, false if it fails. */
	static bool begin();

	/*
	converts the given time to a String in ISO8601 format.
	@param string: pointer to char array to write the result to. must be at least 20 bytes.
	@param time: time in seconds since 1970-01-01T00:00:00. uses the current time
	provided by the RTC if no time is given or is given as 0.
	@return: pointer to char array containing the ISO8691 formatted date/time string. */
	static char* timeToISO8601(char *string, time_t time = 0L);

	/*
	converts the given time ti a String in HH:MM:SS format.
	@param string: pointer to char array to write the result to. must be at least 9 bytes.
	@param time: time in seconds since 1970-01-01T00:00:00. uses the current time
	provided by the RTC if no time is given or is given as 0.
	@param format_12_hrs: when set to true, returned string will be in 12hr format
	(default: 24hr format).
	@return: pointer to char array containing the time in HMS format. */
	static char* timeToHMS(char *string, bool format_12_hrs = false, time_t time = 0L);

	/*
	converts the given time to a String in MM/DD/YY format.
	@param string: pointer to char array to write the result to. must be at least 9 bytes.
	@param time: time in seconds since 1970-01-01T00:00:00. uses the current time
	provided by the RTC if no time is given or is given as 0.
	@param delimiter: delimiter to use (default: '/').
	@return: pointer to char array containing the date in MM/DD/YY format. */
	static char* timeToMDY(char *string, char delimiter = '/', time_t time = 0L);

	/*
	returns the current system time in seconds since 1970-01-01T00:00:00. */
	static time_t getTime();

	/*
	sets the current time to the given time.
	@param time: time in seconds since 1970-01-01T00:00:00.
	@return: true if successful, false otherwise.*/
	static void setDateTime(time_t time);

	/*
	sets the current time to the given year, month, day, hour, minute and second.
	@return: true if successful, false otherwise.*/
	static void setDateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second);

	/*
	sets the current date to the given date. */
	static void setNewDate(uint16_t year, uint8_t month, uint8_t day);

	/*
	sets the current time to the given time. */
	static void setNewTime(uint8_t hour, uint8_t minute, uint8_t second);

	/*
	@return: the currently set UTC offset time in hours.*/
	static float getUTCOffset();

	/*
	sets the UTC offset to the given value and writes the value to EEPROM.
	@param offset: offset in hours.
	@return: true on success, false if setting the offset failed (offset > 24 hours). */
	static bool setUTCOffset(float offset);

private:
	static float offset_UTC_;		//number of hours to add to convert time to UTC.
};

#endif /*YAAATIME_H_*/


