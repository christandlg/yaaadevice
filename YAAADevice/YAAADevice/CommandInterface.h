//Command Interface class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef COMMAND_INTERFACE_H_
#define COMMAND_INTERFACE_H_

#include <Arduino.h>
#include <Stream.h>

#include "defaults.h"

class CommandInterface :
	public Stream 
{
public:
	CommandInterface();
	virtual ~CommandInterface();

	/*
	initializes the object. *must* be called before the object is used. 
	@return true on success, false otherwise. */
	bool begin();

	/*
	@param config element identifier. 
	@return config element as String. */
	virtual String getConfig(uint8_t config) = 0;

	/*
	@param config element identifier
	@param value to set
	@return true on success, false otherwise. */
	virtual bool setConfig(uint8_t config, String value) = 0;

	//Stream implementation
	//---------------------------------------------------------------------------------------------
	virtual int available() = 0;
	virtual int read() = 0;
	virtual int peek() = 0;

	//virtual size_t readBytes(char *buffer, size_t length); // read chars from stream into buffer
	//virtual size_t readBytes(uint8_t *buffer, size_t length) {
	//	return readBytes((char *)buffer, length);
	//}
	//---------------------------------------------------------------------------------------------

	//Print implementation
	//---------------------------------------------------------------------------------------------
	virtual size_t write(uint8_t data) = 0;
	//virtual size_t write(const uint8_t *buffer, size_t size);

	virtual void flush() { /* Empty implementation for backward compatibility */ }
	//---------------------------------------------------------------------------------------------

protected:
	/*
	initializes the interface. must be implemented by derived classes. */
	virtual bool beginInterface() = 0;
};

#endif /* COMMAND_INTERFACE_H_ */
