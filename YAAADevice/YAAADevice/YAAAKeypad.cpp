//Keypad class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "YAAAKeypad.h"

YAAAKeypad::YAAAKeypad(keypad_type_t keypad_type) :
	keypad_type_(keypad_type),
	read_interval_(250L), read_last_(0L),
	press_start_(0L), hold_interval_(500L)
{
}

YAAAKeypad::~YAAAKeypad()
{
}

uint8_t YAAAKeypad::getType()
{
	return keypad_type_;
}

bool YAAAKeypad::isReadDue()
{
	return (millis() - read_last_ > read_interval_);
}

void YAAAKeypad::updateKey(uint8_t key_code)
{
	switch (key_.state_)
	{
		case YAAAKey::IDLE:
			if (key_code != YAAAKey::KEY_NONE)
			{
				key_.state_ = YAAAKey::PRESSED;
				key_.key_ = key_code;
				press_start_ = millis();
			}
			break;
		case YAAAKey::PRESSED:
			if (key_code != key_.key_)
				key_.state_ = YAAAKey::RELEASED;
			else
				key_.state_ = YAAAKey::HOLD_SHORT;
			break;
		case YAAAKey::HOLD_SHORT:
			if (key_code != key_.key_)
				key_.state_ = YAAAKey::RELEASED;

			else if (millis() - press_start_ > hold_interval_)
				key_.state_ = YAAAKey::HOLD_LONG;
			break;
		case YAAAKey::HOLD_LONG:
			if (key_code != key_.key_)
				key_.state_ = YAAAKey::RELEASED;
			break;
		case YAAAKey::RELEASED:
			key_.state_ = YAAAKey::IDLE;
			break;
	}
}

