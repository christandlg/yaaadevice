//TTP229B Keypad class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAAKEYPAD_TTP229_H_
#define YAAAKEYPAD_TTP229_H_

#include <Arduino.h>
#include "YAAAKeypad.h"

class YAAAKeypad_TTP229B : public YAAAKeypad
{
public:
	//keypad is expected to be in 16 key mode.
	//@param settings Keypad Settings as YAAAKeypad::KeypadSettings
	YAAAKeypad_TTP229B(YAAAKeypad::KeypadSettings settings);

	~YAAAKeypad_TTP229B();

	/*
	@return the pressed key as YAAAKeypad::key_t*/
	YAAAKey getKey();

private:
	/*
	@return: 2 byte unsigned integer. set bits correlate to pressed buttons. LSB = 1, MSB = 16*/
	uint16_t read();

	uint8_t getKeyCode(uint16_t bits);

	uint8_t pin_SCL_;
	uint8_t pin_SDO_;

	bool active_high_;		//default: false
};

#endif /* KEYPAD_TTP229_H_ */

