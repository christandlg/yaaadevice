//Focuser class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Focuser.h"

Focuser::Focuser() :
	stepper_motor_(NULL),

	speed_(1.0f),

	number_of_steps_(200), gear_ratio_(1.0f),
	automatic_stepper_release_(true),
	microns_per_rev_(32767.0f), step_size_(1.0f),

	step_type_(StepperMotor::STEPTYPE_FULL),

	limit_lower_enabled_(true), limit_upper_enabled_(true),
	limit_lower_(0), limit_upper_(32767),

	endstop_lower_(NULL), endstop_upper_(NULL), home_switch_(NULL),
	endstop_home_switch_(false),

	limit_state_(0), endstop_state_(0),

	homing_(false), home_cleared_(false),
	home_first_pos_found_(false), home_found_(false),
	home_position_(0), home_first_pos_(0),
	homing_error_(error_code_homing_t::ERROR_HOMING_NONE),
	home_start_time_(0L), home_timeout_(0L),

	absolute_(true),
	max_increment_(32767),

	temp_comp_sensor_(0),
	temp_comp_interval_(0), temp_comp_last_(0),
	temp_comp_factor_(1.0f),
	temp_comp_ref_pos_(0), temp_comp_ref_temp_(0.0f),

	temp_comp_state_(temp_comp_state_t::TC_DISABLED),

	locked_(lock_source_t::LOCK_NONE)
{
	uint8_t motor_driver;
	readEEPROM(EEPROMHandler::focuser_variable_::MOTOR_DRIVER_TYPE, motor_driver);

	readEEPROM(EEPROMHandler::focuser_variable_::MOTOR_DRIVER_TYPE, number_of_steps_);

	switch (motor_driver)
	{
#ifndef ARDUINO_SAM_DUE
	case StepperMotor::ADAFRUIT_MOTORSHIELD_V2:
	{
		//read Motor Shield I2C Address and used terminal.
		uint8_t i2c_adress;
		uint8_t terminal;
		readEEPROM(EEPROMHandler::focuser_variable_::I2C_ADDRESS, i2c_adress);
		readEEPROM(EEPROMHandler::focuser_variable_::TERMINAL, terminal);

		stepper_motor_ = new StepperMotorAFMSV2(i2c_adress, terminal, number_of_steps_);
	}
	break;
#endif /* ARDUINO_SAM_DUE */
	case StepperMotor::FOUR_WIRE_STEPPER:
	{
		//read four wire stepper pins.
		uint8_t /*pin_enable,*/ pin1, pin2, pin3, pin4;
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_1, pin1);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_2, pin2);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_3, pin3);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_4, pin4);

		//stepper_motor_ = new StepperMotor(pin1, pin2, pin3, pin4);
		stepper_motor_ = new StepperMotorFourWire(pin1, pin2, pin3, pin4);

		//read motor driver pin inversion settings.
		bool pin1_inv, pin2_inv, pin3_inv, pin4_inv;
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_1_INVERTED, pin1_inv);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_2_INVERTED, pin2_inv);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_3_INVERTED, pin3_inv);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_4_INVERTED, pin4_inv);

		stepper_motor_->setPinsInverted(pin1_inv, pin2_inv, pin3_inv, pin4_inv, false);
	}
	break;
	case StepperMotor::DRIVER_A4988:
	case StepperMotor::DRIVER_DRV8825:
	{
		//read motor driver pins.
		uint8_t step, direction, enable, ms1, ms2, ms3;
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_1, step);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_2, direction);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_3, enable);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_4, ms1);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_5, ms2);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_6, ms3);

		stepper_motor_ = new StepperMotorDriver(motor_driver, step, direction, enable, ms1, ms2, ms3);

		//read motor driver pin inversion settings.
		bool step_inv, direction_inv, enable_inv;
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_1_INVERTED, step_inv);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_2_INVERTED, direction_inv);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_3_INVERTED, enable_inv);

		stepper_motor_->setPinsInverted(direction_inv, step_inv, enable_inv);

		//read microstepping steptype pin inversion settings.
		bool ms1_inv, ms2_inv, ms3_inv;
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_4_INVERTED, ms1_inv);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_5_INVERTED, ms2_inv);
		readEEPROM(EEPROMHandler::focuser_variable_::PIN_6_INVERTED, ms3_inv);

		stepper_motor_->setMicroStepPinsInverted(ms1_inv, ms2_inv, ms3_inv);
	}
	break;
	default:
		stepper_motor_ = new StepperMotorFourWire(255, 255, 255, 255, false);
		break;
	}

	//failsafe.
	//call default constructor when an invalid driver type has been read from EEPROM.
	if (!stepper_motor_)
		return;

	stepper_motor_->release();

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	//read maximum speed and acceleration.
	//float acceleration;
	//readEEPROM(EEPROMHandler::focuser_variable_::ACCELERATION, acceleration);
	readEEPROM(EEPROMHandler::focuser_variable_::MAX_SPEED, speed_);

	//set default stepper motor parameters. 
	//when using an Adafruit Motor Shield, maximum stepper speed is limited by I2C communication 
	//speed. maximum speed for all Adafruit Motorshield driven steppers combined is 700 - 1200 
	//steps / second (depending on steptype and I2C communication speed).  
	//independent of used stepper drivers, stepper speed should not exceed 1000 steps / second for
	//each individual stepper according to AccelStepper documentation ("Speeds of more than 1000 
	//steps per second are unreliable").
	//setting the maximum speed too high will result in skipped steps. 
	//acceleration is limited to prevent the motor from skipping steps when accelerating.
	stepper_motor_->setMaxSpeed(speed_);					//speed in steps / second
	stepper_motor_->setSpeed(speed_);					//speed in steps / second
	//stepper_motor_->setAcceleration(acceleration);			//steps / second^2

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	//read gear ration and microns per focuser knob revolution.
	readEEPROM(EEPROMHandler::focuser_variable_::GEAR_RATIO, gear_ratio_);
	readEEPROM(EEPROMHandler::focuser_variable_::MICRONS_PER_REV, microns_per_rev_);

	//calculate step size in microns.
	step_size_ = microns_per_rev_ * gear_ratio_ / static_cast<float>(number_of_steps_);

	//read step types.
	readEEPROM(EEPROMHandler::focuser_variable_::STEP_TYPE, step_type_);
	//readEEPROM(EEPROMHandler::focuser_variable_::STEP_TYPE_TEMP_COMP, step_type_temp_);

	//set fast steptype as default.
	stepper_motor_->setStepType(step_type_);

	//read automatic stepper release setting.
	readEEPROM(EEPROMHandler::focuser_variable_::AUTOMATIC_STEPPER_RELEASE, automatic_stepper_release_);
	stepper_motor_->setAutomaticRelease(automatic_stepper_release_);

	//read x and y axis movement limit information from EEPROM.
	readEEPROM(EEPROMHandler::focuser_variable_::LIMIT_LOWER_ENABLED, limit_lower_enabled_);
	readEEPROM(EEPROMHandler::focuser_variable_::LIMIT_UPPER_ENABLED, limit_upper_enabled_);
	readEEPROM(EEPROMHandler::focuser_variable_::LIMIT_LOWER, limit_lower_);
	readEEPROM(EEPROMHandler::focuser_variable_::LIMIT_UPPER, limit_upper_);

	//read maximum increment settings.
	readEEPROM(EEPROMHandler::focuser_variable_::MAX_INCREMENT, max_increment_);
	readEEPROM(EEPROMHandler::focuser_variable_::MAX_STEP, max_step_);

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------
	//initialize the switches.
	uint8_t pin_nr;
	bool inverted;
	bool enabled = false;

	//endstop switches.
	readEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_LOWER_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_LOWER, pin_nr);
		readEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_LOWER_INVERTED, inverted);
		endstop_lower_ = new Button(pin_nr, inverted);
	}

	readEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_UPPER_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_UPPER, pin_nr);
		readEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_UPPER_INVERTED, inverted);
		endstop_upper_ = new Button(pin_nr, inverted);
	}

	//home position switch.
	readEEPROM(EEPROMHandler::focuser_variable_::HOME_SWITCH_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::focuser_variable_::HOME_SWITCH, pin_nr);
		readEEPROM(EEPROMHandler::focuser_variable_::HOME_SWITCH_INVERTED, inverted);

		//if the pin number is the same as the lower endstops pin number:
		if (pin_nr == endstop_lower_->getPinNumber())
		{
			home_switch_ = endstop_lower_;
			endstop_home_switch_ = true;
		}

		//if the pin number is the same as the upper endstops pin number:
		else if (pin_nr == endstop_upper_->getPinNumber())
		{
			home_switch_ = endstop_upper_;
			endstop_home_switch_ = true;
		}

		//if none of the above, create a new button instance.
		else
		{
			home_switch_ = new Button(pin_nr, inverted);
			endstop_home_switch_ = false;
		}
	}

	readEEPROM(EEPROMHandler::focuser_variable_::HOME_TIMEOUT, home_timeout_);

	//read absolute setting.
	readEEPROM(EEPROMHandler::focuser_variable_::POSITION_ABSOLUTE, absolute_);

	readEEPROM(EEPROMHandler::focuser_variable_::TC_SENSOR, temp_comp_sensor_);

	////read temperature sensor and temperature compensation settings.
	////temperature sensors 
	////-----------------------------------------------------------------------------------
	//for (uint8_t i = 0; i < TEMP_SENSORS; i++)
	//{
	//	temperature_sensors_[i] = NULL;
	//
	//	YAAASensor::SensorSettings sensor_settings;
	//
	//	//read temperature sensor settings from EEPROM.
	//	EEPROMHandler::readValue(
	//		EEPROMHandler::device_type_::TELESCOPE,
	//		EEPROMHandler::telescope_variable_::TEMPERATURE_SENSOR,
	//		i,
	//		sensor_settings);
	//
	//	//skip sensor creation if sensor is not a temperature sensor
	//	if (sensor_settings.sensor_type_ != YAAASensor::SENSOR_TEMPERATURE)
	//		continue;
	//
	//	//select correct TemperatureSensor object to create.
	//	switch (sensor_settings.sensor_device_)
	//	{
	//		case SensorTemperature::DEVICE_ANALOG_IC:
	//			temperature_sensors_[i] = new SensorTemperatureAnalogIC(sensor_settings);
	//			break;
	//		case SensorTemperature::DEVICE_THERMISTOR:
	//			temperature_sensors_[i] = new SensorTemperatureThermistor(sensor_settings);
	//			break;
	//		case SensorTemperature::DEVICE_ONEWIRE_DS18:
	//			temperature_sensors_[i] = new SensorTemperatureOneWire(sensor_settings);
	//			break;
	//		case SensorTemperature::DEVICE_MLX90614:
	//			temperature_sensors_[i] = new SensorTemperatureMLX90614(sensor_settings);
	//			break;
	//		default:
	//			break;
	//	}
	//
	//	//attempt to initialize the temperature sensor. if it fails, delete the sensor.
	//	if (temperature_sensors_[i])
	//	{
	//		if (!temperature_sensors_[i]->begin())
	//		{
	//			delete temperature_sensors_[i];
	//			temperature_sensors_[i] = NULL;
	//		}
	//	}
	//}

	//start homing if startup auto homing is enabled.
	bool startup_auto_home = false;
	readEEPROM(EEPROMHandler::focuser_variable_::STARTUP_AUTO_HOME, startup_auto_home);
	if (startup_auto_home)
		findHome();
}

Focuser::~Focuser()
{
}

void Focuser::run()
{
	endstop_state_ = checkEndstops();
	limit_state_ = checkLimits();

	if (homing_)
		home();

	if (!isMoving() && stepper_motor_->speed() != 0.0f)
		stepper_motor_->setSpeed(0.0f);

	stepper_motor_->runSpeedToPosition();
}

bool Focuser::isHoming()
{
	return homing_;
}

uint8_t Focuser::getHomingError()
{
	return homing_error_;
}

bool Focuser::isMoving()
{
	//return stepper_motor_->speed() != 0.0f;
	return stepper_motor_->distanceToGo() != 0;
}

int32_t Focuser::getPosition()
{
	return stepper_motor_->currentPosition();
}

uint8_t Focuser::setPosition(int32_t position)
{
	uint8_t return_value = 0;

	if (isHoming())
		return_value += ERROR_HOMING;

	if (!absolute_)
		return_value += ERROR_ABSOLUTE;

	if (position < -max_step_)
		return_value += ERROR_LIMIT_LOWER;

	if (position > max_step_)
		return_value += ERROR_LIMIT_UPPER;

	if (return_value == ERROR_NONE)
	{
		stepper_motor_->setCurrentPosition(position);

		//reinitialize temperature compensation when temperature compensation is enabled.
		if (temp_comp_state_ != TC_DISABLED)
			temp_comp_state_ = TC_INITIALIZE;
	}

	return return_value;
}

uint8_t Focuser::halt()
{
	uint8_t return_value = ERROR_NONE;

	if (homing_)
		return_value += ERROR_HOMING;

	if (return_value == ERROR_NONE)
	{
		stepper_motor_->setSpeed(0.0f);
		stepper_motor_->move(0);
	}

	return return_value;
}

uint8_t Focuser::move(int32_t target)
{
	uint8_t return_value = 0;

	//return an error if the focuser is currently homing.
	if (isHoming())
		return_value += ERROR_HOMING;

	//when set to absolute mode, check if the target is between 0 and max step and adjust 
	//the target if necessary.
	if (absolute_)
		target = constrain(target, 0, max_step_);

	//when the focuser is set to relative mode, calculate an absolute target position.
	else
		target += stepper_motor_->currentPosition();

	//check movement limits (if enabled).
	if (limit_lower_enabled_ && (target < limit_lower_))
		return_value += ERROR_LIMIT_LOWER;

	if (limit_upper_enabled_ && (target > limit_upper_))
		return_value += ERROR_LIMIT_UPPER;

	//check if the max increment limit is exceeded.
	if (abs(target - stepper_motor_->currentPosition()) > max_increment_)
		return_value += ERROR_MAX_INCREMENT;

	if (return_value == ERROR_NONE)
	{
		moveTo(target);

		//suspend temperature compensation movement.
		if (temp_comp_state_ != TC_DISABLED)
			temp_comp_state_ = TC_WAIT_MOVEMENT;
	}

	return return_value;
}

uint8_t Focuser::moveDirection(bool direction)
{
	return move(direction ? max_step_ : -max_step_);
}

uint8_t Focuser::moveTime(int32_t time)
{
	//calculate the distance to go (in steps).
	int32_t distance = static_cast<long>(static_cast<float>(time) * stepper_motor_->speed() / 1000.0f);

	if (absolute_)
		distance += stepper_motor_->currentPosition();

	return move(distance);
}

bool Focuser::canFindhome()
{
	if (!home_switch_)
		return false;

	return true;
}

bool Focuser::findHome()
{
	halt();

	homing_error_ = Focuser::ERROR_HOMING_NONE;

	homing_ = true;
	home_found_ = false;
	home_cleared_ = true;
	home_first_pos_found_ = false;

	//check if a home switch is available.
	if (!canFindhome())
		homing_error_ += Focuser::ERROR_HOMING_SWITCH_NOT_AVAILABLE;

	//check if the home position is within the movement limits.
	if (home_position_ < -max_step_)
		homing_error_ += ERROR_HOMING_LIMIT_LOWER;

	if (home_position_ > max_step_)
		homing_error_ += ERROR_HOMING_LIMIT_UPPER;

	if (homing_error_ != Focuser::ERROR_HOMING_NONE)
		return false;

	int32_t direction = 1L;		//direction to go during homing.

	//determine the direction to go. if the home switch is also an endstop switch
	//move towards the endstop switch. if the home switch is not an endstop switch
	//move in the direction the home switch is expected to be.
	if (endstop_home_switch_)
	{
		if (endstop_lower_ && (home_switch_->getPinNumber() == endstop_lower_->getPinNumber()))
			direction = 1L;
		if (endstop_upper_ && (home_switch_->getPinNumber() == endstop_upper_->getPinNumber()))
			direction = -1L;
	}
	else
	{
		if (stepper_motor_->currentPosition() >= home_position_)
			direction = -1L;
		else
			direction = 1L;
	}

	//when the home switch is triggered the focuser is already at or near its home 
	//position and the home switch needs to be cleared first. to do this, reverse the 
	//homing direction.
	if (home_switch_->getState())
	{
		direction *= -1L;
		home_cleared_ = false;
	}

	moveTo(direction * LONG_MAX / 2);

	homing_ = true;

	return true;
}

bool Focuser::getAbsolute()
{
	return absolute_;
}

float Focuser::getTemperature(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkTempSensor(sensor))
		return NAN;

	//return the last measured temperature from the sensor.
	return temperature_sensors_[sensor]->getValue();
}

bool Focuser::getTemperatureComplete(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkTempSensor(sensor))
		return false;

	//return true if the sensor has finished the mesaurement, false otherwise.
	return temperature_sensors_[sensor]->hasValue();
}

bool Focuser::measureTemperature(uint8_t sensor)
{
	//check the given sensor number.
	if (!checkTempSensor(sensor))
		return false;

	//start a temperature measurement and return the result.
	return temperature_sensors_[sensor]->measure();
}

bool Focuser::getTempCompensation()
{
	return temp_comp_state_ != TC_DISABLED;
}

bool Focuser::setTempCompensation(bool enable)
{
	if (!checkTempSensor(temp_comp_sensor_))
		return false;

	//try to enable temperature compensation if enabling was requested. 
	if (enable)
		temp_comp_state_ = TC_INITIALIZE;

	//disable temperature compensation if disabling was requested (ignores sensor 
	//number) or an invalid sensor number was given.
	else
		temp_comp_state_ = TC_DISABLED;

	return true;
}

uint8_t Focuser::getTempCompSensor()
{
	return temp_comp_sensor_;
}

bool Focuser::setTempCompSensor(uint8_t sensor)
{
	if (!checkTempSensor(sensor))
		return false;

	temp_comp_sensor_ = sensor;
	return true;
}

float Focuser::getStepSize()
{
	return step_size_;
}

uint8_t Focuser::reportSwitches()
{
	uint8_t return_value = 0;

	//bit 1 contains the state of the lower endstop switch.
	if (endstop_lower_)
		return_value += static_cast<uint8_t>(endstop_lower_->getState());

	//bit 2 contains the state of the upper endstop switch.
	if (endstop_upper_)
		return_value += static_cast<uint8_t>(endstop_upper_->getState()) << 1;

	//bit 3 contains the state of the home switch.
	if (home_switch_)
		return_value += static_cast<uint8_t>(home_switch_->getState()) << 2;

	return return_value;
}

uint8_t Focuser::getLocked()
{
	return locked_;
}

void Focuser::setLocked(uint8_t source)
{
	switch (source)
	{
	case 0:
		locked_ = LOCK_NONE;
		break;
	case 1:
		locked_ = LOCK_LOCAL;
		break;
	case 2:
		locked_ = LOCK_REMOTE;
		break;
	default:
		break;
	}
}

void Focuser::setSpeed(Focuser::speed_t speed)
{
	speed_ = getSpeedFromEnum(speed);

	float curr_speed = stepper_motor_->speed();

	if (curr_speed != 0.0f)
		stepper_motor_->setSpeed(speed_ * curr_speed / abs(curr_speed));
}

//---------------------------------------------------------------------------------------
//private functions
uint8_t Focuser::checkEndstops()
{
	uint8_t return_value = 0;

	if (endstop_lower_ && (endstop_lower_->getState() == true))
	{
		return_value += 1;

		if (stepper_motor_->distanceToGo() <= 0)
			moveTo(stepper_motor_->currentPosition());
	}

	if (endstop_upper_ && (endstop_upper_->getState() == true))
	{
		return_value += 2;

		if (stepper_motor_->distanceToGo() >= 0)
			moveTo(stepper_motor_->currentPosition());
	}

	return return_value;
}

uint8_t Focuser::checkLimits()
{
	uint8_t return_value = 0;

	if (limit_lower_enabled_ && (stepper_motor_->currentPosition() <= -max_step_))
	{
		return_value += 1;

		if (stepper_motor_->distanceToGo() >= 0)
			moveTo(-max_step_);
	}

	if (limit_upper_enabled_ && (stepper_motor_->currentPosition() >= max_step_))
	{
		return_value += 2;

		if (stepper_motor_->distanceToGo() <= 0)
			moveTo(max_step_);
	}

	return return_value;
}

float Focuser::stepsToMicrons(int32_t steps)
{
	return static_cast<float>(steps)* step_size_;
}

int32_t Focuser::micronsToSteps(float microns)
{
	return static_cast<int32_t>(microns / step_size_);
}

bool Focuser::checkTempSensor(uint8_t sensor)
{
	//check the sensor number
	if (sensor > TEMP_SENSORS)
		return false;

	//check if the sensor was intialized
	if (!temperature_sensors_[sensor])
		return false;

	return true;
}

void Focuser::clearHome()
{
	//when the home switch is no longer triggered the home position has been cleared.
	if (!home_switch_->getState())
	{
		//invert direction of the stepper.
		moveTo(-1 * stepper_motor_->targetPosition());

		home_cleared_ = true;
	}
}

void Focuser::home()
{
	//find home timeout check
	if ((home_timeout_ != 0L) && (millis() - home_start_time_ > home_timeout_))
	{
		homing_ = false;
		homing_error_ = Focuser::ERROR_HOMING_TIMEOUT;
		halt();
		return;
	}

	if (!home_cleared_)
		clearHome();

	else if (!home_found_)
	{
		int32_t location = stepper_motor_->currentPosition();
		bool home_triggered = home_switch_->getState();

		//if the home switch is an endstop switch:
		if (endstop_home_switch_)
		{
			if (home_triggered)
			{
				//the home position has been found. set the current position as the new 
				//destination for the stepper.
				moveTo(location);

				home_found_ = true;
			}
		}

		//if the home switch is not an endstop switch, the home position is the position 
		//between the two positions where the switch changes its state.
		else
		{
			//when the home switch is triggered and the first position where the switch is 
			//pressed has not yet been found:
			if (home_triggered && !home_first_pos_found_)
			{
				home_first_pos_found_ = true;
				home_first_pos_ = location;
			}

			//when the switch is released and the first position where the switch is pressed 
			//has been found before:
			if (!home_triggered && home_first_pos_found_)
			{
				//calculate center position and set new stepper targets.
				moveTo((home_first_pos_ + location) / 2);

				home_found_ = true;
			}
		}
	}

	//do nothing while the focuser is moving towards its home position.
	else if (isMoving())
	{

	}

	//if the focuser has stopped moving after finding the home position synchronize
	//the home position and disable homing.
	else if (!isMoving())
	{
		stepper_motor_->setCurrentPosition(home_position_);
		homing_ = false;
	}
}

void Focuser::moveTo(int32_t target)
{
	stepper_motor_->moveTo(target);

	float sign = (target - stepper_motor_->currentPosition()) >= 0 ? 1.0f : -1.0f;

	stepper_motor_->setSpeed(sign * speed_);
}

float Focuser::getSpeedFromEnum(Focuser::speed_t speed)
{
	float max_speed = 1.0f;

	//read max speed from EEPROM.
	readEEPROM(EEPROMHandler::focuser_variable_::MAX_SPEED, max_speed);

	switch (speed)
	{
	case Focuser::SPEED_SLOWEST:
		max_speed *= 0.001f;
		break;
	case Focuser::SPEED_SLOW:
		max_speed *= 0.01f;
		break;
	case Focuser::SPEED_FAST:
		max_speed *= 0.1f;
		break;
	default:
		//case Focuser::SPEED_FASTEST:
		break;
	}

	return max_speed;
}

//uint8_t Focuser::getStepType()
//{
//	return stepper_motor_->getStepType();
//}
//
//bool Focuser::setStepType(uint8_t step_type)
//{
//	int32_t microstep_before = stepper_motor_->getMicroStepFactor();
//
//	if (!stepper_motor_->setStepType(step_type))
//		return false;
//
//	int32_t microstep_after = stepper_motor_->getMicroStepFactor();
//
//	stepper_motor_->setCurrentPosition(stepper_motor_->currentPosition() * microstep_after / microstep_before);
//	max_step_ = max_step_ *  microstep_after / microstep_before;
//
//	return true;
//}

void Focuser::temperatureCompensation()
{
	switch (temp_comp_state_)
	{
	case TC_DISABLED:
		//do nothing if temperature compensation is not enabled.
		break;
	case TC_INITIALIZE:
	{
		//do nothing if the initial temperature measurement is not yet finished.
		if (!getTemperatureComplete(temp_comp_sensor_))
			return;

		temp_comp_ref_pos_ = stepper_motor_->currentPosition();
		temp_comp_ref_temp_ = temperature_sensors_[temp_comp_sensor_]->getValue();

		temp_comp_last_ = millis();
		temp_comp_state_ = TC_READY;
	}
	break;

	//
	case TC_READY:
	{
		//do nothing until temperature compensation interval has passed.
		if (!(millis() - temp_comp_last_ > temp_comp_interval_))
			return;

		measureTemperature(temp_comp_sensor_);

		temp_comp_state_ = TC_MEASURE;
	}
	break;

	case TC_MEASURE:
	{
		//do nothing if the temperature measurement is not yet finished.
		if (!getTemperatureComplete(temp_comp_sensor_))
			return;

		//calculate temperature difference between reference temperature and measured
		//temperature.
		float delta_temp = getTemperature(temp_comp_sensor_) - temp_comp_ref_temp_;

		if (delta_temp != 0.0f)
		{
			//calculate number of adjustment steps
			int32_t steps_adj = micronsToSteps(temp_comp_factor_ * delta_temp);

			//move focuser to new position.
			moveTo(temp_comp_ref_pos_ + steps_adj);
		}

		temp_comp_last_ = millis();
		temp_comp_state_ = TC_READY;
	}
	break;

	case TC_WAIT_MOVEMENT:
	{
		//do nothing as long as the focuser is moving.
		if (isMoving())
			return;

		//reinitialize temperature compensation once the focuser movement has stopped.
		temp_comp_state_ = TC_INITIALIZE;
	}
	break;
	}
}

void Focuser::writeDefaults()
{
	writeEEPROM(EEPROMHandler::focuser_variable_::MAX_SPEED, DEF_F_MAX_SPEED);

	writeEEPROM(EEPROMHandler::focuser_variable_::GEAR_RATIO, DEF_F_GEAR_RATIO);
	writeEEPROM(EEPROMHandler::focuser_variable_::NUMBER_OF_STEPS, static_cast<uint16_t>(DEF_F_NUMBER_OF_STEPS));

	writeEEPROM(EEPROMHandler::focuser_variable_::STEP_TYPE, static_cast<uint8_t>(DEF_F_STEP_TYPE));

	writeEEPROM(EEPROMHandler::focuser_variable_::HOME_SWITCH_ENABLED, static_cast<uint8_t>(DEF_F_HOME_SWITCH_ENABLED));
	writeEEPROM(EEPROMHandler::focuser_variable_::HOME_SWITCH, static_cast<uint8_t>(DEF_F_HOME_SWITCH_PIN));
	writeEEPROM(EEPROMHandler::focuser_variable_::HOME_SWITCH_INVERTED, static_cast<uint8_t>(DEF_F_HOME_SWITCH_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::HOME_POSITION, static_cast<int32_t>(DEF_F_HOME_POS));

	writeEEPROM(EEPROMHandler::focuser_variable_::STARTUP_AUTO_HOME, static_cast<uint8_t>(DEF_F_STARTUP_AUTO_HOME));
	writeEEPROM(EEPROMHandler::focuser_variable_::AUTOMATIC_STEPPER_RELEASE, static_cast<uint8_t>(DEF_F_AUTO_STEPPER_RELEASE));

	writeEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_LOWER_ENABLED, static_cast<uint8_t>(DEF_F_ENDSTOP_LOWER_ENABLED));
	writeEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_UPPER_ENABLED, static_cast<uint8_t>(DEF_F_ENDSTOP_UPPER_ENABLED));
	writeEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_LOWER, static_cast<uint8_t>(DEF_F_ENDSTOP_LOWER_PIN));
	writeEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_UPPER, static_cast<uint8_t>(DEF_F_ENDSTOP_UPPER_PIN));
	writeEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_LOWER_INVERTED, static_cast<uint8_t>(DEF_F_ENDSTOP_LOWER_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::ENDSTOP_UPPER_INVERTED, static_cast<uint8_t>(DEF_F_ENDSTOP_UPPER_INVERTED));

	writeEEPROM(EEPROMHandler::focuser_variable_::LIMIT_LOWER_ENABLED, static_cast<uint8_t>(DEF_F_LIMIT_LOWER_ENDBLED));
	writeEEPROM(EEPROMHandler::focuser_variable_::LIMIT_UPPER_ENABLED, static_cast<uint8_t>(DEF_F_LIMIT_UPPER_ENBALED));
	//writeEEPROM(EEPROMHandler::focuser_variable_::LIMIT_LOWER, static_cast<uint32_t>(DEF_F_LIMIT_LOWER));
	//writeEEPROM(EEPROMHandler::focuser_variable_::LIMIT_UPPER, static_cast<int32_t>(DEF_F_LIMIT_UPPER));

	writeEEPROM(EEPROMHandler::focuser_variable_::MOTOR_DRIVER_TYPE, static_cast<uint8_t>(DEF_F_MOTOR_DRIVER_TYPE));

	writeEEPROM(EEPROMHandler::focuser_variable_::I2C_ADDRESS, static_cast<uint8_t>(DEF_F_I2C_ADDRESS));
	writeEEPROM(EEPROMHandler::focuser_variable_::TERMINAL, static_cast<uint8_t>(DEF_F_TERMINAL));

	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_1, static_cast<uint8_t>(DEF_F_PIN_1));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_2, static_cast<uint8_t>(DEF_F_PIN_2));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_3, static_cast<uint8_t>(DEF_F_PIN_3));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_4, static_cast<uint8_t>(DEF_F_PIN_4));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_5, static_cast<uint8_t>(DEF_F_PIN_5));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_6, static_cast<uint8_t>(DEF_F_PIN_6));

	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_1_INVERTED, static_cast<uint8_t>(DEF_F_PIN_1_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_2_INVERTED, static_cast<uint8_t>(DEF_F_PIN_2_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_3_INVERTED, static_cast<uint8_t>(DEF_F_PIN_3_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_4_INVERTED, static_cast<uint8_t>(DEF_F_PIN_4_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_5_INVERTED, static_cast<uint8_t>(DEF_F_PIN_5_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::PIN_6_INVERTED, static_cast<uint8_t>(DEF_F_PIN_6_INVERTED));

	writeEEPROM(EEPROMHandler::focuser_variable_::POSITION_ABSOLUTE, static_cast<uint8_t>(DEF_F_POSITION_ABSOLUTE));
	writeEEPROM(EEPROMHandler::focuser_variable_::MICRONS_PER_REV, DEF_F_MICRONS_PER_REV);
	writeEEPROM(EEPROMHandler::focuser_variable_::MAX_INCREMENT, static_cast<int32_t>(DEF_F_MAX_INCREMENT));
	writeEEPROM(EEPROMHandler::focuser_variable_::MAX_STEP, static_cast<int32_t>(DEF_F_MAX_STEP));

	writeEEPROM(EEPROMHandler::focuser_variable_::HOME_TIMEOUT, static_cast<uint32_t>(DEF_F_HOME_TIMEOUT));

	writeEEPROM(EEPROMHandler::focuser_variable_::TC_AVAILABLE, static_cast<uint8_t>(DEF_F_TC_AVAILABLE));
	writeEEPROM(EEPROMHandler::focuser_variable_::TC_SENSOR, static_cast<uint8_t>(DEF_F_TC_SENSOR));
	writeEEPROM(EEPROMHandler::focuser_variable_::TC_FACTOR, DEF_F_TC_FACTOR);

	writeEEPROM(EEPROMHandler::focuser_variable_::MC_AVAILABLE, static_cast<uint8_t>(DEF_F_MC_AVAILABLE));
	writeEEPROM(EEPROMHandler::focuser_variable_::MC_BTN_LOCK_AVAILABLE, static_cast<uint8_t>(DEF_F_MC_BTN_LOCK_AVAILABLE));
	writeEEPROM(EEPROMHandler::focuser_variable_::MC_BTN_LOCK, static_cast<uint8_t>(DEF_F_MC_BTL_LOCK));
	writeEEPROM(EEPROMHandler::focuser_variable_::MC_BTN_LOCK_INVERTED, static_cast<uint8_t>(DEF_F_MC_BTN_LOCK_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::MC_BTN_IN, static_cast<uint8_t>(DEF_F_MC_BTN_IN));
	writeEEPROM(EEPROMHandler::focuser_variable_::MC_BTN_IN_INVERTED, static_cast<uint8_t>(DEF_F_MC_BTN_IN_INVERTED));
	writeEEPROM(EEPROMHandler::focuser_variable_::MC_BTN_OUT, static_cast<uint8_t>(DEF_F_MC_BTN_OUT));
	writeEEPROM(EEPROMHandler::focuser_variable_::MC_BTN_OUT_INVERTED, static_cast<uint8_t>(DEF_F_MC_BTN_OUT_INVERTED));

	SensorTemperature::SensorSettings sensor_settings = {
		DEF_F_TEMPERATURE_SENSOR_TYPE,
		DEF_F_TEMPERATURE_SENSOR_DEVICE,
		DEF_F_TEMPERATURE_SENSOR_UNIT,
		DEF_F_TEMPERATURE_SENSOR_PARAMETERS
	};

	for (uint8_t i = 0; i < TEMP_SENSORS; i++)
		writeEEPROM(EEPROMHandler::focuser_variable_::TEMPERATURE_SENSOR, sensor_settings, i);
}

