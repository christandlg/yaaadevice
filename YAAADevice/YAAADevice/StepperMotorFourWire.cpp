//Four Wire Stepper motor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "StepperMotorFourWire.h"

StepperMotorFourWire::StepperMotorFourWire(uint8_t pin1, uint8_t pin2, uint8_t pin3, uint8_t pin4, bool enable) :
StepperMotor(pin1, pin2, pin3, pin4, enable)
{
	step_type_ = STEPTYPE_FULL;
}

StepperMotorFourWire::~StepperMotorFourWire()
{
}

//uint8_t StepperMotorFourWire::getStepType()
//{
//	//4 wire steppers only support the "full" step type.
//	return STEPTYPE_FULL;
//}

bool StepperMotorFourWire::setStepType(uint8_t step_type)
{

	//only STEPTPYE_FULL is available for four wire steppers.
	if (step_type == StepperMotor::STEPTYPE_FULL)
		return true;

	return false;
}

