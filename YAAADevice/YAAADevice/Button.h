//simple button class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef BUTTON_H_
#define BUTTON_H_

#include <Arduino.h>

#define DEBOUNCING_DELAY 50		//software debouncing delay in ms
#define DOUBLEPRESS_INTERVAL 200
#define LONGPRESS_INTERVAL 1000

class Button
{
public:

	struct ButtonSettings
	{
		uint8_t pin_nr_;
		bool inverted_;
	};

	enum event_t {
		EVENT_NONE = 0,
		EVENT_PRESS = 1,
		EVENT_RELEASE = 2,
		//EVENT_DOUBLEPRESS = 3,
		EVENT_HOLD_SHORT = 4,		//button is being held down shorter than LONGPRESS_INTERVAL ms
		EVENT_HOLD_LONG = 5			//button is being held down longer than LONGPRESS_INTERVAL ms
	};

	//constructor
	//@param: 
	// - the number of the input pin. 
	// - optional: if the button logic is inverted. 
	// - - inverted = false: input pin is pulled LOW on button press.
	// - - inverted = true: input pin is pulled HIGH on button press.
	Button(uint8_t pinNr, bool invert = false);
	Button(ButtonSettings settings);

	//returns if (and how) button state has changed since the last call of getEvent().
	//@return: button event as Button::event_t.
	uint8_t getEvent();

	//returns the current button state. no software debouncing.
	//@return: 
	//true - button triggered.
	//false - button not triggered.
	bool getState();

	//@return: the Buttons digital input pin number.
	uint8_t getPinNumber();

	//@return: if the Buttons logic is inverted or not.
	bool getInverted();

private:
	uint8_t pin_number_;
	bool inverted_;			//true if the button logic is inverted.

	bool current_state_;
	bool previous_state_;

	//time (in milliseconds) since button state was read the last time
	uint32_t last_read_;

	//time (in milliseconds) since the button was last pressed.
	uint32_t last_press_;
	uint32_t press_start_;
};

#endif /* BUTTON_H_ */

