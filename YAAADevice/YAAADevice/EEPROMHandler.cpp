//EEPROMHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "EEPROMHandler.h"

#ifdef ARDUINO_SAM_DUE
DueFlashStorage EEPROMHandler::dfs_;
#endif

EEPROMHandler::EEPROMHandler()
{
}

EEPROMHandler::~EEPROMHandler()
{
}

bool EEPROMHandler::begin()
{
#if defined(ESP8266) || defined(ESP32)
	EEPROM.begin(4096);
#endif
	return true;
}

uint8_t EEPROMHandler::getVariableType(uint16_t device, uint16_t variable)
{
	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		switch (variable)
		{
		case EEPROMHandler::general_variable_::CAMERA_ROTATOR_AVAILABLE:
		case EEPROMHandler::general_variable_::DOME_AVAILABLE:
		case EEPROMHandler::general_variable_::FILTER_WHEEL_AVAILABLE:
		case EEPROMHandler::general_variable_::FOCUSER_AVAILABLE:
		case EEPROMHandler::general_variable_::SAFETY_MONITOR_AVAILABLE:
		case EEPROMHandler::general_variable_::TELESCOPE_AVAILABLE:
		case EEPROMHandler::general_variable_::DISPLAY_TYPE:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_TYPE:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN0:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN1:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN2:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN3:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN4:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN5:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN6:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN7:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN8:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN9:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN10:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN11:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN12:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN13:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN14:
		case EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN15:
		case EEPROMHandler::general_variable_::ETHERNET_ENABLED:
		case EEPROMHandler::general_variable_::ETHERNET_DHCP:
		case EEPROMHandler::general_variable_::ONEWIRE_PIN:
		case EEPROMHandler::general_variable_::SD_CS_PIN:
		case EEPROMHandler::general_variable_::SD_LOG:
		case EEPROMHandler::general_variable_::I2C_PIN_SCL:
		case EEPROMHandler::general_variable_::I2C_PIN_SDA:
		case EEPROMHandler::general_variable_::WIFI_ENABLED:
		case EEPROMHandler::general_variable_::WIFI_DHCP:
		case EEPROMHandler::general_variable_::WIFI_CHANNEL:
		case EEPROMHandler::general_variable_::WIFI_SSID_LENGTH:
		case EEPROMHandler::general_variable_::WIFI_PASSWORD_LENGTH:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME_LENGTH:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_MODE:
		case EEPROMHandler::general_variable_::OTA_HOSTNAME_LENGTH:
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD_HASH:
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD_LENGTH:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH_LENGTH:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME_LENGTH:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD_LENGTH:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS_LENGTH:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH_LENGTH:
#endif /* defined(ESP8266) || defined(ESP32) */
			return EEPROMHandler::UINT8_T_;

		case EEPROMHandler::general_variable_::ETHERNET_PORT:
		case EEPROMHandler::general_variable_::WIFI_PORT:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PORT:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PORT:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PORT:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::DISPLAY_RES_X:
		case EEPROMHandler::general_variable_::DISPLAY_RES_Y:
			return EEPROMHandler::UINT16_T_;

		case EEPROMHandler::general_variable_::EEPROM_CONTROL:
			return EEPROMHandler::INT32_T_;

		case EEPROMHandler::general_variable_::SERIAL_BAUD_RATE:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_UPDATE_INTERVAL:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::DISPLAY_UPDATE_INTERVAL:
			return EEPROMHandler::UINT32_T_;

		case EEPROMHandler::general_variable_::TIME_UTC_OFFSET:
			return EEPROMHandler::FLOAT_;

		case EEPROMHandler::general_variable_::ETHERNET_MAC:
		case EEPROMHandler::general_variable_::ETHERNET_IP:
		case EEPROMHandler::general_variable_::ETHERNET_DNS:
		case EEPROMHandler::general_variable_::ETHERNET_GATEWAY:
		case EEPROMHandler::general_variable_::ETHERNET_SUBNET:
		case EEPROMHandler::general_variable_::WIFI_MAC:
		case EEPROMHandler::general_variable_::WIFI_IP:
		case EEPROMHandler::general_variable_::WIFI_DNS:
		case EEPROMHandler::general_variable_::WIFI_GATEWAY:
		case EEPROMHandler::general_variable_::WIFI_SUBNET:
			return EEPROMHandler::ARRAY_;

		case EEPROMHandler::general_variable_::WIFI_SSID:
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HOSTNAME:
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH:
#endif /* defined(ESP8266) || defined(ESP32) */
			return EEPROMHandler::STRING_;

		case EEPROMHandler::general_variable_::INPUT_INFODISPLAY_KEYPAD:
			return EEPROMHandler::KEYPAD_INFO_;
		}
	}

	//general telescope settings.
	if (device == EEPROMHandler::device_type_::TELESCOPE)
	{
		switch (variable)
		{
		case EEPROMHandler::telescope_variable_::STARTUP_AUTO_HOME:
		case EEPROMHandler::telescope_variable_::PARK_ENABLED:
		case EEPROMHandler::telescope_variable_::MOUNT_TYPE:
		case EEPROMHandler::telescope_variable_::MC_AVAILABLE:
		case EEPROMHandler::telescope_variable_::MC_BTN_TOGGLE_AVAILABLE:
		case EEPROMHandler::telescope_variable_::MC_BTN_SPEED_AVAILABLE:
		case EEPROMHandler::telescope_variable_::MC_JST_AVAILABLE:
		case EEPROMHandler::telescope_variable_::MC_BTN_TOGGLE:
		case EEPROMHandler::telescope_variable_::MC_BTN_SPEED:
		case EEPROMHandler::telescope_variable_::MC_JST_INPUT_X:
		case EEPROMHandler::telescope_variable_::MC_JST_INPUT_Y:
		case EEPROMHandler::telescope_variable_::MC_JST_DEADZONE:
			return EEPROMHandler::UINT8_T_;

		case EEPROMHandler::telescope_variable_::SITE_LATITUDE:
		case EEPROMHandler::telescope_variable_::SITE_LONGITUDE:
		case EEPROMHandler::telescope_variable_::SITE_ELEVATION:
		case EEPROMHandler::telescope_variable_::FOCAL_LENGTH:
		case EEPROMHandler::telescope_variable_::APERTURE_DIAMETER:
		case EEPROMHandler::telescope_variable_::APERTURE_AREA:
			return EEPROMHandler::FLOAT_;

		case EEPROMHandler::telescope_variable_::TEMPERATURE_SENSOR:
			return EEPROMHandler::SENSOR_SETTINGS_;
		}
	}

	//axis specific telescope settings.
	if (device == EEPROMHandler::device_type_::TELESCOPE_AXIS)
	{
		switch (variable)
		{
		case EEPROMHandler::telescope_axis_variable_::STEPTYPE_FAST:
		case EEPROMHandler::telescope_axis_variable_::STEPTYPE_SLOW:
		case EEPROMHandler::telescope_axis_variable_::HOME_SWITCH_ENABLED:
		case EEPROMHandler::telescope_axis_variable_::HOME_SWITCH:
		case EEPROMHandler::telescope_axis_variable_::HOME_SWITCH_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::HOME_ALIGNMENT_SYNC:
		case EEPROMHandler::telescope_axis_variable_::AUTOMATIC_STEPPER_RELEASE:
		case EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER_ENABLED:
		case EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER_ENABLED:
		case EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER:
		case EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER:
		case EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::ENDSTOP_HARD_STOP:
		case EEPROMHandler::telescope_axis_variable_::LIMIT_LOWER_ENABLED:
		case EEPROMHandler::telescope_axis_variable_::LIMIT_UPPER_ENABLED:
		case EEPROMHandler::telescope_axis_variable_::LIMIT_HARD_STOP:
		case EEPROMHandler::telescope_axis_variable_::LIMIT_HANDLING:
		case EEPROMHandler::telescope_axis_variable_::MOTOR_DRIVER_TYPE:
		case EEPROMHandler::telescope_axis_variable_::I2C_ADDRESS:
		case EEPROMHandler::telescope_axis_variable_::TERMINAL:
		case EEPROMHandler::telescope_axis_variable_::PIN_1:
		case EEPROMHandler::telescope_axis_variable_::PIN_2:
		case EEPROMHandler::telescope_axis_variable_::PIN_3:
		case EEPROMHandler::telescope_axis_variable_::PIN_4:
		case EEPROMHandler::telescope_axis_variable_::PIN_5:
		case EEPROMHandler::telescope_axis_variable_::PIN_6:
		case EEPROMHandler::telescope_axis_variable_::PIN_1_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::PIN_2_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::PIN_3_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::PIN_4_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::PIN_5_INVERTED:
		case EEPROMHandler::telescope_axis_variable_::PIN_6_INVERTED:
			return EEPROMHandler::UINT8_T_;

		case EEPROMHandler::telescope_axis_variable_::NUMBER_OF_STEPS:
			return EEPROMHandler::UINT16_T_;

		case EEPROMHandler::telescope_axis_variable_::HOMING_DIRECTION:
			return EEPROMHandler::INT16_T_;

		case EEPROMHandler::telescope_axis_variable_::HOME_TIMEOUT:
			return EEPROMHandler::UINT32_T_;

		case EEPROMHandler::telescope_axis_variable_::MAX_SPEED:
		case EEPROMHandler::telescope_axis_variable_::ACCELERATION:
		case EEPROMHandler::telescope_axis_variable_::GEAR_RATIO:
		case EEPROMHandler::telescope_axis_variable_::HOME_ALIGNMENT:
		case EEPROMHandler::telescope_axis_variable_::PARK_POS:
		case EEPROMHandler::telescope_axis_variable_::LIMIT_LOWER:
		case EEPROMHandler::telescope_axis_variable_::LIMIT_UPPER:
			return EEPROMHandler::FLOAT_;
		}
	}

	//filter wheel settings.
	if (device == EEPROMHandler::device_type_::FILTER_WHEEL)
	{
		switch (variable)
		{
		case EEPROMHandler::filter_wheel_variable_::STEPTYPE:
		case EEPROMHandler::filter_wheel_variable_::HOME_SWITCH_ENABLED:
		case EEPROMHandler::filter_wheel_variable_::HOME_SWITCH:
		case EEPROMHandler::filter_wheel_variable_::HOME_SWITCH_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::STARTUP_AUTO_HOME:
		case EEPROMHandler::filter_wheel_variable_::AUTOMATIC_STEPPER_RELEASE:
		case EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER_ENABLED:
		case EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER_ENABLED:
		case EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER:
		case EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER:
		case EEPROMHandler::filter_wheel_variable_::ENDSTOP_LOWER_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::ENDSTOP_UPPER_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::ENDSTOP_HARD_STOP:
		case EEPROMHandler::filter_wheel_variable_::LIMIT_LOWER_ENABLED:
		case EEPROMHandler::filter_wheel_variable_::LIMIT_UPPER_ENABLED:
		case EEPROMHandler::filter_wheel_variable_::LIMIT_HARD_STOP:
			//case EEPROMHandler::filter_wheel_variable_::LIMIT_HANDLING :
		case EEPROMHandler::filter_wheel_variable_::MOTOR_DRIVER_TYPE:
		case EEPROMHandler::filter_wheel_variable_::I2C_ADDRESS:
		case EEPROMHandler::filter_wheel_variable_::TERMINAL:
		case EEPROMHandler::filter_wheel_variable_::PIN_1:
		case EEPROMHandler::filter_wheel_variable_::PIN_2:
		case EEPROMHandler::filter_wheel_variable_::PIN_3:
		case EEPROMHandler::filter_wheel_variable_::PIN_4:
		case EEPROMHandler::filter_wheel_variable_::PIN_5:
		case EEPROMHandler::filter_wheel_variable_::PIN_6:
		case EEPROMHandler::filter_wheel_variable_::PIN_1_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::PIN_2_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::PIN_3_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::PIN_4_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::PIN_5_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::PIN_6_INVERTED:
		case EEPROMHandler::filter_wheel_variable_::NUMBER_OF_FILTERS:
		case EEPROMHandler::filter_wheel_variable_::FILTER_HOLDER_TYPE:
		case EEPROMHandler::filter_wheel_variable_::MC_AVAILABLE:
		case EEPROMHandler::filter_wheel_variable_::MC_BTN_LOCK_AVAILABLE:
		case EEPROMHandler::filter_wheel_variable_::MC_BTN_NEXT_AVAILABLE:
		case EEPROMHandler::filter_wheel_variable_::MC_BTN_PREVIOUS_AVAILABLE:
		case EEPROMHandler::filter_wheel_variable_::MC_BTN_LOCK:
		case EEPROMHandler::filter_wheel_variable_::MC_BTN_NEXT:
		case EEPROMHandler::filter_wheel_variable_::MC_BTN_PREVIOUS:
			//case EEPROMHandler::filter_wheel_variable_::MC_BTN_LOCK_INVERTED:
			//case EEPROMHandler::filter_wheel_variable_::MC_BTN_NEXT_INVERTED:
			//case EEPROMHandler::filter_wheel_variable_::MC_BTN_PREVIOUS_INVERTED:

			return EEPROMHandler::UINT8_T_;

		case EEPROMHandler::filter_wheel_variable_::NUMBER_OF_STEPS:
			return EEPROMHandler::UINT16_T_;

			//case EEPROMHandler::filter_wheel_variable_::FOCUS_OFFSET:
		case EEPROMHandler::filter_wheel_variable_::HOME_OFFSET:
			return EEPROMHandler::INT32_T_;

		case EEPROMHandler::filter_wheel_variable_::HOME_TIMEOUT:
			return EEPROMHandler::UINT32_T_;

		case EEPROMHandler::filter_wheel_variable_::MAX_SPEED:
		case EEPROMHandler::filter_wheel_variable_::ACCELERATION:
		case EEPROMHandler::filter_wheel_variable_::GEAR_RATIO:
			//case EEPROMHandler::filter_wheel_variable_::PARK_POS :
		case EEPROMHandler::filter_wheel_variable_::LIMIT_LOWER:
		case EEPROMHandler::filter_wheel_variable_::LIMIT_UPPER:
			return EEPROMHandler::FLOAT_;

		case EEPROMHandler::filter_wheel_variable_::TEMPERATURE_SENSOR:
			return EEPROMHandler::SENSOR_SETTINGS_;

		case EEPROMHandler::filter_wheel_variable_::FILTER_INFO:
			return EEPROMHandler::FILTER_INFO_;
		}
	}

	//focuser settings.
	if (device == EEPROMHandler::device_type_::FOCUSER)
	{
		switch (variable)
		{
		case EEPROMHandler::focuser_variable_::POSITION_ABSOLUTE:
		case EEPROMHandler::focuser_variable_::TC_AVAILABLE:
		case EEPROMHandler::focuser_variable_::TC_SENSOR:
		case EEPROMHandler::focuser_variable_::STEP_TYPE:
			//case EEPROMHandler::focuser_variable_::STEP_TYPE_TEMP_COMP:
		case EEPROMHandler::focuser_variable_::HOME_SWITCH_ENABLED:
		case EEPROMHandler::focuser_variable_::HOME_SWITCH:
		case EEPROMHandler::focuser_variable_::HOME_SWITCH_INVERTED:
		case EEPROMHandler::focuser_variable_::AUTOMATIC_STEPPER_RELEASE:
		case EEPROMHandler::focuser_variable_::STARTUP_AUTO_HOME:
		case EEPROMHandler::focuser_variable_::ENDSTOP_LOWER_ENABLED:
		case EEPROMHandler::focuser_variable_::ENDSTOP_UPPER_ENABLED:
		case EEPROMHandler::focuser_variable_::ENDSTOP_LOWER:
		case EEPROMHandler::focuser_variable_::ENDSTOP_UPPER:
		case EEPROMHandler::focuser_variable_::ENDSTOP_LOWER_INVERTED:
		case EEPROMHandler::focuser_variable_::ENDSTOP_UPPER_INVERTED:
			//case EEPROMHandler::focuser_variable_::ENDSTOP_HARD_STOP:
		case EEPROMHandler::focuser_variable_::LIMIT_LOWER_ENABLED:
		case EEPROMHandler::focuser_variable_::LIMIT_UPPER_ENABLED:
			//case EEPROMHandler::focuser_variable_::LIMIT_HARD_STOP:
				//case EEPROMHandler::filter_wheel_variable_::LIMIT_HANDLING :
		case EEPROMHandler::focuser_variable_::MOTOR_DRIVER_TYPE:
		case EEPROMHandler::focuser_variable_::I2C_ADDRESS:
		case EEPROMHandler::focuser_variable_::TERMINAL:
		case EEPROMHandler::focuser_variable_::PIN_1:
		case EEPROMHandler::focuser_variable_::PIN_2:
		case EEPROMHandler::focuser_variable_::PIN_3:
		case EEPROMHandler::focuser_variable_::PIN_4:
		case EEPROMHandler::focuser_variable_::PIN_5:
		case EEPROMHandler::focuser_variable_::PIN_6:
		case EEPROMHandler::focuser_variable_::PIN_1_INVERTED:
		case EEPROMHandler::focuser_variable_::PIN_2_INVERTED:
		case EEPROMHandler::focuser_variable_::PIN_3_INVERTED:
		case EEPROMHandler::focuser_variable_::PIN_4_INVERTED:
		case EEPROMHandler::focuser_variable_::PIN_5_INVERTED:
		case EEPROMHandler::focuser_variable_::PIN_6_INVERTED:
		case EEPROMHandler::focuser_variable_::MC_AVAILABLE:
		case EEPROMHandler::focuser_variable_::MC_BTN_LOCK_AVAILABLE:
		case EEPROMHandler::focuser_variable_::MC_BTN_LOCK:
		case EEPROMHandler::focuser_variable_::MC_BTN_IN:
		case EEPROMHandler::focuser_variable_::MC_BTN_OUT:
		case EEPROMHandler::focuser_variable_::MC_BTN_LOCK_INVERTED:
		case EEPROMHandler::focuser_variable_::MC_BTN_IN_INVERTED:
		case EEPROMHandler::focuser_variable_::MC_BTN_OUT_INVERTED:
			return EEPROMHandler::UINT8_T_;

		case EEPROMHandler::focuser_variable_::NUMBER_OF_STEPS:
			return EEPROMHandler::UINT16_T_;

		case EEPROMHandler::focuser_variable_::LIMIT_LOWER:
		case EEPROMHandler::focuser_variable_::LIMIT_UPPER:
		case EEPROMHandler::focuser_variable_::MAX_INCREMENT:
		case EEPROMHandler::focuser_variable_::MAX_STEP:
		case EEPROMHandler::focuser_variable_::HOME_POSITION:
			return EEPROMHandler::INT32_T_;

		case EEPROMHandler::focuser_variable_::HOME_TIMEOUT:
			return EEPROMHandler::UINT32_T_;

		case EEPROMHandler::focuser_variable_::MAX_SPEED:
			//case EEPROMHandler::focuser_variable_::ACCELERATION:
		case EEPROMHandler::focuser_variable_::GEAR_RATIO:
		case EEPROMHandler::focuser_variable_::MICRONS_PER_REV:
		case EEPROMHandler::focuser_variable_::TC_FACTOR:
			return EEPROMHandler::FLOAT_;

		case EEPROMHandler::focuser_variable_::TEMPERATURE_SENSOR:
			return EEPROMHandler::SENSOR_SETTINGS_;
		}
	}

	return EEPROMHandler::UNKNOWN_;
}

uint16_t EEPROMHandler::stringToVariable(uint16_t device, char* string)
{
	uint16_t return_value = -1;

	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		if (strcmp_P(string, PSTR("EEPROM_CONTROL")) == 0)
			return_value = general_variable_::EEPROM_CONTROL;

		if (strcmp_P(string, PSTR("SERIAL_BAUD_RATE")) == 0)
			return_value = general_variable_::SERIAL_BAUD_RATE;

		if (strcmp_P(string, PSTR("CAMERA_ROTATOR_AVAILABLE")) == 0)
			return_value = general_variable_::CAMERA_ROTATOR_AVAILABLE;

		if (strcmp_P(string, PSTR("DOME_AVAILABLE")) == 0)
			return_value = general_variable_::DOME_AVAILABLE;

		if (strcmp_P(string, PSTR("FILTER_WHEEL_AVAILABLE")) == 0)
			return_value = general_variable_::FILTER_WHEEL_AVAILABLE;

		if (strcmp_P(string, PSTR("FOCUSER_AVAILABLE")) == 0)
			return_value = general_variable_::FOCUSER_AVAILABLE;

		if (strcmp_P(string, PSTR("SAFETY_MONITOR_AVAILABLE")) == 0)
			return_value = general_variable_::SAFETY_MONITOR_AVAILABLE;

		if (strcmp_P(string, PSTR("TELESCOPE_AVAILABLE")) == 0)
			return_value = general_variable_::TELESCOPE_AVAILABLE;

		if (strcmp_P(string, PSTR("DISPLAY_TYPE")) == 0)
			return_value = general_variable_::DISPLAY_TYPE;

		if (strcmp_P(string, PSTR("DISPLAY_RES_X")) == 0)
			return_value = general_variable_::DISPLAY_RES_X;

		if (strcmp_P(string, PSTR("DISPLAY_RES_Y")) == 0)
			return_value = general_variable_::DISPLAY_RES_Y;

		if (strcmp_P(string, PSTR("DISPLAY_UPDATE_INTERVAL")) == 0)
			return_value = general_variable_::DISPLAY_UPDATE_INTERVAL;

		if (strcmp_P(string, PSTR("DISPLAY_BRIGHTNESS")) == 0)
			return_value = general_variable_::DISPLAY_BRIGHTNESS;

		if (strcmp_P(string, PSTR("DISPLAY_CONTRAST")) == 0)
			return_value = general_variable_::DISPLAY_CONTRAST;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_TYPE")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_TYPE;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN0")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN0;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN1")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN1;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN2")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN2;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN3")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN3;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN4")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN4;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN5")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN5;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN6")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN6;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN7")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN7;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN8")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN8;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN9")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN9;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN10")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN10;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN11")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN11;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN12")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN12;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN13")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN13;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN14")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN14;

		if (strcmp_P(string, PSTR("DISPLAY_INTERFACE_PIN15")) == 0)
			return_value = general_variable_::DISPLAY_INTERFACE_PIN15;

		if (strcmp_P(string, PSTR("ETHERNET_ENABLED")) == 0)
			return_value = general_variable_::ETHERNET_ENABLED;

		if (strcmp_P(string, PSTR("ETHERNET_DHCP")) == 0)
			return_value = general_variable_::ETHERNET_DHCP;

		if (strcmp_P(string, PSTR("ETHERNET_MAC")) == 0)
			return_value = general_variable_::ETHERNET_MAC;

		if (strcmp_P(string, PSTR("ETHERNET_IP")) == 0)
			return_value = general_variable_::ETHERNET_IP;

		if (strcmp_P(string, PSTR("ETHERNET_DNS")) == 0)
			return_value = general_variable_::ETHERNET_DNS;

		if (strcmp_P(string, PSTR("ETHERNET_GATEWAY")) == 0)
			return_value = general_variable_::ETHERNET_GATEWAY;

		if (strcmp_P(string, PSTR("ETHERNET_SUBNET")) == 0)
			return_value = general_variable_::ETHERNET_SUBNET;

		if (strcmp_P(string, PSTR("ETHERNET_PORT")) == 0)
			return_value = general_variable_::ETHERNET_PORT;

		if (strcmp_P(string, PSTR("SD_CS_PIN")) == 0)
			return_value = general_variable_::SD_CS_PIN;

		if (strcmp_P(string, PSTR("SD_LOG")) == 0)
			return_value = general_variable_::SD_LOG;

		if (strcmp_P(string, PSTR("ONEWIRE_PIN")) == 0)
			return_value = general_variable_::ONEWIRE_PIN;

		if (strcmp_P(string, PSTR("I2C_PIN_SCL")) == 0)
			return_value = general_variable_::I2C_PIN_SCL;

		if (strcmp_P(string, PSTR("I2C_PIN_SDA")) == 0)
			return_value = general_variable_::I2C_PIN_SDA;

		if (strcmp_P(string, PSTR("TIME_UTC_OFFSET")) == 0)
			return_value = general_variable_::TIME_UTC_OFFSET;

		if (strcmp_P(string, PSTR("INPUT_INFODISPLAY_KEYPAD")) == 0)
			return_value = general_variable_::INPUT_INFODISPLAY_KEYPAD;

		if (strcmp_P(string, PSTR("WIFI_ENABLED")) == 0)
			return_value = general_variable_::WIFI_ENABLED;

		if (strcmp_P(string, PSTR("WIFI_CHANNEL")) == 0)
			return_value = general_variable_::WIFI_CHANNEL;

		if (strcmp_P(string, PSTR("WIFI_DHCP")) == 0)
			return_value = general_variable_::WIFI_DHCP;

		if (strcmp_P(string, PSTR("WIFI_MAC")) == 0)
			return_value = general_variable_::WIFI_MAC;

		if (strcmp_P(string, PSTR("WIFI_IP")) == 0)
			return_value = general_variable_::WIFI_IP;

		if (strcmp_P(string, PSTR("WIFI_DNS")) == 0)
			return_value = general_variable_::WIFI_DNS;

		if (strcmp_P(string, PSTR("WIFI_GATEWAY")) == 0)
			return_value = general_variable_::WIFI_GATEWAY;

		if (strcmp_P(string, PSTR("WIFI_SUBNET")) == 0)
			return_value = general_variable_::WIFI_SUBNET;

		if (strcmp_P(string, PSTR("WIFI_PORT")) == 0)
			return_value = general_variable_::WIFI_PORT;

		if (strcmp_P(string, PSTR("WIFI_SSID_LENGTH")) == 0)
			return_value = general_variable_::WIFI_SSID_LENGTH;

		if (strcmp_P(string, PSTR("WIFI_PASSWORD_LENGTH")) == 0)
			return_value = general_variable_::WIFI_PASSWORD_LENGTH;

		if (strcmp_P(string, PSTR("WIFI_SSID")) == 0)
			return_value = general_variable_::WIFI_SSID;

		if (strcmp_P(string, PSTR("WIFI_PASSWORD")) == 0)
			return_value = general_variable_::WIFI_PASSWORD;

		if (strcmp_P(string, PSTR("WIFI_HOSTNAME_LENGTH")) == 0)
			return_value = general_variable_::WIFI_HOSTNAME_LENGTH;

		if (strcmp_P(string, PSTR("WIFI_HOSTNAME")) == 0)
			return_value = general_variable_::WIFI_HOSTNAME;
#if defined(ESP8266) || defined(ESP32)
		if (strcmp_P(string, PSTR("OTA_MODE")) == 0)
			return_value = general_variable_::OTA_MODE;

		if (strcmp_P(string, PSTR("OTA_HOSTNAME_LENGTH")) == 0)
			return_value = general_variable_::OTA_HOSTNAME_LENGTH;

		if (strcmp_P(string, PSTR("OTA_HOSTNAME")) == 0)
			return_value = general_variable_::OTA_HOSTNAME;

		if (strcmp_P(string, PSTR("OTA_ARDUINOOTA_PORT")) == 0)
			return_value = general_variable_::OTA_ARDUINOOTA_PORT;

		if (strcmp_P(string, PSTR("OTA_ARDUINOOTA_PASSWORD_HASH")) == 0)
			return_value = general_variable_::OTA_ARDUINOOTA_PASSWORD_HASH;

		if (strcmp_P(string, PSTR("OTA_ARDUINOOTA_PASSWORD_LENGTH")) == 0)
			return_value = general_variable_::OTA_ARDUINOOTA_PASSWORD_LENGTH;

		if (strcmp_P(string, PSTR("OTA_ARDUINOOTA_PASSWORD")) == 0)
			return_value = general_variable_::OTA_ARDUINOOTA_PASSWORD;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PORT")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PORT;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PATH_LENGTH")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PATH_LENGTH;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PATH")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PATH;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_USERNAME_LENGTH")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_USERNAME_LENGTH;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_USERNAME")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_USERNAME;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PASSWORD_LENGTH")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PASSWORD_LENGTH;

		//if (strcmp_P(string, PSTR("OTA_WEBBROWSER_PASSWORD")) == 0)
		//	return_value = general_variable_::OTA_WEBBROWSER_PASSWORD;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_UPDATE_INTERVAL")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_UPDATE_INTERVAL;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_ADDRESS_LENGTH")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_ADDRESS_LENGTH;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_ADDRESS")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_ADDRESS;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_PATH_LENGTH")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_PATH_LENGTH;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_PATH")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_PATH;

		//if (strcmp_P(string, PSTR("OTA_HTTPSERVER_PORT")) == 0)
		//	return_value = general_variable_::OTA_HTTPSERVER_PORT;
#endif /* defined(ESP8266) || defined(ESP32) */

		if (strcmp_P(string, PSTR("MQTT_ENABLED")) == 0)
			return_value = general_variable_::MQTT_ENABLED;

		if (strcmp_P(string, PSTR("MQTT_SERVER")) == 0)
			return_value = general_variable_::MQTT_SERVER;

		if (strcmp_P(string, PSTR("MQTT_PORT")) == 0)
			return_value = general_variable_::MQTT_PORT;

		if (strcmp_P(string, PSTR("MQTT_CLIENT_ID")) == 0)
			return_value = general_variable_::MQTT_CLIENT_ID;

		if (strcmp_P(string, PSTR("MQTT_USER")) == 0)
			return_value = general_variable_::MQTT_USER;

		if (strcmp_P(string, PSTR("MQTT_PASSWORD")) == 0)
			return_value = general_variable_::MQTT_PASSWORD;

		if (strcmp_P(string, PSTR("MQTT_TOPIC_PUBLISHED")) == 0)
			return_value = general_variable_::MQTT_TOPIC_PUBLISHED;

		if (strcmp_P(string, PSTR("MQTT_TOPIC_SUBSCRIBED")) == 0)
			return_value = general_variable_::MQTT_TOPIC_SUBSCRIBED;
	}

	if (device == EEPROMHandler::device_type_::TELESCOPE)
	{
		if (strcmp_P(string, PSTR("STARTUP_AUTO_HOME")) == 0)
			return_value = telescope_variable_::STARTUP_AUTO_HOME;

		if (strcmp_P(string, PSTR("PARK_ENABLED")) == 0)
			return_value = telescope_variable_::PARK_ENABLED;

		if (strcmp_P(string, PSTR("MOUNT_TYPE")) == 0)
			return_value = telescope_variable_::MOUNT_TYPE;

		if (strcmp_P(string, PSTR("SITE_LATITUDE")) == 0)
			return_value = telescope_variable_::SITE_LATITUDE;

		if (strcmp_P(string, PSTR("SITE_LONGITUDE")) == 0)
			return_value = telescope_variable_::SITE_LONGITUDE;

		if (strcmp_P(string, PSTR("SITE_ELEVATION")) == 0)
			return_value = telescope_variable_::SITE_ELEVATION;

		if (strcmp_P(string, PSTR("FOCAL_LENGTH")) == 0)
			return_value = telescope_variable_::FOCAL_LENGTH;

		if (strcmp_P(string, PSTR("APERTURE_DIAMETER")) == 0)
			return_value = telescope_variable_::APERTURE_DIAMETER;

		if (strcmp_P(string, PSTR("APERTURE_AREA")) == 0)
			return_value = telescope_variable_::APERTURE_AREA;

		if (strcmp_P(string, PSTR("MC_AVAILABLE")) == 0)
			return_value = telescope_variable_::MC_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_TOGGLE_AVAILABLE")) == 0)
			return_value = telescope_variable_::MC_BTN_TOGGLE_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_SPEED_AVAILABLE")) == 0)
			return_value = telescope_variable_::MC_BTN_SPEED_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_JST_AVAILABLE")) == 0)
			return_value = telescope_variable_::MC_JST_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_TOGGLE")) == 0)
			return_value = telescope_variable_::MC_BTN_TOGGLE;

		if (strcmp_P(string, PSTR("MC_BTN_SPEED")) == 0)
			return_value = telescope_variable_::MC_BTN_SPEED;

		if (strcmp_P(string, PSTR("MC_JST_INPUT_X")) == 0)
			return_value = telescope_variable_::MC_JST_INPUT_X;

		if (strcmp_P(string, PSTR("MC_JST_INPUT_Y")) == 0)
			return_value = telescope_variable_::MC_JST_INPUT_Y;

		if (strcmp_P(string, PSTR("MC_JST_DEADZONE")) == 0)
			return_value = telescope_variable_::MC_JST_DEADZONE;

		if (strcmp_P(string, PSTR("TEMPERATURE_SENSOR")) == 0)
			return_value = telescope_variable_::TEMPERATURE_SENSOR;
	}

	if (device == EEPROMHandler::device_type_::TELESCOPE_AXIS)
	{
		if (strcmp_P(string, PSTR("MAX_SPEED")) == 0)
			return_value = telescope_axis_variable_::MAX_SPEED;

		if (strcmp_P(string, PSTR("ACCELERATION")) == 0)
			return_value = telescope_axis_variable_::ACCELERATION;

		if (strcmp_P(string, PSTR("STEPTYPE_FAST")) == 0)
			return_value = telescope_axis_variable_::STEPTYPE_FAST;

		if (strcmp_P(string, PSTR("STEPTYPE_SLOW")) == 0)
			return_value = telescope_axis_variable_::STEPTYPE_SLOW;

		if (strcmp_P(string, PSTR("GEAR_RATIO")) == 0)
			return_value = telescope_axis_variable_::GEAR_RATIO;

		if (strcmp_P(string, PSTR("NUMBER_OF_STEPS")) == 0)
			return_value = telescope_axis_variable_::NUMBER_OF_STEPS;

		if (strcmp_P(string, PSTR("HOME_SWITCH_ENABLED")) == 0)
			return_value = telescope_axis_variable_::HOME_SWITCH_ENABLED;

		if (strcmp_P(string, PSTR("HOME_SWITCH")) == 0)
			return_value = telescope_axis_variable_::HOME_SWITCH;

		if (strcmp_P(string, PSTR("HOME_SWITCH_INVERTED")) == 0)
			return_value = telescope_axis_variable_::HOME_SWITCH_INVERTED;

		if (strcmp_P(string, PSTR("HOME_ALIGNMENT_SYNC")) == 0)
			return_value = telescope_axis_variable_::HOME_ALIGNMENT_SYNC;

		if (strcmp_P(string, PSTR("HOMING_DIRECTION")) == 0)
			return_value = telescope_axis_variable_::HOMING_DIRECTION;

		if (strcmp_P(string, PSTR("HOME_ALIGNMENT")) == 0)
			return_value = telescope_axis_variable_::HOME_ALIGNMENT;

		if (strcmp_P(string, PSTR("HOME_TIMEOUT")) == 0)
			return_value = telescope_axis_variable_::HOME_TIMEOUT;

		if (strcmp_P(string, PSTR("AUTOMATIC_STEPPER_RELEASE")) == 0)
			return_value = telescope_axis_variable_::AUTOMATIC_STEPPER_RELEASE;

		if (strcmp_P(string, PSTR("PARK_POS")) == 0)
			return_value = telescope_axis_variable_::PARK_POS;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER_ENABLED")) == 0)
			return_value = telescope_axis_variable_::ENDSTOP_LOWER_ENABLED;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER_ENABLED")) == 0)
			return_value = telescope_axis_variable_::ENDSTOP_UPPER_ENABLED;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER")) == 0)
			return_value = telescope_axis_variable_::ENDSTOP_LOWER;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER")) == 0)
			return_value = telescope_axis_variable_::ENDSTOP_UPPER;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER_INVERTED")) == 0)
			return_value = telescope_axis_variable_::ENDSTOP_LOWER_INVERTED;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER_INVERTED")) == 0)
			return_value = telescope_axis_variable_::ENDSTOP_UPPER_INVERTED;

		if (strcmp_P(string, PSTR("ENDSTOP_HARD_STOP")) == 0)
			return_value = telescope_axis_variable_::ENDSTOP_HARD_STOP;

		if (strcmp_P(string, PSTR("LIMIT_LOWER_ENABLED")) == 0)
			return_value = telescope_axis_variable_::LIMIT_LOWER_ENABLED;

		if (strcmp_P(string, PSTR("LIMIT_UPPER_ENABLED")) == 0)
			return_value = telescope_axis_variable_::LIMIT_UPPER_ENABLED;

		if (strcmp_P(string, PSTR("LIMIT_LOWER")) == 0)
			return_value = telescope_axis_variable_::LIMIT_LOWER;

		if (strcmp_P(string, PSTR("LIMIT_UPPER")) == 0)
			return_value = telescope_axis_variable_::LIMIT_UPPER;

		if (strcmp_P(string, PSTR("LIMIT_HARD_STOP")) == 0)
			return_value = telescope_axis_variable_::LIMIT_HARD_STOP;

		if (strcmp_P(string, PSTR("LIMIT_HANDLING")) == 0)
			return_value = telescope_axis_variable_::LIMIT_HANDLING;

		if (strcmp_P(string, PSTR("MOTOR_DRIVER_TYPE")) == 0)
			return_value = telescope_axis_variable_::MOTOR_DRIVER_TYPE;

		if (strcmp_P(string, PSTR("I2C_ADDRESS")) == 0)
			return_value = telescope_axis_variable_::I2C_ADDRESS;

		if (strcmp_P(string, PSTR("TERMINAL")) == 0)
			return_value = telescope_axis_variable_::TERMINAL;

		if (strcmp_P(string, PSTR("PIN_1")) == 0)
			return_value = telescope_axis_variable_::PIN_1;

		if (strcmp_P(string, PSTR("PIN_2")) == 0)
			return_value = telescope_axis_variable_::PIN_2;

		if (strcmp_P(string, PSTR("PIN_3")) == 0)
			return_value = telescope_axis_variable_::PIN_3;

		if (strcmp_P(string, PSTR("PIN_4")) == 0)
			return_value = telescope_axis_variable_::PIN_4;

		if (strcmp_P(string, PSTR("PIN_5")) == 0)
			return_value = telescope_axis_variable_::PIN_5;

		if (strcmp_P(string, PSTR("PIN_6")) == 0)
			return_value = telescope_axis_variable_::PIN_6;

		if (strcmp_P(string, PSTR("PIN_1_INVERTED")) == 0)
			return_value = telescope_axis_variable_::PIN_1_INVERTED;

		if (strcmp_P(string, PSTR("PIN_2_INVERTED")) == 0)
			return_value = telescope_axis_variable_::PIN_2_INVERTED;

		if (strcmp_P(string, PSTR("PIN_3_INVERTED")) == 0)
			return_value = telescope_axis_variable_::PIN_3_INVERTED;

		if (strcmp_P(string, PSTR("PIN_4_INVERTED")) == 0)
			return_value = telescope_axis_variable_::PIN_4_INVERTED;

		if (strcmp_P(string, PSTR("PIN_5_INVERTED")) == 0)
			return_value = telescope_axis_variable_::PIN_5_INVERTED;

		if (strcmp_P(string, PSTR("PIN_6_INVERTED")) == 0)
			return_value = telescope_axis_variable_::PIN_6_INVERTED;
	}

	if (device == EEPROMHandler::device_type_::FILTER_WHEEL)
	{
		if (strcmp_P(string, PSTR("MAX_SPEED")) == 0)
			return_value = filter_wheel_variable_::MAX_SPEED;

		if (strcmp_P(string, PSTR("ACCELERATION")) == 0)
			return_value = filter_wheel_variable_::ACCELERATION;

		if (strcmp_P(string, PSTR("STEP_TYPE")) == 0)
			return_value = filter_wheel_variable_::STEPTYPE;

		if (strcmp_P(string, PSTR("GEAR_RATIO")) == 0)
			return_value = filter_wheel_variable_::GEAR_RATIO;

		if (strcmp_P(string, PSTR("NUMBER_OF_STEPS")) == 0)
			return_value = filter_wheel_variable_::NUMBER_OF_STEPS;

		if (strcmp_P(string, PSTR("HOME_SWITCH_ENABLED")) == 0)
			return_value = filter_wheel_variable_::HOME_SWITCH_ENABLED;

		if (strcmp_P(string, PSTR("HOME_SWITCH")) == 0)
			return_value = filter_wheel_variable_::HOME_SWITCH;

		if (strcmp_P(string, PSTR("HOME_SWITCH_INVERTED")) == 0)
			return_value = filter_wheel_variable_::HOME_SWITCH_INVERTED;

		if (strcmp_P(string, PSTR("HOME_TIMEOUT")) == 0)
			return_value = filter_wheel_variable_::HOME_TIMEOUT;

		//if (strcmp_P(string, PSTR("HOMING_DIRECTION")) == 0)
		//	return_value = filter_wheel_variable_::HOMING_DIRECTION;

		if (strcmp_P(string, PSTR("HOME_OFFSET")) == 0)
			return_value = filter_wheel_variable_::HOME_OFFSET;

		if (strcmp_P(string, PSTR("STARTUP_AUTO_HOME")) == 0)
			return_value = filter_wheel_variable_::STARTUP_AUTO_HOME;

		if (strcmp_P(string, PSTR("AUTOMATIC_STEPPER_RELEASE")) == 0)
			return_value = filter_wheel_variable_::AUTOMATIC_STEPPER_RELEASE;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER_ENABLED")) == 0)
			return_value = filter_wheel_variable_::ENDSTOP_LOWER_ENABLED;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER_ENABLED")) == 0)
			return_value = filter_wheel_variable_::ENDSTOP_UPPER_ENABLED;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER")) == 0)
			return_value = filter_wheel_variable_::ENDSTOP_LOWER;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER")) == 0)
			return_value = filter_wheel_variable_::ENDSTOP_UPPER;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER_INVERTED")) == 0)
			return_value = filter_wheel_variable_::ENDSTOP_LOWER_INVERTED;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER_INVERTED")) == 0)
			return_value = filter_wheel_variable_::ENDSTOP_UPPER_INVERTED;

		if (strcmp_P(string, PSTR("ENDSTOP_HARD_STOP")) == 0)
			return_value = filter_wheel_variable_::ENDSTOP_HARD_STOP;

		if (strcmp_P(string, PSTR("LIMIT_LOWER_ENABLED")) == 0)
			return_value = filter_wheel_variable_::LIMIT_LOWER_ENABLED;

		if (strcmp_P(string, PSTR("LIMIT_UPPER_ENABLED")) == 0)
			return_value = filter_wheel_variable_::LIMIT_UPPER_ENABLED;

		if (strcmp_P(string, PSTR("LIMIT_LOWER")) == 0)
			return_value = filter_wheel_variable_::LIMIT_LOWER;

		if (strcmp_P(string, PSTR("LIMIT_UPPER")) == 0)
			return_value = filter_wheel_variable_::LIMIT_UPPER;

		if (strcmp_P(string, PSTR("LIMIT_HARD_STOP")) == 0)
			return_value = filter_wheel_variable_::LIMIT_HARD_STOP;

		//if (strcmp_P(string, PSTR("LIMIT_HANDLING")) == 0)
		//	return_value = filter_wheel_variable_::LIMIT_HANDLING;

		if (strcmp_P(string, PSTR("MOTOR_DRIVER_TYPE")) == 0)
			return_value = filter_wheel_variable_::MOTOR_DRIVER_TYPE;

		if (strcmp_P(string, PSTR("I2C_ADDRESS")) == 0)
			return_value = filter_wheel_variable_::I2C_ADDRESS;

		if (strcmp_P(string, PSTR("TERMINAL")) == 0)
			return_value = filter_wheel_variable_::TERMINAL;

		if (strcmp_P(string, PSTR("PIN_1")) == 0)
			return_value = filter_wheel_variable_::PIN_1;

		if (strcmp_P(string, PSTR("PIN_2")) == 0)
			return_value = filter_wheel_variable_::PIN_2;

		if (strcmp_P(string, PSTR("PIN_3")) == 0)
			return_value = filter_wheel_variable_::PIN_3;

		if (strcmp_P(string, PSTR("PIN_4")) == 0)
			return_value = filter_wheel_variable_::PIN_4;

		if (strcmp_P(string, PSTR("PIN_5")) == 0)
			return_value = filter_wheel_variable_::PIN_5;

		if (strcmp_P(string, PSTR("PIN_6")) == 0)
			return_value = filter_wheel_variable_::PIN_6;

		if (strcmp_P(string, PSTR("PIN_1_INVERTED")) == 0)
			return_value = filter_wheel_variable_::PIN_1_INVERTED;

		if (strcmp_P(string, PSTR("PIN_2_INVERTED")) == 0)
			return_value = filter_wheel_variable_::PIN_2_INVERTED;

		if (strcmp_P(string, PSTR("PIN_3_INVERTED")) == 0)
			return_value = filter_wheel_variable_::PIN_3_INVERTED;

		if (strcmp_P(string, PSTR("PIN_4_INVERTED")) == 0)
			return_value = filter_wheel_variable_::PIN_4_INVERTED;

		if (strcmp_P(string, PSTR("PIN_5_INVERTED")) == 0)
			return_value = filter_wheel_variable_::PIN_5_INVERTED;

		if (strcmp_P(string, PSTR("PIN_6_INVERTED")) == 0)
			return_value = filter_wheel_variable_::PIN_6_INVERTED;

		if (strcmp_P(string, PSTR("NUMBER_OF_FILTERS")) == 0)
			return_value = filter_wheel_variable_::NUMBER_OF_FILTERS;

		if (strcmp_P(string, PSTR("FILTER_HOLDER_TYPE")) == 0)
			return_value = filter_wheel_variable_::FILTER_HOLDER_TYPE;

		if (strcmp_P(string, PSTR("MC_AVAILABLE")) == 0)
			return_value = filter_wheel_variable_::MC_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_LOCK_AVAILABLE")) == 0)
			return_value = filter_wheel_variable_::MC_BTN_LOCK_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_NEXT_AVAILABLE")) == 0)
			return_value = filter_wheel_variable_::MC_BTN_NEXT_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_PREVIOUS_AVAILABLE")) == 0)
			return_value = filter_wheel_variable_::MC_BTN_PREVIOUS_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_LOCK")) == 0)
			return_value = filter_wheel_variable_::MC_BTN_LOCK;

		if (strcmp_P(string, PSTR("MC_BTN_NEXT")) == 0)
			return_value = filter_wheel_variable_::MC_BTN_NEXT;

		if (strcmp_P(string, PSTR("MC_BTN_PREVIOUS")) == 0)
			return_value = filter_wheel_variable_::MC_BTN_PREVIOUS;

		if (strcmp_P(string, PSTR("FILTER_INFO")) == 0)
			return_value = filter_wheel_variable_::FILTER_INFO;

		if (strcmp_P(string, PSTR("TEMPERATURE_SENSOR")) == 0)
			return_value = filter_wheel_variable_::TEMPERATURE_SENSOR;
	}

	if (device == EEPROMHandler::device_type_::FOCUSER)
	{
		if (strcmp_P(string, PSTR("MAX_SPEED")) == 0)
			return_value = focuser_variable_::MAX_SPEED;

		//if (strcmp_P(string, PSTR("ACCELERATION")) == 0)
		//	return_value = focuser_variable_::ACCELERATION;

		if (strcmp_P(string, PSTR("GEAR_RATIO")) == 0)
			return_value = focuser_variable_::GEAR_RATIO;

		if (strcmp_P(string, PSTR("NUMBER_OF_STEPS")) == 0)
			return_value = focuser_variable_::NUMBER_OF_STEPS;

		if (strcmp_P(string, PSTR("STEP_TYPE")) == 0)
			return_value = focuser_variable_::STEP_TYPE;

		//if (strcmp_P(string, PSTR("STEP_TYPE_TEMP_COMP")) == 0)
		//	return_value = focuser_variable_::STEP_TYPE_TEMP_COMP;


		if (strcmp_P(string, PSTR("HOME_SWITCH_ENABLED")) == 0)
			return_value = focuser_variable_::HOME_SWITCH_ENABLED;

		if (strcmp_P(string, PSTR("HOME_SWITCH")) == 0)
			return_value = focuser_variable_::HOME_SWITCH;

		if (strcmp_P(string, PSTR("HOME_SWITCH_INVERTED")) == 0)
			return_value = focuser_variable_::HOME_SWITCH_INVERTED;

		if (strcmp_P(string, PSTR("HOME_POSITION")) == 0)
			return_value = focuser_variable_::HOME_POSITION;

		if (strcmp_P(string, PSTR("HOME_TIMEOUT")) == 0)
			return_value = focuser_variable_::HOME_TIMEOUT;

		//if (strcmp_P(string, PSTR("HOMING_DIRECTION")) == 0)
		//return_value = focuser_variable_::HOMING_DIRECTION;
		//}


		if (strcmp_P(string, PSTR("AUTOMATIC_STEPPER_RELEASE")) == 0)
			return_value = focuser_variable_::AUTOMATIC_STEPPER_RELEASE;

		if (strcmp_P(string, PSTR("STARTUP_AUTO_HOME")) == 0)
			return_value = focuser_variable_::STARTUP_AUTO_HOME;


		if (strcmp_P(string, PSTR("ENDSTOP_LOWER_ENABLED")) == 0)
			return_value = focuser_variable_::ENDSTOP_LOWER_ENABLED;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER_ENABLED")) == 0)
			return_value = focuser_variable_::ENDSTOP_UPPER_ENABLED;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER")) == 0)
			return_value = focuser_variable_::ENDSTOP_LOWER;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER")) == 0)
			return_value = focuser_variable_::ENDSTOP_UPPER;

		if (strcmp_P(string, PSTR("ENDSTOP_LOWER_INVERTED")) == 0)
			return_value = focuser_variable_::ENDSTOP_LOWER_INVERTED;

		if (strcmp_P(string, PSTR("ENDSTOP_UPPER_INVERTED")) == 0)
			return_value = focuser_variable_::ENDSTOP_UPPER_INVERTED;

		//if (strcmp_P(string, PSTR("ENDSTOP_HARD_STOP")) == 0)
		//	return_value = focuser_variable_::ENDSTOP_HARD_STOP;


		if (strcmp_P(string, PSTR("LIMIT_LOWER_ENABLED")) == 0)
			return_value = focuser_variable_::LIMIT_LOWER_ENABLED;

		if (strcmp_P(string, PSTR("LIMIT_UPPER_ENABLED")) == 0)
			return_value = focuser_variable_::LIMIT_UPPER_ENABLED;

		if (strcmp_P(string, PSTR("LIMIT_LOWER")) == 0)
			return_value = focuser_variable_::LIMIT_LOWER;

		if (strcmp_P(string, PSTR("LIMIT_UPPER")) == 0)
			return_value = focuser_variable_::LIMIT_UPPER;

		//if (strcmp_P(string, PSTR("LIMIT_HARD_STOP")) == 0)
		//	return_value = focuser_variable_::LIMIT_HARD_STOP;


		if (strcmp_P(string, PSTR("MOTOR_DRIVER_TYPE")) == 0)
			return_value = focuser_variable_::MOTOR_DRIVER_TYPE;

		if (strcmp_P(string, PSTR("I2C_ADDRESS")) == 0)
			return_value = focuser_variable_::I2C_ADDRESS;

		if (strcmp_P(string, PSTR("TERMINAL")) == 0)
			return_value = focuser_variable_::TERMINAL;

		if (strcmp_P(string, PSTR("PIN_1")) == 0)
			return_value = focuser_variable_::PIN_1;

		if (strcmp_P(string, PSTR("PIN_2")) == 0)
			return_value = focuser_variable_::PIN_2;

		if (strcmp_P(string, PSTR("PIN_3")) == 0)
			return_value = focuser_variable_::PIN_3;

		if (strcmp_P(string, PSTR("PIN_4")) == 0)
			return_value = focuser_variable_::PIN_4;

		if (strcmp_P(string, PSTR("PIN_5")) == 0)
			return_value = focuser_variable_::PIN_5;

		if (strcmp_P(string, PSTR("PIN_6")) == 0)
			return_value = focuser_variable_::PIN_6;

		if (strcmp_P(string, PSTR("PIN_1_INVERTED")) == 0)
			return_value = focuser_variable_::PIN_1_INVERTED;

		if (strcmp_P(string, PSTR("PIN_2_INVERTED")) == 0)
			return_value = focuser_variable_::PIN_2_INVERTED;

		if (strcmp_P(string, PSTR("PIN_3_INVERTED")) == 0)
			return_value = focuser_variable_::PIN_3_INVERTED;

		if (strcmp_P(string, PSTR("PIN_4_INVERTED")) == 0)
			return_value = focuser_variable_::PIN_4_INVERTED;

		if (strcmp_P(string, PSTR("PIN_5_INVERTED")) == 0)
			return_value = focuser_variable_::PIN_5_INVERTED;

		if (strcmp_P(string, PSTR("PIN_6_INVERTED")) == 0)
			return_value = focuser_variable_::PIN_6_INVERTED;


		if (strcmp_P(string, PSTR("POSITION_ABSOLUTE")) == 0)
			return_value = focuser_variable_::POSITION_ABSOLUTE;

		if (strcmp_P(string, PSTR("MICRONS_PER_REV")) == 0)
			return_value = focuser_variable_::MICRONS_PER_REV;

		if (strcmp_P(string, PSTR("MAX_INCREMENT")) == 0)
			return_value = focuser_variable_::MAX_INCREMENT;

		if (strcmp_P(string, PSTR("MAX_STEP")) == 0)
			return_value = focuser_variable_::MAX_STEP;

		if (strcmp_P(string, PSTR("TC_AVAILABLE")) == 0)
			return_value = focuser_variable_::TC_AVAILABLE;

		if (strcmp_P(string, PSTR("TC_SENSOR")) == 0)
			return_value = focuser_variable_::TC_SENSOR;

		if (strcmp_P(string, PSTR("TC_FACTOR")) == 0)
			return_value = focuser_variable_::TC_FACTOR;

		if (strcmp_P(string, PSTR("TEMPERATURE_SENSOR")) == 0)
			return_value = focuser_variable_::TEMPERATURE_SENSOR;


		if (strcmp_P(string, PSTR("MC_AVAILABLE")) == 0)
			return_value = focuser_variable_::MC_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_LOCK_AVAILABLE")) == 0)
			return_value = focuser_variable_::MC_BTN_LOCK_AVAILABLE;

		//if (strcmp_P(string, PSTR("MC_BTN_NEXT_AVAILABLE")) == 0)
		//	return_value = focuser_variable_::MC_BTN_NEXT_AVAILABLE;

		//if (strcmp_P(string, PSTR("MC_BTN_PREVIOUS_AVAILABLE")) == 0)
		//	return_value = focuser_variable_::MC_BTN_PREVIOUS_AVAILABLE;

		if (strcmp_P(string, PSTR("MC_BTN_LOCK")) == 0)
			return_value = focuser_variable_::MC_BTN_LOCK;

		if (strcmp_P(string, PSTR("MC_BTN_IN")) == 0)
			return_value = focuser_variable_::MC_BTN_IN;

		if (strcmp_P(string, PSTR("MC_BTN_OUT")) == 0)
			return_value = focuser_variable_::MC_BTN_OUT;

		if (strcmp_P(string, PSTR("MC_BTN_LOCK_INVERTED")) == 0)
			return_value = focuser_variable_::MC_BTN_LOCK_INVERTED;

		if (strcmp_P(string, PSTR("MC_BTN_IN_INVERTED")) == 0)
			return_value = focuser_variable_::MC_BTN_IN_INVERTED;

		if (strcmp_P(string, PSTR("MC_BTN_OUT_INVERTED")) == 0)
			return_value = focuser_variable_::MC_BTN_OUT_INVERTED;
	}

	return return_value;
}

uint8_t EEPROMHandler::getEEPROMStringLength(uint16_t device, uint16_t variable)
{
	uint8_t length = 0;

	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		switch (variable)
		{
		case EEPROMHandler::general_variable_::WIFI_SSID:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_SSID_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_PASSWORD_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_HOSTNAME_LENGTH,
				0,
				length);
			break;
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HOSTNAME:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_HOSTNAME_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS_LENGTH,
				0,
				length);
			break;
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH:
			EEPROMHandler::readValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH_LENGTH,
				0,
				length);
			break;
#endif /* defined(ESP8266) || defined(ESP32) */
		default:
			break;
		}
	}

	return length;
}

bool EEPROMHandler::setEEPROMStringLength(uint16_t device, uint16_t variable, uint8_t length)
{
	if (length > getEEPROMMaxStringLength(device, variable))
		return false;

	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		switch (variable)
		{
		case EEPROMHandler::general_variable_::WIFI_SSID:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_SSID_LENGTH,
				0,
				length);
			return true;
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_PASSWORD_LENGTH,
				0,
				length);
			return true;
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_HOSTNAME_LENGTH,
				0,
				length);
			return true;
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HOSTNAME:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_HOSTNAME_LENGTH,
				0,
				length);
			return true;
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD_LENGTH,
				0,
				length);
			return true;
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH_LENGTH,
				0,
				length);
			return true;
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME_LENGTH,
				0,
				length);
			return true;
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD_LENGTH,
				0,
				length);
			return true;
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS_LENGTH,
				0,
				length);
			return true;
			return true;
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH:
			EEPROMHandler::writeValue(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::OTA_HTTPSERVER_PATH_LENGTH,
				0,
				length);
			return true;
#endif /* defined(ESP8266) || defined(ESP32) */
		default:
			break;
		}
	}

	return false;
}

uint8_t EEPROMHandler::getEEPROMMaxStringLength(uint16_t device, uint16_t variable)
{
	uint8_t length = 0;

	if (device == EEPROMHandler::device_type_::GENERAL)
	{
		switch (variable)
		{
		case EEPROMHandler::general_variable_::WIFI_SSID:
			length = 32;
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
#if defined(ESP8266) || defined(ESP32)
		case EEPROMHandler::general_variable_::OTA_HOSTNAME:
		case EEPROMHandler::general_variable_::OTA_ARDUINOOTA_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PATH:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_USERNAME:
		case EEPROMHandler::general_variable_::OTA_WEBBROWSER_PASSWORD:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_ADDRESS:
		case EEPROMHandler::general_variable_::OTA_HTTPSERVER_PORT:
#endif /* defined(ESP8266) || defined(ESP32) */
		case EEPROMHandler::general_variable_::MQTT_SERVER:
		case EEPROMHandler::general_variable_::MQTT_CLIENT_ID:
		case EEPROMHandler::general_variable_::MQTT_USER:
		case EEPROMHandler::general_variable_::MQTT_PASSWORD:
		case EEPROMHandler::general_variable_::MQTT_TOPIC_PUBLISHED:
		case EEPROMHandler::general_variable_::MQTT_TOPIC_SUBSCRIBED:
			length = 63;
		}
	}

	return length;
}

uint16_t EEPROMHandler::calculateOffset(uint16_t device, uint16_t variable, uint16_t index)
{
	//the value offset is different for each device and value read.
	uint16_t offset = 0;

	//calculate option- specific offsets.
	switch (device)
	{
	case device_type_::GENERAL:
	{

	}
	break;

	case device_type_::CAMERA_ROTATOR:
	{

	}
	break;

	case device_type_::DOME:
	{

	}
	break;

	case device_type_::FILTER_WHEEL:
	{
		if (variable == filter_wheel_variable_::TEMPERATURE_SENSOR)
		{
			index = constrain(index, 0, TEMP_SENSORS - 1);

			//offset = sizeof(YAAASensor::SensorSettings) * index;		//settings for 1 temperature sensor take up 20 bytes of EEPROM.
			offset = 20 * index;		//settings for 1 temperature sensor take up 20 bytes of EEPROM.
		}

		if (variable == filter_wheel_variable_::FILTER_INFO)
		{
			index = constrain(index, 0, 20);

			//offset = sizeof(FilterWheel::FilterInfo) * index;
			offset = 10 * index;
		}
	}
	break;

	case device_type_::FOCUSER:
	{
		if (variable == focuser_variable_::TEMPERATURE_SENSOR)
		{
			index = constrain(index, 0, TEMP_SENSORS - 1);

			//offset = sizeof(TemperatureSensor::SensorSettings) * index;		//settings for 1 temperature sensor take up 20 bytes of EEPROM.
			offset = 20 * index;		//settings for 1 temperature sensor take up 20 bytes of EEPROM. TODO use YAAASensor::SensorSettings struct size?
		}
	}
	break;

	case device_type_::SAFETY_MONITOR:
	{

	}
	break;

	case device_type_::TELESCOPE:
	{
		if (variable == telescope_variable_::TEMPERATURE_SENSOR)
		{
			index = constrain(index, 0, TEMP_SENSORS - 1);

			//offset = sizeof(TemperatureSensor::SensorSettings) * index;		//settings for 1 temperature sensor take up 20 bytes of EEPROM.
			offset = 20 * index;		//settings for 1 temperature sensor take up 20 bytes of EEPROM. TODO use YAAASensor::SensorSettings struct size?
		}
	}
	break;

	case device_type_::TELESCOPE_AXIS:
	{
		offset = index * 128;
	}
	break;
	}

	return offset;
}
