//defaults.h file
//this file contains default values.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



#ifndef DEFAULTS_H_
#define DEFAULTS_H_

//----------------------------------------------------------------------------------
//general default values
#define DEF_G_EEPROM_CONTROL 149597870

#define DEF_G_CAMERA_ROTATOR_ENABLED false
#define DEF_G_DOME_ENABLED false
#define DEF_G_FILTER_WHEEL_ENABLED true
#define DEF_G_FOCUSER_ENABLED true
#define DEF_G_SAFETY_MONITOR_ENABLED false
#define DEF_G_TELESCOPE_ENABLED true

#define DEF_G_SD_CS_PIN 255
#define DEF_G_SD_LOG false

#define DEF_G_ONEWIRE_PIN 255

#define DEF_G_TIME_UTC_OFFSET 0.0f

#define DEF_G_SERIAL_BAUDRATE 9600

#define DEF_G_DISPLAY_TYPE Display::DISPLAY_NONE
#define DEF_G_DISPLAY_RES_X 20
#define DEF_G_DISPLAY_RES_Y 4
#define DEF_G_DISPLAY_UPDATE_INTERVAL 250L
#define DEF_G_DISPLAY_BRIGHTNESS 128
#define DEF_G_DISPLAY_CONTRAST 128
#define DEF_G_DISPLAY_INTERFACE_TYPE Display::INTERFACE_NATIVE
#define DEF_G_DISPLAY_INTERFACE_PIN0 255
#define DEF_G_DISPLAY_INTERFACE_PIN1 255
#define DEF_G_DISPLAY_INTERFACE_PIN2 255
#define DEF_G_DISPLAY_INTERFACE_PIN3 255
#define DEF_G_DISPLAY_INTERFACE_PIN4 255
#define DEF_G_DISPLAY_INTERFACE_PIN5 255
#define DEF_G_DISPLAY_INTERFACE_PIN6 255
#define DEF_G_DISPLAY_INTERFACE_PIN7 255
#define DEF_G_DISPLAY_INTERFACE_PIN8 255
#define DEF_G_DISPLAY_INTERFACE_PIN9 255
#define DEF_G_DISPLAY_INTERFACE_PIN10 255
#define DEF_G_DISPLAY_INTERFACE_PIN11 255
#define DEF_G_DISPLAY_INTERFACE_PIN12 255
#define DEF_G_DISPLAY_INTERFACE_PIN13 255
#define DEF_G_DISPLAY_INTERFACE_PIN14 255
#define DEF_G_DISPLAY_INTERFACE_PIN15 255

#define DEF_G_ETHERNET_ENABLED false
#define DEF_G_ETHERNET_DHCP false
#define DEF_G_ETHERNET_MAC { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }
#define DEF_G_ETHERNET_IP  { 192, 168, 0, 201 }
#define DEF_G_ETHERNET_DNS { 192, 168, 0, 1 }
#define DEF_G_ETHERNET_GATEWAY { 192, 168, 0, 1 }
#define DEF_G_ETHERNET_SUBNET { 255, 255, 255, 0 }
#define DEF_G_ETHERNET_PORT 23

#define DEF_G_WIFI_ENABLED false
#define DEF_G_WIFI_DHCP false
#define DEF_G_WIFI_MAC { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }
#define DEF_G_WIFI_IP  { 192, 168, 0, 201 }
#define DEF_G_WIFI_DNS { 192, 168, 0, 1 }
#define DEF_G_WIFI_GATEWAY { 192, 168, 0, 1 }
#define DEF_G_WIFI_SUBNET { 255, 255, 255, 0 }
#define DEF_G_WIFI_PORT 23
#define DEF_G_WIFI_CHANNEL 0
#define DEF_G_WIFI_SSID_LENGTH 4
#define DEF_G_WIFI_PASSWORD_LENGTH 8
#define DEF_G_WIFI_SSID "SSID"
#define DEF_G_WIFI_PASSWORD "PASSWORD"
#define DEF_G_WIFI_HOSTNAME_LENGTH 10
#define DEF_G_WIFI_HOSTNAME "yaaadevice"

#if defined(ESP8266) || defined(ESP32)
#define DEF_G_OTA_MODE OTAHandler::MODE_DISABLED
#define DEF_G_OTA_HOSTNAME_LENGTH 10
#define DEF_G_OTA_HOSTNAME "YAAADevice"
#define DEF_G_OTA_ARDUINOOTA_PORT 8266
#define DEF_G_OTA_ARDUINOOTA_PASSWORD_HASH false
#define DEF_G_OTA_ARDUINOOTA_PASSWORD_LENGTH 10
#define DEF_G_OTA_ARDUINOOTA_PASSWORD "YAAADevice"
#define DEF_G_OTA_WEBBROWSER_PORT 8266
#define DEF_G_OTA_WEBBROWSER_PATH_LENGTH 1
#define DEF_G_OTA_WEBBROWSER_PATH ""
#define DEF_G_OTA_WEBBROWSER_USERNAME_LENGTH 10
#define DEF_G_OTA_WEBBROWSER_USERNAME "YAAADevice"
#define DEF_G_OTA_WEBBROWSER_PASSWORD_LENGTH 10
#define DEF_G_OTA_WEBBROWSER_PASSWORD "YAAADevice"
#define DEF_G_OTA_HTTPSERVER_UPDATE_INTERVAL 30 * 60 * 1000  //30 minutes
#define DEF_G_OTA_HTTPSERVER_ADDRESS_LENGTH 9
#define DEF_G_OTA_HTTPSERVER_ADDRESS "127.0.0.1"
#define DEF_G_OTA_HTTPSERVER_PATH_LENGTH 17
#define DEF_G_OTA_HTTPSERVER_PATH "yaaadevice/update"
#define DEF_G_OTA_HTTPSERVER_PORT 8266
#endif /* defined(ESP8266) || defined(ESP32) */

#define DEF_G_KEYPAD_TYPE YAAAKeypad::TYPE_NONE
#define DEF_G_KEYPAD_ACTIVE_HIGH false
#define DEF_G_KEYPAD_PARAMETERS { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }
#define DEF_G_KEYPAD_READ_INTERVAL 250L


//----------------------------------------------------------------------------------
#define DEF_SENSOR_AUTOMATIC_MEAUREMENTS false
#define DEF_SENSOR_AVERAGE_SAMPLES 5
#define DEF_SENSOR_AVERAGE_PERIOD 600000L

#define DEF_SENSOR_LOG_DELIMITER ','
#define DEF_SENSOR_LOG_INTERVAL 10000L

#define DEF_SENSOR_TYPE YAAASensor::SENSOR_UNKNOWN
#define DEF_SENSOR_DEVICE YAAASensor::SENSOR_UNKNOWN
#define DEF_SENSOR_UNIT 255
#define DEF_SENSOR_AVG_SAMPLES 5
#define DEF_SENSOR_PARAMETERS { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }

#define DEF_SENSOR_TEMPERATURE_TYPE YAAASensor::SENSOR_TEMPERATURE
#define DEF_SENSOR_TEMPERATURE_DEVICE SensorTemperature::DEVICE_UNKOWN
#define DEF_SENSOR_TEMPERATURE_UNIT SensorTemperature::UNIT_CELSIUS
#define DEF_SENSOR_TEMPERATURE_AVG_SAMPLES 5

#define DEF_SENSOR_TEMPERATURE_ANALOG_IC_ZERO_VOLTAGE 0
#define DEF_SENSOR_TEMPERATURE_ANALOG_IC_REFERENCE_VOLTAGE 5000
#define DEF_SENSOR_TEMPERATURE_ANALOG_IC_COEFFICIENT 10
#define DEF_SENSOR_TEMPERATURE_ANALOG_IC_PARAMETERS { 0x11, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x13, 0x88, 0x00, 0x0A }

#define DEF_SENSOR_TEMPERATURE_ONEWIRE_DS18_DEVICE SensorTemperature::DEVICE_ONEWIRE_DS18
#define DEF_SENSOR_TEMPERATURE_ONEWIRE_DS18_PARAMETERS { 0x10, 0x36, 0x9C, 0xDE, 0x2, 0x8, 0x0, 0x1F, 0x00, 0x1F }

#define DEF_SENSOR_TEMPERATURE_MLX90614_DEVICE SensorTemperature::DEVICE_MLX90614
#define DEF_SENSOR_TEMPERATURE_MLX90614_I2C_ADDRESS 0x5A
#define DEF_SENSOR_TEMPERATURE_MLX90614_EMISSIVITY 0xFFFF
#define DEF_SENSOR_TEMPERATURE_MLX90614_ZONE SensorTemperatureMLX90614::RAM_TEMP_OBJ1
#define DEF_SENSOR_TEMPERATURE_MLX90614_ZONE_AMB SensorTemperatureMLX90614::RAM_TEMP_A
#define DEF_SENSOR_TEMPERATURE_MLX90614_PARAMETRERS { 0x5A, 0x07, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF }
#define DEF_SENSOR_TEMPERATURE_MLX90614_PARAMETRERS_AMB { 0x5A, 0x06, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }

#define DEF_SENSOR_TEMPERATURE_THERMISTOR_PULLUP 4700.0f
#define DEF_SENSOR_TEMPERATURE_THERMISTOR_REFERENCE_RES 10000.0f
#define DEF_SENSOR_TEMPERATURE_THERMISTOR_REFERENCE_TEMP 298.15f
#define DEF_SENSOR_TEMPERATURE_THERMISTOR_PARAMETER_B 3435.0f
#define DEF_SENSOR_TEMPERATURE_THERMISTOR_PARAMETERS { 0x11, 0xFF, 0x00, 0x2F, 0x00, 0x64, 0x0B, 0xA6, 0x0D, 0x6B }

//----------------------------------------------------------------------------------
//camera rotator default values

//----------------------------------------------------------------------------------
//dome default values

//----------------------------------------------------------------------------------
//focuser default values
#define DEF_F_MAX_SPEED 100.0f					//maximum stepper motor speed in steps / second
#define DEF_F_GEAR_RATIO 2.0f
#define DEF_F_NUMBER_OF_STEPS 200				//stepper motor steps / revolution
#define DEF_F_STEP_TYPE StepperMotor::STEPTYPE_FULL

#define DEF_F_HOME_SWITCH_ENABLED false
#define DEF_F_HOME_SWITCH_PIN 255
#define DEF_F_HOME_SWITCH_INVERTED false		//true if switch is active high
#define DEF_F_HOME_POS 0
#define DEF_F_STARTUP_AUTO_HOME false
#define DEF_F_AUTO_STEPPER_RELEASE true			//true if the stepper motor should be deactivated automatically at the end of a movement

#define DEF_F_ENDSTOP_LOWER_ENABLED false
#define DEF_F_ENDSTOP_LOWER_PIN 255
#define DEF_F_ENDSTOP_LOWER_INVERTED false		//true if switch is active high
#define DEF_F_ENDSTOP_UPPER_ENABLED false
#define DEF_F_ENDSTOP_UPPER_PIN 255
#define DEF_F_ENDSTOP_UPPER_INVERTED false		//true if switch is active high

#define DEF_F_LIMIT_LOWER_ENDBLED true
#define DEF_F_LIMIT_LOWER 0						//movement limit in steps
#define DEF_F_LIMIT_UPPER_ENBALED true
#define DEF_F_LIMIT_UPPER 32767					//movement limit in steps

#define DEF_F_MOTOR_DRIVER_TYPE StepperMotor::FOUR_WIRE_STEPPER
#define DEF_F_I2C_ADDRESS 0x61
#define DEF_F_TERMINAL 2
#define DEF_F_PIN_1 255
#define DEF_F_PIN_2 255
#define DEF_F_PIN_3 255
#define DEF_F_PIN_4 255
#define DEF_F_PIN_5 255
#define DEF_F_PIN_6 255
#define DEF_F_PIN_1_INVERTED false				//true if switch is active high
#define DEF_F_PIN_2_INVERTED false				//true if switch is active high
#define DEF_F_PIN_3_INVERTED false				//true if switch is active high
#define DEF_F_PIN_4_INVERTED false				//true if switch is active high
#define DEF_F_PIN_5_INVERTED false				//true if switch is active high
#define DEF_F_PIN_6_INVERTED false				//true if switch is active high

#define DEF_F_POSITION_ABSOLUTE false
#define DEF_F_MICRONS_PER_REV 25000.0f			//microns per knob revolution
#define DEF_F_MAX_INCREMENT 32767				//the maximum number of steps allowed in one move operation
#define DEF_F_MAX_STEP 32767					//maximum step position (not necessary equal to LIMIT_UPPER

#define DEF_F_HOME_TIMEOUT 0L

#define DEF_F_TC_AVAILABLE false				//true if temperature compensation is enabled
#define DEF_F_TC_SENSOR 0						//temperature sensor index to use for temperature compensation
#define DEF_F_TC_FACTOR 25.0f					//temperature compensation factor (um / �K)

#define DEF_F_MC_AVAILABLE false
#define DEF_F_MC_BTN_LOCK_AVAILABLE false		//true if local lock switch is available
#define DEF_F_MC_BTL_LOCK 255
#define DEF_F_MC_BTN_LOCK_INVERTED false		//true if switch is active high
#define DEF_F_MC_BTN_IN 255
#define DEF_F_MC_BTN_IN_INVERTED false			//true if switch is active high
#define DEF_F_MC_BTN_OUT 255
#define DEF_F_MC_BTN_OUT_INVERTED false			//true if switch is active high

#define DEF_F_TEMPERATURE_SENSOR_TYPE YAAASensor::SENSOR_TEMPERATURE
#define DEF_F_TEMPERATURE_SENSOR_DEVICE SensorTemperature::DEVICE_UNKOWN
#define DEF_F_TEMPERATURE_SENSOR_UNIT SensorTemperature::UNIT_CELSIUS
#define DEF_F_TEMPERATURE_SENSOR_PARAMETERS { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
//----------------------------------------------------------------------------------


//telescope default values
#define DEF_T_MOUNT_TYPE Telescope::mount_type_t::ALT_AZ

#define DEF_T_SITE_LATITUDE 47.0f
#define DEF_T_SITE_LONGITIDE 15.0f
#define DEF_T_SITE_ELEVATION 300.0f

#define DEF_T_FOCAL_LENGTH 1.2f
#define DEF_T_APERTURE_AREA 0.203f
#define DEF_T_APERTURE_DIAMETER 0.254f

#define DEF_T_PARK_ENABLED false
#define DEF_T_STARTUP_AUTO_HOME false

#define DEF_T_MC_AVAILABLE false
#define DEF_T_MC_BTN_TOGGLE_AVAILABLE false
#define DEF_T_MC_BTN_TOGGLE_PIN 255
#define DEF_T_MC_BTN_TOGGLE_INVERTED false
#define DEF_T_MC_BTN_SPEED_AVAILABLE false
#define DEF_T_MC_BTN_SPEED_PIN 255
#define DEF_T_MC_BTN_SPEED_INVERTED false
#define DEF_T_MC_JST_INPUT_X 255
#define DEF_T_MC_JST_INPUT_Y 255
#define DEF_T_MC_JST_DEADZONE 10

#define DEF_T_TEMPERATURE_SENSOR_TYPE YAAASensor::SENSOR_TEMPERATURE
#define DEF_T_TEMPERATURE_SENSOR_DEVICE SensorTemperature::DEVICE_UNKOWN
#define DEF_T_TEMPERATURE_SENSOR_UNIT SensorTemperature::UNIT_CELSIUS
#define DEF_T_TEMPERATURE_SENSOR_PARAMETERS { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

#define DEF_TA_MAX_SPEED 400.0f						//maximum stepper motor speed in steps / second
#define DEF_TA_ACCELERATION 100.0f					//maximum stepper motor speed in steps / second
#define DEF_TA_GEAR_RATIO 150.0f
#define DEF_TA_NUMBER_OF_STEPS 200					//stepper motor steps / revolution
#define DEF_TA_STEP_TYPE_FAST StepperMotor::STEPTYPE_FULL
#define DEF_TA_STEP_TYPE_SLOW StepperMotor::STEPTYPE_FULL

#define DEF_TA_HOME_SWITCH_ENABLED false
#define DEF_TA_HOME_SWITCH_PIN 255
#define DEF_TA_HOME_SWITCH_INVERTED false			//true if switch is active high
#define DEF_TA_HOME_ALIGNMENT 45.0f
#define DEF_TA_HOME_ALIGNMENT_SYNC false
#define DEF_TA_HOME_DIRECTION 1
#define DEF_TA_HOME_TIMEOUT 0L						//timeout for find home operation in ms. 0 to disable.
#define DEF_TA_STARTUP_AUTO_HOME false
#define DEF_TA_AUTO_STEPPER_RELEASE true			//true if the stepper motor should be deactivated automatically at the end of a movement

#define DEF_TA_PARK_POS 30.0f

#define DEF_TA_ENDSTOP_LOWER_ENABLED false
#define DEF_TA_ENDSTOP_LOWER_PIN 255
#define DEF_TA_ENDSTOP_LOWER_INVERTED false			//true if switch is active high
#define DEF_TA_ENDSTOP_UPPER_ENABLED false
#define DEF_TA_ENDSTOP_UPPER_PIN 255
#define DEF_TA_ENDSTOP_UPPER_INVERTED false			//true if switch is active high
#define DEF_TA_ENDSTOP_HARD_STOP true

#define DEF_TA_LIMIT_LOWER_ENDBLED true
#define DEF_TA_LIMIT_LOWER 0.0f							//movement limit in degrees
#define DEF_TA_LIMIT_UPPER_ENBALED true
#define DEF_TA_LIMIT_UPPER 90.0f						//movement limit in degrees
#define DEF_TA_LIMIT_UPPER_X 360.0f						//movement limit in degrees (for x axis only)
#define DEF_TA_LIMIT_HARD_STOP true
#define DEF_TA_LIMIT_HANDLING TelescopeAxis::LIMIT_HANDLING_ERROR

#define DEF_TA_MOTOR_DRIVER_TYPE StepperMotor::ADAFRUIT_MOTORSHIELD_V2
#define DEF_TA_I2C_ADDRESS 0x60
#define DEF_TA_TERMINAL 1
#define DEF_TA_PIN_1 255
#define DEF_TA_PIN_2 255
#define DEF_TA_PIN_3 255
#define DEF_TA_PIN_4 255
#define DEF_TA_PIN_5 255
#define DEF_TA_PIN_6 255
#define DEF_TA_PIN_1_INVERTED false				//true if switch is active high
#define DEF_TA_PIN_2_INVERTED false				//true if switch is active high
#define DEF_TA_PIN_3_INVERTED false				//true if switch is active high
#define DEF_TA_PIN_4_INVERTED false				//true if switch is active high
#define DEF_TA_PIN_5_INVERTED false				//true if switch is active high
#define DEF_TA_PIN_6_INVERTED false				//true if switch is active high


//filter wheel default values
#define DEF_W_MAX_SPEED 400.0f					//maximum stepper motor speed in steps / second
#define DEF_W_ACCELERATION 100.0f					//maximum stepper motor speed in steps / second
#define DEF_W_GEAR_RATIO 10.0f
#define DEF_W_NUMBER_OF_STEPS 200				//stepper motor steps / revolution
#define DEF_W_STEP_TYPE StepperMotor::STEPTYPE_FULL

#define DEF_W_HOME_SWITCH_ENABLED false
#define DEF_W_HOME_SWITCH_PIN 255
#define DEF_W_HOME_SWITCH_INVERTED false		//true if switch is active high
#define DEF_W_HOME_OFFSET 0L
#define DEF_W_STARTUP_AUTO_HOME false
#define DEF_W_AUTO_STEPPER_RELEASE true			//true if the stepper motor should be deactivated automatically at the end of a movement
#define DEF_W_HOME_TIMEOUT 0L					//timeout for find home operation in ms. 0 to disable.

#define DEF_W_ENDSTOP_LOWER_ENABLED false
#define DEF_W_ENDSTOP_LOWER_PIN 255
#define DEF_W_ENDSTOP_LOWER_INVERTED false		//true if switch is active high
#define DEF_W_ENDSTOP_UPPER_ENABLED false
#define DEF_W_ENDSTOP_UPPER_PIN 255
#define DEF_W_ENDSTOP_UPPER_INVERTED false		//true if switch is active high
#define DEF_W_ENDSTOP_HARD_STOP true

#define DEF_W_LIMIT_LOWER_ENDBLED true
#define DEF_W_LIMIT_LOWER 0						//movement limit in steps
#define DEF_W_LIMIT_UPPER_ENBALED true
#define DEF_W_LIMIT_UPPER 2000					//movement limit in steps
#define DEF_W_LIMIT_HARD_STOP true

#define DEF_W_MOTOR_DRIVER_TYPE StepperMotor::FOUR_WIRE_STEPPER
#define DEF_W_I2C_ADDRESS 0x62
#define DEF_W_TERMINAL 1
#define DEF_W_PIN_1 255
#define DEF_W_PIN_2 255
#define DEF_W_PIN_3 255
#define DEF_W_PIN_4 255
#define DEF_W_PIN_5 255
#define DEF_W_PIN_6 255
#define DEF_W_PIN_1_INVERTED false				//true if switch is active high
#define DEF_W_PIN_2_INVERTED false				//true if switch is active high
#define DEF_W_PIN_3_INVERTED false				//true if switch is active high
#define DEF_W_PIN_4_INVERTED false				//true if switch is active high
#define DEF_W_PIN_5_INVERTED false				//true if switch is active high
#define DEF_W_PIN_6_INVERTED false				//true if switch is active high

#define DEF_W_MC_AVAILABLE false
#define DEF_W_MC_BTN_LOCK_AVAILABLE false		//true if local lock switch is available
#define DEF_W_MC_BTN_LOCK 0xff
#define DEF_W_MC_BTN_LOCK_INVERTED false		//true if switch is active high
#define DEF_W_MC_BTN_NEXT_AVAILABLE false
#define DEF_W_MC_BTN_NEXT 0xff
#define DEF_W_MC_BTN_NEXT_INVERTED false			//true if switch is active high
#define DEF_W_MC_BTN_PREVIOUS_AVAILABLE false
#define DEF_W_MC_BTN_PREVIOUS 0xff
#define DEF_W_MC_BTN_PREVIOUS_INVERTED false			//true if switch is active high

#define DEF_W_NUMBER_OF_FILTERS 4
#define DEF_W_FILTER_HOLDER_TYPE FilterWheel::SLIDER

#define DEF_W_TEMPERATURE_SENSOR_TYPE YAAASensor::SENSOR_TEMPERATURE
#define DEF_W_TEMPERATURE_SENSOR_DEVICE SensorTemperature::SENSOR_UNKNOWN
#define DEF_W_TEMPERATURE_SENSOR_UNIT SensorTemperature::UNIT_CELSIUS
#define DEF_W_TEMPERATURE_SENSOR_PARAMETERS { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

#define DEF_W_FILTER_TYPE FilterWheel::FILTER_NONE
#define DEF_W_FILTER_RFU 0x00
#define DEF_W_FILTER_FOCUS_OFFSET 0L
#define DEF_W_FILTER_POSITION_OFFSET 0L

#endif /* DEFAULTS_H_ */

