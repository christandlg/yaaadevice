//Telescope Axis class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "TelescopeAxis.h"

TelescopeAxis::TelescopeAxis(uint8_t axis) :
	axis_(axis),
	automatic_stepper_release_(true),

	step_type_fast_(3), step_type_slow_(3),

	moving_(false),
	slewing_(false),

	homing_(false), home_found_(true),
	home_cleared_(false), homing_error_(TelescopeAxis::ERROR_HOMING_NONE),
	home_start_time_(0L), home_timeout_(0L),

	tracking_(false), tracking_rate_(0.0f), tracking_rate_offset_(0.0f),
	moveaxis_(false), moveaxis_offset_(0.0f),
	pulse_guiding_(false), pulse_guide_end_(0),
	target_(0.0f),

	endstop_lower_(NULL), endstop_upper_(NULL),

	endstop_home_switch_(false),
	home_switch_(NULL),

	limit_lower_enabled_(false), limit_upper_enabled_(false),
	limit_lower_(0), limit_upper_(0),
	limit_handling_(LIMIT_HANDLING_ERROR),

	endstop_state_(0), limit_state_(0),
	hard_stop_endstop_(true), hard_stop_limit_(true)
{
	uint8_t stepper_driver = StepperMotor::FOUR_WIRE_STEPPER;
	readEEPROM(EEPROMHandler::telescope_axis_variable_::MOTOR_DRIVER_TYPE, stepper_driver);

	uint16_t number_of_steps = 200;
	readEEPROM(EEPROMHandler::telescope_axis_variable_::NUMBER_OF_STEPS, number_of_steps);

	switch (stepper_driver)
	{
#ifndef ARDUINO_SAM_DUE
	case StepperMotor::ADAFRUIT_MOTORSHIELD_V2:
	{
		//read Motor Shield I2C Address and used terminal.
		uint8_t i2c_adress;
		uint8_t terminal;
		readEEPROM(EEPROMHandler::telescope_axis_variable_::I2C_ADDRESS, i2c_adress);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::TERMINAL, terminal);

		//stepper_motor_ = new StepperMotor(i2c_adress, terminal, number_of_steps);
		stepper_motor_ = new StepperMotorAFMSV2(i2c_adress, terminal, number_of_steps);
	}
	break;
#endif /* ARDUINO_SAM_DUE */
	case StepperMotor::FOUR_WIRE_STEPPER:
	{
		//read four wire stepper pins.
		uint8_t /*pin_enable,*/ pin1, pin2, pin3, pin4;
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_1, pin1);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_2, pin2);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_3, pin3);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_4, pin4);

		//stepper_motor_ = new StepperMotor(pin1, pin2, pin3, pin4);
		stepper_motor_ = new StepperMotorFourWire(pin1, pin2, pin3, pin4);

		//read motor driver pin inversion settings.
		bool pin1_inv, pin2_inv, pin3_inv, pin4_inv;
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_1_INVERTED, pin1_inv);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_2_INVERTED, pin2_inv);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_3_INVERTED, pin3_inv);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_4_INVERTED, pin4_inv);

		stepper_motor_->setPinsInverted(pin1_inv, pin2_inv, pin3_inv, pin4_inv, false);
	}
	break;
	case StepperMotor::DRIVER_A4988:
	case StepperMotor::DRIVER_DRV8825:
	{
		//read motor driver pins.
		uint8_t step, direction, enable, ms1, ms2, ms3;
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_1, step);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_2, direction);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_3, enable);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_4, ms1);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_5, ms2);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_6, ms3);

		stepper_motor_ = new StepperMotorDriver(stepper_driver, step, direction, enable, ms1, ms2, ms3);

		//read motor driver pin inversion settings.
		bool step_inv, direction_inv, enable_inv;
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_1_INVERTED, step_inv);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_2_INVERTED, direction_inv);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_3_INVERTED, enable_inv);

		stepper_motor_->setPinsInverted(direction_inv, step_inv, enable_inv);

		//read microstepping steptype pin inversion settings.
		bool ms1_inv, ms2_inv, ms3_inv;
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_4_INVERTED, ms1_inv);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_5_INVERTED, ms2_inv);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_6_INVERTED, ms3_inv);

		stepper_motor_->setMicroStepPinsInverted(ms1_inv, ms2_inv, ms3_inv);
	}
	break;
	default:
		stepper_motor_ = new StepperMotorFourWire(255, 255, 255, 255, false);
		break;
	}

	//failsafe.
	//call default constructor when an invalid driver type has been read from EEPROM.
	if (!stepper_motor_)
		return;

	stepper_motor_->release();

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	//read maximum speed and acceleration.
	float max_speed = 1.0f;
	float acceleration = 1.0f;
	readEEPROM(EEPROMHandler::telescope_axis_variable_::ACCELERATION, acceleration);
	readEEPROM(EEPROMHandler::telescope_axis_variable_::MAX_SPEED, max_speed);

	//set default stepper motor parameters. 
	//when using an Adafruit Motor Shield, maximum stepper speed is limited by I2C communication 
	//speed. maximum speed for all Adafruit Motorshield driven steppers combined is 700 - 1200 
	//steps / second (depending on steptype and I2C communication speed).  
	//independent of used stepper drivers, stepper speed should not exceed 1000 steps / second for
	//each individual stepper according to AccelStepper documentation ("Speeds of more than 1000 
	//steps per second are unreliable").
	//setting the maximum speed too high will result in skipped steps. 
	//acceleration is limited to prevent the motor from skipping steps when accelerating.
	stepper_motor_->setMaxSpeed(max_speed);				//speed in steps / second
	stepper_motor_->setAcceleration(acceleration);			//steps / second^2

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	//read gear ratio and calculate steps per degree
	float gear_ratio = 1.0f;
	readEEPROM(EEPROMHandler::telescope_axis_variable_::GEAR_RATIO, gear_ratio);

	steps_per_degree_ = static_cast<float>(number_of_steps)* gear_ratio / 360.0;

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------

	//read step type.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::STEPTYPE_FAST, step_type_fast_);
	readEEPROM(EEPROMHandler::telescope_axis_variable_::STEPTYPE_SLOW, step_type_slow_);

	//set fast steptype as default.
	stepper_motor_->setStepType(step_type_fast_);

	//read automatic stepper release setting.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::AUTOMATIC_STEPPER_RELEASE, automatic_stepper_release_);
	stepper_motor_->setAutomaticRelease(automatic_stepper_release_);

	//read home alignment of the telesocope axis.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_ALIGNMENT, home_alignment_);
	setCurrentAlignment(home_alignment_);					//telescope starts at the home position.

	readEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_ALIGNMENT_SYNC, home_alignment_sync_);

	//read park position.
	//float park_position;
	readEEPROM(EEPROMHandler::telescope_axis_variable_::PARK_POS, park_position_);

	//read x and y axis movement limit information from EEPROM.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_LOWER_ENABLED, limit_lower_enabled_);
	readEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_UPPER_ENABLED, limit_upper_enabled_);
	readEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_LOWER, limit_lower_deg_);
	readEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_UPPER, limit_upper_deg_);
	limit_lower_ = degreesToSteps(limit_lower_deg_);
	limit_upper_ = degreesToSteps(limit_upper_deg_);

	//read method to handle movement limit violations when setting a slew target.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_HANDLING, limit_handling_);

	//--------//--------//--------//--------//--------//--------//--------//--------//--------//--------
	//initialize the switches.
	uint8_t pin_nr = 255;
	bool inverted = 255;
	bool enabled = false;

	//endstop switches.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER, pin_nr);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER_INVERTED, inverted);
		endstop_lower_ = new Button(pin_nr, inverted);
	}

	readEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER, pin_nr);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER_INVERTED, inverted);
		endstop_upper_ = new Button(pin_nr, inverted);
	}

	//home position switch.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_SWITCH_ENABLED, enabled);
	if (enabled)
	{
		readEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_SWITCH, pin_nr);
		readEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_SWITCH_INVERTED, inverted);

		//if the pin number is the same as the lower endstops pin number:
		if (endstop_lower_ && (pin_nr == endstop_lower_->getPinNumber()))
		{
			home_switch_ = endstop_lower_;
			endstop_home_switch_ = true;
		}

		//if the pin number is the same as the upper endstops pin number:
		else if (endstop_upper_ && (pin_nr == endstop_upper_->getPinNumber()))
		{
			home_switch_ = endstop_upper_;
			endstop_home_switch_ = true;
		}

		//if none of the above, create a new button instance.
		else
		{
			home_switch_ = new Button(pin_nr, inverted);
			endstop_home_switch_ = false;
		}
	}

	//read timout for homing operation.
	readEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_TIMEOUT, home_timeout_);

	readEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_HARD_STOP, hard_stop_endstop_);
	readEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_HARD_STOP, hard_stop_limit_);
}

TelescopeAxis::~TelescopeAxis()
{
	delete stepper_motor_;

	if (endstop_lower_)
	{
		delete endstop_lower_;
		endstop_lower_ = NULL;
	}

	if (endstop_upper_)
	{
		delete endstop_upper_;
		endstop_upper_ = NULL;
	}

	//TODO check if the home switch is an endstop switch and, in this case, 
	//do not delete the home switch.
	if (home_switch_)
	{
		delete home_switch_;
		home_switch_ = NULL;
	}
}

void TelescopeAxis::run()
{
	//check endstops and movement limits.
	endstop_state_ = checkEndstops();
	limit_state_ = checkLimits();

	//find home position if requested.
	if (homing_)
		home();

	//when the telescope has finished a slewing operation, restore tracking, moveAxis and 
	//pulseGuide commands.
	else if (slewing_) slewing();

	////if pulse guiding is enabled for any axis, check the remaining pulse guiding time.
	//else if (pulse_guiding_) pulseGuiding();

	//moveAxis commands do not have stop criterions, nothing happens here.
	else if (moveaxis_) {}

	//tracking commands do not have stop criterions, nothing happens here.
	else if (tracking_) {}

	//step the stepper when a step is due. moving_ is true when the stepper is moving.
	//when the telescope is slewing or homing use run() (implements acceleration).
	if (slewing_ || homing_) // || parked_)
	{
		moving_ = stepper_motor_->run();
	}

	//use runSpeed() when Tracking, MoveAxis or PulseGuiding commands are active.
	else if (tracking_ || moveaxis_ || pulse_guiding_)
	{
		stepper_motor_->runSpeed();

		//runSpeed() only returns true when the stepper has actually been stepped, so a "manual" 
		//check if is necessary.
		moving_ = (stepper_motor_->speed() != 0.0);
	}

	//in any other case (e.g. AbortSlew has been called) use run()
	else
		moving_ = stepper_motor_->run();
}

void TelescopeAxis::stop()
{
	homing_ = false;								//disable homing command.

	abortSlew();									//disable slew and moveAxis commands.

	if (pulse_guiding_) pulse_guiding_ = false;		//disable pulse guiding command.

	tracking(false);								//disable tracking command.

	stop(false);									//disable any other movement (e.g. parking, homing).
}

void TelescopeAxis::stop(bool hard_stop)
{
	if (hard_stop)
		stepper_motor_->hardStop();

	else
		stepper_motor_->stop();
}

//--------------------------------------------------------------------------------------------------
//functions to return current movement / command state.
bool TelescopeAxis::isMoving()
{
	return moving_;
}

bool TelescopeAxis::isPulseGuiding()
{
	return pulse_guiding_;
}

bool TelescopeAxis::isSlewing()
{
	return slewing_;
}

bool TelescopeAxis::isMoveAxis()
{
	return moveaxis_;
}

bool TelescopeAxis::isTracking()
{
	return tracking_;
}

bool TelescopeAxis::atHome()
{
	//todo:
	//must only be set to true after a successful homing operation and needs to be reset with any 
	//slewing operation.

	bool return_value = true;

	//return false if homing is still in progress.
	if (homing_) return_value = false;

	//return false if the axis is not yet at its home position.
	else if (stepper_motor_->currentPosition() != degreesToSteps(home_alignment_))
		return_value = false;

	return return_value;
}

bool TelescopeAxis::isHoming()
{
	return homing_;
}

uint8_t TelescopeAxis::getHomingError()
{
	return homing_error_;
}

float TelescopeAxis::getHomePosition()
{
	return home_alignment_;
}

//functions to issue ASCOM commands.
uint8_t TelescopeAxis::slew(float target)
{
	uint8_t return_value = TelescopeAxis::ERROR_NONE;

	//check slew target
	if (!canSlewTo(target))
		return_value = TelescopeAxis::ERROR_OUT_OF_BOUNDS;

	//when no error occurred up to this point, set the new target.
	if (return_value == TelescopeAxis::ERROR_NONE)
	{
		//adjust target if necessary
		if (limit_handling_ == TelescopeAxis::LIMIT_HANDLING_ADJUST)
			constrainToLimits(target);

		//slew uses "fast" step type.
		setStepType(step_type_fast_);

		setNewTarget(degreesToSteps(target));
		slewing_ = true;
	}

	//bits of the response byte are set to true on errors.
	//possible return values are:
	//MSB - - - - - - LSB
	//	x x x x x x x x
	//	| | | | | | | |
	//	| | | | | | | - requested coordinate out of bounds.
	//	| | | | | | - not used.
	//	| | | | | -	not used.
	//	| | | | - not used.
	//	| | | -	not used.
	//	| | - not used.
	//	| - not used.
	//	- not used.
	return return_value;
}

bool TelescopeAxis::canSlewTo(float target)
{
	bool return_value = true;

	switch (limit_handling_)
	{
	case TelescopeAxis::LIMIT_HANDLING_IGNORE:
	case TelescopeAxis::LIMIT_HANDLING_ADJUST:
		break;
		//case TelescopeAxis::LIMIT_HANDLING_ERROR:
	default:
		//when limit handling is set to LIMIT_HANDLING_ERROR, raise an error when a target beyond 
		//the telescope's movement limits is given. 
		//this is the default behaviour when the settings saved to EEPROM are invalid.
		if (!withinLimits(target))
			return_value = false;
	}

	return return_value;
}

void TelescopeAxis::abortSlew()
{
	//disable moveAxis command. automatically restores tracking. does nothing when moveAxis 
	//command is not enabled.
	moveAxis(0.0);

	//when the telescope is slewing about this axis, stop the slew movement and 
	//restore tracking. 
	if (slewing_)
	{
		stop(false);

		slewing_ = false;

		restoreTracking();
	}
}

bool TelescopeAxis::canFindHome()
{
	if (!home_switch_)
		return false;

	return true;
}

void TelescopeAxis::findHome()
{
	//disable any other movements.
	stop();

	homing_error_ = TelescopeAxis::ERROR_HOMING_NONE;

	//initialize homing process.
	home_found_ = false;
	home_cleared_ = true;
	home_first_pos_found_ = false;

	//check if this axis can be homed.
	if (!canFindHome())
		homing_error_ |= TelescopeAxis::ERROR_HOMING_SWITCH_NOT_AVAILABLE;

	//check if the home position is beyond the axis' movement limits.
	if (limit_lower_enabled_ && (home_alignment_ < limit_lower_))
		homing_error_ |= TelescopeAxis::ERROR_HOMING_LIMIT_LOWER;

	if (limit_upper_enabled_ && (home_alignment_ > limit_upper_))
		homing_error_ |= TelescopeAxis::ERROR_HOMING_LIMIT_UPPER;

	if (homing_error_ != TelescopeAxis::ERROR_HOMING_NONE)
		return;

	long direction = 1L;

	//if the home switch is an endstop switch:
	if (endstop_home_switch_)
	{
		//if the home switch is the lower endstop switch, move downwards to find the 
		//home position.
		if (endstop_lower_ && (home_switch_->getPinNumber() == endstop_lower_->getPinNumber()))
		{
			direction = -1L;
		}

		//if the home switch is the upper endstop switch, move upwards to find the 
		//home position.
		if (endstop_upper_ && (home_switch_->getPinNumber() == endstop_upper_->getPinNumber()))
		{
			direction = 1L;
		}
	}

	//when the home switch is not an endstop switch, move towards the (assumed) 
	//home position.
	else
	{
		if (stepper_motor_->currentPosition() >= degreesToSteps(home_alignment_))
			direction = -1L;
		else direction = 1L;
	}


	//when the home / endstop switch is triggered, the home switch needs to be cleared 
	//first. to do this, the movement direction of the steppers is inverted.
	if (home_switch_->getState())
	{
		direction *= -1L;			//invert direction.

		home_cleared_ = false;
	}

	//activate "fast" steptype.
	setStepType(step_type_fast_);

	//set arbitrary target position for the stepper. do not adjust target to limits.
	setNewTarget(direction * LONG_MAX / 2);

	home_start_time_ = millis();

	//enable homing mode. 
	homing_ = true;
}

void TelescopeAxis::moveAxis(float speed, bool set_step_type)
{
	//when speed is 0.0 and moveAxis was active before:
	//disable moveAxis and restore motion of the telescope to the last specified tracking state.
	if (speed == 0.0)
	{
		//when moveAxis command was active, end command. 
		if (moveaxis_)
		{
			stop(true);			//stop stepper movement (hard stop).

			moveaxis_ = false;	//disable move axis command for given axis.

			//reset moveAxis offset.
			moveaxis_offset_ = 0.0f;
			moveaxis_rate_ = 0.0f;

			restoreTracking();
		}
	}

	//moveAxis command received with speed != 0.0: 
	//activate moveAxis command.
	else //if (speed != 0.0)
	{
		//disable pulseGuide command and enable moveAxis command for the given axis.
		//pulse_guiding_ = false;

		//check if one of the endstops or limits is triggered and prohibit setting a new speed
		//if necessary.
		if (endstop_state_ != 0 || limit_state_ != 0)
		{
			//abort if the lower endstop / limit is triggered and desired speed < 0.0.
			if (speed < 0.0)
			{
				if ((endstop_state_ & 1) != 0 || (limit_state_ & 1) != 0) return;
			}

			//abort if the upper endstop / limit is triggered and desired speed > 0.0.
			else // if (speed > 0.0)
			{
				if ((endstop_state_ & 2) != 0 || (limit_state_ & 2) != 0) return;
			}
		}

		moveaxis_ = true;

		//reset moveAxis offset.
		moveaxis_offset_ = 0.0f;
		moveaxis_rate_ = speed;

		//activate "fast" steptype.
		if (set_step_type)
			setStepType(step_type_fast_);

		//set new speed. speed is given in degrees / second.
		setSpeedDeg(moveaxis_rate_);
	}
}

void TelescopeAxis::park()
{
	//stop all movements.
	stop();

	//send telescope to park position. 
	//will fail if park position is outside the movement limits.
	slew(park_position_);
}

float TelescopeAxis::getPark()
{
	return park_position_;
}

void TelescopeAxis::setPark()
{
	//set the current position as the new park position.
	park_position_ = stepsToDegrees(stepper_motor_->currentPosition());

	//write these values to the EEPROM.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PARK_POS, park_position_);
}

bool TelescopeAxis::canPark()
{
	return canSlewTo(park_position_);
}

void TelescopeAxis::pulseGuide(uint8_t direction, uint32_t duration, float rate)
{
	if (direction == DIR_DOWN) 
		rate *= -1.0f;

	//activate pulseGuide command and save end time.
	pulse_guiding_ = true;
	pulse_guide_end_ = millis() + duration;

	//activate "slow" steptype.
	setStepType(step_type_slow_);

	//set the new speed. 
	setSpeedDeg(rate);
}

void TelescopeAxis::tracking(bool enable)
{
	if (enable)
	{
		tracking_ = true;
		pulse_guiding_ = false;	//disable pulseGuide command.

		//set speed to tracking rate's value and set "slow" tracking rate. only set speed and 
		//tracking rate when no other movement commands are active.
		if (!slewing_ && !moveaxis_)
		{
			setStepType(step_type_slow_);

			setSpeedDeg(tracking_rate_ + tracking_rate_offset_);
		}
	}

	else //if (!enable)
	{
		//do nothing when tracking is not active.
		if (tracking_)
		{
			tracking_ = false;

			//stop movement only if no other movement commands are active.
			if (!slewing_ && !moveaxis_)
				stop(true);
		}
	}
}

float TelescopeAxis::getTrackingRate()
{
	return tracking_rate_;
}

void TelescopeAxis::setTrackingRate(float rate)
{
	tracking_rate_ = rate;

	//apply updated tracking rate (does nothing if tracking is currently not enabled 
	//for this axis).
	restoreTracking();
}

float TelescopeAxis::getTrackingRateOffset()
{
	return tracking_rate_offset_;
}

void TelescopeAxis::setTrackingRateOffset(float rate)
{
	tracking_rate_offset_ = rate;

	//todo: when setting an offset rate of 0 when tracking rate is also 0 the stepper 
	//is not released.
	//keep like this for now, steppers will be released when tracking is disabled.

	restoreTracking();
}

//functions to get and set alignment, target and speed.
float TelescopeAxis::getCurrentAlignment()
{
	return stepsToDegrees(stepper_motor_->currentPosition());
}

void TelescopeAxis::setCurrentAlignment(float alignment)
{
	//when the home alignment can be synced and the axis is at its home position,
	//sync the home position, too.
	if (home_alignment_sync_ && atHome()) {
		home_alignment_ = alignment;
		writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_ALIGNMENT, alignment);
	}

	//convert degrees to steps and set current stepper position.
	//setCurrentPosition(degreesToSteps(alignment));
	stepper_motor_->setCurrentPosition(degreesToSteps(alignment));
}

float TelescopeAxis::getTarget()
{
	return target_;
}

bool TelescopeAxis::setTarget(float target)
{
	if (!canSlewTo(target))
		return false;

	target_ = target;

	return true;
}

float TelescopeAxis::getCurrentTarget()
{
	return stepsToDegrees(stepper_motor_->targetPosition());
}

float TelescopeAxis::getSpeed()
{
	return stepper_motor_->speed();
}

//conversion, limits and endstops.
int32_t TelescopeAxis::degreesToSteps(float degrees)
{
	float microstep_factor = static_cast<float>(stepper_motor_->getMicroStepFactor());

	return static_cast<long>(degrees * steps_per_degree_ * microstep_factor);
}

float TelescopeAxis::stepsToDegrees(int32_t steps)
{
	int32_t microstep_factor = stepper_motor_->getMicroStepFactor();

	float conversion_factor = steps_per_degree_ * static_cast<float>(microstep_factor);

	return static_cast<float>(steps) / conversion_factor;
}

float TelescopeAxis::getMaxSpeedDeg()
{
	return stepper_motor_->maxSpeed() / steps_per_degree_;
}

float TelescopeAxis::getMoveAxisOffset()
{
	return moveaxis_offset_;
}

bool TelescopeAxis::setMoveAxisOffset(float offset)
{
	if (!moveaxis_)
		return false;

	//store moveAxis offset speed.
	moveaxis_offset_ = offset;

	//set new speed. offset is given in degrees / second.
	setSpeedDeg(moveaxis_rate_ + moveaxis_offset_);

	return true;
}

bool TelescopeAxis::withinLimits(float target)
{
	return !(
		(limit_lower_enabled_ && target < limit_lower_deg_) ||
		(limit_upper_enabled_ && target > limit_upper_deg_));

	////when limit handling is set to LIMIT_HANDLING_IGNORE or LIMIT_HANDLING_ADJUST checking the 
	////limits is not necessary, return true.
	//if (limit_handling_ == LIMIT_HANDLING_IGNORE || limit_handling_ == LIMIT_HANDLING_ADJUST)
	//	return true;
	//
	////when limits are enabled, check if the target is within the limits. 
	//else if ((limit_lower_enabled_ && target < limit_lower_deg_) ||
	//	(limit_upper_enabled_ && target > limit_upper_deg_))
	//	return false;
	//
	////when no limits are enabled or the target is within the limits return true.
	//else return true;
}

uint8_t TelescopeAxis::reportSwitches()
{
	uint8_t return_value = 0;

	//bit 1 contains the state of the lower endstop switch.
	if (endstop_lower_)
		return_value |= static_cast<uint8_t>(endstop_lower_->getState());

	//bit 2 contains the state of the upper endstop switch.
	if (endstop_upper_)
		return_value |= static_cast<uint8_t>(endstop_upper_->getState()) << 1;

	//bit 3 contains the state of the home switch.
	if (home_switch_)
		return_value |= static_cast<uint8_t>(home_switch_->getState()) << 2;

	return return_value;
}

//--------------------------------------------------------------------------------------------------
//private functions.

//limits and endstops.
float TelescopeAxis::constrainToLimits(float target)
{
	//adjust target to movement limits if necessary.
	if (limit_lower_enabled_ && target < limit_lower_deg_)
		target = limit_lower_deg_;

	if (limit_upper_enabled_ && target > limit_upper_deg_)
		target = limit_upper_deg_;

	return target;
}

uint8_t TelescopeAxis::checkLimits()
{
	uint8_t return_value = 0;
	bool harmless = true;		//true if not moving or moving away from the limit.
	long limit = 0;

	//when the current position is below the lower limit:
	if (limit_lower_enabled_ && (stepper_motor_->currentPosition() <= limit_lower_))
	{
		return_value += 1;

		limit = limit_lower_;

		//when a command using run() is active the target position and the movement direction 
		//have to be checked (to catch axis decelerating beyond the limits).
		if (slewing_ || homing_)
		{
			//when the axis is slewing to a target above the current position, 
			//an action is required
			if (stepper_motor_->distanceToGo() < 0)
				harmless = false;

			//when hard stopping is enabled and the axis is moving upwards (even if
			//the target is below the current position) an action is required.
			if (hard_stop_limit_ && stepper_motor_->speed() < 0.0f)
				harmless = false;
		}

		//when any other command (e.g. using runSpeed()) is active and the axis is 
		//moving upwards an action is required
		else if (stepper_motor_->speed() < 0.0)
			harmless = false;
	}

	//when the current position is at or above the upper limit:
	if (limit_upper_enabled_ && (stepper_motor_->currentPosition() >= limit_upper_))
	{
		return_value += 2;

		limit = limit_upper_;

		//when a command using run() is active the target position and the movement direction 
		//have to be checked (to catch axis decelerating beyond the limits).
		if (slewing_ || homing_)
		{
			//when the axis is slewing to a target above the current position, 
			//an action is required
			if (stepper_motor_->distanceToGo() > 0)
				harmless = false;

			//when hard stopping is enabled and the axis is moving upwards (even if
			//the target is below the current position) an action is required.
			if (hard_stop_limit_ && stepper_motor_->speed() > 0.0f)
				harmless = false;
		}

		//when any other command (e.g. using runSpeed()) is active and the axis is 
		//moving upwards an action is required
		else if (stepper_motor_->speed() > 0.0)
			harmless = false;
	}

	if (!harmless)
	{
		//when homing is active and the home position has not yet been found, reverse the stepper 
		//direction. does not end homing process.
		if (homing_ && !home_found_)
		{
			//execute a hard stop (if allowed). do not use stop(true) as it would reset the stepper 
			//target.
			if (hard_stop_limit_)
				stepper_motor_->setSpeed(0.0);

			//reverse stepper target. homing target is always +- LONG_MAX / 2.
			setNewTarget(-1L * stepper_motor_->targetPosition());
		}

		//in any other case, stop the stepper.
		else
		{
			stop();			//disables homing, slewing, moveAxis, pulse guiding and tracking 

			if (hard_stop_limit_)
				stop(true);				//execute a hard stop if allowed.

			setNewTarget(limit);		//move the stepper to its movement limit.

			slewing_ = true;
		}
	}

	return return_value;
}

uint8_t TelescopeAxis::checkEndstops()
{
	uint8_t return_value = 0;	//the return value holds endstop state (regardless of actions).
	bool harmless = true;		//false when an action is required.

	//if the lower endstop switch is triggered:
	if (endstop_lower_ && (endstop_lower_->getState() == true))
	{
		return_value += 1;

		//when a command using run() is active the target position and the movement direction 
		//have to be checked (to catch axis decelerating beyond the limits).
		if (slewing_ || homing_)
		{
			//when the axis is slewing to a target above the current position, 
			//an action is required
			if (stepper_motor_->distanceToGo() < 0)
				harmless = false;

			//when hard stopping is enabled and the axis is moving upwards (even if
			//the target is below the current position) an action is required.
			if (hard_stop_limit_ && stepper_motor_->speed() < 0.0f)
				harmless = false;
		}

		//when any other command (e.g. using runSpeed()) is active and the axis is 
		//moving upwards an action is required
		else if (stepper_motor_->speed() < 0.0)
			harmless = false;
	}

	//if the upper endstop switch is triggered:
	if (endstop_upper_ && endstop_upper_->getState() == true)
	{
		return_value += 2;

		//when a command using run() is active the target position and the movement direction 
		//have to be checked (to catch axis decelerating beyond the limits).
		if (slewing_ || homing_)
		{
			//when the axis is slewing to a target above the current position, 
			//an action is required
			if (stepper_motor_->distanceToGo() > 0)
				harmless = false;

			//when hard stopping is enabled and the axis is moving upwards (even if
			//the target is below the current position) an action is required.
			if (hard_stop_limit_ && stepper_motor_->speed() > 0.0f)
				harmless = false;
		}

		//when any other command (e.g. using runSpeed()) is active and the axis is 
		//moving upwards an action is required
		else if (stepper_motor_->speed() > 0.0)
			harmless = false;
	}


	if (!harmless)		//an action is required.
	{
		//when homing is active and the home position has not yet been found, reverse the stepper 
		//direction. does not end homing process.
		if (homing_ && !home_found_)
		{
			//execute a hard stop (if allowed). do not use stop(true) as it would reset the stepper 
			//target.
			if (hard_stop_endstop_)
				stepper_motor_->setSpeed(0.0);

			//reverse stepper target. homing target is always +- LONG_MAX / 2.
			setNewTarget(-1L * stepper_motor_->targetPosition());
		}

		//in any other case, stop the stepper. (slew, moveAxis, tracking, ...)
		else
		{
			stop();			//disables homing, slewing, moveAxis, pulse guiding and tracking 

			if (hard_stop_endstop_)
				stop(true);						//execute a hard stop (if allowed).	

			//set a new target for the stepper and activate slewing. this will move 
			//the axis to the current position.
			//else
			setNewTarget(stepper_motor_->currentPosition());

			slewing_ = true;
		}
	}

	return return_value;
}

void TelescopeAxis::setSpeed(float speed)
{
	stepper_motor_->setSpeed(speed);
}

void TelescopeAxis::setSpeedDeg(float speed)
{
	setSpeed(speed * steps_per_degree_ * stepper_motor_->getMicroStepFactor());
}

//--------------------------------------------------------------------------------------------------
//movement control functions.
bool TelescopeAxis::atDestination()
{
	return (stepper_motor_->distanceToGo() == 0) && (stepper_motor_->speed() == 0.0);
}

void TelescopeAxis::setNewTarget(int32_t target)
{
	stepper_motor_->moveTo(target);
}

void TelescopeAxis::clearHome()
{
	//check the state of the home switches.
	bool home_triggered = home_switch_->getState();

	//when the switch is no longer pressed the home position has been cleared.
	if (!home_triggered)
	{
		//invert direction of the stepper.
		setNewTarget(-1 * stepper_motor_->targetPosition());

		home_cleared_ = true;
	}
}

void TelescopeAxis::home()
{
	//find home timeout check
	if ((home_timeout_ != 0L) && (millis() - home_start_time_ > home_timeout_))
	{
		homing_ = false;
		homing_error_ = TelescopeAxis::ERROR_HOMING_TIMEOUT;
		stop(hard_stop_limit_);
		return;
	}

	//the home switches need to be cleared before homing can begin.
	if (!home_cleared_)
		clearHome();

	else if (!home_found_)
	{
		long location = stepper_motor_->currentPosition();
		bool home_triggered = home_switch_->getState();

		//if the home switch is an endstop switch:
		if (endstop_home_switch_)
		{
			if (home_triggered)
			{
				//the home position has been found. set the current position as the new 
				//destination for the stepper.
				setNewTarget(location);

				home_found_ = true;
			}
		}

		//if the home switch is not an endstop switch, the home position is the position 
		//between the two positions where the switch changes its state.
		else
		{
			//when the home switch is triggered and the first position where the switch is 
			//pressed has not yet been found:
			if (home_triggered && !home_first_pos_found_)
			{
				home_first_pos_found_ = true;
				home_first_pos_ = location;
			}

			//when the switch is released and the first position where the switch is pressed 
			//has been found before:
			if (!home_triggered && home_first_pos_found_)
			{
				//calculate center position and set new stepper targets.
				setNewTarget((home_first_pos_ + location) / 2);

				home_found_ = true;
			}
		}
	}

	//when the home position has been found but the telescope has not yet reached the home position: 
	//do nothing here, just continue moving the telescope in the main loop.
	else if (moving_)
	{
		//Serial.println("moving"); 
	}

	//when the home position has been found and the telescope has reached the home position, 
	//synchronize the home position and set homing_ to false.
	else if (!moving_)
	{
		//set stepper position to the home position.
		setCurrentAlignment(home_alignment_);

		//end homing process.
		homing_ = false;
	}
}

void TelescopeAxis::restoreTracking()
{
	//reactivate tracking for this axis only when the telescope is not 
	//moving in response to a slew or moveAxis command.
	if (tracking_ && !slewing_ && !moveaxis_) tracking(true);
}

void TelescopeAxis::pulseGuiding()
{
	//todo: rework pulseGuide code.
	if (!pulse_guiding_) return;

	//check if pulse guiding duration has expired.
	if (pulse_guiding_ && pulse_guide_end_ <= millis())
	{
		//stop the motion.
		stop(true);

		//disable pulse guiding for x axis.
		pulse_guiding_ = false;

		////restore tracking and moveAxis commands.
		//restoreTracking();
		//restoreMoveAxis();
	}
}

void TelescopeAxis::slewing()
{
	//when the stepper has reached its target, restore tracking.
	if (atDestination())
	{
		slewing_ = false;

		restoreTracking();
	}
}

bool TelescopeAxis::setStepType(uint8_t step_type)
{
	bool return_value = stepper_motor_->setStepType(step_type);

	//adjust limit positions.
	limit_lower_ = degreesToSteps(limit_lower_deg_);
	limit_upper_ = degreesToSteps(limit_upper_deg_);

	return return_value;
}

void TelescopeAxis::writeDefaults(uint8_t axis)
{
	//write default maximum speed.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::MAX_SPEED, axis, DEF_TA_MAX_SPEED);

	//write default acceleration. 
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ACCELERATION, axis, DEF_TA_ACCELERATION);

	//write default step type.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::STEPTYPE_FAST, axis, static_cast<uint8_t>(DEF_TA_STEP_TYPE_FAST));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::STEPTYPE_SLOW, axis, static_cast<uint8_t>(DEF_TA_STEP_TYPE_SLOW));

	//write default number of steps.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::NUMBER_OF_STEPS, axis, static_cast<uint16_t>(DEF_TA_NUMBER_OF_STEPS));

	//write default gear ratio.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::GEAR_RATIO, axis, DEF_TA_GEAR_RATIO);

	//write default automatic stepper release behaviour.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::AUTOMATIC_STEPPER_RELEASE, axis, static_cast<byte>(DEF_TA_AUTO_STEPPER_RELEASE));

	//write default home switch information.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_SWITCH_ENABLED, axis, static_cast<byte>(DEF_TA_HOME_SWITCH_ENABLED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_SWITCH, axis, static_cast<uint8_t>(DEF_TA_HOME_SWITCH_PIN));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_SWITCH_INVERTED, axis, static_cast<byte>(DEF_TA_HOME_SWITCH_INVERTED));
	//writeEEPROM(EEPROMHandler::telescope_axis_variable_::STARTUP_AUTO_HOME, axis, static_cast<byte>(DEF_TA_STARTUP_AUTO_HOME));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_ALIGNMENT, axis, DEF_TA_HOME_ALIGNMENT);
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_TIMEOUT, axis, DEF_TA_HOME_TIMEOUT);
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOMING_DIRECTION, axis, static_cast<uint8_t>(DEF_TA_HOME_DIRECTION));

	writeEEPROM(EEPROMHandler::telescope_axis_variable_::HOME_ALIGNMENT_SYNC, axis, static_cast<uint8_t>(DEF_TA_HOME_ALIGNMENT_SYNC));

	//write default parking position.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PARK_POS, axis, DEF_TA_PARK_POS);

	//write default endstop information.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER_ENABLED, axis, static_cast<byte>(DEF_TA_ENDSTOP_LOWER_ENABLED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER, axis, static_cast<uint8_t>(DEF_TA_ENDSTOP_LOWER_PIN));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_LOWER_INVERTED, axis, static_cast<byte>(DEF_TA_ENDSTOP_LOWER_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER_ENABLED, axis, static_cast<byte>(DEF_TA_ENDSTOP_UPPER_ENABLED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER, axis, static_cast<uint8_t>(DEF_TA_ENDSTOP_UPPER_PIN));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_UPPER_INVERTED, axis, static_cast<byte>(DEF_TA_ENDSTOP_UPPER_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::ENDSTOP_HARD_STOP, axis, static_cast<byte>(DEF_TA_ENDSTOP_HARD_STOP));

	//write default limit information.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_LOWER_ENABLED, axis, static_cast<byte>(DEF_TA_LIMIT_LOWER_ENDBLED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_LOWER, axis, DEF_TA_LIMIT_LOWER);
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_UPPER_ENABLED, axis, static_cast<byte>(DEF_TA_LIMIT_UPPER_ENBALED));
	if (axis == 0)
		writeEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_UPPER, axis, DEF_TA_LIMIT_UPPER_X);
	else
		writeEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_UPPER, axis, DEF_TA_LIMIT_UPPER);
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_HARD_STOP, axis, static_cast<byte>(DEF_TA_LIMIT_HARD_STOP));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::LIMIT_HANDLING, axis, static_cast<uint8_t>(DEF_TA_LIMIT_HANDLING));

	//write default motor driver type.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::MOTOR_DRIVER_TYPE, axis, static_cast<uint8_t>(DEF_TA_MOTOR_DRIVER_TYPE));

	//write default Adafruit Motor Shield settings.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::I2C_ADDRESS, axis, static_cast<uint8_t>(DEF_TA_I2C_ADDRESS));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::TERMINAL, axis, static_cast<uint8_t>(DEF_TA_TERMINAL));

	//write default Four Wire Stepper / Stepper Driver settings.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_1, axis, static_cast<uint8_t>(DEF_TA_PIN_1));		//setting a non- existent pin.
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_2, axis, static_cast<uint8_t>(DEF_TA_PIN_2));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_3, axis, static_cast<uint8_t>(DEF_TA_PIN_3));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_4, axis, static_cast<uint8_t>(DEF_TA_PIN_4));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_5, axis, static_cast<uint8_t>(DEF_TA_PIN_5));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_6, axis, static_cast<uint8_t>(DEF_TA_PIN_6));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_1_INVERTED, axis, static_cast<byte>(DEF_TA_PIN_1_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_2_INVERTED, axis, static_cast<byte>(DEF_TA_PIN_2_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_3_INVERTED, axis, static_cast<byte>(DEF_TA_PIN_3_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_4_INVERTED, axis, static_cast<byte>(DEF_TA_PIN_4_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_5_INVERTED, axis, static_cast<byte>(DEF_TA_PIN_5_INVERTED));
	writeEEPROM(EEPROMHandler::telescope_axis_variable_::PIN_6_INVERTED, axis, static_cast<byte>(DEF_TA_PIN_6_INVERTED));
}
