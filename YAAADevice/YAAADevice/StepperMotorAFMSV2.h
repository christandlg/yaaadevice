//Adafruit Motor Shield V2 (wrapper) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef STEPPERMOTOR_AFMSV2_H_
#define STEPPERMOTOR_AFMSV2_H_

#ifndef ARDUINO_SAM_DUE

#include <Wire.h>
#include <Adafruit_MotorShield.h>

#include "StepperMotor.h"

class StepperMotorAFMSV2 :
	public StepperMotor
{
public:
	//constructor for class using Adafruit Motor Shield V2.
	//@param:
	// - I2C address of the motor shield.
	// - terminal on the motor shield.
	// - number of steps of the stepper motor.
	StepperMotorAFMSV2(uint8_t I2CAddress, uint8_t terminal, uint16_t number_of_steps);

	~StepperMotorAFMSV2();

	//activates the stepper by setting PWM outputs to 100%.
	void activate();

	//releases the stepper motor.
	void release();

	//overrides AccelStepper's function run().
	//sets current step type and current stepper when using Adafruit Motor Shield, calls 
	//automaticRelease() and calls StepperMotor's run() function.
	bool run();

	//overrides AccelStepper's function runSpeed().
	//sets current step type and current stepper when using Adafruit Motor Shield, calls 
	//automaticRelease() and calls StepperMotor's runSpeed() function.
	bool runSpeed();

	//overrides AccelStepper's function runSpeedToPosition().
	//sets current step type and current stepper when using Adafruit Motor Shield, calls 
	//automaticRelease() and calls StepperMotor's runSpeedToPosition() function.
	bool runSpeedToPosition();

	//@return: the currently set step type as step_type_t step type.
	uint8_t getStepType();

	//sets the current steptype to the steptype provided and recalculates stepper position when
	//steptype has changed. 
	//@return: 
	// - true when the step type was set successfully.
	// - false when an invalid step type has been given for this stepper driver.
	bool setStepType(uint8_t step_type);

private:

	Adafruit_MotorShield *AFMS_;			//Adafruit Motor Shield instance.
	Adafruit_StepperMotor *stepper_;		//Adafruit Motor Shield Stepper Motor instance.

	static uint8_t current_step_type_;					//steptype for the adafruit motor shield stepper.
	static Adafruit_StepperMotor *current_stepper_;		//the current motor.	

	static void stepperForwardStep();		//forward step function for adafruit motor shield driven steppers.
	static void stepperBackwardStep();		//backward step function for adafruit motor shield driven steppers.
};

#endif /* ARDUINO_SAM_DUE */

#endif /* STEPPERMOTOR_AFMSV2_H_ */

