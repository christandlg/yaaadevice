//Stepper Motor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef STEPPERMOTOR_H_
#define STEPPERMOTOR_H_

#include <Arduino.h>

//#include <Wire.h>
//#include <Adafruit_MotorShield.h>
#include <AccelStepper.h>

#define QUICKSTEP 5							//special (non- adafruit motorshield) steptype 

class StepperMotor : public AccelStepper
{
public:
	enum stepper_driver_t : uint8_t
	{
		ADAFRUIT_MOTORSHIELD_V2 = 0,	//stepper is driven by an Adafruit Motor Shield V2.
		FOUR_WIRE_STEPPER = 1,			//stepper is a simple 4 wire stepper.
		DRIVER_A4988 = 2,				//stepper is driven by a A4988 stepper driver.
		DRIVER_DRV8825 = 3				//stepper is driven by a DRV8825 stepper driver.
	};

	enum step_type_t : uint8_t
	{
		STEPTYPE_QUICKSTEP = 0,				//for Adafruit Motor Shield V2.
		STEPTYPE_SINGLE = 1,				//for Adafruit Motor Shield V2.
		//STEPTYPE_DOUBLE = 2,				//for Adafruit Motor Shield V2.
		STEPTYPE_FULL = 3,					//for stepper drivers and Adafruit Motor Shield V2 (DOUBLE).
		STEPTYPE_MICROSTEP_2 = 4,			//for stepper drivers and Adafruit Motor Shield V2.
		STEPTYPE_MICROSTEP_4 = 5,			//for stepper drivers.
		STEPTYPE_MICROSTEP_8 = 6,			//for stepper drivers.
		STEPTYPE_MICROSTEP_16 = 7,			//for stepper drivers and Adafruit Motor Shield V2.
		STEPTYPE_MICROSTEP_32 = 8,			//for stepper drivers.
		STEPTYPE_MICROSTEP_64 = 9,			//for stepper drivers.
		STEPTYPE_MICROSTEP_128 = 10			//for stepper drivers.
	};
	
	//constructor for class using callback functions.
	//@param:
	// - callback function that steps 1 step forward (clockwise)
	// - callback function that steps 1 step backwards (counterclockwise)
	StepperMotor(void(*forward)(), void(*backward)());

	//constructor for class using 4 wire stepper. 
	//@param:
	// - digital output pins pin1, pin2, pin3, pin4 controlling the stepper motor.
	// - if the output pins will be enabled at construction time. defaults to false.
	StepperMotor(uint8_t pin1 , uint8_t pin2, uint8_t pin3, uint8_t pin4, bool enable);

	//constructor for class using stepper drivers.
	//@param:
	// - digital output pins step, diection and enable_pin controlling the stepper motor.
	// - digital output pins ms1, ms2, ms3 to set microstepping steptype.
	// - if the output pins will be enabled at construction time. defaults to false.
	StepperMotor(uint8_t driver, uint8_t step, uint8_t direction, uint8_t enable_pin, uint8_t ms1, uint8_t ms2, uint8_t ms3, bool enable);

	virtual ~StepperMotor();

	//overrides AccelStepper's function run(). 
	//calls automaticRelease() and calls AccelStepper's run() function.
	virtual bool run();

	//overrides AccelStepper's function runSpeed().
	//calls automaticRelease() and calls AccelStepper's runSpeed() function.
	virtual bool runSpeed();

	//overrides AccelStepper's function runSpeedToPosition().
	//calls automaticRelease() and calls AccelStepper's runSpeedToPosition() function.
	virtual bool runSpeedToPosition();

	//stops the stepper motor instantly. implementation is kind of hack-ish because 
	//AccelStepper does not support bypassing acceleration.
	virtual void hardStop();

	//activates the stepper. 
	//default behaviour: calls AccelStepper::enableOutputs() and sets 
	//stepper_released_ to false.
	//when overriding remember to set stepper_released_ to false.
	virtual void activate();

	//releases the stepper motor. derived classes must implement this function.
	//default behaviour: calls AccelStepper::disableOutputs() and sets 
	//stepper_released_ to true.
	//when overriding remember to set stepper_released_ to true.
	virtual void release();

	//sets automatic stepper release to the provided state.
	void setAutomaticRelease(bool enabled);

	/*
	@return: true if the stepper is at its destination (distance to go == 0 and speed == 0),
	false otherwise.*/
	virtual bool atDestination();

	//@return: the currently set step type as step_type_t step type. derived classes 
	//may override (see StepperMotorAFMSV2 class).
	//default behaviour: returns step type stored in step_type_.
	virtual uint8_t getStepType();

	//sets the current steptype to the steptype provided and recalculates stepper 
	//position when steptype has changed. derived classes must override.
	//default behaviour: returns false to indicate that the step type was not set.
	//@return: 
	// - true when the step type was set successfully.
	// - false when an invalid step type has been given for this stepper driver.
	virtual bool setStepType(uint8_t step_type) = 0;

	//@param: step type to return microstep factor for. when not given, the current 
	//step type is used.
	//@return: the microstep factor of the given (or current) step type (= number 
	//of micro steps in a full step).
	long getMicroStepFactor(uint8_t step_type = 255);

	//sets the inversion settings for the microstepping steptype pins.
	virtual void setMicroStepPinsInverted(bool ms1_inv = false, bool ms2_inv = false, bool ms3_inv = false);

protected:
	uint8_t stepper_driver_;				//this axis' stepper driver type.

	uint8_t step_type_;						//currently active step type.

	bool stepper_released_;					//true if the stepper is currently released.

private:
	bool automatic_stepper_release_;		//true if the stepper should be automatically released when not moving.

	/*
	automatically releases the stepper when automatic_stepper_release_ is true and the stepper 
	has reached its destination or the speed has been set to 0.*/
	void automaticRelease();
};

#endif /* STEPPERMOTOR_H_ */

