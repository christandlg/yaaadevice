//Driver type Stepper motor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "StepperMotorDriver.h"

StepperMotorDriver::StepperMotorDriver(uint8_t driver, uint8_t step, uint8_t direction, uint8_t enable_pin, uint8_t ms1, uint8_t ms2, uint8_t ms3, bool enable) :
	StepperMotor(driver, step, direction, enable_pin, ms1, ms2, ms3, enable),
	pin_ms1_(ms1), pin_ms2_(ms2), pin_ms3_(ms3),
	pin_ms1_inverted_(0), pin_ms2_inverted_(0), pin_ms3_inverted_(0)
{
	//set the enable pin for the driver.
	AccelStepper::setEnablePin(enable_pin);

	//set the steptype pins to OUTPUT mode.
	pinMode(ms1, OUTPUT);
	pinMode(ms2, OUTPUT);
	pinMode(ms3, OUTPUT);

	//initialize MICROSTEP SELECT outputs
	step_type_ = STEPTYPE_SINGLE;

	setStepType(STEPTYPE_FULL);
}

StepperMotorDriver::~StepperMotorDriver()
{
}

bool StepperMotorDriver::setStepType(uint8_t step_type)
{
	//preserve currently set step type (for stepper position adjustment).
	uint8_t prev_step_type = getStepType();

	if (step_type == prev_step_type)
		return true;

	uint8_t pin_1_value = LOW;
	uint8_t pin_2_value = LOW;
	uint8_t pin_3_value = LOW;

	switch(stepper_driver_) 
	{
		case StepperMotor::DRIVER_A4988:
			switch (step_type)
			{
				case STEPTYPE_FULL:
					pin_1_value = LOW;
					pin_2_value = LOW;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_2:
					pin_1_value = HIGH;
					pin_2_value = LOW;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_4:
					pin_1_value = LOW;
					pin_2_value = HIGH;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_8:
					pin_1_value = HIGH;
					pin_2_value = HIGH;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_16:
					pin_1_value = HIGH;
					pin_2_value = HIGH;
					pin_3_value = HIGH;
					break;
				default:
					//return false when an incorrect steptype has been given.
					return false;
			}
			break;
		case StepperMotor::DRIVER_DRV8825:
			switch (step_type)
			{
				case STEPTYPE_FULL:
					pin_1_value = LOW;
					pin_2_value = LOW;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_2:
					pin_1_value = HIGH;
					pin_2_value = LOW;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_4:
					pin_1_value = LOW;
					pin_2_value = HIGH;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_8:
					pin_1_value = HIGH;
					pin_2_value = HIGH;
					pin_3_value = LOW;
					break;
				case STEPTYPE_MICROSTEP_16:
					pin_1_value = LOW;
					pin_2_value = LOW;
					pin_3_value = HIGH;
					break;
				case STEPTYPE_MICROSTEP_32:
					pin_1_value = LOW;		//LOW  | HIGH | HIGH
					pin_2_value = HIGH;		//HIGH | LOW  | HIGH
					pin_3_value = HIGH;		//HIGH | HIGH | HIGH
					break;
				default:
					//return false when an incorrect steptype has been given.
					return false;
			}
			break;
		default:
			return false;
	}

	//save the set step type (as step_type_t steptype).
	step_type_ = step_type;

	//set pins according to requested steptype.
	digitalWrite(pin_ms1_, pin_1_value ^ pin_ms1_inverted_);
	digitalWrite(pin_ms2_, pin_2_value ^ pin_ms2_inverted_);
	digitalWrite(pin_ms3_, pin_3_value ^ pin_ms3_inverted_);

	//recalculate stepper position.
	//changing from a higher microstep factor to a lower factor might result in loss of pointing 
	//accuracy.
	long next_factor = getMicroStepFactor(step_type);
	long prev_factor = getMicroStepFactor(prev_step_type);

	long updated_position = AccelStepper::currentPosition() * next_factor / prev_factor;
	AccelStepper::setCurrentPosition(updated_position);

	return true;
}

void StepperMotorDriver::setMicroStepPinsInverted(bool ms1_inv, bool ms2_inv, bool ms3_inv)
{
	pin_ms1_inverted_ = static_cast<uint8_t>(ms1_inv);
	pin_ms2_inverted_ = static_cast<uint8_t>(ms2_inv);
	pin_ms3_inverted_ = static_cast<uint8_t>(ms3_inv);
}
