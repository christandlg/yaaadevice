//Keypad class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAAKEYPAD_H_
#define YAAAKEYPAD_H_

#include <Arduino.h>

#include "YAAAKey.h"

class YAAAKeypad
{
public:
	enum keypad_type_t : uint8_t
	{
		TYPE_NONE = 0,		//no keypad
		TYPE_MATRIX = 1,	//matrix keypad
		TYPE_I2C = 2,		//kaypad is connected via I2C interface
		TYPE_SPI = 3,		//keypad is connected via SPI interface
		TYPE_TWI = 4,		//keypad is connected via generic two wire interface (e.g. TTP229)
		TYPE_GENERIC = 5,	//keypad is a collection of individual buttons.
		TYPE_UNKNOWN = 255
	};
	
	//struct containing keypad settings
	//parameters array is keypad type specific:
	//Generic:
	// - (1 byte) key code
	// - (1 byte) pin number
	// - (1 byte) key code
	// - (1 byte) pin number
	// - (1 byte) key code
	// - (1 byte) pin number
	// - (1 byte) key code
	// - (1 byte) pin number
	// - (1 byte) key code
	// - (1 byte) pin number
	// - (1 byte) key code
	// - (1 byte) pin number
	//TWI:
	// - (1 byte) SCL pin
	// - (1 byte) SDO pin
	// - rest is RFU
	//Matrix:
	// - (1 byte) Layout
	// - (1 byte) RFU
	// - (4 x 1 byte) pin numbers of row input pins
	// - (4 x 1 byte) pin numbers or column input pins
	// - rest is RFU
	struct KeypadSettings
	{
		uint8_t type_;
		uint8_t active_high_;
		uint8_t parameters_[12];
		uint32_t read_interval_;
	};

	YAAAKeypad(keypad_type_t keypad_type);

	~YAAAKeypad();

	/*
	@return YAAAKey ojbect which contains the currently pressed key and key state.
	MUST be implemented by derived class.*/
	virtual YAAAKey getKey() = 0;

	/*
	@return: the keypad type as YAAAKeypad::keypad_type_t.*/
	uint8_t getType();

	/*
	@return: true if a read is due (last read is older than the read interval specified for 
	this keypad)*/
	bool isReadDue();

protected:
	/*
	updates the state and code of class member key_.
	@param key_code current key code (as YAAAKey::key_t) */
	void updateKey(uint8_t key_code);

	uint8_t keypad_type_;		//type of keypad as YAAAKeypad::keypad_type_t.

	uint32_t read_interval_;	//timeout between reads. 
	uint32_t read_last_;		//time (in ms) of last read.

	uint32_t press_start_;		//time (in ms) when the key was pressed.
	uint32_t hold_interval_;	//if a key is held longer than this interval, the key state is changed to "hold".

	YAAAKey key_;				//YAAAKey object containing currently pressed key and state.
};

#endif /* YAAAKEYPAD_H_ */

