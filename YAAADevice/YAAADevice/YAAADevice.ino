//YAAADevice main file.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include <Arduino.h>

//OneWire library is needed for OneWire Temperature sensors.
#include <OneWire.h>

#include "EEPROMHandler.h"

#include "YAAATime.h"

#include "YAAALog.h"

#include "YAAASensor.h"

#include "OTAHandler.h"

#include "InfoDisplay.h"

//Class needed for Communication between PC and Arduino.
#include "RemoteCommandHandler.h"

//this class handles all manual control input.
#include "ManualControl.h"
//------------------------------------------------

//ASCOM Devices
#include "Telescope.h"
#include "FilterWheel.h"
#include "Focuser.h"
//#include "Dome.h"
//#include "CameraRotator.h"
//#include "SafetyMonitor.h"

//------------------------------------------------
//Global Varables. 

OneWire *one_wire_;

//ASCOM Devices:
//telescope mount
Telescope *telescope_ = NULL;

//filter wheel
FilterWheel *filter_wheel_ = NULL;

//camera rotator
//CameraRotator *camera_rotator_ = NULL;

//dome
//Dome *dome_ = NULL;

//focuser
Focuser *focuser_ = NULL;

//safety monitor
//SafetyMonitor *safety_monitor_ = NULL;
//------------------------------------------------

//create logger
YAAALog log_("YAAA.log");

void writeAllDefaults()
{
	//this function initializes the Arduino's EEPROM with default values. 

	Display::writeDefaults();

	RemoteCommandHandler::writeDefaults();

	ManualControl::writeDefaults();

	InfoDisplay::writeDefaults();

	//Dome::writeDefaults();

	//CameraRotator::writeDefaults();

	FilterWheel::writeDefaults();

	Focuser::writeDefaults();

	Telescope::writeDefaults();

	//SafetyMonitor::writeDefaults();

#if defined(ESP8266) || defined(ESP32)
	OTAHandler::writeDefaults();
#endif

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::CAMERA_ROTATOR_AVAILABLE,
		0,
		static_cast<byte>(DEF_G_CAMERA_ROTATOR_ENABLED));

	EEPROMHandler::writeValue
	(EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DOME_AVAILABLE,
		0,
		static_cast<byte>(DEF_G_DOME_ENABLED));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::FILTER_WHEEL_AVAILABLE,
		0,
		static_cast<byte>(DEF_G_FILTER_WHEEL_ENABLED));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::FOCUSER_AVAILABLE,
		0,
		static_cast<byte>(DEF_G_FOCUSER_ENABLED));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SAFETY_MONITOR_AVAILABLE,
		0,
		static_cast<byte>(DEF_G_SAFETY_MONITOR_ENABLED));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::TELESCOPE_AVAILABLE,
		0,
		static_cast<byte>(DEF_G_TELESCOPE_ENABLED));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_CS_PIN,
		0,
		static_cast<uint8_t>(DEF_G_SD_CS_PIN));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_LOG,
		0,
		static_cast<uint8_t>(DEF_G_SD_LOG));

#if defined(ESP8266)
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SCL,
		0,
		D3);

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SDA,
		0,
		D2);
#elif defined(ESP32)
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SCL,
		0,
		SCL);

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SDA,
		0,
		SDA);
#endif

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ONEWIRE_PIN,
		0,
		static_cast<uint8_t>(DEF_G_ONEWIRE_PIN));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::TIME_UTC_OFFSET,
		0,
		DEF_G_TIME_UTC_OFFSET);

	//reset eeprom control number.
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::EEPROM_CONTROL,
		0,
		DEF_G_EEPROM_CONTROL);
}

void loop() {
	// put your main code here, to run repeatedly:

	remote_command_handler_.run();

	//process local user input
	manual_control_.run();

	//move telescope.
	//if (telescope_available)
	if (telescope_)
		telescope_->run();

	//move filterwheel.
	if (filter_wheel_)
		filter_wheel_->run();

	if (focuser_)
		focuser_->run();

	//display information on display.
	info_display_.run();

#if defined(ESP8266) || defined(ESP32)
	OTAHandler::run();
#endif /* defined(ESP8266) || defined(ESP32) */
}

void setup() {
	// put your setup code here, to run once:

	EEPROMHandler::begin();

	uint32_t eeprom_control_number = 0L;

	//check eeprom control number to determine if the EEPROM needs to be initialized with 
	//default values.
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::EEPROM_CONTROL,
		0,
		eeprom_control_number);

	//write defaults to the EEPROM if necessary.
	if (eeprom_control_number != DEF_G_EEPROM_CONTROL)
		writeAllDefaults();

#if defined(ARDUINO_SAM_DUE)
	analogReadResolution(10);
#endif

	//I2C SCL and SDA pins are configurable on ESP8266 based boards.
#if defined(ESP8266) || defined(ESP32)
	uint8_t i2c_pin_scl = 255;
	uint8_t i2c_pin_sda = 255;

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SCL,
		0,
		i2c_pin_scl);

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::I2C_PIN_SDA,
		0,
		i2c_pin_sda);
	Wire.begin(i2c_pin_sda, i2c_pin_scl);
#else
  //other Arduinos (Mega, Due) use fixed pins for SCL / SDA
	Wire.begin();
#endif

	YAAATime::begin();

	//read and set offset from UTC.
	float time_utc_offset = 0.0f;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::TIME_UTC_OFFSET,
		0,
		time_utc_offset);
	YAAATime::setUTCOffset(time_utc_offset);

	//read SD chip select pin and initialize log class
	uint8_t sd_chip_select = 255;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_CS_PIN,
		0,
		sd_chip_select);
	YAAALog::initSD(sd_chip_select);

	log_.begin();

	//read log setting and set logging
	bool sd_log_enabled = false;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SD_LOG,
		0,
		sd_log_enabled);
	log_.setLogMode(sd_log_enabled);

	remote_command_handler_.begin();

#if defined(ESP8266) || defined(ESP32)
	OTAHandler::begin();
#endif /* defined(ESP8266) || defined(ESP32) */

	//read onewire pin number
	uint8_t onewire_pin = 255;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ONEWIRE_PIN,
		0,
		onewire_pin);
	one_wire_ = new OneWire(onewire_pin);

	//read which devices are available.
	bool enabled = false;

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::CAMERA_ROTATOR_AVAILABLE,
		0,
		enabled);
	//if (enabled)
	//	camera_rotator_ = new CameraRotator();

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DOME_AVAILABLE,
		0,
		enabled);
	//if (enabled)
	//	dome_ = new Dome();

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::FILTER_WHEEL_AVAILABLE,
		0,
		enabled);
	if (enabled)
		filter_wheel_ = new FilterWheel();

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::FOCUSER_AVAILABLE,
		0,
		enabled);
	if (enabled)
		focuser_ = new Focuser();

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SAFETY_MONITOR_AVAILABLE,
		0,
		enabled);
	//if (enabled)
	//	safety_monitor_ = new SafetyMonitor();

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::TELESCOPE_AVAILABLE,
		0,
		enabled);
	if (enabled)
		telescope_ = new Telescope();

	//initialize display.
	info_display_.init();

	//create manual control handler (control with local input devices)
	manual_control_.init();

	//Change the i2c clock to 400kHz.
	//increases maximum stepper motor steps / second when using Adafruit Motor Shield.
	//must be done *after* initializing the motor shields with Adafruit_MotorShield::begin().
	//https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/faq
//#ifndef ARDUINO_SAM_DUE
	//TWBR = ((F_CPU / 400000l) - 16) / 2;
//#else
	Wire.setClock(400000L);		//TODO MLX90614 cannot operate at 400kHz.
}
