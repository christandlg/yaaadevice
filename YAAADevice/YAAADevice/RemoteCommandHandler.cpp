//RemoteCommandHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "RemoteCommandHandler.h"

#include "EEPROMHandler.h"
#include "Telescope.h"
#include "FilterWheel.h"
#include "Focuser.h"

#include "constants.h"

RemoteCommandHandler::RemoteCommandHandler() :
	device_(NULL),
	command_(NULL),
	value_(NULL),
	command_interface_current_(INTERFACE_SERIAL),
	lx200_precision_(false),
	lx200_protocol_precision_(false),
	lx200_time_format_12hr_(false)
{
	for (uint8_t i = 0; i < NUM_COMMAND_INTERFACES; i++)
	{
		command_interfaces_[i] = NULL;
		input_buffers_[i] = "";
	}
}

RemoteCommandHandler::~RemoteCommandHandler()
{
	for (uint8_t i = 0; i < NUM_COMMAND_INTERFACES; i++)
	{
		delete command_interfaces_[i];
		input_buffers_[i] = "";		//not actually necessary
	}
}

void RemoteCommandHandler::begin()
{
	//Serial command interface - always present
	command_interfaces_[INTERFACE_SERIAL] = new CommandInterfaceSerial();
	command_interfaces_[INTERFACE_SERIAL]->begin();

	//ethernet command interface
#ifdef FEATURE_ETHERNET
#if !defined(ESP8266) && !defined(ESP32)
	bool ethernet_enabled = false;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::ETHERNET_ENABLED,
		0,
		ethernet_enabled);

	if (ethernet_enabled) {
		command_interfaces_[INTERFACE_ETHERNET] = new CommandInterfaceEthernet();
		command_interfaces_[INTERFACE_ETHERNET]->begin();
		input_buffers_[INTERFACE_ETHERNET] = "";
	}
#endif /* !defined(ESP8266) && !defined(ESP32) */
#endif /* FEATURE_ETHERNET */

	//WIFI command interface
#ifdef FEATURE_WIFI
#if defined(ESP8266) || defined(ESP32)
	bool wifi_enabled = false;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_ENABLED,
		0,
		wifi_enabled);
	if (wifi_enabled)
	{
		command_interfaces_[INTERFACE_WIFI] = new CommandInterfaceWifi();
		command_interfaces_[INTERFACE_WIFI]->begin();
		input_buffers_[INTERFACE_WIFI] = "";
	}
#endif /* defined(ESP8266) || defined(ESP32) */
#endif /* FEATURE_WIFI */

	//	//LoRa command interface
	//#ifdef FEATURE_LoRa
	//	bool lora_enabled = false;
	//
	//	EEPROMHandler::readValue(
	//		EEPROMHandler::device_type_::GENERAL,
	//		EEPROMHandler::general_variable_::LoRa_ENABLED,
	//		0,
	//		lora_enabled);
	//	if (lora_enabled)
	//	{
	//		command_interfaces_[INTERFACE_LoRa] = new CommandInterfaceLoRa();
	//		command_interfaces_[INTERFACE_LoRa]->begin();
	//		input_buffers_[INTERFACE_LoRa] = "";
	//	}
	//#endif /* FEATURE_LoRa */
}

void RemoteCommandHandler::run()
{
	switch (command_interface_current_)
	{
	case INTERFACE_SERIAL:
	case INTERFACE_ETHERNET:
	case INTERFACE_WIFI:
	case INTERFACE_LoRa:
		//fetch command from interface
		fetchCommand(command_interface_current_);

		//if command is complete: execute
		if (extractCommand(input_buffers_[command_interface_current_]))
			executeCommand();
#ifdef FEATURE_LX200
		else if (extractCommandLX200(input_buffers_[command_interface_current_]))
			executeLX200();
#endif /* FEATURE_LX200 */

		//next command interface on next iteration
		command_interface_current_++;

		break;
	default:
		//reset counter
		command_interface_current_ = INTERFACE_SERIAL;
		break;
	}
}

bool RemoteCommandHandler::isInterfaceEnabled(uint8_t interface)
{
	switch (interface)
	{
	case RemoteCommandHandler::INTERFACE_SERIAL:
	case RemoteCommandHandler::INTERFACE_ETHERNET:
	case RemoteCommandHandler::INTERFACE_WIFI:
		if (command_interfaces_[interface])
			return true;
	default:
		break;
	}

	return false;
}

//--------------------------------------------------------------------------------------------------
//private functions.

bool RemoteCommandHandler::extractCommand(String & in)
{
	//check if input string is empty
	if (in.length() == 0)
		return false;

	int16_t start = -1;
	int16_t end = -1;

	//check if input string contains a start of message character
	//check if the message protocol is YAAADEVICE
	start = in.indexOf(COMMAND_START);
	end = in.indexOf(COMMAND_END);

	//return false if no start of command character or end of command character has been received
	//or the end of command character has a smaller index than the start of command character (wtf?)
	if ((start < 0) || (end < 0) || (end < start))
		return false;

	//if the message contains an end of message character, copy the message 
	//to out. clear the in string from the beginning until the position of the 
	//end of message char (which should be empty anyway). 
	//end position character is not in the string retured by substring, so calling
	//substring with 'end + 1' is necessary
	String out = in.substring(start, end + 1);		//copy string to out
	in.remove(0, end + 1);					//clear in. 

	memcpy(input_, out.c_str(), out.length());

	return true;
}

#ifdef FEATURE_LX200
bool RemoteCommandHandler::extractCommandLX200(String & in)
{
	//check if input string is empty
	if (in.length() == 0)
		return false;

	int16_t start = -1;
	int16_t end = -1;

	//check if input string contains a start of message character
	//check if the message protocol is YAAADEVICE
	start = in.indexOf(COMMAND_LX200_START);
	end = in.indexOf(COMMAND_LX200_END);

	//return false if no start of command character or end of command character has been received
	//or the end of command character has a smaller index than the start of command character (wtf?)
	if ((start < 0) || (end < 0) || (end < start))
		return false;

	//if the message contains an end of message character, copy the message 
	//to out. clear the in string from the beginning until the position of the 
	//end of message char (which should be empty anyway). 
	//end position character is not in the string retured by substring, so calling
	//substring with 'end + 1' is necessary
	String out = in.substring(start, end + 1);		//copy string to out
	in.remove(0, end + 1);					//clear in. 

	memcpy(input_, out.c_str(), out.length());

	return true;
}
#endif /* FEATURE_LX200 */

void RemoteCommandHandler::fetchCommand(uint8_t interface)
{
	//do nothing if command interface is not initialized. 
	if (!isInterfaceEnabled(interface))
		return;

	//when a byte is in the interface's input buffer, read it into the local input buffer.
	//'<' marks the start of a new message. 
	//'>' marks the end of a message.
	//maximum message length is 128 bytes.
	while (command_interfaces_[interface]->available())
	{
		//read 1 byte from the interface
		char character = static_cast<char>(command_interfaces_[interface]->read());

		//reset input buffer if a start of command character was received. 
		if (strchr(COMMAND_START, character) != NULL)
			input_buffers_[interface] = "";		//reset input String

		input_buffers_[interface] += character;

		//exit loop if an end of command character was received. 
		if (strchr(COMMAND_END, character) != NULL)
			break;

		//reset input buffer if it is full.
		if (input_buffers_[interface].length() >= COMMAND_MAX_LENGTH)
			input_buffers_[interface] = "";
	}
}

void RemoteCommandHandler::executeCommand()
{
	//get pointer to target device identifier within input string.
	device_ = tokenize(input_);

	//get pointer to command identifier within input string.
	command_ = tokenize(NULL);

	switch (getDeviceType(device_))
	{
	case RemoteCommandHandler::GENERAL:
		generalCommand();
		break;
	case RemoteCommandHandler::CAMERA_ROTATOR:
		cameraRotatorCommand();
		break;
	case RemoteCommandHandler::DOME:
		domeCommand();
		break;
	case RemoteCommandHandler::FILTER_WHEEL:
		filterWheelCommand();
		break;
	case RemoteCommandHandler::FOCUSER:
		focuserCommand();
		break;
	case RemoteCommandHandler::SAFETY_MONITOR:
		safetyMonitorCommand();
		break;
	case RemoteCommandHandler::TELESCOPE:
		telescopeCommand();
		break;
	default:
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_DEVICE);
		break;
	}
}

#ifdef FEATURE_LX200
void RemoteCommandHandler::executeLX200()
{
	//LX200 protocol:
	switch (input_[1])
	{
	case 0x06:		//alignment query
	{
		char return_value = '\0';

		//returns:
		//A - AltAz mode
		//D - If scope is currently engaged by the Autostar Updater Program
		//L - Land mode
		//P - Polar mode
		switch (telescope_->getMountType())
		{
		case Telescope::ALT_AZ:
			return_value = 'A';
			break;
		case Telescope::GERMAN_EQUATORIAL:
		case Telescope::EQUATORIAL:
			return_value = 'P';
			break;
		}

		if (return_value != '\0')
			sendRaw(return_value);
	}
	return;

	case 'A':		//alignment commands
	{
		switch (input_[2])
		{
		case 'a':	//start telescope automatic alignment sequence
			//todo implement
			//returns:
			//0 - scope not AltAz or align fails
			//1 - success
			return;
		case 'L':	//set telescope to land alignment mode (no site data or alignment)
			//todo implement
			//returns nothing
			return;
		case 'P':	//set telescope to polar alignment mode
			//todo implement
			//returns nothing
			return;
		case 'A':	//set telescope to AltAz alignment mode
			//todo implement
			//returns nothing
			return;
		}
	}
	return;

	case 'B':	//reticule / accessory control
	{
		switch (input_[2])
		{
		case '+':	//increase reticule brightness
			//todo implement
			//returns nothing
			return;
		case '-':	//decrease reticule brightness
			//todo implement
			//returns nothing
			return;
		case 'D':	//set reticule duty flash cycle to n (0 ... 9)
		{
			//todo implement
			uint8_t duty_cycle = static_cast<uint8_t>(atoi(extractLX200String(input_, 3, 2).c_str()));

			//returns nothing
		}
		return;

		default:	//set reticule flash rate to n (0 ... 9)
		{
			//todo implement
			uint8_t flash_rate = charToInt(input_[2]);

			//returns nothing
		}
		return;
		}
	}
	return;

	case 'C':		//sync control
	{
		switch (input_[2])
		{
		case 'L':	//sync telescope to current selenographic coordinates
			//todo implement
			//returns nothing
			return;
		case 'M':	//sync telescope to currently selected database objects coordinates
			//todo implement
			//returns: name of object
			return;
		}
	}
	return;

	case 'D':		//distance bars
	{
		//returns: a string of bars indicating the distance to the current target location
		//or 1 bar if the telescope is currently slewing
		if (telescope_->isSlewing())
			sendRaw(0x7f);
	}
	return;

	case 'f':		//fan / heater commands
	{
		switch (input_[2])
		{
		case '+':	//turn on tube exhaust fan
			//todo implement
			//returns nothing
			return;
		case '-':	//turn off tube exhaust fan
			//todo implement
			//returns nothing
			return;
		case 'H':	//set corretor plate heater level (in percent)
		{
			//char array holding 3 numbers + end of string
			String number = extractLX200String(input_, 3, 3);

			uint8_t percent = static_cast<uint8_t>(strtoul(number.c_str(), NULL, DEC));

			if (percent > 100)		//ignore the command if the percent given is > 100
				return;

			//todo implement

			//returns nothing
		}
		return;

		case 'p':
		{
			switch (input_[3])
			{
			case '+':	//turn on switched 12v panel power	
				//returns nothing	
				return;
			case '-':	//turn off switched 12v panel power	
				//returns nothing	
				return;
			}
		}
		return;

		case 'T':	//return optical tube assembly temperature
		{
			//Returns <sdd.ddd>#  - a '#' terminated signed ASCII real number indicating the Celsius ambient temperature.
			//telescope_->measureTemperature(0);

			//while (!telescope_->getTemperatureComplete(0))
			//	delay(100);

			float temp = telescope_->getTemperature(0);

			String return_value = "";

			if (temp >= 0.0f)
				return_value = "+";

			return_value += String(temp, 3);
			return_value += "#";

			sendRaw(return_value);
		}
		return;

		case 'C':	//return corrector plate temperature
		{
			//Returns <sdd.ddd>#  - a '#' terminated signed ASCII real number indicating the Celsius ambient temperature.

			float temp = telescope_->getTemperature(1);

			String return_value = "";

			if (temp >= 0.0f)
				return_value = "+";

			return_value += String(temp, 3);
			return_value += "#";

			sendRaw(return_value);
		}
		return;
		}
	}
	return;

	case 'F':		//focuser control
	{
		switch (input_[2])
		{
		case '+':	//start moving focuser inward (positive direction).
			focuser_->moveDirection(true);
			//returns nothing
			break;;

		case '-':	//start moving focuser outward (negative direction).
			focuser_->moveDirection(false);
			//returns nothing
			break;

		case 'B':	//query focuser busy status
		{
			//returns: 
			//1 - focuser moving
			//0 - focuser idle
			uint8_t return_value = (focuser_->isMoving() ? 1 : 0);

			sendRaw(return_value);
		}
		break;

		case 'C':	//collimator command.	
		{
			uint8_t direction = 255;

			switch (input_[3])
			{
			case 'n':	//move north
				direction = Telescope::NORTH;
				break;
			case 's':	//move south:
				direction = Telescope::SOUTH;
				break;
			case 'e':	//move east
				direction = Telescope::EAST;
				break;
			case 'w':	//move west
				direction = Telescope::WEST;
				break;
			}

			if (direction == 255)
				return;

			//todo implement

			//returns nothing	
		}
		return;

		case 'L':
		{
			//focuser preset
			uint8_t preset_nr = charToInt(input_[4]);

			switch (input_[3])
			{
			case 'D':	//define current position as preset		
			{
				uint8_t preset = charToInt(input_[4]);

				//preset numbers are (1 ... 9)
				if (preset == 0)
					return;

				//todo implement

				//returns nothing	
			}
			return;

			case 'N':	//assign name to preset	
			{
				uint8_t preset = charToInt(input_[4]);

				//preset numbers are (1 ... 9)
				if (preset == 0)
					return;

				String name = extractLX200String(input_, 5, 16);

				//todo implement

				//returns nothing	
			}
			return;

			case 'S':	//sync focuser to preset position <n>
			{
				uint8_t preset = charToInt(input_[4]);

				//preset numbers are (1 ... 9)
				if (preset == 0)
					return;

				//todo implement

				//returns nothing		
			}
			return;
			}
		}
		return;

		case 'P':	//pulse focuser
		{
			String time = extractLX200String(input_, 3, 6);

			int32_t time_millis = strtol(time.c_str(), NULL, DEC);

			focuser_->moveTime(time_millis);
		}
		return;

		case 'p':	//query position
		{
			//returns: a '#' terminated ASCII integer which is the current position.
			String return_value = String(focuser_->getPosition());
			sendRaw(return_value + '#');
		}
		break;

		case 'Q':	//halt motion
			focuser_->halt();
			break;

		case 'F':	//set focuser speed to fastest setting
			focuser_->setSpeed(Focuser::SPEED_FASTEST);
			return;
		case 'S':	//set focuser speed to slowest setting
			focuser_->setSpeed(Focuser::SPEED_SLOWEST);
			return;
		default:	//set focuser speed to n
		{
			uint8_t speed = charToInt(input_[2]);

			//speed settings are (1 ... 4)
			if ((speed == 0) || speed > 4)
				return;

			Focuser::speed_t speed_setting = Focuser::SPEED_FASTEST;

			switch (speed)
			{
			case 1:
				speed_setting = Focuser::SPEED_SLOWEST;
				break;
			case 2:
				speed_setting = Focuser::SPEED_SLOW;
				break;
			case 3:
				speed_setting = Focuser::SPEED_FAST;
				break;
				//case 4:
				//	speed_setting = Focuser::SPEED_SLOWEST;
				//	break;
			}

			focuser_->setSpeed(speed_setting);
		}
		return;
		}
	}
	return;

	case 'g':		//gps / magnetometer commands
	{
		switch (input_[2])
		{
		case '+':	//turn on gps
			//todo implement
			//returns nothing	
			return;
		case '-':	//turn off gps		
			//todo implement
			//returns nothing	
			return;
		case 'p':	//output NMEA GPS data stream
		{
			if (input_[3] != 's')	//command != ':gps#'
				return;

			//returns: NMEA GPS data stream + '#'
		}
		return;

		case 'T':	//turn on gps and update system time.
			//returns:
			//0 - gps timeout or user interrupted
			//1 - success
			return;
		}
	}
	return;

	case 'G':		//get telescope information
	{
		switch (input_[2])
		{
		case '0':	//get alignment menu entry 0
			//returns: # terminated ascii string
			return;
		case '1':	//get alignment menu entry 0
			//returns: # terminated ascii string
			return;
		case '2':	//get alignment menu entry 0
			//returns: # terminated ascii string
			return;
		case 'A':	//get telescope altitude.
		{
			if (telescope_->getMountType() != Telescope::ALT_AZ)
				return;

			float alignment = telescope_->getCurrentAlignment(TelescopeAxis::Y_AXIS);

			String angle = Telescope::degreesToDMS(alignment, lx200_protocol_precision_, false);

			sendRaw(angle + "#");
			//returns: sDD*MM or sDD*MM'SS (depends on precision setting) + '#'
		}
		return;

		case 'a':	//get telescope time (12h format)
		{
			//returns: time as HH:MM:SS (12h format) + '#'
			char *return_value = new char[10];
			strcat(YAAATime::timeToHMS(return_value, true), "#");
			sendRaw(return_value);
			delete return_value;
		}
		return;

		case 'b':	//get browse brighter magnitude limit
			//returns: sMM.M (magnitude of faintest object returned by find/browse command) + '#'
			return;
		case 'C':	//get current date.
		{
			//returns: current date in MM/DD/YY format + '#'
			char *return_value = new char[10];
			strcat(YAAATime::timeToMDY(return_value, '/'), "#");
			sendRaw(return_value);
			delete return_value;
		}
		return;

		case 'c':	//get time format
		{
			//returns: 12# or 24# depending on settings.
			String return_value = "";

			if (lx200_time_format_12hr_)
				return_value += 12;
			else
				return_value += 24;

			sendRaw(return_value + "#");
		}
		return;
		case 'D':	//get telescope declination
		{
			if (telescope_->getMountType() == Telescope::ALT_AZ)
				return;

			float alignment = telescope_->getCurrentAlignment(TelescopeAxis::Y_AXIS);

			String angle = Telescope::degreesToDMS(alignment, lx200_protocol_precision_, false);

			sendRaw(angle + "#");
			//returns: sDD*MM or sDD*MM'SS (depends on precision setting) + '#'
		}
		return;

		case 'd':	//get currently selected object / target declination
		{
			if (telescope_->getMountType() == Telescope::ALT_AZ)
				return;

			float alignment = telescope_->getTarget(TelescopeAxis::Y_AXIS);

			String angle = Telescope::degreesToDMS(alignment, lx200_protocol_precision_, false);

			//returns: sDD*MM or sDD*MM'SS (depends on precision setting) + '#'
			sendRaw(angle + "#");
		}
		return;

		case 'E':	//get selenographic latitude
			//returns: sDD*MM#	(+99*99# when not pointed at the moon)
			return;
		case 'e':	//get selenographic longitude
			//returns: sDDD*MM# (+999*99# when not pointed at the moon)
			return;
		case  'F':	//find field diameter
			//returns: ascii int expressing the diameter of the field search (NNN#)
			return;
		case 'f':	//get browse faint magnitude limit
			//returns: sMM.M (magnitude of brightest object returned by find/browse command) + '#'
			return;
		case 'G':	//get UTC offset time
		{
			//returns: sHH (if offset is a whole number) or sHH.H + '#' 
			String return_value = "";

			float offset_hrs = YAAATime::getUTCOffset();

			//add sign
			if (offset_hrs >= 0.0f)
				return_value += "+";
			else
				return_value += "-";

			//expected: 2 decimal places.
			if (abs(offset_hrs) < 10.0f)
				return_value += "0";

			//print hours with fractional part if necessary.
			if (offset_hrs - static_cast<int16_t>(offset_hrs) == 0.0f)
				return_value += String(static_cast<int16_t>(abs(offset_hrs)));
			else
				return_value += String(abs(offset_hrs), 1);

			sendRaw(return_value + "#");
		}
		return;

		case 'g':	//get current site longitude
		{
			//returns: sDDD*MM#
			String return_value = Telescope::degreesToDMS(telescope_->getSiteLongitude(), false, true);

			sendRaw(return_value + "#");
		}
		return;

		case 'H':	//get daylight savings time
			//returns: 
			//1 - in effect
			//0 - not in effect
			return;
		case 'h':	//get elevation high limit 
			//returns: sDD*
			return;
		case 'L':	//get local time in 24h format
		{
			char *return_value = new char[10];
			strcat(YAAATime::timeToHMS(return_value, false), "#");
			sendRaw(return_value);
			delete return_value;
		}
		return;

		case 'l':	//get larger size limit
			//returns: NNN'# (the size of the smallest object returned by browse / find)
			return;
		case 'M':	//get site 1 name
			//returns: A '#' terminated string with the name of the requested site.
			return;
		case 'N':	//get site 2 name
			//returns: A '#' terminated string with the name of the requested site.
			return;
		case 'O':	//get site 3 name
			//returns: A '#' terminated string with the name of the requested site.
			return;
		case 'P':	//get site 4 name
			//returns: A '#' terminated string with the name of the requested site.
			return;
		case 'o':	//get elevation low limit
			//returns: sDD*
			return;
		case 'q':	//get minimim quality for find operation
			//returns: the minimum quality of object returned by the find command
			//SU# - super
			//EX# - excellent
			//VG# - very good
			//GD# - good
			//FR# - fair
			//PR# - poor
			//VP# - very poor
			return;
		case 'R':	//get telescope's RA
		{
			if (telescope_->getMountType() == Telescope::ALT_AZ)
				return;

			float alignment = telescope_->getCurrentAlignment(TelescopeAxis::X_AXIS);

			String angle = Telescope::degreesToHMS(alignment, lx200_protocol_precision_);

			//returns: HH:MM.T or HH:MM:SS (depends on precision setting) + '#'
			sendRaw(angle + "#");
		}
		return;

		case 'r':	//get current target / object RA
		{
			if (telescope_->getMountType() == Telescope::ALT_AZ)
				return;

			float target = telescope_->getTarget(TelescopeAxis::X_AXIS);

			String angle = Telescope::degreesToHMS(target, lx200_protocol_precision_);

			//returns: HH:MM.T or HH:MM:SS (depends on precision setting) + '#'
			sendRaw(angle + "#");
		}
		return;

		case 'S':	//get sidereal time
			//returns: sidereal time as an ASCII sexidecimal value as HH:MM:SS (24h format) + '#'
			return;
		case 's':	//get smaller size limit
			//returns: NNN'# (the size of the largest object returned by browse / find)
			return;
		case 'T':	//get tracking rate
		{
			//returns: TT.T# (current track frequency in Hz. 60Hz = 1 rev / 24h)

			//get currently set tracking rate
			uint8_t current = telescope_->getTrackingRate();

			//get currently set rate in arcseconds / second
			float rate = abs(telescope_->getWellKnownTrackingRate(current));

			//get currently set right ascension tracking rate offset (in arcseconds / sidereal second)
			float offset = telescope_->getRightAscensionRate();

			//convert offset to arcseconds /second
			offset *= 0.9972695677;

			rate += offset;

			//convert to "motor clocks" (60Hz = 15 arcseconds / second)
			rate *= 60.0f / 15.0f;

			//send response.
			sendRaw(String(rate, 1));
		}
		return;

		case 't':	//get current site latitude
		{
			String return_value = Telescope::degreesToDMS(telescope_->getSiteLatitude(), false);

			//returns: sDD*MM#
			sendRaw(return_value + "#");
		}
		return;

		case 'V':	//get telesope firmware info
		{
			String return_value = "";

			switch (input_[3])
			{
			case 'D':	//firmware date
				//returns: mmmm dd yyyy#
				return_value = __DATE__;
				break;
			case 'N':	//firmware number
				//returns: dd.d#
				return_value = "00.1";
				break;
			case 'P':	//telescope product name
				//returns: string + '#'
				return_value = "YAAATelescope";
				break;
			case 'T':	//telescope firmware time
				//returns: HH:MM:SS#
				return_value = __TIME__;
				break;
			}

			if (return_value != "")
				sendRaw(return_value + '#');
		}
		return;

		case 'y':	//get deepsky object search string
			//returns: GPDCO#
			//G - galaxies
			//P - planetary nebulas
			//D - diffuse nebulas
			//C - global clusters
			//O - open clusters
			return;
		case 'Z':	//get telescope azimuth
		{
			if (telescope_->getMountType() != Telescope::ALT_AZ)
				return;

			float alignment = telescope_->getCurrentAlignment(TelescopeAxis::X_AXIS);

			String angle = Telescope::degreesToDMS(alignment, lx200_protocol_precision_, true);

			sendRaw(angle + "#");
			//returns: sDD*MM or sDD*MM'SS (depends on precision setting) + '#'
		}
		return;
		}
	}
	return;

	case 'h':		//home position commands
	{
		switch (input_[2])
		{
		case 'C':
			//calibrate home position
			return;
		case 'F':
			//find home position
			telescope_->findHome();
			return;
		case 'I':	//sets date and time (:hIYYMMDDHHMMSS#)
		{
			uint16_t new_year = 0;
			uint8_t new_month = 0, new_day = 0;
			uint8_t new_hour = 0, new_minute = 0, new_second = 0;

			new_year = static_cast<uint16_t>(strtol(extractLX200String(input_, 3, 2).c_str(), NULL, DEC));
			new_month = static_cast<uint8_t>(strtol(extractLX200String(input_, 5, 2).c_str(), NULL, DEC));
			new_day = static_cast<uint8_t>(strtol(extractLX200String(input_, 7, 2).c_str(), NULL, DEC));
			new_hour = static_cast<uint8_t>(strtol(extractLX200String(input_, 9, 2).c_str(), NULL, DEC));
			new_minute = static_cast<uint8_t>(strtol(extractLX200String(input_, 11, 2).c_str(), NULL, DEC));
			new_second = static_cast<uint8_t>(strtol(extractLX200String(input_, 13, 2).c_str(), NULL, DEC));

			new_year += 2000;	//because only the last two digits of the year are given, the 21st century is assumed.

			YAAATime::setDateTime(new_year, new_month, new_day, new_hour, new_minute, new_second);

			sendRaw(1);

			return;
		}
		case 'N':	//power off telescope (sleep)
			return;
		case 'P':	//slew to park position				
			telescope_->park();
			return;
		case 'S':	//sets the current position as the park position.				
			telescope_->setPark();
			return;
		case 'W':	//wakeup sleeping telescope				
			return;
		case '?':	//query homing status
		{
			//returns: 0 - failed; 1 - successful, 2 - in progress
			String return_value = "";

			//if (!(telescope_->canFindHome() & 3)) {
			//	sendRaw(0);
			//	return;
			//}

			//todo: how to detect that homing has failed?
			return_value = (telescope_->isHoming() ? "2" : "1");

			sendRaw(return_value);
		}
		return;
		}
	}
	return;

	case 'H':	//toggle time format
		lx200_time_format_12hr_ = !lx200_time_format_12hr_;
		return;
	case 'I':	//initialize telescope		
		return;
	case 'L':		//object library commands
	{
		switch (input_[2])
		{
		case 'B':	//finds previous objectand sets it as the current target object
		{
			//do stuff
		}
		return;

		case 'C':	//set current target to deep sky object catalog number N
		{
			uint32_t number = strtoul(extractLX200String(input_, 3, 4).c_str(), NULL, DEC);

			//do stuff
		}
		return;

		case 'F':	//find object based on size, type, upper / lower limit and quality constraints and set it as the current target object
		{
			//do stuff
		}
		return;

		case 'f':	//identify object in current field
		{
			//do stuff
		}
		return;

		case 'I':	//get object information
		{
			//do stuff
			//returns string
		}
		return;

		case 'M':	//set current target object to messier obj. N
		{
			uint32_t number = strtoul(extractLX200String(input_, 3, 4).c_str(), NULL, DEC);

			//do stuff
		}
		return;

		case 'N':	//find next deep sky target
		{
			//do stuff
		}
		return;

		case 'o':	//select deep sky library
		{
			uint8_t library = charToInt(input_[3]);

			//do stuff
		}
		return;

		case 's':	//select star catalog
		{
			uint8_t catalog = charToInt(input_[3]);

			//do stuff
		}
		return;

		case 'S':	//select star
		{
			uint32_t star = strtoul(extractLX200String(input_, 3, 4).c_str(), NULL, DEC);

			//do stuff
		}
		return;
		}
	}
	return;

	case 'M':		//telescope movement commands
	{
		switch (input_[2])
		{
		case 'A':	//slew to target Alt / Az
		{
			uint8_t return_value = telescope_->slewToTarget();

			//returns:
			//0 - success
			//1 - error
			if (return_value == 0)
				sendRaw(0);
			else
				sendRaw(1);
		}
		return;

		case 'g':	//guide telescope in the given direction for the given time (ms)
		{
			uint32_t duration = strtoul(extractLX200String(input_, 4, 4).c_str(), NULL, DEC);
			uint8_t direction = 255;
			float rate = 0.0f;		//todo: which rate?

			switch (input_[3])
			{
			case 'n':	//move north
				direction = 0;
				break;
			case 's':	//move south
				direction = 1;
				break;
			case 'e':	//move east
				direction = 2;
				break;
			case 'w':	//move west
				direction = 3;
				break;
			}

			telescope_->pulseGuide(direction, duration, rate);
		}
		return;

		case 'n':	//move telescope north at current slew rate
		{
			//todo

		}
		return;

		case 's':	//move telescope south at current slew rate
		{
			//todo

		}
		return;

		case 'e':	//move telescope east at current slew rate
		{
			//todo

		}
		return;

		case 'w':	//move telescope west at current slew rate
		{
			//todo

		}
		return;

		case 'S':	//slew to target object
		{
			String return_value = "";
			uint8_t result = telescope_->slewToTarget();

			if (result == 0)
				return_value = "0";
			else
				return_value = "1" + String("some_message");

			//todo: determine if object is below horizon or above upper limit.

			//returns:
			//0 - slew possible
			//1<string># - object below horizon + message
			//2<string># - object below higher + message
			sendRaw(return_value);
		}
		return;
		}
	}
	return;

	case 'P':		//high precision toggle
	{
		lx200_precision_ = !lx200_precision_;

		if (lx200_precision_ == true)
			sendRaw("HIGH PRECISION");
		else
			sendRaw("LOW PRECISION");
	}
	return;

	case 'Q':	//movement commands
	{
		switch (input_[2])
		{
		case '#':	//halt all current slewing
		{
			telescope_->abortSlew();
			//returns nothing
		}
		return;

		case 'n':	//halt eastwards slews
		{
			//todo
		}
		return;

		case 's':	//halt eastwards slews
		{
			//todo
		}
		return;

		case 'e':	//halt eastwards slews
		{
			//todo
		}
		return;

		case 'w':	//halt eastwards slews
		{
			//todo
		}
		return;
		}
	}
	return;

	case 'r':		//field derotator commands
	{
		switch (input_[2])
		{
		case '+':
			//turn on field rotator
			return;
		case '-':
			//turn off field rotator
			return;
		}
	}
	return;

	case 'R':		//slew rate commands
	{
		switch (input_[2])
		{
		case 'C':	//set slew rate to centering rate(2nd slowest)
		{
			//todo
		}
		return;

		case 'G':	//set slew rate to guiding rate (slowest)	
		{
			//todo
		}
		return;

		case 'M':	//set slew rate to find rate (2nd fastest)	
		{
			//todo
		}
		return;

		case 'S':	//set slew rate to max (fastest)	
		{
			//todo
		}
		return;

		case 'A':	//set RA/Az slew rate to DD.D degrees / second
		{
			float rate = atof(extractLX200String(input_, 3, 4).c_str());
			//todo


		}
		return;

		case 'E': 	//set Dec/Alt slew rate to DD.D degrees / second
		{
			float rate = atof(extractLX200String(input_, 3, 4).c_str());
			//todo


		}
		return;

		case 'g': 	//set guide rate to DD.D arcseconds / second (offset to current tracking rate)
		{
			float rate = atof(extractLX200String(input_, 3, 5).c_str());
			//todo

		}
		return;
		}
	}
	return;

	case 'S':		//telescope set commands
	{
		switch (input_[2])
		{
		case 'a':	//set target altitude
		{
			//return instantly if the telescope is not Alt-Az
			if (telescope_->getMountType() != Telescope::ALT_AZ)
			{
				sendRaw(0);
				return;
			}

			float angle = 0.0f;
			bool within_limits = false;

			//extract 
			float degrees = atof(extractLX200String(input_, 3, 3).c_str());
			float minutes = atof(extractLX200String(input_, 7, 2).c_str());
			float seconds = 0.0f;

			if (input_[9] == '\'')
				seconds = atof(extractLX200String(input_, 10, 2).c_str());

			angle = degrees + minutes / 60.0f + seconds / 3600.0f;

			//returns:
			//1 - object within slew range
			//0 - object out of slew range
			sendRaw(static_cast<uint8_t>(telescope_->setTarget(TelescopeAxis::Y_AXIS, angle)));
		}
		return;

		case 'z':	//set target azimuth
		{
			//return instantly if the telescope is not Alt-Az
			if (telescope_->getMountType() != Telescope::ALT_AZ)
			{
				sendRaw(0);
				return;
			}

			float angle = 0.0f;
			bool within_limits = false;

			//extract 
			float degrees = atof(extractLX200String(input_, 3, 3).c_str());
			float minutes = atof(extractLX200String(input_, 7, 2).c_str());

			angle = degrees + minutes / 60.0f;

			//returns:
			//1 - object within slew range
			//0 - object out of slew range
			sendRaw(static_cast<uint8_t>(telescope_->setTarget(TelescopeAxis::X_AXIS, angle)));
		}
		return;

		case 'b':	//set brighter limit
		{
			float limit = atof(extractLX200String(input_, 3, 5).c_str());

			//todo...

			//returns:
			//0 - invalid
			//1 - valid
		}
		return;

		case 'B':	//set baud rate n
		{
			uint8_t digit = charToInt(input_[3]);

			if (digit == 0)
				return;

			//returns 1 at current baud rate, then switches to the new baud rate
			sendRaw(1);

			uint32_t baud_rate = 0;
			switch (digit)
			{
			case 1:
				baud_rate = 57600L;
				break;
			case 2:
				baud_rate = 38400L;
				break;
			case 3:
				baud_rate = 28800L;
				break;
			case 4:
				baud_rate = 19200L;
				break;
			case 5:
				baud_rate = 14400L;
				break;
			case 6:
				baud_rate = 9600L;
				break;
			case 7:
				baud_rate = 4800L;
				break;
			case 8:
				baud_rate = 2400L;
				break;
			case 9:
				baud_rate = 1200L;
				break;
			}

			//TODO set baud rate to given rate 
			//serial_handler_->changeBaudRate(baud_rate);
		}
		return;

		case 'C':	//change handbox date to MM/DD/YY
		{
			uint8_t month, day;
			uint16_t year;

			month = static_cast<uint8_t>(atoi(extractLX200String(input_, 3, 2).c_str()));
			day = static_cast<uint8_t>(atoi(extractLX200String(input_, 6, 2).c_str()));
			year = static_cast<uint16_t>(atoi(extractLX200String(input_, 9, 2).c_str()));

			year += 2000;	//because only the last two digits of the year are given, the 21st century is assumed.

			//return an error message if the given date is invalid, otherwise set the new 
			//date.
			if (month > 12 || day > 31)
				sendRaw("0\0");
			else
			{
				YAAATime::setNewDate(year, month, day);

				sendRaw(String("1") + String("Updating Planetary Data#"));
			}
		}
		return;

		case 'd':	//set target object declination
		{
			//return instantly if the telescope is Alt-Az
			if (telescope_->getMountType() == Telescope::ALT_AZ)
			{
				sendRaw(0);
				return;
			}

			float angle = 0.0f;
			bool within_limits = false;

			//extract 
			float degrees = atof(extractLX200String(input_, 3, 3).c_str());
			float minutes = atof(extractLX200String(input_, 7, 2).c_str());
			float seconds = 0.0f;

			if (lx200_protocol_precision_ == true)
				seconds = atof(extractLX200String(input_, 10, 2).c_str());

			angle = degrees + minutes / 60.0f + seconds / 3600.0f;

			//returns:
			//1 - object within slew range
			//0 - object out of slew range
			sendRaw(static_cast<uint8_t>(telescope_->setTarget(TelescopeAxis::Y_AXIS, angle)));
		}
		return;

		case 'r':	//set target right ascension
		{
			//return instantly if the telescope is Alt-Az
			if (telescope_->getMountType() == Telescope::ALT_AZ)
			{
				sendRaw(0);
				return;
			}

			float angle = 0.0f;
			bool within_limits = false;

			//extract 
			float degrees = atof(extractLX200String(input_, 3, 3).c_str());
			float minutes = atof(extractLX200String(input_, 7, 2).c_str());
			float seconds = 0.0f;

			//message format:
			//:SrHH:MM.T#
			//:SrHH:MM:SS#

			//read seconds or fraction of minute (depending on precision setting)
			//if (input_[9] == ':')
			if (lx200_protocol_precision_ == true)
				seconds = atof(extractLX200String(input_, 10, 2).c_str());
			else
				seconds = atof(extractLX200String(input_, 10, 1).c_str()) * 6.0f;	//convert from fraction to seconds

			angle = degrees + minutes / 60.0f + seconds / 3600.0f;

			//returns:
			//1 - object within slew range
			//0 - object out of slew range
			sendRaw(static_cast<uint8_t>(telescope_->setTarget(TelescopeAxis::X_AXIS, angle)));
		}
		return;

		case 'E':	//set selenographic latitude
		{
			float angle = 0.0f;

			//extract 
			float degrees = atof(extractLX200String(input_, 3, 3).c_str());
			float minutes = atof(extractLX200String(input_, 7, 2).c_str());

			angle = degrees + minutes / 60.0f;

			//returns:
			//1 - moon is up and coordinates are valid
			//0 - invalid
		}
		return;

		case 'e':	//set selenographic longitude
		{
			float angle = 0.0f;

			//extract 
			float degrees = atof(extractLX200String(input_, 3, 4).c_str());
			float minutes = atof(extractLX200String(input_, 8, 2).c_str());

			angle = degrees + minutes / 60.0f;

			//returns:
			//1 - moon is up and coordinates are valid
			//0 - invalid
		}
		return;

		case 'f':	//set faint magnitude limit
		{
			//todo...
		}
		return;

		case 'g':	//set current site longitude
		{
			//todo...
		}
		return;

		case 'G':	//set number of hours added to local time to yield UTC / GMT
		{
			float hour = atof(extractLX200String(input_, 3, 5).c_str());

			String return_value = YAAATime::setUTCOffset(hour) ? "1" : "0";

			sendRaw(return_value + "#");
		}
		return;

		case 'D':	//set daylight savings mode
		{

		}
		return;

		case 'h':	//set minimum object elevation limit
		{

		}
		return;

		case 'o':	//set highest elevation to which the telescope will slew
		{

		}
		return;

		case 'L':	//set local time
		{
			uint8_t hour = 0, minute = 0, second = 0;

			hour = static_cast<uint8_t>(atoi(extractLX200String(input_, 3, 2).c_str()));
			minute = static_cast<uint8_t>(atoi(extractLX200String(input_, 6, 2).c_str()));
			second = static_cast<uint8_t>(atoi(extractLX200String(input_, 9, 2).c_str()));

			//return an error message if the given time is invalid, otherwise set the
			//time.
			if (hour > 23 || minute > 59 || second > 59)
				sendRaw(0);
			else
			{
				YAAATime::setNewTime(hour, minute, second);

				sendRaw(1);
			}
		}
		return;

		case 'M':	//set site 1's name to <string> (up to 15 chars)
		{

		}
		return;

		case 'N':	//set site 2's name to <string> (up to 15 chars)
		{

		}
		return;

		case 'O':	//set site 3's name to <string> (up to 15 chars)
		{

		}
		return;

		case 'P':	//set site 4's name to <string> (up to 15 chars)
		{

		}
		return;

		case 'q':	//set quality limit used in FIND/BROWSE
		{

		}
		return;

		case 'l':	//set size of smallest object returned by FIND/BROWSE in arcminutes
		{

		}
		return;

		case 's':	//set size of largest object for FIND/BROWSE
		{

		}
		return;

		case 'S':	//set local sidereal time to HH:MM:SS
		{
			//returns: 0 - invalid; 1 - valid
		}
		return;

		case 't':	//set current site latitude to sDD*MM
		{

		}
		return;

		case 'T':	//set current tracking rate to TTT.T Hz
		{
			//extract desired rate from command
			float rate = atof(extractLX200String(input_, 2, 5).c_str());

			//set telescope to sidereal tracking rate (60.1644Hz)
			uint8_t return_value = telescope_->setTrackingRate(Telescope::TR_SIDEREAL);

			if (return_value != 0)
			{
				sendRaw(0);
				return;
			}

			//calculate tracking rate offset
			rate -= 60.1644f;
			rate *= 0.250684479;	//convert from Hz to arcseconds / sidereal second

			//apply tracking rate offset
			telescope_->setRightAscensionRate(rate);

			sendRaw(1);
		}
		return;

		case 'w':	//set maximum slew rate. N = 2 ... 8
		{
			float slew_rate = static_cast<float>(charToInt(input_[2]));

			//returns: 0 - invalid; 1 - valid
			if ((slew_rate < 2.0f) || (slew_rate > 8.0f))
			{
				sendRaw(0);
				return;
			}

			//todo...
		}
		return;

		case 'y':	//set object selection string used by the FIND/BROWSE command.
		{

		}
		return;
		}
	}
	return;

	case 'T':		//tracking comands
	{
		//60 Hz <==> 15.0 arcseconds / second
		//1 sidereal second = 0.9972695677 seconds.
		//1 second = 1.002737916 sidereal seconds

		//0.1Hz = 0.025 arcseconds / second = 0.0250684479 arcseconds / sidereal second
		switch (input_[2])
		{
		case '+':	//increase manual rate by 0.1Hz
		{
			float current = telescope_->getRightAscensionRate();

			telescope_->setRightAscensionRate(current + 0.0250684479f);

			//returns nothing
		}
		return;

		case '-':	//decrease manual rate by 0.1Hz
		{
			float current = telescope_->getRightAscensionRate();

			telescope_->setRightAscensionRate(current - 0.0250684479f);

			//returns nothing
		}
		return;

		case 'L':	//set lunar tracking rate
			telescope_->setTrackingRate(Telescope::TR_LUNAR);
			return;
		case 'M':	//set custom tracking rate
			return;
		case 'Q':	//set default tracking rate
			telescope_->setTrackingRate(Telescope::TR_SIDEREAL);
			return;
		default:	//set manual rate to given decimal rate.
		{
			float rate = atof(extractLX200String(input_, 2, 8).c_str());

			//todo: set manual rate to rate.

			//returns: 1
			sendRaw(1);
		}
		return;
		}
	}
	return;

	case 'U':		//User Format control  / protocol precision toggle
	{
		lx200_protocol_precision_ = !lx200_protocol_precision_;
	}
	return;

	case 'V':		//PEC readout
	{
		uint16_t segment = static_cast<uint16_t>(atoi(extractLX200String(input_, 3, 4).c_str()));
		float pec_entry = 0.0f;

		switch (input_[2])
		{
		case 'D':	//return Decliantion PEC table entry
			break;
		case 'R':	//return Right Ascension PEC table entry
			break;
		}

		//returns: D.DDDD rate adjustment factor for the given segment.
		sendRaw(String(abs(pec_entry), 4));
	}
	return;

	case 'W':		//site select
	{
		uint8_t digit = charToInt(input_[3]);

		if ((digit == 0) || (digit > 4))
			return;

		//set current site to digit.

		//returns nothing
	}
	return;

	case '$':
	{
		switch (input_[2])
		{
		case 'B':		//active backlash compensation
		{
			//extract numbers from input
			String number = extractLX200String(input_, 4, 2);

			//parse number
			uint8_t anti_backlash = static_cast<uint8_t>(strtoul(number.c_str(), NULL, DEC));

			switch (input_[3])
			{
			case 'A':
			{
				//do stuff
				break;
			}
			case 'Z':
			{
				//do stuff
				break;
			}
			}
		}
		return;

		case 'Q':		//smart drive control
		{
			//proprietary functions, ignore?
		}
		return;
		}
	}
	return;

	case '?':		//help text retrieval
	{
		sendRaw("help");
		sendRaw("replaceMe");
	}
	return;
	}
}

String RemoteCommandHandler::extractLX200String(char* input, uint8_t start_pos, uint8_t num_chars)
{
	//char* output = new char(num_chars + 1);
	String output = "";

	uint8_t i = 0;

	while ((i < num_chars) && (input[start_pos + i] != '#'))
	{
		//output[i] = input[start_pos + i];
		output += input[start_pos + i];
		i++;
	}

	//output[i] = '\0';

	return output;
}
#endif /* FEATURE_LX200 */

bool RemoteCommandHandler::checkAvailable(uint8_t device)
{
	switch (device)
	{
	case GENERAL:
		return true;
		//case CAMERA_ROTATOR:	//dome not implemented yet
		//	if (camera_rotator_) return true;
		//	break;
		//case DOME:			//dome not implemented yet
		//	if (dome_) return true;
		//	break;
	case FOCUSER:
		if (focuser_) return true;
		break;
	case FILTER_WHEEL:
		if (filter_wheel_) return true;
		break;
		//case SAFETY_MONITOR:
		//	return false;		//safety monitor not implemented yet.
	case TELESCOPE:
		if (telescope_)	return true;
		break;

	}

	return false;
}

bool RemoteCommandHandler::checkLocked(uint8_t device)
{
	switch (device)
	{
		//case GENERAL:
		//	return false;		//general cannot be locked.
		//case CAMERA_ROTATOR:
		//	return false;		//camera rotator not implemented yet.
		//case DOME:
		//	return false;		//dome not implemented yet.
	case FOCUSER:
		if (focuser_->getLocked() == FilterWheel::LOCK_LOCAL)
		{
			sendError(RemoteCommandHandler::ERROR_DEVICE_LOCKED);
			return true;
		}
		break;
	case FILTER_WHEEL:
		if (filter_wheel_->getLocked() == FilterWheel::LOCK_LOCAL)
		{
			sendError(RemoteCommandHandler::ERROR_DEVICE_LOCKED);
			return true;
		}
		break;
		//case SAFETY_MONITOR:
		//	return false;		//safety monitor not implemented yet.
	case TELESCOPE:
		if (telescope_->getLocked() == Telescope::LOCK_LOCAL)
		{
			sendError(RemoteCommandHandler::ERROR_DEVICE_LOCKED);
			return true;
		}
		break;
	}

	return false;
}

uint8_t RemoteCommandHandler::getCommandType()
{
	//tokenize input string and determine command type.
	value_ = tokenize(NULL);

	if (strcmp_P(value_, PSTR("GET")) == 0) return command_type_::GET;
	if (strcmp_P(value_, PSTR("SET")) == 0) return command_type_::SET;

	return UNKNOWN;
}

uint16_t RemoteCommandHandler::getTelescopeAxis(char* axis)
{
	//if no axis string was given, tokenize the input string.
	if (axis == NULL)
		axis = tokenize(NULL);

	if (strcmp_P(axis, PSTR("X_AXIS")) == 0) return TelescopeAxis::X_AXIS;
	if (strcmp_P(axis, PSTR("Y_AXIS")) == 0) return TelescopeAxis::Y_AXIS;
	if (strcmp_P(axis, PSTR("Z_AXIS")) == 0) return TelescopeAxis::Z_AXIS;

	return UNKNOWN;
}

uint8_t RemoteCommandHandler::parseCommand(uint8_t device)
{
	//tokenize input string and determine command.
	//command_ = tokenize(NULL);

	if (device == RemoteCommandHandler::GENERAL)
	{
		if (command_ == NULL) return general_command_::NONE;

		if (strcmp_P(command_, PSTR("ONEWIRE_TEMP")) == 0) return general_command_::ONEWIRE_TEMP;

		if (strcmp_P(command_, PSTR("TIME")) == 0) return general_command_::TIME;

		if (strcmp_P(command_, PSTR("DATETIME")) == 0) return general_command_::DATETIME;

		if (strcmp_P(command_, PSTR("UTC_OFFSET")) == 0) return general_command_::UTC_OFFSET;

		if (strcmp_P(command_, PSTR("LOG_MODE")) == 0) return general_command_::LOG_MODE;

		if (strcmp_P(command_, PSTR("LOG_STATE")) == 0) return general_command_::LOG_STATE;

		if (strcmp_P(command_, PSTR("LOG_DUMP")) == 0) return general_command_::LOG_DUMP;

		if (strcmp_P(command_, PSTR("LOG_CLEAR")) == 0) return general_command_::LOG_CLEAR;

		if (strcmp_P(command_, PSTR("ETHERNET_IP")) == 0) return general_command_::ETHERNET_IP;

		if (strcmp_P(command_, PSTR("ETHERNET_GATEWAY")) == 0) return general_command_::ETHERNET_GATEWAY;

		if (strcmp_P(command_, PSTR("ETHERNET_DNS")) == 0) return general_command_::ETHERNET_DNS;

		if (strcmp_P(command_, PSTR("ETHERNET_SUBNET")) == 0) return general_command_::ETHERNET_SUBNET;

		if (strcmp_P(command_, PSTR("ETHERNET_PORT")) == 0) return general_command_::ETHERNET_PORT;

#if defined(ESP8266) || defined(ESP32)
		if (strcmp_P(command_, PSTR("WIFI_IP")) == 0) return general_command_::WIFI_IP;

		if (strcmp_P(command_, PSTR("WIFI_GATEWAY")) == 0) return general_command_::WIFI_GATEWAY;

		if (strcmp_P(command_, PSTR("WIFI_DNS")) == 0) return general_command_::WIFI_DNS;

		if (strcmp_P(command_, PSTR("WIFI_SUBNET")) == 0) return general_command_::WIFI_SUBNET;

		if (strcmp_P(command_, PSTR("WIFI_PORT")) == 0) return general_command_::WIFI_PORT;

		if (strcmp_P(command_, PSTR("WIFI_RSSI")) == 0) return general_command_::WIFI_RSSI;

		if (strcmp_P(command_, PSTR("OTA_ENABLED")) == 0) return general_command_::OTA_ENABLED;

		if (strcmp_P(command_, PSTR("OTA_MODE")) == 0) return general_command_::OTA_MODE;
#endif /* defined(ESP8266) || defined(ESP32) */

		if (strcmp_P(command_, PSTR("MQTT_ENABLED")) == 0) return general_command_::MQTT_ENABLED;

		if (strcmp_P(command_, PSTR("MQTT_STATE")) == 0) return general_command_::MQTT_STATE;

		if (strcmp_P(command_, PSTR("WRITE_DEFAULTS")) == 0) return general_command_::WRITE_DEFAULTS;

		if (strcmp_P(command_, PSTR("SRAM")) == 0) return general_command_::SRAM;

		if (strcmp_P(command_, PSTR("SETUP")) == 0) return general_command_::SETUP;

		if (strcmp_P(command_, PSTR("CONNECTED")) == 0) return general_command_::CONNECTED;
	}

	if (device == RemoteCommandHandler::TELESCOPE)
	{
		if (command_ == NULL) return telescope_command_::NONE;

		if (strcmp_P(command_, PSTR("ALIGNMENT")) == 0) return telescope_command_::ALIGNMENT;

		if (strcmp_P(command_, PSTR("SLEW")) == 0) return telescope_command_::SLEW;

		if (strcmp_P(command_, PSTR("ABORT_SLEW")) == 0) return telescope_command_::ABORT_SLEW;

		if (strcmp_P(command_, PSTR("MOVE")) == 0) return telescope_command_::MOVE;

		if (strcmp_P(command_, PSTR("MOVEAXIS")) == 0) return telescope_command_::MOVEAXIS;

		if (strcmp_P(command_, PSTR("TARGET")) == 0) return telescope_command_::TARGET;

		if (strcmp_P(command_, PSTR("FIND_HOME")) == 0) return telescope_command_::FIND_HOME;

		if (strcmp_P(command_, PSTR("AT_HOME")) == 0) return telescope_command_::AT_HOME;

		if (strcmp_P(command_, PSTR("HOMING_ERROR")) == 0) return telescope_command_::HOMING_ERROR;

		if (strcmp_P(command_, PSTR("PARKED")) == 0) return telescope_command_::PARKED;

		if (strcmp_P(command_, PSTR("PARK_POS")) == 0) return telescope_command_::PARK_POS;

		if (strcmp_P(command_, PSTR("PULSE_GUIDE")) == 0) return telescope_command_::PULSE_GUIDE;

		if (strcmp_P(command_, PSTR("SWITCH_STATE")) == 0) return telescope_command_::SWITCH_STATE;

		if (strcmp_P(command_, PSTR("TRACKING")) == 0) return telescope_command_::TRACKING;

		if (strcmp_P(command_, PSTR("TRACKING_RATE")) == 0) return telescope_command_::TRACKING_RATE;

		if (strcmp_P(command_, PSTR("RA_RATE")) == 0) return telescope_command_::RA_RATE;

		if (strcmp_P(command_, PSTR("DEC_RATE")) == 0) return telescope_command_::DEC_RATE;

		if (strcmp_P(command_, PSTR("CONNECTED")) == 0) return telescope_command_::CONNECTED;

		if (strcmp_P(command_, PSTR("TEMP")) == 0) return telescope_command_::TEMP;

		if (strcmp_P(command_, PSTR("TEMP_MEASURE")) == 0) return telescope_command_::TEMP_MEASURE;

		if (strcmp_P(command_, PSTR("WRITE_DEFAULTS")) == 0) return telescope_command_::WRITE_DEFAULTS;

		if (strcmp_P(command_, PSTR("SETUP")) == 0) return telescope_command_::SETUP;
	}

	if (device == RemoteCommandHandler::FOCUSER)
	{
		if (command_ == NULL) return focuser_command_::NONE;

		if (strcmp_P(command_, PSTR("ABSOLUTE")) == 0) return focuser_command_::ABSOLUTE;

		if (strcmp_P(command_, PSTR("POSITION")) == 0) return focuser_command_::POSITION;

		if (strcmp_P(command_, PSTR("MOVE")) == 0) return focuser_command_::MOVE;

		if (strcmp_P(command_, PSTR("MOVE_DIRECTION")) == 0) return focuser_command_::MOVE_DIRECTION;

		if (strcmp_P(command_, PSTR("MOVE_TIME")) == 0) return focuser_command_::MOVE_TIME;

		if (strcmp_P(command_, PSTR("HALT")) == 0) return focuser_command_::HALT;

		if (strcmp_P(command_, PSTR("TEMP")) == 0) return focuser_command_::TEMP;

		if (strcmp_P(command_, PSTR("TEMP_MEASURE")) == 0) return focuser_command_::TEMP_MEASURE;

		if (strcmp_P(command_, PSTR("TEMP_COMP")) == 0) return focuser_command_::TEMP_COMP;

		if (strcmp_P(command_, PSTR("TEMP_COMP_SENSOR")) == 0) return focuser_command_::TEMP_COMP_SENSOR;

		if (strcmp_P(command_, PSTR("FIND_HOME")) == 0) return focuser_command_::FIND_HOME;

		if (strcmp_P(command_, PSTR("SWITCH_STATE")) == 0) return focuser_command_::SWITCH_STATE;

		if (strcmp_P(command_, PSTR("CONNECTED")) == 0) return focuser_command_::CONNECTED;

		if (strcmp_P(command_, PSTR("WRITE_DEFAULTS")) == 0) return focuser_command_::WRITE_DEFAULTS;

		if (strcmp_P(command_, PSTR("SETUP")) == 0) return focuser_command_::SETUP;
	}

	if (device == RemoteCommandHandler::FILTER_WHEEL)
	{
		if (command_ == NULL) return filter_wheel_command_::NONE;

		if (strcmp_P(command_, PSTR("POSITION")) == 0) return filter_wheel_command_::POSITION;

		if (strcmp_P(command_, PSTR("FOCUS_OFFSET")) == 0) return filter_wheel_command_::FOCUS_OFFSET;

		if (strcmp_P(command_, PSTR("CURRENT_FILTER")) == 0) return filter_wheel_command_::CURRENT_FILTER;

		if (strcmp_P(command_, PSTR("SWITCH_STATE")) == 0) return filter_wheel_command_::SWITCH_STATE;

		if (strcmp_P(command_, PSTR("FIND_HOME")) == 0) return filter_wheel_command_::FIND_HOME;

		if (strcmp_P(command_, PSTR("AT_HOME")) == 0) return filter_wheel_command_::AT_HOME;

		if (strcmp_P(command_, PSTR("MEASURE_HOME_OFFSET")) == 0) return filter_wheel_command_::MEASURE_HOME_OFFSET;

		if (strcmp_P(command_, PSTR("HOMING_ERROR")) == 0) return filter_wheel_command_::HOMING_ERROR;

		if (strcmp_P(command_, PSTR("CONNECTED")) == 0) return filter_wheel_command_::CONNECTED;

		if (strcmp_P(command_, PSTR("WRITE_DEFAULTS")) == 0) return filter_wheel_command_::WRITE_DEFAULTS;

		if (strcmp_P(command_, PSTR("SETUP")) == 0) return filter_wheel_command_::SETUP;
	}

	if (device == RemoteCommandHandler::CAMERA_ROTATOR)
		return UNKNOWN;

	if (device == RemoteCommandHandler::DOME)
		return UNKNOWN;

	if (device == RemoteCommandHandler::FOCUSER)
		return UNKNOWN;

	if (device == RemoteCommandHandler::SAFETY_MONITOR)
		return UNKNOWN;

	return UNKNOWN;
}

char RemoteCommandHandler::getArrayDelimiter(uint16_t setup_device, uint16_t setup_option)
{
	switch (setup_device)
	{
	case EEPROMHandler::device_type_::GENERAL:
		switch (setup_option)
		{
		case EEPROMHandler::general_variable_::ETHERNET_IP:
		case EEPROMHandler::general_variable_::ETHERNET_DNS:
		case EEPROMHandler::general_variable_::ETHERNET_GATEWAY:
		case EEPROMHandler::general_variable_::ETHERNET_SUBNET:
		case EEPROMHandler::general_variable_::WIFI_IP:
		case EEPROMHandler::general_variable_::WIFI_DNS:
		case EEPROMHandler::general_variable_::WIFI_GATEWAY:
		case EEPROMHandler::general_variable_::WIFI_SUBNET:
			return '.';
		case EEPROMHandler::general_variable_::ETHERNET_MAC:
		case EEPROMHandler::general_variable_::WIFI_MAC:
			return ':';
		case EEPROMHandler::general_variable_::WIFI_SSID:
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
			return '\0';
		}
		break;
	}

	return '.';	//return '.' if the setup option was unknown.
}

uint8_t RemoteCommandHandler::getArraySize(uint16_t setup_device, uint16_t setup_option)
{
	switch (setup_device)
	{
	case EEPROMHandler::device_type_::GENERAL:
		switch (setup_option)
		{
		case EEPROMHandler::general_variable_::ETHERNET_IP:
		case EEPROMHandler::general_variable_::ETHERNET_DNS:
		case EEPROMHandler::general_variable_::ETHERNET_GATEWAY:
		case EEPROMHandler::general_variable_::ETHERNET_SUBNET:
		case EEPROMHandler::general_variable_::WIFI_IP:
		case EEPROMHandler::general_variable_::WIFI_DNS:
		case EEPROMHandler::general_variable_::WIFI_GATEWAY:
		case EEPROMHandler::general_variable_::WIFI_SUBNET:
			return 4;
		case EEPROMHandler::general_variable_::ETHERNET_MAC:
		case EEPROMHandler::general_variable_::WIFI_MAC:
			return 6;
		case EEPROMHandler::general_variable_::WIFI_SSID:
			return 33;
		case EEPROMHandler::general_variable_::WIFI_PASSWORD:
		case EEPROMHandler::general_variable_::WIFI_HOSTNAME:
			return 64;
		}
		break;
	}

	return 0;	//return 0 if the setup option was unknown.
}

uint8_t RemoteCommandHandler::getDeviceType(char* chr)
{
	if (strcmp(chr, "G") == 0) return RemoteCommandHandler::GENERAL;

	if (strcmp(chr, "C") == 0) return RemoteCommandHandler::CAMERA_ROTATOR;

	if (strcmp(chr, "D") == 0) return RemoteCommandHandler::DOME;

	if (strcmp(chr, "F") == 0) return RemoteCommandHandler::FOCUSER;

	if (strcmp(chr, "S") == 0) return RemoteCommandHandler::SAFETY_MONITOR;

	if (strcmp(chr, "T") == 0) return RemoteCommandHandler::TELESCOPE;

	if (strcmp(chr, "W") == 0) return RemoteCommandHandler::FILTER_WHEEL;

	if (strcmp(chr, "O") == 0) return RemoteCommandHandler::OBSERVING_CONDITIONS;

	return UNKNOWN;
}

char* RemoteCommandHandler::tokenize(char* chr)
{
	return strtok(chr, COMMAND_DELIMITERS);
}

void RemoteCommandHandler::sendError(uint16_t error_code)
{
	//responses from the arduino are expected in the following format / pattern:
	//"<D,X,0>"
	// || | ||
	// || | |end of message
	// || | return code.
	// || command identifier.
	// |device identifier.
	// beginning of message.

	String error_message = "ERROR_";
	error_message += String(error_code);

	sendResponse(error_message);
}

void RemoteCommandHandler::sendResponse(float return_value)
{
	sendResponse(device_, command_, String(return_value, 7));
}

template <class T> void RemoteCommandHandler::sendResponse(const T& return_value)
{
	sendResponse(device_, command_, String(return_value));
}

void RemoteCommandHandler::sendResponse(char* device, char* command, String return_value)
{
	//responses from the arduino are expected in the following format / pattern:
	//"<D,X,0>"
	// || | ||
	// || | |end of message
	// || | return code.
	// || command identifier.
	// |device identifier.
	// beginning of message.

	String response = "<";
	response += device;
	response += ",";
	response += command;
	response += ",";
	response += return_value;
	response += ">";

	command_interfaces_[command_interface_current_]->println(response);

	log_.log(">> " + response);
}

template <class T> void RemoteCommandHandler::sendRaw(const T& message)
{
	command_interfaces_[command_interface_current_]->print(message);
}

void RemoteCommandHandler::readOption(
	uint16_t setup_device,
	uint16_t setup_option,
	uint16_t index,
	uint8_t variable_type)
{
	//TODO replace with switch statement
	if (variable_type == EEPROMHandler::UINT8_T_)
	{
		uint8_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::UINT16_T_)
	{
		uint16_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::UINT32_T_)
	{
		uint32_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::INT16_T_)
	{
		int16_t return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::INT32_T_)
	{
		long return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::FLOAT_)
	{
		float return_value;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			return_value);
		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::ARRAY_)
	{
		String return_value = "";

		//determine size of array to read
		uint8_t num_bytes = getArraySize(setup_device, setup_option);

		//use correct delimiter
		char delimiter = getArrayDelimiter(setup_device, setup_option);

		//allocate array and read variable from EEPROM
		byte *content = new byte[num_bytes];
		EEPROMHandler::readArray(
			setup_device,
			setup_option,
			index,
			content,
			num_bytes);

		//assemble string to send to pc
		for (uint8_t i = 0; i < num_bytes; i++)
		{
			switch (setup_option)
			{
			case EEPROMHandler::general_variable_::ETHERNET_MAC:
			case EEPROMHandler::general_variable_::WIFI_MAC:
				return_value += String(content[i], HEX);
				break;
				//case EEPROMHandler::general_variable_::WIFI_SSID:
				//case EEPROMHandler::general_variable_::WIFI_PASSWORD:
				//	return_value += content[i];
				//	break;
			default:
				return_value += String(content[i], DEC);
				break;
			}

			//if (setup_option == EEPROMHandler::general_variable_::ETHERNET_MAC)
			//{
			//	//return_value += "0x";
			//	return_value += String(content[i], HEX);
			//}
			//else
			//	return_value += String(content[i], DEC);

			if ((i < num_bytes - 1) && (delimiter != '\0'))
				return_value += delimiter;
		}

		delete content;

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::FILTER_INFO_)
	{
		String return_value = "";

		FilterWheel::FilterInfo filter_info;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			filter_info);

		return_value += filter_info.type;
		return_value += " ";
		return_value += filter_info.RFU;
		return_value += " ";
		return_value += filter_info.focus_offset;
		return_value += " ";
		return_value += filter_info.position_offset;

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::SENSOR_SETTINGS_)
	{
		String return_value = "";

		YAAASensor::SensorSettings sensor_settings =
		{
			DEF_SENSOR_TYPE,
			DEF_SENSOR_DEVICE,
			DEF_SENSOR_UNIT,
			DEF_SENSOR_PARAMETERS
		};

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			sensor_settings);

		return_value += sensor_settings.sensor_type_;
		return_value += " ";
		return_value += sensor_settings.sensor_device_;
		return_value += " ";
		return_value += sensor_settings.unit_;
		return_value += " ";

		//assemble string to send to pc
		for (uint8_t i = 0; i < sizeof(sensor_settings.parameters_); i++)	//SensorSettings::parameters MUST be static
		{
			return_value += String(sensor_settings.parameters_[i], HEX);

			if (i < 9)
				return_value += " ";
		}

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::BUTTON_INFO_)
	{
		String return_value = "";

		//read button settings from EEPROM
		Button::ButtonSettings settings =
		{
			255,
			false
		};

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			settings);

		//assemble response string
		return_value += settings.pin_nr_;
		return_value += " ";
		return_value += settings.inverted_;

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::KEYPAD_INFO_)
	{
		String return_value = "";

		//read keypad settings from EEPROM.
		YAAAKeypad::KeypadSettings settings;

		EEPROMHandler::readValue(
			setup_device,
			setup_option,
			index,
			settings);

		//assemble response string
		return_value += settings.type_;
		return_value += " ";
		return_value += settings.active_high_;
		return_value += " ";

		for (uint8_t i = 0; i < sizeof(settings.parameters_); i++)
		{
			return_value += String(settings.parameters_[i], DEC);

			return_value += " ";
		}

		return_value += settings.read_interval_;

		sendResponse(return_value);
	}

	if (variable_type == EEPROMHandler::STRING_)
	{
		String return_value = "";

		EEPROMHandler::readString(
			setup_device,
			setup_option,
			index,
			return_value);

		sendResponse(return_value.c_str());
	}

	if (variable_type == UNKNOWN) {
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
	}
}

void RemoteCommandHandler::writeOption(
	uint16_t setup_device,
	uint16_t setup_option,
	uint16_t index,
	uint8_t variable_type)
{
	//char* string_value = tokenize(NULL);
	value_ = tokenize(NULL);

	//send an error message to the pc if the value to write could not be parsed.
	//empty strings are allowed. 
	if ((value_ == NULL) && (variable_type != EEPROMHandler::STRING_))
	{
		sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
		return;
	}

	if (variable_type == EEPROMHandler::UINT8_T_)
	{
		uint8_t setup_value = static_cast<uint8_t>(atoi(value_));

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::UINT16_T_)
	{
		uint16_t setup_value = static_cast<uint16_t>(atoi(value_));

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::INT16_T_)
	{
		int16_t setup_value = atoi(value_);

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::INT32_T_)
	{
		long setup_value = strtol(value_, NULL, DEC);

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::UINT32_T_)
	{
		uint32_t setup_value = strtoul(value_, NULL, DEC);

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::FLOAT_)
	{
		float setup_value = static_cast<float>(atof(value_));

		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	if (variable_type == EEPROMHandler::ARRAY_)
	{
		//determine size of array to read
		uint8_t num_bytes = getArraySize(setup_device, setup_option);

		//allocate array 
		byte *content = new byte[num_bytes];

		//parse command to find bytes
		for (uint8_t i = 0; i < num_bytes; i++)
		{
			//check current substring
			if (value_ == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
				delete content;
				return;
			}

			//convert substring to byte.
			if (setup_option == EEPROMHandler::general_variable_::ETHERNET_MAC)
				content[i] = static_cast<byte>(strtoul(value_, NULL, HEX));
			else
				content[i] = static_cast<byte>(strtoul(value_, NULL, DEC));

			//read next substring
			value_ = tokenize(NULL);
		}

		//write array to EEPROM
		EEPROMHandler::writeArray(
			setup_device,
			setup_option,
			index,
			content,
			num_bytes);

		delete content;
	}

	if (variable_type == EEPROMHandler::FILTER_INFO_)
	{
		FilterWheel::FilterInfo filter_info;

		//read filter type	
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}
		filter_info.type = static_cast<uint8_t>(atoi(value_));

		//read focus offset
		value_ = tokenize(NULL);	//read next substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}

		filter_info.focus_offset = strtol(value_, NULL, DEC);

		//read position offset
		value_ = tokenize(NULL);	//read next substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}

		filter_info.position_offset = strtol(value_, NULL, DEC);

		//save struct
		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			filter_info);
	}

	if (variable_type == EEPROMHandler::SENSOR_SETTINGS_)
	{
		YAAASensor::SensorSettings sensor_settings =
		{
			DEF_SENSOR_TYPE,
			DEF_SENSOR_DEVICE,
			DEF_SENSOR_UNIT,
			DEF_SENSOR_PARAMETERS
		};

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_settings.sensor_type_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_settings.sensor_device_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//read next substring
		value_ = tokenize(NULL);

		//check current substring
		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		sensor_settings.unit_ = static_cast<uint8_t>(strtoul(value_, NULL, DEC));

		//parse sensor parameters array
		byte content[sizeof(sensor_settings.parameters_)];

		for (uint8_t i = 0; i < sizeof(content); i++)
		{
			//read next substring
			value_ = tokenize(NULL);

			//check current substring
			if (value_ == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
				return;
			}

			content[i] = static_cast<byte>(strtoul(value_, NULL, HEX));
		}

		memcpy(sensor_settings.parameters_, content, sizeof(sensor_settings.parameters_));

		//save struct
		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			sensor_settings);
	}

	if (variable_type == EEPROMHandler::BUTTON_INFO_)
	{
		Button::ButtonSettings settings;

		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}
		settings.pin_nr_ = static_cast<uint8_t>(atoi(value_));

		value_ = tokenize(NULL);	//read next substring

		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}
		settings.inverted_ = static_cast<bool>(atoi(value_));

		//save struct
		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			settings);
	}

	if (variable_type == EEPROMHandler::KEYPAD_INFO_)
	{
		YAAAKeypad::KeypadSettings settings;

		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}
		settings.type_ = static_cast<uint8_t>(atoi(value_));

		value_ = tokenize(NULL);	//read next substring

		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}
		settings.active_high_ = static_cast<bool>(atoi(value_));

		value_ = tokenize(NULL);	//read next substring

		for (uint8_t i = 0; i < sizeof(settings.parameters_); i++)
		{
			//check current substring
			if (value_ == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
				return;
			}

			//content[i] = static_cast<byte>(atoi(value_));
			settings.parameters_[i] = static_cast<byte>(strtoul(value_, NULL, DEC));

			//read next substring
			value_ = tokenize(NULL);
		}

		if (value_ == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}
		settings.read_interval_ = strtoul(value_, NULL, DEC);

		//save struct
		EEPROMHandler::writeValue(
			setup_device,
			setup_option,
			index,
			settings);
	}

	if (variable_type == EEPROMHandler::STRING_)
	{
		//check if the device and option is valid for a string. 
		uint8_t max_length = EEPROMHandler::getEEPROMMaxStringLength(setup_device, setup_option);
		if (max_length == 0)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		String setup_value = (value_ == NULL ? "" : String(value_));

		//write array to EEPROM
		EEPROMHandler::writeString(
			setup_device,
			setup_option,
			index,
			setup_value);
	}

	//echo the written value to the PC.
	RemoteCommandHandler::readOption(setup_device, setup_option, index, variable_type);
}

//---------------------------------------------------------
//functions for the different devices
void RemoteCommandHandler::telescopeCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::TELESCOPE);

	if (command == telescope_command_::NONE)	//no more commands in message.
		return;

	//send an error message when the command could not be parsed.
	if (command == UNKNOWN)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD);
		return;
	}

	//read command type.
	uint8_t command_type = getCommandType();

	//--------------------------------------------------------------------
	//commands requiring GET / SET parameter

	if (command_type == UNKNOWN)
	{
		//send error message to pc.
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD_TYPE);
		return;
	}

	//CONNECTED GET command received
	if (command == telescope_command_::CONNECTED)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(checkAvailable(TELESCOPE));

			return;
		}
	}

	if (!checkAvailable(TELESCOPE))
	{
		sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
		return;
	}

	//send an error message when trying to use a SET and the focuser is  locked 
	//for local operation.
	else if (checkLocked(TELESCOPE) && (command_type == command_type_::SET))
	{
		sendError(RemoteCommandHandler::ERROR_DEVICE_LOCKED);
		return;
	}

	//CONNECTED SET command received
	if (command == telescope_command_::CONNECTED)
	{
		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//SETUP command received.
	if (command == telescope_command_::SETUP)
	{
		telescopeSetup(command_type);
		return;
	}

	//ABORT_SLEW command received.
	if (command == telescope_command_::ABORT_SLEW)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			//abort slew and moveAxis commands on all axes.
			sendResponse(telescope_->abortSlew());
		}

		return;
	}

	//ALIGNMENT command received.
	if (command == telescope_command_::ALIGNMENT)
	{
		//read axis.
		uint8_t axis = getTelescopeAxis();

		if (axis == UNKNOWN)
		{
			//send error message to pc
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		if (command_type == command_type_::GET)
		{
			//read current alignment from telescope and send to pc.
			sendResponse(telescope_->getCurrentAlignment(axis));
		}

		if (command_type == command_type_::SET)
		{
			//parse alignment.
			char* string_alignment = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_alignment == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			float alignment = static_cast<float>(atof(string_alignment));

			//set alignment.
			sendResponse(telescope_->setCurrentAlignment(axis, alignment));
		}

		return;
	}

	//MOVE command received.
	if (command == telescope_command_::MOVE)
	{
		if (command_type == command_type_::GET)
		{
			//send error message to pc.
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			uint8_t axis = getTelescopeAxis();

			//parse slew destination.			
			char* string_target = tokenize(NULL);

			//when a target is given for both axes, call Telescope::slew()
			if (axis == UNKNOWN || string_target == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			float target = static_cast<float>(atof(string_target));

			//start movement.
			sendResponse(telescope_->move(axis, target));
		}

		return;
	}

	//SLEW command received.
	if (command == telescope_command_::SLEW)
	{
		if (command_type == command_type_::GET)
		{
			//read current slewing state and send to the pc.
			sendResponse(telescope_->isSlewing());
		}

		if (command_type == command_type_::SET)
		{
			//parse slew destination.			
			char* string_x = tokenize(NULL);
			char* string_y = tokenize(NULL);

			//when a target is given for both axes, call Telescope::slew()
			if (string_x != NULL && string_y != NULL)
			{

				float target_x = static_cast<float>(atof(string_x));
				float target_y = static_cast<float>(atof(string_y));

				//set alignment.
				sendResponse(telescope_->slew(target_x, target_y));
			}

			//when no target is given, call Telescope::slewToTarget().
			else if (string_x == NULL && string_y == NULL)
			{
				sendResponse(telescope_->slewToTarget());
			}

			//in any other case send an error message.
			else //if (string_x == NULL || string_y == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			}
		}

		return;
	}

	if (command == telescope_command_::MOVEAXIS)
	{
		if (command_type == command_type_::GET)
		{
			//read current slewing state and send to pc.
			sendResponse(telescope_->isMoveAxis());
		}

		if (command_type == command_type_::SET)
		{
			//read axis.
			uint8_t axis = getTelescopeAxis();

			//parse rate.
			char* string_rate = tokenize(NULL);

			if (axis == UNKNOWN || string_rate == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			float rate = static_cast<float>(atof(string_rate));

			//set alignment.
			sendResponse(telescope_->moveAxis(axis, rate));
		}

		return;
	}

	if (command == telescope_command_::TARGET)
	{
		//read axis.
		uint8_t axis = getTelescopeAxis();

		if (axis == UNKNOWN)
		{
			sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
			return;
		}

		if (command_type == command_type_::GET)
		{
			//read current slew target state and send to pc.
			sendResponse(telescope_->getTarget(axis));
		}

		if (command_type == command_type_::SET)
		{
			//parse target.
			char* string_target = tokenize(NULL);

			if (string_target == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			float target = static_cast<float>(atof(string_target));

			//set alignment.
			sendResponse(telescope_->setTarget(axis, target));
		}

		return;
	}

	//FIND_HOME command received.
	if (command == telescope_command_::FIND_HOME)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(telescope_->isHoming());
		}

		if (command_type == command_type_::SET)
		{
			//activate homing process and send response to pc.
			sendResponse(telescope_->findHome());
		}

		return;
	}

	//AT_HOME command received.
	if (command == telescope_command_::AT_HOME)
	{
		if (command_type == command_type_::GET)
		{
			//attempt to parse optional axis parameter
			uint8_t axis = getTelescopeAxis();

			//if no axis has been given, read if telescope is at its home position 
			//and send response to the pc.
			if (axis == UNKNOWN)
				sendResponse(telescope_->atHome());

			//return at home state of this axis
			else
				sendResponse(telescope_->atHome(axis));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == telescope_command_::HOMING_ERROR)
	{
		if (command_type == command_type_::GET)
		{
			//attempt to parse optional axis parameter
			uint8_t axis = getTelescopeAxis();

			//send response to the pc.
			sendResponse(telescope_->getHomingError(axis));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//PARKED command received.
	if (command == telescope_command_::PARKED)
	{
		if (command_type == command_type_::GET)
		{
			//read current parked state and send response to the pc.
			sendResponse(telescope_->atPark());
		}

		if (command_type == command_type_::SET)
		{
			//parse requested parked state.
			char* string_park = tokenize(NULL);

			if (string_park == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			bool park = static_cast<bool>(atoi(string_park));

			//attempt to park or unpark the telescope.
			if (park)
				sendResponse(telescope_->park());
			else
				sendResponse(telescope_->unpark());
		}

		return;
	}

	//PARK_POS command received.
	if (command == telescope_command_::PARK_POS)
	{
		if (command_type == command_type_::GET)
		{
			uint8_t axis = getTelescopeAxis();

			if (axis == UNKNOWN)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sendResponse(telescope_->getPark(axis));
		}

		if (command_type == command_type_::SET)
		{
			//set the telescope's park position to be its current position.
			telescope_->setPark();

			//send response to pc.
			sendResponse(0);
		}

		return;
	}

	//PULSE_GUIDE command received.
	if (command == telescope_command_::PULSE_GUIDE)
	{
		if (command_type == command_type_::GET)
		{
			//read current pulse guiding state and send response to the pc.
			sendResponse(telescope_->isPulseGuiding());
		}

		if (command_type == command_type_::SET)
		{
			//read axis.
			uint8_t axis = getTelescopeAxis();

			//parse rate.
			char* string_time = tokenize(NULL);
			char* string_rate = tokenize(NULL);

			//send error message to pc when axis is unknown or time or rate strings 
			//could not be tokenized.
			if (axis == UNKNOWN || string_time == NULL || string_rate == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			unsigned long duration = strtoul(string_time, NULL, DEC);
			float rate = static_cast<float>(atof(string_rate));

			//initialte pulse guiding.
			sendResponse(telescope_->pulseGuide(axis, duration, rate));
		}

		return;
	}

	//SWITCH_STATE command received.
	if (command == telescope_command_::SWITCH_STATE)
	{
		if (command_type == command_type_::GET)
		{
			//read axis.
			uint8_t axis = getTelescopeAxis();

			if (axis == UNKNOWN)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			//send switch state to pc.
			sendResponse(telescope_->reportSwitches(axis));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//TRACKING command received.
	if (command == telescope_command_::TRACKING)
	{
		if (command_type == command_type_::GET)
		{
			//read current pulse guiding state and send response to the pc.
			sendResponse(telescope_->isTracking());
		}

		if (command_type == command_type_::SET)
		{
			//parse rate.
			char* string_enable = tokenize(NULL);

			if (string_enable == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			bool enable = static_cast<bool>(atoi(string_enable));

			//initialte pulse guiding.
			sendResponse(telescope_->tracking(enable));
		}

		return;
	}

	//TRACKING_RATE command received.
	if (command == telescope_command_::TRACKING_RATE)
	{
		if (command_type == command_type_::GET)
		{
			//send response to the pc.
			sendResponse(telescope_->getTrackingRate());
		}

		if (command_type == command_type_::SET)
		{
			//parse rate.
			char* string_rate = tokenize(NULL);

			if (string_rate == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t rate = static_cast<uint8_t>(atoi(string_rate));

			//set sidereal tracking rate and send response to pc.
			sendResponse(telescope_->setTrackingRate(rate));
		}

		return;
	}

	//RA_RATE command received.
	if (command == telescope_command_::RA_RATE)
	{
		if (command_type == command_type_::GET)
		{
			//read current pulse guiding state and send response to the pc.
			sendResponse(telescope_->getRightAscensionRate());
		}

		if (command_type == command_type_::SET)
		{
			//parse rate.
			char* string_rate = tokenize(NULL);

			if (string_rate == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD_TYPE);
				return;
			}

			float rate = static_cast<float>(atof(string_rate));

			//set sidereal tracking rate and send response to the pc.
			sendResponse(telescope_->setRightAscensionRate(rate));
		}

		return;
	}

	//DEC_RATE command received.
	if (command == telescope_command_::DEC_RATE)
	{
		if (command_type == command_type_::GET)
		{
			//read current pulse guiding state and send response to the pc.
			sendResponse(telescope_->getDeclinationRate());
		}

		if (command_type == command_type_::SET)
		{
			//parse rate.
			char* string_rate = tokenize(NULL);

			if (string_rate == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			float rate = static_cast<float>(atof(string_rate));

			//set sidereal tracking rate and send response to the pc.
			sendResponse(telescope_->setDeclinationRate(rate));
		}

		return;
	}

	if (command == telescope_command_::TEMP)
	{
		//get the last measured temperature.
		if (command_type == command_type_::GET)
		{
			//parse sensor.
			char* string_sensor = tokenize(NULL);

			if (string_sensor == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t sensor = static_cast<uint8_t>(atoi(string_sensor));

			sendResponse(telescope_->getTemperature(sensor));
		}
		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
	}

	if (command == telescope_command_::TEMP_MEASURE)
	{
		//check if a temperature measurement is completed.
		if (command_type == command_type_::GET)
		{
			//parse sensor.
			char* string_sensor = tokenize(NULL);

			if (string_sensor == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t sensor = static_cast<uint8_t>(atoi(string_sensor));

			sendResponse(telescope_->getTemperatureComplete(sensor));
		}

		//start a temperature measurement.
		if (command_type == command_type_::SET)
		{
			//parse sensor.
			char* string_sensor = tokenize(NULL);

			if (string_sensor == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t sensor = static_cast<uint8_t>(atoi(string_sensor));

			sendResponse(telescope_->measureTemperature(sensor));
		}
	}

	if (command == telescope_command_::WRITE_DEFAULTS)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			Telescope::writeDefaults();
			sendResponse(0);
		}

		return;
	}
}

void RemoteCommandHandler::telescopeSetup(uint8_t command_type)
{
	uint16_t setup_device = EEPROMHandler::device_type_::TELESCOPE;	//default to TELESCOPE.
	uint16_t setup_option = 0;
	uint16_t index = 0;
	uint8_t variable_type = 0;		//size of the variable in EEPROM.

	//parse option to get or set:
	char* string_option = tokenize(NULL);

	//TODO check if string_option is really a telescope axis. change setup command layout to have axis before option.

	setup_option = EEPROMHandler::stringToVariable(setup_device, string_option);

	//if the option is not a (general) telescope option, check if it is a 
	//telescope axis option.
	if (setup_option == UNKNOWN_OPTION)
	{
		setup_device = EEPROMHandler::device_type_::TELESCOPE_AXIS;
		//setup_option = getOption(setup_device, string_option);
		setup_option = EEPROMHandler::stringToVariable(setup_device, string_option);

		//when the option is unknown send an error message to the pc.
		//if (setup_option == UNKNOWN)
		if (setup_option == UNKNOWN_OPTION)
		{
			sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
			return;
		}
	}

	//when the option is a telescope axis option read the telescope axis.
	if (setup_device == EEPROMHandler::device_type_::TELESCOPE_AXIS)
	{
		index = getTelescopeAxis();

		//send error message to pc when the axis is unknown.
		if (index == UNKNOWN)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}
	}

	//get variable size of the variable to be get or set:
	//variable_type = getVariableType(setup_device, setup_option);
	variable_type = EEPROMHandler::getVariableType(setup_device, setup_option);

	if (variable_type == UNKNOWN)
		sendError(RemoteCommandHandler::RemoteCommandHandler::ERROR_INVALID_PARAMETER);

	//extract sensor number when reading or writing sensor settings.
	if (variable_type == EEPROMHandler::SENSOR_SETTINGS_)
	{
		//parse index.
		char* string_index = tokenize(NULL);

		if (string_index == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		index = static_cast<uint8_t>(atoi(string_index));
	}

	if (command_type == command_type_::GET)
	{
		readOption(setup_device, setup_option, index, variable_type);
	}

	if (command_type == command_type_::SET)
	{
		writeOption(setup_device, setup_option, index, variable_type);
	}
}

void RemoteCommandHandler::focuserCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::FOCUSER);

	if (command == filter_wheel_command_::NONE)		//no more commands in message.
		return;

	//send an error message when the command could not be parsed.
	if (command == UNKNOWN)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD);
		return;
	}

	//read command type.
	uint8_t command_type = getCommandType();

	if (command_type == UNKNOWN)
	{
		//send error message to pc.
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD_TYPE);
		return;
	}

	//CONNECTED GET command received.
	if (command == focuser_command_::CONNECTED)
	{
		if (command_type == command_type_::GET)
		{
			//send switch state to pc.
			sendResponse(checkAvailable(FOCUSER));

			return;
		}
	}

	if (!checkAvailable(FOCUSER))
	{
		sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
		return;
	}

	//send an error message when trying to use a SET and the focuser is locked 
	//for local operation.
	else if (checkLocked(FOCUSER) && (command_type == command_type_::SET))
	{
		sendError(RemoteCommandHandler::ERROR_DEVICE_LOCKED);
		return;
	}

	//CONNECTED SET command received.
	if (command == focuser_command_::CONNECTED)
	{
		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == focuser_command_::SETUP)
	{
		focuserSetup(command_type);
		return;
	}

	if (command == focuser_command_::ABSOLUTE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(focuser_->getAbsolute());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == focuser_command_::POSITION)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(focuser_->getPosition());
		}

		if (command_type == command_type_::SET)
		{
			//parse alignment.
			char* string_position = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_position == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			int32_t position = strtol(string_position, NULL, DEC);

			sendResponse(focuser_->setPosition(position));
		}

		return;
	}

	if (command == focuser_command_::MOVE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(focuser_->isMoving());
		}

		if (command_type == command_type_::SET)
		{
			//parse alignment.
			char* string_target = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_target == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			int32_t target = strtol(string_target, NULL, DEC);

			sendResponse(focuser_->move(target));
		}

		return;
	}

	if (command == focuser_command_::MOVE_DIRECTION)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			//parse direction.
			char* string_direction = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_direction == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			bool direction = static_cast<bool>(atoi(string_direction));

			sendResponse(focuser_->moveDirection(direction));
		}

		return;
	}

	if (command == focuser_command_::MOVE_TIME)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			//parse direction.
			char* string_time = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_time == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			int32_t time = strtol(string_time, NULL, DEC);

			sendResponse(focuser_->moveTime(time));
		}

		return;
	}

	//halts the focuser movement.
	if (command == focuser_command_::HALT)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			sendResponse(focuser_->halt());
		}

		return;
	}

	//reads the temperature of the given sensor.
	if (command == focuser_command_::TEMP)
	{
		//get the last measured temperature.
		if (command_type == command_type_::GET)
		{
			//parse sensor.
			char* string_sensor = tokenize(NULL);

			if (string_sensor == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t sensor = static_cast<uint8_t>(atoi(string_sensor));

			sendResponse(focuser_->getTemperature(sensor));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
	}

	//initiate a temperature measurement or check if a measurement is complete.
	if (command == focuser_command_::TEMP_MEASURE)
	{
		//check if a temperature measurement is completed.
		if (command_type == command_type_::GET)
		{
			//parse sensor.
			char* string_sensor = tokenize(NULL);

			if (string_sensor == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t sensor = static_cast<uint8_t>(atoi(string_sensor));

			sendResponse(focuser_->getTemperatureComplete(sensor));
		}

		//start a temperature measurement.
		if (command_type == command_type_::SET)
		{
			//parse sensor.
			char* string_sensor = tokenize(NULL);

			if (string_sensor == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t sensor = static_cast<uint8_t>(atoi(string_sensor));

			sendResponse(focuser_->measureTemperature(sensor));
		}
	}

	if (command == focuser_command_::TEMP_COMP)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(focuser_->getTempCompensation());
		}

		//enable or disable temperature compensation.
		if (command_type == command_type_::SET)
		{
			//parse sensor.
			char* string_enable = tokenize(NULL);

			if (string_enable == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			bool enable = static_cast<bool>(atoi(string_enable));

			sendResponse(focuser_->setTempCompensation(enable));
		}
	}

	if (command == focuser_command_::TEMP_COMP_SENSOR)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(focuser_->getTempCompSensor());
		}

		//enable or disable temperature compensation.
		if (command_type == command_type_::SET)
		{
			//parse sensor.
			char* string_sensor = tokenize(NULL);

			if (string_sensor == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			uint8_t sensor = static_cast<uint8_t>(atoi(string_sensor));

			sendResponse(focuser_->setTempCompSensor(sensor));
		}
	}

	if (command == focuser_command_::FIND_HOME)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(focuser_->isHoming());
		}

		if (command_type == command_type_::SET)
		{
			sendResponse(focuser_->findHome());
		}

		return;
	}

	//SWITCH_STATE command received.
	if (command == focuser_command_::SWITCH_STATE)
	{
		if (command_type == command_type_::GET)
		{
			//send switch state to pc.
			sendResponse(focuser_->reportSwitches());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//WRITE_DEFAULTS command received.
	if (command == focuser_command_::WRITE_DEFAULTS)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			Focuser::writeDefaults();
			sendResponse(0);
		}

		return;
	}
}

void RemoteCommandHandler::focuserSetup(uint8_t command_type)
{
	uint16_t setup_device = EEPROMHandler::device_type_::FOCUSER;
	uint16_t setup_option = 0;
	uint8_t index = 0;
	uint8_t variable_type = 0;		//size of the variable in EEPROM.

	//parse option to get or set:
	//setup_option = getOption(setup_device);
	char* string_option = tokenize(NULL);
	setup_option = EEPROMHandler::stringToVariable(setup_device, string_option);

	//if the option is not a (general) telescope option, check if it is a 
	//telescope axis option.
	if (setup_option == UNKNOWN_OPTION)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
		return;
	}

	//get variable size of the variable to be get or set:
	variable_type = EEPROMHandler::getVariableType(setup_device, setup_option);

	//extract sensor number when reading or writing sensor settings.
	if (variable_type == EEPROMHandler::SENSOR_SETTINGS_)
	{
		//parse index.
		char* string_index = tokenize(NULL);

		if (string_index == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		index = static_cast<uint8_t>(atoi(string_index));
	}

	if (command_type == command_type_::GET)
	{
		readOption(setup_device, setup_option, index, variable_type);
	}

	if (command_type == command_type_::SET)
	{
		writeOption(setup_device, setup_option, index, variable_type);
	}
}

void RemoteCommandHandler::filterWheelCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::FILTER_WHEEL);

	if (command == filter_wheel_command_::NONE)		//no more commands in message.
		return;

	//send an error message when the command could not be parsed.
	if (command == UNKNOWN)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD);
		return;
	}

	//read command type.
	uint8_t command_type = getCommandType();

	if (command_type == UNKNOWN)
	{
		//send error message to pc.
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD_TYPE);
		return;
	}

	//CONNECTED GET command received.
	if (command == telescope_command_::CONNECTED)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(checkAvailable(FILTER_WHEEL));

			return;
		}
	}

	if (!checkAvailable(FILTER_WHEEL))
	{
		sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
		return;
	}

	//send an error message when trying to use a SET and the filter wheel is locked 
	//for local operation.
	else if (checkLocked(FILTER_WHEEL) && (command_type == command_type_::SET))
	{
		sendError(RemoteCommandHandler::ERROR_DEVICE_LOCKED);
		return;
	}

	//CONNECTED SET command received.
	if (command == telescope_command_::CONNECTED)
	{
		if (command_type == command_type_::SET)
		{
			//send an error message to the pc when the command type is invalid for this command.
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == filter_wheel_command_::SETUP)
	{
		filterWheelSetup(command_type);
		return;
	}

	if (command == filter_wheel_command_::POSITION)
	{
		if (command_type == command_type_::GET)
		{
			//read currently set filter and send response.
			sendResponse(filter_wheel_->getFilter());
		}

		if (command_type == command_type_::SET)
		{
			//parse alignment.
			char* string_position = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_position == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			int16_t position = atoi(string_position);

			//set filter.
			sendResponse(filter_wheel_->moveFilter(position));
		}

		return;
	}

	if (command == filter_wheel_command_::CURRENT_FILTER)
	{
		if (command_type == command_type_::GET)
		{
			//send an error message to the pc when the command type is invalid for this command.
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			//parse alignment.
			char* string_filter = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_filter == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			int16_t filter = atoi(string_filter);

			//set filter.
			sendResponse(filter_wheel_->setCurrentFilter(filter));
		}

		return;
	}

	if (command == filter_wheel_command_::FOCUS_OFFSET)
	{
		if (command_type == command_type_::GET)
		{
			//parse alignment.
			char* string_filter = tokenize(NULL);

			//send error messag to pc when string could not be tokenized.
			if (string_filter == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			int16_t filter = atoi(string_filter);

			//send response to the pc.
			sendResponse(filter_wheel_->getFocusOffset(filter));
		}

		if (command_type == command_type_::SET)
		{
			//send an error message to the pc when the command type is invalid for this command.
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == filter_wheel_command_::NUMBER_OF_FILTERS)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(filter_wheel_->getNumberOfFilters());
		}

		if (command_type == command_type_::SET)
		{
			//send an error message to the pc when the command type is invalid for this command.
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == filter_wheel_command_::SWITCH_STATE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(filter_wheel_->reportSwitches());
		}

		if (command_type == command_type_::SET)
		{
			//send an error message to the pc when the command type is invalid for this command.
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//AT_HOME command received.
	if (command == filter_wheel_command_::AT_HOME)
	{
		if (command_type == command_type_::GET)
		{
			//send response to the pc.
			sendResponse(filter_wheel_->atHome());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == filter_wheel_command_::FIND_HOME)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(filter_wheel_->isHoming());
		}

		if (command_type == command_type_::SET)
		{
			sendResponse(filter_wheel_->findHome());
		}

		return;
	}

	if (command == filter_wheel_command_::MEASURE_HOME_OFFSET)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(filter_wheel_->isMeasuringHomeOffset());
		}

		if (command_type == command_type_::SET)
		{
			sendResponse(filter_wheel_->measureHomeOffset());
		}

		return;
	}

	if (command == filter_wheel_command_::HOMING_ERROR)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(filter_wheel_->getHomingError());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	if (command == filter_wheel_command_::WRITE_DEFAULTS)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			FilterWheel::writeDefaults();
			sendResponse(0);
		}

		return;
	}
}

void RemoteCommandHandler::filterWheelSetup(uint8_t command_type)
{
	uint16_t setup_device = EEPROMHandler::device_type_::FILTER_WHEEL;
	uint16_t setup_option = 0;
	uint8_t index = 0;
	uint8_t variable_type = 0;		//size of the variable in EEPROM.

	//parse option to get or set:
	//setup_option = getOption(setup_device);
	char* string_option = tokenize(NULL);
	setup_option = EEPROMHandler::stringToVariable(setup_device, string_option);

	//if the option is not a (general) telescope option, check if it is a 
	//telescope axis option.
	if (setup_option == UNKNOWN_OPTION)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
		return;
	}

	//if a filter info is get or set, read the filter number.
	if (setup_option == EEPROMHandler::filter_wheel_variable_::FILTER_INFO ||
		setup_option == EEPROMHandler::filter_wheel_variable_::TEMPERATURE_SENSOR)
	{
		//parse filter number:
		char* string_filter = tokenize(NULL);

		if (string_filter == NULL)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_PARAMETER);
			return;
		}

		index = static_cast<uint8_t>(atoi(string_filter));
	}

	//todo: move to readOption() / writeOption() ?
	//get variable size of the variable to be get or set:
	variable_type = EEPROMHandler::getVariableType(setup_device, setup_option);

	if (command_type == command_type_::GET)
	{
		readOption(setup_device, setup_option, index, variable_type);
	}

	if (command_type == command_type_::SET)
	{
		writeOption(setup_device, setup_option, index, variable_type);
	}
}

void RemoteCommandHandler::generalCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::GENERAL);

	if (command == general_command_::NONE)
		return;	//no more commands in message.

	//send an error message when the command could not be parsed.
	if (command == UNKNOWN)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD);
		return;
	}

	//read command type.
	uint8_t command_type = getCommandType();

	if (command_type == UNKNOWN)
	{
		//send error message to pc.
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_CMD_TYPE);
		return;
	}

	if (command == general_command_::ONEWIRE_TEMP)
	{
		if (command_type == command_type_::GET)
		{
			//parse temperature sensor number.
			char* string_number = tokenize(NULL);

			//when no sensor number has been given return the number of sensors
			if (string_number == NULL)
				sendResponse(SensorTemperatureOneWire::getNumSensors());

			else
			{
				uint8_t number = static_cast<uint8_t>(atoi(string_number));
				uint8_t addr[8];

				//read serial number and send it to the pc.
				if (SensorTemperatureOneWire::getSensorSerial(number, addr))
				{
					//convert array to string
					String address = "";

					for (uint8_t i = 0; i < 8; i++)
					{
						address += String(addr[i], HEX);

						if (i < 7)
							address += ' ';
					}

					//send serial number to pc
					sendResponse(address);
				}
				else
					sendResponse(false);
			}
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//TIME command received
	if (command == general_command_::TIME)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(YAAATime::getTime());
		}

		if (command_type == command_type_::SET)
		{
			time_t time = 0;

			char* string_time = tokenize(NULL);

			if (string_time == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			time = static_cast<time_t>(strtoul(string_time, NULL, DEC));

			YAAATime::setDateTime(time);

			sendResponse(YAAATime::getTime());
		}

		return;
	}

	//DATETIME command received
	if (command == general_command_::DATETIME)
	{
		if (command_type == command_type_::GET)
		{
			char *response = new char[20];
			YAAATime::timeToISO8601(response);
			sendResponse(response);
			delete response;
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//UTC_OFFSET command received
	if (command == general_command_::UTC_OFFSET)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(YAAATime::getUTCOffset());
		}

		if (command_type == command_type_::SET)
		{
			char* string_offset = tokenize(NULL);

			if (string_offset == NULL)
			{
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			sendResponse(YAAATime::setUTCOffset(atof(string_offset)));
		}

		return;
	}

	//LOG_MODE command received
	if (command == general_command_::LOG_MODE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(log_.getLogMode());
		}

		if (command_type == command_type_::SET)
		{
			// parse requested log state.
			char* string_log = tokenize(NULL);

			if (string_log == NULL)
			{
				//send error message to pc.
				sendError(RemoteCommandHandler::ERROR_MISSING_PARAMETER);
				return;
			}

			bool log = static_cast<bool>(atoi(string_log));

			sendResponse(log_.setLogMode(log));
		}

		return;
	}

	//LOG_STATE command received
	if (command == general_command_::LOG_STATE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(log_.getLogState());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//LOG_DUMP command received
	if (command == general_command_::LOG_DUMP)
	{
		if (command_type == command_type_::GET)
		{
			//send false if the YAAALog class was not initialized.
			if (log_.getLogState() <= YAAALog::LOG_NOT_INITIALIZED)
			{
				sendResponse(false);
				return;
			}

			log_.startDump();

			while (log_.hasLine())
				sendRaw(log_.getLine());

			log_.stopDump();

			sendResponse(true);
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		return;
	}

	//LOG_CLEAR command received
	if (command == general_command_::LOG_CLEAR)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			sendResponse(log_.clearLog());
		}

		return;
	}

	////SOFTWARE_VERSION command 
	//if (command == general_command_::SOFTWARE_VERSION)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(YAAAVERSION);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}
	//
	////SOFTWARE_DATETIME command 
	//if (command == general_command_::SOFTWARE_DATETIME)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(YAAADATETIME);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}
	//
	////DEVICE_NAME command 
	//if (command == general_command_::DEVICE_NAME)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(DEVICE_NAME);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}
	//
	////DEVICE_INFO command 
	//if (command == general_command_::DEVICE_INFO)
	//{
	//	if (command_type == command_type_::GET)
	//	{
	//		sendResponse(DEVICE_INFO);
	//	}
	//
	//	if (command_type == command_type_::SET)
	//	{
	//		sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
	//	}
	//
	//	return;
	//}

	if (command == general_command_::WRITE_DEFAULTS)
	{
		if (command_type == command_type_::GET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}

		if (command_type == command_type_::SET)
		{
			writeAllDefaults();
			sendResponse(true);
		}
		return;
	}

	//SETUP command received.
	if (command == general_command_::SETUP)
	{
		generalSetup(command_type);
		return;
	}

	if (command == general_command_::CONNECTED)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(true);
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

#if !defined(ESP8266) && !defined(ESP32)
	
#ifdef FEATURE_ETHERNET
	if (command == general_command_::ETHERNET_IP)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_IP));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_DNS)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_DNS));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_GATEWAY)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_GATEWAY));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_SUBNET)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_SUBNET));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::ETHERNET_PORT)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_ETHERNET])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_ETHERNET]->getConfig(CommandInterfaceEthernet::CONFIG_PORT));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}
#endif /* FEATURE_ETHERNET */

#endif /* !defined(ESP8266) && !defined(ESP32) */

#if defined(ESP8266) || defined(ESP32)
	if (command == general_command_::WIFI_IP)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_IP));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_DNS)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_DNS));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_GATEWAY)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_GATEWAY));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_SUBNET)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_SUBNET));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_PORT)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_PORT));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::WIFI_RSSI)
	{
		if (command_type == command_type_::GET)
		{
			if (!command_interfaces_[INTERFACE_WIFI])
				sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
			else
				sendResponse(command_interfaces_[INTERFACE_WIFI]->getConfig(CommandInterfaceWifi::CONFIG_RSSI));
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::OTA_ENABLED)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(OTAHandler::getOTAMode() != 0);
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}

	if (command == general_command_::OTA_MODE)
	{
		if (command_type == command_type_::GET)
		{
			sendResponse(OTAHandler::getOTAMode());
		}

		if (command_type == command_type_::SET)
		{
			sendError(RemoteCommandHandler::ERROR_INVALID_CMD_TYPE);
		}
		return;
	}
#endif /* defined(ESP8266) || defined(ESP32) */
}

void RemoteCommandHandler::generalSetup(uint8_t command_type)
{
	uint16_t setup_device = EEPROMHandler::device_type_::GENERAL;
	uint16_t setup_option = 0;
	uint8_t index = 0;
	uint8_t variable_type = 0;

	//parse option to get or set:
	//setup_option = getOption(setup_device);
	char* string_option = tokenize(NULL);
	setup_option = EEPROMHandler::stringToVariable(setup_device, string_option);

	//send error response if the setup option was not recognized.
	if (setup_option == UNKNOWN_OPTION)
	{
		sendError(RemoteCommandHandler::ERROR_UNKNOWN_OPTION);
		return;
	}

	//get variable size of the variable to get or set:
	variable_type = EEPROMHandler::getVariableType(setup_device, setup_option);

	if (command_type == command_type_::GET)
	{
		readOption(setup_device, setup_option, index, variable_type);
	}

	if (command_type == command_type_::SET)
	{
		writeOption(setup_device, setup_option, index, variable_type);
	}
}

void RemoteCommandHandler::cameraRotatorCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::CAMERA_ROTATOR);

	//camera rotator control not implemented yet!
	sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
	//tokenize(NULL);
}

void RemoteCommandHandler::domeCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::DOME);

	//dome control not implemented yet!
	sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
	//tokenize(NULL);
}

void RemoteCommandHandler::safetyMonitorCommand()
{
	//parse telescope command.
	uint8_t command = parseCommand(RemoteCommandHandler::SAFETY_MONITOR);

	//safety monitor control not implemented yet!
	sendError(RemoteCommandHandler::ERROR_DEVICE_UNAVAILABLE);
	//tokenize(NULL);
}

void RemoteCommandHandler::writeDefaults()
{
	CommandInterfaceSerial::writeDefaults();

#if !defined(ESP8266) && !defined(ESP32)

#ifdef FEATURE_ETHERNET
	CommandInterfaceEthernet::writeDefaults();
#endif /* FEATURE_ETHERNET */
#endif /* !defined(ESP8266) && !defined(ESP32) */

#if defined(ESP8266) || defined(ESP32)
	CommandInterfaceWifi::writeDefaults();
#endif /* defined(ESP8266) || defined(ESP32) */
}

RemoteCommandHandler remote_command_handler_;
