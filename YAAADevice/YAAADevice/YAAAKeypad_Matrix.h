//MAtrix type Keypad class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAAKEYPAD_MATRIX_H_
#define YAAAKEYPAD_MATRIX_H_

#include <Arduino.h>
#include "YAAAKeypad.h"

class YAAAKeypad_Matrix :
	public YAAAKeypad
{
public:
	enum keypad_layout_t
	{
		LAYOUT3x4STD = 0,		//standard 3x4 layout (0-9, *, #)
		LAYOUT4x4STD = 1,		//standard 4x4 layout (0-9, *, #, A, B, C, D)
	};

	YAAAKeypad_Matrix(YAAAKeypad::KeypadSettings settings);
	~YAAAKeypad_Matrix();

	/*
	@return the pressed key as YAAAKeypad::key_t*/
	YAAAKey getKey();

private:
	/*
	reads the inputs pins of the keypad and returns 2 bytes indicating the state of
	the input pins.
	@return: 2 bytes containing indices of pressed buttons. 
	*/
	uint16_t read();

	uint8_t getKeyCode(uint16_t bits);

	uint8_t num_cols_;
	uint8_t num_rows_;

	//arrays containing input pins
	uint8_t *col_pins_;
	uint8_t *row_pins_;

	uint8_t *matrix_;		//button to keys assignment 1D-matrix

	uint8_t layout_;

	bool active_high_;		//default: false
};

#endif /* YAAAKEYPAD_MATRIX_H_ */

