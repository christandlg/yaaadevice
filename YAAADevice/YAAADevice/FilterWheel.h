//Filter Wheel class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef FILTERWHEEL_H_
#define FILTERWHEEL_H_

#include <Arduino.h>

#include <limits.h>

#include "defaults.h"

#include "EEPROMHandler.h"
#include "Button.h"

#include "YAAASensor.h"
#include "SensorTemperature.h"
#include "SensorTemperatureOneWire.h"
#include "SensorTemperatureAnalogIC.h"
#include "SensorTemperatureThermistor.h"
#include "SensorTemperatureMLX90614.h"

#include "StepperMotor.h"
#include "StepperMotorDriver.h"
#include "StepperMotorAFMSV2.h"
#include "StepperMotorFourWire.h"

class FilterWheel
{
public:
	enum filter_holder_type_t: uint8_t
	{
		SLIDER = 0,
		WHEEL = 1
	};

	enum error_code_t : uint8_t
	{
		ERROR_NONE = 0,				//no error occured.
		ERROR_INVALID_FILTER = 1,	//invalid filter number given.
		ERROR_MOVING = 2,			//filter holder moving.
		ERROR_CURRENT_FILTER = 4,	//requested filter already selected.
		ERROR_HOMING = 8,			//filter wheel is currently homing.
	};

	enum error_code_homing_t : uint8_t
	{
		ERROR_HOMING_NONE = 0,					//no error occurred.
		ERROR_HOMING_LIMIT_LOWER = 8,					//homing position beyond lower movement limit
		ERROR_HOMING_LIMIT_UPPER = 16,					//homing position beyond upper movement limit
		ERROR_HOMING_SWITCH_NOT_AVAILABLE = 32,			//no home switch available.
		//ERROR_HOMING_SWITCH_NOT_TRIGGERED = 64,	//homing switch has not been triggered, but both limits or endstops have been hit.	//TODO implement
		ERROR_HOMING_TIMEOUT = 128				//homing operation timed out.
	};

	enum lock_source_t : uint8_t
	{
		LOCK_NONE = 0,				//device currently not locked.
		LOCK_LOCAL = 1,				//device currently locally locked
		LOCK_REMOTE = 2				//device currently remote locked.
	};

	enum filter_type_t : uint8_t
	{
		FILTER_NONE = 0,     //no filter in slot
		FILTER_CLEAR = 1,

		//color filters
		FILTER_VIOLET = 10,
		FILTER_BLUE = 11,
		FILTER_GREEN = 12,
		FILTER_YELLOW = 13,
		FILTER_ORANGE = 14,
		FILTER_RED = 15,

		FILTER_DARK_VIOLET = 20,
		FILTER_DARK_BLUE = 21,
		FILTER_DARK_GREEN = 22,
		FILTER_DARK_YELLOW = 23,
		FILTER_DARK_ORANGE = 24,
		FILTER_DARK_RED = 25,

		FILTER_LIGHT_VIOLET = 30,
		FILTER_LIGHT_BLUE = 31,
		FILTER_LIGHT_GREEN = 32,
		FILTER_LIGHT_YELLOW = 33,
		FILTER_LIGHT_ORANGE = 34,
		FILTER_LIGHT_RED = 35,

		//color mixtures
		FILTER_BLUE_GREEN = 40,
		FILTER_GEEN_YELLOW = 41,

		//polarization filters
		FILTER_POLARIZATION = 110,

		//celestial objects filters
		FILTER_MOON = 130,
		FILTER_SUN = 131,

		//other filters
		FILTER_ARTIFICIAL = 140,
		FILTER_PHOTOMETRIC = 141,

		//bandpass filters
		FILTER_LUMINANCE = 150,		//allows whole visible spectrum to pass

		//high pass filters
		FILTER_IR = 170,

		//low pass filters
		FILTER_UV = 180,

		//unkown filter
		FILTER_UNKNOWN = 255
	};

	struct FilterInfo
	{
		uint8_t type;				//filter type
		uint8_t RFU;				//RFU
		int32_t focus_offset;		//focus offset in microns.
		int32_t position_offset;	//position offset of the filter on the filter holder.
	};

	/*
	default contructor. reads stored settings from the Arduino's EEPROM, initializes 
	a StepperMotor instance and home / endstop switches.*/
	FilterWheel();
	~FilterWheel();
	
	/*
	checks endstops and limits, moves stepper.*/
	void run();

	/*
	disables all movement commands (homing, move, ...)*/
	void stop();

	/*
	stops the stepper.
	stepper is stopped as quickly as possible according to speed and acceleration parameters.
	@param: true: stepper is stopped immediately; false: stepper decelerates.*/
	void stop(bool hard_stop);

	/*
	@return: true if the wheel is moving. */
	bool isMoving();

	/*
	@return: true if the home position can be found (a home switch exists). */
	bool canFindHome();

	/*
	initiates homing. if called during a homing operation, homing is restarted.
	@return: true on success, false otherwise. on false, call getHomingError to get more information 
	on why it failed. */
	bool findHome();

	/*
	@return: true if the filter wheel is a its home position.*/
	bool atHome();

	/*
	@return: true if homing is in progress, false otherwise.*/
	bool isHoming();

	/*
	@return: error codes as error_code_homing_t generated during the last find home operation.*/
	uint8_t getHomingError();

	/*
	@return: true if home offset measurement is in progress.*/
	bool isMeasuringHomeOffset();

	/*
	measure offset between home position and filter 0 position. sets the current stepper position 
	to 0 and starts a slightly modified homing operation that updates the home offset to the 
	stepper position instead of the stepper position to the home offset. the updated home 
	offset value is written to the Arduino's EEPROM.
	requires that the user manually moves the filter wheel / holder to filter position 0.
	@return: true when measuring home offset was started successfully.*/
	bool measureHomeOffset();

	/*
	sets the current position to be the steppers home position.*/
	void setHome();

	/*
	move filter wheel to new position.
	when the filter wheel is a wheel (and is not a slider), moveFilter() moves the 
	filter wheel in the direction with fewer steps to the target.
	@param: number of filter to be made the active filter (0 - n).
	@return: bits of the return byte are set to 1 on errors.
	MSB - - - - - - LSB
		x x x x x x x x
		| | | | | | | |
		| | | | | | | - invalid filter number given.
		| | | | | | - not used.
		| | | | | -	filter wheel already at requested position.
		| | | | - filter wheel is currently homing.
		| | | -	not used.
		| | - not used.
		| - not used.
		- not used. */
	uint8_t moveFilter(int16_t filter);

	/*
	move the filter wheel to the next filter. moves to filter 0 if called if the current
	filter is the last filter on the filter holder.
	@return: see moveFilter()*/
	uint8_t moveNextFilter();

	/*
	move the filter wheel to the previous filter. moves to the last filter if the current
	filter is the first filter on the filter holder.
	@return: see moveFilter()*/
	uint8_t movePrevFilter();

	/*@return: the current filter number when the filter wheel is stationary,
	-1 when the filter wheel is moving. */
	int16_t getFilter();

	/*returns the focus offset for the given filter (in micrometers).
	@param: filter number (0 to N-1)
	@return: focus offset in micrometers, read from EEPROM.*/
	long getFocusOffset(int16_t filter);

	/*
	set the currently active filter. does not acutally move the filter wheel.
	@param: number of filter to be made the active filter (0 - n).
	@return: bits of the return byte are set to 1 on errors.
	MSB - - - - - - LSB
		x x x x x x x x
		| | | | | | | |
		| | | | | | | - invalid filter number given.
		| | | | | | - filter wheel is currently moving.
		| | | | | -	filter wheel already at requested position.
		| | | | - filter wheel is currently homing.
		| | | -	not used.
		| | - not used.
		| - not used.
		- not used.*/
	uint8_t setCurrentFilter(int16_t filter);

	/*@return: number of filters in the filterwheel*/
	uint8_t getNumberOfFilters();

	/*
	@return: endstop / home switch information. 
	MSB - - - - - - LSB
		x x x x x x x x
		| | | | | | | |
		| | | | | | | - lower endstop triggered.
		| | | | | | - upper endstop triggered.
		| | | | | -	home switch triggered.
		| | | | - not used.
		| | | -	not used.
		| | - not used.
		| - not used.
		- not used.*/
	uint8_t reportSwitches();

	/*returns lock source as lock_source_t.*/
	uint8_t getLocked();

	/*sets lock source.
	@param source: lock source as lock_source_t.*/
	void setLocked(uint8_t source);

	/*returns the temperature measured by the given sensor.
	@param sensor: sensor number to use (0 - n-1)
	@return: temperature in the requested temperature unit or 999.0 to indicate a
	failed measurement.*/
	float getTemperature(uint8_t sensor);

	//@param temperature sensor to check
	//@return: true when the given sensor has completed a temperature measurement.
	bool getTemperatureComplete(uint8_t sensor);

	//initiates a temperature measurement.
	//@param sensor using to measure
	//@return true if the measurement was started successfully, false otherwise.
	bool measureTemperature(uint8_t sensor);

	/*
	writes default values to the Arduino's EEPROM.
	this function will be called when the eeprom control number check in the main setup function
	fails or the user/driver sends a restore defaults command.*/
	static void writeDefaults();

private:
	//checks if the given sensor is available.
	//@param sensor: sensor to check.
	//@return: true if the sensor has been successfully initialized, false otherwise.
	bool checkSensor(uint8_t sensor);

	StepperMotor *stepper_motor_;

	float steps_between_filters_;

	uint8_t number_of_filters_;
	uint8_t current_filter_;

	uint8_t filter_holder_type_;

	bool moving_;
	bool measuring_home_offset_;

	bool homing_;
	uint8_t homing_error_;
	bool home_found_;
	bool home_cleared_;
	int32_t home_offset_;
	bool startup_auto_home_;

	bool home_first_pos_found_;
	int32_t home_first_pos_;

	uint32_t home_start_time_;
	uint32_t home_timeout_;

	bool limit_lower_enabled_;
	bool limit_upper_enabled_;
	int32_t limit_lower_;
	int32_t limit_upper_;

	bool hard_stop_endstop_;
	bool hard_stop_limit_;

	Button *home_switch_;
	Button *endstop_lower_;
	Button *endstop_upper_;

	int32_t *position_offsets_;

	/*returns true if the distance to the target is 0 and the stepper is not moving any more.*/
	bool atDestination();

	/*
	checks and returns the state of the endstop switches.
	 - when an endstop is hit:
	 - - sets bits of the return value depending on endstop (see below).
	 - - when the stepper moves away from the endstop, does nothing.
	 - - when the stepper moves towards the endstop:
	 - - - when homing is active, does not stop but reverses stepper direction. 
	 - - when homing is not active:
	 - - - calls stop(axis) to stop the stepper.
	@return: byte indicating if (and which) endstop has been hit.
	possible return values are:
	MSB - - - - - - LSB
		x x x x x x x x
		| | | | | | | |
		| | | | | | | - lower endstop hit.
		| | | | | | - upper endstop hit.
		| | | | | -	not used.
		| | | | - not used.
		| | | -	not used.
		| | - not used.
		| - not used.
		- not used.*/
	uint8_t checkEndstops();

	uint8_t endstop_state_;		//last read state of the endstops.

	/*
	checks and returns the state of the software movement limits.
	 - when a limit is triggered:
	 - - sets bits of the return value depending on endstop (see below).
	 - - when the stepper moves away from the limit, does nothing.
	 - - when the stepper moves towards the limit:
	 - - - when homing is active, does not stop but reverses stepper direction. 
	 - - when homing is not active:
	 - - - calls stop() to stop the stepper.
	@return: byte indicating if (and which) limit was triggered.
	possible return values are:
	MSB - - - - - - LSB
		x x x x x x x x
		| | | | | | | |
		| | | | | | | - lower limit triggered.
		| | | | | | - upper limit triggered.
		| | | | | -	not used.
		| | | | - not used.
		| | | -	not used.
		| | - not used.
		| - not used.
		- not used.*/
	uint8_t checkLimits();

	uint8_t limit_state_;		//last read state of the limits.

	/*
	clears the home switch.
	 - checks the state of the home switch.
	 - when the home switch has been cleared, reverses the homing direction and enables
		 normal homing.*/
	void clearHome();

	/*
	moves the filter wheel to the home position and sets the position to the filter wheel
	home position. 
	home position is triggered externally (switch).*/
	void home();

	/*
	@param: filter number (0 to N-1)
	@return: location offset of filter in steps, read from EEPROM.*/
	long getLocationOffset(int16_t filter);

	/*
	decides if a stepper is to be released or enabled.
	running in quickstep mode.
	 - releases the stepper when it has reached its destination.
	 - enables the stepper when it is released but is not at its destination.*/
	//void automaticRelease();

	uint8_t locked_;			//locked state of the filter wheel (LOCK_NONE, LOCK_LOCAL or LOCK_REMOTE)

	SensorTemperature *temperature_sensors_[TEMP_SENSORS];

	template <class T> static void readEEPROM(EEPROMHandler::filter_wheel_variable_::FilterWheelVariable_t_ variable, T& value, uint8_t index = 0)
	{
		EEPROMHandler::readValue
			(
			EEPROMHandler::device_type_::FILTER_WHEEL, 
			variable, 
			index, 
			value
			);
	};

	template <class T> static void writeEEPROM(EEPROMHandler::filter_wheel_variable_::FilterWheelVariable_t_ variable, const T& value, uint8_t index = 0)
	{
		EEPROMHandler::writeValue
			(
			EEPROMHandler::device_type_::FILTER_WHEEL, 
			variable, 
			index, 
			value
			);
	};
};

#endif /* FILTERWHEEL_H_ */

