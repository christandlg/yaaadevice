//TTP229B class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "YAAAKeypad_TTP229B.h"

YAAAKeypad_TTP229B::YAAAKeypad_TTP229B(YAAAKeypad::KeypadSettings settings) :
	YAAAKeypad(YAAAKeypad::TYPE_TWI),
	pin_SCL_(settings.parameters_[0]), pin_SDO_(settings.parameters_[0]),
	active_high_(settings.active_high_)
{
	read_interval_ = settings.read_interval_;

	pinMode(pin_SCL_, OUTPUT);
	pinMode(pin_SDO_, INPUT);

	digitalWrite(pin_SCL_, HIGH ^ active_high_);
}

YAAAKeypad_TTP229B::~YAAAKeypad_TTP229B()
{
}

YAAAKey YAAAKeypad_TTP229B::getKey()
{
	read_last_ = millis();

	//read currently pressed key
	uint8_t key_code = getKeyCode(read());

	updateKey(key_code);

	return key_;
}

uint16_t YAAAKeypad_TTP229B::read()
{
	uint16_t value = 0;

	//clock 16 times to read everything
	for (uint8_t i = 0; i < 16; i++)
	{
		digitalWrite(pin_SCL_, HIGH ^ active_high_);
		delayMicroseconds(10);
		digitalWrite(pin_SCL_, LOW ^ active_high_);
		delayMicroseconds(10);

		value |= (((digitalRead(pin_SDO_) == HIGH) ^ active_high_) << i);
	}

	return value;
}

uint8_t YAAAKeypad_TTP229B::getKeyCode(uint16_t bits)
{
	switch (bits)
	{
		case 0x0000:
			return YAAAKey::KEY_NONE;
		case 0x0001:
			return YAAAKey::KEY_1;
		case 0x0002:
			return YAAAKey::KEY_2;
		case 0x0004:
			return YAAAKey::KEY_3;
		case 0x0008:
			return YAAAKey::KEY_ENTER;
		case 0x0010:
			return YAAAKey::KEY_4;
		case 0x0020:
			return YAAAKey::KEY_5;
		case 0x0040:
			return YAAAKey::KEY_6;
		case 0x0080:
			return YAAAKey::KEY_UP;
		case 0x0100:
			return YAAAKey::KEY_7;
		case 0x0200:
			return YAAAKey::KEY_8;
		case 0x0400:
			return YAAAKey::KEY_9;
		case 0x0800:
			return YAAAKey::KEY_DOWN;
		case 0x1000:
			return YAAAKey::KEY_ESC;
		case 0x2000:
			return YAAAKey::KEY_0;
		case 0x4000:
			return YAAAKey::KEY_LEFT;
		case 0x8000:
			return YAAAKey::KEY_RIGHT;
		default:
			return YAAAKey::KEY_INVALID;
	}
}

