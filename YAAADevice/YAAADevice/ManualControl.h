//Manual Control class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef MANUALCONTROL_H_
#define MANUALCONTROL_H_

#include "Telescope.h"
extern Telescope *telescope_;

#include "FilterWheel.h"
extern FilterWheel *filter_wheel_;

#include "EEPROMHandler.h"

#include "InfoDisplay.h"

//#include <TimerOne.h>
//#include <ClickEncoder.h>

#include "YAAAKeypad_TTP229B.h"
#include "YAAAKeypad_Matrix.h"
#include "YAAAKeypad_Generic.h"

#include "Joystick.h"
#include "Button.h"

class ManualControl
{
public:
	//enum input_type_t
	//{
	//	INPUT_NONE = 0,				//no input.
	//	INPUT_DIGITAL = 1,			//uses a digital input (= button).
	//	INPUT_ANALOG = 2,			//uses an analog input (joystick, potentiometer, ...)
	//	INPUT_ROTARY_ENCODER = 4	//uses a rotary encoder
	//};

	ManualControl();
	~ManualControl();

	/*
	reads settings from EEPROM and initializes control devices (buttons, joystick etc).*/
	void init();

	//handles input by digital and analog input pins.
	void run();

	//writes default settings to the EEPROM.
	static void writeDefaults();

private:
	//filter wheel manual control section
	//--------------------------------------------------------------------
	bool filter_wheel_mc_available_;

	Button *btn_filter_wheel_mc_lock_;	//button to lock / unlock the filter wheel.
	Button *btn_filter_wheel_mc_next_;	
	Button *btn_filter_wheel_mc_prev_;

	//--------------------------------------------------------------------
	//focuser manual control section
	bool focuser_mc_available_;

	Button *btn_focuser_mc_lock_;
	Button *btn_focuser_mc_in_;
	Button *btn_focuser_mc_out_;

	uint8_t focuser_locked_state_;		//locked state of the focuser before manual control.
	
	//--------------------------------------------------------------------
	//telescope manual control section
	bool telescope_mc_available_;

	Button *btn_telescope_mc_lock_;		//button for enabling / disabling manual control
	Button *btn_telescope_mc_speed_;	//button for speed selection

	Joystick *jst_telescope_mc_;		//joystick for manual control of x and y axes

	uint8_t telescope_mc_speed_;		//speed setting for manual control 
	int16_t telescope_mc_speed_x_;		//currently active speed for x axis
	int16_t telescope_mc_speed_y_;		//currently active speed for y axis

	uint8_t telescope_locked_state_;	//locked state of the telescope before manual control.

	//--------------------------------------------------------------------
	//infodisplay manual control section

	YAAAKeypad *keypad_;

	Button *btn_infodisplay_up_;
	Button *btn_infodisplay_down_;
	Button *btn_infodisplay_left_;
	Button *btn_infodisplay_right_;
	Button *btn_infodisplay_plus_;
	Button *btn_infodisplay_minus_;

	//--------------------------------------------------------------------
	//handles manual control input and function calls for the filter wheel.
	void filterWheelManualControl();

	//handles manual control input and function calls for the focuser.
	void focuserManualControl();

	//handles manual control input and function calls for the telescope.
	void telescopeManualControl();

	//passes manual control input to the InfoDisplay class.
	void infoDisplayManualControl();

	//ClickEncoder *display_contrast_encoder_;

	//----------------------------------------------------------------------------------------------
	//templates for reading and writing to/from the EEPROM.
	template <class T> static void readEEPROM(EEPROMHandler::general_variable_::GeneralVariable_t_ variable, T& value)
	{
		EEPROMHandler::readValue
			(
			EEPROMHandler::device_type_::GENERAL,
			variable,
			0,
			value
			);
	};

	template <class T> static void writeEEPROM(EEPROMHandler::general_variable_::GeneralVariable_t_ variable, const T& value)
	{
		EEPROMHandler::writeValue
			(
			EEPROMHandler::device_type_::GENERAL,
			variable,
			0,
			value
			);
	};
};

extern ManualControl manual_control_;

#endif /*MANUALCOLTROL_H_*/

