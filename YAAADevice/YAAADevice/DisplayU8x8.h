//U8x8 Display class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef DisplayU8X8_H_
#define DisplayU8X8_H_

#include <Arduino.h>

#include <U8x8lib.h>
#include "Display.h"

class DisplayU8x8 :
	public Display 
{
public:
	DisplayU8x8(
	uint16_t res_x,
	uint16_t res_y,
	uint8_t pin_enable = 255,
	uint8_t i2c_sda = 255,
	uint8_t i2c_scl = 255
	);

private:
	//-----------------------------------------------------------------------------
	//display interface

	U8X8 *display_;

	/*Destructor. Deletes display content arrays.*/
	~DisplayU8x8();

	/*clears the display.*/
	void clear();

	/*writes a string to the display.
	 - line to write to
	 - line position to write to
	 - message to write.*/
	void printMessage(uint16_t pos, uint16_t line, String message);

	/*updates the display on a per-character basis if new data is available.
	only 1 character is ever updated in one program loop, this ensures the display
	does not block the execution of the rest of the code.*/
	void update();

	/*@return: the currently set contrast or 0 if contrast feature is not enabled.*/
	uint8_t getContrast();

	/*sets the contrast to the given value (0 - 255).
	@return: true if call was successful, false otherwise.*/	
	bool setContrast(uint8_t value);

	/*writes the resolution of the display to the variables x and y and returns true.*/
	virtual bool getResolution(uint16_t &x, uint16_t &y);

	//end of interface
	//-----------------------------------------------------------------------------

	uint16_t res_x_;					//x resolution of display
	uint16_t res_y_;					//y resolution of display
	
	uint8_t pin_reset_;					//display reset pin

	uint8_t val_contrast_;				//contrast setting 
};

#endif /*DisplayU8X8_H_*/

