//Display Class for YAAADevice. 
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Display.h"

Display::Display() :
features_(0)
{
}

Display::~Display() 
{
}

void Display::write(uint16_t pos, uint16_t line, float message, uint8_t dec)
{
	printMessage(pos, line, String(message, dec));
}

bool Display::checkFeature(uint32_t feature)
{
	//check if the given feature bit is set for this device.
	if ((feature && features_) != 0)
		return true;

	else return false;
}

bool Display::addCustomChar(uint8_t index, uint8_t* character)
{
	return false;
}

bool Display::printCustomChar(uint16_t pos, uint16_t line, byte index)
{
	return false;
}

uint8_t Display::getBrightness()
{
	return 0;
}

uint8_t Display::getContrast()
{
	return 0;
}

bool Display::setBrightness(uint8_t value)
{
	return false;
}

bool Display::setContrast(uint8_t value)
{
	return false;
}

bool Display::getResolution(uint16_t &x, uint16_t &y)
{
	x = 0;
	y = 0;
	return false;
}

void Display::setFeature(uint32_t feature)
{
	features_ |= feature;
}

void Display::writeDefaults()
{
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_TYPE,
		0,
		static_cast<uint8_t>(Display::DISPLAY_NONE));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_RES_X,
		0,
		static_cast<uint16_t>(20));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_RES_Y,
		0,
		static_cast<uint16_t>(4));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_UPDATE_INTERVAL,
		0,
		static_cast<uint32_t>(250));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_BRIGHTNESS,
		0,
		static_cast<uint8_t>(128));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_CONTRAST,
		0,
		static_cast<uint8_t>(0));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_TYPE,
		0,
		static_cast<uint8_t>(Display::INTERFACE_NATIVE));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN0,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN1,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN2,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN3,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN4,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN5,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN6,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN7,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN8,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN9,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN10,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN11,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN12,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN13,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN14,
		0,
		static_cast<uint8_t>(255));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::DISPLAY_INTERFACE_PIN15,
		0,
		static_cast<uint8_t>(255));
}

