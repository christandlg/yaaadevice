//Stepper Motor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "StepperMotor.h"

//constructor for Stepper Motor objects using callback functions.
StepperMotor::StepperMotor(void(*forward)(), void(*backward)()) : 
AccelStepper(forward, backward),
stepper_driver_(ADAFRUIT_MOTORSHIELD_V2),

step_type_(StepperMotor::STEPTYPE_FULL),

stepper_released_(false),

automatic_stepper_release_(false)
{
	//nothing to do here...
}

//constructor for 4 wire stepper.
StepperMotor::StepperMotor(uint8_t pin1, uint8_t pin2, uint8_t pin3, uint8_t pin4, bool enable) :
AccelStepper(AccelStepper::FULL4WIRE, pin1, pin2, pin3, pin4, enable),
stepper_driver_(FOUR_WIRE_STEPPER),

step_type_(StepperMotor::STEPTYPE_FULL),

stepper_released_(false),

automatic_stepper_release_(false)
{
	//nothing to do here...
}

//constructor for A4988 / DRV8825 stepper driver.
StepperMotor::StepperMotor(uint8_t driver, uint8_t step, uint8_t direction, uint8_t enable_pin, uint8_t ms1, uint8_t ms2, uint8_t ms3, bool enable) :
AccelStepper(AccelStepper::DRIVER, step, direction, 0, 0, enable),
stepper_driver_(driver),

step_type_(StepperMotor::STEPTYPE_FULL),

stepper_released_(false),

automatic_stepper_release_(false)
{
	//nothing to do here...
}

StepperMotor::~StepperMotor()
{
}

bool StepperMotor::run()
{
	automaticRelease();

	return AccelStepper::run();
}

bool StepperMotor::runSpeed()
{
	automaticRelease();

	return AccelStepper::runSpeed();
}

bool StepperMotor::runSpeedToPosition()
{
	automaticRelease();

	return AccelStepper::runSpeedToPosition();
}

void StepperMotor::hardStop()
{
	AccelStepper::move(0);
	AccelStepper::setSpeed(0.0);
}

void StepperMotor::activate()
{
	AccelStepper::enableOutputs();

	stepper_released_ = false;
}

void StepperMotor::release()
{
	AccelStepper::disableOutputs();

	stepper_released_ = true;
}

uint8_t StepperMotor::getStepType()
{
	return step_type_;
}

bool StepperMotor::setStepType(uint8_t step_type)
{
	return false;
}

void StepperMotor::setAutomaticRelease(bool enabled)
{
	automatic_stepper_release_ = enabled;
}

long StepperMotor::getMicroStepFactor(uint8_t step_type)
{
	//return microstep factor of currently active steptype when no steptype was given.
	if (step_type == 255) 
		step_type = getStepType();

	switch (step_type)
	{
	case STEPTYPE_MICROSTEP_2:
		return 2L;
	case STEPTYPE_MICROSTEP_4:
		return 4L;
	case STEPTYPE_MICROSTEP_8:
		return 8L;
	case STEPTYPE_MICROSTEP_16:
		return 16L;
	case STEPTYPE_MICROSTEP_32:
		return 32L;
	case STEPTYPE_MICROSTEP_64:
		return 64L;
	case STEPTYPE_MICROSTEP_128:
		return 128L;
	default:
		//STEPTYPE_QUICKSTEP, STEPTYPE_SINGLE, STEPTYPE_FULL
		return 1L;
	}
}

void StepperMotor::setMicroStepPinsInverted(bool ms1_inv, bool ms2_inv, bool ms3_inv)
{
	return;
}

//--------------------------------------------------------------------------------------------------
//private member functions.
void StepperMotor::automaticRelease()
{
	//TODO:
	//this function prevents corrent function of AFMSV2 connected steppers.
	
	//if the stepper is at its destination and automatic release is activated, 
	//release the stepper.
	if (atDestination())
	{
		if (automatic_stepper_release_ && !stepper_released_)
		{
			release();
		}
	}

	//if the stepper is released and is not at its destination, activate the stepper
	else if (stepper_released_)
	{
		activate();
	}
}

bool StepperMotor::atDestination()
{
	return (AccelStepper::distanceToGo() == 0) && (AccelStepper::speed() == 0.0);
}

