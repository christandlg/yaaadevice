//Command Interface (Serial) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "CommandInterfaceSerial.h"

CommandInterfaceSerial::CommandInterfaceSerial() :
	baud_rate_(DEF_G_SERIAL_BAUDRATE)

{
	//read baud rate from the EEPROM. 
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SERIAL_BAUD_RATE,
		0,
		baud_rate_);

	//when the baud rate is invalid, use DEFAULT_BAUD_RATE.
	if (!checkBaudRate(baud_rate_))
		baud_rate_ = DEF_G_SERIAL_BAUDRATE;

#ifdef ARDUINO_SAM_DUE
	SerialUSB.begin(baud_rate_);
	while (!SerialUSB);		//wait for the serial port to open
#else
	Serial.begin(baud_rate_);

#endif
}

CommandInterfaceSerial::~CommandInterfaceSerial()
{
#ifdef ARDUINO_SAM_DUE
	SerialUSB.end();
#else
	Serial.end();
#endif
}

bool CommandInterfaceSerial::changeBaudRate(uint32_t baud_rate)
{
	//do nothing if an invalid baud rate is given
	if (checkBaudRate(baud_rate) == false)
		return false;

	//save baud rate to EEPROM
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SERIAL_BAUD_RATE,
		0,
		baud_rate);

	//wait for transmission of outgoing data to complete.
#ifdef ARDUINO_SAM_DUE
	SerialUSB.flush();
#else
	Serial.flush();
#endif

	delay(2);				//wait until the last byte is sent

							//close serial connection
#ifdef ARDUINO_SAM_DUE
	SerialUSB.end();
#else
	Serial.end();
#endif

	//reopen serial connection with given baud rate
#ifdef ARDUINO_SAM_DUE
	SerialUSB.begin(baud_rate);
	while (!SerialUSB);		//wait for the serial port to open
#else
	Serial.begin(baud_rate);
#endif

	return true;
}

bool CommandInterfaceSerial::checkBaudRate(uint32_t baud_rate)
{
	//valid baud rates: 300, 600, 1200, 2400, 4800, 9600, 
	//19200, 28800, 38400, 57600, 115200.
	if (baud_rate == SERIAL_BAUD_RATE_300 ||
		baud_rate == SERIAL_BAUD_RATE_600 ||
		baud_rate == SERIAL_BAUD_RATE_1200 ||
		baud_rate == SERIAL_BAUD_RATE_2400 ||
		baud_rate == SERIAL_BAUD_RATE_4800 ||
		baud_rate == SERIAL_BAUD_RATE_9600 ||
		baud_rate == SERIAL_BAUD_RATE_19200 ||
		baud_rate == SERIAL_BAUD_RATE_28800 ||
		baud_rate == SERIAL_BAUD_RATE_38400 ||
		baud_rate == SERIAL_BAUD_RATE_57600 ||
		baud_rate == SERIAL_BAUD_RATE_115200
		)
		return true;

	return false;
}

String CommandInterfaceSerial::getConfig(uint8_t config)
{
	String return_value = "";

	switch (config) {
		case CommandInterfaceSerial::CONFIG_BAUD_RATE:
			return_value = String(baud_rate_);
			break;
		default:
			break;
	}

	return return_value;
}

bool CommandInterfaceSerial::setConfig(uint8_t config, String value)
{
	switch (config)
	{
		case CommandInterfaceSerial::CONFIG_BAUD_RATE:
			return changeBaudRate(static_cast<uint32_t>(value.toInt()));
		default:
			break;
	}

	return false;
}

int CommandInterfaceSerial::available()
{
	return Serial.available();
}

int CommandInterfaceSerial::read()
{
	return Serial.read();
}

int CommandInterfaceSerial::peek()
{
	return Serial.peek();
}

size_t CommandInterfaceSerial::write(uint8_t data)
{
	return Serial.write(data);
}

//size_t CommandInterfaceSerial::write(const uint8_t * buffer, size_t size)
//{
//	return Serial.write(buffer, size);
//}

void CommandInterfaceSerial::flush()
{
	Serial.flush();
}

void CommandInterfaceSerial::writeDefaults()
{
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SERIAL_BAUD_RATE,
		0,
		static_cast<uint32_t>(DEF_G_SERIAL_BAUDRATE)
	);
}

bool CommandInterfaceSerial::beginInterface()
{
	//read baud rate from the EEPROM. 
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::SERIAL_BAUD_RATE,
		0,
		baud_rate_);

	//when the baud rate is invalid, use DEFAULT_BAUD_RATE.
	if (!checkBaudRate(baud_rate_))
		baud_rate_ = DEF_G_SERIAL_BAUDRATE;

#ifdef ARDUINO_SAM_DUE
	SerialUSB.begin(baud_rate_);
	while (!SerialUSB);		//wait for the serial port to open
#else
	Serial.begin(baud_rate_);

#endif

	return true;
}


