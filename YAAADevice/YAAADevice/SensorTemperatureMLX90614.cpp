//MLX90614 Temperature sensor class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "SensorTemperatureMLX90614.h"

SensorTemperatureMLX90614::SensorTemperatureMLX90614(YAAASensor::SensorSettings sensor_settings):
	SensorTemperature(sensor_settings), 
	i2c_address_(DEF_SENSOR_TEMPERATURE_MLX90614_I2C_ADDRESS),
	zone_(DEF_SENSOR_TEMPERATURE_MLX90614_ZONE),
	emissivity_(DEF_SENSOR_TEMPERATURE_MLX90614_EMISSIVITY),
	//value_ambient_(0),
	value_(0)
{
	i2c_address_ = sensor_settings.parameters_[0];
	zone_ = sensor_settings.parameters_[1];
	emissivity_ = ((sensor_settings.parameters_[2] << 8) | sensor_settings.parameters_[3]);
}


SensorTemperatureMLX90614::~SensorTemperatureMLX90614()
{
	//nothing to do here...
}

bool SensorTemperatureMLX90614::begin()
{
	Wire.setClock(100000L);			//MLX90614 max I2C clock is 100kHz

	//attempt to read a temperature
	Wire.beginTransmission(i2c_address_);
	Wire.write(OPCODE_RAM_ACCESS | RAM_TEMP_A);			//read ambient temperature command
	if (Wire.endTransmission(false) != 0)
		return false;

	//expect 3 bytes (2 bytes data, 1 byte PEC)
	if (Wire.requestFrom(i2c_address_, static_cast<uint8_t>(3)) != 3)
		return false;

	//TODO implement PEC calculation
	////attempt to set emissivity correction coefficient
	////write 0 to EEPROM before writing desired value.
	//Wire.beginTransmission(i2c_address_);
	//Wire.write(OPCODE_EEPROM_ACCESS | EEPROM_ECC);
	//Wire.write(0);
	//Wire.write(0);
	//Wire.write(/*PEC*/);
	//if (Wire.endTransmission(false) != 0)
	//	return false;
	//
	////write desired ECC value
	//Wire.beginTransmission(i2c_address_);
	//Wire.write(OPCODE_EEPROM_ACCESS | EEPROM_ECC);
	//Wire.write(emissivity_);
	//Wire.write(/*PEC*/);
	//if (Wire.endTransmission(false) != 0)
	//	return false;

	return true;
}

bool SensorTemperatureMLX90614::measure()
{
	//reset saved temperature value.
	value_ = 0;

	//read object temperature
	Wire.beginTransmission(i2c_address_);
	Wire.write(OPCODE_RAM_ACCESS | zone_);		//read zone temperature
	if (Wire.endTransmission(false) != 0)
		return false;

	//expect 3 bytes (2 bytes data, 1 byte PEC)
	if (Wire.requestFrom(i2c_address_, static_cast<uint8_t>(3)) != 3)
		return false;

	value_ = Wire.read();
	value_ |= Wire.read() << 8;

	//check if error bit is set
	if (checkErrorBit(value_))
		return false;

	updateMeasurementTime();
	return true;
}

bool SensorTemperatureMLX90614::hasValue()
{
	return ((value_ != 0) && !checkErrorBit(value_));
}

float SensorTemperatureMLX90614::getValue()
{
	return convertTemperature(
		calculateTemperature(value_),
		SensorTemperature::UNIT_KELVIN,
		temperature_unit_);
}

float SensorTemperatureMLX90614::calculateTemperature(uint16_t raw_value)
{
	//ignore MSB (used as error indicator in object temperature raw value)
	raw_value &= 0x7FFF;

	//1LSB = 0.02K
	return static_cast<float>(raw_value) * 0.02f;
}

bool SensorTemperatureMLX90614::checkErrorBit(uint16_t raw_value)
{
	return ((raw_value & 0x8000) != 0);
}
