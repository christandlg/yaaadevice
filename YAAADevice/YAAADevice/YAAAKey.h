//Keypad key class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef YAAAKEY_H_
#define YAAAKEY_H_

#include <Arduino.h>

struct YAAAKey
{
	enum key_t : uint8_t
	{
		KEY_0 = 0,
		KEY_1 = 1,
		KEY_2 = 2,
		KEY_3 = 3,
		KEY_4 = 4,
		KEY_5 = 5,
		KEY_6 = 6,
		KEY_7 = 7,
		KEY_8 = 8,
		KEY_9 = 9,
		KEY_UP = 20,
		KEY_DOWN = 21,
		KEY_LEFT = 22,
		KEY_RIGHT = 23,
		KEY_ENTER = 30,
		KEY_ESC = 31,
		KEY_INVALID = 254,		//invalid key press (e.g. multiple keys pressed)
		KEY_NONE = 255
	};

	enum state_t : uint8_t
	{
		IDLE = 0,
		PRESSED = 1,
		RELEASED = 2,
		HOLD_SHORT = 3,
		HOLD_LONG = 4
	};

	uint8_t key_ = KEY_NONE;
	uint8_t state_ = IDLE;
};

#endif /* YAAAKEY_H_ */

