//RemoteCommandHandler class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef REMOTECOMMANDHANDLER_H_
#define REMOTECOMMANDHANDLER_H_

#include <Arduino.h>

#include "constants.h"
#include "features.h"

#include "CommandInterface.h"
#include "CommandInterfaceSerial.h"
#include "CommandInterfaceEthernet.h"
//#include "CommandInterfaceSoftwareSerial.h"
#include "CommandInterfaceWifi.h"

#include "YAAATime.h"

#include "YAAALog.h"

#include "Button.h"
#include "YAAAKeypad.h"

#include "OTAHandler.h"

#define UNKNOWN 255					//return value for unknown command / axis / ...

#define charToInt(x) (x - '0')		//converts a single digit character to an unsigned integer.

#define UNKNOWN_OPTION static_cast<uint16_t>(-1)

#define NUM_COMMAND_INTERFACES 3

class Telescope;
extern Telescope *telescope_;

class FilterWheel;
extern FilterWheel *filter_wheel_;

class Focuser;
extern Focuser *focuser_;

extern YAAALog log_;

class OTAHandler;

extern void writeAllDefaults();

class RemoteCommandHandler
{
public:
	enum command_interfaces_t : uint8_t
	{
		INTERFACE_SERIAL = 0,
		INTERFACE_ETHERNET = 1,
		INTERFACE_WIFI = 2,
		INTERFACE_LoRa = 3,
	};

	enum serial_error_codes_t : uint16_t
	{
		ERROR_NONE = 0,					//not necessary?
		ERROR_UNKNOWN_DEVICE = 1,
		ERROR_UNKNOWN_CMD = 2,
		ERROR_UNKNOWN_CMD_TYPE = 4,
		ERROR_INVALID_CMD_TYPE = 8,
		ERROR_INVALID_PARAMETER = 16,
		ERROR_MISSING_PARAMETER = 32,
		ERROR_UNKNOWN_OPTION = 64,
		ERROR_DEVICE_UNAVAILABLE = 128,
		ERROR_DEVICE_LOCKED = 256
	};

	enum device_type_t : uint8_t
	{
		GENERAL = 0,				//general variables (e.g. serial communication connection speed).
		CAMERA_ROTATOR = 1,			//variables for the camera rotator.
		DOME = 2,					//variables for the dome.
		FILTER_WHEEL = 3,			//variabled for the filter wheel.
		FOCUSER = 4,				//variables for the focuser.
		SAFETY_MONITOR = 5,			//variables for the safety monitor.
		TELESCOPE = 6,				//variables for the telescope.
		TELESCOPE_AXIS = 7,			//variables for the telescope's axes.
		OBSERVING_CONDITIONS = 8	//variables for the observing conditions sensors.
	};

	struct command_type_
	{
		enum command_type_t
		{
			GET = 0,
			SET = 1,
		};
	};

	enum protocol_t
	{
		YAAADEVICE = 0,
		LX200 = 1
	};

	struct general_command_
	{
		enum general_command_t
		{
			CONNECTED = 0,
			ONEWIRE_TEMP = 10,			//returns number of one wire temperature sensors or sensor addresses.

			TIME = 20,					//gets or sets the time of a RTC connected to the arduino.
			DATETIME = 21,				//gets or sets the time of a RTC in ISO 8601 format.
			UTC_OFFSET = 22,			//gets or sets the offset from local time to yield UTC.

			//SOFTARE_NAME = 30,		//static string "YAAADevice" 
			//SOFTWARE_VERSION = 31,		//YAAADevice software version
			//SOFTWARE_DATETIME = 32,		//upload date & time

			//DEVICE_NAME = 40,			//device name (31 chars)
			//DEVICE_INFO = 41,			//device information (31 chars)

			LOG_MODE = 50,				//gets or sets the currently set log mode
			LOG_STATE = 51,				//gets the current log mode
			LOG_DUMP = 52,				//dumps the contents of the log file to the computer
			LOG_CLEAR = 53,				//clears the log file

			ETHERNET_IP = 60,			//gets the current ethernet local IP
			ETHERNET_DNS = 61,			//gets the current ethernet DNS server
			ETHERNET_GATEWAY = 62,		//gets the current ethernet gateway
			ETHERNET_SUBNET = 63,		//gets the current ethernet subnet
			ETHERNET_PORT = 64,			//gets the current port

#if defined(ESP8266) || defined(ESP32)
			WIFI_IP = 70,				//gets the current wifi local IP
			WIFI_DNS = 71,				//gets the current wifi DNS server
			WIFI_GATEWAY = 72,			//gets the current wifi gateway
			WIFI_SUBNET = 73,			//gets the current wifi subnet
			WIFI_PORT = 74,				//gets the current port
			WIFI_RSSI = 75,				//gets current RSSI

			OTA_ENABLED = 80,			//gets if OTA is enabled or not.
			OTA_MODE = 81,				//gets current OTA mode. 
#endif /* defined(ESP8266) || defined(ESP32) */

			MQTT_ENABLED = 100,			//gets if MQTT is enabled. 
			MQTT_STATE = 101,			//gets MQTT state. 

			WRITE_DEFAULTS = 251,		//initializes EEPROM with default values.
			SRAM = 252,					//returns free SRAM in bytes.
			SETUP = 253,
			NONE = 254
		};
	};

	struct telescope_command_
	{
		enum  telescope_command_t
		{
			ALIGNMENT = 0,			//gets or sets alignment of given axis.

			SLEW = 1,				//gets slewing state or sets slewing target.
			ABORT_SLEW = 2,			//abort any slew / moveaxis command.
			MOVEAXIS = 3,			//gets or sets moveaxis state.
			TARGET = 4,				//gets or sets slew targets.
			MOVE = 5,				//slews the given axis to the given position.

			FIND_HOME = 10,			//sets homing process active or gets if homing is active.
			AT_HOME = 11,			//query if the telescope is at home.
			HOMING_ERROR = 12,		//gets error code of last homing operation.

			PARKED = 20,			//gets or sets parked state.
			PARK_POS = 21,			//gets or sets the telescope's park position

			PULSE_GUIDE = 30,		//gets or sets pulse guide state.

			TRACKING = 40,			//gets or sets tracking state.
			TRACKING_RATE = 41,		//gets or sets tracking rate.
			RA_RATE = 42,			//gets or sets right ascension axis tracking offset rate.
			DEC_RATE = 43,			//gets or sets declination axis tracking offset rate.

			SWITCH_STATE = 50,		//gets current state of endstop and home switches.

			TEMP = 60,				//temperature at given site (e.g. main mirror)
			TEMP_MEASURE = 61,		//temperature measurement start and check completed

			CONNECTED = 251,		//returns 1 if the telescope is enabled, 0 otherwise.
			WRITE_DEFAULTS = 252,	//initiates writing of default values.
			SETUP = 253,			//gets or sets telescope settings.
			NONE = 254,				//no command (command_ == NULL)
		};
	};

	struct focuser_command_
	{
		enum focuser_command_t
		{
			ABSOLUTE = 0,			//gets focuser absolute mode.
			POSITION = 1,			//gets or sets focuser position.

			MOVE = 10,				//sets focuser movement target or gets focuser movement state.
			MOVE_DIRECTION = 11,	//sets focuser to move in a given direction.
			MOVE_TIME = 12,			//sets focuser to move in a given direction for a given time.
			HALT = 13,				//halts any focuser movement except temperature compensation movement.

			TEMP = 20,				//gets current temperature or sets temperature sensor to use for temperature compensation.
			TEMP_MEASURE = 21,		//temperature measurement start and check completed
			TEMP_COMP = 22,			//gets or sets temperature compensation mode.
			TEMP_COMP_SENSOR = 23,	//gets or sets the sensor used for temperature compensation 

			FIND_HOME = 30,			//sets homing process active or gets if homing is active.
			//AT_HOME = 31,			//query if the focuser is at home.

			SWITCH_STATE = 50,		//gets current state of endstop and home switches.

			CONNECTED = 251,		//returns 1 when the focuer is enabled, 0 otherwise.
			WRITE_DEFAULTS = 252,	//initiates writing of default values.
			SETUP = 253,			//gets or sets focuser settings.
			NONE = 254,				//no command (command_ == NULL)
		};
	};

	struct filter_wheel_command_
	{
		enum filter_wheel_command_t
		{
			POSITION = 0,				//gets current filter or sets requested filter.

			FOCUS_OFFSET = 1,			//gets focus offset for specified filter.

			NUMBER_OF_FILTERS = 2,		//gets number of filters on the filter wheel / slider.
			CURRENT_FILTER = 3,			//changes current filter without moving the filter holder.

			SWITCH_STATE = 4,			//gets current state of endstop and home switches.

			FIND_HOME = 5,				//finds the filter holder's home position and updates the stepper position.
			AT_HOME = 6,
			MEASURE_HOME_OFFSET = 7,	//finds and updates the filter holder's home position.
			HOMING_ERROR = 8,			//gets error codes generated during last find home operation. 

			CONNECTED = 251,			//returns 1 if the telescope is enabled, 0 otherwise.
			WRITE_DEFAULTS = 252,		//initiates writing of default values.
			SETUP = 253,				//gets or sets filter wheel settings.
			NONE = 254					//no command (command_ == NULL)
		};
	};

	/*initializes some variables.*/
	RemoteCommandHandler();

	~RemoteCommandHandler();

	/*
	additional initialization. */
	void begin();

	/*
	queries interfaces for commands and if any have been received, executes commands. */
	void run();

	bool isInterfaceEnabled(uint8_t interface);

	static void writeDefaults();

private:

	/*
	extracts a command from an input string and stores it into input_.
	@param (by reference) input string. command will be removed from this string.
	@return true if a command was returned, false otherwise. */
	bool extractCommand(String& in);

#ifdef FEATURE_LX200
	/*
	extracts a LX200 command from an input string and stores it into input_.
	@param (by reference) input string. command will be removed from this string.
	@return true if a command was returned, false otherwise. */
	bool extractCommandLX200(String& in);
#endif /* FEATURE_LX200 */

	/*
	fetches the input of the given command interface.
	@param interface interface to check. */
	void fetchCommand(uint8_t interface);

	/*called once after a serial message has been successfully received.
	tries to parse the target device from the serial message and calls the appropriate
	[device]Command() function if successful. sends an error message to the pc when parsing 
	of the target device fails.*/
	void executeCommand();

#ifdef FEATURE_LX200
	/*called after a serial message has been successfully received and the protocol has
	been recognized as the LX200 protocol.*/
	void executeLX200();

	/*extracts charactes and returns a char array. 
	stops on '#' character.
	@param: input the complete input string.
	param start_pos: start position for string extraction.
	@param num_chars: number of characters to extract. defaults to 8.*/
	static String extractLX200String(char* input, uint8_t start_pos, uint8_t num_chars = 8);
#endif /* FEATURE_LX200 */

	/*checks if the given device is available (enabled).
	@param: device to check (as device_type_t)
	@return: true if the device is available, false if not or invalid device given.*/
	bool checkAvailable(uint8_t device);

	/*checks if the given device is locked. 
	param device: the device to check as SerialHandler::device_type_t.
	returns true if locked, false otherwise.*/
	bool checkLocked(uint8_t device);

	/*returns the correct array delimiter for the given setup option.
	@param setup_option
	@return: array delimiter*/
	static char getArrayDelimiter(uint16_t setup_device, uint16_t setup_option);

	/*@param the setup option
	@return the array size for the given setup option*/
	static uint8_t getArraySize(uint16_t setup_device, uint16_t setup_option);

	/*returns the device identified by the given string.*/
	static uint8_t getDeviceType(char* chr);

	/*returns the type of command specified by the given string. */
	uint8_t getCommandType();

	/*returns the command specified by the given string for the given device.
	@param:
	 - device identifier (EEPROMHandler::device_type_ member) */
	uint8_t parseCommand(uint8_t device);

	/*
	calls strotk(char* string, char* delimiter) with the defined delimiters.
	@param:
	 - string to be tokenized.
	@return:
	 - pointer to the token or NULL pointer (if none found).*/
	char* tokenize(char* chr);

	/*returns the axis specified by the given string. 
	@return:
	 - telescope axis as Mount::telescope_axis_t.*/
	uint16_t getTelescopeAxis(char* axis = NULL);

	//parse command for mount. 
	void telescopeCommand();

	//parses and executes mount setup commands. 
	void telescopeSetup(uint8_t command_type);

	//parse command for focuser.
	void focuserCommand();

	void focuserSetup(uint8_t command_type);

	//parse command for filterwheel.
	void filterWheelCommand();

	//parse and execute filter wheel setup commands.
	void filterWheelSetup(uint8_t command_type);

	//parse general device command.
	void generalCommand();
	void generalSetup(uint8_t command_type);

	//parse command for safety monitor
	void safetyMonitorCommand();
	void safetyMonitorSetup();

	//parse command for camera rotator
	void cameraRotatorCommand();
	void cameraRotatorSetup();

	//parse command for dome
	void domeCommand();
	void domeSetup();

	//sends an error message to the computer.
	//error messages have the following format:
	//ERROR_#
	void sendError(uint16_t error_code);

	/*sends a response to the computer. 
	 - calls sendResponse(char* device, char* command, float return_value) using the
		 last set values of device_ and command_.
	@params:
	 - return value of the last executed command.*/
	void sendResponse(float return_value);

	/*sends a response to the computer. template for all data types except float.
	 - calls sendResponse(char* device, char* command,  long return_value) using the
		 last set values of device_ and command_.
	@params:
	 - return value of the last executed command.*/
	template <class T> void sendResponse(const T& return_value);

	/*sends a response to the computer. Supports Serial and TCP connection. response is 
	sent using the interface the command was received on.
	@params:
	 - device: a string / char array specifying the device (e.g. 'W' = filter wheel)
	 - command: a string / char array specifying the command that has been executed
	 - return_value: return value of the command. default to 0 when not available. when return_value
		 is a float, print to serial with 7 digits after the decimal point.*/
	void sendResponse(char* device, char* command, String return_value);

	/*sends the given message using the current interface.*/
	template <class T> void sendRaw(const T& message);

	///*
	//converts the given time to a String in ISO8601 format.
	//@param time: time in seconds since 1970-01-01T00:00:00.
	//@return: given time in ISO8601 format.*/
	//static String timeToISO8601(time_t time);
	//
	///*
	//converts the given time ti a String in HH:MM:SS format.
	//@param time: time in seconds since 1970-01-01T00:00:00.
	//@param format_12_hrs: when set to true, returned string will be in 12hr format 
	//(default: 24hr format).
	//@return: given time in HH:MM:SS format.	*/
	//static String timeToHMS(time_t time, bool format_12_hrs = false);
	//
	///*
	//converts the given time to a String in MM/DD/YY format.
	//@param time: time in seconds since 1970-01-01T00:00:00.
	//@param delimiter: delimiter to use (default: '/').
	//@return: given date in MM/DD/YY format.	*/
	//static String timeToMDY(time_t time, char delimiter = '/');
	//
	//static String timeTo12Hrs(time_t time);
	//
	//static String timeTo24Hrs(time_t time);

	/*reads a value from EEPROM and sends it to the pc.*/
	void readOption(
		uint16_t setup_device,
		uint16_t setup_option,
		uint16_t index,
		uint8_t variable_type);

	/*parses a string to a variable of the given type / size, saves it to the EEPROM and 
	sends it to the pc.*/
	void writeOption(
		uint16_t setup_device,
		uint16_t setup_option,
		uint16_t index,
		uint8_t variable_type);

	//uint8_t protocol_;				//protocol of the current command (YAAADEVICE or LX200)

	char input_[COMMAND_MAX_LENGTH + 1];			//complete input string.
	char* device_;		//pointer to part of message indicating recipient device for the command.
	char* command_;		//pointer to part of message containing a command identifier.
	char* value_;		//pointer to part of message holding a value.

	//LX200 specific variables:
	//high precision pointing
	bool lx200_precision_;				//true: high; false: low

	//high: Dec/Az/El displays and messages HH:MM:SS sDD*MM:SS
	//low: RA displays and messages HH:MM.T sDD*MM
	bool lx200_protocol_precision_;		//true: high; false: low

	bool lx200_time_format_12hr_;		//true: 12hr format, false: 24hr format

	uint8_t command_interface_current_;
	CommandInterface *command_interfaces_[NUM_COMMAND_INTERFACES];			//collection of all command interface instances

	String input_buffers_[NUM_COMMAND_INTERFACES];	//holds all input buffers
	String input_string_;

	//TODO 
	//fetch received data from commandinterface* classes
	//store commandinterface* input in this class for all commandinterfaces

};

extern RemoteCommandHandler remote_command_handler_;

#endif /*REMOTECOMMANDHANDLER_H_*/

