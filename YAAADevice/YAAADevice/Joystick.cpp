//simple Joystick class. 
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Joystick.h"

//Joystick class contructor 
//x axis pin, y axis pin, deadzone, button pin
Joystick::Joystick(uint8_t xAxis, uint8_t yAxis, uint8_t dz, uint8_t btnPin) :
x_axis_pin_(xAxis), y_axis_pin_(yAxis),
deadzone_(dz),
button_(NULL)
{
	pinMode(x_axis_pin_, INPUT);
	pinMode(y_axis_pin_, INPUT);

	//read and save joystick offset
	readOffset();

	//create button instance only if a button pin was given.
	if (btnPin != 255)
		button_ = new Button(btnPin);
}

Joystick::~Joystick()
{
	if (button_)
		delete button_;
}

int16_t Joystick::getAxisValue(joystick_axis_t axis)
{
	uint8_t axis_pin;
	int16_t offset;

	if (axis == X_AXIS)
	{
		axis_pin = x_axis_pin_;
		offset = x_offset_;
	}

	else //if (axis == Y_AXIS)
	{
		axis_pin = y_axis_pin_;
		offset = y_offset_;
	}

	//read current joystick axis position.
	int16_t raw_value = analogRead(axis_pin);

	//apply offset.
	raw_value -= offset;

	//shift the range from 0 - 1023 to -512 to 512.
	int16_t axis_value = map(raw_value, 0, 1023, -MAX_VALUE, MAX_VALUE);

	//constrain values to -512 to 512 range.
	axis_value = constrain(axis_value, -MAX_VALUE, MAX_VALUE);

	//apply deadzone. 
	if (axis_value > -deadzone_ / 2 && axis_value < deadzone_ / 2)
		return 0;

	return axis_value;
}

uint8_t Joystick::getButtonEvent()
{
	//return 0 if the joystick does not have a button.
	if (!button_) 
		return 0;

	return button_->getEvent();
}

bool Joystick::getButtonState()
{
	//return false (button not pressed) if the joystick does not have a button.
	if (!button_) 
		return false;

	return button_->getState();
}

void Joystick::read(int16_t& x_axis, int16_t& y_axis)
{
	x_axis = getAxisValue(X_AXIS);
	y_axis = getAxisValue(Y_AXIS);
}

void Joystick::read(int16_t& x_axis, int16_t& y_axis, int16_t& button)
{
	//x_axis = getAxisValue(X_AXIS);
	//y_axis = getAxisValue(Y_AXIS);
	read(x_axis, y_axis);

	button = getButtonEvent();
}

void Joystick::setDeadzone(uint8_t dz)
{
	deadzone_ = dz;
}

void Joystick::readOffset()
{
	x_offset_ = analogRead(x_axis_pin_) - 512;
	y_offset_ = analogRead(y_axis_pin_) - 512;
}

