//Command Interface (WiFi) class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#if defined(ESP8266) || defined(ESP32)

#include "CommandInterfaceWifi.h"

CommandInterfaceWifi::CommandInterfaceWifi() : 
	server_(NULL),
	port_(DEF_G_WIFI_PORT),
	use_dhcp_(DEF_G_WIFI_DHCP)
{
	//nothing to do here...
}

CommandInterfaceWifi::~CommandInterfaceWifi()
{
	//nothing to do here...
}

String CommandInterfaceWifi::getConfig(uint8_t config)
{
	String return_value = "";

	switch (config)
	{
		case CONFIG_IP:
		case CONFIG_DNS:
		case CONFIG_GATEWAY:
		case CONFIG_SUBNET:
		{
			IPAddress ip;

			switch (config) {
				case CONFIG_IP:
					ip = WiFi.localIP();
					break;
				case CONFIG_DNS:
					ip = WiFi.dnsIP();
					break;
				case CONFIG_GATEWAY:
					ip = WiFi.gatewayIP();
					break;
				case CONFIG_SUBNET:
					ip = WiFi.subnetMask();
					break;
				default:
					ip = IPAddress(0, 0, 0, 0);
					break;
			}

			return_value += String(ip[0]);
			return_value += ".";
			return_value += String(ip[1]);
			return_value += ".";
			return_value += String(ip[2]);
			return_value += ".";
			return_value += String(ip[3]);
		}
		break;
		case CONFIG_PORT:
			return_value = String(port_);
			break;
		case CONFIG_RSSI:
			return_value = String(WiFi.RSSI());
			break;
		default:
			return_value = "";
			break;
	}

	return return_value;
}

bool CommandInterfaceWifi::setConfig(uint8_t config, String value)
{
	return false;
}

int CommandInterfaceWifi::available()
{
	return client_ && client_.available();
}

int CommandInterfaceWifi::read()
{
	return client_.read();
}

int CommandInterfaceWifi::peek()
{
	return client_.peek();
}

size_t CommandInterfaceWifi::write(uint8_t data)
{
	return client_.write(data);
}

size_t CommandInterfaceWifi::write(const uint8_t * buffer, size_t size)
{
	return client_.write(buffer, size);
}

void CommandInterfaceWifi::flush()
{
	client_.flush();
}

void CommandInterfaceWifi::writeDefaults()
{
	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_ENABLED,
		0,
		static_cast<byte>(DEF_G_WIFI_ENABLED));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_DHCP,
		0,
		static_cast<uint8_t>(DEF_G_WIFI_DHCP));

	byte mac[] = DEF_G_WIFI_MAC;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_MAC,
		0,
		mac,
		sizeof(mac));

	uint8_t ip[] = DEF_G_WIFI_IP;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_IP,
		0,
		ip,
		sizeof(ip));

	uint8_t dns[] = DEF_G_WIFI_DNS;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_DNS,
		0,
		dns,
		sizeof(dns));

	uint8_t gateway[] = DEF_G_WIFI_GATEWAY;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_GATEWAY,
		0,
		gateway,
		sizeof(gateway));

	uint8_t subnet[] = DEF_G_WIFI_SUBNET;
	EEPROMHandler::writeArray(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_SUBNET,
		0,
		subnet,
		sizeof(subnet));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_PORT,
		0,
		static_cast<uint16_t>(DEF_G_WIFI_PORT));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_CHANNEL,
		0,
		static_cast<uint16_t>(DEF_G_WIFI_CHANNEL));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_SSID_LENGTH,
		0,
		static_cast<uint8_t>(DEF_G_WIFI_SSID_LENGTH));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_SSID,
		0,
		DEF_G_WIFI_SSID);

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_PASSWORD_LENGTH,
		0,
		static_cast<uint8_t>(DEF_G_WIFI_PASSWORD_LENGTH));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_PASSWORD,
		0,
		DEF_G_WIFI_PASSWORD);

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_HOSTNAME_LENGTH,
		0,
		static_cast<uint8_t>(DEF_G_WIFI_HOSTNAME_LENGTH));

	EEPROMHandler::writeValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_HOSTNAME,
		0,
		DEF_G_WIFI_HOSTNAME);
}

bool CommandInterfaceWifi::beginInterface()
{
	bool enabled = false;

	//read DHCP setting
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_ENABLED,
		0,
		enabled);

	if (!enabled)
	{
		WiFi.mode(WIFI_OFF);
		return false;
	}

	WiFi.persistent(true);
	WiFi.mode(WIFI_STA);

	//read DHCP setting
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_DHCP,
		0,
		use_dhcp_);

	//if not using DHCP, read ip address, subnet, gateway and dns server address from EEPROM
	if (!use_dhcp_)
	{
		//read IP, Gateway and Subnet from EEPROM
		uint8_t ip[4] = DEF_G_ETHERNET_IP;
		uint8_t gateway[4] = DEF_G_ETHERNET_GATEWAY;
		uint8_t dns[4] = DEF_G_ETHERNET_DNS;
		uint8_t subnet[4] = DEF_G_ETHERNET_SUBNET;

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::WIFI_IP,
			0,
			ip,
			sizeof(ip));

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::WIFI_DNS,
			0,
			dns,
			sizeof(dns));

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::WIFI_GATEWAY,
			0,
			gateway,
			sizeof(gateway));

		EEPROMHandler::readArray(
			EEPROMHandler::device_type_::GENERAL,
			EEPROMHandler::general_variable_::WIFI_SUBNET,
			0,
			subnet,
			sizeof(subnet));

		WiFi.config(IPAddress(ip), IPAddress(gateway), IPAddress(subnet), IPAddress(dns));
	}

	//read host name from EEPROM
	uint8_t hostname_length = 0;
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_HOSTNAME_LENGTH,
		0,
		hostname_length);

	if ((hostname_length > 0) && (hostname_length <= 63))
	{
		if (char *content = new char[hostname_length + 1])
		{
			EEPROMHandler::readArray(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_HOSTNAME,
				0,
				content,
				hostname_length);

			content[hostname_length] = '\0';

#if defined(ESP8266) 
			WiFi.hostname(content);		//TODO using a hostname seems not to work.
#elif defined(ESP32)
			WiFi.setHostname(content);		//TODO using a hostname seems not to work.
#endif

			delete[] content;
			content = NULL;
		}
	}

	//read SSID, PSK and channel from EEPROM
	uint8_t eeprom_SSID_length = 0;
	uint8_t eeprom_PWD_length = 0;

	String eeprom_SSID = "";
	String eeprom_PWD = "";

	uint8_t eeprom_channel = 0;

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_SSID_LENGTH,
		0,
		eeprom_SSID_length);

	if ((eeprom_SSID_length > 0) && (eeprom_SSID_length <= 63))
	{
		if (char *content = new char[eeprom_SSID_length + 1])

		{
			EEPROMHandler::readArray(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_SSID,
				0,
				content,
				eeprom_SSID_length);

			content[eeprom_SSID_length] = '\0';

			eeprom_SSID = String(content);

			delete[] content;
			content = NULL;
		}
	}

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_PASSWORD_LENGTH,
		0,
		eeprom_PWD_length);

	if ((eeprom_PWD_length > 0) && (eeprom_PWD_length <= 63))
	{
		if (char *content = new char[eeprom_PWD_length + 1])
		{
			EEPROMHandler::readArray(
				EEPROMHandler::device_type_::GENERAL,
				EEPROMHandler::general_variable_::WIFI_PASSWORD,
				0,
				content,
				eeprom_PWD_length);

			content[eeprom_PWD_length] = '\0';

			eeprom_PWD = String(content);

			delete[] content;
			content = NULL;
		}
	}

	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_CHANNEL,
		0,
		eeprom_channel);

	if (WiFi.status() != WL_CONNECTED)		//fix for ESP8266 board v. 2.3.0, see https://github.com/esp8266/Arduino/issues/2702
		WiFi.begin(eeprom_SSID.c_str(), eeprom_PWD.c_str(), static_cast<int32_t>(eeprom_channel));

	uint32_t connect_start = millis();

	// Wait for connection
	do
	{
		delay(500);
	} while ((WiFi.status() != WL_CONNECTED) && (millis() - connect_start < CONNECT_TIMEOUT));

	//read port setting
	EEPROMHandler::readValue(
		EEPROMHandler::device_type_::GENERAL,
		EEPROMHandler::general_variable_::WIFI_PORT,
		0,
		port_);

	//open server...
	server_ = WiFiServer(port_);

	server_.begin();
	server_.setNoDelay(true);

	return true;
}

#endif /* defined(ESP8266) || defined(ESP32) */
