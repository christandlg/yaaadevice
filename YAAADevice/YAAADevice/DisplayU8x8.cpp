//U8x8 Display class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "DisplayU8x8.h"

DisplayU8x8::DisplayU8x8(uint16_t res_x, uint16_t res_y, uint8_t pin_reset, uint8_t i2c_sda, uint8_t i2c_scl) :
  res_x_(res_x), res_y_(res_y),
  pin_reset_(pin_reset),
  val_contrast_(0),
  display_(NULL)
{
  //set supported features
  setFeature(Display::FEATURE_TEXT);
  setFeature(Display::FEATURE_CUSTOM_CHAR);
  setFeature(Display::FEATURE_DRAW);
  setFeature(Display::FEATURE_CONTRAST);

  //create SD1306 object. use software I2C if SDA and SCL pins have been specified.
  if ((i2c_sda != 255) && (i2c_scl != 255))
    display_ = new U8X8_SSD1306_128X64_NONAME_SW_I2C(i2c_scl, i2c_sda, pin_reset_);
  else
    display_ = new U8X8_SSD1306_128X64_NONAME_HW_I2C(pin_reset_);

  if (!display_)
    return;

  display_->begin();
  display_->setPowerSave(0);

  printMessage(0, 10, "Hello World!");
}

DisplayU8x8::~DisplayU8x8()
{
  if (display_)
  {
    delete display_;
    display_ = NULL;
  }
}

void DisplayU8x8::update()
{
  //TODO implement
}
uint8_t DisplayU8x8::getContrast()
{
  return val_contrast_;
}

bool DisplayU8x8::setContrast(uint8_t value)
{
  if (!display_)
    return false;

  //return false if brightness pin has not been set.
  if (!checkFeature(Display::FEATURE_CONTRAST))
    return false;

  val_contrast_ = value;

  display_->setContrast(val_contrast_);

  return true;
}

bool DisplayU8x8::getResolution(uint16_t &x, uint16_t &y)
{
  x = res_x_;
  y = res_y_;

  return true;
}

void DisplayU8x8::printMessage(uint16_t pos, uint16_t line, String message)
{
  if (!display_)
    return;

  display_->setFont(u8x8_font_chroma48medium8_r);	// choose a suitable font
  display_->drawString(pos, 10 * line, message.c_str());	// write something to the internal memory
}

void DisplayU8x8::clear()
{
  if (!display_)
    return;

  display_->clear();
}

