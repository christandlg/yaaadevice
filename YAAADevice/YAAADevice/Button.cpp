//simple button class.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#include "Button.h"

Button::Button(uint8_t pinNr, bool invert) :
	pin_number_(pinNr),
	inverted_(invert),

	current_state_(false), previous_state_(false),
	last_read_(0L), press_start_(0L)
{
	//set button pin to INPUT and activate internal pullup resistor
	pinMode(pin_number_, INPUT_PULLUP);

	previous_state_ = getState();	//initialize previous state.
}

Button::Button(ButtonSettings settings) :
	pin_number_(settings.pin_nr_),
	inverted_(settings.inverted_),

	current_state_(false), previous_state_(false),
	last_read_(0L), press_start_(0L)
{
	//set button pin to INPUT and activate internal pullup resistor
	pinMode(pin_number_, INPUT_PULLUP);

	previous_state_ = getState();	//initialize previous state.
}

uint8_t Button::getEvent()
{
	//software debouncing, not reliable 
	//button state is only read every DEBOUNCING_DELAY milliseconds to prevent false readings
	//caused by pressing / releasing the button.
	if (millis() - last_read_ > DEBOUNCING_DELAY)
	{
		last_read_ = millis();

		//read button state
		getState();

		//if the state of the button has changed
		if (current_state_ != previous_state_)
		{
			//button changed from UP to DOWN
			if (current_state_ == true)
				return Button::EVENT_PRESS;

			//button changed from DOWN to UP
			else
				return Button::EVENT_RELEASE;
		}
		else if (current_state_)
		{
			if (millis() - press_start_ >= LONGPRESS_INTERVAL)
				return Button::EVENT_HOLD_LONG;
			else
				return Button::EVENT_HOLD_SHORT;
		}
	}

	return Button::EVENT_NONE;
}

bool Button::getState()
{
	previous_state_ = current_state_;

	current_state_ = ((digitalRead(pin_number_) == LOW) ^ inverted_);

	//save time of press start
	if (current_state_ && (previous_state_ != current_state_))
		press_start_ = millis();

	return current_state_;
}

uint8_t Button::getPinNumber()
{
	return pin_number_;
}

bool Button::getInverted()
{
	return inverted_;
}

