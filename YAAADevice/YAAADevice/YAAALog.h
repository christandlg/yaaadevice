//Log class for YAAADevice.
//Copyright (C) 2014-2018  Gregor Christandl
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


#ifndef YAAALOG_H_
#define YAAALOG_H_

#include <SPI.h>
#include <SD.h>

#include "YAAATime.h"

#define LINE_LENGTH 80		//maximum length of one line during log file dumping

class YAAALog
{
public:
	enum log_state_t : int8_t {
		LOG_ERROR_SD = -3,
		LOG_ERROR_FILE = -2,
		LOG_NOT_INITIALIZED = -1,			//log class has not yet been initialized or initialization failed
		LOG_DISABLED = 0,					//logging is disabled
		LOG_ENABLED = 1,					//logging is enabled
	};

	enum sd_state_t : uint8_t
	{
		SD_INITIALIZED = 0,				//SD card has been initialized
		SD_NOT_INITIALIZED = 1,			//SD card has not yet been initialized
		SD_ERROR = 2,					//SD card error
	};

	YAAALog(String file_name);

	~YAAALog();

	/*
	initializes the SD card. must be called before calling begin() on any instance. 
	@param chip_select_pin SPI SD card chip select pin.
	@return true on success or if the SD card was successfully initialized before, false otherwise. */
	static bool initSD(uint8_t chip_select_pin);

	/*
	initilaizes a YAAALog instance.  
	@return true on success false otherwise. */
	bool begin();

	//static uint8_t getSDState();

	bool setLogMode(bool enable);

	bool getLogMode();

	uint8_t getLogState();

	//logs a message to the log file
	//@param value value to log to file 
	//@return true if the message was logged correctly, false otherwise
	template <class T> bool log(T& value, uint8_t file_index = 0)
	{
		//return false if the log class has not been initialized.
		if (log_state_ != YAAALog::LOG_ENABLED)
			return false;

		//return false if the log file could not be opened.
		if (!file_)
			return false;

		//generate timestamp
		char *time_string = new char[20];
		YAAATime::timeToISO8601(time_string);

		//save log message
		file_.println(String(time_string) + "\t" + String(value));
		delete time_string;

		//save file
		file_.flush();

		return true;
	}

	//@return: true if more lines can be read from the file (the end of the file
	//has not yet been reached), false otherwise.
	bool hasLine();

	//@return: the next line of the file. inserts a break after LINE_LENGTH characters.
	//returns an empty string if the log file can not be read.
	String getLine();

	//seeks to the beginning of the log file.
	void startDump();

	//seeks to the end of the log file.
	void stopDump();

	//removes the log file and creates a new one.
	//@return true if successful, false if the old file could not be deleted or 
	//a new one could not be created.
	bool clearLog();

private:
	static uint8_t sd_state_;		//SD card state as sd_state_t

	int8_t log_state_;				//Log state as log_state_t.

	String file_name_;	

	File file_;
};

#endif /*YAAALOG_H_*/

